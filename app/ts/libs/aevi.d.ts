/**
 * d.ts extensions for grid
 */

declare module JQueryUI {
	interface Datepicker extends Widget, DatepickerOptions {
		_gotoToday(...args: any[]): void;
    }
}

interface Event {
    dataTransfer: any;
}

interface BaseJQueryEventObject extends Event {
    dataTransfer: any;
    files: any;
}

interface Element {
    files: any;
}

interface Document {
    selection: any;
}

/**
 * Cropper
 */
declare module Cropper {
    interface CropperOptions {
        aspectRatio: any;
        autoCropArea: number;
        strict: boolean;
        guides: boolean;
        background: boolean;
        dragCrop: boolean;
        cropBoxMovable: boolean;
        cropBoxResizable: boolean;
        doubleClickToggle: boolean;
    }

    interface Static {
        cropper(...args: any[]): any;
    }
    interface JQueryExtension {
        cropper(...args: any[]): any;
    }
}

interface JQueryStatic {
    cropper(...args: any[]): Cropper.Static;
}

interface JQuery {
    cropper(...args: any[]): Cropper.JQueryExtension;
}

declare module 'cropper' {
    export = Cropper;
}
