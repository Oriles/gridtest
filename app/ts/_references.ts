/**
 * bootstrap
 */

/// <reference path='references.ts' />
/// <reference path='MyRetail/AeviAjaxPreloader.ts' />
/// <reference path='MyRetail/AeviGlobal.ts' />
/// <reference path='MyRetail/AeviHelp.ts' />
/// <reference path='MyRetail/AeviSearch.ts' />
/// <reference path='MyRetail/AeviStorage.ts' />

/**
 * API
 */
/// <reference path='MyRetail/AeviApiService/IAeviApiService.ts' />
/// <reference path='MyRetail/AeviApiService/AeviApiService.ts' />
/// <reference path='MyRetail/AeviApiService/AeviApiDataService.ts' />
/// <reference path='MyRetail/AeviApiService/AeviApiAuthorization.ts' />

/**
 * DATA
 */


/// <reference path='MyRetail/AeviDataService/IAeviEntityStatusResponse.ts' />
/// <reference path='MyRetail/AeviDataService/IAeviResponseStatus.ts' />
/// <reference path='MyRetail/AeviDataService/IAeviDataService.ts' />
/// <reference path='MyRetail/AeviDataService/IAeviSuccessResponse.ts' />
/// <reference path='MyRetail/AeviDataService/IAeviPutResponse.ts' />
/// <reference path='MyRetail/AeviDataService/IAeviCommitResponse.ts' />
/// <reference path='MyRetail/AeviDataService/IAeviEntitySuccessResponse.ts' />
/// <reference path='MyRetail/AeviDataService/IImportResponse.ts' />
/// <reference path='MyRetail/AeviDataService/IAeviBlockStateCommand.ts' />

/// <reference path='MyRetail/AeviDataService/IAeviErrorResponse.ts' />
/// <reference path='MyRetail/AeviDataService/AeviDataService.ts' />
/// <reference path='MyRetail/AeviDataRepository/IAeviDataRepository.ts' />

/**
 * REPOSITORY
 */
/// <reference path='MyRetail/AeviDataRepository/AeviUser.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviDataRepository.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviDataEntity.ts' />
/// <reference path='MyRetail/AeviDataRepository/IAeviRecordIdentification.ts' />

/**
 * TABLE DESCRIPTION
 */
/// <reference path='MyRetail/AeviDataRepository/AeviTableDescription/AeviTableDescription.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviTableDescription/IAeviTableDescription.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviTableDescription/IAeviSaveModeExcel.ts' />

/// <reference path='MyRetail/AeviDataRepository/AeviTableDescription/AeviTableColumnDescription/IAeviWidthType.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviTableDescription/AeviTableColumnDescription/IAeviSemanticType.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviTableDescription/AeviTableColumnDescription/IAeviDisplayType.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviTableDescription/AeviTableColumnDescription/IAeviColumnActionType.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviTableDescription/AeviTableColumnDescription/IAeviColumnDescription.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviTableDescription/AeviTableColumnDescription/IAeviEntityActionDefinition.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviTableDescription/AeviTableColumnDescription/IAeviEntityActionResultType.ts' />

/**
 * FORM DESCRIPTION
 */
/// <reference path='MyRetail/AeviDataRepository/AeviFormDescription/AeviFormDescription.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviFormDescription/IAeviFormDescription.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviFormDescription/IAeviFormSuccessResponse.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviFormDescription/IAeviRowDescription.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviFormDescription/IAeviItemDescription.ts' />



/**
 * TABLE DATA
 */
/// <reference path='MyRetail/AeviDataRepository/AeviTableData.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviDataSorter.ts' />
/// <reference path='MyRetail/AeviDataRepository/AeviStatus.ts' />


/**
 * FORM
 */
/// <reference path='MyRetail/AeviForm/AeviForm.ts' />
/// <reference path='MyRetail/AeviForm/AeviFormDataBinder.ts' />
/// <reference path='MyRetail/AeviForm/AeviFormRepository.ts' />
/// <reference path='MyRetail/AeviForm/AeviFormView.ts' />
/// <reference path='MyRetail/AeviForm/AeviFormEditorFactory.ts' />

/**
 * DOM
 */
/// <reference path='MyRetail/AeviTableHeader/AeviHeaderRow.ts' />
/// <reference path='MyRetail/AeviTableHeader/AeviHeaderColumn.ts' />
/// <reference path='MyRetail/AeviTableBody/AeviBodyRow.ts' />
/// <reference path='MyRetail/AeviTableBody/AeviBodyCell.ts' />

/**
 * ROOT
 */
/// <reference path='MyRetail/AeviClipboard.ts' />
/// <reference path='MyRetail/AeviDate.ts' />
/// <reference path='MyRetail/AeviGrid.ts' />
/// <reference path='MyRetail/AeviImage.ts' />
/// <reference path='MyRetail/AeviStatusBar.ts' />
/// <reference path='MyRetail/AeviBubbless.ts' />
/// <reference path='MyRetail/AeviDimensions.ts' />
/// <reference path='MyRetail/AeviGridApi.ts' />
/// <reference path='MyRetail/AeviLocalization.ts' />
/// <reference path='MyRetail/AeviSum.ts' />
/// <reference path='MyRetail/AeviClientSide.ts' />
/// <reference path='MyRetail/AeviDOM.ts' />
/// <reference path='MyRetail/AeviGridEditor.ts' />
/// <reference path='MyRetail/AeviGridLocker.ts' />
/// <reference path='MyRetail/AeviPager.ts' />
/// <reference path='MyRetail/AeviToolbar.ts' />
/// <reference path='MyRetail/AeviToolbarSubMenu.ts' />
/// <reference path='MyRetail/AeviConsts.ts' />
/// <reference path='MyRetail/AeviGuidGenerator.ts' />
/// <reference path='MyRetail/AeviLockMessage.ts' />
/// <reference path='MyRetail/AeviContextMenu.ts' />

/**
 * MODAL
 */
/// <reference path='MyRetail/AeviModal/IAeviModal.ts' />
/// <reference path='MyRetail/AeviModal/AeviModal.ts' />
/// <reference path='MyRetail/AeviModal/AeviSimpleModal.ts' />

/**
 * EDITORS
 */
/// <reference path='MyRetail/AeviEditors/IAeviEditor.ts' />
/// <reference path='MyRetail/AeviEditors/AeviEditorFactory.ts' />
/// <reference path='MyRetail/AeviEditors/AeviEditor.ts' />
/// <reference path='MyRetail/AeviEditors/AeviFakeEditor.ts' />
/// <reference path='MyRetail/AeviEditors/AeviTextEditor.ts' />
/// <reference path='MyRetail/AeviEditors/AeviActionEditor.ts' />
/// <reference path='MyRetail/AeviEditors/AeviEnumEditor.ts' />
/// <reference path='MyRetail/AeviEditors/AeviImageEditor.ts' />
/// <reference path='MyRetail/AeviEditors/AeviNumberEditor.ts' />
/// <reference path='MyRetail/AeviEditors/AeviDatePickerLanguages.ts' />

/**
 * VALIDATORS
 */

/// <reference path='MyRetail/AeviValidators/AeviValidatorFactory.ts' />
/// <reference path='MyRetail/AeviValidators/IAeviValidator.ts' />
/// <reference path='MyRetail/AeviValidators/AeviValidator.ts' />
/// <reference path='MyRetail/AeviValidators/AeviNumberValidator.ts' />
/// <reference path='MyRetail/AeviValidators/AeviIntegerNumberValidator.ts' />
/// <reference path='MyRetail/AeviValidators/AeviRowNumberValidator.ts' />
/// <reference path='MyRetail/AeviValidators/AeviTextValidator.ts' />
/// <reference path='MyRetail/AeviValidators/AeviCurrencyValidator.ts' />
/// <reference path='MyRetail/AeviValidators/AeviServerGeneratedValidator.ts' />
/// <reference path='MyRetail/AeviValidators/AeviDateTimeValidator.ts' />
/// <reference path='MyRetail/AeviValidators/AeviMaxLengthValidator.ts' />
/// <reference path='MyRetail/AeviValidators/AeviEANValidator.ts' />
/// <reference path='MyRetail/AeviValidators/AeviEnumValidator.ts' />
/// <reference path='MyRetail/AeviValidators/AeviImageValidator.ts' />

/**
 * FILTERS
 */
/// <reference path='MyRetail/AeviFilter/IAeviFilterEditor.ts' />
/// <reference path='MyRetail/AeviFilter/AeviFilterFactory.ts' />
/// <reference path='MyRetail/AeviFilter/AeviFilter.ts' />
/// <reference path='MyRetail/AeviFilter/AeviFilterEditor.ts' />
/// <reference path='MyRetail/AeviFilter/AeviNumberFilterEditor.ts' />
/// <reference path='MyRetail/AeviFilter/AeviDateFilterEditor.ts' />
/// <reference path='MyRetail/AeviFilter/AeviEnumFilterEditor.ts' />

/**
 * HANDLERS
 */
/// <reference path='MyRetail/AeviHandlers/IAeviHandler.ts' />
/// <reference path='MyRetail/AeviHandlers/IAeviChangeType.ts' />
/// <reference path='MyRetail/AeviHandlers/AeviFilterHandler.ts' />
/// <reference path='MyRetail/AeviHandlers/AeviHelpHandler.ts' />
/// <reference path='MyRetail/AeviHandlers/AeviSearchHandler.ts' />
/// <reference path='MyRetail/AeviHandlers/AeviToolbarHandler.ts' />
/// <reference path='MyRetail/AeviHandlers/AeviGridHandler.ts' />
/// <reference path='MyRetail/AeviHandlers/AeviPagerHandler.ts' />
/// <reference path='MyRetail/AeviHandlers/AeviStatusBarHandler.ts' />
/// <reference path='MyRetail/AeviHandlers/AeviHeaderColumnHandler.ts' />
/// <reference path='MyRetail/AeviHandlers/AeviBodyRowHandler.ts' />
/// <reference path='MyRetail/AeviHandlers/AeviContextMenuHandler.ts' />
/// <reference path='MyRetail/AeviHandlers/AeviModalHandler.ts' />
/// <reference path='MyRetail/AeviHandlers/AeviFormHandler.ts' />


/**
 * OTHERS
 */
/// <reference path='MyRetail/AeviDownloader.ts' />

/**
 * PLUGINS
 */
/// <reference path='MyRetail/AeviPlugins/AeviPluginFactory.ts' />
/// <reference path='MyRetail/AeviPlugins/IAeviPlugin.ts' />
/// <reference path='MyRetail/AeviPlugins/AeviDynamicEnumPlugin/AeviDynamicEnumPlugin.ts' />
/// <reference path='MyRetail/AeviPlugins/AeviDynamicEnumPlugin/AeviDynamicEnumWrapper.ts' />
/// <reference path='MyRetail/AeviPlugins/AeviActionPricePlugin/AeviActionPricePlugin.ts' />

/**
 * PUBLIC INTERFACES
 */
/// <reference path='MyRetail/AeviPublicInterfaces/IAeviCellIndex.ts' />
/// <reference path='MyRetail/AeviPublicInterfaces/IAeviCellValue.ts' />
/// <reference path='MyRetail/AeviPublicInterfaces/IAeviImageData.ts' />
/// <reference path='MyRetail/AeviPublicInterfaces/IAeviMoveToProps.ts' />
/// <reference path='MyRetail/AeviPublicInterfaces/IAeviExtraColumn.ts' />
/// <reference path='MyRetail/AeviPublicInterfaces/IAeviColumnAction.ts' />
/// <reference path='MyRetail/AeviPublicInterfaces/IAeviBinaryData.ts' />

