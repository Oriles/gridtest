/// <reference path='../../references.ts' />
/// <reference path='../AeviHandlers/AeviFilterHandler.ts' />
/// <reference path='../AeviDate.ts' />
/// <reference path='AeviFilterEditor.ts' />
/// <reference path='AeviNumberFilterEditor.ts' />
/// <reference path='AeviEnumFilterEditor.ts' />
/// <reference path='AeviDateFilterEditor.ts' />

class AeviFilterFactory {
	AeviGrid: any;
	AeviDataRepository: any;
	filter: any;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.AeviDataRepository = this.AeviGrid.AeviDataRepository;
	}

	createFilter(filter: any) {
		var displayType = this.AeviGrid.AeviConsts.dataTypes[filter.DisplayType];

		switch (displayType) {
			case 'Text':
				this.filter = new AeviFilterEditor(filter);
				break;

			case 'Number':
			case 'IntegerNumber':
				this.filter = new AeviNumberFilterEditor(filter);
				break;

			case 'DateTime':
			case 'ShortDate':
			case 'ShortTime':
				this.filter = new AeviDateFilterEditor(filter, this.AeviGrid.AeviLocalization.getCulture(), displayType);
				break;

			case 'Enum':
				this.filter = new AeviEnumFilterEditor(filter);
				break;

			default:
				this.filter = null;
				break;
		}

	}

	getFilter() {
		return this.filter;
	}
	
}