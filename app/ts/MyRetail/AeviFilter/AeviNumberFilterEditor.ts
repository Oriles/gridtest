/// <reference path='../../references.ts' />
/// <reference path='IAeviFilterEditor.ts' />
/// <reference path='AeviFilterEditor.ts' />

class AeviNumberFilterEditor extends AeviFilterEditor implements IAeviFilterEditor {
	filter: any;

	constructor(filter: any) {
		super(filter);
	}

	render() {
		return '<span class="aeviToolbar__filterWrapper" style="width: ' + this.filter.MinWidthPixels + 'px"><input type="text" tabindex="-1" placeholder="' + this.filter.Caption + '" id="filter_' + '" data-column="' + this.filter.ColumnName + '" class="aeviToolbar__numberfilter"></span>';
	}
}
