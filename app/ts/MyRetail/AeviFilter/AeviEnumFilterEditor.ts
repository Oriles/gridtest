/// <reference path='../../references.ts' />
/// <reference path='IAeviFilterEditor.ts' />
/// <reference path='AeviFilterEditor.ts' />

class AeviEnumFilterEditor extends AeviFilterEditor implements IAeviFilterEditor {
	filter: any;

	constructor(filter: any) {
		super(filter);
	}

	render(): string {
		this.filter.Caption = (_.isNull(this.filter.Caption)) ? '' : '<span class="aeviToolbar__filterCaption">' + this.filter.Caption + ': </span>';
		var required: string = (this.filter.Required) ? '* ' : '';

		var html = [
			'<div class="dib">',
			this.filter.Caption,
			required,
			'<span class="aeviToolbarFilter aeviToolbarFilter_',
			this.filter.ColumnName,
			' relative aeviToolbar__filterWrapper" style="width: ',
			this.filter.MinWidthPixels,
			'px">',
			this.getRenderedElement(),
			'</span></div>'
		];

		return html.join('');
	}

	getRenderedElement(): string {
		var enumValues: any[] = this.filter.EnumValues;
		var className: string = 'aeviToolbar__enumfilter';
		var defaultValue: number = this.filter.DefaultValue;

		var code = '<select id="aeviEnumEditor" class="' + className + '">';

		for (var i = 0; i < enumValues.length; i++) {
			var enumValue = enumValues[i];
			code += '<option value="' + enumValue.DataValue + '"' + this.getDefaultValue(defaultValue, enumValue.DataValue) + '>' + enumValue.DisplayValue + '</option>';
		}

		code += '</select>';

		return code;
	}

	getDefaultValue(defaultValue: number, enumDataValue: number): string {
		if (defaultValue == enumDataValue)
			return ' selected';
		return '';
	}
}
