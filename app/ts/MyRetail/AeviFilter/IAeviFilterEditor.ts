/// <reference path='../../references.ts' />

interface IAeviFilterEditor {
	render(): string;
}
