/// <reference path='../../references.ts' />
/// <reference path='IAeviFilterEditor.ts' />
/// <reference path='AeviFilterEditor.ts' />
/// <reference path='../AeviDate.ts' />

class AeviDateFilterEditor extends AeviFilterEditor implements IAeviFilterEditor {
	filter: any;
	filterEl: JQuery;
	displayType: string;
	culture: string;
	AeviDate: any;

	constructor(filter: any, culture: string, displayType: string) {
		super(filter);
		this.culture = culture;
		this.displayType = displayType;
	}

	initDateFormat() {
		this.AeviDate = new AeviDate(this.culture, this.displayType);
	}

	render(): string {
		this.initDateFormat();

		setTimeout(() => {
			this.filterEl = $('.aeviToolbarFilter_' + this.filter.ColumnName);
			this.listener();
		});

		this.filter.Caption = (_.isNull(this.filter.Caption)) ? '' : '<span class="aeviToolbar__filterCaption">' + this.filter.Caption + ': </span>';
		var required: string = (this.filter.Required) ? '* ' : '';

        var aeviDate = new AeviDate(this.culture, this.filter.DisplayType, this.filter.DefaultValue);
        this.filter.DefaultValue = aeviDate.getString();

		return '<div class="dib">' + this.filter.Caption + required + '<span class="aeviToolbar__filterWrapper aeviToolbarFilter aeviToolbarFilter_' + this.filter.ColumnName + ' relative" style="width: ' + this.filter.MinWidthPixels + 'px"><input type="text" value="' + this.filter.DefaultValue +'" id="filter_' + this.filter.ColumnName + '" class="aeviToolbar__datefilter" value=""><span class="datepicker"></span></span></div>';
		
	}

	listener() {
		$(document).on(AeviConsts.clickEvent + ' ' + AeviConsts.touchEvent, this.filterEl.selector + ' .datepicker', () => {
			this.showCalendar();
		});

        var editor = this.filterEl.find('input, textarea, select');

		if (editor.length && window.navigator.msPointerEnabled) {
			editor[0].addEventListener("MSPointerDown", function() { this.showCalendar(); }, false);
		}
	}

	showCalendar() {
		var editor = this.filterEl.find('.aeviToolbar__datefilter');
		var culture = this.AeviDate.culture;

		$.datepicker.setDefaults($.datepicker.regional[culture]);
		
		/**
		 * FIX JQUERY-UI BUG WITH TODAY BUTTON
		 */
		var old_goToToday = $.datepicker._gotoToday;
		$.datepicker._gotoToday = function(id) {
			var currentDatepicker = this;

			old_goToToday.call(currentDatepicker, id);
			currentDatepicker._selectDate(id);

			setTimeout(() => {
				editor.datetimepicker('hide');
			}, 0);
		};

		editor.datetimepicker({ showTimepicker: false });
		editor.datetimepicker('show');
	}
}
