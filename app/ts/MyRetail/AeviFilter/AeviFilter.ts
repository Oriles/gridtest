/// <reference path='../../references.ts' />
/// <reference path='../AeviHandlers/AeviFilterHandler.ts' />
/// <reference path='../AeviDate.ts' />
/// <reference path='../AeviEditors/AeviEditor.ts' />

class AeviFilter {
	AeviGrid: any;
	AeviDate: any;
	filters: any[];
	handler: any;

	constructor(aeviGrid: any, filters: any[]) {
		this.AeviGrid = aeviGrid;
		this.initFilters(filters);
	}

	initFilters(filters): void {
		this.filters = [];

		this.AeviGrid.AeviFilterFactory = new AeviFilterFactory(this.AeviGrid);

		for (var i = 0; i < filters.length; i++) {
			var filter = filters[i];

			this.AeviGrid.AeviFilterFactory.createFilter(filter);
			
			var filterEditor = this.AeviGrid.AeviFilterFactory.getFilter();
			var editor = filterEditor.render();

			this.filters.push({
				id: filter.ColumnName,
				html: editor,
				value: null,
				displayType: this.AeviGrid.AeviConsts.dataTypes[filter.DisplayType],
				required: filter.Required
			});
		}

		this.handler = new AeviFilterHandler(this);
	}

	setFilterValue(columnName: string, val: string): void {
		for (var i = 0; i < this.filters.length; i++) {
			if (this.filters[i].id === columnName)
				this.filters[i].value = val;
		}
	}

	getFilters(): any[] {
		return this.filters;
	}

	getFilterValues(): any[] {
		var values = [];

		for (var i = 0; i < this.filters.length; i++) {
			var filter = this.filters[i];
			var value;

			switch (filter.displayType) {
				case 'DateTime':
				case 'ShortDate':
				case 'ShortTime':
					var dateVal = $('.aeviToolbarFilter_' + filter.id + ' input').val();
					this.AeviDate = new AeviDate(this.AeviGrid.AeviLocalization.getCulture(), filter.displayType, dateVal);
					value = this.AeviDate.getIsoString();
					break;

				case 'Enum':
					value = $('.aeviToolbarFilter_' + filter.id + ' select').val();
					break;

				default:
					value = filter.value;
					break;
			}

			if (value === 'null')
				value = null;

			values.push({
				ColumnName: filter.id,
				Value: value,
			});
		}

		return values;
	}

	getRequiredFiltersId(): any[] {
		var filters = [];

		for (var i = 0; i < this.filters.length; i++) {
			var filter = this.filters[i];

			if (filter.required)
				filters.push(filter.id);
		}

		return filters;
	}

	isFiltersFilled(): boolean {
		var requiredFiltersId = this.getRequiredFiltersId();
		var filterValues = this.getFilterValues();

		for (var i = 0; i < filterValues.length; i++) {
			for (var j = 0; j < requiredFiltersId.length; j++) {
				if (filterValues[i].ColumnName === requiredFiltersId[j]) {
					var filterVal = filterValues[i].Value;

					if (aeviIsUndefinedOrNull(filterVal) || filterVal === 'null' || filterVal === '' || filterVal === ' ' || _.isEmpty(filterVal))
						return false;
				}
			}
		}

		return true;
	}

	getJSONFilterValues(): string {
		return JSON.stringify(this.getFilterValues());
	}
}
