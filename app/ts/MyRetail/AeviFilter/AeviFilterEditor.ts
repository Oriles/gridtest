/// <reference path='../../references.ts' />
/// <reference path='IAeviFilterEditor.ts' />

class AeviFilterEditor implements IAeviFilterEditor {
	filter: any;

	constructor(filter: any) {
		this.filter = filter;
	}

	render() {
		this.filter.Caption = (_.isNull(this.filter.Caption)) ? '' : '<span class="aeviToolbar__filterCaption">' + this.filter.Caption + ': </span>';		
		var required = (this.filter.Required) ? '* ' : '';
		return '<div class="dib">' + this.filter.Caption + required + '<span class="aeviToolbar__filterWrapper" style="width: ' + this.filter.MinWidthPixels + 'px"><input type="text" tabindex="-1" id="filter_' + this.filter.ColumnName + '" data-column="' + this.filter.ColumnName + '" class="aeviToolbar__textfilter"></span></div>';
	}
}
