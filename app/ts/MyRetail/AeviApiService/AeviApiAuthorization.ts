/// <reference path='../../references.ts' />

class AeviApiAuthorization {
    static requestVerificationTokenName = '__RequestVerificationToken';
    static aspNetSessionIdName = 'ASP.NET_SessionId';
    static aspxAuthName = '.ASPXAUTH';

    requestVerificationToken: string;
    aspNetSessionId: string;
    aspxAuth: string;
    isCookiesNull: boolean;

    constructor() {
        this.getCookies();
        this.nullCookies();
        this.listener();
    }

    getCookies() {
        this.requestVerificationToken = $.cookie(AeviApiAuthorization.requestVerificationTokenName);
        this.aspNetSessionId = $.cookie(AeviApiAuthorization.aspNetSessionIdName);
        this.aspxAuth = $.cookie(AeviApiAuthorization.aspxAuthName);
    }

    setCookies() {
        $.cookie(AeviApiAuthorization.requestVerificationTokenName, this.requestVerificationToken);
        $.cookie(AeviApiAuthorization.aspNetSessionIdName, this.aspNetSessionId);
        $.cookie(AeviApiAuthorization.aspxAuthName, this.aspxAuth);
        this.isCookiesNull = false;
    }

    nullCookies() {
        $.cookie(AeviApiAuthorization.requestVerificationTokenName, null);
        $.cookie(AeviApiAuthorization.aspNetSessionIdName, null);
        $.cookie(AeviApiAuthorization.aspxAuthName, null);
        this.isCookiesNull = true;
    }

    listener() {
        $(document).ajaxSend(() => {
            this.nullCookies();
        }).ajaxStop(() => {
            this.setCookies();
        });
    }
}
