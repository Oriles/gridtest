/// <reference path='../../references.ts' />

interface IAeviApiService {
	apiAddress: string;
	username: string;
	password: string;
	slot: number;
	entity: string;
	lang: string;
	entityType: number;
	activeAjaxConnection: number;
	token: string;
	isDataChanged: boolean;
	isCellChanged: boolean;
    caption: string;

    getAccessToken(): any;
    getTableDescription(): JQueryXHR;
    getFormDescription(): JQueryXHR;
    getTableData(query: string, force: boolean): JQueryXHR;
    getTableRow(guid: string): JQueryXHR;
    putData(data: any, async: boolean): JQueryXHR;
    deleteData(guids: string[]);
    commit(): JQueryXHR;
    getDataEntity(): JQueryXHR;
    //getComposedUrl(url: any[]): string;
    //getComposedParameter(parameter): string;
    executeAction(actionData: IAeviEntityActionDefinition, async: boolean): JQueryXHR;
    getOrSetTableSettings(requestType: string, tableSettings?: string): JQueryXHR;
	setDataChanged(val: boolean): void;
	setCellChanged(val: boolean): void;
}
