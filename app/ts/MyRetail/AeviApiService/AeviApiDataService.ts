/// <reference path='../../references.ts' />

class AeviApiDataService implements IAeviApiService{
	AeviGrid: any;
	apiAddress: string;
	username: string;
	password: string;
	slot: number;
	entity: string;
	lang: string;
	entityType: number;
	async: boolean;
	caption: string;
	activeAjaxConnection: number;
	token: string;
	isDataChanged: boolean;
	isCellChanged: boolean;
    AeviApiAuthorization: any;    

    static SAVE_URL_LENGTH = 1800;

    static API = 'Call?';
    static TOKEN = 'token';
    static CONTROLLER = 'Controller';
    static ACTION = 'Action';
    static ID = 'Id';
    static CAPTION = 'Caption';
    static LANGUAGE = 'Language';
    static JSONFILTER = 'JsonFilter';
    static FORCE = 'Force';
    static ENTITYTYPE = 'EntityType';
    static JSONSORTINFOS = 'JsonSortInfos';
    static OUTPUTTYPE = 'OutputType';
    static ACCESSTOKEN = 'access_token';
    static ITEMKEY = 'ItemKey';
    static DATAMODE = 'DataMode';
    
	constructor(aeviGrid: any, apiAddress: string, username: string, password: string, slot: number, entity: string, lang: string, token: string, entityType: number) {
        //this.AeviApiAuthorization = new AeviApiAuthorization();

        this.AeviGrid = aeviGrid;
        this.apiAddress = (apiAddress[apiAddress.length - 1] === '/') ? apiAddress.substring(0, apiAddress.length - 1) : apiAddress;
        this.username = username;
        this.password = password;
        this.slot = slot;
        this.entity = entity;
        this.lang = lang;
        this.entityType = entityType;
        this.async = true;
        this.caption = null;
        this.activeAjaxConnection = 0;

        this.setDataChanged(false);
        this.setCellChanged(false);

        this.token = null;

        if (!aeviIsUndefinedOrNull(token)) {
            this.token = token;
        }
	}

    getAccessToken() {
        return $.ajax({
			type: 'POST',
			async: this.async,
			url: AeviApiDataService.getComposedUrl([
				this.apiAddress,
                AeviApiDataService.TOKEN
			]),
			data: 'username=' + this.username + '&password=' + this.password + '&grant_type=password&SlotID=' + this.slot + '&languageIsoCode=cs'
		}).done((response) => {
			this.token = AeviApiDataService.getTokenWithBearer(response.access_token);
		});
	}

	static getTokenWithBearer(token: string): string {
		return 'Bearer ' + token;
	}

	getTokenWithoutBearer() {
		if (_.isUndefined(this.token)) {
			console.log('Token is undefined');
			return null;
		}

		return this.token.replace('Bearer', '');
	}

	getTableDescription(): JQueryXHR {
        return $.ajax({
			headers: { 'Authorization': this.token },
			type: 'GET',
			url: AeviApiDataService.getComposedUrl([
				this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableDescription' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang },
                    { name: AeviApiDataService.ENTITYTYPE, value: this.entityType }
                ]
			]),
			contentType: 'application/json; charset=utf-8'
		});
	}

    getFormDescription(): JQueryXHR {
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'GET',
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableDescription' },
                    { name: AeviApiDataService.ACTION, value: 'GetFormDescription' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang },
                    { name: AeviApiDataService.ENTITYTYPE, value: this.entityType }
                ]
            ]),
            contentType: 'application/json; charset=utf-8'
        });
    }

	getTableData(query: string, force: boolean): JQueryXHR {
		return $.ajax({
            headers: {'Authorization': this.token },
			type: 'GET',
			url: AeviApiDataService.getComposedUrl([
				this.apiAddress,
				[
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableData' },
					{ name: AeviApiDataService.ID, value: this.entity },
					{ name: AeviApiDataService.LANGUAGE, value: this.lang },
					{ name: AeviApiDataService.JSONFILTER, value: query },
					{ name: AeviApiDataService.FORCE, value: force },
					{ name: AeviApiDataService.ENTITYTYPE, value: this.entityType }
				]
			]),
			contentType: 'application/json; charset=utf-8'
		});
	}

	getTableRow(guid: string): JQueryXHR {
		return $.ajax({
			headers: { 'Authorization': this.token },
			type: 'GET',
			async: false,
			url: AeviApiDataService.getComposedUrl([
				this.apiAddress,
				[
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableData' },
					{ name: AeviApiDataService.ID, value: this.entity },
					{ name: AeviApiDataService.LANGUAGE, value: this.lang },
					{ name: AeviApiDataService.ITEMKEY, value: guid }
				]
			]),
			contentType: 'application/json; charset=utf-8'
		});
	}

	putData(data: any, async: boolean): JQueryXHR {
		this.setDataChanged(true);
		var asyncType = (aeviIsUndefinedOrNull(async)) ? this.async : async;

		return $.ajax({
			headers: { 'Authorization': this.token },
			async: asyncType,
			type: 'PUT',
			contentType: 'application/json',
			url: AeviApiDataService.getComposedUrl([
				this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableData' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang },
                    { name: AeviApiDataService.DATAMODE, value: this.AeviGrid.mode.clipboard }
                ]
			]),
			data: JSON.stringify(data)
		});
	}

	deleteData(guids: string[]) {
		this.setDataChanged(true);

		var deleteUrl = AeviApiDataService.getComposedUrl([
			this.apiAddress,
            [
                { name: AeviApiDataService.API },
                { name: AeviApiDataService.CONTROLLER, value: 'TableData' },
                { name: AeviApiDataService.ID, value: this.entity },
                { name: AeviApiDataService.LANGUAGE, value: this.lang + '&' },
            ]
		]);

		var batch: string[] = [];
		var query: string = deleteUrl;

		for (var i = 0; i < guids.length; i++) {
			guids[i] = 'ids=' + guids[i] + '&';

			if (query.length < AeviApiDataService.SAVE_URL_LENGTH) {
				query += guids[i];
			} else {
				batch.push(query);
				query = deleteUrl + guids[i];
			}
		}

		batch.push(query);

		if (batch[batch.length - 1] === deleteUrl) {
			batch.pop();
		}

		var ajaxRequests: JQueryAjaxSettings[] = [];

		// REMOVE LAST '&' CHAR AND CALL API
		for (var j = 0; j < batch.length; j++) {
			batch[j] = batch[j].substring(0, batch[j].length - 1);

			var request = $.ajax({
				headers: { 'Authorization': this.token },
				type: 'DELETE',
				async: this.async,
				url: batch[j],
				contentType: 'application/json'
			});

			ajaxRequests.push(request);
		}

		return ajaxRequests[0];
	}

	commit(): JQueryXHR {
		return $.ajax({
			headers: { 'Authorization': this.token },
			async: this.async,
			type: 'POST',
			url: AeviApiDataService.getComposedUrl([
				this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableData' },
                    { name: AeviApiDataService.ACTION, value: 'Commit' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang }
                ]
			]),
            data: { '__RequestVerificationToken': this.AeviGrid.antiForgeryToken }
		});
	}

	getDataEntity(): JQueryXHR {
        return $.ajax({
			headers: { 'Authorization': this.token },
			type: 'GET',
			async: this.async,
			url: AeviApiDataService.getComposedUrl([
				this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'DataEntity' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang },
                    { name: AeviApiDataService.ENTITYTYPE, value: this.entityType }
                ]
			]),
			contentType: 'application/json; charset=utf-8'
		});
	}

	static getComposedUrl(url: any[]): string {
		for (var i = 0; i < url.length; i++) {
			if (_.isArray(url[i])) {
				var params = [];
				var parameters = url[i];
				url.splice(i, 1);

				for (var j = 0; j < parameters.length; j++) {
					var parameter = parameters[j];
					parameter = AeviApiDataService.getComposedParameter(parameter);
					params.push(parameter);
				}

				var stringParameters = params.join('&');
				stringParameters = stringParameters.replace('&', '');
				url.push(stringParameters);
			}
		}

		return url.join('/');
	}

	static getComposedParameter(parameter): string {
		if (_.isUndefined(parameter.value))
			return parameter.name;
		return parameter.name + '=' + parameter.value;
	}

	getDownloadFileUrl(JSONFilter, JSONSortInfos, fileType) {
		return AeviApiDataService.getComposedUrl([
			this.apiAddress,
            [
                { name: 'GetFile?' },
                { name: AeviApiDataService.ID, value: this.entity },
                { name: AeviApiDataService.CAPTION, value: this.caption },
                { name: AeviApiDataService.LANGUAGE, value: this.lang },
                { name: AeviApiDataService.JSONFILTER, value: JSONFilter },
                { name: AeviApiDataService.FORCE, value: false },
                { name: AeviApiDataService.ENTITYTYPE, value: this.entityType },
                { name: AeviApiDataService.JSONSORTINFOS, value: JSONSortInfos },
                { name: AeviApiDataService.OUTPUTTYPE, value: fileType },
                { name: AeviApiDataService.ACCESSTOKEN, value: this.getTokenWithoutBearer() }
            ]
		]);
	}

    getExportFileUrl(fileType) {
        var JSONFilter = '';

        if (!aeviIsUndefinedOrNull(this.AeviGrid.AeviFilter)) {
            JSONFilter = encodeURIComponent(this.AeviGrid.AeviFilter.getJSONFilterValues());
        }

        return AeviApiDataService.getComposedUrl([
            this.apiAddress,
            [
                { name: 'ExportFile' + fileType + '?' },
                { name: AeviApiDataService.ID, value: this.entity },
                { name: AeviApiDataService.CAPTION, value: this.caption },
                { name: AeviApiDataService.LANGUAGE, value: this.lang },
                { name: AeviApiDataService.JSONFILTER, value: JSONFilter },
                { name: AeviApiDataService.ENTITYTYPE, value: this.entityType },
                { name: AeviApiDataService.ACCESSTOKEN, value: this.getTokenWithoutBearer() }
            ]
        ]);
    }

    executeAction(actionData: IAeviEntityActionDefinition, async: boolean): JQueryXHR {
		var JSONData = JSON.stringify(actionData);
		var asyncType = (aeviIsUndefinedOrNull(async)) ? this.async : async;

        return $.ajax({
			headers: { 'Authorization': this.token },
			type: 'POST',
			async: asyncType,
			url: AeviApiDataService.getComposedUrl([
				this.apiAddress,
				[
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableData' },
                    { name: AeviApiDataService.ACTION, value: 'ExecuteAction' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang },
                    { name: AeviApiDataService.ENTITYTYPE, value: this.entityType }
                ]
			]),
            data: { __RequestVerificationToken: this.AeviGrid.antiForgeryToken, jsonRequest: JSONData },
            contentType: "application/x-www-form-urlencoded"
		});
	}

    getRoles(): JQueryXHR {
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'GET',
            async: this.async,
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                'api',
                'Roles',
                this.lang,
                this.entity,
                this.entityType
            ]),
            contentType: 'application/json; charset=utf-8'
        });
    }

	setTableSettings(tableSettings) {
		return this.getOrSetTableSettings('PUT', JSON.stringify(tableSettings));
	}

	getTableSettings() {
		return this.getOrSetTableSettings('GET');
	}

	getOrSetTableSettings(requestType: string, tableSettings?: string): JQueryXHR {
		return $.ajax({
			headers: { 'Authorization': this.token },
			type: requestType,
			async: this.async,
			url: AeviApiDataService.getComposedUrl([
				this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableSettings' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang }
                ]
			]),
			data: tableSettings,
			contentType: "application/json; charset=utf-8",
		});
	}

	setDataChanged(val: boolean) {
		//console.log('AeviApiService.setDataChanged() with value: ' + val);
		//console.log("caller is " + arguments.callee.caller);
		this.isDataChanged = val;
	}

	setCellChanged(val: boolean) {
		//console.log('AeviApiService.setCellChanged() with value: ' + val);
		//console.log("caller is " + arguments.callee.caller);
		this.isCellChanged = val;
	}

    importFile(data: IAeviBinaryData, fileType: string): JQueryXHR {
        var importData: any = {};
        importData['__RequestVerificationToken'] = this.AeviGrid.antiForgeryToken;
        importData['BinaryData'] = data;
        importData[AeviApiDataService.ID] = this.entity;
        importData[AeviApiDataService.LANGUAGE] = this.lang;
        importData[AeviApiDataService.ENTITYTYPE] = this.entityType;

        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'POST',
            async: this.async,
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: 'ImportFile' + fileType }
                ]
            ]),
            data: importData
        });
    }

    setDataEntityBlockStatus(): JQueryXHR {
        var importData: any = {};
        importData['__RequestVerificationToken'] = this.AeviGrid.antiForgeryToken;
        importData[AeviApiDataService.ID] = this.entity;

        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'POST',
            async: this.async,
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: 'SetDataEntityBlockStatus' }
                ]
            ]),
            data: importData
        });
    }

    getDataEntityStatus(): JQueryXHR {
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'GET',
            async: this.async,
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: 'GetDataEntityStatus?' },
                    { name: AeviApiDataService.ID, value: this.entity }
                ]
            ])
        });
    }
}
