/// <reference path='../references.ts' />
/// <reference path='AeviHandlers/AeviStatusBarHandler.ts' />

class AeviStatusBar {
	AeviGrid: any;
    statusBarItem: HTMLElement;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.render();
	}

	render(): IAeviHandler {
		var statusBarClass: string = this.AeviGrid.AeviConsts.statusBarSelector.replace('.', '');
		var statusBarItemClass: string = this.AeviGrid.AeviConsts.statusBarItemSelector.replace('.', '');
		var statusBarItemHelperClass: string = this.AeviGrid.AeviConsts.aeviStatusBarHelperSelector.replace('.', '');
		
		if(!$(this.AeviGrid.AeviConsts.statusBarSelector).length) {
			this.AeviGrid.aeviWrapper.append(
				'<div class="' + statusBarClass + '">' +
					'<p class="' + statusBarItemClass + '"></p>' + 
					'<p class="' + statusBarItemHelperClass + '" title="' + this.AeviGrid.AeviLocalization.translate('tooltip_help') + '"></p>' +  
				'</div>'
			);

            this.statusBarItem = this.AeviGrid.AeviDOM.getEl(this.AeviGrid.AeviConsts.statusBarItemSelector);
		}

		return new AeviStatusBarHandler(this.AeviGrid);
	}

	show(message: string, iconType: string) {
		this.clear();

		if(!_.isNull(message) && message !== 'null') {
			if(_.isUndefined(iconType))
				iconType = 'error';

            this.statusBarItem.classList.add(iconType);
            this.statusBarItem.innerHTML = message;
		}
	}

	error(message: string): void {
		if(aeviIsUndefinedOrNull(message) || message === '')
			message = null;

		this.show(message, 'error');
	}

	info(message: string): void {
		if(aeviIsUndefinedOrNull(message) || message === '')
			message = null;

		this.show(message, 'info');
	}

	success(message: string): void {
		if(aeviIsUndefinedOrNull(message) || message === '')
			message = null;

		this.show(message, 'success');
	}

    clear(): void {
        this.statusBarItem.classList.remove('error');
        this.statusBarItem.classList.remove('info');
        this.statusBarItem.classList.remove('success');
        this.statusBarItem.innerHTML = '';
    }

    renderHelpIcon(): void {
        var aeviStatusBarQuestSelectorClass: string = this.AeviGrid.AeviConsts.aeviStatusBarQuestSelector.makeClassFromSelector();
        $(this.AeviGrid.AeviConsts.aeviStatusBarHelperSelector).append('<i class="' + aeviStatusBarQuestSelectorClass + '"></i>');
    }

    /**
     * function is not used
     */
    renderInfoIcon(): void {
        var aeviStatusBarInfoSelectorClass: string = this.AeviGrid.AeviConsts.aeviStatusBarInfoSelector.makeClassFromSelector();
        $(this.AeviGrid.AeviConsts.aeviStatusBarHelperSelector).append('<i class="' + aeviStatusBarInfoSelectorClass + '"></i>');
    }
}
