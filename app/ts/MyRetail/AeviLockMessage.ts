/// <reference path='../references.ts' />
/// <reference path='AeviModal/AeviModal.ts' />

class AeviLockMessage {
	AeviGrid: any;
	lockMessageShowed: boolean;
	isModalOpened: boolean;
	maximumViews: number;
	views: number;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.lockMessageShowed = false;
		this.isModalOpened = false;
		this.maximumViews = 3;
		this.views = 0;
	}

	show() {
		if (this.views >= this.maximumViews)
			return;

		var modal = new AeviModal(this.AeviGrid);

		modal.setContent({
			title: this.AeviGrid.AeviLocalization.translate('protected_mode_title'),
			text: '<p>' + this.AeviGrid.AeviLocalization.translate('protected_mode') + '</p>',
			buttons: [
				'aeviProtectedUnlock',
				'simpleModalClose'
			]
		});

		modal.show();
		modal.focus('last');

		this.increaseViews();
	}

	isLockMessageShowed() {
		if (aeviIsUndefinedOrNull(this.lockMessageShowed) || !this.lockMessageShowed)
			return false;
		else
			return true;
	}

	increaseViews() {
		this.views++;
	}
}
