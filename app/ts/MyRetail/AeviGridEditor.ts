/// <reference path='../references.ts' />
/// <reference path='AeviModal/AeviModal.ts' />
/// <reference path='AeviTableHeader/AeviHeaderRow.ts' />
/// <reference path='AeviTableBody/AeviBodyRow.ts' />

class AeviGridEditor {
	AeviGrid: any;
	AeviDataService: any;
	AeviDataRepository: any;
	AeviApiService: any;
	sourceElementId: string;
	
	table: JQuery;
	headerRow: any;

	renderedRowsLength: number;
	actualRefreshRange: any;

	constructor(aeviGrid: any, aeviDataService: any, sourceElementId: string) {
		this.AeviGrid = aeviGrid;
		this.AeviDataService = aeviDataService;
		this.AeviDataRepository = this.AeviGrid.AeviDataRepository;
		this.AeviApiService = this.AeviDataService.AeviApiService;
		this.sourceElementId = sourceElementId;
	}

	renderTableHead() {
		this.table = $(this.AeviGrid.AeviConsts.tableSelector);

        if(this.AeviDataRepository.AeviTableDescription.isReport()) {
            this.table[0].classList.add('isReport');
        }

		this.headerRow = new AeviHeaderRow(this.AeviGrid.AeviConsts);
		this.headerRow.render(this.AeviDataRepository.AeviTableDescription.getColumns());
	}

	renderTableBody(data) {
		var html = [];
		var from = 0;
		var to = data.length;

		var tbodySelector = this.table.selector + ' tbody';

		if (!this.AeviGrid.AeviDOM.length(tbodySelector)) {
			this.table.append('<tbody></tbody>');
		}

		var AeviDataRepository = this.AeviGrid.AeviDataService.AeviDataRepository;
		var bodyRow = new AeviBodyRow(AeviDataRepository, this.AeviGrid);

		for (var i = 0; i < to; i++)
			html.push(bodyRow.render(data[i], i));

		this.table.find('tbody').append(html.join(''));

		this.renderedRowsLength = to;

		this.refreshData({
			From: from,
			To: to
		});
	}

	deleteTableBody() {
		this.table.find('tbody tr').remove();
	}

	refreshData(range?: any) {
		if (aeviIsUndefinedOrNull(range))
			range = this.actualRefreshRange;

		this.actualRefreshRange = range;

		var refreshData = this.AeviDataRepository.AeviTableData.getRecords(range.From, range.To);

		var table = this.AeviGrid.AeviDOM.getPureTable();
		var body = table.getElementsByTagName('tbody')[0];
		var rows = body.getElementsByTagName('tr');

		var bodyRow = new AeviBodyRow(this.AeviDataRepository, this.AeviGrid);

		var len: number = refreshData.length;
		len = this.AeviGrid.AeviConsts.pager.visibleRows;

		for (var i = 0; i < len; i++) {
			bodyRow.refresh(rows[i], refreshData[i]);
        }

		if (!_.isUndefined(this.AeviGrid.AeviPager))
			this.AeviGrid.AeviPager.setVisibleContentInfo();

		// target previous selected cell
		var selectedCells = this.AeviDataRepository.getSelectedCellsPositions();

        if (selectedCells) {
			if (selectedCells.cells.length < 2 && selectedCells.rows.length < 2) {
				var cell = this.AeviGrid.AeviDOM.getCell(selectedCells.cells[0], selectedCells.rows[0]);

                if (cell.length) {
					//this.AeviGrid.AeviGridHandler.cellChange(cell);
                }
			}
		}

        setTimeout(() => {
			for (i = len + 1; i < rows.length; i++)
				rows[i].classList.add('hidden');
		});

		this.AeviGrid.checkRowTinge();
		this.AeviGrid.setSizes();
	}

	refreshRow(properties: IAeviRecordIdentification): void {
		var rowIndex: number = null;

		if (!_.isUndefined(properties.rowIndex))
			rowIndex = properties.rowIndex;

		if (!_.isUndefined(properties.guid))
			rowIndex = this.AeviDataRepository.AeviTableData.getRecordIndexByGuid(properties.guid);

		var locAeviBodyRow = new AeviBodyRow(this.AeviDataRepository, this.AeviGrid);

		var row: HTMLTableRowElement = this.AeviGrid.AeviDOM.getPureRow(rowIndex);

		var refreshData: any[] = this.AeviDataRepository.AeviTableData.getRecords(rowIndex, rowIndex + 1);

		locAeviBodyRow.refresh(row, refreshData[0]);

		this.AeviGrid.setSizes();
	}

	renderNewRow() {
		var addRow: number = 1;
		if (this.AeviDataRepository.isMaxRowCountExceeded(addRow)) {
			this.AeviGrid.showMaxRowCountMessage();
			return;
		}

        var selectedRowIndex = this.AeviGrid.AeviDOM.getSelectedRowIndex();

        if (_.isNull(selectedRowIndex)) {
            var lastSelectedCell = this.AeviGrid.AeviGridHandler.lastSelectedCell[0];
            var cellIndexes: IAeviCellIndex = this.AeviGrid.AeviDOM.getCellIndexes(lastSelectedCell);
            selectedRowIndex = cellIndexes.rowIndex;
        }

        if (aeviIsUndefinedOrNull(selectedRowIndex)) {
			this.AeviGrid.print('AeviGridEditor.renderNewRow(), variable "rowIndex" is undefined or null.');
			return;
		}

		var properties = {
			Index: selectedRowIndex,
			Status: this.AeviGrid.AeviConsts.statuses.newrow + ' ' + this.AeviGrid.AeviConsts.statuses.sortedRow
		};

		this.AeviDataRepository.AeviTableData.insertRecord(properties);
		
		this.refreshData();

		this.AeviGrid.setSizes();

		var cellToBeFocused: JQuery = this.AeviGrid.AeviDOM.getCell(1, selectedRowIndex + 1);
        setTimeout(() => {
            this.AeviGrid.AeviGridHandler.cellChange(cellToBeFocused);
        });
	}

	deleteRow(): void {
		var guids: string[] = this.getDeletedRowGuids();
		var rowNumberIndex: number = this.AeviGrid.AeviDataRepository.AeviTableDescription.getRowNumberIndex();

		if (guids.length > 0) {
			var firstNotDeletedRecordIndex = this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableData.getRecord(guids[0])[rowNumberIndex] - 1;

			if (firstNotDeletedRecordIndex < 0)
				firstNotDeletedRecordIndex = 0;

			this.AeviGrid.AeviDataService.deleteData(guids.slice(), firstNotDeletedRecordIndex);
		}

		this.AeviGrid.setSizes();
	}

	getDeletedRowGuids(): string[] {
		var guids: string[] = [];		
		var selectedRowsIndexes: number[] = this.AeviDataRepository.AeviTableData.getSelectedRows();
		var selectedRowsLength: number = selectedRowsIndexes.length;

		for (var i = 0; i < selectedRowsLength; i++) {
			guids.push(this.AeviDataRepository.AeviTableData.getGuidByRowIndex(selectedRowsIndexes[i]));
		}

		return guids;
	}

	showBorder(fromCellIndex: IAeviCellIndex, toCellIndex: IAeviCellIndex): void {
		var selectedIndexes: number[] = [];

		var selectedCells = document.querySelectorAll('.aeviCell.selected');
		var selectedCellsLength = selectedCells.length;

		for (var i = 0; i < selectedCellsLength; i++) {
			var cell = <HTMLElement>selectedCells[i];
			var cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes(cell);

			selectedIndexes.push(cellIndexes.cellIndex);

			if (fromCellIndex.rowIndex == cellIndexes.rowId)
				cell.classList.add('selectedTop');

			if (toCellIndex.rowIndex == cellIndexes.rowId)
				cell.classList.add('selectedBottom');
		}

		var minCellIndex = _.min(selectedIndexes);
		var maxCellIndex = _.max(selectedIndexes);

		for (var i = 0; i < selectedCellsLength; i++) {
			var cell = <HTMLElement>selectedCells[i];
			var cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes(cell);

			if (cellIndexes.cellId === minCellIndex)
				cell.classList.add('selectedLeft');

			if (cellIndexes.cellId === maxCellIndex)
				cell.classList.add('selectedRight');
		}
	}
}
