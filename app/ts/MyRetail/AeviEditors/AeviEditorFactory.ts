/// <reference path='../../references.ts' />
/// <reference path='AeviTextEditor.ts' />
/// <reference path='AeviNumberEditor.ts' />
/// <reference path='AeviCurrencyEditor.ts' />
/// <reference path='AeviActionEditor.ts' />
/// <reference path='AeviImageEditor.ts' />
/// <reference path='AeviDateEditor.ts' />
/// <reference path='AeviEnumEditor.ts' />

class AeviEditorFactory {
	AeviGrid: any;
	AeviDataRepository: any;
	AeviBubble: any;
	editor: any;

	constructor(aeviGrid: any) {
        this.AeviGrid = aeviGrid;
		this.AeviDataRepository = this.AeviGrid.AeviDataRepository;
	}

	createEditor($cell: JQuery): void {
        var indexes: IAeviCellIndex = this.AeviGrid.AeviDOM.getCellIndexes($cell);

		var column = this.AeviDataRepository.AeviTableDescription.getColumnHeader(indexes.cellIndex);

        if (aeviIsUndefinedOrNull(column)) {
			this.AeviGrid.print('AeviEditor.create(), variable "column" is undefined or null.');
			this.focus($cell);
			return;
		}

		var referenceSettingsColumn = this.AeviDataRepository.getReferenceSettings(indexes.rowIndex, indexes.cellIndex);

        if (!_.isNull(referenceSettingsColumn)) {
			column = referenceSettingsColumn;
		}

		/**
		 * If column has EntityActionDefinition create ActionEditor
		 */
		if (!aeviIsUndefinedOrNull(column.EntityActionDefinition)) {
            this.editor = new AeviActionEditor(this.AeviGrid, $cell, column.EntityActionDefinition);
			return;
		}

		var displayType = this.AeviGrid.AeviConsts.dataTypes[column.DisplayType];

        if(displayType === 'Image') {
            this.editor = new AeviImageEditor(this.AeviGrid, $cell);
            return;
        }

        if (column.ReadOnly || this.AeviDataRepository.AeviTableDescription.isReport() || this.AeviGrid.AeviGridLocker.isLocked) {
            this.editor = null;
            this.focus($cell);
            return;
        }

		switch (displayType) {
			case 'Image':
				this.editor = new AeviImageEditor(this.AeviGrid, $cell);
				break;

			case 'Text':
				this.editor = new AeviTextEditor(this.AeviGrid, $cell);
				break;

			case 'Number':
			case 'IntegerNumber':
				this.editor = new AeviNumberEditor(this.AeviGrid, $cell, displayType);
				break;

			case 'Currency':
				this.editor = new AeviCurrencyEditor(this.AeviGrid, $cell);
				break;

			case 'DateTime':
			case 'ShortDate':
			case 'ShortTime':
				this.editor = new AeviDateEditor(this.AeviGrid, $cell, displayType);
				break;

			case 'Enum':
				this.editor = new AeviEnumEditor(this.AeviGrid, $cell, column);
				break;

			case 'Boolean':
			case 'Hyperlink':
			case 'RegularExpression':
				this.editor = null;
				this.AeviGrid.print('AeviEditorFactory.createEditor(), "' + displayType + '" editor is not supported.');
				break;

			default:
				this.editor = null;
				break;	
		}
	}

	getEditor() {
		return this.editor;
	}

	isEditorExists(): boolean {
		return !aeviIsUndefinedOrNull(this.editor);
	}

	focus($cell: JQuery): void {
		$cell.focus();
	}
}
