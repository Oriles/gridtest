/// <reference path='../../references.ts' />
/// <reference path='IAeviEditor.ts' />
/// <reference path='AeviEditor.ts' />
/// <reference path='../AeviImage.ts' />

class AeviImageEditor extends AeviEditor implements IAeviEditor {
    defaultValue: IAeviImageData;
    imageEditorLink: HTMLElement;

	constructor(aeviGrid, cell) {
		super(aeviGrid, cell);
		this.editorId = '.aeviCellImage';
		this.isDataValueChanged = false;
        this.setState('initial');
	}

    setReady(): void {
        this.render();
    }

	render() {
        this.renderHiddenInput();
        this.editorEl = this.$cell.find('div > input');
        this.cell.classList.add('hasEditor');
        this.listener();
        this.setState('render');
	}

    renderFormEditor(column: IAeviExtendedItemDescription, fakeIndexes: IAeviCellIndex): string {
        var image: IAeviImageData = column.Value;
        var deleteBtnClass: string = '';
        var editBtnClass: string = '';
        var uploadBtnClass: string = ' hidden ';
        var imgClass: string = '';

        if(_.isNull(image.Value)) {
            //image.Value = AeviGlobal.getEmptyImageBase64();
            deleteBtnClass = ' hidden ';
            editBtnClass = ' hidden ';
            uploadBtnClass = '';
            imgClass = ' hidden ';
        }

        this.cellIndexes = fakeIndexes;
        this.columnName = column.ColumnName;
        this.defaultValue = _.clone(image);

        if (this.AeviGrid.mode.clipboard === 1) {
            return '<input class="form-control" disabled type="text" readonly value="' + AeviImage.getLocalizedText('image_blocked') + '">';
        }

        var HTML: string = '';
        var buttonTemplate = AeviImageEditor.getButtonTemplate();
        var imgTemplate = AeviImageEditor.getImgTemplate();

        HTML += imgTemplate({ imgClass: imgClass, maxHeight: column.MaxHeight - 40, imgFormat: AeviImage.getImageFormat(), imgValue: image.Value });

        if (!column.IsReadOnly) {
            HTML += buttonTemplate({ buttonClass: 'aeviButton--secondary' + uploadBtnClass, action: 'upload', translate: this.AeviGrid.AeviLocalization.translate('image_missing') });
            HTML += buttonTemplate({ buttonClass: 'aeviButton--black' + deleteBtnClass, action: 'delete', translate: this.AeviGrid.AeviLocalization.translate('image_delete') });
            HTML += buttonTemplate({ buttonClass: editBtnClass, action: 'edit', translate: this.AeviGrid.AeviLocalization.translate('image_edit') });
        }

        return HTML;
    }

    static getButtonTemplate() {
        return _.template('<button class="aeviButton <%= buttonClass %> text-bottom" data-imageAction="<%= action %>"><%= translate %></button>');
    }

    static getImgTemplate() {
        return _.template('<img class="aeviForm__image <%= imgClass %>" style="max-height: <%= maxHeight %>px;" src="data:image/<%= imgFormat %>;base64,<%= imgValue %>">');
    }

	renderHiddenInput() {
		this.$cell.find('div').append('<input type="text" class="inputHidden">');
	}

	remove() {
		this.cell.classList.remove('hasEditor');

        if(!aeviIsUndefinedOrNull(this.editorEl) && this.editorEl.length)
		    this.editorEl.remove();

        this.setState('initial');
	}

    isValueChanged(): boolean {
        return this.isDataValueChanged;
    }

    formListener() {
        $(document).off('click', '*[data-imageAction]').on('click', '*[data-imageAction]', (event) => {
            event.preventDefault();
            var button = <Element>event.currentTarget;
            var action = button.getAttribute('data-imageAction');

            var allowImageChange = true;

            var imageWindow = new AeviImage(this.AeviGrid, this, this.cellIndexes, allowImageChange);
            imageWindow.render();

            switch (action) {
                case 'upload':
                    $(AeviConsts.imageModalFile).trigger('click');
                    break;

                case 'edit':
                    $(AeviConsts.imageModalEdit).trigger('click');
                    break;

                case 'delete':
                    $(AeviConsts.imageModalDelete).trigger('click');
                    break;

                default:
                    break;
            }
        });

        $(document).off(AeviConsts.imageModalChangeEvent).on(AeviConsts.imageModalChangeEvent, (event, settings: any) => {
            if (!this.AeviGrid.AeviDOM.isFormVisible()) {
                return;
            }

            switch (settings.type) {
                case 'upload':
                    this.binder.publish(settings.data);
                    $(AeviConsts.formImage).attr('src', 'data:image/' + AeviImage.getImageFormat() +';base64,' + settings.data.Value);
                    break;

                case 'delete':
                    $(AeviConsts.formImage + ',' + AeviConsts.formImageDeleteButton + ',' + AeviConsts.formImageEditButton).addClass('hidden');
                    $(AeviConsts.formImageUploadButton).removeClass('hidden');
                    break;

                case 'close':
                    $(AeviConsts.formImage).attr('src', 'data:image/' + AeviImage.getImageFormat() +';base64,' + this.defaultValue.Value);
                    break;

                case 'saveAndClose':
                    this.binder.publish(settings.data);
                    $(AeviConsts.formImage).attr('src', 'data:image/' + AeviImage.getImageFormat() +';base64,' + settings.data.Value);
                    $(AeviConsts.formImage + ',' + AeviConsts.formImageDeleteButton + ',' + AeviConsts.formImageEditButton).removeClass('hidden');
                    $(AeviConsts.formImageUploadButton).addClass('hidden');
                    break;

                default:
                    break;
            }
        });
    }

	listener() {
        $(document).off('click', this.editorId).on('click', this.editorId, (event) => {
            this.imageEditorLink = <HTMLElement>event.currentTarget;
            this.renderImageWindow();

		});
	}

    renderImageWindow() {
        /**
         * SPECIAL CASE:
         * user delete an image using form
         */
        if(aeviIsUndefinedOrNull(this.imageEditorLink)) {
            return;
        }

        var className: string = this.imageEditorLink.getAttribute('class');

        if(className.indexOf('imageBlocked') !== -1) {
            return;
        }

        if (this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(this.cellIndexes.rowIndex, 'newrow') && this.AeviGrid.AeviDataRepository.isMaxRowCountExceeded()) {
            event.preventDefault();
            this.AeviGrid.showMaxRowCountMessage();
            return false;
        }

        var isReport: boolean = this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport();
        var isLocked: boolean = this.AeviGrid.AeviGridLocker.isLocked;
        var isReadOnly: boolean = (isReport || isLocked);

        if (isReadOnly) {
            if(className.indexOf('imageMissing') !== -1) {
                return;
            }
        }

        var allowImageChange = !isReadOnly;

        var imageWindow = new AeviImage(this.AeviGrid, this, this.cellIndexes, allowImageChange);
        imageWindow.render();
    }
}
