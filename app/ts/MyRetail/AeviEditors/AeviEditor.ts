/// <reference path='../../references.ts' />
/// <reference path='IAeviEditor.ts' />
/// <reference path='../AeviDataRepository/AeviDataBinder.ts' />

class AeviEditor implements IAeviEditor {
	AeviGrid: any;
    AeviBubble: any;
	$cell: JQuery;
	cell: HTMLElement;
	border;
	borderWidth: number;

	cellIndexes: IAeviCellIndex;
	editorId: string;
	editorEl: any;
	cellClass: string;
	defaultValue: any;
	isDataValueChanged: boolean;
	binder: any;
	editorState: string;
	fakeInputId: string;
    columnName: string;

	constructor(aeviGrid, cell, settings?) {
		this.AeviGrid = aeviGrid;
		this.$cell = cell;
		this.cell = this.$cell[0];
		this.border = this.cell.childNodes[0];
		this.borderWidth = $(this.border).outerWidth();

		this.cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes(this.cell);

		this.editorId = 'aeviTextEditor';
		this.isDataValueChanged = false;
		this.fakeInputId = this.AeviGrid.AeviConsts.fakeInputSelector;

        this.AeviBubble = new AeviBubbless(this.AeviGrid);
        this.AeviBubble.render(this.$cell);
        this.showBubble();

        this.setState('initial');
	}

    showBubble() {
        var AeviDataRepository = this.AeviGrid.AeviDataRepository;

        if (AeviDataRepository.AeviTableDescription.isReport()) {
            return;
        }

        var rowIndex: number = this.cellIndexes.rowIndex;
        var message: string = null;

        if (AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'invalid') || AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'softInvalid')) {
            message = AeviDataRepository.AeviTableData.getRecordCellValue(rowIndex, AeviDataRepository.AeviTableDescription.getInvalidCellsIndex())[this.cellIndexes.cellIndex].message;
        }

        if(!aeviIsUndefinedOrNull(this.AeviBubble)) {
            this.AeviBubble.show(this.$cell, message);
        }
    }

	getState(): string {
		return this.editorState;
	}

	setState(state: string): void {
		this.editorState = state;
	}

    setReady(): void {
        $(this.border).off('mousedown').on('mousedown', (event) => {
            if (this.getState() === 'ready') {
                if (this.AeviGrid.AeviDOM.getTagName(event.currentTarget) === 'TEXTAREA') {
                    return false;
                }

                this.render();

                var cursorPosition = this.calculateCursorPosition(event);

                setTimeout(() => {
                    setSelectionRange($(this.editorEl).focus().get(0), cursorPosition, cursorPosition);
                });
            }
        });

        var input: JQuery = $(this.fakeInputId);

        setTimeout(() => {
            this.AeviGrid.AeviDOM.focusWithoutScroll(input);
		}, 10);

		setTimeout(() => {
			this.setState('ready');
		}, 80);

		$(document).on('keydown', this.fakeInputId, (event) => {
            if(!this.isInputValueValid(this.getDataVal(), event)) {
                this.AeviBubble.show(this.$cell, this.AeviGrid.AeviLocalization.translate('cannot_insert_char'));
                event.preventDefault();
                return false;
            }

			if (this.getState() === 'ready') {
				if (!this.AeviGrid.AeviClientSide.isNavigationKeyPressed(event)) {
					this.render();
				}
			}
		});
	}

    /**
     * TODO! hardstrings...
     */
	render(): void {
        this.removeReadyListeners();

        var value = this.getDataVal();
        this.defaultValue = value;

        var inputValue = (_.isNull(value) || value === 'null') ? '' : this.defaultValue;

        this.cell.innerHTML = '<div class="aeviCellBorder"><textarea id="' + this.editorId + '" class="aeviCellInput" value="' + inputValue + '">' + inputValue + '</textarea></div>';
        this.cell.classList.add('hasEditor');
        this.cellClass = this.cell.getAttribute('class');
        this.editorEl = this.getEditorEl();

        $(this.editorEl).select().focus();
        this.setState('render');
        this.listener();
	}

    renderFormEditor(column: IAeviExtendedItemDescription, fakeIndexes: IAeviCellIndex): string {
        var dataValue = column.Value;

        if (_.isNull(dataValue) || dataValue === 'null') {
            dataValue = '';
        }

        this.cellIndexes = fakeIndexes;

        this.columnName = column.ColumnName;

        var validity = this.AeviGrid.AeviDataRepository.AeviTableData.getValidityInfo(this.cellIndexes.rowIndex, this.cellIndexes.cellIndex);
        var validityClass = '';

        if (!validity.valid) {
            if (validity.hard) {
                validityClass = 'validateError';
            } else {
                validityClass = 'softValidateError';
            }
        }

        var readOnly = '';
        if (column.IsReadOnly) {
            readOnly = 'readonly';
        }

        var template = _.template('<input id="aeviForm-<%= ColumnName %>" <%= ReadOnly %> class="form-control <%= ValidityClass %>" name="<%= ColumnName %>" data-columnName="<%= ColumnName %>" type="text" value="<%= DataValue %>">');

        return template({
            ColumnName: column.ColumnName,
            ValidityClass: validityClass,
            DataValue: dataValue,
            ReadOnly: readOnly
        });
    }

	remove(): void {
		this.removeReadyListeners();
        this.AeviBubble.destroy();

		if (this.getState() !== 'render') {
			this.setState('initial');
			return;
		}
		
		this.setCellVal(this.getEditorVal());

		if (this.isValueChanged(this.getDataVal())) {
			this.isDataValueChanged = true;
		}

		this.setState('initial');

		$(document).off('keydown keyup', '#' + this.editorId);
	}

	isValueChanged(val: string): boolean {
        return (val !== this.defaultValue);
    }

	getEditorEl() {
		return <HTMLInputElement>document.getElementById(this.editorId);
	}

	getEditorVal() {
        if(aeviIsUndefinedOrNull(this.editorEl)) {
            this.AeviGrid.print('AeviEditor.getEditorVal(), "this.editorEl" is undefined or null.');
            return null;
        }

		return this.editorEl.value;
	}

	setCellVal(val: string) {
		this.cell.innerHTML = '<div class="aeviCellBorder">' + val + '</div>';
	}

	getCellVal() {
		return this.cell.innerText;
	}

    getDataVal() {
        return this.AeviGrid.AeviDataRepository.AeviTableData.getRecordCellValue(this.cellIndexes.rowId, this.cellIndexes.cellId);
	}

	removeCellClasses(): void {
		this.cellClass = this.cellClass.replace('hasEditor', '').replace('typing', '');
		this.cell.className = this.cellClass;
	}

	initBinder(): void {
		this.binder = new AeviDataBinder(this.AeviGrid.AeviDataRepository.AeviTableData);
		this.binder.subscribe(this.cellIndexes);
	}

    initFormBinder(formRepository): void {
        this.binder = new AeviFormDataBinder(formRepository);
        this.binder.subscribe(this.columnName);
    }

    formListener(): void {
        var editorWrapper = $('#aeviForm-' + this.columnName).closest('.aeviForm__editorWrapper');

        var destroyBubbleIfIsRendered = false;
        this.AeviBubble = new AeviBubbless(this.AeviGrid, destroyBubbleIfIsRendered);
        this.AeviBubble.render(editorWrapper);

        $(document).on('mousedown', '#aeviForm-' + this.columnName, (event) => {
            this.showBubble();
        });

        $(document).on('keyup', '#aeviForm-' + this.columnName, (event) => {
            var editor = <HTMLInputElement>event.currentTarget;
            this.binder.publish(editor.value);

            var validatorFactory = new AeviValidatorFactory(this.AeviGrid, this.AeviGrid.AeviDataRepository.AeviLocalization, this.AeviGrid.AeviDataRepository.AeviTableDescription);
            validatorFactory.createValidator(editor.value, this.cellIndexes.cellIndex);
            var validator = validatorFactory.getValidator();
            var result = validator.validate();

            if (!result.valid) {
                this.AeviBubble.show(editorWrapper, result.message);
            }
        });

        $(document).on('keydown', '#aeviForm-' + this.columnName, (event) => {
            var editor = <HTMLInputElement>event.currentTarget;
            var value = editor.value;
            this.AeviBubble.hide();

            if(!this.isInputValueValid(value, event)) {
                this.AeviBubble.show(editorWrapper, this.AeviGrid.AeviLocalization.translate('cannot_insert_char'));
            }
        });

        $(document).on('blur', '#aeviForm-' + this.columnName, (event) => {
            this.AeviBubble.hide();
        });
    }

	listener(): void {
		this.initBinder();

        $(document).on('keydown', '#' + this.editorId, (event) => {
            var codeKey = this.AeviGrid.AeviClientSide.getKeyName(event);

            if(codeKey === 'escape') {
                this.editorEl.value = this.defaultValue;
            }

            if(codeKey === 'left arrow') {
                this.goPrevCellIfRequired(this.$cell, $(this.editorEl), event);
            }

            if(codeKey === 'right arrow') {
                this.goNextCellIfRequired(this.$cell, $(this.editorEl), event);
            }

            if(!this.isInputValueValid(this.getEditorVal(), event)) {
                this.AeviBubble.show(this.$cell, this.AeviGrid.AeviLocalization.translate('cannot_insert_char'));
            }
        });

		$(document).on('keyup', '#' + this.editorId, (event) => {
            this.binder.publish(this.getEditorVal());
		});
	}

	removeReadyListeners(): void {
		$(document).off('keydown', this.fakeInputId);
		$(this.border).off('mousedown');
	}

	/**
	 * @return {number} Caret position
	 */
	calculateCursorPosition(event): number {
		var offsetHelper = 0;
		if (this.AeviGrid.AeviClientSide.isChrome() || this.AeviGrid.AeviClientSide.isSafari())
			offsetHelper = 5;
		var cellX = this.$cell.offset().left + offsetHelper;
		var clickedX = event.pageX - cellX;
		var textWidth = $(this.editorEl).textWidth();
        var editorValue = this.getEditorVal();
        var charCount = (aeviIsUndefinedOrNull(editorValue)) ? 0 : editorValue.length;
		var oneCharWidth = Math.round(textWidth / charCount);
		return Math.floor(clickedX / oneCharWidth);
	}

    isInputValueValid(value, event): boolean {
        return true;
    }

    goPrevCellIfRequired(cell, editor, event) {
        var caretPosition = this.getCaretPosition(editor);

        if(0 === caretPosition) {
            event.preventDefault();
            event.stopPropagation();
            this.AeviGrid.AeviGridHandler.AeviBodyCellHandler.shiftTab(cell);
            return;
        }
    }

    goNextCellIfRequired(cell, editor, event) {
        var caretPosition = this.getCaretPosition(editor);

        if(editor.val().length === caretPosition) {
            event.preventDefault();
            event.stopPropagation();
            this.AeviGrid.AeviGridHandler.AeviBodyCellHandler.tab(cell);
            return;
        }
    }

    getCaretPosition($input) {
        var input = $input.get(0);
        if (!input) return;

        if (document.selection)
            input.focus();

        return 'selectionStart' in input ? input.selectionStart:'' || Math.abs(document.selection.createRange().moveStart('character', -input.value.length));
    }
}
