/// <reference path='../../references.ts' />

class AeviFakeEditor {
    AeviGrid: any;
    editorEl: HTMLInputElement;
    currentValue: any;

    constructor(aeviGrid: any) {
        this.AeviGrid = aeviGrid;
        this.render();
    }

    render() {
        var selector = this.AeviGrid.AeviConsts.fakeInputSelector;

        if (this.AeviGrid.AeviDOM.length(selector))
            return;

        selector = selector.makeClassFromSelector();

        var styles = 'position: fixed; top: 0; left: 0; width: 0; height: 0; z-index: -9999999999;';

        $('body').append('<textarea id="' + selector + '" type= "text" style= "' + styles +'"></textarea>');

        this.editorEl = <HTMLInputElement>document.getElementById(selector);
    }

    setCSSPosition(y: number, x: number) {
        this.editorEl.style.top = y + 'px';
        this.editorEl.style.left = x + 'px';
    }

    setContent(cell: JQuery) {
        this.currentValue = this.AeviGrid.AeviDataRepository.AeviTableData.getRecordsToClipboard();
        this.editorEl.value = this.currentValue;

        if (this.AeviGrid.AeviClientSide.isIe() || this.AeviGrid.AeviClientSide.isEdge()) {
            var offset = AeviBodyCell.getOffset(cell[0]);
            this.setCSSPosition(offset.top, offset.left);
        }
    }

    getContent() {
        return this.currentValue;
    }

    focus() {
        this.editorEl.select();
    }
}
