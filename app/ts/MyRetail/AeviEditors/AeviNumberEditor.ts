/// <reference path='../../references.ts' />
/// <reference path='IAeviEditor.ts' />
/// <reference path='AeviEditor.ts' />

class AeviNumberEditor extends AeviEditor implements IAeviEditor {
	dataType: any;

	constructor(aeviGrid, cell, dataType) {
		super(aeviGrid, cell);
		this.editorId = 'aeviNumberEditor';
		this.dataType = dataType;
	}

	render() {
		super.render();
        this.editorEl.classList.add('text-right');
	}

    isInputValueValid(value, event): boolean {
        var keyCode = event.keyCode;

        if(!_.isNull(value)) {
            value = value.toString();

            if ((~value.indexOf(',') && (keyCode == 188 || keyCode == 190 || keyCode == 110)) ||
                (~value.indexOf('.') && (keyCode == 190 || keyCode == 188 || keyCode == 110))) {
                event.preventDefault();
                return false;
            }
        }

        // Allow: backspace, delete, tab, escape, enter and . ,
        if ($.inArray(keyCode, [46, 8, 9, 27, 13, 110, 190, 188]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (keyCode == 65 && ( event.ctrlKey === true || event.metaKey === true ) ) ||
                // Allow: home, end, left, right, down, up
            (keyCode >= 35 && keyCode <= 40)){
            return true;
        }

        // Ensure that it is a number and stop the keypress
        if (((keyCode < 48 || keyCode > 57)) && (keyCode < 96 || keyCode > 105)) {
            event.preventDefault();
            return false;
        }

        if(keyCode > 47 && keyCode < 58) {
            if(!event.shiftKey) {
                event.preventDefault();
                return false;
            }
        }

        return true;
    }
}
