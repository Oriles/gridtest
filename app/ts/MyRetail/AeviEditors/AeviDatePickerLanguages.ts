/// <reference path='../../references.ts' />

class AeviDatePickerLanguages {

	constructor(){
		this.extend();
	}

	extend(): void{
		$.datepicker.regional['vi'] = {
			closeText: 'Đóng',
			prevText: '&#x3c;Trước',
			nextText: 'Tiếp&#x3e;',
			currentText: 'Hôm nay',
			monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',
				'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],
			monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',
				'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
			dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],
			dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
			dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
			weekHeader: 'Tu',			
			firstDay: 0,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};

		$.datepicker.regional['tr'] = {
			closeText: "kapat",
			prevText: "&#x3C;geri",
			nextText: "ileri&#x3e",
			currentText: "bugün",
			monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran",
				"Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
			monthNamesShort: ["Oca", "Şub", "Mar", "Nis", "May", "Haz",
				"Tem", "Ağu", "Eyl", "Eki", "Kas", "Ara"],
			dayNames: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"],
			dayNamesShort: ["Pz", "Pt", "Sa", "Ça", "Pe", "Cu", "Ct"],
			dayNamesMin: ["Pz", "Pt", "Sa", "Ça", "Pe", "Cu", "Ct"],
			weekHeader: "Hf",			
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ""
		};
	}	
}