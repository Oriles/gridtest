/// <reference path='../../references.ts' />
/// <reference path='IAeviEditor.ts' />
/// <reference path='AeviEditor.ts' />
/// <reference path='../AeviDate.ts' />
/// <reference path='AeviDatePickerLanguages.ts' />

class AeviDateEditor extends AeviEditor implements IAeviEditor {
	AeviDataRepository: any;
	AeviDate: any;
    displayType: string;
    hiddenEditorEl: any;
    datePickerLanguages: any;

	constructor(aeviGrid, cell, displayType) {
		super(aeviGrid, cell);
		this.editorId = 'aeviDateInput';
        this.displayType = displayType;
		this.AeviDataRepository = this.AeviGrid.AeviDataRepository;        
        this.datePickerLanguages = new AeviDatePickerLanguages();

        $.datepicker.setDefaults($.datepicker.regional[this.AeviGrid.AeviLocalization.getCulture()]);
        
	}

    render() {
        this.removeReadyListeners();

        this.initDateFormat();

        this.defaultValue = this.getDataVal();

        this.cell.innerHTML = '<div class="aeviCellBorder">' + '<input type="text" id="' + this.editorId + '" class="aeviCellDate" value="' + this.AeviDate.getString() + '">' + '<i class="datepicker"></i>' + '</div>';
        this.cell.classList.add('hasEditor');
        this.cellClass = this.cell.getAttribute('class');
        this.editorEl = this.getEditorEl();

        $(this.editorEl).select().focus();
        this.setState('render');
        this.listener();
	}

    renderFormEditor(column: IAeviExtendedItemDescription) {
        var dataVal = column.Value;
        this.AeviDate = new AeviDate(this.AeviGrid.AeviLocalization.getCulture(), this.displayType, dataVal);

        $(document).off('click touchstart', 'input[data-fakeColumnName="' + column.ColumnName + '"] + .datepicker').on('click touchstart', 'input[data-fakeColumnName="' + column.ColumnName + '"] + .datepicker', (event) =>{
            this.editorEl = document.querySelector('input[data-fakeColumnName="' + column.ColumnName + '"]');
            this.hiddenEditorEl = document.querySelector('input[data-columnName="' + column.ColumnName + '"]');
            this.showFormCalendar();
        });

        this.columnName = column.ColumnName;

        var readOnly = (column.IsReadOnly) ? 'readonly': '';

        var HTML = '<input type="hidden" data-columnName="' + column.ColumnName + '" name="' + column.ColumnName + '"><input class="form-control aeviCellDate" ' + readOnly + ' id="aeviForm-' + this.columnName + '" data-fakeColumnName="' + column.ColumnName + '" type="text" value="' + this.AeviDate.getString() + '">';

        if (column.IsReadOnly) {
            return HTML;
        }

        return HTML + '<i class="datepicker"></i>';
    }

    formListener(): void {}

    showFormCalendar(): void {
        var __this = this;
        var old_goToToday = $.datepicker._gotoToday;

        $.datepicker._gotoToday = function(id) {
            var currentDatepicker = this;

            old_goToToday.call(currentDatepicker, id);
            currentDatepicker._selectDate(id);

            /*
             * MUST BE IN OTHER THREAD BECAUSE IE
             */
            setTimeout(() => {
                $(__this.editorEl).datetimepicker('hide');
            }, 0);

            __this.AeviDate.parseString(this.value);
            __this.binder.publish(__this.AeviDate.getIsoString());
        };
       
        $(this.editorEl).datetimepicker({
            showTimepicker: false,
            onSelect:(val) => {
                this.AeviDate.parseString(val);
                this.binder.publish(this.AeviDate.getIsoString());
            },
            currentText : $.datepicker.regional[this.AeviGrid.AeviLocalization.getCulture()].currentText,
            closeText: $.datepicker.regional[this.AeviGrid.AeviLocalization.getCulture()].closeText
        });

        $(this.editorEl).datetimepicker('show');
    }

    publishChanges() {
        this.AeviDate.parseString(this.getEditorVal());
        this.binder.publish(this.AeviDate.getIsoString());
    }

    listener(): void {
        this.initBinder();

        $(document).on('keyup', '#' + this.editorId, (event) => {
            this.publishChanges();
        });

        $(document).on('click touchstart', this.$cell.selector + ' .datepicker', (event) =>{
            this.showCalendar();
        });


        if(this.AeviGrid.AeviDOM.length('#' + this.editorId) && window.navigator.msPointerEnabled) {
            this.editorEl.addEventListener("MSPointerDown", () => {
                this.showCalendar();
            }, false);
        }
    }

	initDateFormat() {
        this.AeviDate = new AeviDate(this.AeviGrid.AeviLocalization.getCulture(), this.displayType, this.getDataVal());
	}

    remove() {
        super.remove();
        $(document).off('click touchstart', this.$cell.selector + ' .datepicker');
        $(this.editorEl).datepicker('destroy');
    }

    showCalendar(): void {
        /**
		 * FIX JQUERY-UI BUG WITH TODAY BUTTON
		 */
        var __this = this;
		var old_goToToday = $.datepicker._gotoToday;
		$.datepicker._gotoToday = function(id) {
			var currentDatepicker = this;

			old_goToToday.call(currentDatepicker, id);
			currentDatepicker._selectDate(id);

			/*
			 * MUST BE IN OTHER THREAD BECAUSE IE
			 */
			setTimeout(() => {
				$(__this.editorEl).datetimepicker('hide');
			}, 0);

            __this.publishChanges();
		};

        $(this.editorEl).datetimepicker({
            showTimepicker: false,
            onSelect:() => {
                this.publishChanges();
            },
            currentText : $.datepicker.regional[this.AeviGrid.AeviLocalization.getCulture()].currentText,
            closeText: $.datepicker.regional[this.AeviGrid.AeviLocalization.getCulture()].closeText
        });

        $(this.editorEl).datetimepicker('show');
	}

    extendLanguages(): void {

    }
}
