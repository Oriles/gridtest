/// <reference path='../../references.ts' />

class AeviActionEditor extends AeviEditor implements IAeviEditor {
	actionParameters: IAeviEntityActionDefinition;

	constructor(aeviGrid, cell, entityActionDefinition: IAeviEntityActionDefinition) {
		super(aeviGrid, cell);
		this.editorId = 'aeviActionEditor';
		this.actionParameters = entityActionDefinition;
	}

    setReady() {
        this.render();
    }

	render() {
		this.defaultValue = this.getDataVal();

        var HTML: string = null;

        if(this.actionParameters.ResultType === MyRetail.IAeviEntityActionResultType.Image) {
            HTML = '<div class="aeviCellBorder"><a class="aeviActionLink" id="aeviActionEditor">' + this.defaultValue + '</a></div>';
        }

        if(this.actionParameters.ResultType === MyRetail.IAeviEntityActionResultType.Form) {
            HTML = '<div class="aeviCellBorder noImage"><a class="aeviActionLink" id="aeviActionEditor"></a></div>';
        }

        this.cell.innerHTML = HTML;

        this.setState('render');
		this.listener();
	}

    remove() {
		this.cell.innerHTML = '<div class="aeviCellBorder">' + this.defaultValue + '</div>';
        this.setState('initial');
	}

	listener() {
		$(document).off('click', '#aeviActionEditor').on('click', '#aeviActionEditor', (event) => {
			event.preventDefault();
			this.AeviGrid.AeviDataService.executeAction(this.cellIndexes.rowIndex, this.actionParameters);
		});
	}

    trigger() {
        $('#aeviActionEditor').trigger('click');
    }
}
