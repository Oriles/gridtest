/// <reference path='../../references.ts' />
/// <reference path='IAeviEditor.ts' />
/// <reference path='AeviEditor.ts' />

class AeviEnumEditor extends AeviEditor implements IAeviEditor {
	column: any;
	isEditorRendered: boolean;
	cellIndexes: any;
    isAnyOptionSelected: boolean;

	constructor(aeviGrid, cell, column) {
		super(aeviGrid, cell);
		this.editorId = 'aeviEnumEditor';
		this.column = column;
	}

    setReady() {
        AeviDOM.disableScroll();
        this.render();
        this.showBubble();
        setTimeout(() => { AeviDOM.enableScroll(); }, 50);
    }

	render() {
        this.defaultValue = this.getDataVal();

        this.cell.innerHTML = this.getRenderedElement();

        this.editorEl = <HTMLSelectElement>document.getElementById(this.editorId);

        this.cell.classList.add('hasEditor');
        this.cell.classList.add('aeviCellSelect');
        this.cellClass = this.cell.getAttribute('class');

        this.setState('render');

        this.checkEditorRendered();
        this.listener();
	}

    renderFormEditor(column: IAeviExtendedItemDescription, fakeIndexes: IAeviCellIndex): string {
        this.columnName = column.ColumnName;
        this.cellIndexes = fakeIndexes;

        return this.getRenderedElement({ renderForm: true, className: 'form-control', columnName: column.ColumnName, id: ('aeviForm-' + column.ColumnName), IsReadOnly: column.IsReadOnly });
    }

	getRenderedElement(settings?: any): string {
		if (this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(this.cellIndexes.rowId, 'newrow') && this.AeviGrid.AeviDataService.AeviDataRepository.isMaxRowCountExceeded())
			return '<div class="aeviCellBorder"></div>';

        if (aeviIsUndefinedOrNull(settings)) {
            var settings: any = {
                className: '',
                columnName: '',
                renderForm: false,
                id: 'aeviEnumEditor'
            };
        }

        var disabled = '';

        if(!aeviIsUndefinedOrNull(settings.IsReadOnly)) {
            if (settings.IsReadOnly) {
                disabled = 'disabled';
            }
        }

        var column = this.column;
		var enumValues = column.EnumValues;
		var className = 'aeviEnumEditor';

        var HTML = '';

        if(settings.renderForm === false) {
            HTML += '<div class="aeviCellBorder">';
        }

        this.isAnyOptionSelected = false;

        HTML += '<select id="' + settings.id + '" data-columnName="' + settings.columnName + '" class="' + settings.className + ' ' + className + '">';
		
		for (var i = 0; i < enumValues.length; i++) {
			var enumValue = enumValues[i];

            HTML += '<option ' + disabled + ' value="' + enumValue.DataValue + '"';

			var cellDataValue: any = this.getDataVal();

            if (cellDataValue === 'null')
				cellDataValue = null;

			if (cellDataValue === 'true')
				cellDataValue = true;

			if (cellDataValue === 'false')
				cellDataValue = false;

            //console.log(enumValue.DataValue, cellDataValue);

			if (enumValue.DataValue == cellDataValue) {
				HTML += ' selected';
                this.isAnyOptionSelected = true;
            }

			HTML += '>' + enumValue.DisplayValue + '</option>';
		}

        HTML += '</select>';

        if(settings.renderForm === false) {
            HTML += '</div>';
        }

        return HTML;
	};

	remove(): void {
		this.removeCellClasses();

        this.setCellVal(this.getEditorVal());
		
		if (this.isValueChanged(this.getDataVal())) {
			this.isDataValueChanged = true;
        }

        this.setState('initial');
	}

    formListener(): void {
        $(document).off('change', '#aeviForm-' + this.columnName).on('change', '#aeviForm-' + this.columnName, () => {
            var editor = <HTMLSelectElement>document.getElementById('aeviForm-' + this.columnName);
            this.binder.publish(editor.value);
        });

        /**
         * SPECIAL CASE
         * if input value is not choosed, listener manually select first available value
         */
        $('#aeviForm-' + this.columnName).change();
    }

	listener() {
		this.initBinder();

        if (this.isAnyOptionSelected === false) {
            this.isDataValueChanged = true;
            this.AeviGrid.AeviDataService.AeviApiService.setCellChanged(true);
            this.binder.publish(this.getEditorDataVal());
        }

		$(document).off('mousedown', '#' + this.editorId).on('mousedown', '#' + this.editorId, (event) => {			
			if (this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(this.cellIndexes.rowId, 'newrow') && this.AeviGrid.AeviDataService.AeviDataRepository.isMaxRowCountExceeded()) {
				event.preventDefault();
				this.AeviGrid.showMaxRowCountMessage();
				return false;
			}
		});

		$(document).off('change', '#' + this.editorId).on('change', '#' + this.editorId, () => {
			this.AeviGrid.AeviDataService.AeviApiService.setCellChanged(true);
			this.binder.publish(this.getEditorDataVal());
		});

        $(document).off('keydown', '#' + this.editorId).on('keydown', '#' + this.editorId, (event) => {
            var codeKey = this.AeviGrid.AeviClientSide.getKeyName(event);

            if(codeKey === 'left arrow') {
                event.preventDefault();
                this.AeviGrid.AeviGridHandler.AeviBodyCellHandler.shiftTab(this.$cell);
            }

            if(codeKey === 'right arrow') {
                event.preventDefault();
                this.AeviGrid.AeviGridHandler.AeviBodyCellHandler.tab(this.$cell);
            }
        });

		document.addEventListener("keydown", (event) => {
			if (this.$cell && this.$cell.hasClass('hasEditor')) {
                var key = this.AeviGrid.AeviClientSide.getKeyName(event)

                if (key === 'backspace') {
					event.preventDefault();
				} else if (key === '(space)' && this.AeviGrid.AeviClientSide.isIe()) {
					this.$cell.toggleClass('typing');
				}

				this.editorEl.focus();
			}
		});

		if (this.AeviGrid.AeviClientSide.isSafari()) {
			$(document).off('click', '#' + this.editorId).on('click', '#' + this.editorId, () => {
				setTimeout(() => this.AeviGrid.select(this.cell), 0);
			});
		}
	}

    getEditorDataVal() {
        var val = (this.isEditorRendered) ? this.editorEl.options[this.editorEl.selectedIndex].value : this.cell.innerText;
        return (val === '') ? null : val;
    }

	getEditorVal() {
		var val = (this.isEditorRendered) ? this.editorEl.options[this.editorEl.selectedIndex].text : this.cell.innerText;
		return (val === '') ? '' : val;
	}

	checkEditorRendered() {
		var optionsLength: number = 0;
		this.isEditorRendered = (!aeviIsUndefinedOrNull(this.editorEl));

		if (this.isEditorRendered)
			optionsLength = this.editorEl.options.length;

		if (this.isEditorRendered) {
			if (optionsLength < 1)
				this.isEditorRendered = false;
		}
	}
}
