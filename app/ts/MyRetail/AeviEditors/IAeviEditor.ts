/// <reference path='../../references.ts' />

interface IAeviEditor {
	AeviGrid: any;
	defaultValue: any;
	cellIndexes: any;
    cell: HTMLElement;
    $cell: JQuery;
	editorId: string;
	editorEl: any;
    isDataValueChanged: boolean;
    binder: any;

	render(...args: any[]): void;
    remove(...args: any[]): void;
    listener(...args: any[]): void;

    renderImageWindow?(): void;
}
