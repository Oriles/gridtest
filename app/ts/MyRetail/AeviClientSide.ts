/// <reference path='../references.ts' />

class AeviClientSide {
    AeviGrid: any;
    browser: string;
    system: string;
    touchDevice: boolean;

    static BROWSER = {
        CHROME: 'chrome',
        IE: 'ie',
        OPERA: 'opera',
        FIREFOX: 'firefox',
        SAFARI: 'safari',
        EDGE: 'edge'
    };

    static SYSTEM = {
        WINDOWS: 'Windows',
        MACOS: 'MacOS',
        UNIX: 'UNIX',
        LINUX: 'Linux'
    };

    mac: boolean = null;
    windows: boolean = null;
    chrome: boolean = null;
    safari: boolean = null;
    ie: boolean = null;
    firefox: boolean = null;
    opera: boolean = null;

    constructor(aeviGrid: any) {
        this.AeviGrid = aeviGrid;
        this.browser = this.getBrowser();
        this.system = this.getSystem();
        this.touchDevice = 'ontouchstart' in window || navigator.msMaxTouchPoints ? true : false;
        document.querySelector('body').classList.add(this.getTrimmedBrowser());
    }

    getBrowser() {
        var ua= navigator.userAgent, tem,
        M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
        }
        if(M[1]=== 'Chrome'){
            tem= ua.match(/\bOPR\/(\d+)/);
            if(tem!== null) return 'Opera '+tem[1];
        }
        if(/Edge/i.test(navigator.userAgent)){
           return 'edge';
        }
        M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem= ua.match(/version\/(\d+)/i))!== null) M.splice(1, 1, tem[1]);
        return M.join(' ');
    }

    getTrimmedBrowser(): string {
        return this.browser.toLowerCase().split(' ')[0];
    }

    getSystem() {
        var os: string = null;
        var appVersion: string = navigator.appVersion;

        if (appVersion.indexOf('Win')!=-1) { os = AeviClientSide.SYSTEM.WINDOWS; }
        if (appVersion.indexOf('Mac')!=-1) { os = AeviClientSide.SYSTEM.MACOS; }
        if (appVersion.indexOf('X11')!=-1) { os = AeviClientSide.SYSTEM.UNIX; }
        if (appVersion.indexOf('Linux')!=-1) { os = AeviClientSide.SYSTEM.LINUX; }

        return os;
    }

    isMac() {
        if (!aeviIsUndefinedOrNull(this.mac))
            return this.mac;

        this.mac = navigator.platform.toUpperCase().indexOf('MAC') >= 0;
        return this.mac;
    }

    isBrowser(browser: string): boolean {
        if (this.browser.toLowerCase().indexOf(browser) !== -1)
            return true;
        return false;
    }

    isChrome(): boolean {
        return this.isBrowser(AeviClientSide.BROWSER.CHROME);
    }

    isIe(): boolean {
        return this.isBrowser(AeviClientSide.BROWSER.IE);
    }

    isEdge(): boolean {
        return this.isBrowser(AeviClientSide.BROWSER.EDGE);
    }

    isOpera(): boolean {
        return this.isBrowser(AeviClientSide.BROWSER.OPERA);
    }

    isFirefox(): boolean {
        return this.isBrowser(AeviClientSide.BROWSER.FIREFOX);
    }

    isSafari(): boolean {
        return this.isBrowser(AeviClientSide.BROWSER.SAFARI);
    }

    isLowerThanIe11(): boolean {
        if (!this.isIe())
            return false;

        if (this.browser.toLowerCase().indexOf('11') === -1)
            return true;
        return false;
    }

    isTouch() {
        return this.touchDevice;
    }

    getKeyName(event): string {
        return this.AeviGrid.AeviConsts.keys[event.keyCode || event.which];
    }

    isKey(event, key: string): boolean {
        var keyName = this.getKeyName(event);
        return (keyName == key);
    }

    getNumberWithLocalizedDecimalPoint(val) {
        if(aeviIsUndefinedOrNull(val))
            return null;

        val = val.replace('.', ',');
        return val;
    }

    static isPasteEvent(event, key: string) {
        return ((event.ctrlKey || event.metaKey) && key === 'v');
    }

    isNavigationKeyPressed(event) {
        var key = this.getKeyName(event);

        if (aeviIsUndefinedOrNull(key))
            return true;

        if(event.ctrlKey || event.metaKey)
            return true;

        switch(key) {
            case 'backspace':
            case 'enter':
            case 'shift':
            case 'ctrl':
            case 'tab':
            case 'alt':
            case 'pause/break':
            case 'caps lock':
            case 'insert':
            case 'num lock':
            case 'scroll lock':
            case 'f1':
            case 'f2':
            case 'f3':
            case 'f4':
            case 'f5':
            case 'f6':
            case 'f7':
            case 'f8':
            case 'f9':
            case 'f10':
            case 'f11':
            case 'f12':
            case '33':
            case '34':
            case '35':
            case '46':
            case '37':
            case '38':
            case '39':
            case '40':
            case 'up arrow':
            case 'right arrow':
            case 'down arrow':
            case 'left arrow':
            case 'home':
            case 'end':
            case 'page down':
            case 'page up':
            case 'escape':
            case 'left window key':
            case 'right window key':
                //event.preventDefault();
                return true;

            default:
                return false;
        }
    }
}
