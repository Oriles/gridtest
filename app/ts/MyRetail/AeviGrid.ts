/// <reference path='AeviApiService/AeviApiService.ts' />
/// <reference path='AeviDataRepository/AeviDataRepository.ts' />
/// <reference path='AeviDataService/AeviDataService.ts' />
/// <reference path='AeviGridApi.ts' />

/// <reference path='AeviGridEditor.ts' />

/// <reference path='AeviDimensions.ts' />

/// <reference path='AeviHandlers/AeviGridHandler.ts' />

/// <reference path='AeviPlugins/AeviPluginFactory.ts' />
/// <reference path='AeviDOM.ts' />
/// <reference path='AeviClientSide.ts' />
/// <reference path='AeviConsts.ts' />
/// <reference path='AeviLocalization.ts' />
/// <reference path='AeviAjaxPreloader.ts' />
/// <reference path='AeviHelp.ts' />
/// <reference path='AeviStatusBar.ts' />
/// <reference path='AeviBubbless.ts' />
/// <reference path='AeviSum.ts' />
/// <reference path='AeviImage.ts' />
/// <reference path='AeviPlugins/AeviPluginApi.ts' />
/// <reference path='AeviLockMessage.ts' />

class AeviGrid {
	firstLoad: boolean;
	mode: any;
	apiAddress: string;
	tableId: string;
	apiUsername: string;
	apiPassword: string;
	apiSlot: number;
	apiEntity: string;
	apiLang: string;
	apiToken: string;
	entityType: number;
    antiForgeryToken: any;
	theme: string;
	pluginsNames: any;
	excelFilePath: string;
	debugger: boolean;
	gridVersion: string;
	apiVersion: string;
	plugins: any;
	scrollEl: JQuery;
	aeviWrapper: JQuery;
	aeviTableId: string;

	AeviApiService: any;
	AeviDataService: any;
	AeviDataRepository: any;
	AeviGridApi: any;

	AeviPluginApi: any;
	AeviDOM: any;
	AeviClientSide: any;
	AeviDimensions: any;
	AeviConsts: any;
	AeviLocalization: any;
	AeviHelp: any;
	AeviAjaxPreloader: any;
	AeviStatusBar: any;
	AeviBubbless: any;
	AeviSum: any;
	AeviGridHandler: any;
	AeviImage: any;
	AeviGridEditor: any;
	AeviLockMessage: any;
    AeviDownloader: any;
    AeviToolbar: any;

	/**
	 * instance of AeviSearch is created in AeviGridHandler, CTRL+F or AeviToolbarHandler.search()
	 */
	AeviSearch: any;

	lastSelectedCell: any;

	/**
	 * instance of AeviPager is created in AeviDataService.init()
	 */
	AeviPager: any;

	/**
	 * instance of AeviGridLocker is created in AeviDataService.showData()
	 */
	AeviGridLocker: any;

	/**
	 * instance of AeviFilter is created in AeviDataService.initFilters()
	 */
	AeviFilter: any;

    /**
     * instance of AeviFilter is created in AeviFilter.initFilters()
     */
    AeviFilterFactory: any;

	lockMessageShowed: boolean;
	isAllRowsSelected: boolean;

    /**
     * instance of AeviFilter is created in AeviGridHandler.cellEnter()
     */
    AeviFakeEditor: any;

    /**
     * if another user editing entity
     */
    blockMode: boolean;

    /**
     * if another user commit data
     */
    changeMode: boolean;

	constructor(settings: any) {
        this.blockMode = false;
		this.firstLoad = true;

        this.initializeParameters(settings);

		this.initializeEnvironment({
			debugger: true,
			gridVersion: '1.0'
		});
		
		this.setGridTheme();
		this.setGridModes();


        new AeviStorage();

		this.AeviDOM = new AeviDOM(this);
		this.AeviClientSide = new AeviClientSide(this);
		this.AeviConsts = new AeviConsts(this, this.tableId);
		this.AeviLocalization = new AeviLocalization(this.apiLang, settings, this.AeviConsts, this.AeviClientSide);
		this.AeviHelp = new AeviHelp(this);

        AeviGrid.initializeToastr();

		this.render();

		this.AeviAjaxPreloader = new AeviAjaxPreloader(this);
		this.scrollEl = $('#' + this.tableId).find('.table-body');
		this.AeviStatusBar = new AeviStatusBar(this);
		this.AeviBubbless = new AeviBubbless(this);

		this.AeviSum = new AeviSum(this);

        this.AeviApiService = new AeviApiService(this, this.apiAddress, this.apiUsername, this.apiPassword, this.apiSlot, this.apiEntity, this.apiLang, this.apiToken, this.entityType);
		this.AeviDataRepository = new AeviDataRepository(this.AeviConsts, this.AeviLocalization, this);

        this.AeviDownloader = new AeviDownloader(this);

		this.AeviGridApi = new AeviGridApi(this, this.AeviDataRepository);

		this.initializePlugins();

		this.AeviDataService = new AeviDataService(this, this.AeviApiService, this.AeviDataRepository);
		this.AeviGridEditor = new AeviGridEditor(this, this.AeviDataService, this.tableId);

		this.AeviGridHandler = new AeviGridHandler(this);

        AeviImage.init(this);
	}

	render() {
		var app: Element = document.getElementById(this.tableId);

        this.aeviTableId = 'aeviTable';

        app.classList.add('inner-container');

		var tableHeadHTML: string = '<div class="table-head"></div><div class="table-body"><table id="' + this.aeviTableId + '" class="' + this.aeviTableId + '"></table></div>';
		this.AeviDOM.append(app, tableHeadHTML);

		$(app).wrap('<div id="' + this.tableId + '-wrapper' + '" class="aeviWrapper"></div>');

		this.aeviWrapper = $(document.getElementById(this.tableId + '-wrapper'));

		$(app).wrap('<div class="outer-container"></div>');
	}

	setGridModes() {
		this.mode = {clipboard : 0};
	}

	setGridTheme() { 
		document.querySelector('body').classList.add('aeviTheme--' + this.theme);
	}

	static initializeToastr() {
		toastr.options = {
			closeButton : true,
			positionClass : 'toast-top-right',
			timeOut : 8000,
			showEasing: 'swing',
	  		hideEasing: 'linear',
	  		hideDuration: 300
		};
	}

	initializeEnvironment(settings: any) {
		this.debugger = settings.debugger;
		if(!this.debugger) {console.log = function() {};}
		this.gridVersion = settings.gridVersion;
		this.apiVersion = settings.apiVersion;
	}

	initializeParameters(settings: any) {
		var requiredParameters = {
			tableId: {required: true, value: ''},
			apiAddress: {required: true, value: ''},
			apiUsername: {required: false, value: ''},
			apiPassword: {required: false, value: ''},
			apiSlot: {required: false, value: ''},
			apiEntity: {required: true, value: ''},
			apiLang: {required: false, value: ''},
			apiToken: {required: false, value: ''},
			entityType: {required: true, value : 0},
			theme: {required: false, value: 0},
			pluginsNames: {required: false, value : []},
			excelFilePath: {required: false, value: []},
            antiForgeryToken: {required: true,value: ''},
            roles: {required: true, value : {}},
		};

		var stateIndex: any;
		
		for(var index in settings) { 
			for(stateIndex in requiredParameters) {
				if(stateIndex === index) {
					this[stateIndex] = settings[index];
				}
			}		
		}
	}

	initializePlugins() {
		this.AeviPluginApi = new AeviPluginApi(this);

		if(aeviIsUndefinedOrNull(this.pluginsNames) || _.isEmpty(this.pluginsNames))
			this.pluginsNames = [];

		if(!_.contains(this.pluginsNames, 'DynamicEnum'))
			this.pluginsNames.push('DynamicEnum');

		this.plugins = [];
		var pluginFactory = new AeviPluginFactory(this.AeviGridApi);

		for(var i = 0; i < this.pluginsNames.length; i++) {
			var pluginName = this.pluginsNames[i];
			var plugin = pluginFactory.get(pluginName);

			if(!aeviIsUndefinedOrNull(plugin)) {
				this.plugins.push(plugin);
			}
		}
	}

	print(message: string) {
		if(this.debugger) {
			console.log(
			  '%cAeviErrorLog: ' + message,
			  'color: #ed1c24; font-size: 12px'
			);
		}
	}

	log(message: string) {
		if(this.debugger) {
			console.log(
			  '%cAeviLog: ' + message,
			  'color: #008C25; font-size: 12px'
			);
		}
	}

	setSizes(): void {
		if(_.isUndefined(this.AeviDimensions))
			this.AeviDimensions = new AeviDimensions(this);
		else
			this.AeviDimensions.getInitialDimensions();
	}

	selectFirstCell(): void {
        var props: IAeviMoveToProps = {
            rowId: 0,
            cellId: 0,
            scrollLeft: true,
            click: false
        };

        this.AeviPager.moveTo(props);

        /**
         * selecting first VISIBLE cell
         * @type {number}
         */
        var columnIndex = 0;
        var isVisible = false;

        do {
            columnIndex++;
            var header = this.AeviDataRepository.AeviTableDescription.getColumnHeader(columnIndex);
            isVisible = this.AeviDataRepository.AeviTableDescription.isVisible(header);

        } while(!isVisible);

        props.cellId = columnIndex;
        props.click = true;

		this.AeviPager.moveTo(props);
	}

    select(cell: HTMLElement): void {
        this.unSelectAllRowsAndCells();
        this.addSelectedCellClasses(cell);
        this.checkRowTinge();
    }

    unSelectAllRowsAndCells(): void {
        var elems = this.AeviDOM.getSelectedRowsAndCells();

        for(var i = 0; i < elems.length; i++) {
            for(var j = 0; j < this.AeviConsts.selectedCellClassesToRemove.length; j++) {
                AeviDOM.removeClass(elems[i], this.AeviConsts.selectedCellClassesToRemove[j]);
            }
        }
    }

    addSelectedCellClasses(cell: HTMLElement) {
        for(var i = 0; i < this.AeviConsts.selectedCellClasses.length; i++) {
            AeviDOM.addClass(cell, this.AeviConsts.selectedCellClasses[i]);
        }
    }

    checkRowTinge(): void {
        if(this.AeviDataRepository.isMoreCellsSelected())
            return;

        var selectedCellPosition: IAeviCellIndex = this.AeviDataRepository.getSelectedCellPosition();

        if(_.isNull(selectedCellPosition))
            return;

        var row: HTMLTableRowElement = this.AeviDOM.getPureRow(selectedCellPosition.rowIndex);

        if (!_.isNull(row))
            AeviDOM.addClass(row, 'tinge');
    }

	multiSelect(fromCellIndex: IAeviCellIndex, toCellIndex: IAeviCellIndex): void {
		var upToDown: boolean = (fromCellIndex.rowIndex < toCellIndex.rowIndex);
		var helper: number = null;

		if (!upToDown) {
			helper = fromCellIndex.rowIndex;
			fromCellIndex.rowIndex = toCellIndex.rowIndex;
			toCellIndex.rowIndex = helper;
		}

		var leftToRight: boolean = (fromCellIndex.cellIndex < toCellIndex.cellIndex);

		if (!leftToRight) {
			helper = fromCellIndex.cellIndex;
			fromCellIndex.cellIndex = toCellIndex.cellIndex;
			toCellIndex.cellIndex = helper;
		}

		this.AeviDataService.AeviDataRepository.AeviTableData.setSelectedPositions(fromCellIndex, toCellIndex);
        this.AeviFakeEditor.setContent();
        this.AeviFakeEditor.focus();

		/**
		 * one cell
		 */
		if (JSON.stringify(fromCellIndex) === JSON.stringify(toCellIndex))
			return;

		var table: any = <HTMLTableElement>document.getElementById(this.tableId);
		var tableBody = table.querySelector('table tbody');

		this.unSelectAllRowsAndCells();

		for (var j = fromCellIndex.rowIndex; j <= toCellIndex.rowIndex; j++) {
			for (var i = fromCellIndex.cellIndex; i <= toCellIndex.cellIndex; i++) {
				if (j < 200) {					
					var row = tableBody.rows[j];
					row.cells[i].classList.add('selected');
				}
			}
		}

		this.AeviGridEditor.showBorder(fromCellIndex, toCellIndex);
	}

	showLockMessage() {
		if (!aeviIsUndefinedOrNull(this.AeviSearch) && this.AeviSearch.isSearchVisible)
			return;

		if (!aeviIsUndefinedOrNull(this.lockMessageShowed) && this.lockMessageShowed)
			return;

		if (aeviIsUndefinedOrNull(this.AeviLockMessage))
			this.AeviLockMessage = new AeviLockMessage(this);

		if (!this.AeviDOM.isModalVisible())
			this.AeviLockMessage.show();
	}

	isLockMessageShowed() {
		if (aeviIsUndefinedOrNull(this.AeviLockMessage))
			return false;
		else
			return this.AeviLockMessage.isLockMessageShowed();
	}

	showMaxRowCountMessage() {
		var message = this.AeviLocalization.translate('max_row_count_1');
		message += this.AeviDataService.AeviDataRepository.maxRowCount;
		message += this.AeviLocalization.translate('max_row_count_2');

		var modal = new AeviModal(this);

		modal.setContent(<IAeviModalContent>{
			title: this.AeviLocalization.translate('warning'),
			text: '<p>' + message + '</p>'
		});

		modal.show();
	}

	selectRow(rowIndexes: number[]): void {
		this.unSelectAllRowsAndCells();
		this.AeviDataRepository.AeviTableData.setSelectedRows(rowIndexes, false);

		this.isAllRowsSelected = this.AeviDataRepository.AeviTableData.getVisibleRecordsLength() === rowIndexes.length;

		for (var i = 0; i < rowIndexes.length; i++) {
			var row = this.AeviDOM.getPureRow(rowIndexes[i]);

			/**
			 * IN DOM SELECT ONLY RENDERED ROWS
			 */
			if (!aeviIsUndefinedOrNull(row)) {
				row.classList.add('selected');
				var rowNumber = row.querySelector('.aeviRowNumber');
				var rowCells = row.querySelectorAll('.aeviCell');

				rowNumber.classList.add('selected');
				for (var j = 0; j < rowCells.length; j++)
					rowCells[j].classList.add('selected');
			}
		}
	}

	selectColumn(columnIndexes: number[], selectAllCells: boolean): void {
		this.unSelectAllRowsAndCells();

		if(selectAllCells === true)
			this.AeviDataRepository.AeviTableData.setSelectedRows([0], selectAllCells);
		else
			this.AeviDataRepository.AeviTableData.setSelectedColumns(columnIndexes);

		this.AeviGridEditor.refreshData();
	}
}
