﻿/// <reference path='../references.ts' />

class AeviImage {
    static AeviGrid: any;
    originalImageData: IAeviImageData;
    cell: HTMLElement;

    constructor(public AeviGrid: any, public imageEditor: IAeviEditor, public cellIndexes: IAeviCellIndex, public allowImageChange: boolean) {
        this.originalImageData = _.clone(this.getImageData());
        this.cell = this.AeviGrid.AeviDOM.getPureCell(cellIndexes.cellIndex, cellIndexes.rowIndex);
    }

    static init(aeviGrid: any) {
        AeviImage.AeviGrid = aeviGrid;
    }

    render() {
        var imageData: IAeviImageData = this.getImageData();
        this.AeviGrid.croppingMode = false;

        var modal = new AeviModal(this.AeviGrid);
        var modalTextContent: string = this.getModalContent(imageData);
        modal.setContent(<IAeviModalContent>{ text: modalTextContent });
        modal.setOptions(<IAeviModalOptions>{ minWidth: 630, minHeight: 500 });
        modal.show(() => {

            AeviImage.imageDataChanged(imageData);

            $(document).off('click', AeviConsts.imageModalUpload).on('click', AeviConsts.imageModalUpload, () => {
                $(AeviConsts.imageModalFile).click();
            });

            $(document).off('dragenter dragover', AeviConsts.imageModalUpload).on('dragenter dragover', AeviConsts.imageModalUpload, (event) => {
                event.preventDefault();
                event.stopPropagation();
            });

            $(document).off('drop', AeviConsts.imageModalUpload).on('drop', AeviConsts.imageModalUpload, (event) => {
                event.preventDefault();
                event.stopPropagation();

                var dropEvent = event.originalEvent || window.event;
                var files = dropEvent.dataTransfer.files;

                if (files) {
                    this.uploadFile(files[0]);
                }
            });

            $(document).off('change', AeviConsts.imageModalFile).on('change', AeviConsts.imageModalFile, (event) => {
                this.uploadFile(event.target.files[0]);
            });

            $(document).off('click', AeviConsts.imageModalSaveAndClose).on('click', AeviConsts.imageModalSaveAndClose, () => {
                this.save(imageData);
                $(document).trigger(AeviConsts.imageModalChangeEvent, { type: 'saveAndClose', data: imageData });
            });

            $(document).off('click', AeviConsts.imageModalDelete).on('click', AeviConsts.imageModalDelete, () => {
                this.AeviGrid.AeviDOM.closeModals();

                var modal = new AeviModal(AeviImage.AeviGrid, (confirm) => {
                    if (confirm) {
                        this.deleteImage(imageData);
                        $(document).trigger(AeviConsts.imageModalChangeEvent, { type: 'delete' });
                    } else {
                        this.reopen();
                    }
                });

                modal.setContent({
                    title : AeviImage.getLocalizedText('warning'),
                    text : '<p>' + AeviImage.getLocalizedText('image_delete_message') + '</p>',
                    buttons : [
                        'aeviModalAccept',
                        'aeviModalDenied'
                    ]
                });

                modal.show();
                modal.focus('last');
            });

            $(document).off('click', AeviConsts.imageModalCloseWithoutClose).on('click', AeviConsts.imageModalCloseWithoutClose, () => {
                $('#aeviImage').cropper('destroy');
                this.setInitialValue();
                this.AeviGrid.AeviDOM.closeModals();
                $(document).trigger(AeviConsts.imageModalChangeEvent, { type: 'close' });
            });

            $(document).off('click', AeviConsts.imageModalClose).on('click', AeviConsts.imageModalClose, () => {
                if(AeviImage.AeviGrid.AeviDOM.length('.cropper-container')) {
                    var close = confirm(AeviImage.getLocalizedText('image_close_unsaved'));
                    if(close) {
                        $('#aeviImage').cropper('destroy');
                        $(document).trigger('aeviImageClose');
                    }
                }else {
                    $('#aeviImage').cropper('destroy');
                    AeviImage.AeviGrid.AeviDOM.closeModals();
                }
            });

        }, () => {
            if(this.AeviGrid.AeviDOM.length('.cropper-container')) {

            }else {
                $('#aeviImage').cropper('destroy');
                this.AeviGrid.AeviDOM.closeModals();
            }
        });

        $(document).off('click', AeviConsts.imageModalEdit).on('click', AeviConsts.imageModalEdit, (event) => {
            event.preventDefault();
            this.callImageCropper();
            this.imageEditor.isDataValueChanged = true;
            $(event.currentTarget).detach();
        });
    }

    getImageData(): IAeviImageData {
        var imageData = this.AeviGrid.AeviDataRepository.AeviTableData.getRecordCellValue(this.cellIndexes.rowIndex, this.cellIndexes.cellIndex);

        if (imageData.HasValue && (imageData.Value === null || imageData.Value.length === 0)) {
            var guid = this.AeviGrid.AeviDataRepository.AeviTableData.getGuidByRowIndex(this.cellIndexes.rowIndex);
            var newImageData = this.AeviGrid.AeviDataService.getImageData(guid, this.cellIndexes.cellIndex);

            if(!aeviIsUndefinedOrNull(newImageData)) {
                this.AeviGrid.AeviDataRepository.AeviTableData.updateRecordCellValue(this.cellIndexes.rowIndex, this.cellIndexes.cellIndex, newImageData);
                imageData = newImageData;
            }
        }

        return imageData;
    }

    getModalContent(imageData: IAeviImageData): string {
       var HTML: string = '<div class="aeviImageModal">' +
            '<div class="image">' +
                '<div class="upload ' + AeviConsts.imageModalUpload.makeClass() + '">' + AeviImage.getLocalizedText('image_drag') + '</div>' +
                    '<img id="aeviImage" src="data:image/' + AeviImage.getImageFormat() +';base64,' + imageData.Value + '">' +
                '</div>' +
                '<div class="buttons">' +
                    '<input type="file" class="file ' + AeviConsts.imageModalFile.makeClass() + '" accept="image/png, image/' + AeviImage.getImageFormat() + '" style="display:none;">' +
                '</div>' +
            '</div>';

        if(imageData.HasValue && this.allowImageChange)
            HTML += '<input type="button" class="aeviButton aeviImageEdit ' + AeviConsts.imageModalEdit.makeClass() + '" id="aeviImageEdit" value="' + AeviImage.getLocalizedText('image_edit') + '">';

        if(this.allowImageChange)
            HTML += '<input type="button" class="aeviButton pull-left delete ' + AeviConsts.imageModalDelete.makeClass() + '" value="' + AeviImage.getLocalizedText('image_delete') + '">';

        HTML += '<input type="button" class="aeviButton aeviButton--secondary pull-right saveAndClose ' + AeviConsts.imageModalSaveAndClose.makeClass() + '" value="' + AeviImage.getLocalizedText('save_and_close') +'">';

        if(this.allowImageChange)
            HTML += '<input type="button" class="aeviButton aeviButton--black pull-right ' + AeviConsts.imageModalCloseWithoutClose.makeClass() + '" value="' + AeviImage.getLocalizedText('close_without_save') +'">';

        HTML += '</div>' +
                '<a class="aeviImageModal__close"></a>' +
                '</div>';

        return HTML;
    }

    listenCropButtons(cropElement): void {
        $(document).off('click', '.cropZoom').on('click', '.cropZoom', (event) => {
            var zoomValue = 0.1;
            var element = <HTMLElement>event.currentTarget;

            var action: string = element.getAttribute('data-action');

            if(action === 'unzoom')
                zoomValue = zoomValue * -1;

            cropElement.cropper('zoom', zoomValue);
        });
    }

    static imageDataChanged(imageData: IAeviImageData) {
        if (imageData.HasValue) {
            $(AeviConsts.imageModalUpload).hide();
            $('.aeviImageModal img').attr('src', 'data:image/' + AeviImage.getImageFormat() + ';base64,' + imageData.Value).show();
            $(AeviConsts.imageModalDelete).show();
            $(AeviConsts.imageModalEdit).show();
        }
        else {
            $('.aeviImageModal img').hide().attr('src', '');
            $(AeviConsts.imageModalDelete).hide();
            $(AeviConsts.imageModalEdit).hide();
            $(AeviConsts.imageModalUpload).show();
        }
    }

    deleteImage(imageData: IAeviImageData): any {
        this.clearImage();

        if (imageData.Changed) {
            this.imageEditor.isDataValueChanged = true;
        }

        $('#aeviImage').cropper('destroy');
        this.refreshCell();
        this.reopen();
    }

    save(imageData: IAeviImageData) {

        if (imageData.Changed) {
            this.imageEditor.isDataValueChanged = true;
            this.refreshCell();
        }

        if (this.AeviGrid.croppingMode) {
            this.cropImage(imageData);
        }

        $('#aeviImage').cropper('destroy');
        this.AeviGrid.AeviDOM.closeModals();

        if (imageData.Changed) {
            this.AeviGrid.AeviGridHandler.cellChange(this.AeviGrid.AeviDOM.getCell(this.cellIndexes.cellIndex, this.cellIndexes.rowIndex));
            this.AeviGrid.AeviGridHandler.selectedCellEditor.isDataValueChanged = true;
            this.AeviGrid.AeviGridHandler.triggerClick();
        }
    }

    reopen() {
        setTimeout(() => this.imageEditor.renderImageWindow());
    }

    cropImage(imageData: IAeviImageData) {
        var cropElement = $('#aeviImage');

        var croppedCanvas = cropElement.cropper('getCroppedCanvas', {height: 200});
        var croppedImage = AeviImage.convertToImage(croppedCanvas);

        cropElement.cropper('replace', croppedImage);

        imageData.Value = AeviImage.getImageDataWithoutPrefix(croppedImage);
        imageData.Changed = true;

        this.AeviGrid.croppingMode = false;
        AeviImage.imageDataChanged(imageData);
    }

    uploadFile(file) {
        this.readFile(file, () => {
            setTimeout(() => {
                this.callImageCropper();
            }, 200);
        });
    }

    refreshCell() {
        if (this.AeviGrid.AeviDOM.isFormVisible()) {
            return;
        }

        this.AeviGrid.AeviGridEditor.refreshRow(<IAeviRecordIdentification>{rowIndex: this.cellIndexes.rowIndex});
        this.AeviGrid.select(this.cell);
    }

    readFile(file, done) {
        var reader = new FileReader();

        reader.onload = () => {
            var imageData = this.getImageData();
            imageData.HasValue = true;
            imageData.Value = reader.result.substring(reader.result.indexOf(',') + 1);
            imageData.Changed = true;
            AeviImage.imageDataChanged(imageData);
            $(document).trigger(AeviConsts.imageModalChangeEvent, { type: 'upload', data: imageData });
        };

        if(aeviIsUndefinedOrNull(file))
            return;

        reader.readAsDataURL(file);

        done(true);
    }

    setInitialValue() {
        var imageData: IAeviImageData = this.getImageData();

        imageData.HasValue = this.originalImageData.HasValue;
        imageData.Value =  this.originalImageData.Value;
        imageData.Changed =  this.originalImageData.Changed;

        AeviImage.imageDataChanged(imageData);
    }

    clearImage() {
        var imageData: IAeviImageData = this.getImageData();

        if (imageData) {
            imageData.HasValue = false;
            imageData.Value = null;
            imageData.Changed = true;

            AeviImage.imageDataChanged(imageData);
        }
    }

    callImageCropper() {
        var cropElement = $('#aeviImage');
        cropElement.cropper('destroy');

        this.AeviGrid.croppingMode = true;

        cropElement.cropper({
            aspectRatio : 2 / 3,
            autoCropArea : 1,
            strict : false,
            guides : false,
            background : true,
            dragCrop : false,
            cropBoxMovable : false,
            cropBoxResizable : false,
            doubleClickToggle : false
        });

        if(!$('.cropButtons').length) {
            var zoomingButtons = '<span class="cropButtons pull-left" style="margin-left: 7px;">' +
                '<span class="aeviButton cropZoom zoomin" data-action="zoom"></span>' +
                '<span class="aeviButton cropZoom zoomout" data-action="unzoom"></span>' +
                '</span>';

            $(zoomingButtons).insertAfter('.simplemodal-data .aeviButton.delete');

            this.listenCropButtons(cropElement);
        }
    }

    static getDefaultValue(): IAeviImageData {
        return {
            HasValue: false,
            Changed: false,
            Value: null
        };
    }

    static getDefaultBlockedValue(): IAeviImageData {
        return {
            HasValue: false,
            Changed: false,
            Value: 'blocked'
        };
    }

    static getDisplayValue(imageData) {
        var result = null;
        var className = 'imagePresent';
        var text = AeviImage.getLocalizedText('image_present');

        if(imageData) {
            if(!imageData.HasValue) {
                className = 'imageMissing';
                text = AeviImage.getLocalizedText('image_missing');
            }

            if(imageData.Value === 'blocked') {
                className = 'imageBlocked';
                text = AeviImage.getLocalizedText('image_blocked');
            }

            result = '<a class="aeviCellImage ' + className + '">' + text + '</a>';
        }

        return result;
    }

    static getDataValue() {
        return null;
    }

    static getLocalizedText(code: string): string {
        return AeviImage.AeviGrid.AeviLocalization.translate(code);
    }

    static getImageFormat() {
        return AeviImage.AeviGrid.AeviConsts.imageFormat;
    }

    static convertToImage(canvas) {
        canvas = AeviImage.convertTransparentPixelsToWhitePixels(canvas);
        return canvas.toDataURL('image/' + AeviImage.getImageFormat());
    }

    static convertTransparentPixelsToWhitePixels(canvas) {
        var ctx = canvas.getContext('2d');
        var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        var data = imgData.data;

        for (var i = 0; i < data.length; i += 4){
            if (data[i + 3] < 255) {
                data[i] = 255;
                data[i + 1] = 255;
                data[i + 2] = 255;
                data[i + 3] = 255;
            }
        }

        ctx.putImageData(imgData, 0, 0);

        return canvas;
    }

    static getImageDataWithoutPrefix(url: string): string {
        return url.replace('data:image/' + AeviImage.getImageFormat() + ';base64,', '').replace('data:image/png;base64,', '');
    }
}
