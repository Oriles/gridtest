/// <reference path='../references.ts' />

class AeviContextMenu {
	AeviGrid: any;
	contextMenuType: string;
	AeviDataRepository: IAeviDataRepository;
	handler: any;
	contextMenuItems: any;
	renderedItems: any;
	contextMenu: JQuery;
	isContextMenuEmpty: boolean;

	constructor(aeviGrid: any, contextMenuType: string) {
		this.contextMenuType = contextMenuType;
		this.AeviGrid = aeviGrid;
		this.AeviDataRepository = this.AeviGrid.AeviDataRepository;
		this.isContextMenuEmpty = false;

		this.initializeItems();

		if(this.AeviGrid.AeviDOM.isContextMenuVisible())
			$('.aeviContextMenu').hide('fade', 300);

		this.render();

		if (!this.handler) {
			this.handler = new AeviContextMenuHandler(this.AeviGrid, this);
		}
	}

	initializeItems(): void {
		this.contextMenuItems = {
			'insertRowHtml': '<span class="aeviContextMenu__addRow"><%= insertRow %></span>',
			'deleteRowHtml': '<span class="aeviContextMenu__deleteRow"><%= deleteRow %></span>',
			'cut': '<span class="aeviContextMenu__cutCell"><%= cut %></span>',
			'copy': '<span class="aeviContextMenu__copyCell"><%= copy %></span>',
			'insert': '<span class="aeviContextMenu__insertCell"><%= paste %></span>'
		};
	}

	getItems(keys): string {
		var HTML = '';
		var arrayOfKeys = keys.split('|');
		this.renderedItems = [];

		for (var i = 0; i < arrayOfKeys.length; i++) {
			var value = this.contextMenuItems[arrayOfKeys[i]];

			if (!_.isUndefined(value) && value)
				HTML += value;
		}

		this.renderedItems = arrayOfKeys;

		return HTML;
	}

	checkAndGetRowNumberTools(): string {
		if (!aeviIsUndefinedOrNull(this.AeviGrid.mode) && !aeviIsUndefinedOrNull(this.AeviGrid.mode.clipboard) && this.AeviGrid.mode.clipboard === true)
			return '';

		if (!_.isNull(this.AeviDataRepository.AeviTableDescription.fixedSize) || this.AeviDataRepository.AeviTableDescription.readOnly)
			return '';

        var tools: string = '';

        if (this.AeviGrid.AeviDataService.AeviUser.roles.Create) {
            tools += this.getItems('insertRowHtml|');
        }

        if (this.AeviGrid.AeviDataService.AeviUser.roles.Delete) {
            tools += this.getItems('deleteRowHtml|');
        }

        return tools;
	}

	render(): void {
		var items = '';

		switch (this.contextMenuType) {
			case 'rowNumber':
				items = this.checkAndGetRowNumberTools();
				break;

			default:
				break;
		}

		if (items === '')
			this.isContextMenuEmpty = true;

		var itemsTemplate = _.template(items);

		var template = _.template(
			'<div class="aeviContextMenu aeviContextMenu-<%= type %>">' +
			'<div class="aeviContextMenu__inner">' +
			'<%= items %>' +
			'</div>' +
			'</div>'
		);

		$('html').append(
			template({
				type: this.contextMenuType,
				items: itemsTemplate({
					insertRow: this.AeviGrid.AeviLocalization.translate('insert_row'),
					deleteRow: this.AeviGrid.AeviLocalization.translate('delete_row'),
					copy: this.AeviGrid.AeviLocalization.translate('toolbar_copy'),
				})
			})
		);

		this.contextMenu = $('.aeviContextMenu-' + this.contextMenuType);
	}

	refreshItems() {
		var items = null;

		switch (this.contextMenuType) {
			case 'rowNumber':
				items = this.checkAndGetRowNumberTools();
				break;

			default:
				break;
		}
	}

	showMenu(event): void {
		if (!this.isContextMenuEmpty)
			this.handler.showMenu(event);
	}

	hideMenu(): void {
		this.handler.hideMenu();
	}
}