/// <reference path='../../references.ts' />

class AeviSearchHandler {
	AeviGrid: any;
	AeviSearch: any;
	AeviConsts: any;

	constructor(aeviGrid: any, aeviSearch: any) {
		this.AeviGrid = aeviGrid;
		this.AeviSearch = aeviSearch;
		this.AeviConsts = this.AeviGrid.AeviConsts;

		this.setSearchAsDraggable();
		this.listener();
	}

	setSearchAsDraggable() {
		$('.' + this.AeviSearch.aeviSearch).draggable({
			start: function(event) { }
		});
	}

	focusField() {
		$(this.AeviConsts.search.selector.field).focus();
	}

	listener() {
		$(document).off(AeviConsts.clickEvent, this.AeviConsts.search.selector.find).on('click', this.AeviConsts.search.selector.find, (event) => {
			event.preventDefault();
            this.AeviSearch.find(AeviSearchHandler.getKeyWord(this.AeviConsts), AeviSearchHandler.getDirection(this.AeviConsts));
		});

		$(document).on(AeviConsts.clickEvent, this.AeviConsts.search.selector.cancel, (event) => {
			event.preventDefault();
			this.AeviSearch.hide();
		});

		$(document).on(AeviConsts.keyDownEvent, this.AeviConsts.search.selector.field, (event) => {
			var code: number = event.keyCode || event.which;			
			var codeKey: string = this.AeviConsts.keys[code];

            if (codeKey === 'enter') {
                event.preventDefault();
                this.AeviSearch.find(AeviSearchHandler.getKeyWord(this.AeviConsts), AeviSearchHandler.getDirection(this.AeviConsts));
			}

			if (codeKey === 'escape') {
				event.preventDefault();
				this.AeviSearch.hide();
			}
		});
	}

    static getKeyWord(consts) {
        var el = <HTMLInputElement>document.querySelector(consts.search.selector.field);
        return el.value;
    }

    static getDirection(consts) {
        var el = <HTMLInputElement>document.querySelector(consts.search.selector.direction + ':checked');
        return el.value;
    }
}
