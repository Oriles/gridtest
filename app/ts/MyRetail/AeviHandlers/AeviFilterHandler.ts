/// <reference path='../../references.ts' />

class AeviFilterHandler {
	AeviFilter: any;
	AeviGrid: any;

	constructor(aeviFilter: any) {
		this.AeviFilter = aeviFilter;
		this.AeviGrid = this.AeviFilter.AeviGrid;
		this.listener();
	}

	listener() {
		var selectors = ['.aeviToolbar__numberfilter', '.aeviToolbar__textfilter'].join(', ');

        $(document).on('blur', selectors, (event) => {
        	var editor = <JQuery>$(event.currentTarget);
        	var val = editor.val();
        	var columnName = editor[0].getAttribute('data-column');
        	this.AeviFilter.setFilterValue(columnName, val);
        });
	}
}
