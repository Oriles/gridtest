/// <reference path='../../references.ts' />
/// <reference path='../AeviHelp.ts' />

class AeviHelpHandler {
	AeviGrid: any;
	AeviHelp: any;

	constructor(grid, help) {
		this.AeviGrid = grid;
		this.AeviHelp = help;
		this.listener();	
	}

	listener() {
		$(document).on('click', '.aeviHelp__close', (event) => {
			event.preventDefault();
			this.AeviHelp.hide();
		})
	}
}
