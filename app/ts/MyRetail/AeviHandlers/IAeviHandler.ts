/// <reference path='../../references.ts' />

interface IAeviHandler {
	AeviGrid: any;
	
    listener(...args: any[]): void;
}
