/// <reference path='../../references.ts' />

class AeviHeaderColumnHandler {
	AeviGrid: any;
	ctrlHold: boolean;
	shiftHold: boolean;
	columnIndexes: number[];
	aeviColumnNumber: string;

	mouseDown: boolean;
	mouseDownCell: JQuery;

	range: any[];

	firstClick: any;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.ctrlHold = false;
		this.shiftHold = false;
		this.columnIndexes = [];
		this.aeviColumnNumber = '.aeviColumn';

		this.listener();
	}

	/**
	 * TODO!
	 * hardstring
	 */
	listener() {
		$(document).off('mousedown', this.AeviGrid.AeviConsts.columnSelector).on('mousedown', this.AeviGrid.AeviConsts.columnSelector, (event) => {
			this.mouseDown = true;
			this.mouseDownCell = $(event.currentTarget);

            this.AeviGrid.AeviGridHandler.triggerClick();

			if (this.mouseDownCell.hasClass('selected')) {
				this.mouseDownCell.removeClass('selected');
				this.unSelect();
			} else {
				this.click(this.mouseDownCell);
				this.mouseDownCell.addClass('selected');
			}
		});

		$(document).off('mouseup', this.AeviGrid.AeviConsts.columnSelector).on('mouseup', this.AeviGrid.AeviConsts.columnSelector, () => {
			this.mouseDown = false;
		});

		$(document).off('mouseover', this.AeviGrid.AeviConsts.columnSelector).on('mouseover', this.AeviGrid.AeviConsts.columnSelector, () => {
			if (this.mouseDown && this.mouseDownCell.length) {
				var from = this.mouseDownCell.index();
				var to = $(this).index();
				var range = [];

				if (!this.ctrlHold) {
					if (from >= to) {
						var helper = from;
						from = to;
						to = helper;
						range = _.range(from, to + 1);
					} else {
						range = _.range(from, to + 1);
					}
				} else {
					range = this.range;
				}

				this.AeviGrid.selectColumn(range);
				this.range = range;
			}
		});

		$(document).on('keydown', (event) => {
			var code = event.keyCode || event.which;

            if (code === 17 || this.AeviGrid.AeviClientSide.isMac())
                this.ctrlHold = true;

			this.shiftHold = (code === 16);
		});

		$(document).on('keyup', () => {
			this.ctrlHold = false;
			this.shiftHold = false;
		});

		/**
		 * TODO!
		 * hardstring
		 */
		$(document).on('mousedown touchstart', this.AeviGrid.AeviConsts.columnSelector + ' .aeviSort', (event) => {
			var sortButton = $(event.currentTarget);
			var sortButtons = $(this.AeviGrid.AeviConsts.sortButtonSelector);

			this.AeviGrid.AeviGridHandler.triggerClick();

			sortButtons.removeClass('active').addClass('default');
			sortButton.addClass('active').removeClass('default');

            var classList = null;
			var defaultOrder = true;

			if (sortButton.hasClass('sortUp'))
				classList = 'sortUp';

			if (sortButton.hasClass('sortDown'))
				classList = 'sortDown';

			if (sortButton.hasClass('default'))
				classList = 'default';

			sortButtons.removeClass('sortUp sortDown');
			var sortDirection = null;

			switch (classList) {
				case null:
					sortDirection = 'up';
					sortButton.addClass('sortUp');
					defaultOrder = false;
					break;

				case 'sortUp':
					sortDirection = 'down';
					sortButton.removeClass('sortUp').addClass('sortDown');
					defaultOrder = false;
					break;

				case 'sortDown':
					sortDirection = 'default';
					sortButton.removeClass('sortDown').addClass('default');
					defaultOrder = true;
					break;
			}

            var sortColumnIndex: number = parseInt(sortButton.parent()[0].getAttribute('id').split('-')[1]);

            if (defaultOrder) {
                sortColumnIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnIndexByName('DefaultOrder');
            }

            this.sort(sortDirection, sortColumnIndex);

			return false;
		});

		$(document).on('click', this.AeviGrid.AeviConsts.aeviSelectAllSelector, (event) => {
            var button: JQuery = $(event.currentTarget);

            if (button.hasClass('isSelected')) {
                button.removeClass('isSelected');
                this.AeviGrid.unSelectAllRowsAndCells();
                this.AeviGrid.AeviDataRepository.AeviTableData.removeSelectedPositions();
            } else {
                this.AeviGrid.AeviGridHandler.triggerClick();
                this.AeviGrid.selectRow(_.range(0, this.AeviGrid.AeviDataRepository.getVisibleRecordsLength()));
                button.addClass('isSelected');
            }
		});
	}

	sort(sortDirection: string, sortBy: number) {
		this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableData.sort(sortDirection, sortBy);
		this.AeviGrid.AeviGridEditor.refreshData();
	}

	unSort() {
		$(this.AeviGrid.AeviConsts.columnSelector + this.AeviGrid.AeviConsts.aeviSelectAllSelector).removeClass('active sortDown sortUp').addClass('default');
	}

	unSelect() {
		this.AeviGrid.selectColumn([], false);
	}

    /**
     * todo!
     */
	click(columnElement: JQuery): void {
		var columnIndex = AeviDOM.getColumnIndex(columnElement);

		if (this.range) {
			$.each(this.range, (i, el) => {
				if ($.inArray(el, this.columnIndexes) === -1) this.columnIndexes.push(this.range[i]);
			});
		}

		/**
		 * If user doesn't hold ctrl clear array of selected rows
		 */
		if (!this.ctrlHold && this.columnIndexes !== undefined) { this.columnIndexes.length = 0; }

		/**
		 * If user hold shift
		 */
		if (this.shiftHold) {

			/**
			 * If user clicked on different row target range(from,to)
			 */
			if (this.firstClick !== undefined && this.firstClick != columnIndex) {
				var direction = (this.firstClick > columnIndex) ? 1 : 0;
				var from = 0;
				var to = 0;
				var helper = null;

				if (!direction) {
					from = this.firstClick;
					to = columnIndex;
				} else {
					from = columnIndex;
					to = helper;
				}

				this.columnIndexes = _.range(from, to + 1);
			} else {
				this.columnIndexes.push(columnIndex);
			}
		} else {
			this.columnIndexes.push(columnIndex);

			if (columnElement.hasClass('selected')) {
				this.columnIndexes = _.without(this.columnIndexes, columnIndex);
			}

			this.firstClick = columnIndex;
		}

		this.range = this.columnIndexes;
		this.AeviGrid.selectColumn(this.columnIndexes);
	}
}
