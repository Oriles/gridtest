/// <reference path='../../references.ts' />
/// <reference path='../AeviHelp.ts' />

class AeviModalHandler {
	AeviGrid: any;
	modal: any;
	modalWindow: any;

	constructor(aeviGrid: any, modal, modalWindow) {
		this.AeviGrid = aeviGrid;
		this.modal = modal;
		this.modalWindow = modalWindow;
        this.listener();
	}

	listener() {
		$(document).off('click', '#aeviDataServiceCommitTemp').on('click', '#aeviDataServiceCommitTemp', (event) => {
			this.AeviGrid.AeviDataService.isServerDataInvalid = false;
			this.commit(event, false);
		});

		$(document).off('click', '#aeviDataServiceCommitTempAndClose').on('click', '#aeviDataServiceCommitTempAndClose', (event) => {
			this.AeviGrid.AeviDataService.isServerDataInvalid = false;
			this.commit(event, true);
		});

		$(document).off('click', '#aeviDataServiceDeleteTemp').on('click', '#aeviDataServiceDeleteTemp', (event) => {
			this.AeviGrid.AeviDataService.isServerDataInvalid = false;
			this.deleteTemp(event, false);
		});

		$(document).off('click', '#aeviDataServiceDeleteTempAndClose').on('click', '#aeviDataServiceDeleteTempAndClose', (event) => {
			this.AeviGrid.AeviDataService.isServerDataInvalid = false;
			this.deleteTemp(event, true);
		});

		$(document).off('click', '#aeviDataServiceDeleteTempIgnoreAndClose').on('click', '#aeviDataServiceDeleteTempIgnoreAndClose', (event) => {
			this.AeviGrid.AeviDataService.isServerDataInvalid = false;
			this.deleteTemp(event, false);
		});

		$(document).off('click', '#aeviDataServiceClose').on('click', '#aeviDataServiceClose', (event) => {
			event.preventDefault();
			this.AeviGrid.AeviDataService.showData();
            this.modal.close();
		});

		$(document).off('click', '#aeviDataServiceDiscard').on('click', '#aeviDataServiceDiscard', (event) => {
            this.discardTemp(event);
		});
	}

	discardTemp(event) {
		event.preventDefault();

		this.AeviGrid.mode.clipboard = 0;

		this.AeviGrid.AeviDataRepository.enableOrDisableImageColumns('enable');

		this.AeviGrid.AeviDataService.getDataByQuery('', true).done(() => {
            this.AeviGrid.AeviDataService.isServerDataInvalid = false;
			this.AeviGrid.AeviGridLocker.unLock();
            this.AeviGrid.AeviStatusBar.info(this.AeviGrid.AeviLocalization.translate('discard_done_message'));
		});

        this.modal.close();
	}

	deleteTemp(event, close) {
		event.preventDefault();
		var JSONFilter = '';

		this.AeviGrid.mode.clipboard = 0;

		if (!aeviIsUndefinedOrNull(this.AeviGrid.AeviFilter))
			JSONFilter = encodeURIComponent(this.AeviGrid.AeviFilter.getJSONFilterValues());

		this.AeviGrid.AeviDataService.getDataByQuery(JSONFilter, true);

		if (close)
            this.modal.close();
	}

	commit(event, close) {
		event.preventDefault();

        var JSONFilter = '';

		if (!aeviIsUndefinedOrNull(this.AeviGrid.AeviFilter)) {
			JSONFilter = encodeURIComponent(this.AeviGrid.AeviFilter.getJSONFilterValues());
        }

		this.AeviGrid.AeviDataService.commit(JSONFilter, true).done(() => {
			if (close) {
				this.modalWindow.close();
			}
		});
	}
}
