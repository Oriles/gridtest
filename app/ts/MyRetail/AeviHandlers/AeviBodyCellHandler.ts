/// <reference path='../../references.ts' />
/// <reference path='IAeviHandler.ts' />

/**
 * TODO!
 * # columns length checking in tab and shifttab
 * # check touch events
 */
class AeviBodyCellHandler implements IAeviHandler {
	AeviGrid: any;
	AeviGridHandler;

	mouseDown: boolean;
	mouseDownCell: JQuery;

	firstCellIndex: number;
	firstRowIndex: number;

	lastCellIndex: number;
	lastRowIndex: number;

	touchElement: HTMLElement;

	constructor(aeviGrid: any, aeviGridHandler: any) {
		this.AeviGrid = aeviGrid;
		this.AeviGridHandler = aeviGridHandler;
		this.listener();
	}

	listener(): void {
		$(document).off('touchstart', this.AeviGrid.AeviConsts.cellSelector).on('touchstart', this.AeviGrid.AeviConsts.cellSelector, (event) => {
			this.touchElement = <HTMLElement>event.currentTarget;
		});

		$(document).off('touchmove', this.AeviGrid.AeviConsts.cellSelector).on('touchmove', this.AeviGrid.AeviConsts.cellSelector, (event) => {
			this.touchElement = null;
		});

		$(document).off('touchend', this.AeviGrid.AeviConsts.cellSelector).on('touchend', this.AeviGrid.AeviConsts.cellSelector, (event) => {
			if (this.touchElement == event.currentTarget)
				this.AeviGrid.AeviGridHandler.cellChange(event.currentTarget);

			this.touchElement = null;
		});

		$(document).off('mousedown', this.AeviGrid.AeviConsts.cellSelector).on('mousedown', this.AeviGrid.AeviConsts.cellSelector, (event) => {
			var cell = event.currentTarget;
			var $cell: JQuery = $(cell);

			if (event.button === 0 || !event.originalEvent) {
				this.mouseDown = true;
				
				this.AeviGrid.AeviGridHandler.cellChange($cell, MyRetail.IAeviChangeType.byClick);

				this.firstCellIndex = this.AeviGrid.AeviDOM.getCellIndex(cell);
				this.firstRowIndex = this.AeviGrid.AeviDOM.getCellParentIndex(cell);
				this.mouseDownCell = $cell;
			}

			if ($('.aeviContextMenu').length) {
				$('.aeviContextMenu').hide();
			}
		});

		$(document).on('mouseup', (event) => {
			this.mouseDown = false;
			this.removeNoSelection();
		});

		$(document).off('mouseover', this.AeviGrid.AeviConsts.cellSelector).on('mouseover', this.AeviGrid.AeviConsts.cellSelector, (event) => {
			if (this.mouseDown === true) {
				var cell = event.currentTarget;

				this.addNoSelection();

				this.lastCellIndex = this.AeviGrid.AeviDOM.getCellIndex(cell);
				this.lastRowIndex = this.AeviGrid.AeviDOM.getCellParentIndex(cell);

				var fromCellIndex: IAeviCellIndex = {
					rowIndex: this.firstRowIndex,
					cellIndex: this.firstCellIndex
				};

				var toCellIndex: IAeviCellIndex = {
					rowIndex: this.lastRowIndex,
					cellIndex: this.lastCellIndex
				};

				this.AeviGrid.multiSelect(fromCellIndex, toCellIndex);
			}
		});

		$(document).on(AeviConsts.keyDownEvent, (event) => {
            if (this.AeviGrid.AeviDOM.isSearchVisible()) {
                return;
            }

			var code = event.keyCode || event.which;
			var selectedCell: HTMLElement = this.AeviGrid.AeviDOM.getSelectedCell();
            var $selectedCell: JQuery = $(selectedCell);
            var isCellSelected: boolean = (!_.isNull(selectedCell));

            if (!this.AeviGrid.AeviDOM.isModalVisible()) {
				var key = this.AeviGrid.AeviConsts.keys[code];

				if (key === 'tab') {
                    if (!aeviIsUndefinedOrNull(this.AeviGrid.AeviSearch) && this.AeviGrid.AeviSearch.isSearchVisible) {
                        return;
                    }

                    if (this.AeviGrid.AeviDOM.isFormVisible()) {
                        return;
                    }

					event.preventDefault();

					if (event.shiftKey) {
						if (isCellSelected) {
                            this.shiftTab($selectedCell);
                        }
					}
					else {
						if (isCellSelected) {
                            this.tab($selectedCell);
                        }
					}
				}

                if (key === 'enter') {
					event.preventDefault();
					if (isCellSelected) {
						this.enter($selectedCell);
                    }
				}

                var isEditorInRenderMode: boolean = false;
                var isEditorExists: boolean = (!aeviIsUndefinedOrNull(this.AeviGrid.AeviGridHandler.selectedCellEditor));
                if(isEditorExists)
                    isEditorInRenderMode = (this.AeviGrid.AeviGridHandler.selectedCellEditor.getState() === 'render');

                if(isCellSelected && !isEditorInRenderMode) {

                    if(!AeviClientSide.isPasteEvent(event, key)) {
                        event.preventDefault();
                    }

                    switch(key) {
                        case 'left arrow':
                                this.leftArrow($selectedCell);
                            break;

                        case 'right arrow':
                                this.rightArrow($selectedCell);
                            break;

                        case 'up arrow':
                                this.upArrow($selectedCell);
                            break;

                        case 'down arrow':
                                this.downArrow($selectedCell);
                            break;

                        case 'home':
                                if (event.ctrlKey || event.metaKey)
                                    this.firstRowCell();
                                else
                                    this.firstCell($selectedCell);
                            break;

                        case 'end':
                                if (event.ctrlKey || event.metaKey)
                                    this.lastRowCell();
                                else
                                    this.lastCell($selectedCell);
                            break;

                        default:
                            break;
                    }
                }
			}
		});
	}

    addNoSelection(): void {
		$(this.AeviGrid.AeviConsts.toolbarSelector + ', ' + this.AeviGrid.AeviConsts.statusBarSelector).addClass(this.AeviGrid.AeviConsts.noSelectionClass);
	}

	removeNoSelection(): void {
		$(this.AeviGrid.AeviConsts.toolbarSelector + ', ' + this.AeviGrid.AeviConsts.statusBarSelector).removeClass(this.AeviGrid.AeviConsts.noSelectionClass);
	}

	tab(cellObject: JQuery) {
		var newCell: JQuery = this.AeviGrid.AeviDOM.getCellByTab(cellObject, 'tab');
		var cellIndexes: IAeviCellIndex = this.AeviGrid.AeviDOM.getCellIndexes(newCell);

        if (this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(cellIndexes.rowIndex, 'hidden'))
            return;

        this.AeviGridHandler.cellChange(newCell, MyRetail.IAeviChangeType.byKey);

        if(cellIndexes.cellIndex === this.AeviGrid.AeviDataRepository.AeviTableDescription.getRowNumberIndex()) {
            setTimeout(() => {
                this.tab(newCell);
            });
        }
	}

	shiftTab(cellObject: JQuery): void {
		var cell: JQuery = this.AeviGrid.AeviDOM.getCellByTab(cellObject, 'shiftTab');
        var cellIndexes: IAeviCellIndex = this.AeviGrid.AeviDOM.getCellIndexes(cell);

        if(cellIndexes.cellIndex === this.AeviGrid.AeviDataRepository.AeviTableDescription.getRowNumberIndex())
            return;

        this.AeviGridHandler.cellChange(cell, MyRetail.IAeviChangeType.byKey);

        if(cellIndexes.cellIndex === this.AeviGrid.AeviDataRepository.AeviTableDescription.getRowNumberIndex()) {
            setTimeout(() => {
                this.tab(cell);
            });
        }
	}

	downArrow(cellObject: JQuery): void {
		var cell = this.AeviGrid.AeviDOM.getCellByArrow(cellObject, 'down');
		var cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes(cell);

        if (this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(cellIndexes.rowIndex, 'hidden'))
            return;

		this.AeviGridHandler.cellChange(cell, MyRetail.IAeviChangeType.byKey);
	}

	enter(cellObject: JQuery): void {
		this.downArrow(cellObject);
	}

	leftArrow(cellObject: JQuery): void {
		this.shiftTab(cellObject);
	}

	rightArrow(cellObject: JQuery): void {
		this.tab(cellObject);
	}

	upArrow(cell: JQuery): void {
		var newCell = this.AeviGrid.AeviDOM.getCellByArrow(cell, 'up');
		this.AeviGridHandler.cellChange(newCell, MyRetail.IAeviChangeType.byKey);
	}

	firstCell(cell: JQuery): void {
		var rowIndex = this.AeviGrid.AeviDOM.getCellIndexes(cell).rowIndex;
		var cellIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getFirstVisibleColumn();
		this.AeviGrid.AeviPager.moveTo(<IAeviMoveToProps>{ cellId: cellIndex, rowId: rowIndex, click: true });
		this.AeviGrid.AeviPager.moveTo(<IAeviMoveToProps>{ cellId: cellIndex + 1, rowId: rowIndex, click: true });
	}

	firstRowCell(): void {
        var cellIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getFirstVisibleColumn();
        this.AeviGrid.AeviPager.moveTo(<IAeviMoveToProps>{ cellId: cellIndex, rowId: 0, click: true });
		this.AeviGrid.AeviPager.moveTo(<IAeviMoveToProps>{ cellId: cellIndex + 1, rowId: 0, click: true });
	}

	lastCell(cell: JQuery): void {
		var rowIndex = this.AeviGrid.AeviDOM.getCellIndexes(cell).rowIndex;
		var cellIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getLastVisibleColumn();

		var newCell = this.AeviGrid.AeviDOM.getCell(cellIndex, rowIndex);
		this.AeviGridHandler.cellChange(newCell);
	}

	lastRowCell(): void {
		var cellIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getLastVisibleColumn();

		var repository = this.AeviGrid.AeviDataService.AeviDataRepository;
		var firstHiddenRecordIndex = repository.AeviTableData.getFirstHiddenRecordIndex();

		// -2 is first non newrow record
		var rowIndex = firstHiddenRecordIndex - 2;

		if (_.isNull(firstHiddenRecordIndex)) {
			rowIndex = repository.getVisibleAndNewRecordsLength();
			rowIndex = rowIndex - 1;
		}

		if (rowIndex < 0)
			rowIndex = 0;

		this.AeviGrid.AeviPager.moveTo({ cellId: cellIndex, rowId: rowIndex, click: true });
	}
}
