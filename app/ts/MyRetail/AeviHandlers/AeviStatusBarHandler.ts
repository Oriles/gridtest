/// <reference path='../../references.ts' />
/// <reference path='../AeviModal/AeviModal.ts' />

class AeviStatusBarHandler {
	AeviGrid: any;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.listener();
	}

	listener() {
		$(document).on('click', '.aeviStatusBarInfo', (event) => {
			event.preventDefault();

			var modal = new AeviModal(this.AeviGrid, null);

			modal.setContent({
				text : '<p>' + this.AeviGrid.AeviLocalization.translate('grid_version') + ': <strong>' + this.AeviGrid.gridVersion + '</strong></p>' + 
					   '<p>' + this.AeviGrid.AeviLocalization.translate('api_version') + ': <strong>' + this.AeviGrid.apiVersion + '</strong></p>'
			});

			modal.show(null, null);
		})

		$(document).on('click', '.aeviStatusBarQuest', (event) => {
			event.preventDefault();
			this.AeviGrid.AeviHelp.show();
		})

		$(document).on('click', '.aeviProtected p, .aeviStatusBarItem', (event) => {
			event.preventDefault();
			$(this).parent().toggleClass('isOpened');
		})
	}
}
