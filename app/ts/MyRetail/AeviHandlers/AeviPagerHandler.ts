/// <reference path='../../references.ts' />

class AeviPagerHandler {
	AeviGrid: any;
	AeviPager: any;
	aeviConsts: any;
	canScrollUp: boolean;

	lastScrollTop: number;
	lastScrollLeft: number;

	aeviScrollElement: JQuery;
	table: JQuery;

	offsets: any;
	offsetJumps: any;
	jumps: any;
	tableTop: number;
	heightOfRow: number;
	to: any;
	from: any;
	tollerance: any;
	toTolleranceOffset: number;
	fromTolleranceOffset: number;
	numberOfNewVisibleRows: number;
	countOfNewRows: number;	

	constructor(aeviPager: any, aeviGrid: any) {
		var _this = this;
		this.AeviGrid = aeviGrid;
		this.AeviPager = aeviPager;
		this.aeviConsts = this.AeviGrid.AeviConsts;
		this.canScrollUp = false;

		this.aeviScrollElement = this.AeviPager.aeviAppElement.find(this.aeviConsts.tableBodySelector);
		this.table = this.aeviScrollElement.find('table');

		this.lastScrollTop = 0;
		this.lastScrollLeft = 0;

		this.setInitialValues();

		this.listener();
	}

	setInitialValues() {
		this.heightOfRow = this.AeviPager.rowHeight;
		this.tollerance = this.AeviPager.tollerance;
		this.numberOfNewVisibleRows = this.AeviPager.numberOfNewVisibleRows;
		this.countOfNewRows = parseInt(this.aeviConsts.pager.countOfNewRows);
		this.setJumps();
	}

	getScrollContainerOffset(element) {
		var xPosition = 0;
		var yPosition = 0;

		while (element) {
			xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
			yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
			element = element.offsetParent;
		}

		return yPosition;
	}

	scroll(aeviScrollEl, event) {
		if (this.AeviGrid.AeviDataService.AeviDataRepository.getVisibleRecordsLength() < this.numberOfNewVisibleRows)
			return false;

		/**
		 * On horizontal scroll return
		 */
		var scrollLeft = aeviScrollEl.scrollLeft();
		if (this.lastScrollLeft !== scrollLeft) {
			this.lastScrollLeft = scrollLeft;
			return;
		}

		var scrollTop = aeviScrollEl.scrollTop();
		var direction = (scrollTop > this.lastScrollTop) ? 'down' : 'up';
		this.lastScrollTop = scrollTop;

		var scrollContainerOffset = this.getScrollContainerOffset(document.getElementById('aeviScrollDiv'));
		var closestIndex = null;

		if (direction == 'down') {
			if (scrollContainerOffset < this.toTolleranceOffset) {
				closestIndex = this.getClosestIndex(scrollContainerOffset);

				if (closestIndex > this.offsetJumps.length)
					closestIndex = this.offsetJumps.length;

				this.pageData(this.offsetJumps[closestIndex].jump, this.offsetJumps[closestIndex].jump + this.numberOfNewVisibleRows);
			}
		} else {
			if (scrollContainerOffset > this.fromTolleranceOffset) {
				closestIndex = this.getClosestIndex(scrollContainerOffset);

				if (closestIndex < 0)
					closestIndex = 0;

				this.pageData(this.offsetJumps[closestIndex].jump, this.offsetJumps[closestIndex].jump + this.numberOfNewVisibleRows);
			}
		}
	}

	getClosestIndex(scrollContainerOffset) {
		return findWithAttr(this.offsetJumps, 'offset', this.offsets.getClosest(scrollContainerOffset)) - 2;
	}

	pageData(from, to) {
		if (this.from === from || this.to === to)
			return false;

		this.from = from;
		this.to = to;

		this.setTableTop(this.from * this.heightOfRow);
		this.setRange({ from: this.from, to: this.to });
		this.AeviGrid.AeviGridEditor.refreshData({ From: this.from, To: this.to});
	}

	setTableTop(value) {
		this.table[0].style.top = value + 'px';
		this.tableTop = value;
	}

	setRange(range) {
		this.from = range.from;
		this.to = range.to;
		this.canScrollUp = (this.from === 0);

		var fromJumpValue = this.from + this.tollerance;

		var fromJumpId = findWithAttr(this.offsetJumps, 'jump', fromJumpValue);

		this.fromTolleranceOffset = this.offsetJumps[fromJumpId].offset;

		var toJumpValue = this.to - this.tollerance;
		var toJumpId = findWithAttr(this.offsetJumps, 'jump', toJumpValue);

		if (_.isUndefined(toJumpId))
			toJumpId = fromJumpId;

		this.toTolleranceOffset = this.offsetJumps[toJumpId].offset;
	}

	getRange() {
		return {
			from: this.from,
			to: this.to
		};
	}

	setJumps() {
		var visibleRecordsLength = parseInt(this.AeviGrid.AeviDataService.AeviDataRepository.getVisibleRecordsLength());

		var countOfJumps = (visibleRecordsLength / this.countOfNewRows) + 3;
		this.jumps = [];
		var jump = 0;

		for (var i = 0; i < countOfJumps; i++) {
			this.jumps.push(jump);
			jump += this.countOfNewRows;
		}

		this.offsetJumps = [];

		var tableBodyHeight = this.AeviGrid.AeviDimensions.privateDimensions.table_body.height;
		var tableBodyOffsetTop = this.aeviScrollElement.offset().top;

		for (i = 0; i < this.jumps.length; i++) {
			this.offsetJumps.push({
				jump: this.jumps[i],
				offset: ((this.heightOfRow * this.jumps[i]) - tableBodyHeight - tableBodyOffsetTop) * - 1
			});
		}

		this.offsets = [];

		for (i = 0; i < this.offsetJumps.length; i++) {
			this.offsets.push(this.offsetJumps[i].offset);
		}

		this.AeviPager.setScrollContainerHeight(visibleRecordsLength * this.heightOfRow);
	}

	getLastTolleranceRow() {
		var rowIndex = this.to - this.tollerance;
		var rowNode = this.AeviGrid.AeviDOM.getRow(rowIndex);
		var rowOffsetTop = (rowNode.length) ? rowNode.offset().top : null;

		return {
			rowIndex: rowIndex,
			rowNode: rowNode,
			rowOffsetTop: rowOffsetTop
		};
	}

	getFirstTolleranceRow() {
		var rowIndex = this.from + this.tollerance;
		var rowNode = this.AeviGrid.AeviDOM.getRow(rowIndex);
		var rowOffsetTop = (rowNode.length) ? rowNode.offset().top : null;

		return {
			rowIndex: rowIndex,
			rowNode: rowNode,
			rowOffsetTop: rowOffsetTop
		};
	}

	listener() {
        this.aeviScrollElement.on('scroll', (event) => {
            this.scroll($(event.currentTarget), event);
            this.AeviPager.setVisibleContentInfo();
        });

		$(document).on('click', this.aeviConsts.arrowSelector, (event) => {
			event.preventDefault();

			var arrow: HTMLElement = <HTMLElement>event.currentTarget;

			var arrowClass = this.aeviConsts.arrowSelector.makeClassFromSelector();
			var arrowDirection = arrow.getAttribute('class').replace(arrowClass, '').replace(/ /g, '');

			var props: IAeviMoveToProps = {};

			switch (arrowDirection) {
				case 'right':
					props.rowId = this.AeviPager.arrowSteps.nextRightStep.rowIndex;
					props.cellId = this.AeviPager.arrowSteps.nextRightStep.cellIndex;
					props.scrollLeft = true;
                    break;

				case 'left':
					props.rowId = this.AeviPager.arrowSteps.prevLeftStep.rowIndex;
					props.cellId = this.AeviPager.arrowSteps.prevLeftStep.cellIndex;
					props.scrollLeft = true;
					break;
			}

            this.AeviPager.moveTo(props);
		});
	}	
}
