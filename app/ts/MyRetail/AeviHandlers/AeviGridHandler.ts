/// <reference path='../../references.ts' />
/// <reference path='../../MyRetail.ts' />
/// <reference path='../AeviEditors/AeviEditorFactory.ts' />
/// <reference path='../AeviValidators/AeviValidatorFactory.ts' />
/// <reference path='../AeviValidators/IAeviValidationInfo.ts' />
/// <reference path='../AeviHandlers/IAeviHandler.ts' />
/// <reference path='../AeviHandlers/AeviBodyCellHandler.ts' />
/// <reference path='../AeviHandlers/AeviHeaderColumnHandler.ts' />
/// <reference path='../AeviHandlers/AeviBodyRowHandler.ts' />


class AeviGridHandler {
	AeviGrid: any;
	AeviDataRepository: IAeviDataRepository;

	AeviBodyRowHandler: IAeviHandler;
	AeviBodyCellHandler: IAeviHandler;
	AeviHeaderColumnHandler: IAeviHandler;

	selectedCell: JQuery;
	selectedCellEditor: any;
	lastSelectedCell: JQuery;

	selectedCellLeaveValue: any;
	selectedCellEnterValue: any;
	
	selectedRowIndex: number;

    lastEvent: string;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.AeviDataRepository = this.AeviGrid.AeviDataRepository;

		this.AeviBodyCellHandler = new AeviBodyCellHandler(this.AeviGrid, this);
		this.AeviHeaderColumnHandler = new AeviHeaderColumnHandler(this.AeviGrid);
		this.AeviBodyRowHandler = new AeviBodyRowHandler(this.AeviGrid);
        this.AeviGrid.AeviClipboard = new AeviClipboard(this.AeviGrid);

		this.listener();
	}

	cellChange(cell: JQuery, changeBy?: MyRetail.IAeviChangeType) {
        if (this.selectedCell) {
			if (_.isNull(cell)) {
				if (!_.isNull(this.selectedCell)) {
					this.cellLeave(true);
					this.selectedCell = null;
					this.AeviGrid.AeviGridEditor.refreshData();
				}

				this.AeviGrid.AeviBubbless.destroy();
				return false;
			}

			if (cell.attr('id') == this.selectedCell.attr('id'))
				return false;

			this.cellLeave(this.isRowPositionChanged(cell));
		}

        this.cellEnter(cell, changeBy);
	}

	cellLeave(isRowPositionChanged: boolean): void {
		var cellIndex = this.AeviGrid.AeviDOM.getCellIndexes(this.selectedCell);
		//var row = this.AeviGrid.AeviDOM.getRow(cellIndex.rowIndex);
		
		this.lastSelectedCell = this.selectedCell;

        if (!aeviIsUndefinedOrNull(this.selectedCellEditor)) {

            this.removeCellEditor();

			var rowStatus = this.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(cellIndex.rowIndex);

			if (this.selectedCellEditor.isDataValueChanged) {
				/**
				 * validate cell
				 */
				var validityResult: IAeviValidationInfo = this.AeviDataRepository.AeviTableData.validateCell(cellIndex.rowIndex, cellIndex.cellIndex);

				this.AeviDataRepository.AeviTableData.setCellValidityInfo(validityResult, cellIndex.rowIndex, cellIndex.cellIndex);
				this.AeviDataRepository.AeviTableData.checkValidityStatus(cellIndex.rowIndex);

				// this.selectedCellLeaveValue = this.lastSelectedCell.val...
				// callPluginOnCellLeave()

				rowStatus = this.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(cellIndex.rowIndex);

				if (rowStatus.indexOf('newrow') === -1) {
					this.AeviDataRepository.AeviTableData.AeviStatus.addByRowIndex(cellIndex.rowIndex, 'update');
                }

				if (rowStatus.indexOf('newrow') !== -1) {
					this.AeviDataRepository.AeviTableData.AeviStatus.addByRowIndex(cellIndex.rowIndex, 'insert');
					this.AeviDataRepository.AeviTableData.validateRow(cellIndex.rowIndex);
				}
			} else {
				// this.selectedCellLeaveValue = this.lastSelectedCell.val...
				// callPluginOnCellLeave()
			}

            var isRowWillBeRefreshedOnPutDone: boolean = false;

			if(isRowPositionChanged) {
				// callPluginOnRowsChanged()

				rowStatus = this.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(cellIndex.rowIndex);
				
				if (rowStatus.indexOf('invalid') === -1) {
					var callPutRequest: boolean = false;

                    if (rowStatus.indexOf('insert') !== -1 || rowStatus.indexOf('update') !== -1) {
						callPutRequest = true;
                    }

					if(callPutRequest) {
                        this.AeviGrid.AeviDataService.putRow(cellIndex.rowIndex);
                        isRowWillBeRefreshedOnPutDone = true;
                    }
				}
			}

            if (this.selectedCellEditor.isDataValueChanged && !isRowWillBeRefreshedOnPutDone) {
                this.AeviGrid.AeviGridEditor.refreshRow({ rowIndex: cellIndex.rowIndex });

                var nextRowIndex = cellIndex.rowIndex + 1;
                var nextRowStatus = this.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(nextRowIndex);

                if (!_.isNull(nextRowStatus)) {
                    if(nextRowStatus.indexOf('hidden') !== -1) {
                        this.AeviDataRepository.AeviTableData.AeviStatus.removeByRowIndex(nextRowIndex, 'hidden');
                        this.AeviDataRepository.AeviTableData.AeviStatus.addByRowIndex(nextRowIndex, 'newrow');
                        this.AeviGrid.AeviGridEditor.refreshRow({ rowIndex: nextRowIndex });
                    }
                }
            }
		}

		if (!_.isNull(cellIndex)) {
			var nextRowIndex = cellIndex.rowIndex + 1;
			this.scrollToNextRowIfIsNew(nextRowIndex);
		}
	}

    cellEnter($cell: JQuery, callBy?: MyRetail.IAeviChangeType) {
        if (aeviIsUndefinedOrNull(callBy)) {
            callBy = MyRetail.IAeviChangeType.byGrid;
        }

        this.selectedCell = $cell;

        if(aeviIsUndefinedOrNull(this.AeviGrid.AeviFakeEditor))
            this.AeviGrid.AeviFakeEditor = new AeviFakeEditor(this.AeviGrid);

        //this.selectedCellEnterValue = $cell[0].getAttribute('data-value');
        var cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes($cell[0]);

        /**
         * fake input is necessary for event handling and clipboard
         */
		if (aeviIsUndefinedOrNull(cellIndexes)) {
			this.AeviGrid.print('AeviGridHandler.cellEnter(), variable "cellIndexes" is undefined or null because parameter "cell" is empty or null');
            return null;
		}

		this.selectedRowIndex = cellIndexes.rowIndex;

		var selectedIndexes = {rowIndex: cellIndexes.rowIndex, cellIndex: cellIndexes.cellIndex};

		this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableData.setSelectedPositions(selectedIndexes, selectedIndexes);

		var action: IAeviEntityActionDefinition  = this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnActionByColumnIndex(cellIndexes.cellIndex);
        var editorFactory = new AeviEditorFactory(this.AeviGrid);

        if (this.AeviGrid.AeviGridLocker.isLocked) {
            editorFactory.focus($cell);
            editorFactory.createEditor($cell);
            this.selectedCellEditor = editorFactory.getEditor();

            /**
             * special case
             * Grid is locked and user want to view image in readOnly mode
             */
            if (this.selectedCellEditor instanceof AeviImageEditor) {
                this.selectedCellEditor.listener();
            }

            /**
             * special case
             * Grid is locked and user want to click to actionLink
             */
            if (!aeviIsUndefinedOrNull(action)) {
                if (!_.isNull(action.ActionId) || action.ActionId !== 'null' || action.ActionId !== '') {
                    editorFactory.createEditor($cell);
                    this.selectedCellEditor = editorFactory.getEditor();
                    this.selectedCellEditor.setReady();

                    /**
                     * open editor only if call event is by click
                     */
                    if (callBy === MyRetail.IAeviChangeType.byClick) {
                        this.selectedCellEditor.trigger();
                    }
                }
            }
        } else {
            editorFactory.createEditor($cell);
            this.selectedCellEditor = editorFactory.getEditor();

            if(editorFactory.isEditorExists()) {
                editorFactory.focus($cell);
                this.selectedCellEditor.setReady();

                if (this.selectedCellEditor instanceof AeviActionEditor && callBy === MyRetail.IAeviChangeType.byClick) {
                    this.selectedCellEditor.trigger();
                }
            }
        }

        this.AeviGrid.select(this.selectedCell[0]);

        this.AeviGrid.AeviFakeEditor.setContent($cell);
	}

    removeCellEditor(): void {
        if (!aeviIsUndefinedOrNull(this.selectedCellEditor))
            this.selectedCellEditor.remove();
    }

	triggerClick(): void {
        if (this.selectedCell && this.selectedCell.length) {
            this.AeviGrid.AeviDataRepository.AeviTableData.removeSelectedPositions();
			this.cellChange(null);
			$(document).trigger(AeviConsts.mouseUpEvent);
		}
	}

	isRowPositionChanged(cell: JQuery): boolean {
		if (cell.length < 1 || _.isNull(cell)) {
			return;
		}

		return (this.selectedRowIndex !== this.AeviGrid.AeviDOM.getCellIndexes(cell).rowId);
	}

	scrollToNextRowIfIsNew(nextRowIndex: number): void {
        if (!this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(nextRowIndex, 'newrow')) {
			return;
        }

        if (!this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(nextRowIndex + 1, 'hidden')) {
            return;
        }

		var nextCell = this.AeviGrid.AeviDOM.getCell(0, nextRowIndex);

		if (aeviIsUndefinedOrNull(nextCell) || nextCell.length < 1) {
			return;
        }

		if (!AeviBodyCell.isInViewport(nextCell[0], document.getElementById(this.AeviGrid.tableId))) {
            $(document).trigger('mouseup');
            this.AeviGrid.AeviPager.moveTo({ rowId: nextRowIndex });
        }
	}

	static isUserClickToJqueryUI(clickedElement: HTMLElement): boolean {
        return AeviGridHandler.isUserClickTo(clickedElement, '.ui-widget');
	}

    static isUserClickToModal(clickedElement: HTMLElement): boolean {
        if(!$('.simplemodal-container').length)
            return false;

        return AeviGridHandler.isUserClickTo(clickedElement, '.simplemodal-container');
    }

    static isUserClickTo(clickedElement: HTMLElement, selector: string): boolean {
        return $.contains($(selector)[0], clickedElement);
    }

	isUserClickToAeviTable(element: HTMLElement): boolean {
		var outerContainer = document.querySelector(this.AeviGrid.AeviConsts.outerContainerSelector);
		var search = document.querySelector(this.AeviGrid.AeviConsts.searchSelector);

		var result = $.contains(outerContainer, element);

		if (!result) {
			if (!aeviIsUndefinedOrNull(search))
				result = $.contains(search, element);
		}

		if (!result) {
			if ($(element).hasClass('aeviArrow'))
				result = true;
		}

		return result;
	}

	listener(): void {
        //$(document).on(AeviConsts.allEvents, (event) => {
        //    this.lastEvent = event.type;
        //});

		$(document).on(AeviConsts.keyDownEvent, (event) => {
			var code = event.keyCode || event.which;
			var codeKey = this.AeviGrid.AeviConsts.keys[code];

            if (codeKey === 'a' && (event.ctrlKey || event.metaKey)) {
				event.preventDefault();
				$(this.AeviGrid.AeviConsts.aeviSelectAllSelector).trigger('click');
			}
		
			if (codeKey === 'c' && (event.ctrlKey || event.metaKey)) {
				if (!aeviIsUndefinedOrNull(this.selectedCell) || this.AeviGrid.AeviDOM.isRowSelected() || this.AeviGrid.AeviClipboard.isDialogRendered)
					this.AeviGrid.AeviClipboard.copy();
			}

			if (codeKey === 'backspace') {
				if ($('.aeviToolbar__textfilter').is(':focus')) {
					return;
				}

                if (this.AeviGrid.AeviDOM.isFormVisible()) {
                    return;
                }

                if (this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport()) {
                    return;
                }

                if (!$(this.AeviGrid.AeviConsts.cellSelector).hasClass('hasEditor')) {
					if (aeviIsUndefinedOrNull(this.AeviGrid.AeviSearch)) {
						event.preventDefault();
						return;
					} else {
						if (!this.AeviGrid.AeviSearch.isSearchVisible) {
							event.preventDefault();
							return;
						}
					}
				}
			}

			if (codeKey === 'delete') {
                if(!aeviIsUndefinedOrNull(this.AeviGrid.AeviGridLocker)) {
                    if (!this.AeviGrid.AeviGridLocker.isLocked) {
                        if (this.AeviGrid.AeviDataRepository.isMoreCellsSelected()) {
                            event.preventDefault();
                            this.AeviGrid.AeviDataService.clearData();
                            this.triggerClick();
                            this.AeviGrid.AeviGridEditor.refreshData();
                        }
                    }
                }
			}
		});

		$(document).on(AeviConsts.mouseUpEvent, 'body', (event) => {
            var clickedElement = <HTMLElement>event.target;

			if (!this.isUserClickToAeviTable(clickedElement)) {
				if ($('.ui-widget').length && AeviGridHandler.isUserClickToJqueryUI(clickedElement)) {
                    return;
                }

                if (this.AeviGrid.isModalOpened) {
                    return;
                }

				this.triggerClick();
			}
		});

		$(document).on(AeviConsts.clickEvent, 'body', (event) => {
			if (!this.isUserClickToAeviTable(<HTMLElement>event.target)) {
                if(!this.AeviGrid.isModalOpened)
                    this.triggerClick();
            }

			if (this.AeviGrid.AeviDOM.length(this.AeviGrid.AeviConsts.contextMenuSelector))
				$(this.AeviGrid.AeviConsts.contextMenuSelector).hide();
		});

		$(document).on(AeviConsts.keyDownEvent, (event) => {
			var key = this.AeviGrid.AeviClientSide.getKeyName(event);

			if (key === 'f3' || ((event.ctrlKey || event.metaKey) && key === 'f')) {
				event.preventDefault();

                if (this.AeviGrid.AeviDOM.isCellSelected()) {
                    this.triggerClick();
                }

				if (aeviIsUndefinedOrNull(this.AeviGrid.AeviSearch)) {
                    this.AeviGrid.AeviSearch = new AeviSearch(this.AeviGrid);
                } else {
                    this.AeviGrid.AeviSearch.show();
                }
			}
		});

		$(document).off(AeviConsts.doubleClickEvent, '.aeviCell').on(AeviConsts.doubleClickEvent, '.aeviCell', (event) => {
            event.preventDefault();

            if (!this.AeviGrid.AeviDataService.AeviUser.roles.Write) {
                return;
            }

			if (this.AeviGrid.AeviDOM.isCellSelected() && !this.AeviGrid.isLockMessageShowed() && this.AeviGrid.AeviGridLocker.isLocked && !this.AeviGrid.AeviDataRepository.AeviTableDescription.realReadOnly) {
				this.AeviGrid.showLockMessage();
				return false;
			}

			if (this.AeviGrid.AeviDataRepository.isMaxRowCountExceeded() && !this.AeviGrid.AeviGridLocker.isLocked) {
				var indexes = this.AeviGrid.AeviDOM.getCellIndexes(<HTMLElement>event.currentTarget);

				if (this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.isNew(indexes.rowIndex)) {
					event.preventDefault();
					this.AeviGrid.showMaxRowCountMessage();
					return false;
				}
			}

			return false;
		});

		window.onbeforeunload = () => {
            if(!aeviIsUndefinedOrNull(this.AeviGrid.AeviDataService.AeviApiService.AeviApiAuthorization)) {
                if(this.AeviGrid.AeviDataService.AeviApiService.AeviApiAuthorization.isCookiesNull) {
                    this.AeviGrid.AeviDataService.AeviApiService.AeviApiAuthorization.setCookies();
                }
            }

			if (this.AeviGrid.AeviDataService.AeviApiService.isDataChanged || this.AeviGrid.AeviDataService.AeviApiService.isCellChanged) {
				return this.AeviGrid.AeviLocalization.translate('close_window');
			}
		};

        document.addEventListener(AeviConsts.pasteEvent, (event) => {
            /**
             * todo!
             * implement every modal window
             */
            if(!this.AeviGrid.isClipboardModalOpened) {
                if(!this.AeviGrid.AeviDOM.isCellSelected())
                    return;
            }

            if(this.AeviGrid.AeviDataRepository.AeviTableDescription.readOnly || this.AeviGrid.AeviGridLocker.isLocked) {
                event.preventDefault();
                return false;
            }

            this.AeviGrid.AeviClipboard.paste(event);
        });

		$(window).on(AeviConsts.resizeEvent, (event) => {
			this.AeviGrid.setSizes();
			this.AeviGrid.AeviGridEditor.headerRow.refresh(this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumns());
			this.AeviGrid.AeviPager.refreshScrollContainer();
			this.AeviGrid.AeviPager.setArrowPositions();
		});
	}
}
