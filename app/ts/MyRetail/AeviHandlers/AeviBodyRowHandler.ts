/// <reference path='../../references.ts' />
/// <reference path='IAeviHandler.ts' />

/**
 * TODO!
 * selecting rows works wrong
 */
class AeviBodyRowHandler implements IAeviHandler {
	AeviGrid: any;
	AeviDataRepository: IAeviDataRepository;
	AeviContextMenu: any;

	ctrlHold: boolean;
	shiftHold: boolean;
	mouseDown: boolean;

	range: any[];
	rowIndexes: any[];
	firstRowIndex: number;
	firstClick: number;

	aeviRowNumber: JQuery;
	mouseDownCell: JQuery;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.AeviDataRepository = this.AeviGrid.AeviDataRepository;
		this.ctrlHold = false;
		this.shiftHold = false;
		this.rowIndexes = [];
		this.firstRowIndex = null;
		this.aeviRowNumber = $(this.AeviGrid.AeviConsts.rowNumberSelector);
		this.listener();
	}

	listener() {
		$(document).off('mousedown', this.aeviRowNumber.selector).on('mousedown', this.aeviRowNumber.selector, (event) => {
			this.AeviGrid.AeviGridHandler.triggerClick();

			if (event.button === 0) {
				this.mouseDown = true;
				this.mouseDownCell = $(event.currentTarget);
				this.click(this.mouseDownCell);

				if (this.AeviGrid.AeviDOM.isContextMenuVisible())
					this.AeviGrid.AeviDOM.hideAllContextMenus();
			}
		});

		$(document).on('mouseup', this.aeviRowNumber.selector + ', ' + this.AeviGrid.AeviConsts.cellSelector, () => {
			this.mouseDown = false;
			this.firstRowIndex = null;
		});

		$(document).on('mouseover', this.aeviRowNumber.selector + ', ' + this.AeviGrid.AeviConsts.cellSelector, (event) => {
			if (this.mouseDown) {
				var range = [];
				var firstRowIndex = this.firstRowIndex;

				if (this.ctrlHold)
					range = this.range;
				else
					range = [];

				if (_.isNull(firstRowIndex)) {
					firstRowIndex = this.AeviGrid.AeviDOM.getCellIndexes(this.mouseDownCell).rowId;
					this.firstRowIndex = firstRowIndex;
				}

				var lastRowIndex = this.AeviGrid.AeviDOM.getCellIndexes($(event.currentTarget)).rowId;
				var direction = (firstRowIndex < lastRowIndex) ? 'down' : 'up';
				var i = 0;

				if (direction == 'down') {
					for (i = firstRowIndex; i < lastRowIndex + 1; i++) {
						range.push(i);
					}
				} else {
					for (i = firstRowIndex; i > lastRowIndex - 1; i--) {
						range.push(i);
					}
				}

				this.AeviGrid.selectRow(range);
				this.range = range;
			}
		});

		$(document).on('keydown', (event) => {
			var key = this.AeviGrid.AeviClientSide.getKeyName(event);

			if (key === 'ctrl' || this.AeviGrid.AeviClientSide.isMac())
				this.ctrlHold = true;

			if (key === 'shift')
				this.shiftHold = true;

			/*
			 *	DELETE ROW
			 */
			if (key === 'delete' && $('.aeviRowNumber').hasClass('selected')) {
				if (this.AeviGrid.mode.clipboard === 1)
					return;

				this.AeviGrid.AeviToolbar.AeviToolbarHandler.deleteRows();
			}
		});

		$(document).on('keyup', () => {
			this.ctrlHold = false;
			this.shiftHold = false;
		});

		$(document).on('contextmenu', this.aeviRowNumber.selector, (event) => {
			event.preventDefault();
            var target: JQuery = $(event.currentTarget);

			if (this.AeviGrid.AeviDataRepository.AeviTableDescription.readOnly || this.AeviGrid.mode.clipboard === 1 || this.AeviGrid.AeviGridLocker.isLocked)
				return false;

			if (!target.hasClass('selected'))
				this.click(target);

			this.AeviGrid.lastSelectedCell = <JQuery>$(event.currentTarget);

			if (!this.AeviContextMenu)
				this.AeviContextMenu = new AeviContextMenu(this.AeviGrid, 'rowNumber');

			this.AeviContextMenu.showMenu(event);
		});
	}

	/**
	 * TODO!
	 * this need refactor
	 */
	click(cell: JQuery): void {		
		this.AeviGrid.lastSelectedCell = cell;
        var rowIndex = this.AeviGrid.AeviDOM.getCellParentIndex(cell[0]);

        var message = this.AeviDataRepository.AeviTableData.getErrorMessage(<IAeviRecordIdentification>{ rowIndex: rowIndex });
        message = (message === '' || aeviIsUndefinedOrNull(message)) ? null : message;
		this.AeviGrid.AeviStatusBar.error(message);

		if (this.range) {
			$.each(this.range, (i, el) => {
				if ($.inArray(el, this.rowIndexes) === -1) this.rowIndexes.push(this.range[i]);
			});
		}

        /**
         * If user doesn't hold ctrl clear array of selected rows
         */
        if(!this.ctrlHold) {
            this.rowIndexes.length = 0;
            this.AeviDataRepository.clearSelectedRows();
        }

		if (this.shiftHold) {
			/**
			 * If user clicked on different row target range(from,to)
			 */
			if (this.firstClick !== undefined && this.firstClick != rowIndex) {
				var direction = (this.firstClick > rowIndex) ? 1 : 0;
				var from = 0;
				var to = 0;
				var helper = null;

				if (!direction) {
					from = this.firstClick;
					to = rowIndex;
				} else {
					helper = this.firstClick;
					from = rowIndex;
					to = helper;
				}

				for (var x = from; x <= to; x++) {
					this.rowIndexes.push(x);
				}

			} else {
				this.rowIndexes.push(rowIndex);
			}
		} else {
			if(!this.ctrlHold)
				this.rowIndexes = [];

			this.rowIndexes.push(rowIndex);

			if (cell.hasClass('selected')) {
				this.rowIndexes = _.without(this.rowIndexes, rowIndex);
			}

			this.firstClick = rowIndex;
		}

		this.range = this.rowIndexes;
		this.AeviGrid.selectRow(this.rowIndexes);
	}
}
