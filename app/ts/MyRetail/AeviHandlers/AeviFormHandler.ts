/// <reference path='../../references.ts' />

class AeviFormHandler implements IAeviHandler{
    AeviGrid: any;
    AeviForm: any;
    buttons: any;
    static formPrefix = 'aeviForm-';

    constructor(aeviForm: any) {
        this.AeviForm = aeviForm;
        this.AeviGrid = this.AeviForm.AeviGrid;

        this.buttons = {
            'close':  $('.' + AeviFormHandler.formPrefix + this.AeviForm.rowIndex + '-close'),
            'save':  $('#' + AeviFormHandler.formPrefix + this.AeviForm.rowIndex + '-save'),
            'leftArrow':  $('#' + AeviFormHandler.formPrefix + this.AeviForm.rowIndex + '-left'),
            'rightArrow':  $('#' + AeviFormHandler.formPrefix + this.AeviForm.rowIndex + '-right'),
            'rowInput':  $('#' + AeviFormHandler.formPrefix + this.AeviForm.rowIndex + '-rowInput'),
        };

        this.listener();
        this.formListener();
	}

    listener() {
        for (var i = 0; i < this.AeviForm.editors.length; i++) {
            var editor = this.AeviForm.editors[i];
            editor.initFormBinder(this.AeviForm.AeviFormRepository);
            editor.formListener();
        }

        $(document).on('keydown', (event) => {
            if (!this.AeviGrid.AeviDOM.isFormVisible()) {
                return;
            }

            var key = this.AeviGrid.AeviClientSide.getKeyName(event);

            if (key === 'escape') {
                this.AeviForm.destroy();
            }
        });
    }

    formListener() {
        this.buttons.close.on('click', (event) => {
            event.preventDefault();
            this.AeviForm.setImageDefaultValue();
            this.AeviForm.destroy();
            this.AeviGrid.AeviGridEditor.refreshRow({ rowIndex: this.AeviForm.rowIndex });
        });

        this.buttons.save.on('click', (event) => {
            event.preventDefault();
            this.AeviForm.updateRecord();

            this.setRowPutType();

            this.AeviGrid.AeviDataRepository.AeviTableData.validateRow(this.AeviForm.rowIndex);
            this.AeviGrid.AeviGridEditor.refreshRow({ rowIndex: this.AeviForm.rowIndex });

            if (!this.AeviForm.AeviDataService.AeviDataRepository.AeviTableData.AeviStatus.is(this.AeviForm.rowIndex, 'invalid')) {
                this.AeviForm.AeviDataService.putRow(this.AeviForm.rowIndex);
            }

            this.AeviForm.destroy();
        });

        this.buttons.leftArrow.on('click', (event) => {
            event.preventDefault();
            this.AeviForm.setImageDefaultValue();
            this.AeviForm.destroy();
            return new AeviForm(this.AeviForm.rowIndex - 1, this.AeviForm.AeviDataService, {isOpenedByToolbar: this.AeviForm.isOpenedByToolbar});
        });

        this.buttons.rightArrow.on('click', (event) => {
            event.preventDefault();
            this.AeviForm.setImageDefaultValue();
            this.AeviForm.destroy();
            return new AeviForm(this.AeviForm.rowIndex + 1, this.AeviForm.AeviDataService, {isOpenedByToolbar: this.AeviForm.isOpenedByToolbar});
        });
    }

    setRowPutType() {
        var rowStatus = this.AeviForm.AeviDataService.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(this.AeviForm.rowIndex);

        if (rowStatus.indexOf('newrow') === -1) {
            this.AeviForm.AeviDataService.AeviDataRepository.AeviTableData.AeviStatus.addByRowIndex(this.AeviForm.rowIndex, 'update');
        }

        if (rowStatus.indexOf('newrow') !== -1) {
            this.AeviForm.AeviDataService.AeviDataRepository.AeviTableData.AeviStatus.addByRowIndex(this.AeviForm.rowIndex, 'insert');
        }
    }
}
