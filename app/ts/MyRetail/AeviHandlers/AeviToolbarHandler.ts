/// <reference path='../../references.ts' />
/// <reference path='../AeviToolbar.ts' />
/// <reference path='../AeviModal/AeviModal.ts' />
/// <reference path='../AeviGridLocker.ts' />
/// <reference path='../AeviSearch.ts' />

import IAeviBlockStateCommand = MyRetail.IAeviBlockStateCommand;
class AeviToolbarHandler {
	AeviGrid: any;
	AeviLocalization: any;
	rowHeight: number;
    importExportSubMenu: any;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.AeviLocalization = this.AeviGrid.AeviLocalization;
	}

	save(): void {
        this.AeviGrid.AeviDataService.AeviApiService.getDataEntityStatus()
            .done((response: IAeviEntityStatusResponse) => {
                if (response.Status === MyRetail.IAeviResponseStatus.Blocked) {
                    this.AeviGrid.AeviDataService.showBlockModal(response)
                        .done(() => {
                            this.AeviGrid.AeviDataService.AeviApiService.setDataEntityBlockStatus().done(() => {
                                this.callCommit();
                            });
                        })
                        .fail(() => {
                            this.AeviGrid.AeviGridLocker.lock();
                        });
                } else if (response.Status === MyRetail.IAeviResponseStatus.Changed) {
                    this.AeviGrid.AeviDataService.showDataChangedModal(response)
                        .done(() => {
                            this.AeviGrid.AeviDataService.AeviApiService.setDataEntityBlockStatus().done(() => {
                                this.AeviGrid.AeviDataService.getDataByQuery('', true);
                            });
                        })
                        .fail(() => {
                            this.AeviGrid.AeviGridLocker.lock();
                        });
                } else {
                    this.callCommit();
                }
            });
	}

    callCommit() {
        this.AeviGrid.AeviGridHandler.triggerClick();
        var JSONFilter = '';

        if (!aeviIsUndefinedOrNull(this.AeviGrid.AeviFilter))
            JSONFilter = encodeURIComponent(this.AeviGrid.AeviFilter.getJSONFilterValues());

        this.AeviGrid.AeviDataService.commit(JSONFilter, true);
        this.AeviGrid.AeviGridHandler.AeviHeaderColumnHandler.unSort();
    }

	discard(): void {
		this.AeviGrid.AeviGridHandler.triggerClick();

		var modal = new AeviModal(this.AeviGrid);

        var content: IAeviModalContent = {
            title: null,
            text: null
        };

        content.title = this.AeviLocalization.translate('warning');

        if (this.AeviGrid.AeviDataService.AeviApiService.isDataChanged || this.AeviGrid.AeviDataService.AeviApiService.isCellChanged) {
            content.text ='<p>' + this.AeviLocalization.translate('discard_modal_message') + '</p>';
            content.buttons = ['aeviDataServiceDiscard', 'aeviDataServiceClose'];
		} else {
            content.text = '<p>' + this.AeviLocalization.translate('no_changes_message') + '</p>';
		}

        modal.setContent(content);
		modal.show();
		modal.focus('last');
	}

	query() {
        $('.aeviToolbar__textfilter').trigger('blur');
        this.AeviGrid.AeviGridHandler.triggerClick();

        if (!aeviIsUndefinedOrNull(this.AeviGrid.AeviFilter)) {
            if (!this.AeviGrid.AeviFilter.isFiltersFilled()) {
                toastr.error(this.AeviLocalization.translate('some_filters_not_filled'));
                this.AeviGrid.AeviStatusBar.error(this.AeviLocalization.translate('some_filters_not_filled'));
                return;
            }
        }

        var JSONFilter = encodeURIComponent(this.AeviGrid.AeviFilter.getJSONFilterValues());

        if (this.AeviGrid.AeviDataService.AeviApiService.isDataChanged || this.AeviGrid.AeviDataService.AeviApiService.isCellChanged) {
			var modal = new AeviModal(this.AeviGrid);

			modal.setContent({
				title: this.AeviLocalization.translate('warning'),
				text: '<p>' + this.AeviLocalization.translate('temporary_message_filter') + '</p>',
				buttons: [
					'aeviDataServiceCommitTempAndClose',
					'aeviDataServiceDeleteTempAndClose',
					'aeviDataServiceClose'
				]
			});

			modal.show();
			modal.focus('last');
		} else {
			if (aeviIsUndefinedOrNull(JSONFilter)) {
				this.AeviGrid.print('AeviToolbarHandler.query(), variable "JSONFilter" is undefined or null.');
				return;
			}

			this.AeviGrid.AeviDataService.getDataByQuery(JSONFilter, true).done((response) => {
				if (!_.isNull(response.Data))
					toastr.info(this.AeviLocalization.translate('result_showed') + '' + response.Data.length + ' ' + this.AeviLocalization.translate('records'));

				this.AeviGrid.AeviAjaxPreloader.hide();
			});
		}
	}

    locker() {
		if (aeviIsUndefinedOrNull(this.AeviGrid.AeviGridLocker))
			this.AeviGrid.AeviGridLocker = new AeviGridLocker(this.AeviGrid);

		if (this.AeviGrid.AeviGridLocker.isLocked)
			this.AeviGrid.AeviGridLocker.unLock();
		else
			this.AeviGrid.AeviGridLocker.lock();
	}

	search() {
        this.AeviGrid.AeviGridHandler.triggerClick();

		if (aeviIsUndefinedOrNull(this.AeviGrid.AeviSearch))
			this.AeviGrid.AeviSearch = new AeviSearch(this.AeviGrid);
		else
			this.AeviGrid.AeviSearch.show();
	}

	refresh() {
		this.AeviGrid.AeviGridEditor.refreshData();
	}

	insertRow() {
		this.AeviGrid.AeviGridEditor.renderNewRow();
	}

    insertByForm() {
        this.AeviGrid.AeviDataService.getBlockStateCommand()
            .done((response: MyRetail.IAeviBlockStateCommand) => {
                if (response === MyRetail.IAeviBlockStateCommand.CONTINUE) {
                    this.insertRow();

                    var selectedRowIndex = this.AeviGrid.AeviDOM.getSelectedRowIndex();

                    if (_.isNull(selectedRowIndex)) {
                        var lastSelectedCell = this.AeviGrid.AeviGridHandler.lastSelectedCell[0];
                        var cellIndexes: IAeviCellIndex = this.AeviGrid.AeviDOM.getCellIndexes(lastSelectedCell);
                        selectedRowIndex = cellIndexes.rowIndex;
                    }

                    selectedRowIndex++;

                    return new AeviForm(selectedRowIndex, this.AeviGrid.AeviDataService, {isOpenedByToolbar: true});
                }
            })
            .fail((response: MyRetail.IAeviBlockStateCommand) => {
                if (response === MyRetail.IAeviBlockStateCommand.LOCK) {
                    this.AeviGrid.AeviGridLocker.lock();
                }

                if (response === MyRetail.IAeviBlockStateCommand.RELOAD) {
                    this.AeviGrid.AeviDataService.getDataByQuery('', true);
                }
            });
    }

    deleteRows(): void {
		if (!_.isNull(this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.fixedSize) || this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.readOnly)
			return;

		var rowGuidsLength = this.AeviGrid.AeviGridEditor.getDeletedRowGuids().length;

		if (rowGuidsLength === 0) {
			var modalWarning = new AeviModal(this.AeviGrid);

			modalWarning.setContent({
				title: this.AeviLocalization.translate('warning'),
				text: '<p>' + this.AeviLocalization.translate('no_rows_selected') + '</p>'
			});

			modalWarning.show();
			return;
		}

		var modal = new AeviModal(this.AeviGrid, (confirm) => {
			if (confirm)
				this.AeviGrid.AeviGridEditor.deleteRow();
		});

		modal.setContent({
			title: this.AeviLocalization.translate('warning'),
			text: '<p>' + this.AeviLocalization.translate('delete_rows_question') + ' ' + this.AeviGrid.AeviDataRepository.AeviDataEntity.data.Caption + ' ?' + '</p>',
			buttons: [
				'aeviModalAccept',
				'aeviModalDenied'
			]
		});

		modal.show();
		modal.focus('last');
	}

	popupSwitch() {
		var toastrContainer = $('#toast-container');
		var button = $('.aeviToolbar__showPopup');

		if (toastr.options.positionClass.indexOf('hidden') === -1) {
			toastrContainer.addClass('hidden');
			toastr.options.positionClass += ' hidden';
			button.removeClass('checked');
			localStorage.setItem('aeviPopupSwitch', 'false');
		} else {
			toastrContainer.removeClass('hidden');
			toastr.options.positionClass = toastr.options.positionClass.replace('hidden', '');
			button.addClass('checked');
			localStorage.setItem('aeviPopupSwitch', 'true');
		}
	}

	setFixRows(value) {
		if (_.isNumber(value))
			value = parseInt(value);

		if (value === 'null')
			value = null;

		value = parseInt(value);

		this.AeviGrid.AeviDimensions.fixedHeight = value;

		this.AeviGrid.AeviConsts.pager = {
			rowHeight: this.rowHeight,
			tollerance: 50,
			visibleRows: 200,
			countOfNewRows: 50
		};

		this.AeviGrid.AeviDataService.showData('re-render');
	}

	paste() {
        this.AeviGrid.AeviDataService.getBlockStateCommand()
            .done((response: MyRetail.IAeviBlockStateCommand) => {
                if (response === MyRetail.IAeviBlockStateCommand.CONTINUE) {
                    this.showPasteModal();
                }
            })
            .fail((response: MyRetail.IAeviBlockStateCommand) => {
                if (response === MyRetail.IAeviBlockStateCommand.LOCK) {
                    this.AeviGrid.AeviGridLocker.lock();
                }

                if (response === MyRetail.IAeviBlockStateCommand.RELOAD) {
                    this.AeviGrid.AeviDataService.getDataByQuery('', true);
                }
            });
	}

    showPasteModal() {
        var modal = new AeviModal(this.AeviGrid);
        var onShowFunction = null;
        var onCloseFunction = null;

        if (this.AeviGrid.AeviDataService.AeviApiService.isDataChanged) {
            modal.setContent({
                title: this.AeviLocalization.translate('warning'),
                text: '<p>' + this.AeviLocalization.translate('paste_first_save') + '</p>'
            });

        } else {
            modal.setContent({
                title : this.AeviGrid.AeviLocalization.translate('modal_inserting'),
                text: '<p>' + this.AeviLocalization.translate('toolbar_paste_message') + '</p><input type="text" class="fakeInput" id="fakeInput">'
            });

            onShowFunction = () => { this.AeviGrid.isClipboardModalOpened = 1; };
            onCloseFunction = () => { this.AeviGrid.isClipboardModalOpened = 0; };
        }

        modal.show(onShowFunction, onCloseFunction);
    }

    exportCSV() {
        var csv: string = this.AeviGrid.AeviDataRepository.AeviTableData.getDataAsCSV();
        var fileName = this.AeviGrid.AeviDataRepository.AeviDataEntity.data.Caption + '.csv';
        var dataType = 'data:text/csv;charset=utf-8,%ef%bb%bf';
        this.saveFile(csv, dataType, fileName);
    }

    saveFile(data: string, dataType: string, fileName: string): void {
        var browser = this.AeviGrid.AeviClientSide.getTrimmedBrowser();

        switch (browser) {
            case 'chrome':
            case 'firefox':
                var encodedURI = dataType + encodeURI(data);
                var link = document.createElement('a');
                link.setAttribute('href', encodedURI);
                link.setAttribute('target', '_blank');
                link.setAttribute('download', fileName);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
                break;

            case 'ie':
            case 'edge':
                var encodedURI = '\ufeff' + data;
                var link = document.createElement('a');

                if (window.navigator.msSaveOrOpenBlob){
                    var blobObject = new Blob([encodedURI]);

                    link.onclick = () => {
                        window.navigator.msSaveOrOpenBlob(blobObject, fileName);
                    };

                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
                break;

            case 'safari':
                this.AeviGrid.log('saveFile is not supported for this browser.');
                break;

            default:
                break;
        }
    }

    importFile() {
        if(!this.AeviGrid.AeviDOM.length(AeviConsts.fileImportInput)) {
            var importHTML: string = '<form id="importForm" style="display: none;" enctype="multipart/form-data" action="/" method="POST"><input name="file" id="' + AeviConsts.fileImportInput.makeClassFromSelector() + '" type="file" /></form>';
            $('body').append(importHTML);
        }

        $(document).off('change', AeviConsts.fileImportInput).on('change', AeviConsts.fileImportInput, (event) => {
            this.AeviGrid.AeviDataService.getBlockStateCommand()
                .done((response: MyRetail.IAeviBlockStateCommand) => {
                    if (response === MyRetail.IAeviBlockStateCommand.CONTINUE) {
                        var fileInput = <HTMLInputElement>document.getElementById(AeviConsts.fileImportInput.makeClassFromSelector());
                        var file = fileInput.files[0];
                        var type = file.type;
                        var fileType = null;

                        if (type === 'application/vnd.ms-excel' || type === 'text/csv' || type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            var csvRegex = new RegExp(".+(\.csv)$");
                            var xlsRegex = new RegExp(".+(\.xlsx?)$");

                            if (csvRegex.test(file.name)) {
                                fileType = 'Csv';
                            }

                            if (xlsRegex.test(file.name)) {
                                fileType = 'Xlsx';
                            }

                            var reader = new FileReader();
                            reader.onload = () => {
                                var base64Str: string = 'base64,';
                                var importDataWithMime: IAeviBinaryData = reader.result;
                                var startIndexOfBase64: number = importDataWithMime.indexOf(base64Str);
                                var importDataStartIndex: number = startIndexOfBase64 + base64Str.length;

                                var importData: IAeviBinaryData = importDataWithMime.substring(importDataStartIndex, importDataWithMime.length);

                                this.AeviGrid.AeviDataService.importFile(importData, fileType);
                                fileInput.value = '';
                            };

                            var hideImportMessage = AeviStorage.getStorageParam('hideImportMessage');

                            if (hideImportMessage === false || _.isNull(hideImportMessage)) {
                                var modal = new AeviModal(this.AeviGrid, (confirm) => {
                                    if (confirm) {
                                        reader.readAsDataURL(file);
                                    } else {
                                        fileInput.value = '';
                                    }
                                });

                                modal.setContent({
                                    title: this.AeviLocalization.translate('warning'),
                                    text: this.AeviLocalization.translate('import_modal_message') + '<br><br><label><input class="aevistorage" data-storageKey="hideImportMessage" type="checkbox"> ' + this.AeviLocalization.translate('do_not_show_message_again') + '</label>',
                                    buttons: ['aeviModalAccept', 'aeviModalDenied']
                                });

                                modal.show();
                                modal.focus('last');
                            } else {
                                reader.readAsDataURL(file);
                            }
                        } else {
                            var modal = new AeviModal(this.AeviGrid);
                            modal.setContent({
                                title: this.AeviLocalization.translate('warning'),
                                text: this.AeviLocalization.translate('bad_file_format')
                            });
                            modal.show();
                        }
                    }
                })
                .fail((response: MyRetail.IAeviBlockStateCommand) => {
                    if (response === MyRetail.IAeviBlockStateCommand.LOCK) {
                        this.AeviGrid.AeviGridLocker.lock();
                    }

                    if (response === MyRetail.IAeviBlockStateCommand.RELOAD) {
                        this.AeviGrid.AeviDataService.getDataByQuery('', true);
                    }
                });
        });

        $('#aeviImportFile').trigger('click');
    }

    importOrExportFile(event): void {
        event.stopPropagation();

        if (aeviIsUndefinedOrNull(this.importExportSubMenu)) {
            this.importExportSubMenu = new AeviToolbarSubMenu(this.AeviGrid, 'importexport');
        }

        if (this.importExportSubMenu.isOpened()) {
            this.importExportSubMenu.close();
        } else {
            this.importExportSubMenu.open(event.target);
        }
    }
}
