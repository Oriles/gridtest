/// <reference path='../../references.ts' />

class AeviContextMenuHandler {
	AeviGrid: any;
	AeviContextMenu: any;
	contextMenu: JQuery;

	constructor(aeviGrid: any, aeviContextMenu: any) {
		this.AeviGrid = aeviGrid;
		this.AeviContextMenu = aeviContextMenu;
		this.contextMenu = this.AeviContextMenu.contextMenu;
		this.listener();
	}

	listener() {
		$(document).on('mousedown', '.aeviContextMenu span', (event) => {
			event.preventDefault();
			event.stopPropagation();

			var menuItem = <HTMLElement>event.currentTarget;
			var className = menuItem.getAttribute('class');

			switch (className) {
				case 'aeviContextMenu__addRow':
					this.AeviGrid.AeviGridEditor.renderNewRow();
					break;

				case 'aeviContextMenu__deleteRow':
					this.AeviGrid.AeviToolbar.AeviToolbarHandler.deleteRows();
					break;

				default:
					break;
			}

			this.hideMenu();
		});
	}

	showMenu(event) {
		this.contextMenu
			.css({
				'top': event.pageY + 'px',
				'left': event.pageX + 'px'
			})
			.show('fade', 100)
			.addClass('isVisible');
	}

	/**
	 * TODO!
	 * hardstring
	 */
	hideMenu() {
		this.contextMenu.removeClass('isVisible').hide('fade', 75);
	}
}
