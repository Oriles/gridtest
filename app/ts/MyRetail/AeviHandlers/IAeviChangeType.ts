/// <reference path='../../references.ts' />

module MyRetail {
    export enum IAeviChangeType {
        byClick = 0,
        byKey = 1,
        byGrid = 2
    }
}

