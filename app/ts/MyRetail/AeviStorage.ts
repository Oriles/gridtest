/// <reference path='../references.ts' />

class AeviStorage {
    private static storageKey = 'AeviStorage';

    constructor() {
        var storage = AeviStorage.getStorage();

        if (_.isNull(storage)) {
            storage = AeviStorage.getEmptyStorage();
            AeviStorage.setStorage(storage);
        }

        AeviStorage.listener();
    }

    static listener(): void {
        $(document).on('click', '.aevistorage', (event) => {
            var target = <any>event.currentTarget;
            var nodeType = target.getAttribute('type');
            var storageKey = target.getAttribute('data-storageKey');

            if (nodeType === 'checkbox') {
                var isChecked = target.checked;
                AeviStorage.setStorageParam(storageKey, isChecked);
            }
        });
    }

    static getEmptyStorage(): any {
        return {};
    }

    static setStorage(storage: any): void {
        localStorage.setItem(AeviStorage.storageKey, JSON.stringify(_.clone(storage)));
    }

    static getStorage(): any {
        var storage: string = localStorage.getItem(AeviStorage.storageKey);

        if(_.isNull(storage) || _.isUndefined(storage)) {
            return null;
        }

        return <any>JSON.parse(storage);
    }

    static setStorageParam(key: string, value: string): void {
        var storage = AeviStorage.getStorage();

        storage[key] = value;
        AeviStorage.setStorage(storage);
    }

    static getStorageParam(key: string): any {
        var storage = AeviStorage.getStorage();

        if (_.isNull(storage)) {
            return null;
        }

        var storageKey = storage[key];

        if (_.isNull(storageKey) || _.isUndefined(storageKey)) {
            return null;
        }

        return storageKey;
    }
}
