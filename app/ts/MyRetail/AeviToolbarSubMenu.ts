/// <reference path='../references.ts' />

class AeviToolbarSubMenu {
	AeviGrid: any;
    action: string;
    isSubMenuOpened: boolean;
    localization: any;
    static SUBMENU_ID = 'aeviSubMenu';

	constructor(aeviGrid: any, action: string) {
		this.AeviGrid = aeviGrid;
        this.action = action;
        this.isSubMenuOpened = false;
        this.localization = this.AeviGrid.AeviLocalization;
	}

    render(el: Element): void {
        var HTML: string = '';
        HTML += '<div id="' + AeviToolbarSubMenu.SUBMENU_ID + '" class="aeviToolbar__submenu">';

        switch (this.action) {
            case 'importexport':
                HTML += this.getImportHTML();
                HTML += this.getExportHTML();
                break;
            case 'export':
                HTML = this.getExportHTML();
                break;

            default:
                break;
        }

        HTML += '</div>';

        $(el).append(HTML);
    }

    open(el: Element): void {
        this.render(el);
        this.isSubMenuOpened = true;
        this.listener();
    }

    close(): void {
        $('#' + AeviToolbarSubMenu.SUBMENU_ID).detach();
        this.isSubMenuOpened = false;
    }

    listener(): void {
        $(document).off('click', '#' + AeviToolbarSubMenu.SUBMENU_ID + ' > *').on('click', '#' + AeviToolbarSubMenu.SUBMENU_ID + ' > *', (event) => {
            var btn = <HTMLElement>event.currentTarget;
            var action = btn.getAttribute('data-action');

            switch (action) {
                case 'export':
                    var exportType:string = btn.getAttribute('data-type');

                    switch (exportType) {
                        case 'csv':
                            return this.AeviGrid.AeviDownloader.exportedCsv();

                        case 'patternexcel':
                            return this.AeviGrid.AeviDownloader.exportedPatternXls();

                        case 'xlsx':
                            return this.AeviGrid.AeviDownloader.exportedXlsx();

                        default:
                            break;
                    }
                    break;

                case 'import':
                    setTimeout(() => this.close());
                    this.AeviGrid.AeviToolbar.AeviToolbarHandler.importFile();
                    break;

                default:
                    break;
            }
        });

        $(document).on('click', 'body', () => {
            this.close();
        });
    }

    isOpened(): boolean {
        return this.isSubMenuOpened;
    }

    getImportHTML(): string {
        var HTML: string = '';

        if (this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.isReport() || this.AeviGrid.AeviDataRepository.AeviTableDescription.isFixed()) {
            return HTML;
        }

        if (!this.AeviGrid.AeviGridLocker.isLocked) {
            HTML = '<span data-action="import">' + this.localization.translate('toolbar_import') + '</span>';
        }

        return HTML;
    }

    getExportHTML(): string {
        var HTML: string = '';
        var exportFormats: string[] = [
            'csv',
            'xlsx'
        ];

        if (this.AeviGrid.excelFilePath !== '' && this.AeviGrid.AeviDataRepository.AeviTableDescription.isAllowedPastingFromExcel()) {
            if(this.AeviGrid.mode.clipboard === 0) {
                exportFormats.push('patternexcel');
            }
        }

        for (var i = 0; i < exportFormats.length; i++) {
            var itemName = this.localization.translate('toolbar_export') + ' .' + exportFormats[i];

            if (exportFormats[i] === 'patternexcel') {
                itemName = this.AeviGrid.AeviLocalization.translate('toolbar_xls');
            }

            HTML += '<span data-action="export" data-type="' + exportFormats[i] + '">' + itemName + '</span>';
        }

        return HTML;
    }
}
