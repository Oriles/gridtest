/// <reference path='../references.ts' />

class AeviDOM {
	AeviGrid: any;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
	}

	length(selector: string): boolean {
		if(aeviIsUndefinedOrNull(selector) || !_.isString(selector)) {
			this.AeviGrid.print('AeviDOM.length(), parameter "selector" is undefined or null or is not string.');
			return false;
		}

		return document.querySelectorAll(selector).length >= 1;
	}

	getById(id: string): HTMLElement {
		return <HTMLElement>document.getElementById(id);
	}

	getEl(selector: string): Node {
		if(aeviIsUndefinedOrNull(selector) || !_.isString(selector)) {
			this.AeviGrid.print('AeviDOM.length(), parameter "selector" is undefined or null or is not string.');
			return null;
		}

		return document.querySelector(selector);
	}

	getEls(selector: string): NodeList {
		if(aeviIsUndefinedOrNull(selector) || !_.isString(selector)) {
			this.AeviGrid.print('AeviDOM.length(), parameter "selector" is undefined or null or is not string.');
			return null;
		}

		return document.querySelectorAll(selector);
	}

	/**
	 * isPure detect is element is instance of HTMLElement
	 */
	isPure(element): boolean {
		return (element instanceof HTMLElement);
	}

	getTagName(element): string {
		if(this.isPure(element))
			return element.tagName;
		return element[0].tagName;
	}

    getClass(element): string {
        if(this.isPure(element))
            return element.getAttribute('class');
        return element[0].getAttribute('class');
    }

	append(element: HTMLElement, appendHTML: string): void {
		var div = document.createElement('div');
		div.innerHTML = appendHTML;
		
		while (div.children.length > 0) {
			element.appendChild(div.children[0]);
		}
	}

	show(el: HTMLElement) {
		el.style.display = 'block';
	}

	hide(el: HTMLElement) {
		el.style.display = 'none';
	}

	closeModals() {
		AeviModal.closeAll();
	}

	getVal(el: any) {
		return el.value;
	}

	getPureTable(): HTMLElement {
		return document.getElementById(this.AeviGrid.aeviTableId);
	}

	getCell(cellIndex: number, rowIndex: number): JQuery {
		return $(this.getPureCell(cellIndex, rowIndex));
	}

	getPureCell(cellIndex: number, rowIndex: number): HTMLElement {
		return document.getElementById('cell-' + rowIndex + '.' + cellIndex);
	}

	getRow(rowIndex: number): JQuery {
		return $(this.getPureRow(rowIndex));
	}

    getPureRow(rowIndex: number): HTMLTableRowElement {
		return <HTMLTableRowElement>document.getElementById('row-' + rowIndex);
	}

    getSelectedRow() {
        return document.querySelector('tr' + this.AeviGrid.AeviConsts.statesSelector.selected);
    }

	getSelectedRowsAndCells(): NodeListOf<Element> {
		return document.querySelectorAll(this.AeviGrid.AeviConsts.statesSelector.selected + ',' + this.AeviGrid.AeviConsts.statesSelector.tinge);
	}

	/**
	 * @param  {any} row HTML element or JQuery
	 */
	getRowIndex(row: any): number {
		var stringIndex = null;

		if (this.isPure(row)) {
			if (_.isNull(row))
				return stringIndex;
			else
				stringIndex = row.getAttribute('id').split('-')[1];
		} else {
			if (!row.length)
				return stringIndex;
			else
				stringIndex = row.attr('id').split('-')[1];
		}

		return parseInt(stringIndex);
	}

	/**
	 * @param  {any} cell HTML element or JQuery
	 */
	getCellIndex(cell: any): number {
		var stringIndex = null;

		if (this.isPure(cell))
			stringIndex = cell.getAttribute('id').split('-')[1].split('.')[1];
		else
			stringIndex = cell.attr('id').split('-')[1].split('.')[1];

		return parseInt(stringIndex);
	}

	getCellParentIndex(cell: HTMLElement): number {
		var indexes = this.getCellIndexes(cell);
		var row: HTMLTableRowElement = this.getPureRow(indexes.rowId);
		return this.getRowIndex(row);
	}

	getCellIndexes(object: any): IAeviCellIndex {
		if (aeviIsUndefinedOrNull(object) || object.length < 1) {
			this.AeviGrid.print('AeviDOM.getCellIndexes(), variable "object" is undefined or null or empty');
			return null;
		}

        var attr: string;

		if (this.isPure(object))
			attr = object.id;
		else if (_.isString(object))
			attr = object;
		else
			attr = object.attr('id');

		return this.getSplittedCellIndexes(attr);
	}

	getSplittedCellIndexes(attr): IAeviCellIndex {
		var pieces = attr;

		if (attr.indexOf('-') !== -1)
			pieces = attr.split('-')[1];

		pieces = pieces.split('.');

		return {
			rowId: parseInt(pieces[0]),
			cellId: parseInt(pieces[1]),
			rowIndex: parseInt(pieces[0]),
			cellIndex: parseInt(pieces[1]),
		};
	}

    getCellByTab(cellObject: JQuery, direction: string): JQuery {
		var aeviDataRepository = this.AeviGrid.AeviDataRepository;

		var indexes: IAeviCellIndex = this.getCellIndexes(cellObject);

		var rowsLength = aeviDataRepository.AeviTableData.getRecordsLength();
		var visibleCellsLength = aeviDataRepository.AeviTableDescription.getVisibleCellsLength() - 1;

        var isFirstCell = (indexes.cellIndex === 1);
		var isLastCell = (indexes.cellIndex === visibleCellsLength);
		var isLastRow = (indexes.rowIndex + 1 === rowsLength);

        if (direction == 'tab') {
			if (isLastCell) {
				if (!isLastRow) {
					indexes.cellIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getRowNumberIndex();
					indexes.rowIndex += 1;
				}
			} else {
				indexes.cellIndex += 1;
			}
		}

		if (direction == 'shiftTab') {
			if (isFirstCell) {
				indexes.rowIndex -= 1;
				indexes.cellIndex = visibleCellsLength;
			} else {
				indexes.cellIndex -= 1;
			}
		}

		if (indexes.rowIndex === -1) {
			indexes.rowIndex = 0;
			indexes.cellIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getRowNumberIndex();
		}

		//console.log(cell.cellIndex + ' | ' + cell.rowIndex);
		return this.getCell(indexes.cellIndex, indexes.rowIndex);
	}

	getCellByArrow(cellObject: JQuery, direction: string): JQuery {
		var cell = this.getCellIndexes(cellObject);

		if (direction == 'down')
			cell.rowId += 1;

		if (direction == 'up')
			cell.rowId -= 1;

		if (cell.rowId === -1)
			cell.rowId = 0;

		return this.getCell(cell.cellId, cell.rowId);
	}

	isModalVisible() {
		return $(this.AeviGrid.AeviConsts.modalWindowSelectorId).length;
	}

    isFormVisible() {
        return this.length(AeviConsts.form);
    }

    isSearchVisible() {
        return this.length(this.AeviGrid.AeviConsts.searchSelector);
    }

    getSelectedCell() {
		return document.querySelector('#aeviTable tbody tr .aeviCell.selected');
	}

	isRowSelected() {
		return ($(this.AeviGrid.AeviConsts.rowNumberSelector).hasClass(this.AeviGrid.AeviConsts.statuses.selected));
	}

	isCellSelected() {
		return ($(this.AeviGrid.AeviConsts.cellSelector).hasClass(this.AeviGrid.AeviConsts.statuses.selected));
	}

	isContextMenuVisible(): number {
		return $(this.AeviGrid.AeviConsts.contextMenuSelector).length;
	}

	hideAllContextMenus(): void {
		$(this.AeviGrid.AeviConsts.contextMenuSelector).hide();
	}

    getSelectedRowIndex(): number {
        var selectedRow = this.getSelectedRow();
        return (_.isNull(selectedRow)) ? null : this.getRowIndex(selectedRow);
    }

    static getColumnIndex(column: JQuery) {
		return parseInt(column[0].getAttribute('id').split('-')[1]);
	}

    static addClass(el: HTMLElement, className: string): void {
        el.classList.add(className);
    }

    static removeClass(el: HTMLElement, className: string): void {
        el.classList.remove(className);
    }

    static focus(el: HTMLElement|JQuery): void {
        el.focus();
    }

    focusWithoutScroll(el: HTMLElement|JQuery) {
        var x = window.scrollX, y = window.scrollY;

        if(this.AeviGrid.AeviClientSide.isFirefox()) {
            AeviDOM.disableScroll();

            setTimeout(() => {
                el.focus();
                setTimeout(() => {
                    window.scrollTo(x, y);
                    AeviDOM.enableScroll();
                }, 5);
            });
        } else if (this.AeviGrid.AeviClientSide.isIe() || this.AeviGrid.AeviClientSide.isEdge()) {
            el.focus();
        } else {
            AeviDOM.disableScroll();
            el.focus();
            window.scrollTo(x, y);
            AeviDOM.enableScroll();
        }

    }

    static enableScroll(){
        window.onscroll = function(){};
    }

    static disableScroll() {
        var x = window.scrollX;
        var y = window.scrollY;
        window.onscroll = function(){ window.scrollTo(x, y); };
    }
}
