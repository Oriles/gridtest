/// <reference path='../references.ts' />

class AeviDownloader {
    AeviGrid: any;

	constructor(aeviGrid: any) {
        this.AeviGrid = aeviGrid;
	}

    download(url: string): any {
        return window.open(url, '_blank');
    }

    pdf() {
        var JSONFilter = encodeURIComponent(this.AeviGrid.AeviFilter.getJSONFilterValues());
        var JSONSortInfos = this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.getJSONSortInfos();
        var url = this.AeviGrid.AeviDataService.AeviApiService.getDownloadFileUrl(JSONFilter, JSONSortInfos, 'Pdf');
        this.download(url);
    }

    exportedPatternXls() {
        this.download(this.AeviGrid.excelFilePath);
    }

    exportedCsv() {
        this.download(this.AeviGrid.AeviDataService.AeviApiService.getExportFileUrl('Csv'));
    }

    exportedXls() {
        this.download(this.AeviGrid.AeviDataService.AeviApiService.getExportFileUrl('Xls'));
    }

    exportedXlsx() {
        this.download(this.AeviGrid.AeviDataService.AeviApiService.getExportFileUrl('Xlsx'));
    }
}
