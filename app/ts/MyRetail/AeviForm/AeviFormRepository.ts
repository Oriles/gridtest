/// <reference path='../../references.ts' />

class AeviFormRepository {
	AeviForm: any;
    AeviDataRepository: any;
    rowIndex: number;
    items: any;

	constructor(aeviForm: any, aeviDataRepository: any, rowIndex: number) {
		this.AeviForm = aeviForm;
        this.AeviDataRepository = aeviDataRepository;
        this.rowIndex = rowIndex;

        var rows: IAeviRowDescription[] = this.AeviDataRepository.AeviFormDescription.getRows();
        this.items = {};

        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];

            for (var j = 0; j < row.Items.length; j++) {
                var item: IAeviExtendedItemDescription = <IAeviExtendedItemDescription>_.clone(row.Items[j]);

                if (AeviFormRepository.isEmpty(item)) {
                    this.items[item.ColumnName] = _.clone(item);
                    continue;
                }

                var columnIndex = this.AeviDataRepository.AeviTableDescription.getColumnIndexByName(item.ColumnName);
                var displayType = this.AeviDataRepository.AeviTableDescription.getColumnStringDisplayTypeByIndex(columnIndex);

                if (_.isNull(columnIndex)) {
                    this.AeviForm.AeviGrid.print('Form item with ColumnName: "' + item.ColumnName + '" is not in TableDescription.');
                    continue;
                }

                var value = null;

                switch (displayType) {
                    case 'Image':
                        var guid = this.AeviDataRepository.AeviTableData.getGuidByRowIndex(this.rowIndex);
                        value = this.AeviDataRepository.AeviGrid.AeviDataService.getImageData(guid, columnIndex);
                        break;

                    default:
                        value = this.AeviDataRepository.AeviTableData.getRecordCellValue(this.rowIndex, columnIndex);
                        break;
                }

                item.Value = value;
                item.RowIndex = i;
                item.ColumnIndex = columnIndex;
                item.IsRequired = this.AeviDataRepository.AeviTableDescription.isRequiredByColumnIndex(columnIndex);

                this.items[item.ColumnName] = item;
            }
        }
	}

    static isEmpty(item) {
        return (_.isNull(item.ColumnName) || _.isEmpty(item.ColumnName));
    }
}
