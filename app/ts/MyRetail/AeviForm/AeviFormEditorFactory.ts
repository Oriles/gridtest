/// <reference path='../../references.ts' />

class AeviFormEditorFactory {
    AeviGrid: any;
    AeviDataRepository: IAeviDataRepository;
    editor: any;

    constructor(aeviGrid: any) {
        this.AeviGrid = aeviGrid;
        this.AeviDataRepository = aeviGrid.AeviDataRepository;
    }

    createFormEditor($cell: JQuery, indexes: IAeviCellIndex): void {
        var column = this.AeviDataRepository.AeviTableDescription.getColumnHeader(indexes.cellIndex);

        if (aeviIsUndefinedOrNull(column)) {
            return;
        }

        var referenceSettingsColumn = this.AeviDataRepository.getReferenceSettings(indexes.rowIndex, indexes.cellIndex);

        if (!_.isNull(referenceSettingsColumn)) {
            column = referenceSettingsColumn;
        }

        if (!aeviIsUndefinedOrNull(column.EntityActionDefinition)) {
            return;
        }

        var displayType = this.AeviGrid.AeviConsts.dataTypes[column.DisplayType];

        switch (displayType) {
            case 'Image':
                this.editor = new AeviImageEditor(this.AeviGrid, $cell);
                break;

            case 'Text':
                this.editor = new AeviTextEditor(this.AeviGrid, $cell);
                break;

            case 'Number':
            case 'IntegerNumber':
                this.editor = new AeviNumberEditor(this.AeviGrid, $cell, displayType);
                break;

            case 'Currency':
                this.editor = new AeviCurrencyEditor(this.AeviGrid, $cell);
                break;

            case 'DateTime':
            case 'ShortDate':
            case 'ShortTime':
                this.editor = new AeviDateEditor(this.AeviGrid, $cell, displayType);
                break;

            case 'Enum':
                this.editor = new AeviEnumEditor(this.AeviGrid, $cell, column);
                break;

            case 'Boolean':
            case 'Hyperlink':
            case 'RegularExpression':
                this.editor = null;
                this.AeviGrid.print('AeviEditorFactory.createEditor(), "' + displayType + '" editor is not supported.');
                break;

            default:
                this.editor = null;
                break;
        }
    }

    getEditor() {
        return this.editor;
    }
}
