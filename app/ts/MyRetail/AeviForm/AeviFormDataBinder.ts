/// <reference path='../../references.ts' />

class AeviFormDataBinder {
    AeviFormRepository: any;
    columnName: string;

    constructor(aeviFormRepository: any) {
        this.AeviFormRepository = aeviFormRepository;
    }

    subscribe(columnName) {
        this.columnName = columnName;
    }

    publish(value) {
        this.AeviFormRepository.items[this.columnName].Value = value;
    }
}
