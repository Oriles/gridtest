/// <reference path='../../references.ts' />

class AeviFormView {
	static getMainTemplate() {
        return _.template(
            '<form class="aeviForm" id="aeviForm-<%= RecordIndex %>">' +
                '<fieldset class="well aeviForm__pager">' +
                    '<div>' +
                        '<%= PreviousTemplate %>' +
                        '<%= NextTemplate %>' +
                    '</div>' +
                '</fieldset>' +
                '<legend><%= EntityCaption %>, #<%= RowNumber %></legend>' +
                '<fieldset class="well aeviForm__content">' +
                    '<%= Rows %>' +
                '</fieldset>' +
                '<div class="cfx text-right">' +
                    '<button type="submit" class="aeviButton aeviButton--black aeviForm-<%= RecordIndex %>-close"><%= SaveWithoutClose %></button>' +
                    '<%= SaveTemplate %>' +
                '</div>' +
                '<a class="aeviForm__close aeviForm-<%= RecordIndex %>-close"></a>' +
            '</form>' +
            '<div class="aeviForm__overlay"></div>'
        );
    }

    static getPreviousArrowTemplate() {
        return _.template('<span id="aeviForm-<%= RecordIndex %>-left" class="aeviForm__left pull-left"><%= Previous %></span>');
    }

    static getNextArrowTemplate() {
        return _.template('<span id="aeviForm-<%= RecordIndex %>-right" class="aeviForm__right pull-right"><%= Next %></span>');
    }

    static getRowTemplate() {
        return _.template(
            '<div class="row">' +
                '   <%= Columns %>' +
            '</div>'
        );
    }

    static getSaveButtonTemplate() {
        return _.template('<button id="aeviForm-<%= RecordIndex %>-save" type="submit" class="aeviButton aeviButton--secondary"><%= Save %></button>');
    }

    static getColumnTemplate() {
        return _.template(
            '<div class="form-group <%= Class %>" style="max-height: <%= MaxHeight %>;">' +
                '<label for="aeviForm-<%= ColumnName %>"><%= Caption %> <%= Required %></label>' +
                '<div class="aeviForm__editorWrapper"><%= EditorHTML %></div>' +
            '</div>'
        );
    }

    static getEmptyColumnTemplate() {
        return _.template(
            '<div class="aeviForm__col col-md-<%= ColumnWidth %>">' +
                '<%= Form %>' +
            '</div>'
        );
    }

    static getEmptyTemplate() {
        return _.template('');
    }
}
