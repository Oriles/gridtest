/// <reference path='../../references.ts' />

class AeviForm {
	rowIndex: number;
    AeviGrid: any;
	AeviDataService: IAeviDataService;
    AeviFormRepository: any;
    AeviEditorFactory: any;
    form: HTMLElement;
    $form: JQuery;
    rows: IAeviRowDescription[];
    isFirstRecordVisible: boolean;
    isLastRecordVisible: boolean;
    showInReadOnlyMode: boolean;
    handler: any;
    editors: any[];
    isOpenedByToolbar: boolean;

	constructor(rowIndex: number, aeviDataService: IAeviDataService, options: any) {
		this.rowIndex = rowIndex;
        this.AeviGrid = aeviDataService.AeviGrid;
        this.AeviDataService = aeviDataService;
        this.isOpenedByToolbar = options.isOpenedByToolbar;

        this.rows = this.AeviDataService.AeviDataRepository.AeviFormDescription.getRows();

        this.AeviFormRepository = new AeviFormRepository(this, this.AeviDataService.AeviDataRepository, this.rowIndex);

        this.showInReadOnlyMode = (this.AeviGrid.AeviGridLocker.isLocked || this.AeviDataService.AeviDataRepository.AeviTableDescription.isReadOnly());
        this.editors = [];

        this.render();
        this.handler = new AeviFormHandler(this);
	}

    render() {
        var fakeCell = '<div id="aeviFormFakeCell"><div></div></div>';
        var body: JQuery = $('body');

        body.append(fakeCell);

        var template = AeviFormView.getMainTemplate();
        var previousTemplate = null;
        var nextTemplate = null;

        if (this.rowIndex <= 0) {
            this.isFirstRecordVisible = true;
            previousTemplate = AeviFormView.getEmptyTemplate();
        } else {
            this.isFirstRecordVisible = false;
            previousTemplate = AeviFormView.getPreviousArrowTemplate();
        }

        var saveTemplate = (this.showInReadOnlyMode) ? AeviFormView.getEmptyTemplate() : AeviFormView.getSaveButtonTemplate();

        var nextRowStatus = this.AeviDataService.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(this.rowIndex + 1);

        if ((nextRowStatus.indexOf('newrow') !== -1 || nextRowStatus.indexOf('hidden') !== -1) && nextRowStatus.indexOf('sortedRow') < 0) {
            this.isLastRecordVisible = true;
            nextTemplate = AeviFormView.getEmptyTemplate();
        } else {
            this.isLastRecordVisible = false;
            nextTemplate = AeviFormView.getNextArrowTemplate();
        }

        this.AeviEditorFactory = new AeviFormEditorFactory(this.AeviDataService.AeviGrid);

        var rows = [];

        for (var i = 0; i < this.rows.length; i++) {
            rows.push(this.renderRow(i));
        }

        var HTML: string = template({
            ReadOnly: this.showInReadOnlyMode,
            EntityCaption: this.AeviDataService.AeviDataRepository.AeviDataEntity.data.Caption,
            RecordIndex: this.rowIndex,
            RowNumber: this.rowIndex + 1,
            Rows: rows.join(''),
            SaveWithoutClose: this.AeviGrid.AeviLocalization.translate('close_without_save'),
            PreviousTemplate: previousTemplate({RecordIndex: this.rowIndex, Previous: this.AeviGrid.AeviLocalization.translate('previous') }),
            NextTemplate: nextTemplate({RecordIndex: this.rowIndex, Next: this.AeviGrid.AeviLocalization.translate('next') }),
            SaveTemplate: saveTemplate({RecordIndex: this.rowIndex, Save: this.AeviGrid.AeviLocalization.translate('save_and_close')})
        });

        body.append(HTML);

        this.form = document.getElementById('aeviForm-' + this.rowIndex);
        this.$form = $(this.form);
    }

    refresh() {
        this.destroy();
        return new AeviForm(this.rowIndex, this.AeviDataService, {isOpenedByToolbar: this.isOpenedByToolbar});
    }

    renderRow(rowIndex: number): string {
        var template = AeviFormView.getRowTemplate();

        var columnsLength: number = this.rows[rowIndex].Items.length;

        var columns = [];

        for (var columnIndex = 0; columnIndex < columnsLength; columnIndex++) {
            var rowColumn = this.rows[rowIndex].Items[columnIndex];

            if (!rowColumn.Visible) {
                continue;
            }

            if (_.isEmpty(rowColumn.ColumnName) || _.isNull(rowColumn.ColumnName)) {
                columns.push(AeviForm.renderEmptyColumn(rowColumn));
                continue;
            }

            var column: IAeviExtendedItemDescription = this.AeviFormRepository.items[rowColumn.ColumnName];

            if (!aeviIsUndefinedOrNull(column)) {
                column.IsReadOnly = this.showInReadOnlyMode;
            }

            columns.push(this.renderColumn(column));
        }

        return template({
            Columns: columns.join('')
        });
    }

    renderColumn(column: IAeviExtendedItemDescription): string {
        if (aeviIsUndefinedOrNull(column)) {
           return '';
        }

        var emptyColumnTemplate = AeviFormView.getEmptyColumnTemplate();

        var fakeIndexes: IAeviCellIndex = {
            rowIndex: this.rowIndex,
            rowId: this.rowIndex,
            cellIndex: column.ColumnIndex,
            cellId: column.ColumnIndex
        };

        this.AeviEditorFactory.createFormEditor($('#aeviFormFakeCell'), fakeIndexes);
        var editor = this.AeviEditorFactory.getEditor();

        this.editors.push(editor);

        var columnTemplate = AeviFormView.getColumnTemplate();

        var editorHTML = editor.renderFormEditor(column, fakeIndexes);

        var columnHTML = columnTemplate({
            Caption: column.Caption,
            ColumnName: column.ColumnName,
            Class: column.Style,
            MaxHeight: (!column.MaxHeight || _.isNull(column.MaxHeight)) ? 'none' : column.MaxHeight + 'px',
            EditorHTML: editorHTML,
            Required: (column.IsRequired) ? '*' : ''
        });

        //console.log(columnHTML);

        return emptyColumnTemplate({
            ColumnWidth: column.Width,
            Form: columnHTML
        });
    }

    static renderEmptyColumn(column: IAeviItemDescription) {
        var emptyColumnTemplate = AeviFormView.getEmptyColumnTemplate();
        return emptyColumnTemplate({
            ColumnWidth: column.Width,
            Form: ''
        });
    }

    updateRecord() {
        for (var key in this.AeviFormRepository.items) {
            var item: IAeviExtendedItemDescription = this.AeviFormRepository.items[key];

            var columnIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnIndexByName(item.ColumnName);
            var displayType = this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnStringDisplayTypeByIndex(columnIndex);

            switch (displayType) {
                case 'Image':
                    // image implementation is in ImageEditor.formListener()
                    break;
                default:
                    if (!AeviFormRepository.isEmpty(item)) {
                        var columnIndex = this.AeviDataService.AeviDataRepository.AeviTableDescription.getColumnIndexByName(item.ColumnName);
                        this.AeviDataService.AeviDataRepository.AeviTableData.updateRecordCellValue(this.rowIndex, columnIndex, item.Value);
                    }

                    break;
            }
        }
    }

    destroy() {
        this.$form.remove();
        $('.aeviForm__overlay, #aeviFormFakeCell').remove();
    }

    goToByRowInput() {
        var input = <HTMLInputElement>document.getElementById('aeviForm-' + this.rowIndex + '-rowInput');
        var rowIndex: number = parseInt(input.value) - 1;

        if (_.isNaN(rowIndex)) {
            return;
        }

        if (rowIndex < 0) {
            rowIndex = 0;
        }

        var visibleRecordsLength = this.AeviDataService.AeviDataRepository.AeviTableData.getVisibleRecordsLength();

        if (rowIndex > visibleRecordsLength - 1) {
            rowIndex = visibleRecordsLength - 1;
        }

        this.destroy();
        return new AeviForm(rowIndex, this.AeviDataService, {isOpenedByToolbar: this.isOpenedByToolbar});
    }

    setImageDefaultValue() {
        for (var key in this.AeviFormRepository.items) {
            var item: IAeviExtendedItemDescription = this.AeviFormRepository.items[key];

            var columnIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnIndexByName(item.ColumnName);
            var displayType = this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnStringDisplayTypeByIndex(columnIndex);
            var defaultValue = null;

            switch (displayType) {
                case 'Image':
                    var editor = this.getEditorByColumnName(item.ColumnName);
                    defaultValue = editor.defaultValue;
                    break;

                default:
                    break;
            }

            if (!_.isNull(defaultValue)) {
                this.AeviGrid.AeviDataRepository.AeviTableData.updateRecordCellValue(this.rowIndex, columnIndex, defaultValue);
            }
        }
    }

    getEditorByColumnName(columnName: string) {
        for (var i = 0; i < this.editors.length; i++) {
            if (aeviIsUndefinedOrNull(this.editors[i].columnName)) {
                continue;
            }

            if (this.editors[i].columnName === columnName) {
                return this.editors[i];
            }
        }

        return null;
    }
}
