/// <reference path='../../../references.ts' />
/// <reference path='../IAeviPlugin.ts' />

class AeviActionPricePlugin implements IAeviPlugin  {
    AeviGridApi: any;
    columnNames: any;
    
    constructor(aeviGridApi: any) {
        this.AeviGridApi = aeviGridApi;

        this.columnNames = {
            action: 'ActionPrice',
            actionSince: 'ActionSince',
            actionTill: 'ActionTill'
        };
    }

    onCellLeave(rowIndex: number, columnIndex: number, enterValue: string, leaveValue: string) {
        enterValue = (_.isNull(enterValue) || enterValue === 'null') ? null : enterValue;
        leaveValue = (_.isNull(leaveValue) || leaveValue === 'null') ? null : leaveValue;

        var column = this.AeviGridApi.getColumnHeader(columnIndex);

        if (this.isColumnNameActionPrice(column.ColumnName)) {
            if (!_.isNull(enterValue) || _.isNull(leaveValue))
                return;
            
            var todaysISODate = this.getTodaysISODate();

            var actionSinceIndex = this.AeviGridApi.getColumnHeaderIndexByName(this.columnNames.actionSince) + 1;
            this.AeviGridApi.updateCellValue(rowIndex, actionSinceIndex, todaysISODate);

            var actionTillIndex = this.AeviGridApi.getColumnHeaderIndexByName(this.columnNames.actionTill) + 1;
            this.AeviGridApi.updateCellValue(rowIndex, actionTillIndex, todaysISODate);

            this.AeviGridApi.refreshRow(rowIndex);
        }
    }

    isColumnNameActionPrice(columnName: string) {
        if (columnName.substring(0, 11) === this.columnNames.action)
            return true;
        return false;
    }

    getTodaysISODate() {
       return new Date().toISOString();
    }

    onDataLoaded() {/**/}
    onRowsChanged() {/**/}
    onRowsDeleting() {/**/}
}
