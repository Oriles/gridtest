/// <reference path='../../references.ts' />

interface IAeviPlugin {
    AeviGridApi: any;
    onDataLoaded(...args: any[]): void;
    onRowsChanged(...args: any[]): void;
    onRowsDeleting(...args: any[]): void;
    onCellLeave(...args: any[]): void;
}
