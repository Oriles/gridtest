class AeviPluginFactory {
	AeviGridApi: any;

	constructor(aeviGridApi: any) {
		this.AeviGridApi = aeviGridApi;	
	}

	get(pluginName: string) {
		var instanceName: string = 'Aevi' + pluginName + 'Plugin';
		var plugin: any = null;

		try {
			plugin = new window[instanceName](this.AeviGridApi);	
		}catch (err) {
			this.AeviGridApi.log('AeviPluginFactory.get(), plugin with name: ' + pluginName + ' doesnt exists.');
		}

		return plugin;
	}	
}
