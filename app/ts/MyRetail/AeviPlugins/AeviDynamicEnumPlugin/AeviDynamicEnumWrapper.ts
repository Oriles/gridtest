/// <reference path='../../../references.ts' />

class AeviDynamicEnumWrapper {
    originalEnumValues: any[];
    dynamicEnum: any;
    tableDescription: any;
    dataValueColumnName: string;
    displayValueColumnName: string;

    dataValueColumnIndex = null;
    displayValueColumnIndex = null;

    _isValid: boolean;

    constructor(dynamicEnum: any, tableDescription: any) {
        this.originalEnumValues = [];
        this.dynamicEnum = dynamicEnum;
        this.tableDescription = tableDescription;

        this.dataValueColumnName = dynamicEnum.DynamicEnumSettings.DataValueColumnName;
        this.displayValueColumnName = dynamicEnum.DynamicEnumSettings.DisplayValueColumnName;

        this.dataValueColumnIndex = null;
        this.displayValueColumnIndex = null;

        this.init();
    }

    init(): void {
        this._isValid = this.validate();
        
        if(!this._isValid)
            return;

        this.originalEnumValues = this.dynamicEnum.EnumValues.slice();
    }

    refreshOriginalEnumValues(): void {
        if(this.isValid()) {
            this.dynamicEnum.EnumValues = this.originalEnumValues.slice();
        }        
    }

    fillColumnsIndexes(): boolean {
        for (var i = 0; i < this.tableDescription.Columns.length; i++) {
            var column = this.tableDescription.Columns[i];

            //TODO add global logic to obtain real index
            var indexDeviation = 1;

            if (column.ColumnName == this.dataValueColumnName) {
                //TODO add global logic to obtain real index
                this.dataValueColumnIndex = i + indexDeviation;
            }
            else if (column.ColumnName == this.displayValueColumnName) {
                this.displayValueColumnIndex = i + indexDeviation;
            }
        }

        return (!aeviIsUndefinedOrNull(this.dataValueColumnIndex) && !aeviIsUndefinedOrNull(this.displayValueColumnIndex));
    }

    getDynamicEnumValue(row: any) {
        if (aeviIsUndefinedOrNull(row) || row.length - 1 < this.dataValueColumnIndex || row.length - 1 < this.displayValueColumnIndex) {
            console.log("Row is invalid");
            return null;
        }

        return {
            DataValue: row[this.dataValueColumnIndex],
            DisplayValue: row[this.displayValueColumnIndex]
        };
    }

    addOrUpdateEnumsValues(row: any) {
        this.addUpdateOrDeleteEnumsValues(row, 'insertUpdate');
    }

    deleteEnumsValues(row: any) {
        this.addUpdateOrDeleteEnumsValues(row, 'delete');
    };

    addUpdateOrDeleteEnumsValues(row: any, action: string) {
        if (!this.isValid()) {
            return;
        }

        if (aeviIsUndefinedOrNull(row)) {
            return;
        }

        var foundEnumValue = false;
        var _this = this;
        
        var dynamicEnumValue = _this.getDynamicEnumValue(row);
        if (!dynamicEnumValue) {
            return;
        }

        for (var i = 0; i < _this.dynamicEnum.EnumValues.length; i++) {
            var enumValue = _this.dynamicEnum.EnumValues[i];

            if (enumValue.DataValue === dynamicEnumValue.DataValue) {
                if (action === 'delete') {
                    _this.dynamicEnum.EnumValues.splice(i, 1);
                    return;
                } else {
                    foundEnumValue = true;
                    enumValue.DisplayValue = dynamicEnumValue.DisplayValue;
                    break;
                }
            }
        }

        if (!foundEnumValue) {
            _this.dynamicEnum.EnumValues.push({
                DisplayValue: dynamicEnumValue.DisplayValue,
                DataValue: dynamicEnumValue.DataValue
            });
        }
    };

    isValid(): boolean {
        if (!this._isValid) {
            console.log("AeviDynamicEnumWrapper is not valid");
            return false;
        }

        return this._isValid;
    }

    validate(): boolean {
        if (aeviIsUndefinedOrNull(this.dynamicEnum)) {
            console.log('this.dynamicEnum is null');
            return false;
        }

        if (aeviIsUndefinedOrNull(this.dynamicEnum.EnumValues)) {
            console.log('this.dynamicEnum.EnumValues is null');
            return false;
        }

        if (aeviIsUndefinedOrNull(this.tableDescription)) {
            console.log('this.tableDescription is null');
            return false;
        }

        if (aeviIsUndefinedOrNull(this.tableDescription.Columns) || this.tableDescription.Columns.length === 0) {
            console.log('this.tableDescription.Columns is null');
            return false;
        }

        if (!this.fillColumnsIndexes()) {
            console.log('Cannot find indexes for following columns: ' + this.dataValueColumnName + ' and ' + this.displayValueColumnName);
            return false;
        }

        return true;
    }
}
