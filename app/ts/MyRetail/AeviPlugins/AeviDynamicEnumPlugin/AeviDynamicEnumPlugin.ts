/// <reference path='../../../references.ts' />
/// <reference path='../IAeviPlugin.ts' />
/// <reference path='AeviDynamicEnumWrapper.ts' />

class AeviDynamicEnumPlugin implements IAeviPlugin  {
    AeviGridApi: any;
    columnNames: any;
    isInitialized: boolean;
    dynamicEnumWrappers: any[];
    tableDescription: any;
    
    constructor(aeviGridApi: any) {
        this.AeviGridApi = aeviGridApi;
    }

    init():void {
        if (!this.isInitialized) {
            this.tableDescription = this.AeviGridApi.getTableDescription();
            this.fillDynamicEnums();
            this.isInitialized = true;
        }

        if (aeviIsUndefinedOrNull(this.dynamicEnumWrappers) || this.dynamicEnumWrappers.length === 0) {
            return;
        }

        var data: any[] = this.AeviGridApi.getTableData();

        if (aeviIsUndefinedOrNull(data) || data.length === 0) {
            return;
        }

        if (this.isInitialized === true) {
            this.refreshOriginalEnumsValues();
        }

        this.addUpdateOrDeleteEnumsValues(data, 'insertUpdate');
    }

    onDataLoaded(): void {
        this.init();
    }

    onRowsChanged(rows: any[]): void {
        this.addUpdateOrDeleteEnumsValues(rows, 'insertUpdate');
    }

    onRowsDeleting(rows: any[]) {
        this.addUpdateOrDeleteEnumsValues(rows, 'delete');
    }

    onCellLeave(): void {/**/}

    fillDynamicEnums(): void {
        if (!aeviIsUndefinedOrNull(this.tableDescription) || !aeviIsUndefinedOrNull(this.tableDescription.Columns)) {
            var dataTypes = this.AeviGridApi.getColumnDisplayTypes();
            if (aeviIsUndefinedOrNull(dataTypes)) {
                this.AeviGridApi.log('dataTypes is null');
                return;
            }

            for (var i = 0; i < this.tableDescription.Columns.length; i++) {
                var column = this.tableDescription.Columns[i];
                
                if(this.isDynamicEnum(column, dataTypes)) {
                    this.dynamicEnumWrappers.push(
                        new AeviDynamicEnumWrapper(column, this.tableDescription)
                    );
                }
            }
        }
    }

    refreshOriginalEnumsValues(): void {
        for (var i = 0; i < this.dynamicEnumWrappers.length; i++) {
            this.dynamicEnumWrappers[i].refreshOriginalEnumValues();
        }
    }

    addUpdateOrDeleteEnumsValues (rows: any[], action: string): void {
        if ((aeviIsUndefinedOrNull(rows) ||
            rows.length === 0) ||
            aeviIsUndefinedOrNull(this.dynamicEnumWrappers) ||
            this.dynamicEnumWrappers.length === 0) {
            return;
        }

        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            for (var j = 0; j < this.dynamicEnumWrappers.length; j++) {
                if (action === 'delete') {
                    this.dynamicEnumWrappers[j].deleteEnumsValues(row);
                } else {
                    this.dynamicEnumWrappers[j].addOrUpdateEnumsValues(row);
                }
            }
        }
    }

    isDynamicEnum(column: any, dataTypes: any) {
        var displayType = dataTypes[column.DisplayType];
        return (displayType === 'Enum' && !aeviIsUndefinedOrNull(column.DynamicEnumSettings) && !aeviIsUndefinedOrNull(column.DynamicEnumSettings.DataValueColumnName) && !aeviIsUndefinedOrNull(column.DynamicEnumSettings.DisplayValueColumnName));
    }
}
