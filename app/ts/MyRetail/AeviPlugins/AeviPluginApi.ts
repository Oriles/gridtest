/// <reference path='../../references.ts' />

class AeviPluginApi {
	AeviGrid: any;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
	}

	callPluginsOnRowsChanged(rows) {
		try {
			if (!aeviIsUndefinedOrNull(this.AeviGrid.plugins)) {
				for (var x = 0; x < this.AeviGrid.plugins.length; x++) {
					this.AeviGrid.plugins[x].onRowsChanged(rows);
				}
			}
		} catch (ex) {
			console.log(ex.message);
		}
	}

	callPluginsOnRowsDeleting(rows) {
		try {
			if (!aeviIsUndefinedOrNull(this.AeviGrid.plugins)) {
				for (var x = 0; x < this.AeviGrid.plugins.length; x++) {
					this.AeviGrid.plugins[x].onRowsDeleting(rows);
				}
			}
		} catch (ex) {
			console.log(ex.message);
		}
	}

	callPluginsOnDataLoaded() {
		try {
			if (!aeviIsUndefinedOrNull(this.AeviGrid.plugins)) {
				for (var x = 0; x < this.AeviGrid.plugins.length; x++)
					this.AeviGrid.plugins[x].onDataLoaded();
			}
		} catch (ex) {
			this.AeviGrid.print('AeviGridHandler.callPluginsOnDataLoaded()' + ', ' + ex.message);
		}
	}

	callPluginsOnCellLeave($cell: JQuery, enterValue, leaveValue) {
		var cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes($cell);

		try {
			if (!aeviIsUndefinedOrNull(this.AeviGrid.plugins)) {
				for (var x = 0; x < this.AeviGrid.plugins.length; x++) {
					this.AeviGrid.plugins[x].onCellLeave(cellIndexes.rowId, cellIndexes.cellId, enterValue, leaveValue);
				}
			}
		} catch (ex) {
			console.log(ex.message);
		}
	}	
}
