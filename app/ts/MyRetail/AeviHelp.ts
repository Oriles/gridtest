/// <reference path='../references.ts' />
/// <reference path='AeviHandlers/AeviHelpHandler.ts' />

class AeviHelp {
	AeviGrid: any;
	selector: string;
	element: JQuery;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.selector = this.AeviGrid.AeviConsts.helperSelector;
	}

	render() {
		var className: string = this.selector.makeClassFromSelector();
		var aeviWrapperElement: JQuery = $(this.AeviGrid.AeviConsts.aeviWrapperSelector);
		var aeviWrapperWidth: number = aeviWrapperElement.outerWidth();

		var html: string = '<div class="' + className + '" style="width: ' + aeviWrapperWidth + 'px;">' + 
						'<p class="title">' + this.AeviGrid.AeviLocalization.translate('tooltip_help') + '</p>' + 
						'<p>' + this.AeviGrid.AeviDataRepository.AeviDataEntity.data.Help + '</p>' +
						'<span class="aeviHelp__close" title="' + this.AeviGrid.AeviLocalization.translate('image_close') + '"></span>' + 
					'</div>';

		aeviWrapperElement.parent().prepend(html);

		this.element = $(this.selector);

		var handler: any = new AeviHelpHandler(this.AeviGrid, this);
	}

	show() {
        if(aeviIsUndefinedOrNull(this.element))
			this.render();

		this.element.slideDown(300);
	}

	hide() {
		this.element.slideUp(300);
	}
}
