/// <reference path='../../references.ts' />
/// <reference path='AeviBodyCell.ts' />

class AeviBodyRow {
	AeviDataRepository: any;
	AeviTableDescription: any;
	AeviGrid: any;
	columnsLength: number;
	AeviBodyCell: any;

	constructor(aeviDataRepository: any, aeviGrid: any) {
		this.AeviDataRepository = aeviDataRepository;
		this.AeviTableDescription = this.AeviDataRepository.AeviTableDescription;
		this.AeviGrid = aeviGrid;
		this.columnsLength = this.AeviTableDescription.getColumnsLength() - this.AeviTableDescription.fakeColumnsLength + this.AeviTableDescription.startFakeColumnsLength;
        this.AeviBodyCell = new AeviBodyCell(this.AeviDataRepository, this.AeviGrid);
	}

	render(rowData: any[], rowIndex: number): string {
		var rowInfo = this.getRowInfo(rowData);		
		var HTML: string = '<tr id="row-' + rowInfo.rowIndex + '" class="aeviRow ' + rowInfo.status + '">';

		for (var cellIndex = 0; cellIndex < this.columnsLength; cellIndex++) {
            var header = this.AeviTableDescription.getColumnHeader(cellIndex);

            if (header.Visible === false) {
                continue;
            }

            HTML += this.AeviBodyCell.render(rowIndex, cellIndex);
        }

		HTML += '</tr>';

		return HTML;
	}

	refresh(row: HTMLTableRowElement, rowData: any[]): void {
        if (aeviIsUndefinedOrNull(row))
			return;

		if (aeviIsUndefinedOrNull(rowData)) {
			row.className = 'aeviRow hidden';
            return;
		}

		var rowInfo = this.getRowInfo(rowData);

        if (!this.AeviGrid.AeviDataService.AeviUser.roles.Create) {
            if (rowInfo.status.indexOf('newrow') !== -1) {
                rowInfo.status = AeviBodyRow.setRowAsHiddenAndGetHiddenStatus(rowInfo.status);
            }
        }

        row.className = 'aeviRow ' + rowInfo.status;
		row.id = 'row-' + rowInfo.rowIndex;

        var cellWithVisibleFalseLength = 0;

		for (var i = 0; i < this.columnsLength; i++) {
            var header = this.AeviTableDescription.getColumnHeader(i);

            if (header.Visible === false) {
                cellWithVisibleFalseLength++;
                continue;
            }

            rowInfo.cellIndex = i;

            var cell = row.childNodes[i - cellWithVisibleFalseLength];

            var specialStatus = this.AeviBodyCell.refresh(cell, rowData[i], rowInfo, header);
            AeviBodyRow.setSpecialStatus(row, specialStatus);
		}
	}

    static setSpecialStatus(row, status): void {
        status = status.toString().replace(/ /g, '');
        if(status.length > 0)
            row.classList.add(status);
    }

    static setRowAsHiddenAndGetHiddenStatus(status: string): string {
        status = status.replace('newrow', '').replace('render', '');
        return status += ' hidden';
    }

	getRowInfo(listOfRows: any[]): any {
        var rowIndex: any[] = listOfRows[this.AeviDataRepository.AeviTableDescription.getRowNumberIndex()];

		return {
			rowIndex: rowIndex,
			invalidCells: this.AeviDataRepository.getInvalidPosition(rowIndex),
			selectedCells: this.AeviDataRepository.getSelectedPosition(rowIndex),
			status: ' ' + this.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(rowIndex) + ' ',
		};
	}
}
