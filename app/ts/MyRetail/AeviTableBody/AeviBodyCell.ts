/// <reference path='../../references.ts' />

class AeviBodyCell {
	AeviDataRepository: any;
	AeviTableDescription: any;
	AeviLocalization: any;
	AeviGrid: any;

	dataTypes: any;

	constructor(aeviDataRepository: any, aeviGrid: any) {
		this.AeviDataRepository = aeviDataRepository;
		this.AeviTableDescription = this.AeviDataRepository.AeviTableDescription;
		this.AeviGrid = aeviGrid;
		this.AeviLocalization = this.AeviGrid.AeviLocalization;
		this.dataTypes = this.AeviGrid.AeviConsts.dataTypes;
	}

	render(rowIndex: number, cellIndex: number): string {
		var htmlClass: string = (cellIndex === 0) ? 'aeviRowNumber' : 'aeviCell';
		var currentIndex: string = rowIndex + '.' + cellIndex;

		return '<td tabIndex="' + currentIndex + '" id="cell-' + currentIndex + '" class="' + htmlClass + '"><div class="aeviCellBorder"></div></td>';
	}

    /**
     * return special row status
     */
	refresh(cell, cellValue: any, rowInfo: any, header: IAeviColumnDescription): string {
		var displayType = (aeviIsUndefinedOrNull(header)) ? '2' : this.AeviTableDescription.getDisplayType(header);

		var cellValues = this.getCellValues(displayType, cellValue, header, rowInfo.cellIndex, rowInfo);

        var aeviCellBorder = cell.childNodes[0];

		cell.className = cellValues.Class;

        var message: string = '';

        if(!aeviIsUndefinedOrNull(rowInfo.invalidCells[rowInfo.cellIndex])) {
            message = rowInfo.invalidCells[rowInfo.cellIndex].message;
        }

		cell.setAttributes({
            'data-action': cellValues.Action,
			'id': 'cell-' + rowInfo.rowIndex + '.' + rowInfo.cellIndex,
			'data-message': message
		});

		aeviCellBorder.innerHTML = cellValues.DisplayValue;
		aeviCellBorder.className = cellValues.ChildClass;

        return cellValues.SpecialStatus;
	}

	getCellValues(displayType, displayValue, header, cellIndex, rowInfo) {
		var i;
		var cell = {
			DataValue: displayValue,
			DisplayValue: displayValue,
			Class: 'aeviCell',
			Style: '',
			ChildClass: 'aeviCellBorder',
			Action: null,
            SpecialStatus: ''
		};

        if (header.ColumnName === 'RowNumber') {
			cell.DataValue += 1;
			cell.DisplayValue += 1;
			cell.Class = 'aeviRowNumber';
		}

        if(!aeviIsUndefinedOrNull(rowInfo.invalidCells[cellIndex])) {
            if (!rowInfo.invalidCells[cellIndex].valid) {
                if (rowInfo.invalidCells[cellIndex].hard) {
                    cell.Class += ' validateError ';
                    cell.ChildClass += ' validateError ';
                } else {
                    cell.Class += ' softValidateError ';
                    cell.ChildClass += ' softValidateError ';
                }
            }
        }

		if (_.contains(rowInfo.selectedCells, cellIndex)) {
			cell.Class += ' selected ';
			cell.ChildClass += ' selected ';
		}

		if (!aeviIsUndefinedOrNull(header)) {
			if (header.Visible === false)
				cell.Class += ' hidden ';

			if (!this.AeviDataRepository.AeviTableDescription.isReport()) {
				if (header.ReadOnly)
					cell.Class += ' readOnly ';
			}

			if (!aeviIsUndefinedOrNull(header.EntityActionDefinition)) {
				cell.Action = header.EntityActionDefinition.ActionId;
				cell.Class += ' aeviLink ';

                if(header.EntityActionDefinition.ResultType === MyRetail.IAeviEntityActionResultType.Form) {
                    cell.Class += ' aeviFormLink text-center';
                }
			}
		}

        var referenceSettingsColumn = this.AeviDataRepository.getReferenceSettings(rowInfo.rowIndex, cellIndex);

		if (!_.isNull(referenceSettingsColumn)) {
			header = referenceSettingsColumn;
			displayType = referenceSettingsColumn.DisplayType;
		}

		var values;
		var displayTypeString = this.dataTypes[displayType];

		switch (displayTypeString) {
			case 'DateTime':
			case 'ShortDate':
			case 'ShortTime':
				if (displayValue && displayValue.length > 0) {
					var aeviDate = new AeviDate(this.AeviLocalization.getCulture(), displayType, displayValue);
					cell.DisplayValue = aeviDate.getString(); //value.toLocaleString(this.AeviLocalization.getCulture());
				}
				break;

			case 'Number':
				values = this.getNumberValues(displayValue);

				if (header.ColumnName === 'RowNumber') {
					values.DisplayValue++;
				}

				cell.DisplayValue = values.DisplayValue;
				cell.DataValue = values.DataValue;
				cell.Class += ' text-right ';
				break;

			case 'IntegerNumber':
				cell.Class += ' text-right ';
				break;

			case 'Enum':
				values = this.getEnumValues(displayValue, header);
				cell.DisplayValue = values.DisplayValue;
				cell.DataValue = values.DataValue;
				break;

			case 'Boolean':
				values = this.getBooleanValues(cell);
                cell.DisplayValue = values.DisplayValue;
				cell.DataValue = values.DataValue;
				cell.Class = values.Class;
				break;

			case 'Image':
                cell.DisplayValue = AeviImage.getDisplayValue(displayValue);
				cell.DataValue = AeviImage.getDataValue();
				break;

			case 'Currency':
				values = this.getCurrencyValues(cell, header);
				cell.DisplayValue = values.DisplayValue;
				cell.DataValue = values.DataValue;
				cell.Class += ' text-right ';
				break;

			default:
				break;
		}

		if (cell.DisplayValue === null || cell.DisplayValue === 'null' || cell.DisplayValue === '') {
			cell.DisplayValue = '';
			cell.DataValue = 'null';
		}

        if (this.AeviTableDescription.hasColumnAction(cellIndex)) {
            var columnAction = this.AeviTableDescription.getColumnActionType(cellIndex);

            switch (columnAction) {
                case MyRetail.IAeviColumnActionType.NegativeRowFormating:
                    var isValueNegative = AeviGlobal.isNegative(cell.DataValue);
                    if (isValueNegative) {
                        cell.SpecialStatus = ' negative ';
                    }
                    break;

                default:
                    break;
            }
        }

        return cell;
	}

	getCurrencyValues(cell, header): IAeviCellValue {
		var val: IAeviCellValue = {
            DataValue: null,
            DisplayValue: null
        };

		if (!_.isNull(cell.DisplayValue)) {
			val.DataValue = cell.DisplayValue;
			val.DisplayValue = val.DataValue;

			if (!aeviIsUndefinedOrNull(header.DecimalPlaceCount)) {
				val.DataValue = this.getNumberWithDecimalPlaceCount(val.DataValue, header.DecimalPlaceCount);
				val.DisplayValue = val.DataValue;
			}

			val.DisplayValue = aeviFormatNumber(val.DisplayValue).replace('.', ',');

			if (!_.isNull(header.CurrencyISOCode))
				val.DisplayValue += ' ' + header.CurrencyISOCode;
		} else {
			val.DataValue = cell.DataValue;
			val.DisplayValue = cell.DisplayValue;
		}

		return val;
	}

	getNumberWithDecimalPlaceCount(number, decimalPlaceCount) {
		var roundedNumber: number = Math.round(number * 100) / 100;

		return roundedNumber.toFixed(decimalPlaceCount);
	};

	getBooleanValues(cell) {
		var val: any = {};
		var checked = '';

		if (_.isNull(cell.DisplayValue)) {
			val.DataValue = null;
			val.DisplayValue = 'null';
			checked = 'null';
		}

		if (checked !== 'null')
			checked = (cell.DisplayValue) ? 'true' : 'false';

		val.DisplayValue = '<i class="aeviCellCheckboxRead ' + checked + '">' + checked + '</i>';
		val.DataValue = checked;
		val.Class = ' aeviCell textcenter ';

		if (this.AeviDataRepository.AeviTableDescription.readOnly) {
			val.DisplayValue = (checked === 'true') ? this.AeviGrid.AeviLocalization.translate('yes') : this.AeviGrid.AeviLocalization.translate('no');
			val.Class = val.Class.replace('textcenter', '');
		}

		return val;
	}

	getNumberValues(cell) {
		var val: any = {};
		val.DataValue = cell;

		if (_.isNumber(cell))
			cell = cell.toString();

		if (_.isNull(cell))
			val.DisplayValue = cell;
		else
			val.DisplayValue = cell.replace('.', ',');

		return val;
	}

	getEnumValues(displayValue, header, dataValue?): IAeviCellValue {
		var val: IAeviCellValue = {
            DataValue: null,
            DisplayValue: null
        };

		var enumValues = header.EnumValues;

		if (displayValue === 'true')
			displayValue = true;

		if (displayValue === 'false')
			displayValue = false;

		if (!_.isUndefined(enumValues)) {
			for (var i = 0; i < enumValues.length; i++) {

				if (!_.isNull(displayValue) && !_.isBoolean(displayValue)) {
					displayValue = displayValue.toString().replace(' -', '-').replace('- ', '-').replace(' - ', '-');
				}

				if (_.isUndefined(dataValue)) {
					if (enumValues[i].DataValue == displayValue) {
						val.DataValue = enumValues[i].DataValue;
						val.DisplayValue = enumValues[i].DisplayValue;
					}
				} else {
					if (enumValues[i].DisplayValue == displayValue) {
						val.DataValue = enumValues[i].DataValue;
						val.DisplayValue = enumValues[i].DisplayValue;
					}
				}
			}

			if (_.isEmpty(val.DisplayValue) || val.DisplayValue == 'null') {
				val.DataValue = null;
				val.DisplayValue = '';
			}

			if (_.isNull(val.DataValue) && !_.isNull(displayValue)) {
				val = this.getEnumCombinationValues(val, enumValues, displayValue);
			}
		}

		return val;
	}

	getEnumCombinationValues(val, enumValues, displayValue) {
		if (_.isNull(val.DataValue)) {
			for (var i = 0; i < enumValues.length; i++) {
				if (displayValue === enumValues[i].DataValue) {
					val.DataValue = enumValues[i].DataValue;
					val.DisplayValue = enumValues[i].DisplayValue;
					return val;
				}

				var dataValueWithoutDisplayValue = this.getEnumDisplayValueWithoutDataValue(enumValues[i].DataValue, enumValues[i].DisplayValue);

				if (!_.isNull(dataValueWithoutDisplayValue) && !_.isNull(displayValue)) {
					if (dataValueWithoutDisplayValue === displayValue) {
						val.DataValue = enumValues[i].DataValue;
						val.DisplayValue = enumValues[i].DisplayValue;
						return val;
					}
				}
			}
		}

		return {
			DataValue: null,
			DisplayValue: null
		};
	}

	getEnumDisplayValueWithoutDataValue(dataValue, displayValue) {
		if (!_.isNull(dataValue) && !_.isNull(displayValue))
			return displayValue.toString().replace(dataValue.toString() + '-', '');

		return null;
	}

	getValues(displayTypeString, header, displayValue, dataValue?): any {
		var values = {};

		switch (displayTypeString) {
			case 'Enum':
				values = this.getEnumValues(displayValue, header, displayValue);
				break;
		}

		return values;
	}

	static getOffset(element) {
		return element.getBoundingClientRect();
	}

	static isInViewport(cellEl, bodyEl) {
		var cell = AeviBodyCell.getOffset(cellEl);
		var body = AeviBodyCell.getOffset(bodyEl);

		var isInViewportVertically = (cell.top >= body.top && cell.bottom <= body.bottom) ? true : false;
		if (!isInViewportVertically)
			return false;

		var isInViewportHorizontally = (cell.left >= body.left && cell.right <= body.right) ? true : false;
		if (isInViewportHorizontally)
			return true;

		return false;
	}
}
