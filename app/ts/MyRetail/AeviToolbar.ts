/// <reference path='../references.ts' />
/// <reference path='AeviHandlers/AeviToolbarHandler.ts' />

class AeviToolbar {
	AeviGrid: any;
	AeviToolbarHandler: any;
	AeviAppSelector: string;
	localization: any;
	filters: any[];
	handler: any;
	tools: any[];
	renderedTools: any[];
	renderTools: string;

    constructor(aeviGrid: any, filters: any[]) {
    	//mastering and blastering
		this.AeviGrid = aeviGrid;
		this.AeviAppSelector = this.AeviGrid.tableId;
		this.localization = this.AeviGrid.AeviLocalization;
		this.filters = filters;
		this.initializeTools(filters);

	}

	initializeTools(filters: any[]) {
		var availableTools: string[] = [
			'border',
			'lock',
			'unlock',
			'save',
			'search',
			'copy',
			'refresh',
			'delete_row',
			'insert_row',
			'toolbar_disable_toastr',
			'sort_invalid',
			'page_size',
			'break',
			'query',
			'filter',
			'pdf',
			'xls',
			'paste',
			'discard_changes',
            'insert_by_form',
            'export',
            'importexport'
		];

		this.tools = [];
		for (var i = 0; i < availableTools.length; i++) {
			var name = availableTools[i];
			var translate = this.localization.translate('toolbar_' + name);
			var html = '';
			var className = '';

			switch (name) {
				case 'page_size':
					html = '<p class="aeviToolbar__' + name + '">' +
						'<span>' + this.localization.translate('count_of_rows') + ': </span>' +
						'<select>' +
						'<option value="null">' + this.localization.translate('dynamic') + '</option>' +
						'<option value="100">' + this.localization.translate('extended') + '</option>' +
						'</select>' +
						'</p>';
					break;

				case 'query':
					html = '<p class="aeviToolbar__searchwrapper"><input type="text" tabindex="-1" placeholder="' + this.localization.translate('query_placeholder') + '" id="' + this.AeviAppSelector + '-query' + '" class="aeviToolbar__query"></p>';
					break;

				case 'break':
					html = '<br>';
					break;

				default:
					if (name === 'pdf' || name === 'unlock' || name === 'save' || name === 'unset_block_mode') {
						className = AeviToolbar.getSecondaryButtonClass();
                    }

					if (name === 'xls' || name === 'insert_by_form' || name === 'importexport' || name === 'export') {
						className = 'pull-right';
                    }

					html = '<i id="' + this.AeviAppSelector + '-' + name + '" class="aeviToolbar__' + name + ' ' + className + '">' + translate + '</i>';
					break;
			}

			this.tools[name] = html;
		}

		if (!_.isNull(filters) && filters.length) {
			for (i = 0; i < filters.length; i++) {
				this.tools['filter_' + filters[i].id] = filters[i].html;
			}
		}
	}

	static getSecondaryButtonClass(): string {
		return 'aeviButton--secondary';
	}

	getTools(keys): string {
		var html = '';
		var arrayOfKeys = keys.split('|').aeviClean('');

		for (var i = 0; i < arrayOfKeys.length; i++) {
			var tool = this.tools[arrayOfKeys[i]];

			if (!_.isUndefined(tool) && tool)
				html += tool;
		}

		this.renderedTools = arrayOfKeys;

		return html;
	}

    render(): void {
		this.clearTools();
		this.refreshTools();

		var template = '<div id="' + this.AeviAppSelector + '-toolbar" class="aeviToolbar">' + this.getTools(this.renderTools) + '</div>';

		$(this.AeviGrid.aeviWrapper).prepend(template);

		this.listener();

		setTimeout(() => {
			this.AeviGrid.setSizes();
		}, 700);
	}

	refresh(): void {
		if (this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport())
			return;

		this.clearTools();
		this.refreshTools();
		$('#' + this.AeviAppSelector + '-toolbar').html(this.getTools(this.renderTools));
		this.AeviGrid.setSizes();
	}

	clearTools(): void {
		this.renderTools = '';
	}

	refreshTools(): void {
        this.clearTools();

        if(!_.isNull(this.filters)) {
            for(var i = 0; i < this.filters.length; i++)
                this.addTool('filter_' + this.filters[i].id + '|');
        }

        if(!_.isNull(this.filters) && this.filters.length)
            this.addTool('filter|');

        //this.addTool('page_size|');

        if (!this.AeviGrid.AeviDataRepository.AeviTableDescription.isFixed()) {
            if (!this.AeviGrid.AeviDataRepository.AeviTableDescription.isReadOnly()) {
                if (this.AeviGrid.AeviDataRepository.AeviDataEntity.data.Form === true) {
                    if (this.AeviGrid.AeviDataService.AeviUser.roles.Create) {
                        this.addTool('insert_by_form|');
                    }
                }
            }
        }

        if (_.isNull(this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.getReferenceSettingsColumnNameIndex())) {
            var showOnlyExport = false;

            if (this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.isReport()) {
                showOnlyExport = true;
            } else {
                if (!this.AeviGrid.AeviDataRepository.AeviTableDescription.isFixed()) {
                    if (!this.AeviGrid.AeviDataRepository.AeviTableDescription.isReadOnly()) {
                        showOnlyExport = false;
                    }
                } else {
                    showOnlyExport = true;
                }
            }

            if (showOnlyExport) {
                this.addTool('export|');
            } else {
                this.addTool('importexport|');
            }
        }

        this.addTool('break|');

        if (this.AeviGrid.AeviDataService.AeviUser.roles.Write) {
            this.addTool('save|');
            this.addTool('discard_changes|');
        }

        if (this.AeviGrid.AeviDataService.AeviUser.roles.Write) {
            if(!aeviIsUndefinedOrNull(this.AeviGrid.AeviGridLocker)) {
                if(!this.AeviGrid.AeviGridLocker.isLocked)
                    this.addTool('lock|');
                else
                    this.addTool('unlock|');
            }
        }

        this.addTool('search|');

        if (this.AeviGrid.blockMode) {
            this.addTool('unset_block_mode|');
        }

        if(!this.AeviGrid.AeviDataRepository.AeviTableDescription.isFixed()) {
            if(!this.AeviGrid.AeviDataRepository.AeviTableDescription.isReadOnly()) {

                if(this.AeviGrid.AeviDataRepository.AeviTableDescription.isAllowedPastingFromExcel()) {
                    if (this.AeviGrid.AeviDataService.AeviUser.roles.Create) {
                        this.addTool('paste|');
                    }
                }

                if (this.AeviGrid.AeviDataService.AeviUser.roles.Create) {
                    this.addTool('insert_row|');
                }

                if (this.AeviGrid.AeviDataService.AeviUser.roles.Delete) {
                    this.addTool('delete_row|');
                }

                if(!aeviIsUndefinedOrNull(this.AeviGrid.AeviGridLocker)) {
                    if(this.AeviGrid.AeviGridLocker.isLocked) {
                        this.removeTool('paste|');
                        this.removeTool('insert_row|');
                        this.removeTool('insert_by_form');
                        this.removeTool('delete_row|');
                    }
                }
            }
        }

        if(this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport()) {
            this.addTool('pdf|');
        }

        // removing tools
        if(this.AeviGrid.mode.clipboard === 1) {
            this.removeTool('paste|');
            this.removeTool('insert_row|');
            this.removeTool('delete_row|');
            this.removeTool('unlock|');
            this.removeTool('lock|');
            this.removeTool('filter|');
            this.removeTool('filter_DataEntityFullTextFilter|');
            this.removeTool('importexport|');
            this.removeTool('insert_by_form');
        }

        if(this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport() || this.AeviGrid.AeviDataRepository.AeviTableDescription.isReadOnly()) {
            this.removeTool('unlock|');
            this.removeTool('save|');
            this.removeTool('discard_changes|');
        }

        if (this.AeviGrid.AeviDataRepository.AeviTableDescription.isReadOnly() && !this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport()) {
            this.removeTool('importexport|');
        }
	}

	getSelectors(): any[] {
		var selectors = [];

		for (var tool in this.tools) {
			selectors.push('#' + this.AeviAppSelector + '-' + tool);
		}

		return selectors;
	}

	listener() {
		if (aeviIsUndefinedOrNull(this.AeviToolbarHandler)) {
			this.AeviToolbarHandler = new AeviToolbarHandler(this.AeviGrid);
		}

		var selectors = this.getSelectors();
        /**
         * TODO
         * hardstring
         */
		selectors.push('.aeviToolbar__textfilter');

		var stringifySelectors = selectors.join(", ");

        $(document).off('change', '.aeviToolbar__page_size select').on('change', '.aeviToolbar__page_size select', (event) => {
			this.AeviToolbarHandler.setFixRows($(event.currentTarget).val());
		});

        $(document).off('click', stringifySelectors).on('click', stringifySelectors, (event) => {
            var className = this.AeviGrid.AeviDOM.getClass(event.currentTarget).split(' ')[0];

            switch (className) {
				case 'aeviToolbar__save':
					this.AeviToolbarHandler.save();
					break;

				case 'aeviToolbar__discard_changes':
					this.AeviToolbarHandler.discard();
					break;

				case 'aeviToolbar__search':
					this.AeviToolbarHandler.search();
					break;

				case 'aeviToolbar__lock':
				case 'aeviToolbar__unlock':
					this.AeviToolbarHandler.locker();
					break;

				case 'aeviToolbar__filter':
                    this.AeviToolbarHandler.query();
					break;

				case 'aeviToolbar__refresh':
					this.AeviToolbarHandler.refresh();
					break;

				case 'aeviToolbar__insert_row':
					this.AeviToolbarHandler.insertRow();
					break;

				case 'aeviToolbar__delete_row':
					this.AeviToolbarHandler.deleteRows();
					break;

				case 'aeviToolbar__toolbar_disable_toastr':
					this.AeviToolbarHandler.popupSwitch();
					break;

				case 'aeviToolbar__pdf':
                    this.AeviGrid.AeviDownloader.pdf();
					break;

                //case 'aeviToolbar__xls':
                 //   this.AeviGrid.AeviDownloader.xls();
				//	break;

                //case 'aeviToolbar__import':
                //    this.AeviToolbarHandler.importFile();
                //    break;

                case 'aeviToolbar__export':
                case 'aeviToolbar__importexport':
                    this.AeviToolbarHandler.importOrExportFile(event);
                    break;

                case 'aeviToolbar__paste':
                    this.AeviToolbarHandler.paste();
                    break;

				case 'aeviToolbar__sort_invalid':
					this.AeviGrid.AeviDataRepository.sortInvalid();
					this.AeviGrid.AeviGridEditor.refreshData();
					break;

                case 'aeviToolbar__insert_by_form':
                    this.AeviToolbarHandler.insertByForm();
                    break;

                default:
                    event.preventDefault();
                    break;
			}
		});

		$(document).off('keydown', stringifySelectors).on('keydown', stringifySelectors, (event) => {
            var className = this.AeviGrid.AeviDOM.getClass(event.currentTarget).split(' ')[0];
            var keyString = this.AeviGrid.AeviClientSide.getKeyName(event);

            switch (className) {
                case 'aeviToolbar__textfilter':
                    if (keyString === 'enter')
                        this.AeviToolbarHandler.query();
                    break;

                default:
                    break;
            }
        });
	}

	addTool(tool: string): void {
		this.renderTools += tool;
	}

	removeTool(tool: string): void {
		this.renderTools = this.renderTools.replace(tool, '').replace('||', '|');
	}
}
