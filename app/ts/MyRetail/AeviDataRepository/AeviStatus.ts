/// <reference path='../../references.ts' />
/// <reference path='AeviDataRepository.ts' />

class AeviStatus {
	AeviGridApi: any;	
	AeviDataRepository: any;
	
	constructor(aeviDataRepository: any, aeviGridApi: any) {
		this.AeviGridApi = aeviGridApi;
		this.AeviDataRepository = aeviDataRepository;
	}

	get(guid: string): string {
		var statusIndex = this.AeviDataRepository.getStatusId();

		if (aeviIsUndefinedOrNull(statusIndex)) {
			this.AeviGridApi.log('AeviStatus.get(), statusIndex is undefined or null.');
			return null;
		}

		return this.AeviDataRepository.getRecord(guid)[0][statusIndex];
	}

	getByRowIndex(rowIndex: number): string {
		var statusColumnIndex = this.AeviDataRepository.AeviTableDescription.getStatusIndex();
        return this.AeviDataRepository.AeviTableData.getRecordCellValue(rowIndex, statusColumnIndex);
	}

	add(guid: string, status: string): void {
		var record = this.AeviDataRepository.getRecord(guid).slice();

		if (aeviIsUndefinedOrNull(record)) {
			this.AeviGridApi.log('AeviStatus.add(), record is undefined or null.');
			return null;
		}

		var statusIndex = this.AeviDataRepository.getStatusId();

		if (aeviIsUndefinedOrNull(statusIndex)) {
			this.AeviGridApi.log('AeviStatus.add(), statusIndex is undefined or null.');
			return null;
		}

		record[0][statusIndex] += ' ' + status;

        this.AeviDataRepository.updateRecord(record);
	}

	update(guid: string, status: string): void {
		var record = this.AeviDataRepository.AeviTableData.getRecord(guid).slice();

		if (aeviIsUndefinedOrNull(record)) {
			this.AeviGridApi.log('AeviStatus.update(), record is undefined or null.');
			return null;
		}

		var statusIndex = this.AeviDataRepository.AeviTableDescription.getStatusIndex();
		var rowIndex = this.AeviDataRepository.AeviTableDescription.getRowNumberIndex();

        this.AeviDataRepository.AeviTableData.updateRecordCellValue(rowIndex, statusIndex, status);
	}

	updateByRowIndex(rowIndex: number, status: string): void {
		var statusColumnIndex = this.AeviDataRepository.AeviTableDescription.getStatusIndex();
        this.AeviDataRepository.AeviTableData.updateRecordCellValue(rowIndex, statusColumnIndex, status);
	}

	addByRowIndex(rowIndex: number, status: string): void {
		var statusColumnIndex = this.AeviDataRepository.AeviTableDescription.getStatusIndex();
        var originalStatus = this.getByRowIndex(rowIndex);

        if (originalStatus.indexOf(status) !== -1)
			return;

		var newStatus = originalStatus + ' ' + status;

        this.AeviDataRepository.AeviTableData.updateRecordCellValue(rowIndex, statusColumnIndex, newStatus);
	}

    removeByRowIndex(rowIndex: number, status: string): void {
        var statusColumnIndex = this.AeviDataRepository.AeviTableDescription.getStatusIndex();
        var originalStatus = this.getByRowIndex(rowIndex);
        originalStatus = originalStatus.replace(status, '');
        this.AeviDataRepository.AeviTableData.updateRecordCellValue(rowIndex, statusColumnIndex, originalStatus);

    }

	is(rowIndex: number, status: string): boolean {
		var statusIndex = this.AeviDataRepository.AeviTableDescription.getStatusIndex();
		var record = this.AeviDataRepository.AeviTableData.data[rowIndex];

		if (aeviIsUndefinedOrNull(record)) {
            return null;
        }

		return record[statusIndex].indexOf(status) !== -1;
	}

	isNew(rowIndex: number): boolean {
		return this.is(rowIndex, 'newrow');
	}

	isInvalid(rowIndex: number): boolean {
		return this.is(rowIndex, 'invalid');
	}

	removeInvalidStatuses(rowIndex: number): void {
        this.removeByRowIndex(rowIndex, 'invalid');
        this.removeByRowIndex(rowIndex, 'softInvalid');
	}
}
