/// <reference path='../../references.ts' />

/// <reference path='IAeviDataRepository.ts' />
/// <reference path='IAeviSelectedPositions.ts' />

/// <reference path='AeviTableDescription/AeviTableDescription.ts' />
/// <reference path='AeviTableData.ts' />
 
/// <reference path='AeviStatus.ts' />
/// <reference path='../AeviGridApi.ts' />
/// <reference path='../AeviImage.ts' />

/**
 * AeviDataRepository
 * @class AeviDataRepository
 */
class AeviDataRepository implements IAeviDataRepository {
	AeviTableDescription: any;
	AeviTableData: any;
    AeviDataEntity: any;
    AeviFormDescription: any;

	AeviGrid: any;
	AeviGridApi: any;
	AeviConsts: any;
	AeviLocalization: any;

	tableDesc: any[];
	tableDescRealLength: number;
	data: any[];
	columnDifference: number;
	dataLength: number;
	dataTypes: any;	
	repositoryDataInit: boolean;
    extraColumns: IAeviExtraColumn[];

	constructor(aeviConstants: any, aeviLocalization: any, aeviGrid: any) {
		this.tableDesc = [];
		this.tableDescRealLength = 0;
		this.data = [];
		this.columnDifference = 0;
		this.dataLength = 0;
		this.AeviConsts = aeviConstants;
		this.AeviLocalization = aeviLocalization;
		this.dataTypes = this.AeviConsts.dataTypes;
		
		this.AeviGrid = aeviGrid;
		this.AeviGridApi = new AeviGridApi(this.AeviGrid);

        this.extraColumns = [];

		this.repositoryDataInit = false;
	}

    initDataEntity(aeviDataService: IAeviDataService, response: IAeviEntitySuccessResponse) {
        this.AeviDataEntity = new AeviDataEntity(aeviDataService, response);
    }

	initTableDescription(response) {
        this.AeviTableDescription = new AeviTableDescription(response, this);

        this.initExtraColumns();
        this.setExtraColumnsToDescription();
        this.AeviTableDescription.setColumnActionTypes();
	}

    initForm(response: IAeviFormSuccessResponse) {
        this.AeviFormDescription = new AeviFormDescription(response);
    }

	initTableData(data) {
		this.AeviTableData = new AeviTableData(this);
		this.deleteRepositoryData();
        this.AeviTableData.init(data);
        this.setExtraColumnsToData();

        if (_.isNull(this.AeviTableDescription.fixedSize)) {
			if (!this.AeviTableDescription.realReadOnly) {
				this.AeviTableData.addHiddenRecords();
            }
		}

        this.repositoryDataInit = true;
	}

    initExtraColumns() {
        var rowNumberValue: any = 'RowNumber';
        var rowNumberColumn: IAeviColumnDescription = {
            ColumnName: 'RowNumber',
            Caption: '',
            Visible: true,
            DisplayType: 2,
            WidthType: 0,
            Width: this.AeviConsts.firstColumnWidth
        };
        this.extraColumns.push(<IAeviExtraColumn>{column: rowNumberColumn, value: rowNumberValue});

        if(this.AeviDataEntity.data.Form === true) {
            var formColumnValue: any = 'FormAction';
            var formColumn: IAeviColumnDescription = {
                ColumnName: 'FormAction',
                Caption: '',
                Visible: true,
                DisplayType: 0,
                WidthType: 0,
                Width: 40,
                EntityActionDefinition: {ActionId: '0', ResultType: MyRetail.IAeviEntityActionResultType.Form}
            };
            this.extraColumns.push(<IAeviExtraColumn>{column: formColumn, value: formColumnValue});
        }

        var selectedCellsArrayValue: any = [];
        var selectedCellsArrayColumn: IAeviColumnDescription = {
            ColumnName: 'SelectedCellsArray',
            Caption: 'SelectedCellsArray',
            Visible: false,
            DisplayType: 98,
            WidthType: 0,
            Width: 0
        };
        this.extraColumns.push(<IAeviExtraColumn>{column: selectedCellsArrayColumn, value: selectedCellsArrayValue});

        var invalidCellsArrayValue: any[] = [];
        for (var i = 0; i < this.AeviTableDescription.getColumnsLength(); i++) {
            invalidCellsArrayValue.push({
                hard: false,
                valid: true,
                message: null
            });
        }
        var invalidCellsArrayColumn: IAeviColumnDescription = {
            ColumnName: 'InvalidCellsArray',
            Caption: 'InvalidCellsArray',
            Visible: false,
            DisplayType: 99,
            WidthType: 0,
            Width: 0
        };
        this.extraColumns.push(<IAeviExtraColumn>{column: invalidCellsArrayColumn, value: invalidCellsArrayValue});

        var statusValue: string = 'render';
        var statusColumn: IAeviColumnDescription = {
            ColumnName: 'Status',
            Caption: 'Status',
            Visible: false,
            DisplayType: 100,
            WidthType: 0,
            Width: 0
        };
        this.extraColumns.push(<IAeviExtraColumn>{column: statusColumn, value: statusValue});

        var defaultOrderValue: any = null;
        var defaultOrderColumn: IAeviColumnDescription = {
            ColumnName: 'DefaultOrder',
            Caption: 'DefaultOrder',
            Visible: false,
            DisplayType: 1,
            WidthType: 0,
            Width: 0
        };
        this.extraColumns.push(<IAeviExtraColumn>{column: defaultOrderColumn, value: defaultOrderValue});
    }

	setExtraColumnsToDescription() {
        for(var i = 0; i < this.extraColumns.length; i++) {
            this.AeviTableDescription.setExtraColumn(this.extraColumns[i].column);
            this.columnDifference++;
        }

        this.AeviTableDescription.setColumnsPositions();
    }

    setExtraColumnsToData() {
        for(var i = 0; i < this.extraColumns.length; i++) {
            this.AeviTableData.setExtraColumn(this.extraColumns[i].value);
        }

        this.AeviTableData.setDefaultOrder();
    }

	deleteRepositoryData() {
		this.AeviTableData.deleteAllData();
	}

	/**
	 * REFACTOR 
	 */
	getVisibleRecordsLength() {
		if (aeviIsUndefinedOrNull(this.AeviTableData))
			return null;
		return this.AeviTableData.getVisibleRecordsLength();
	}

	/**
	 * REFACTOR 
	 */
	getVisibleAndNewRecordsLength(): number {
		if (aeviIsUndefinedOrNull(this.AeviTableData))
			return null;
		return this.AeviTableData.getVisibleAndNewRecordsLength();
	}

	deleteRepositoryTableData() {
		this.AeviTableData.deleteAllData();
	}

	/**
	 * TODO!
	 * refactor this into AeviTableDescription
	 */
	getGuidId(): number {return this.AeviTableDescription.getGuidId();}
	getStatusId(): number {return this.AeviTableDescription.getStatusId();}
	getSelectedCellsId(): number {return this.AeviTableDescription.getSelectedCellsId();}
	getInvalidCellsId(): number { return this.AeviTableDescription.getInvalidCellsIndex(); }


	getData() {
		return this.AeviTableData.getData();
	}	

	getDataType(dataTypeNumber: number): string {
		return this.dataTypes[dataTypeNumber];
	}

	getRecords(from: number, to: number): any[] {
		this.AeviGridApi.log('AeviDataRepository.getRecords() is deprecated, use AeviTableData');
		return this.AeviTableData.getRecords(from, to);
	}

	getSelectedCellsPositions(): IAeviSelectedPositions {
		var selectedCellsIndex: number = this.AeviTableDescription.getSelectedCellsIndex();
		var data: any[] = this.AeviTableData.cloneData();
		var length: number = data.length;
		var rows: number[] = [];
		var cells: number[] = [];

		for (var i = 0; i < length; i++) {
			var selectedCells: any[] = data[i][selectedCellsIndex];

			if (_.isNull(selectedCells))
				return null;

			if (selectedCells.length > 0) {
				rows.push(i);
				cells = selectedCells;
			}
		}

		return {
            rows: rows,
            cells: cells
        };
	}

	getSelectedCellPosition(): any {
		var selectedPositions: IAeviSelectedPositions = this.getSelectedCellsPositions();

		var rowIndex = selectedPositions.rows[0];
		var cellIndex = selectedPositions.cells[0];

		if (aeviIsUndefinedOrNull(rowIndex) || aeviIsUndefinedOrNull(cellIndex))
			return null;

		return <IAeviCellIndex>{
			rowIndex: rowIndex,
			cellIndex: cellIndex
		};
	}

	getInvalidPosition(rowIndex: number): any[] {
        return this.AeviTableData.getRecordCellValue(rowIndex, this.AeviTableDescription.getInvalidCellsIndex());
	}

	getSelectedPosition(rowIndex: number): any[] {
        return this.AeviTableData.getRecordCellValue(rowIndex, this.AeviTableDescription.getSelectedCellsIndex());
	}

    getReferenceSettings(rowIndex: number, cellIndex: number): any {
        var row = this.AeviTableData.getRecordByRowIndex(rowIndex);
		var referenceSettingsColumnNameIndex = this.AeviTableDescription.getReferenceSettingsColumnNameIndex();

        if (aeviIsUndefinedOrNull(referenceSettingsColumnNameIndex))
			return null;

        var referenceSettingsColumnString = row[referenceSettingsColumnNameIndex];

		if (_.isNull(referenceSettingsColumnString))
			return null;

		var referenceSettingsColumn = JSON.parse(referenceSettingsColumnString);

		var header = this.AeviTableDescription.getColumnHeader(cellIndex);

		if (aeviIsUndefinedOrNull(header))
			return null;

		var columnName: string = this.AeviTableDescription.getColumnName(header);

		if (referenceSettingsColumn.ColumnName === columnName)
			return referenceSettingsColumn;

		return null;
	}

	isMoreCellsSelected(): boolean {
		var selectedPositions = this.getSelectedCellsPositions();

		return !!(selectedPositions.rows.length > 1 || selectedPositions.cells.length > 1);
	}

	getApiDataByRowIndex(rowIndex: number): any[] {
		return this.AeviTableData.getApiDataByRowIndex(rowIndex);
	}

	isMaxRowCountExceeded(addRow): boolean {
		if (aeviIsUndefinedOrNull(addRow))
			addRow = 0;

		var recordsLen = this.getVisibleRecordsLength() + addRow;

		if (aeviIsUndefinedOrNull(this.AeviTableDescription.maxRowCount))
			return false;

		return (this.AeviTableDescription.maxRowCount < recordsLen);
	}

	enableOrDisableImageColumns(action: string): void {
        var columnsLength: number = this.AeviTableDescription.getColumnsLength();

        var state: boolean = (action === 'disable');

        for(var i = 0; i < columnsLength; i++) {
            var column = this.AeviTableDescription.getColumnHeader(i);
            var displayType = this.AeviTableDescription.getColumnStringDisplayTypeByIndex(i);

            if(!_.isNull(column)) {
                if(displayType === 'Image') {
                    column.ReadOnly = state;
                }
            }
        }
    }

    clearSelectedRows() {
        for(var i = 0; i < this.AeviTableData.getRecordsLength(); i++) {
            if(this.AeviTableData.AeviStatus.is(i, 'selected'))
                this.AeviTableData.AeviStatus.removeByRowIndex(i, 'selected');
        }
    }

    clearCells(selectedCellsPositions: IAeviSelectedPositions) {
        var firstRow = selectedCellsPositions.rows[0];
        var lastRow = selectedCellsPositions.rows[selectedCellsPositions.rows.length - 1];
        var firstCell = selectedCellsPositions.cells[0];
        var lastCell = selectedCellsPositions.cells[selectedCellsPositions.cells.length - 1];

        for(var rowIndex = firstRow; rowIndex < lastRow + 1; rowIndex++) {
            for(var cellIndex = firstCell; cellIndex < lastCell + 1; cellIndex++) {
                this.clearCell(<IAeviCellIndex>{rowIndex: rowIndex, cellIndex: cellIndex});
            }
        }
    }

    clearCell(indexes: IAeviCellIndex) {
        var returnEmptyValue = true;
        var value = this.AeviTableData.getDefaultValue(indexes.cellIndex, null, returnEmptyValue);
        this.AeviTableData.updateRecordCellValue(indexes.rowIndex, indexes.cellIndex, value);
    }
}
