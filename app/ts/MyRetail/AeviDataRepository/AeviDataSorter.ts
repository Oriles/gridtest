/// <reference path='../../references.ts' />

class AeviDataSorter {
    AeviTableDescription: any;
    AeviTableData: any;
	
	constructor(aeviTableDescription: any, aeviTableData: any) {
        this.AeviTableDescription = aeviTableDescription;
        this.AeviTableData = aeviTableData;
	}

    prepareSort() {
        this.AeviTableData.removeSelectedPositions();
        this.AeviTableData.removeHiddenRecords();
    }

    getSortOrder(direction: string): number {
        switch (direction) {
            case 'down':
                return -1;

            case 'up':
                return 1;

            default:
                return 0;
        }
    }

    getFixedColumnIndex(columnIndex: number): number {
        return (_.isNull(columnIndex)) ? this.AeviTableDescription.getDefaultOrderIndex() : columnIndex;
    }

    setSortInfo(sortOrder: number, columnIndex: number) {
        if (sortOrder === 0) {
            this.AeviTableDescription.setSortInfos(null);
        } else {
            this.AeviTableDescription.setSortInfos([{
                ColumnName : this.AeviTableDescription.getColumnNameByIndex(columnIndex),
                SortDirection : (sortOrder === -1) ? 'Ascending' : 'Descending'
            }]);
        }
    }

    sort(direction: string, columnIndex: number) {
        if (this.AeviTableData.getVisibleRecordsLength() < 1) {
            return;
        }

        this.prepareSort();

        var sortOrder = this.getSortOrder(direction);

        columnIndex = this.getFixedColumnIndex(columnIndex);

        var sortedData = _.sortBy(this.AeviTableData.data, (rowData) => {
            var header = this.AeviTableDescription.getColumnHeader(columnIndex);
            var displayType = this.AeviTableDescription.getColumnStringDisplayTypeByIndex(columnIndex);

            var value = null;
            var compareValue = rowData[columnIndex];

            if (aeviIsUndefinedOrNull(compareValue) || compareValue === 'null') {
                compareValue = '';
            }

            switch (displayType) {
                case 'IntegerNumber':
                case 'Number':
                case 'Currency':
                    if (compareValue === '') {
                        compareValue = Number.NEGATIVE_INFINITY;
                        value = compareValue;
                    } else {
                        value = parseFloat(compareValue);
                    }
                    break;

                case 'Enum':
                    value = this.AeviTableData.getEnumDisplayValue(header, compareValue);
                    break;

                case 'Image':
                    value = compareValue.HasValue;
                    break;

                default:
                    value = compareValue;
                    break;
            }

            compareValue = value;

            if (_.isString(compareValue)) {
                value = compareValue.toLowerCase();
            }

            return value;
        });

        this.AeviTableData.data = (direction === 'up') ? sortedData.reverse() : sortedData;

        this.setSortInfo(sortOrder, columnIndex);

        this.AeviTableData.addHiddenRecords();
        this.AeviTableData.updateRowNumber();
    }
}
