/// <reference path='../../references.ts' />
/// <reference path='../AeviDataService/IAeviEntitySuccessResponse.ts' />

class AeviDataEntity {
    AeviDataService: IAeviDataService;
    caption: string;
    help: string;
    data: IAeviEntitySuccessResponse;
	
	constructor(aeviDataService: IAeviDataService, response: IAeviEntitySuccessResponse) {
        this.AeviDataService = aeviDataService;
        this.data = <IAeviEntitySuccessResponse>{};

        for(var key in response) {
            if (response.hasOwnProperty(key)) {
                this.data[key] = response[key];
            }
        }

        this.AeviDataService.AeviApiService.caption = this.data.Caption;

        this.data.Help = AeviGlobal.stringToHTML(this.data.Help);
	}
}
