
interface IAeviItemDescription {
    ColumnName: string;
    Caption: string;
    MaxHeight: number;
    Visible: boolean;
    Width: number;
    Style: string;
}

interface IAeviExtendedItemDescription extends IAeviItemDescription {
    Value: any;
    RowIndex: number;
    ColumnIndex: number;
    IsReadOnly: boolean;
    IsRequired: boolean;
}
