
interface IAeviFormSuccessResponse {
    ID: string;
    Rows: IAeviRowDescription[];
}
