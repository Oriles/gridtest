/// <reference path='../../../references.ts' />

class AeviFormDescription implements IAeviFormDescription {
    data: IAeviFormSuccessResponse;

    constructor(description: IAeviFormSuccessResponse) {
        this.data = description;
    }

    getRows(): IAeviRowDescription[] {
        return this.data.Rows;
    }

    static getFakeRows() {
        var row0 = [
            {
                ColumnName: 'ArticleImage',
                Caption: 'Obrázek',
                MaxHeight: 200,
                Visible: true,
                Width: 12,
                Style: 'text-center image'
            }
        ];

        var row1 = [
            {
                ColumnName: 'VART',
                Caption: 'Číslo zboží',
                MaxHeight: null,
                Visible: true,
                Width: 4,
                Style: ''
            },
            {
                ColumnName: '',
                Caption: '',
                MaxHeight: null,
                Visible: true,
                Width: 2,
                Style: ''
            },
            {
                ColumnName: 'Caption',
                Caption: 'Název zboží',
                MaxHeight: null,
                Visible: true,
                Width: 6,
                Style: ''
            }
        ];

        var row2 = [
            {
                ColumnName: 'ProductGroupNumber',
                Caption: 'Skupina zboží',
                MaxHeight: null,
                Visible: true,
                Width: 4,
                Style: ''
            },
            {
                ColumnName: '',
                Caption: '',
                MaxHeight: null,
                Visible: true,
                Width: 2,
                Style: ''
            },
            {
                ColumnName: 'ActionTill',
                Caption: 'Akční cena platná do',
                MaxHeight: null,
                Visible: true,
                Width: 4,
                Style: ''
            }
        ];

        var row3 = [
            {
                ColumnName: 'VATID',
                Caption: 'DPH%',
                MaxHeight: null,
                Visible: true,
                Width: 3,
                Style: ''
            },
            {
                ColumnName: '',
                Caption: '',
                MaxHeight: null,
                Visible: true,
                Width: 3,
                Style: ''
            },
            {
                ColumnName: 'RetailPrice1',
                Caption: 'Prodejní cena',
                MaxHeight: null,
                Visible: true,
                Width: 4,
                Style: ''
            }
        ];

        var rows: any = [];
        rows.push(row0);
        rows.push(row1);
        rows.push(row2);
        rows.push(row3);
        return rows;
    }
}
