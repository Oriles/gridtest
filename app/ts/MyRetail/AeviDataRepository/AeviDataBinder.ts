/// <reference path='../../references.ts' />

class AeviDataBinder {
	AeviTableData: any;
	indexes: IAeviCellIndex;

	constructor(aeviTableData: any) {
		this.AeviTableData = aeviTableData;
	}

	subscribe(indexes: IAeviCellIndex) {
        this.indexes = indexes;
	}

	publish(editorValue) {
        var dataValue = null;
        var header = this.AeviTableData.AeviTableDescription.getColumnHeader(this.indexes.cellIndex);
        var displayType = this.AeviTableData.AeviTableDescription.getDisplayType(header);
        var displayStringType = this.AeviTableData.AeviTableDescription.getColumnStringDisplayType(displayType);

        switch(displayStringType) {
            case 'Number':
            case 'Currency':
            case 'IntegerNumber':
                dataValue = editorValue.replace(',', '.');
                dataValue = parseFloat(dataValue);
                if(_.isNaN(dataValue)) {
                    dataValue = '';
                }
                break;

            default:
                dataValue = editorValue;
                break;
        }

        this.AeviTableData.updateRecordCellValue(this.indexes.rowIndex, this.indexes.cellIndex, dataValue);
	}
}
