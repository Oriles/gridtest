{}/// <reference path='../../references.ts' />
/// <reference path='../AeviGuidGenerator.ts' />

class AeviTableData {
	AeviDataRepository: IAeviDataRepository;
	AeviTableDescription: any;
	AeviStatus: any;
	AeviGuidGenerator: any;
    AeviDataSorter: any;

	data: any[];
	visibleRecordsLength: number;

	cachedVisibleRecordsLength: number;

	constructor(aeviDataRepository: any) {
		this.AeviDataRepository = aeviDataRepository;
		this.AeviTableDescription = this.AeviDataRepository.AeviTableDescription;
		this.AeviStatus = new AeviStatus(this.AeviDataRepository, this.AeviDataRepository.AeviGridApi);
	}

	init(data: any[]): void {
		this.data = data;
        this.cachedVisibleRecordsLength = this.data.length;
	}

    setExtraColumn(value: any): void {
        for(var i = 0; i < this.getRecordsLength(); i++) {
            if (value === 'RowNumber') {
                this.data[i].unshift(i);
            } else if (value === 'FormAction') {
                this.data[i].unshift(i);
            } else {
                this.data[i].push(value);
            }
        }
	}

    deleteAllData(): void {
		this.data = [];
	}

	getData(): any[] {
		return this.data;
	}

	cloneData(): any[] {
		return this.data.slice();
	}

    setDefaultOrder() {
        var dataLength = this.getRecordsLength();
        var orderColumnIndex = this.AeviTableDescription.getColumnIndexByName('DefaultOrder');

        for (var i = 0; i < dataLength; i++) {
            this.updateRecordCellValue(i, orderColumnIndex, i);
        }
    }

	addHiddenRecords() {
		if (this.AeviTableDescription.isReport() || this.AeviDataRepository.AeviGridApi.getMode().clipboard === 1)
			return;

		var i;
		var poisedRows = 201;
		var realDataLen = this.getVisibleRecordsLength() + 1;

		for (i = this.getVisibleRecordsLength() + 1; i < realDataLen + poisedRows; i++) {
			var status = 'hidden';

			if (i === realDataLen)
				status = 'newrow';

			this.insertRecord({
				Index: i - 1,
				Status: status,
				isAddedByGrid: true
			});
		}

		this.updateVisibleRecordsLength();
	}

	insertRecord(properties) {
		if (!_.isNull(this.AeviTableDescription.fixedSize))
			return false;

		if (aeviIsUndefinedOrNull(this.AeviGuidGenerator)) {
			this.AeviGuidGenerator = new AeviGuidGenerator();
		}

		var row = [];

		for (var i = 0; i < this.AeviTableDescription.getColumnsLength(); i++) {
			if (i === this.AeviTableDescription.getGuidIndex()) {
				row.push(this.AeviGuidGenerator.generate());
			}else {
				var returnEmptyValue = false;
				row.push(this.getDefaultValue(i, properties, returnEmptyValue));
			}
		}

		/**
		 * this increasing visible records length (it is in development => for performance)
		 */
		this.cachedVisibleRecordsLength++;

		var index = properties.Index + 1;
		this.data.splice(index, 0, row);
		this.updateRowNumber();
		this.updateVisibleRecordsLength();
	}

	getDefaultValue(index, properties, returnEmptyValue) {
		var header = this.AeviTableDescription.getColumnHeader(index);		
		return this.getDefaultCellValue(header, properties, returnEmptyValue);
	}

	getDefaultCellValue(header, properties, returnEmptyValue) {
		if (returnEmptyValue === false && this.AeviTableDescription.hasColumnDefaultValue(header)) {
			return this.AeviTableDescription.getColumnDefaultValue(header);
		}

		var displayType = this.AeviTableDescription.getDisplayType(header);
        var displayStringType = this.AeviTableDescription.getColumnStringDisplayType(displayType);

		switch (displayStringType) {
			case 'Text':
			case 'Hyperlink':
			case 'RegularExpression':
				return '';

			case 'Number':
			case 'IntegerNumber':
			case 'Currency':
			case 'DateTime':
			case 'ShortDate':
			case 'ShortTime':
			case 'Boolean':
			case 'Enum':
				return null;

            case 'Image':
                if(!_.isNull(properties) && properties.ImageBlocked) {
                    return AeviImage.getDefaultBlockedValue();
                }
                return AeviImage.getDefaultValue();

			case 'SelectedCellsArray':
				return [];

			case 'InvalidCellsArray':
				var value: any[] = [];

				for (var i = 0; i < this.AeviTableDescription.getColumnsLength(); i++) {
					value.push({
						hard: false,
						valid: true,
						message: null
					});
				}
				return value;

			case 'Status':
				return (!_.isUndefined(properties.Status)) ? properties.Status : null;
		}

		return null;
	}

	getVisibleRecordsLength() {
		return this.updateVisibleRecordsLength();
	}

	updateVisibleRecordsLength() {
		this.visibleRecordsLength = 0;

		for (var i = 0; i < this.data.length; i++) {
			if (this.AeviStatus.is(i, 'render'))
				this.visibleRecordsLength += 1;
		}

		return this.visibleRecordsLength;
	}

	getRecordByRowIndex(rowIndex: number): any[] {
		return this.data[rowIndex];
	}

    getRecordCellValue(rowIndex: number, cellIndex: number): any {
        if (aeviIsUndefinedOrNull(this.data[rowIndex])) {
            return null;
        }

        return this.data[rowIndex][cellIndex];
	}

	updateRecordCellValue(rowIndex: number, cellIndex: number, val: any): void {
        this.data[rowIndex][cellIndex] = val;
	}

	getRecords(from: number, to: number): any[] {
		var data = [];

		if (this.data.length > 0) {
			for (var i = from; i < to; i++) {
				if (!aeviIsUndefinedOrNull(this.data[i])) {
					data.push(this.data[i].slice());
				}
			}
		}

		return data;
	}

	getCloneRecordByRowId(rowIndex: number): any[] {
		if (aeviIsUndefinedOrNull(rowIndex)) {
			this.AeviDataRepository.AeviGridApi.log('rowIndex is undefined or null');
			return null;
		}

		if (aeviIsUndefinedOrNull(this.data[rowIndex])) {
			this.AeviDataRepository.AeviGridApi.log('AeviDataRepository.getCloneRecordByRowId(). Clone record is undefined or null');
			return null;
		}

		return this.data[rowIndex].slice();
	}

	updateRowNumber(): void {
		var length = this.getRecordsLength();

		for (var i = 0; i < length; i++)
			this.data[i].splice(0, 1, i);
	}

	getRecordsLength(): number {
		return this.data.length;
	}

	getVisibleAndNewRecordsLength(): number {
		var length = 0;

		for (var i = 0; i < this.data.length; i++) {
			if (this.AeviStatus.is(i, 'render') || this.AeviStatus.is(i, 'newrow'))
				length++;
		}

		return length;
	}

	setSelectedPositions(from: IAeviCellIndex, to: IAeviCellIndex): void {
		this.removeSelectedPositions();

        var firstRow: number = from.rowIndex;
		var firstCell: number = from.cellIndex;
		var lastRow: number = to.rowIndex;
		var lastCell: number = to.cellIndex;

		var helper: number = null;

		if (lastRow < firstRow) {
			helper = firstRow;
			firstRow = lastRow;
			lastRow = helper;
		}

		if (lastCell < firstCell) {
			helper = firstCell;
			firstCell = lastCell;
			lastCell = helper;
		}

        var listOfSelectedCells: number[] = [];

		for (var x = firstCell; x <= lastCell; x++) {
			listOfSelectedCells.push(x);
        }

		var selectedCellsIndex = this.AeviTableDescription.getSelectedCellsIndex();

        for (var i = firstRow; i < lastRow + 1; i++) {
			if (!_.isUndefined(this.data[i]))
				this.data[i][selectedCellsIndex] = listOfSelectedCells;
		}
	}

	removeSelectedPositions(): void {
		var length = this.getRecordsLength();
		var selectedCellsIndex = this.AeviTableDescription.getSelectedCellsIndex();

		for (var i = 0; i < length; i++) {
            this.updateRecordCellValue(i, selectedCellsIndex, []);
            this.AeviStatus.removeByRowIndex(i, 'selected');
		}
	}

	setRowValidityInfo(rowIndex: number, validityInfos: IAeviValidationInfo[]): void {
		for (var i = 0; i < validityInfos.length; i++)
			this.setCellValidityInfo(validityInfos[i], rowIndex, i);

		this.checkValidityStatus(rowIndex);
	}

	setCellValidityInfo(validityInfo: IAeviValidationInfo, rowIndex: number, cellIndex: number): void {
		var invalidRecordsIndex = this.AeviDataRepository.AeviTableDescription.getInvalidCellsIndex();
		/**
		 * slice() because javascript reference doesnt work
		 */
		var cellValidityInfo = this.getRecordCellValue(rowIndex, invalidRecordsIndex).slice();
		cellValidityInfo[cellIndex] = validityInfo;
		this.updateRecordCellValue(rowIndex, invalidRecordsIndex, cellValidityInfo);
	}

	checkValidityStatus(rowIndex: number): void {
		this.AeviStatus.removeInvalidStatuses(rowIndex);
		var invalidRecordsIndex = this.AeviDataRepository.AeviTableDescription.getInvalidCellsIndex();
		var rowValidityInfo: IAeviValidationInfo[] = this.getRecordCellValue(rowIndex, invalidRecordsIndex).slice();

        var isInvalid: boolean = false;
		var isSoftInvalid: boolean = false;

		for (var i = 0; i < rowValidityInfo.length; i++) {
			var cellValidityInfo = rowValidityInfo[i];

            if(cellValidityInfo.valid === false) {
				if (cellValidityInfo.hard === true)
					isInvalid = true;
				else
					isSoftInvalid = true;
			}
		}

        if(isInvalid)
			this.AeviStatus.addByRowIndex(rowIndex, 'invalid');

		if (isSoftInvalid)
			this.AeviStatus.addByRowIndex(rowIndex, 'softInvalid');
	}

	getApiDataByRowIndex(rowIndex: number): any[] {
		var row = this.getRecordByRowIndex(rowIndex);

        var apiData = [];
		var apiRowData = [];

		for (var i = this.AeviTableDescription.startFakeColumnsLength; i < row.length; i++) {
			var apiCellData = row[i];

			var column = this.AeviTableDescription.getColumnHeader(i);

			if (!_.isNull(column)) {
				var columnName = this.AeviTableDescription.getColumnName(column);

				switch (columnName) {
					case 'SelectedCellsArray':
					case 'InvalidCellsArray':
					case 'Status':
					case 'DefaultOrder':
						continue;

					default:
						break;
				}

				var columnDisplayType = this.AeviTableDescription.getDisplayType(column);
				var columnStringDisplayType = this.AeviTableDescription.getColumnStringDisplayType(columnDisplayType);

				switch (columnStringDisplayType) {
					case 'Text':
					case 'Number':
					case 'IntegerNumber':
					case 'Currency':
					case 'DateTime':
					case 'ShortDate':
					case 'ShortTime':
					case 'Boolean':
                    case 'Enum':
						if (apiCellData === '')
							apiCellData = null;

						break;

					case 'Image':
						if (_.isEmpty(apiCellData) || apiCellData === 'null')
							apiCellData = null;
						break;
				}

				apiCellData = (apiCellData === 'null') ? null : apiCellData;

				apiRowData.push(apiCellData);
			}
		}

		apiData.push(apiRowData);

		return apiData;
	}

	getInvalidRecordsIndexes(): number[] {
		var invalidRowsIndexes: number[] = [];
		var dataLength = this.data.length;

		for (var i = 0; i < dataLength; i++) {
			if (this.AeviStatus.is(i, 'invalid'))
				invalidRowsIndexes.push(i);
		}

		return invalidRowsIndexes;
	}

	sortInvalid(): void {
		var clipboardMode = this.AeviDataRepository.AeviGrid.mode.clipboard;

        if (clipboardMode === 0) {
			this.removeHiddenRecords();
        }

		var invalidDataGuids: string[] = [];
		var invalidData: any[] = [];

        var dataLength = this.getRecordsLength();

        for (var i = 0; i < dataLength; i++) {
            if (this.AeviStatus.is(i, 'invalid')) {
                invalidData.push(this.data[i]);
                invalidDataGuids.push(this.getGuidByRowIndex(i));
            }
		}

        this.deleteData(invalidDataGuids);

        for (var i = 0; i < invalidData.length; i++) {
			this.data.splice(i, 0, invalidData[i]);
		}

		if (clipboardMode === 0) {
			this.addHiddenRecords();
        }

		this.updateRowNumber();
	}

	removeHiddenRecords(): void {
		if (this.AeviTableDescription.isReport())
			return;

		var guids: string[] = [];
		var dataLength = this.getRecordsLength();
        var guidIndex: number = this.AeviTableDescription.getGuidIndex();

		for (var rowIndex = 0; rowIndex < dataLength; rowIndex++) {
            var status = this.AeviStatus.getByRowIndex(rowIndex);

            if (status === 'newrow' || (this.AeviStatus.is(rowIndex, 'hidden') && !this.AeviStatus.is(rowIndex, 'sortedRow'))) {
			    if (!this.AeviStatus.is(rowIndex, 'render')) {
                    guids.push(this.getRecordCellValue(rowIndex, guidIndex));
                }
            }
		}

        this.deleteData(guids);
	}

    deleteData(guids: string[]): void {
        var guidsLength: number = guids.length;

        for (var i = 0; i < guidsLength; i++) {
            this.updateRowNumber();
            var guid = guids[i];
            var rowIndex: number = this.getRecordIndexByGuid(guid);
            this.data.splice(rowIndex, 1);
            this.cachedVisibleRecordsLength--;
        }

        this.updateRowNumber();
        this.updateVisibleRecordsLength();
    }

    getRecordIndexByGuid(guid: string): number {
        var row = this.getRecord(guid);
        return row[this.AeviTableDescription.getRowNumberIndex()];
    }

    getRecord(guid: string): any {
        var guidIndex: number = this.AeviTableDescription.getGuidIndex();
        var dataLength = this.getRecordsLength();

        for (var rowIndex = 0; rowIndex < dataLength; rowIndex++) {
            if (this.getRecordCellValue(rowIndex, guidIndex) === guid)
                return this.data[rowIndex];
        }

        return null;
    }

	getGuidByRowIndex(rowIndex: number): string {
		var guid = this.getRecordCellValue(rowIndex, this.AeviTableDescription.getGuidIndex());

		if (aeviIsUndefinedOrNull(guid)) {
			this.AeviDataRepository.AeviGridApi.log('AeviTableData.getGuidByRowIndex(), guid is undefined or null.');
			return null;
		}

		return guid;
	}

	getErrorMessage(identification: IAeviRecordIdentification): string {
		var errorMessageIndex: number = this.AeviTableDescription.getErrorMessageIndex();
		var rowIndex: number = null;

		if (!_.isUndefined(identification.guid))
			rowIndex = this.getRecordIndexByGuid(identification.guid);
		
		if (!_.isUndefined(identification.rowIndex))
			rowIndex = identification.rowIndex;

		return this.getRecordCellValue(rowIndex, errorMessageIndex);
	}

	setErrorMessage(rowIndex: number, message: string): void {
        this.updateRecordCellValue(rowIndex, this.AeviTableDescription.getErrorMessageIndex(), message);
	}

    /**
     * unused static function
     */
	static getRecordKey(identification: IAeviRecordIdentification) {
		var recordKey = null;

		if (!_.isUndefined(identification.rowIndex))
			recordKey = identification.rowIndex;

		if (!_.isUndefined(identification.guid))
			recordKey = identification.guid;

		return recordKey;
	}

	getSelectedRows(): number[] {
		var selectedRows: number[] = [];

		for (var i = 0; i < this.data.length; i++) {
            if (this.AeviStatus.is(i, 'selected')) {
                if (!this.AeviStatus.is(i, 'newrow') && !this.AeviStatus.is(i + 1, 'hidden')) {
                    selectedRows.push(i);
                }
            }
		}

		return selectedRows;
	}

	setSelectedRows(rows: number[], selectAllCells: boolean): void {
		var selectedCellsIndex: number = this.AeviTableDescription.getSelectedCellsIndex();
		var statusIndex: number = this.AeviTableDescription.getStatusIndex();
		var recordsLength: number = this.getRecordsLength();

		for (var i = 0; i < recordsLength; i++) {
			if (selectAllCells === true) {
				this.data[i][selectedCellsIndex] = _.range(0, this.AeviTableDescription.getColumnsLength());
			} else {
				if (_.contains(rows, i)) {
					this.data[i][selectedCellsIndex] = _.range(0, this.AeviTableDescription.getColumnsLength());
					this.data[i][statusIndex] += (this.data[i][statusIndex].indexOf('selected') === -1) ? ' selected ' : '';
				} else {
					this.data[i][selectedCellsIndex] = [];
					this.data[i][statusIndex] = this.data[i][statusIndex].replace('selected', '');
				}
			}
		}
	}

	setSelectedColumns(columnsIndexes: number[]): void {
		var recordsLength: number = this.getRecordsLength();
		var selectedCellsIndex: number = this.AeviTableDescription.getSelectedCellsIndex();

		for(var i = 0; i < recordsLength; i++) {
			this.data[i][selectedCellsIndex] = columnsIndexes;
		}
	}

    getApiDataWithColumnNames(data) {
        var apiData = {};

        /**
         * i = 1 because we have rowNumberColumn
         */
        for(var i = 1; i < this.AeviTableDescription.getColumnsLength() - this.AeviTableDescription.fakeColumnsLength + 1; i++) {
            var header = this.AeviTableDescription.getColumnHeader(i);
            var displayType: number = this.AeviTableDescription.getColumnDisplayType(header);
            var stringDisplayType: string = this.AeviTableDescription.getColumnStringDisplayType(displayType);
            var cellData = data[i];

            switch(stringDisplayType) {
                case 'Text':
                case 'Number':
                case 'IntegerNumber':
                case 'Currency':
                case 'DateTime':
                case 'ShortDate':
                case 'ShortTime':
                case 'Boolean':
                    if(cellData === '')
                        cellData = null;

                    break;

                case 'Enum':
                case 'Image':
                    if(_.isEmpty(cellData) || cellData === 'null')
                        cellData = null;
                    break;
            }

            if(stringDisplayType === 'Image')
                continue;

            cellData = (cellData === 'null') ? null : cellData;

            var object = {};
            var myVar = header.ColumnName;
            object[myVar] = cellData;

            apiData[header.ColumnName] = cellData;
        }

        return apiData;
    }

    pasteOneCell(data: any, indexes: IAeviCellIndex): void {
        this.updateRecordCellValue(indexes.rowIndex, indexes.cellIndex, data.Rows[0][0]);

        var status = this.AeviStatus.getByRowIndex(indexes.rowIndex);

        this.AeviStatus.addByRowIndex(indexes.rowIndex, 'update');

        if(status.indexOf('insert') !== -1 || status.indexOf('newrow') !== -1) {
            this.AeviStatus.removeByRowIndex(indexes.rowIndex, 'update');
            this.AeviStatus.addByRowIndex(indexes.rowIndex, 'insert');
        }

        this.validateCell(indexes.rowIndex, indexes.cellIndex);
    }

    pasteRecords(data: any, indexes: IAeviCellIndex): void {
        var startRowIndex = indexes.rowIndex;
        var startCellIndex = indexes.cellIndex;
        var rowsLength = data.Rows.length + startRowIndex;

        for(var i = startRowIndex; i < rowsLength; i++) {
            var status = 'render';

            if(aeviIsUndefinedOrNull(this.data[i])) {
                this.insertRecord({ Index : i - 1, Status : 'newrow', ImageBlocked: true });
                status += ' newrow';

                if(this.AeviTableDescription.isFixed())
                    continue;
            }

            var cellLength = data.Rows[i - startRowIndex].length + startCellIndex;

            var headerIndex = startCellIndex;

            for(var j = startCellIndex; j < cellLength; j++) {
                var cellValue: any = data.Rows[i - startRowIndex][j - startCellIndex];

                var header = this.AeviTableDescription.getColumnHeader(headerIndex);
                var displayType = this.AeviTableDescription.getColumnDisplayType(header);
                var displayTypeString = this.AeviTableDescription.getColumnStringDisplayType(displayType);

                switch(displayTypeString) {
                    case 'Enum':
                        var cell = new AeviBodyCell(this, this.AeviDataRepository.AeviGrid);
                        var values = cell.getValues(displayTypeString, header, cellValue);
                        cellValue = values.DataValue;
                        break;

                    case 'Currency':
                    case 'Number':
                        cellValue = cellValue.toString().replace(',', '.');
                        break;

                    case 'DateTime':
                    case 'ShortDate':
                    case 'ShortTime':
                        if (cellValue && cellValue.length > 0) {
                            var aeviDate = new AeviDate(this.AeviDataRepository.AeviGrid.AeviLocalization.getCulture(), header.DisplayType, cellValue);
                            cellValue = aeviDate.getIsoString();
                        }
                        break;

                    default:
                        break;
                }

                this.updateRecordCellValue(i, headerIndex, cellValue);

                /**
                 * skip next column if is hidden (!visible)
                 */
                if(this.isColumnHidden(headerIndex + 1))
                    headerIndex++;
                headerIndex++;
            }

            this.AeviStatus.updateByRowIndex(i, status);
        }

        this.validate();
    }

    isColumnHidden(columnIndex: number): boolean {
        var header = this.AeviTableDescription.getColumnHeader(columnIndex);
        return !this.AeviTableDescription.isVisible(header);
    }

    validate(): void {
        var rowsLength: number = this.getRecordsLength();

        for(var rowIndex = 0; rowIndex < rowsLength; rowIndex++)
            this.validateRow(rowIndex);
    }

    validateRow(rowIndex: number): void {
        var cellsValidity: IAeviValidationInfo[] = [];
        var cellsLength: number = this.AeviTableDescription.getColumnsLength() - this.AeviTableDescription.fakeColumnsLength;

        for (var cellIndex = 0; cellIndex < cellsLength; cellIndex++) {
            cellsValidity.push(this.validateCell(rowIndex, cellIndex));
        }

        this.setRowValidityInfo(rowIndex, cellsValidity);
    }

    validateCell(rowIndex: number, cellIndex: number): IAeviValidationInfo {
        var validateValue = this.getRecordCellValue(rowIndex, cellIndex);
        var validatorFactory = new AeviValidatorFactory(this.AeviDataRepository.AeviGrid, this.AeviDataRepository.AeviLocalization, this.AeviTableDescription);
        validatorFactory.createValidator(validateValue, cellIndex);
        var validator = validatorFactory.getValidator();

        return validator.validate();
    }

    getValidityInfo(rowIndex: number, cellIndex: number): IAeviValidationInfo {
        return this.data[rowIndex][this.AeviTableDescription.getInvalidCellsIndex()][cellIndex];
    }

    exportDataToArray(): any[] {
        var columnNamesDoNotCopy: string[] = [];

        for(var i = 0; i < this.AeviTableDescription.getColumnsLength(); i++) {
            var column = this.AeviTableDescription.getColumnHeader(i);

            if(this.AeviTableDescription.getColumnName(column) === 'FormAction' || this.AeviTableDescription.getColumnName(column) === 'RowNumber') {
                columnNamesDoNotCopy.push(this.AeviTableDescription.getColumnName(column));
            }

            if(!this.AeviTableDescription.isVisible(column)) {
                columnNamesDoNotCopy.push(this.AeviTableDescription.getColumnName(column));
            }
        }

        var data = this.cloneData();
        var cellsLength = this.AeviTableDescription.getColumnsLength();

        var rows: any[] = [];

        for(var i = 0; i < data.length; i++) {
            var row: any[] = [];

            if(this.AeviStatus.is(i, 'hidden') || this.AeviStatus.is(i, 'newrow')) {
                continue;
            }

            for(var j = 0; j < cellsLength; j++) {
                var exportValue: any = null;

                var header = this.AeviTableDescription.getColumnHeader(j);
                var headerName = this.AeviTableDescription.getColumnName(header);
                var displayType = this.AeviTableDescription.getDisplayType(header);
                var displayTypeString = this.AeviTableDescription.getColumnStringDisplayType(displayType);

                /**
                 * do not export this column
                 */
                if (!_.contains(columnNamesDoNotCopy, headerName)) {
                    var value: IAeviCellValue = {
                        DataValue    : data[i][j],
                        DisplayValue : data[i][j],
                        OutputValue  : null
                    };

                    switch(displayTypeString) {
                        case 'Image':
                            continue;

                        case 'Enum':
                            var cell = new AeviBodyCell(this.AeviDataRepository, this.AeviDataRepository.AeviGrid);
                            var values: IAeviCellValue = cell.getEnumValues(value.DisplayValue, header);
                            value.OutputValue = values.DisplayValue;
                            break;

                        case 'Currency':
                            var cell = new AeviBodyCell(this.AeviDataRepository, this.AeviDataRepository.AeviGrid);
                            var values: IAeviCellValue = cell.getCurrencyValues(value, header);

                            if(!_.isNull(values.DisplayValue))
                                value.OutputValue = values.DisplayValue.toString().replace('.', ',');
                            break;

                        case 'DateTime':
                        case 'ShortDate':
                        case 'ShortTime':
                            if (value.DisplayValue && value.DisplayValue.length > 0) {
                                var aeviDate = new AeviDate(this.AeviDataRepository.AeviLocalization.getCulture(), displayType, value.DisplayValue);
                                value.DisplayValue = aeviDate.getString();
                                value.OutputValue = value.DisplayValue;
                            }
                            break;

                        case 'Number':
                            if(!_.isNull(value.DisplayValue))
                                value.OutputValue = value.DisplayValue.toString().replace('.', ',');
                            break;

                        default:
                            value.OutputValue = value.DisplayValue;
                            break;
                    }

                    if(_.isNull(value.OutputValue) || value.OutputValue === 'null') {
                        row.push('');
                        continue;
                    }

                    if(displayTypeString !== 'Currency') {
                        exportValue = this.wrapWithEquatingAndQuotes(value.OutputValue);
                    } else {
                        exportValue = value.OutputValue;
                    }

                    row.push(exportValue);
                }
            }

            rows.push(row);
        }


        return rows;
    }

    getDataAsCSV(): string {
        var csv: string = '';
        var data: any[] = this.exportDataToArray();
        var rowsLength = data.length;

        for (var i = 0; i < rowsLength; i++) {
            var isEndOfRow = false;
            var cellsLength = data[i].length;
            var dataString = data[i].join(";");

            for (var j = 0; j < cellsLength; j++) {
                if (j === cellsLength - 1) {
                    isEndOfRow = true;
                }
            }

            if (isEndOfRow) {
                csv += dataString + '\n';
            } else {
                csv += dataString;
            }
        }

        return csv;
    }

    wrapWithEquatingAndQuotes(str): string {
        return '="' + str + '"';
    }

    getRecordsToClipboard(): string {
        var columnNamesDoNotCopy: string[] = [];

        for(var i = 0; i < this.AeviTableDescription.getColumnsLength(); i++) {
            var column = this.AeviTableDescription.getColumnHeader(i);

            if(this.AeviTableDescription.getColumnName(column) === 'FormAction' || this.AeviTableDescription.getColumnName(column) === 'RowNumber') {
                columnNamesDoNotCopy.push(this.AeviTableDescription.getColumnName(column));
            }

            if(!this.AeviTableDescription.isVisible(column)) {
                columnNamesDoNotCopy.push(this.AeviTableDescription.getColumnName(column));
            }
        }

        var selectedCellsIndex: number = this.AeviTableDescription.getSelectedCellsIndex();
        var rows: any[] = [];
        var cells: any[] = [];
        var data = this.cloneData();

        for (var i = 0; i < data.length; i++) {
            if (data[i][selectedCellsIndex].length && !this.AeviStatus.is(i, 'hidden')) {
                rows.push(i);
                cells = data[i][selectedCellsIndex];
            }
        }

        data = [];
        for (i = 0; i < rows.length; i++) {
            data.push(this.getRecordByRowIndex(rows[i]));
        }

        var startCellIndex: number = Math.min.apply(Math, cells);

        var finishCellIndex: number = Math.max.apply(Math, cells);

        var stringData: string = '';

        var isUserCopyOneCell: boolean = false;

        if(cells.length === 1 && rows.length === 1) {
            isUserCopyOneCell = true;
        }

        for(var i = 0; i < data.length; i++) {
            var isFirstCellComposed = false;

            for(var j = startCellIndex; j <= finishCellIndex; j++) {
                var header = this.AeviTableDescription.getColumnHeader(j);
                var headerName = this.AeviTableDescription.getColumnName(header);
                var displayType = this.AeviTableDescription.getDisplayType(header);
                var displayTypeString = this.AeviTableDescription.getColumnStringDisplayType(displayType);

                var firstChar = (j === startCellIndex) ? '' : '\t';
                var lastChar = (j === finishCellIndex) ? '\r\n' : '';

                /**
                 * do not copy this column
                 */
                if (_.contains(columnNamesDoNotCopy, headerName)) {

                } else {
                    if (isFirstCellComposed) {
                        stringData += firstChar;
                    }

                    isFirstCellComposed = true;

                    var value: IAeviCellValue = {
                        DataValue    : data[i][j],
                        DisplayValue : data[i][j],
                        OutputValue  : null
                    };

                    switch(displayTypeString) {
                        case 'Image':
                            break;

                        case 'Enum':
                            var cell = new AeviBodyCell(this.AeviDataRepository, this.AeviDataRepository.AeviGrid);
                            var values: IAeviCellValue = cell.getEnumValues(value.DisplayValue, header);
                            value.OutputValue = values.DisplayValue;
                            break;

                        case 'Currency':
                            var cell = new AeviBodyCell(this.AeviDataRepository, this.AeviDataRepository.AeviGrid);
                            var values: IAeviCellValue = cell.getCurrencyValues(value, header);

                            if(!_.isNull(values.DisplayValue))
                                value.OutputValue = values.DisplayValue.toString().replace('.', ',');
                            break;

                        case 'DateTime':
                        case 'ShortDate':
                        case 'ShortTime':
                            if (value.DisplayValue && value.DisplayValue.length > 0) {
                                var aeviDate = new AeviDate(this.AeviDataRepository.AeviLocalization.getCulture(), displayType, value.DisplayValue);
                                value.DisplayValue = aeviDate.getString();
                                value.OutputValue = value.DisplayValue;
                            }
                            break;

                        case 'Number':
                            if(!_.isNull(value.DisplayValue))
                                value.OutputValue = value.DisplayValue.toString().replace('.', ',');
                            break;

                        default:
                            value.OutputValue = value.DisplayValue;
                            break;
                    }

                    if(_.isNull(value.OutputValue) || value.OutputValue === 'null')
                        stringData += '';
                    else {
                        if(!isUserCopyOneCell) {
                            if(displayTypeString !== 'Currency')
                                stringData += '="' + value.OutputValue + '"';
                            else {
                                stringData += value.OutputValue;
                            }
                        }else {
                            stringData += value.OutputValue;
                        }
                    }
                }
                stringData += lastChar;
            }
        }

        return stringData;
    }

    sort(direction: string, columnIndex: number) {
        if(aeviIsUndefinedOrNull(this.AeviDataSorter)) {
            this.AeviDataSorter = new AeviDataSorter(this.AeviTableDescription, this);
        }

        this.AeviDataSorter.sort(direction, columnIndex);
    }

    /**
     * todo!
     * refactor anywhere...
     */
    getEnumDisplayValue(header: any, dataValue: string) {
        var enumValues = header.EnumValues;

        for(var i = 0; i < enumValues.length; i++) {
            if(enumValues[i].DataValue == dataValue)
                return enumValues[i].DisplayValue;
        }

        return null;
    }

    getDisplayValue(dataValue, columnIndex: number) {
        var column = this.AeviTableDescription.getColumnHeader(columnIndex);

        if(aeviIsUndefinedOrNull(column) || column.Visible === false)
            return '';

        var displayValue = dataValue;
        var columnType = this.AeviTableDescription.getColumnStringDisplayTypeByIndex(columnIndex);

        switch(columnType) {
            case 'DateTime':
            case 'ShortDate':
            case 'ShortTime':
                var aeviDate = new AeviDate(this.AeviDataRepository.AeviGrid.AeviLocalization.getCulture(), columnType, dataValue);
                displayValue = aeviDate.getString();
                break;

            case 'Enum':
                var enumValues = column.EnumValues;
                for(var x = 0; x < enumValues.length; x++) {
                    if(enumValues[x].DataValue === dataValue)
                        displayValue = enumValues[x].DisplayValue;
                }
                break;

            case 'Image':
                displayValue = null;
                break;

            default:
                break;
        }

        if(aeviIsUndefinedOrNull(displayValue))
            displayValue = '';

        return displayValue.toString();
    }

    getFirstHiddenRecordIndex() {
        for(var rowIndex = 0; rowIndex < this.getRecordsLength(); rowIndex++) {
            if(this.AeviStatus.is(rowIndex, 'hidden'))
                return rowIndex;
        }

        return null;
    }

    getFilledVisibleRecordsLength() {
        var statusIndex = this.AeviTableDescription.getStatusIndex();
        var rowsWithStatusNewRowLength = 0;

        for(var rowIndex = 0; rowIndex < this.data.length; rowIndex++) {
            if(this.AeviStatus.is(rowIndex, 'newrow'))
                rowsWithStatusNewRowLength += 1;
        }

        return this.getVisibleRecordsLength() - rowsWithStatusNewRowLength;
    }
}
