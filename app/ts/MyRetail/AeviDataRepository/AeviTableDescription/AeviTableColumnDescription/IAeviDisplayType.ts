module MyRetail {
    export enum IAeviDisplayType {
        Text = 0,
        Number = 1,
        IntegerNumber = 2,
        Currency = 3,
        DateTime = 4,
        ShortDate = 5,
        ShortTime = 6,
        Boolean = 7,
        Enum = 8,
        Hyperlink = 9,
        RegularExpression = 10,
        Image = 11,
        Reference = 12
    };
}
