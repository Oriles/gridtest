module MyRetail {
    export enum IAeviWidthType {
        Pixels = 0,
        Percents = 1
    };
}
