interface IAeviEntityActionDefinition {
    ActionId: string;
    ResultType: MyRetail.IAeviEntityActionResultType;
    Data?: any[];
}
