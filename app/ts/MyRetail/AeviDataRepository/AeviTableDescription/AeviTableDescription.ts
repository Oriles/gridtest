/// <reference path='../../../references.ts' />

class AeviTableDescription {
	AeviDataRepository: IAeviDataRepository;
	description: IAeviTableDescription;
	fixedSize: number;
	allowFilter: boolean;
	readOnly: boolean;
	realReadOnly: boolean;
	allowPasteFromExcel: boolean;
	saveModeExcel: number;
	columnsLength: number;
	sortInfos: boolean;
	columns: IAeviColumnDescription[];
	firstVisibleColumn: number;
	lastVisibleColumn: number;
	maxRowCount: number;
	tableDescRealLength: number;
	entityType: boolean;
	columnPositions: any;
	visibleColumnsLength: number;
    fakeColumnsLength: number;
    startFakeColumnsLength: number;
    columnsAction: IAeviColumnAction[];
    allowLoadDataImmediately: boolean;

	constructor(description: IAeviTableDescription, aeviDataRepository: IAeviDataRepository) {
		this.AeviDataRepository = aeviDataRepository;
		this.description = description;
		this.fixedSize = description.FixedSize;
		this.maxRowCount = description.MaxRowCount;
		this.readOnly = description.ReadOnly;
		this.realReadOnly = description.ReadOnly;
		this.allowPasteFromExcel = description.AllowPasteFromExcel;
		this.saveModeExcel = description.SaveModeExcel;
		this.columnsLength = description.Columns.length;
		this.sortInfos = null;
		this.columns = description.Columns;
        this.entityType = this.AeviDataRepository.AeviGrid.entityType;
        this.fakeColumnsLength = 0;
        this.startFakeColumnsLength = 0;
        this.columnsAction = [];
        this.allowLoadDataImmediately = description.AllowLoadDataImmediately;

        this.allowFilter = (!_.isNull(description.Filters) && description.Filters.length) ? true : false;
    }

    getDescription() {
		return this.description;
	}

    getColumnHeader(index: number) {
		var columnHeader = this.columns[index];
		
		if (_.isUndefined(columnHeader))
			return null;
		return columnHeader;
	}

    getColumnHeaderByName(name: string): IAeviColumnDescription {
        for(var i = 0; i < this.getColumnsLength(); i++) {
            if(this.getColumnNameByIndex(i) === name) {
                return this.getColumnHeader(i);
            }
        }
        return null;
    }

	getColumnName(columnHeader: IAeviColumnDescription): string {
		if(aeviIsUndefinedOrNull(columnHeader)) {
			this.AeviDataRepository.AeviGridApi.log('AeviTableDescription.getColumnName(), parameter "columnHeader" is undefined or null.');
			return null;
		}
		return columnHeader.ColumnName;
	}

    getColumnNameByIndex(columnIndex: number): string {
        var header = this.getColumnHeader(columnIndex);
        return this.getColumnName(header);
    }


	getColumnDefaultValue(columnHeader: IAeviColumnDescription) {
		return columnHeader.DefaultValue;
	}

	getColumnVisibility(columnHeader: IAeviColumnDescription) {
		return columnHeader.Visible;
	}

    getColumnAction(columnHeader: IAeviColumnDescription): IAeviEntityActionDefinition {
        return columnHeader.EntityActionDefinition;
    }

    static getColumnActionType(columnHeader: IAeviColumnDescription) {
        return columnHeader.ActionType;
    }

    getColumnActionByColumnIndex(columnIndex: number): IAeviEntityActionDefinition {
        var column = this.getColumnHeader(columnIndex);
        return this.getColumnAction(column);
    }

	getColumnIndexByName(name: string) {
        for (var i = 0; i < this.columns.length; i++) {
			var columnName = this.getColumnName(this.columns[i]);

            if (columnName === name)
				return i;
		}
		
		return null;
	}

	/**
	 * TODO!
	 * deprecated, use getColumnDisplayType();
	 */
	getDisplayType(columnHeader: IAeviColumnDescription) {
		return this.getColumnDisplayType(columnHeader);
	}

	getColumnDisplayType(columnHeader: IAeviColumnDescription): number {
		return columnHeader.DisplayType;
	}

	getColumnStringDisplayType(columnDisplayType: number): string {
		return this.AeviDataRepository.AeviGrid.AeviConsts.dataTypes[columnDisplayType];
	}

    getColumnStringDisplayTypeByIndex(columnIndex: number): string {
        var header: IAeviColumnDescription = this.getColumnHeader(columnIndex);

        if (_.isNull(header)) {
            return null;
        }

        var displayType: number = this.getColumnDisplayType(header);
        return this.getColumnStringDisplayType(displayType);
    }

	getColumns(): IAeviColumnDescription[]  {
		return this.columns;
	}

	getColumnsLength(): number {
		return this.getColumns().length;
	}

	isVisible(columnHeader: IAeviColumnDescription) {
		return !!columnHeader.Visible;
	}

    isRequired(column: IAeviColumnDescription) {
        return column.Required;
    }

    isRequiredByColumnIndex(columnIndex: number) {
        var column: IAeviColumnDescription = this.getColumnHeader(columnIndex);
        return this.isRequired(column);
    }

	isReadOnly() {
		return !!this.realReadOnly;
	}

    isColumnReadOnly(column: IAeviColumnDescription): boolean {
        return column.ReadOnly;
    }

    isFixed() {
        return (!_.isNull(this.fixedSize));
    }

	isReport() {
        return !!this.entityType;
	}

    isAllowedPastingFromExcel() {
        return this.allowPasteFromExcel;
    }

	hasColumnDefaultValue(columnHeader: IAeviColumnDescription): boolean {
		if(aeviIsUndefinedOrNull(columnHeader)) {
			this.AeviDataRepository.AeviGridApi.log('AeviTableDescription.hasColumnDefaultValue(), "columnHeader" is undefined or null.');
			return false;
		}

		return !aeviIsUndefinedOrNull(columnHeader.DefaultValue);
	}

	getFirstVisibleColumn() {
		if(!aeviIsUndefinedOrNull(this.firstVisibleColumn))
			return this.firstVisibleColumn;
		
		for(var i = 0; i < this.columns.length; i++) {
			var columnHeader = this.getColumnHeader(i);

			if(_.isNull(columnHeader))
				continue;

			if(this.getColumnVisibility(columnHeader) === true) {
				this.firstVisibleColumn = i;
				return this.firstVisibleColumn;
			}
		}

		return null;
	}

	getLastVisibleColumn() {
		if(!aeviIsUndefinedOrNull(this.lastVisibleColumn))
			return this.lastVisibleColumn;

		for(var i = this.columns.length; i > -1; i--) {
			var columnHeader = this.getColumnHeader(i);

			if(_.isNull(columnHeader))
				continue;

			if(this.getColumnVisibility(columnHeader) === true) {
				this.lastVisibleColumn = i;
				return this.lastVisibleColumn;
			}
		}

		return null;
	}

    /**
     * todo!
     * fake call fuction
     */
	setExtraColumn(column: any): void {
        return this.fakeSetExtraColumn(column);

        if (column.ColumnName === 'RowNumber') {
            this.columns.splice(0, 0, column);
        } else {
			this.columns.push(column);
        }

        this.fakeColumnsLength++;
	}

    fakeSetExtraColumn(column: any): void {
        if (column.ColumnName === 'RowNumber') {
            this.columns.splice(0, 0, column);
            this.startFakeColumnsLength++;
        } else if (column.ColumnName === 'FormAction') {
            this.columns.splice(1, 0, column);
            this.startFakeColumnsLength++;
        } else {
            this.columns.push(column);
        }

        this.fakeColumnsLength++;
    }

	setColumnsPositions() {
		var keyColumnName = this.description.KeyColumnName;
		var errorCodeColumnName = this.description.ErrorCodeColumnName;
		var errorMessageColumnName = this.description.ErrorMessageColumnName;
		var referenceSettingsColumnName = this.description.ReferenceSettingsColumnName;

		var length = this.getColumnsLength();
		this.columnPositions = {};

		for (var i = 0; i < length; i++) {
			var columnHeader = this.getColumnHeader(i);

			if(_.isNull(columnHeader))
				continue;

			var columnName = this.getColumnName(columnHeader);

			switch (columnName) {
				case 'RowNumber':
					this.columnPositions.rowNumber = i;
					break;

				case keyColumnName:
					this.columnPositions.keyColumnName = i;
					break;

				case errorCodeColumnName:
					this.columnPositions.errorCodeColumnName = i;
					break;

				case errorMessageColumnName:
					this.columnPositions.errorMessageColumnName = i;
					break;

				case referenceSettingsColumnName:
					this.columnPositions.referenceSettingsColumnName = i;
					break;

				case 'SelectedCellsArray':
					this.columnPositions.selectedCellsArray = i;
					break;

				case 'InvalidCellsArray':
					this.columnPositions.invalidCellsArray = i;
					break;

				case 'Status':
					this.columnPositions.statusColumn = i;
					break;

				case 'DefaultOrder':
					this.columnPositions.defaultOrder = i;
					break;
			}
		}

		//console.log(this.columnPositions);
	}

	getGuidId() {
		if (_.isUndefined(this.columnPositions) || _.isUndefined(this.columnPositions.keyColumnName))
			return null;
		return this.columnPositions.keyColumnName + 1;
	}

	getStatusId() {
		if (aeviIsUndefinedOrNull(this.columnPositions) || aeviIsUndefinedOrNull(this.columnPositions.statusColumn)) {
			return null;
		}

		return this.columnPositions.statusColumn + 1;
	}

	getSelectedCellsId() {
		if (_.isUndefined(this.columnPositions) || _.isUndefined(this.columnPositions.selectedCellsArray))
			return null;

		return this.columnPositions.selectedCellsArray + 1;
	}

	getInvalidCellsId() {
		if (_.isUndefined(this.columnPositions) || _.isUndefined(this.columnPositions.invalidCellsArray))
			return null;

		return this.columnPositions.invalidCellsArray + 1;
	}

	/**
	 * TODO!
	 * refactor Id versus Index
	 */
	getGuidIndex() {
		return this.columnPositions.keyColumnName;
	}

	getInvalidCellsIndex() {
		return this.columnPositions.invalidCellsArray;
	}

	getSelectedCellsIndex() {
		return this.columnPositions.selectedCellsArray;
	}

	getStatusIndex() {
		return this.columnPositions.statusColumn;
	}

    getDefaultOrderIndex(): number {
        return this.columnPositions.defaultOrder;
    }

	getRowIndex(): number {
		console.log("caller is " + arguments.callee.caller);
		this.AeviDataRepository.AeviGridApi.log('AeviTableDescription.getRowIndex() is deprecated, use getRowNumberIndex()!!!');
		return this.getRowNumberIndex();
	}

	getRowNumberIndex(): number {
		return 0;
	}

    getFirstDataColumnIndex(): number {
        return this.startFakeColumnsLength;
    }

	getErrorMessageIndex() {
		return this.columnPositions.errorMessageColumnName;
	}

	getReferenceSettingsColumnNameIndex() {
		if (_.isUndefined(this.columnPositions) || _.isUndefined(this.columnPositions.referenceSettingsColumnName))
			return null;

		return this.columnPositions.referenceSettingsColumnName;
	}

	getVisibleCellsLength() {
		if (aeviIsUndefinedOrNull(this.visibleColumnsLength)) {
			this.visibleColumnsLength = 0;
            var columnsLength = this.getColumnsLength();

			for (var i = 0; i < columnsLength; i++) {
				var header = this.getColumnHeader(i);

                if(this.isVisible(header))
                    this.visibleColumnsLength++;
			}
		}

		return this.visibleColumnsLength;
	}

    getCountOfPossibleColumnsToCopy(): number {
        var count: number = 0;
        var columnsLenght: number = this.getColumnsLength();

        if (!columnsLenght) {
            return 0;
        }

        for(var i = 0; i < columnsLenght; i++) {
            var column: IAeviColumnDescription = this.getColumnHeader(i);

            if (aeviIsUndefinedOrNull(column)) {
                continue;
            }

            var canIncrease: boolean = false;
            var displayType = this.getDisplayType(column);

            if (column.Visible === true) {
                canIncrease = true;

                if (displayType === MyRetail.IAeviDisplayType.Image) {
                    canIncrease = false;
                }
            }

            if (!canIncrease) {
                continue;
            }

            var columnName = this.getColumnName(column);

            switch (columnName) {
                case 'RowNumber':
                case 'FormAction':
                case 'SelectedCellsArray':
                case 'InvalidCellsArray':
                case 'Status':
                case 'DefaultOrder':
                case this.description.KeyColumnName:
                case this.description.ErrorCodeColumnName:
                case this.description.ErrorMessageColumnName:
                case this.description.ReferenceSettingsColumnName:
                    canIncrease = false;
                    break;

                default:
                    canIncrease = true;
                    break;
            }

            if (canIncrease) {
                count++;
            }
        }

        return count;
    }

    setSortInfos(sortInfos) {
        this.sortInfos = sortInfos;
    }

    getSortInfos(): any {
        return this.sortInfos;
    }

    getJSONSortInfos(): string {
        return JSON.stringify(this.getSortInfos());
    }

    setColumnActionTypes(): void {
        for (var i = 0; i < this.getColumnsLength(); i++) {
            var column = this.getColumnHeader(i);
            var actionType = AeviTableDescription.getColumnActionType(column);

            if (aeviIsUndefinedOrNull(actionType)) {
                continue;
            }

            var columnActionType: IAeviColumnAction = {
                columnIndex: i,
                columnActionType: actionType
            };

            this.columnsAction.push(columnActionType);
        }
    }

    hasColumnAction(columnIndex: number): boolean {
        for(var i = 0; i < this.columnsAction.length; i++) {
            if(this.columnsAction[i].columnIndex === columnIndex)
                return true;
        }
        return false;
    }

    getColumnActionType(columnIndex: number): MyRetail.IAeviColumnActionType {
        for(var i = 0; i < this.columnsAction.length; i++) {
            if(this.columnsAction[i].columnIndex === columnIndex) {
                return this.columnsAction[i].columnActionType;
            }
        }
        return null;
    }

    fakeSetColumnActionTypes(): void {
        for (var i = 0; i < this.getColumnsLength(); i++) {
            if (i === 6) {
                var actionType: MyRetail.IAeviColumnActionType = MyRetail.IAeviColumnActionType.NegativeRowFormating;

                if (aeviIsUndefinedOrNull(actionType)) {
                    continue;
                }

                var columnActionType: IAeviColumnAction = {
                    columnIndex: i,
                    columnActionType: actionType
                };

                this.columnsAction.push(columnActionType);
            }
        }
    }


}
