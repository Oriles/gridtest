
interface IAeviTableDescription {
    Columns: IAeviColumnDescription[];
    KeyColumnName: string;
    ErrorCodeColumnName: string;
    ErrorMessageColumnName: string;
    ReferenceSettingsColumnName: string;
    ReadOnly: boolean;
    FixedSize: number;
    MaxRowCount: number;
    AllowLoadDataImmediately: boolean;
    Filters: IAeviColumnDescription[];
    FixedColumnCount: number;
    AllowPasteFromExcel: boolean;
    SaveModeExcel: MyRetail.IAeviSaveModeExcel;
    MaxRowCountExcel: number;
}
