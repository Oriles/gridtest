interface IAeviDataRepository {
	AeviTableData: any;
	AeviTableDescription: any;
    AeviFormDescription: IAeviFormDescription;
    AeviDataEntity: any;
    AeviGridApi: any;
    AeviGrid: any;
    AeviLocalization: any;
    repositoryDataInit: boolean;

    initDataEntity(aeviDataService: IAeviDataService, response: IAeviEntitySuccessResponse): void;
    initTableDescription(response): void;
    initForm(response: IAeviFormSuccessResponse): void;
    initTableData(data): void;
    deleteRepositoryTableData(): void;
    enableOrDisableImageColumns(action: string): void;
    getSelectedCellsPositions(): IAeviSelectedPositions;
    clearCells(selectedCellsPositions: IAeviSelectedPositions): void;
    clearSelectedRows(): void;
    getReferenceSettings(rowIndex: number, cellIndex: number): any;
}