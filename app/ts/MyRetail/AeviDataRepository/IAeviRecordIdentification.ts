
interface IAeviRecordIdentification {
	guid?: string;
	rowIndex?: number;
}
