interface IAeviRoles {
    Read: boolean; // ! grid is not initialized
    Write: boolean; // ! lock mode
    Delete: boolean; // ! cannot delete
    Create: boolean; // ! cannot insert new record
}
