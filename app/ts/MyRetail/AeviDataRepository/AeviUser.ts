/// <reference path='../../references.ts' />
/// <reference path='IAeviRoles.ts' />

class AeviUser {
    roles: any;

	constructor(roles) {
        this.roles = {
            Read: roles.CanRead,
            Write: roles.CanWrite,
            NativeWrite: roles.CanWrite,
            Create: roles.CanCreate,
            Delete: roles.CanDelete
        };
	}
}

