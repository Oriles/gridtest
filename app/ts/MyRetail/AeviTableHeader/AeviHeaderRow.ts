/// <reference path='../../references.ts' />
/// <reference path='AeviHeaderColumn.ts' />

class AeviHeaderRow {
	AeviConsts: any;
	headerData: any;
	columnsLength: number;

	constructor(aeviConsts: any) {
		this.AeviConsts = aeviConsts;
		this.headerData = [];
		this.columnsLength = 0;
	}

	render(columns: any[]) {
		var cols: string = '';
		var column: any = new AeviHeaderColumn(this.AeviConsts);

        for (var i = 0; i < columns.length; i++) {
            if (columns[i].Visible === false)
				continue;

        	cols += column.render(columns[i], i);
			this.columnsLength++;
		}

		var template =
			'<thead><tr>' +
			cols +
			'</tr></thead>';

		$(this.AeviConsts.tableSelector).append(template);

		this.setWidth(columns);
		this.cloneHeaderRow();
		this.setHeaderRowWidth();
		this.setTableWidth();

		if (this.isColumnsWidthThinnerThanTableWidth())
			this.recalculateColumnsWidth();
	}

	refresh(columns): void {
		this.removeClonnedHeaderRow();
		this.render(columns);
	}

	setWidth(columns) {
		var column = new AeviHeaderColumn(this.AeviConsts);

		for (var i = 0; i < columns.length; i++)
			column.setColWidth(columns[i], i);
	};

	updateCols(): void {
		var columns = document.getElementById(this.AeviConsts.aeviTableId.makeClassFromSelector()).getElementsByTagName('th');
		var cols = [];

		for (var i = 0; i < columns.length; i++) {
			var columnWidth = parseInt(columns[i].getAttribute('data-width'));
            cols.push({
				index: i,
				width: columnWidth
			});
		}

		this.headerData.cols = cols;
	}

	removeClonnedHeaderRow(): void {
		$(this.AeviConsts.aeviWrapperSelector + ' table thead').remove();
		var heads = document.querySelectorAll('.clonnedHead');

		for (var i = 0; i < heads.length; i++) {
			if (i === 0)
				continue;

			var $head = $(heads[i]);

			$head.remove();
		}
	}

	cloneHeaderRow(): void {
		var cons = this.AeviConsts;
		this.updateCols();
		var tableHeadEl = $(cons.tableHeadSelector);
		var aeviTable = $(cons.aeviWrapperSelector + ' table');

		aeviTable.find('thead').clone().prependTo(tableHeadEl).wrap('<table id="' + cons.tableHeaderSelector.makeClassFromSelector() + '" class="clonnedHead"></div>');
	}

	getTableColumnsWidth(): number {
		var width = 0;

		for (var i = 0; i < this.headerData.cols.length; i++)
			width += this.headerData.cols[i].width;

		return width;
	}

	setTableWidth(): void {
		var tableWidth = $(this.AeviConsts.tableHeadSelector).outerWidth() - this.AeviConsts.firstColumnWidth;
		this.headerData.tableWidth = tableWidth;
	}

	setHeaderRowWidth(fixedWidth?) {
		var columnsWidth = (aeviIsUndefinedOrNull(fixedWidth)) ? this.getTableColumnsWidth() : fixedWidth;
		var aeviTable = $(this.AeviConsts.aeviWrapperSelector + ' #aeviTable');
		var tableHeadEl = $(this.AeviConsts.tableHeadSelector);

		aeviTable.css('width', columnsWidth);

		$(this.AeviConsts.tableHeadSelector + ' table').css('width', columnsWidth);

		var tableHeaderHeight = tableHeadEl.outerHeight() * -1;
		aeviTable.css('margin-top', + tableHeaderHeight);

		this.headerData.headerWidth = columnsWidth;
	}

	getDifferenceSpace(): number {
		return this.headerData.tableWidth - this.headerData.headerWidth;
	}

	recalculateColumnsWidth(): void {
		var differenceSpace = this.getDifferenceSpace();
		var valueToAttribution = differenceSpace / this.columnsLength;
		var newColumnsWidth = 0;

		for (var i = 1; i < this.headerData.cols.length; i++) {
			var col = this.headerData.cols[i];
			var newColumnWidth = parseInt(col.width + valueToAttribution);
			newColumnsWidth += newColumnWidth;
			this.headerData.cols[i].width = newColumnWidth;
		}

		var tableHeadColumnsSelector = this.AeviConsts.aeviWrapperSelector + this.AeviConsts.tableHeadSelector + this.AeviConsts.columnSelector;
		this.setColumnsWidth(document.querySelectorAll(tableHeadColumnsSelector));

		var tableBodyColumnsSelector = this.AeviConsts.aeviWrapperSelector + this.AeviConsts.tableBodySelector + this.AeviConsts.columnSelector;
		this.setColumnsWidth(document.querySelectorAll(tableBodyColumnsSelector));

		this.setHeaderRowWidth(newColumnsWidth);
		this.checkEmptySpace();
	}

	/**
	 * index + 1 because 0 is rowNumber
	 */
	setColumnsWidth(columns) {
		for (var i = 0; i < columns.length; i++)
			AeviHeaderColumn.setWidth(columns[i], this.headerData.cols[i + 1].width);
	}

	setLastColumnWidth(emptySpace) {
		var lastColumnWidth = this.headerData.cols[this.headerData.cols.length - 1].width;
		var lastColumn = [];

		var lastHeaderColumnSelector = this.AeviConsts.aeviWrapperSelector + this.AeviConsts.tableHeadSelector + this.AeviConsts.columnSelector;
		var lastHeadColumn = document.querySelectorAll(lastHeaderColumnSelector);
		lastColumn.push(lastHeadColumn[lastHeadColumn.length - 1]);

		var lastBodyColumnSelector = this.AeviConsts.aeviWrapperSelector + this.AeviConsts.tableBodySelector + this.AeviConsts.columnSelector;
		var lastBodyColumn = document.querySelectorAll(lastBodyColumnSelector);
		lastColumn.push(lastBodyColumn[lastBodyColumn.length - 1]);

		for (var i = 0; i < lastColumn.length; i++)
			AeviHeaderColumn.setWidth(lastColumn[i], Math.floor(lastColumnWidth + emptySpace));
	}

	checkEmptySpace() {
		var newColumnsWidth = this.headerData.headerWidth + this.AeviConsts.scrollHeight;
		var emptySpace = this.headerData.tableWidth - newColumnsWidth;

		if (emptySpace > 0) {
			this.setLastColumnWidth(emptySpace);
			this.setHeaderRowWidth(newColumnsWidth);
		}
	}

	isColumnsWidthThinnerThanTableWidth(): boolean {
		return (this.headerData.headerWidth < this.headerData.tableWidth);
	}
}
