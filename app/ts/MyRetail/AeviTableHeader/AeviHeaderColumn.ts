 /// <reference path='../../references.ts' />

class AeviHeaderColumn {
	AeviConsts: any;

	constructor(aeviConsts: any) {
		this.AeviConsts = aeviConsts;
	}

	render(column: any, columnIndex: number) {
		var displayType = this.AeviConsts.dataTypes[column.DisplayType];

        if(column.ColumnName === 'RowNumber')
            return AeviHeaderColumn.renderRowNumberColumn(displayType);

        return AeviHeaderColumn.renderOtherColumn(column, columnIndex, displayType);
	}

    static renderRowNumberColumn(displayType: string): string {
        return '<th class="aeviSelectAll" id="col-0" data-type="' + displayType +'"></th>';
    }

    static renderOtherColumn(column: IAeviColumnDescription, columnIndex: number, displayType: string): string {
        var style: string = '';
        var renderSorting: boolean = true;

        if (column.Required) {
            if (column.Caption.charAt(0) !== '*')
                column.Caption = '* ' + column.Caption;
        }

        if (column.Visible !== true)
            style += ' display: none; ';

        if (!aeviIsUndefinedOrNull(column.EntityActionDefinition)) {
            if(column.EntityActionDefinition.ResultType === MyRetail.IAeviEntityActionResultType.Form) {
                renderSorting = false;
            }
        }

        var HTML: string = '<th id="col-' + columnIndex + '" class="aeviColumn col-' + columnIndex + '"' + 'style="' + style + '" data-columnName="' + column.ColumnName  + '" data-type="' + displayType + '">' + column.Caption;

        if (renderSorting) {
            HTML += '<i class="aeviSort default"></i>';
        }

        HTML += '</th>';

        return HTML;
    }

	refresh(column, columnIndex: number): void {
		this.setColWidth(column, columnIndex);
	}

	setColWidth(column, columnIndex: number): void {
		if (column.Visible === false)
			return;

		var columnWidth = column.Width;
		var maxWidthPixels = column.MaxWidthPixels;
		var minWidthPixels = column.MinWidthPixels;
		var widthType = AeviHeaderColumn.getWidthType(column.WidthType);
		var columnWidthPixels;

		if (widthType === 'Pixels')
			columnWidthPixels = columnWidth;
		else
			columnWidthPixels = this.getColumnWidthByPercents(columnWidth);

		columnWidthPixels = AeviHeaderColumn.getColumnWidthInPixels(columnWidthPixels, maxWidthPixels, minWidthPixels);

		var columnElement = document.getElementById('col-' + columnIndex);

		AeviHeaderColumn.setWidth(columnElement, columnWidthPixels);
	}

	static setWidth(column, value) {
		column.setAttribute('data-width', value);
		column.style.width = value + 'px';
	}

	getColumnWidthByPercents(columnWidth): number {
		var wrapperWidth = Math.floor($(this.AeviConsts.aeviWrapperSelector).outerWidth()) - 1;
		var gridWidthInPixels = wrapperWidth - this.AeviConsts.firstColumnWidth - this.AeviConsts.scrollHeight;
		var onePercentPixels = gridWidthInPixels / 100;

		return columnWidth * onePercentPixels;
	}

	static getColumnWidthInPixels(columnWidthPixels, maxWidthPixels, minWidthPixels) {
		if (maxWidthPixels > 0) {
			if (columnWidthPixels > maxWidthPixels)
				columnWidthPixels = maxWidthPixels;
		}

		if (minWidthPixels > 0) {
			if (columnWidthPixels < minWidthPixels)
				columnWidthPixels = minWidthPixels;
		}

		return Math.floor(columnWidthPixels);
	}

	static getWidthType(value): string {
		if (value === 0)
			return 'Pixels';
		return 'Percents';
	}
}
