/// <reference path='../references.ts' />

class AeviGridLocker {
	AeviGrid: any;
	AeviLocalization: any;
	lockButton: JQuery;
	protectedMessageElement: JQuery;
	realReadOnly: boolean;
	isLocked: boolean;	

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.AeviLocalization = this.AeviGrid.AeviLocalization;
		this.lockButton = $('#' + this.AeviGrid.tableId + '-lock');		
		this.protectedMessageElement = null;
	}

	lock(): void {
        if (!this.AeviGrid.AeviDataService.AeviUser.roles.Write) {
            this.AeviGrid.setSizes();
            this.isLocked = true;
            this.AeviGrid.AeviToolbar.refresh();
            return;
        }

        if (this.AeviGrid.firstLoad)
			this.realReadOnly = this.AeviGrid.AeviDataRepository.AeviTableDescription.readOnly;

		if (!this.realReadOnly && !this.AeviGrid.firstLoad)
			toastr.info(this.AeviLocalization.translate('applocked'));

		var message = this.AeviLocalization.translate('readonly_message');

        if (this.AeviGrid.firstLoad) {
            if (this.realReadOnly) {
                message = this.AeviLocalization.translate('first_load_readonly_message');
                toastr.info(this.AeviLocalization.translate('first_load_readonly_message'));
            } else {
                if (aeviIsUndefinedOrNull(this.protectedMessageElement)) {
                    this.renderProtectedView('<div class="aeviProtected"><p>' + this.AeviLocalization.translate('protected_mode') + '</p><span class="aeviProtected__unlock">' + this.AeviLocalization.translate('enable_editing') + '</span></div>');
                }

                toastr.info(this.AeviLocalization.translate('readonly_message'));
            }
        }

		this.AeviGrid.AeviStatusBar.show(message, 'info');

		this.AeviGrid.setSizes();
		this.isLocked = true;
		this.AeviGrid.AeviToolbar.refresh();
	}

	unLock(): void {
        this.AeviGrid.AeviDataService.getBlockStateCommand()
            .done((response: MyRetail.IAeviBlockStateCommand) => {
                if (response === MyRetail.IAeviBlockStateCommand.CONTINUE) {
                    this.unLockWithoutAccessControl();
                }
            })
            .fail((response: MyRetail.IAeviBlockStateCommand) => {
                if (response === MyRetail.IAeviBlockStateCommand.RELOAD) {
                    this.AeviGrid.AeviDataService.getDataByQuery('', true);
                    this.unLockWithoutAccessControl();
                }
            });
	}

    unLockWithoutAccessControl() {
        toastr.info(this.AeviLocalization.translate('appunlocked'));

        this.AeviGrid.AeviStatusBar.show(this.AeviLocalization.translate('appunlocked'), 'info');
        this.AeviGrid.AeviGridHandler.triggerClick();
        this.removeProtectedView();

        this.AeviGrid.setSizes();
        this.isLocked = false;
        this.AeviGrid.AeviToolbar.refresh();
    }

	renderProtectedView(html) {
        if (!this.AeviGrid.AeviDataService.AeviUser.roles.Write) {
            return;
        }

        this.AeviGrid.aeviWrapper.append(html);
        this.protectedMessageElement = $('.aeviProtected');
        this.protectedViewListener();
	}

	removeProtectedView() {
        if (this.AeviGrid.AeviDOM.length('.aeviProtected'))
            this.protectedMessageElement.remove();

        this.AeviGrid.lockMessageShowed = true;
	}

	protectedViewListener() {
        $(document).off('click', '.aeviProtected__unlock').on('click', '.aeviProtected__unlock', (event) => {
            event.preventDefault();
            this.unLock();
            this.AeviGrid.AeviDOM.closeModals();
        });

        $(document).off('click', '.aeviProtected__save').on('click', '.aeviProtected__save', (event) => {
            event.preventDefault();
            this.AeviGrid.AeviToolbar.AeviToolbarHandler.save();
        });
	}
}
