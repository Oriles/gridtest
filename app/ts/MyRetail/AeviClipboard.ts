/// <reference path='../references.ts' />

class AeviClipboard {
    AeviGrid: any;
    browser: string;
    AeviDataService: any;
    canPasteOneCell: boolean;
    copyModalId: string;
    countOfCells: number;
    countOfRows: number;
    copyModal: IAeviModal;
    isDialogRendered: boolean;

    constructor(aeviGrid: any) {
        this.AeviGrid = aeviGrid;
        this.browser = this.AeviGrid.AeviClientSide.browser.toLowerCase().replace(/[^a-zA-Z]/g, "");
        this.AeviDataService = this.AeviGrid.AeviDataService;
        this.canPasteOneCell = true;
    }

    getClipboardMode() {
        /**
         * 0 = classic mode
         * 1 = extended clipboard mode
         */
        return this.AeviGrid.mode.clipboard;
    }

    allowOneCellPasting(): void {
        this.canPasteOneCell = true;
    }

    disableOneCellPasting(): void {
        this.canPasteOneCell = false;
    }

    copy(): void {
        /**
         * event.clipboardData.setData('text/plain', textToPutOnClipboard);
         * event.preventDefault();
         */
        if (aeviIsUndefinedOrNull(this.isDialogRendered) || !this.isDialogRendered) {
            var textToPutOnClipboard = this.AeviDataService.AeviDataRepository.AeviTableData.getRecordsToClipboard();
            this.renderDialog(textToPutOnClipboard);
        } else {
            setTimeout(() => {
                this.hideDialog();
            }, 200);
        }
    }

    checkPastedData(parsedData, selectedCellIndexes) {
        if(aeviIsUndefinedOrNull(parsedData))
            return false;

        var pastedRowsLength = this.getPastedRowsLength(parsedData);

        if(this.isPastedRowsLengthExceeded(pastedRowsLength)) {
            this.showExceededModal();
            return false;
        }

        var pastedColumnsLength = this.getPastedColumnsLength(parsedData);
        var possibleColumnsLength = this.AeviGrid.AeviDataRepository.AeviTableDescription.getCountOfPossibleColumnsToCopy();

        /**
         * check maximum rows allowed (fixedSize)
         */
        if(this.isPastedRowsLengthExceededFixedSize(pastedRowsLength, selectedCellIndexes)) {
            this.AeviGrid.showMaxRowCountMessage();
            return false;
        }

        /**
         * check columns (columns.length must be the same as in the sample excel file)
         */
        if(pastedColumnsLength !== possibleColumnsLength) {
            if(pastedColumnsLength !== 1) {
                if(this.getClipboardMode() === 0) {
                    this.showOneCellCellModal();
                    return false;
                }
            }

            if(this.getClipboardMode() === 1 && !this.canPasteOneCell) {
                this.showWrongColumnsModal();
                return false;
            }
        }

        /**
         * check columns (columns.length must be the same in the every row, if no -> it is not table format)
         */
        var columnsLength = [];
        for(var i = 0; i < parsedData.Rows.length; i++) {
            var rowColumnsLength = 0;

            for(var j = 0; j < parsedData.Rows[i].length; j++)
                rowColumnsLength++;

            columnsLength.push(rowColumnsLength);
        }

        var firstRowColumnLen = columnsLength[0];

        for(i = 0; i < columnsLength.length; i++) {
            if(columnsLength[i] !== firstRowColumnLen) {
                this.showWrongTableModal();
                return false;
            }
        }

        /**
         * check if one column
         */
        if(columnsLength[0] === 1) {
            /*
             * if one columns and more rows
             */
            if(pastedRowsLength > 1 && this.getClipboardMode() === 0) {
                this.showOneCellCellModal();
                return false;
            }

            return true;
        }

        /**
         * because ↓↓ conditions not required
         */
        if(this.getClipboardMode() === 0) {
            this.showOneCellCellModal();
            return false;
        }

        /**
         * check if row will be paste in row with status "newrow"
         */
        if(this.getClipboardMode() === 0) {
            var rowStatus = this.AeviGrid.AeviDataService.AeviDataRepository.getStatusByRowIndex(selectedCellIndexes.rowId);
            if(rowStatus !== this.AeviGrid.AeviConsts.statuses.newrow) {
                this.showEndTableModal();
                return false;
            }
        }

        /**
         * check if user try paste to different cell than to the first
         */
        if(this.getClipboardMode() === 0) {
            if(selectedCellIndexes.cellId !== 1) {
                this.showDifferentThanFirstCellModal();
                return false;
            }
        }

        return true;
    }

    getDataToBePasted(event: any, dataToBePasted: string): string {
        if (aeviIsUndefinedOrNull(dataToBePasted)) {
            return this.getClipboardData(event);
        } else {
            return dataToBePasted;
        }
    }

    setClipboardMode() {
        this.AeviGrid.mode.clipboard = 1;
        this.disableOneCellPasting();
    }

    paste(event, dataToBePasted?: string): void {
        event.preventDefault();

        if (aeviIsUndefinedOrNull(dataToBePasted)) {
            dataToBePasted = null;
        }

        var clipboardData: string = this.getDataToBePasted(event, dataToBePasted);

        if(!aeviIsUndefinedOrNull(this.AeviGrid.isClipboardModalOpened) && this.AeviGrid.isClipboardModalOpened) {
            this.setClipboardMode();
        }

        if(_.isNull(clipboardData)) {
            this.AeviGrid.print('AeviClipboard.paste(), variable "clipboardData" is null.');
            return;
        }

        var selectedCell = this.AeviGrid.AeviDOM.getSelectedCell();

        if(_.isNull(selectedCell) && this.getClipboardMode() === 0) {
            this.AeviGrid.print('AeviClipboard.paste(), variable "selectedCell" is null (no cell selected).');
            return;
        }

        var selectedCellIndex: IAeviCellIndex = (_.isNull(selectedCell) && this.getClipboardMode() === 1) ? null : this.AeviGrid.AeviDOM.getCellIndexes(selectedCell);
        var parsedData = this.parseData(clipboardData);

        if(!this.checkPastedData(parsedData, selectedCellIndex)) {
            this.AeviGrid.print('AeviClipboard.paste(), input clipboardData is not valid.');
            return;
        }

        // pasting one cell in extended clipboard mode
        if(this.canPasteOneCell && this.getClipboardMode() === 1) {
            this.AeviGrid.AeviDataService.pasteOneCell(parsedData, selectedCellIndex);
            this.AeviGrid.AeviGridEditor.refreshData();
            return;
        }

        if(this.getClipboardMode() === 1) {
            var indexWhereDataWillBePasted: IAeviCellIndex = {
                rowIndex: 0,
                cellIndex: this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.getFirstDataColumnIndex()
            };
            selectedCellIndex = indexWhereDataWillBePasted;
            this.AeviGrid.AeviDataService.AeviDataRepository.deleteRepositoryTableData();
            this.AeviGrid.AeviGridEditor.refreshData();
        }

        var reloadData = false;

        if(this.getClipboardMode() === 1) {
            this.AeviGrid.AeviDataService.commit(null, reloadData).done(() => {
                this.AeviGrid.AeviDataService.paste(parsedData, selectedCellIndex);
                AeviModal.closeAll();
            });
        }else {
            this.AeviGrid.AeviDataService.paste(parsedData, selectedCellIndex);
            this.AeviGrid.AeviGridEditor.refreshData();
            this.AeviGrid.AeviPager.AeviPagerHandler.setJumps();
        }
    }

    getClipboardData(event) {
        var clipboardData = null;

        switch(this.browser) {
            case 'chrome':
            case 'safari':
            case 'firefox':
            case 'opera':
            case 'edge':
                clipboardData = event.clipboardData.getData('Text');
                break;

            case 'ie':
                clipboardData = window.clipboardData.getData("Text");
                break;

            default:
                toastr.error(this.AeviGrid.AeviLocalization.translate('clipboard_support_message'));
                return null;
        }

        return clipboardData;
    }

    parseData(clipboardData) {
        var _this = this;
        var outputData: any = {};

        var lastChar = clipboardData.slice(-1);
        var splitClipboardData = clipboardData.split(/\r\n|\r|\n/g);

        var dataRows = splitClipboardData;
        outputData.Rows = splitClipboardData;

        var countOfRows = outputData.Rows.length;
        var cells = [];

        for(var i = 0; i < dataRows.length; i++) {
            cells[i] = dataRows[i].split("\t");
            outputData.Rows[i] = cells[i];
        }

        var countOfCells = outputData.Rows[0].length;

        /*
         * IF EXCEL => DELETE LAST CHAR \n
         * TODO: REFACTOR
         */
        if(AeviClipboard.isLastCharEmpty(lastChar)) {
            outputData.Rows.length = (Array.isArray(outputData.Rows[countOfRows]) ? outputData.Rows.length : outputData.Rows.length -1);
            countOfRows = outputData.Rows.length;
        }

        if(this.AeviGrid.AeviClientSide.isSafari()) {
            if(AeviClipboard.isLastCharEmpty(lastChar)) {
                outputData.Rows.length = (Array.isArray(outputData.Rows[countOfRows]) ? outputData.Rows.length : outputData.Rows.length -1);
                countOfRows = outputData.Rows.length;
            }
        }

        this.countOfCells = countOfCells;
        this.countOfRows = countOfRows;

        return outputData;
    }

    static isLastCharEmpty(lastChar) {
        if(lastChar === '\r\n' || lastChar === '\n')
            return true;
        return false;
    }

    renderDialog(data) {
        var copyModalId = this.AeviGrid.AeviConsts.copyModalId;

        this.copyModal = new AeviModal(this.AeviGrid);

        this.copyModal.setContent({
            id : copyModalId.replace('#', ''),
            title : this.AeviGrid.AeviLocalization.translate('warning'),
            text : '<p>' + this.AeviGrid.AeviLocalization.translate('manual_copy_message') + '</p>' + '<textarea>' + data + '</textarea>'
        });

        this.copyModal.show(null, () => {
            this.hideDialog();
        });

        this.AeviGrid.AeviGridHandler.removeCellEditor();

        $(this.AeviGrid.AeviConsts.copyModalTextareaSelector).select();

        this.isDialogRendered = true;
    }

    hideDialog() {
        this.copyModal.close();
        this.isDialogRendered = false;
    }

    isPastedRowsLengthExceeded(rowsLength) {
        var maximumPastedRows = (aeviIsUndefinedOrNull(this.AeviGrid.AeviDataRepository.AeviTableDescription.description.MaxRowCountExcel)) ? this.AeviGrid.AeviConsts.clipboard.maximumCountOfInsertedRows : this.AeviGrid.AeviDataRepository.AeviTableDescription.description.MaxRowCountExcel;
        return (rowsLength > maximumPastedRows);
    }

    isPastedRowsLengthExceededFixedSize(rowsLength, selectedCellIndexes) {
        if(aeviIsUndefinedOrNull(this.AeviGrid.AeviDataRepository.maxRowCount))
            return false;

        if(aeviIsUndefinedOrNull(selectedCellIndexes)) {
            selectedCellIndexes = {
                rowId : 0,
                cellId : 1
            };
        }

        var firstPastedRowIndex = selectedCellIndexes.rowId;
        var newRowsLength = rowsLength + firstPastedRowIndex;

        return this.AeviGrid.AeviDataRepository.maxRowCount < newRowsLength;
    }

    getPastedRowsLength(clipboardData) {
        if(aeviIsUndefinedOrNull(clipboardData)) {
            this.AeviGrid.print('AeviClipboard.getPastedRowsLength(), parameter "clipboardData" is undefined or null.');
            return null;
        }

        if(clipboardData.Rows.length < 1) {
            this.AeviGrid.print('AeviClipboard.getPastedRowsLength(), parameter "clipboardData.Rows.length" is lower than 1.');
            return 0;
        }

        return clipboardData.Rows.length;
    }

    getPastedColumnsLength(clipboardData) {
        if(aeviIsUndefinedOrNull(clipboardData)) {
            this.AeviGrid.print('AeviClipboard.getPastedRowsLength(), parameter "clipboardData" is undefined or null.');
            return null;
        }

        if(clipboardData.Rows.length < 1) {
            this.AeviGrid.print('AeviClipboard.getPastedRowsLength(), parameter "clipboardData.Rows.length" is lower than 1.');
            return null;
        }

        if(clipboardData.Rows[0].length < 1) {
            this.AeviGrid.print('AeviClipboard.getPastedRowsLength(), parameter "clipboardData.Rows[0].length" is lower than 1.');
            return 0;
        }

        return clipboardData.Rows[0].length;
    }

    showExceededModal() {
        var maximumPastedRows = (aeviIsUndefinedOrNull(this.AeviGrid.AeviDataRepository.tableDesc.MaxRowCountExcel)) ? this.AeviGrid.AeviConsts.clipboard.maximumCountOfInsertedRows : this.AeviGrid.AeviDataRepository.tableDesc.MaxRowCountExcel;

        this.showModal({
            title : this.AeviGrid.AeviLocalization.translate('warning'),
            text : '<p>' + this.AeviGrid.AeviLocalization.translate('paste_exceeded') + ' ' + maximumPastedRows + ' ' + this.AeviGrid.AeviLocalization.translate('rows') + '</p>'
        });
    }

    showWrongColumnsModal() {
        this.showModal({
            title : this.AeviGrid.AeviLocalization.translate('warning'),
            text : '<p>' + this.AeviGrid.AeviLocalization.translate('paste_wrong_columns_count') + '</p>'
        });
    }

    showWrongTableModal() {
        this.showModal({
            title : this.AeviGrid.AeviLocalization.translate('warning'),
            text : '<p>' + this.AeviGrid.AeviLocalization.translate('paste_wrong_format') + '</p>'
        });
    }

    showWrongTableColumnsAndRowsModal() {
        this.showModal({
            title : this.AeviGrid.AeviLocalization.translate('warning'),
            text : '<p>' + this.AeviGrid.AeviLocalization.translate('paste_wrong_rows_and_cells') + '</p>'
        });
    }

    showEndTableModal() {
        this.showModal({
            title : this.AeviGrid.AeviLocalization.translate('warning'),
            text : '<p>' + this.AeviGrid.AeviLocalization.translate('paste_wrong_target_with_row') + '</p>'
        });
    }

    showDifferentThanFirstCellModal() {
        this.showModal({
            title : this.AeviGrid.AeviLocalization.translate('warning'),
            text : '<p>' + this.AeviGrid.AeviLocalization.translate('paste_wrong_target') + '</p>'
        });
    }

    showOneCellCellModal() {
        this.showModal({
            title : this.AeviGrid.AeviLocalization.translate('warning'),
            text : '<p>' + this.AeviGrid.AeviLocalization.translate('paste_allow_one_cell') + '</p>'
        });
    }

    showModal(content: IAeviModalContent): void {
        var modal = new AeviModal(this.AeviGrid);
        modal.setContent(content);
        modal.show();
    }
}
