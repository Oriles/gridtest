/// <reference path='../references.ts' />

class AeviGlobal {
    static isNegative(value: any) {
        if(aeviIsUndefinedOrNull(value))
            return null;

        var stringValue: string = value.toString();

        return stringValue.charAt(0) === '-';
    }

    static removeDecimalPlaces(num: any): string {
        var strNum: string = num.toString();

        var parts = [];

        if (strNum.indexOf('.'))
            parts = strNum.split('.');

        if (strNum.indexOf(','))
            parts = strNum.split(',');

        return parts[0];
    }

    static getDecimalPlaces(num: any): number {
        var strNum: string = num.toString();

        var parts = [];
        var hasDecimalPoint = true;

        if (strNum.indexOf('.')) {
            parts = strNum.split('.');
            hasDecimalPoint = true;
        } else
            hasDecimalPoint = false;

        if (strNum.indexOf(',')) {
            parts = strNum.split(',');
            hasDecimalPoint = true;
        } else
            hasDecimalPoint = false;

        if (!hasDecimalPoint)
            return null;

        return parseFloat(parts[1]);
    }

    static isElementInBodyViewport(element, bodyEl) {
        var cell = AeviGlobal.getBounding(element);
        var body = AeviGlobal.getBounding(bodyEl);

        var isInViewportVertically = (cell.top >= body.top && cell.bottom <= body.bottom);
        if (!isInViewportVertically)
            return false;

        var isInViewportHorizontally = (cell.left >= body.left && cell.right <= body.right);

        if (isInViewportHorizontally)
            return true;
        return false;
    }

    static isElementInBodyViewportHorizontally(element, bodyEl) {
        var cell = AeviGlobal.getBounding(element);
        var body = AeviGlobal.getBounding(bodyEl);
        return (cell.left >= body.left && cell.right <= body.right);
    }

    static isElementInBodyViewportVertically(element, bodyEl) {
        var cell = AeviGlobal.getBounding(element);
        var body = AeviGlobal.getBounding(bodyEl);
        return (cell.top >= body.top && cell.bottom <= body.bottom);
    }

    static getBounding(element) {
        return element.getBoundingClientRect();
    }

    static stringToHTML(str: string): string {
        if(aeviIsUndefinedOrNull(str)) {
            return null;
        }

        var HTMLString = str.replaceAll('\r\n', '<br>');

        HTMLString = HTMLString.replaceAll('\r', '<br>').replaceAll('\n', '<br>');

        return HTMLString;
    }

    static getEmptyImageBase64() {
        return 'iVBORw0KGgoAAAANSUhEUgAAAnUAAAHQCAMAAADwEGymAAAABGdBTUEAALGPC/xhBQAAAwBQTFRFsLCwsbGxsrKys7OztLS0tbW1tra2t7e3uLi4ubm5urq6u7u7vLy8vb29vr6+v7+/wMDAwcHBwsLCw8PDxMTExcXFxsbGx8fHyMjIycnJysrKy8vLzMzMzc3Nzs7Oz8/P0NDQ0dHR0tLS09PT1NTU1dXV1tbW19fX2NjY2dnZ2tra29vb3Nzc3d3d3t7e39/f4ODg4eHh4uLi4+Pj5OTk5eXl5ubm5+fn6Ojo6enp6urq6+vr7Ozs7e3t7u7u7+/v8PDw8fHx8vLy8/Pz9PT09fX19vb29/f3+Pj4+fn5+vr6+/v7/Pz8/f39/v7+////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIl5M9gAAAAlwSFlzAAAOwgAADsIBFShKgAAAAAd0SU1FB98EFAsxKPr6IvsAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAACsiSURBVHhe7Z0JY+M4lqTZaYmk5Ps+Ur7STl+6RVKUs3pm9urZnZ6uyv//bzbiAZIlUc7Kykpjt8fx2ZJI4OEBBEMAeEHReQucOz6ent6M/+Prr7/+9ttvX/+jvD0/+ziNmtI6g/ntv/z2G42+fv0tQ5CP8mAVfs7O+39D/Nff/vHb1/91X7H5SMet1uUDTIx//Ntt6+PZWeuiYtq6+yt8fEWGX//3oHX28bSaI0Kf/ictUKRf//XxolUpNor08fw6+z+/fWWpv/72r5/OKiYwOvt4/vlfbPP/8Y9f/31wuZzTeevk7BRG9391pf769W/91tnpSdUZ6vT57+YJGQ4uWmcn1fywwTf/+qtt2m//+d+fLl2tLILavhr83cqMbftvN2enlSKh2k7PbiZ///oP2yX/PrquZoX9doqa/JsrNHhsoZL47+OntK6K//j1t19ZT//5fAvXVQmgUi76LA+37u/PN/BSlcnHs/Pu/3C7Dfut34IgfAyrOdpsNJrrDnw2tq7yL8/gyy/ZRbNhYQs01zfSdPPuy5fnL3g9P181ly3W6bDZbHwcThD/y/Pz5H5vyQ3WNjY21uH/kHmR8nEzZsysMDOSnYfJczl+/iuKdI6kVQsENI97yOl5Uj5/aR+kqY94gSbp+sX4F/DlefKlvVXxgrxZ8J37Ccv95Ze/ZieWbg5sfGMDn429h1+w+WRwljSaoFqq9HP5ZTIpkd8vZzRYR20u0WzudJgV/JSdw0ZjHXXiozwoUNL4WP7yPGZtT562uE8WQdYowe7nAl4m4+dfRq1qTo0UJW/GW30rM/jleHnbPM2N1oDbj2KVt1sN7KVGWjFsNlt//cK6/vKluNlIEF8p9kYzOWhjy6wuB8dpc2NqwpqI0jjd3t3eIXjf3rsaFEWRZUUx+LS3vbW1ZTEvbG1tb+3s3+d5keXZOBtdVSx2tgGszrujPMtyeHs42lk0osX29g48neWe0f0+A+De20zZ3jp6QG45SlT0znc2EVAxQdBJu4AXmOWPJ1ULbBp8b1+OYIMiZUV7H/kvgWTg8C4vxgWL3W9hc32cA2vbu9hgFGlspc6LbmuTNrY982D9dggnNBq3sGpl8JGe7e3dgyeUB2TDNou97MXKvXs+oh9s2+gRu2S53AxBsW+HLE4+Hvcv9irbto1CopgHXVgY41NfpGW2di8G8MMdN7o92Mb+ru6Snc2tC/gZcpcMb5Eb6tbHeEwAp21s18hq8gz5ey+ISpMoSbeun+4fjHv89VDjY1Dkg6dHC1vk/v7+6WE4zscoWgmHD3c+Yp67+/u7XpZz742L8ai95OUeq3f26jArUgweHy18OcP7hzb2XlmyQrPu/Z23mgdFenwaWWYo1ahTtQAIu+viq4LqQpHyh8/VciPV/cPTgEVmXeW9ZUfYqvunu7vHu6eR1RGKnXU+w+gezpZNH/pwAovxuOw+woBmPtLDco9Qathh/7SxupQfnd4/PHax7SU2HzX+8LjC5PEJjvompnFZZF1UkY/z3DPk7rHdzlgcKzd2CYyWfTGkh9Jg600AD/esporV3X3P6shU8ogiVcrE/dTGtrEKUN9tCGIa9fApaURxstt9njhKMsaeYbmKcWlhFvMCLCaIhys0Gfm4amBeJiWFYoWH+Co++IdAVtK0GswInUg1Q2TGKsdrTM+TFe7oyuoKb9yEZQuAIGSFwlh9TcZF1cYVnK0YJQ5r5rYAPKMXRwnKWbHNalWOLAcqCRbFmAarTOiBIsc7lqsGVk8woi75PoGnSpnKCcoEM9vHtKv4YcBzWT5zj7DUcOV2wKoczQ8KREfc1GqOhLVNI1gxcx86BesYFNnGsT6tBnzU5EtvLaXqOjQwnCf7o5Qt60VYbrzwhp0D2WGpAotCC7dgX8ElmM7805MBabrad6uL0AdrlE4hdh86DyIylxxFXu3EkvpMqU0fOo+rbmdEOx+8CGv6JQpLVjK/OgcCKU7EomR06cPngQzgDJG0Xp0dC2UODKvSJWzXM7HtDb+8CL/6TDrbn1z1CZaBOxSWbzRHntM088AhDPAyO/paxhUUdeDKbmHGpLuGcV26+2Tb7KOoTmaIMFvyUTNoYl8/7Lmc7dmyBbeGjpyL6cISNOKHMwDMEnZcWsJFM57/Jha/C15wcTMzLiyCFLZvuIQFjhIrTqyYVllTLxU/2GiLe9kTPtWKMtGa20d9u13og5ewCHO0nBsxhaPCmQ3fIUAfM8UntcK4l4+YgyqyQpuZt4JdtUi2YcjI9gwTrnIHKy8Ns3G+FrBwq0vqnQYzi3Gn3sC4brc9DfHxZvOKOx+NNyzSbpWFGUxduIVlzIF9OizAErrwF1w04+3frS0xjfNmKyzILHh1tA9mau9lpR/GmZXhU620JDOr1wxe0q82YQSjnIGtLeKTWjbu5SPmYNCLmbdaYQd8vC0wkS1XYKQzelmex6W1KL7Nuym7NYzr0r2XHtYwM+ISVHBFcQurLX6I73P1mtVPLAj4md6+q45+x8ZF/9xt/AbfkdEfKMuiaT7poYdNKqoT4u0oJj20dVKdCIlUJ8Ij1YnwONVVjyaEeDvc0YRUJ0LiVJfGu22pToSiKDv1erS3ddyV6kQwyt72ZnT76W7k14V4c4rx6OY2Go3cPSZChABiGxURFCfRiWBAbGUZ2e2SQoSiKPKRU51aOxEKdLF5tHDblRBvjamON/ZJdSIUvPEri9DS6RhWBIOqQw/rFoQIAsVWRKPhSD2sCEY+LoaD6Ob6bjR9zk6It6YoRp+uo52tk64PECIA3Z2tqJHuzR7AF+Lt6SQxn4ftqIcVwSg79ZTPw3YnuigmgtGpNaI02euqrROhKMpunTNOaFwnwpFPelBdnOoOdhEOPZkowgPVacYJERipToRHPawIj1QnwiPVifBIdSI8Up0Ij1QnwiPVifBIdSI8Up0Ij1QnwiPVifBIdSI8TnWal1iEJIfqUqlOBMWpzn4zUYhAFGVnrRnFNT2tI8JRlL1aPVpPDrqa50SEohh3G3F0eXbd06ROIhRF0W+dRvbj3FKdCEQxfi5G0QSfEp0IRp5NikhzJoqgFEVhM8RKdyIY0FpWRKNxoXmJRTDQ1BVlZA2dZCdCgTauiEq8qbETwbBxHT5HmVQnQlGgkcO4blyqhxWh4IhuEmXjPFcPK0IB1Y2eo/29s76u/otQ5OVgdztaT/ee1MOKUOSTXlyPkmS3rbZOhILzEidRLdHM/yIcbjbserrX9gFCvDnoYTVXpwiMnkwU4ZHqRHikOhEeqU6ER6oT4ZHqRHikOhEeqU6ER6oT4ZHqRHikOhEeqU6ER6oT4ZHqRHikOhEezhAr1YmwONXVpToRkKLsmup2dQe7CEiHd7AnO3pGTISj7NSSqFHf6U5yHyLEW1N26vVoM93XzP8iIN31NGqdXvYlOhGKohicnUTd3iDzAUK8OUVRdHtRlmvSRBGOYlxkeQTt5TqYEMFAI5dHfNck7CIU1spFaPH0MyciGJwGe+xUJ0QgTG8RZ2IXIhTQ3CTTLzqJoGBcNxlF9ulDhHhz8qLMo/ZTN9PRhAgFxnXtx+jo4Lwv1YlQFOPB0UGUxnsdHyBEADpxGsX1nbbuJRahKMruWqrnJkRQ9LSOCI+eTBThkepEeKQ6ER6pToRHqhPhkepEeKQ6ER6pToRHqhPhkepEeKQ6ER6pToRHqhPhkepEeKQ6ER6nujjd0wyxIhj5pLfWiNJkt6NZE0UoirIT16OUs2FLdSIYnaTmelipTgSiKLv1OKolmpdYhMM9I5Y29p7U1olQYFy31ozS+o6OJkQwikm/Fkdb6X5HbZ0IRVE8NtKo0+7kUp0IRTEuHh+iSVlqCjsRjKIoS86aWBaag12EogBQHfpXqU6EgnOwj6MJxaceVoQiRxcblXnGIZ4QYWBjF9moTqoToWDPGuVSnQiI+5UT+7kJqU4EAv0r2rrhMJPmRDiKfDiMbi7vR5KdCMfo00W01Tjs+lUh3pyi7G4kUZzsag52EQw+N5FKdSIoTnV6MlGERM/DivBIdSI8Up0Ij1QnwiPVifBIdSI8Up0Ij1QnwiPVifBIdSI8Up0Ij1QnwiPVifA41cWcq1O8gu6y/tk41aUJ2jpV7irs4XS8uefU+ZTJ2GqqRN/A7oHx9vCJ+H7c/XWNWDPEvgKlxs+cwipyKC3rdR4f7h8eHx87/bws+UBxbpIU30tRdmtQXaIe9hVcEzcVVZF3765bp0cH+/uHh0enreuH7iA3ZUp5f4hOHUcTnPnfr4sF8jwvi8wauiIftm9Ot9fX00aa4tVYbzS2T64eh0VJM/Wz30/ZidOoFkt1r0Ap5WzO8nzUvthbb6RJHCcgTur1OI7T5t5F254mnr7E71O247rOnLwK2zjXd+bD691mGtfjNEFLB9mhwaMA6/HG4XUXfSylJ9V9H2UnidXDvopTEnvZp5PtBho4SC5JG1QclMe2Lq4ljc2zh4wjO6nuOym7SS1q1PVk4mr8ccI4vz9omuLY1rF7hd7Q1CEIK2nSOLgb6WDiD9BOa9HRzse+ztetBP0rWrrRzV4KnXE854Tmlqi8BBFx3Ni7GfkU0352rj6lx0WKcX9vN3q8fcrUO6zEus3sdjeN2dLZkG76AhYEDab1ePd2aMewTmH2zvEgPl0A34QjH+ef7yIbMPsQsQDqpcju91275vW2DIUXJ7ufhzTm8M4Exxc7Zx4FOxUKD2ojzyO/IqpwVPdw1ISqvMRWYK1eEh88cVJxO3NHwWV8Qw1jXV/qFUh1rwPF9E/TOg8ZvMaqMDJO643zIb7FeTlhIrZwuU33zJmf8eH9iSlS3eugzbrasAOHb6uuhuitK8guM5FZl0rVlZQf1/km5pDqXgViedpNGw10sBjYvQZi7SzK4RP6VQhu2Lm/74zGk9I6W2v6vD8xQ6p7laLIPjY5cnPHr6vhuK7BBm/jYljm3euzo/29/YPj1p3vYHEgLNlVkOpeJ3/cThs8Vfetps61dvg8eBjeHm25w4u0uddqZ26Mpx62ilT3Ollrnb2nna7zcMmtOXXZy3WyG6fn2zyL4tbT9PBuRNVJckuwQqIs08hjEVYHa6To7zXjetp4OZbggumM/5SdW8EHR3ZNjgCnRgja+zwyX2rr5sFQN8ujm9vHl+s5AvCcB38yN7vZ9BKbYlfBkgYavyRZ39xo2kJq7ZsT3jxJsnfPn5BRc7dAUWaf7qKt9EBzsC+QYyhG1Y1a6ytUx5tO4iReP+v2zhr1pB5j1alxWXVx8+hpYre5e8cCFGV3PYkaNf3u/wJ2voNHAf1D09kcJq06WrZ682L0XPTP0jrCXPCS5mBcj5sXPJyAiMULZTv+EDWTne5EFfMCvoGQXJ4X7e2KlNCXxjW0YuutYVmMyp5dueBhLF/eZgrviNp/0qh5GburM46lukXQ0LG1y+42KKZ5KMI4adSbrcGEl1rH3aO4TmlyULfUv1ro5mWmMyfLdOI4qvFeYtXLHOwSUSGjTxvLTR1W+d887U0w8itx1PF4CMHxcGJZoFhHTOOIh7Gq3TmKslOL+dyEnkxcoOBlhaIcXdsh7AI8Xxynp70yz+x3ddENO9lZ2zYP1iG6ZKdvIhYz8km3lkRpXU/reHybZJcT8rFd+Z9qyL0gpSRpHne/jIe8la5EiuJxHzrkiTpvMwOdbiPefEBP7NwKA23dWhw1Y6nOM6e6PHtNdfFRv8zzcpwV/Mch6sNejYexq1QXJ9uf1cEuUuAYdj1KOM+JD3nPQBw8tQaJoEtEK1YMr+x0HTA9oW9N4kZaPx5wpAa9WRL8Z/f7GNjFKZ8im7uOgaWkXt+5y3g8IWZwxgk3p5NURwnZvcB25TQf8Vjh0/QYloJLUjs5nJx22LdSnJaIz1aMbvcQQYXaRQpLQrCytvmQQ8vOWBD9et0ivAeTyoOmsnE5vD1AW2caYscKXfGg9KA7gQFtfBqKdHS762RnRlPZsfmLt7t052wF0ayJS1inyaYOfW3nfMfUZgLimREO3tL9x7zEaA7y9N0mh4BFOfy0DcGhZbRLspaEGsTSER+VlermkOqWsKtXTncPR00+cuh6WMonjhtJevAwzqZP5EyT8BaK8fByK4a1tXYz1aVxff08o0NnK4hUtwhFhxcPTm93TXBTBeFIAUvJ7r0/SVfylJ6jKMoyy8vBxaYds+KQwimVDWQS791lOl+3iFS3iDVh0EiZf9pE32rzSzgB2cnheO8eNjy6ta7YJ4KoeFtJOTjfqJvsfCL2zknjrG/dsXhBqlsEkqPovhRXO426Ozp4UV2S7HwuJyMebpQ28POJxtmomKAFLPutDTvksGaRqsNSenDX082Li0h1i1hTh97zYR+aYUfJaZucgpKktn3HWzT5/A1PwM0aMKZxSbutZo2m7nACC7wLqrl7cjdtGdXmEaluEaoHr95J058AoYQIT4Hs3Bb+WUMbqM0UxHX2y3jrnjU5mrMkwLrletLYbnWm55+lO6luGSojL4YY1Pkmzt4B1Ne4zp8LXlJdOIIFrtGzoOLp0InVJzPQaDaP73lMgU5ZqpPqKuRZXmSPB9Mm7oW4ttnBMQMVRnXNyw4qRO3xPoDxeNjiVbFF1WEtqe98hmB5Y5SQ6paBrIr+eaO+LDqM8jbv+sPRaDgaDgYjzryOTtYlKUb9Qb8/GPQHw+HTEY8lllOnaS3Zvx3ZvQI+1XtGqlsCUsof9+tLfaQb1+0ef2y1Phpnj3YfsVEM7xncOm99bH3c4xVb62TnwXpS376j5KQ6qa4CVDe43IByKqpLa3HaxOiu0eT5uEv/nCsouqe0QAyieLbFHYfMw/MptfiwU84u3r5rpLolinHZPnq5qDWD6whFz8vzdvGHixFP2Tl6pzWKDa86n6Cojut4bRbJ0rOBNEec6uJEdzrNKO527Sr+EjwdQuXwKlea1C55cdUn6J5QdRCjSY4C9GlmxPWUDzNu3rgU7x3e6YS2TjP/zyhG11vWYnnBeCwEqmM7hlf9asR20cg7xzW0ZmwL2aqZYBdTMzGCk/Rw4JK8c3hXZxJtrR92pTpPr7XBdm1ZdU47VBybvXnVoa2ruzPDlszO9C2mZkNoyTc/86SL/bu075XuRiO6OPs01DDXUXZO/GXUV4B4Uqju5Qkcqo6BL4kqyZkEukyap7yIwdMz0zHh+6QYXpxEw0y/0TEFBxP+usSrONV5e6oOPew3hUqs5012+jyKRWW/c9VxTqeR3VXmQ9455dNB9VBikR9SnR2gJM0HqQ6wjYPqNNKYUj7uVS5MLFFRHcZ1v6c6jveSRtL4xLPLNrJ7z/DCYsRbymZnn945UJ0dGXyDH1IdZZemjZafM/Zdqw7funwc+UkCBSjbB7/fbv1x1dnBBDhBunff2mHT+ds6/O6phzXKzrFdSv0GEFhtXnU8X/dt1SGW5+twRHGA0Qy+30X5rh+lwNfOftHpfTf5c5Td02+fOXFt3eVoVmG5na/7plJjNIa8ZpEk+5l9w/P3rTpUAds6drTCsIcfvFpWwvEZVDfrHIreKWcE+FZzhySUMj44rRj7l/fdw1JvbOukuinD661vH06wWavzOqxP8D2qQxRVV2+cSnWmuryI7j8/vnQY7xvUx/2ee4B/NdQPeswr9JE+ybh/inEdVPVNrSISThuXQ7uF733XdlGMbm+ig51T/Ra2A1/D7okJxKtlGTZZST0+H75cuO6e4EiB/e6rsmMi10je8Uoa/33ad0pvdytqciYxqc7Iy9H11rfaLeoxiY8fh4P+wPF53w4UTFmrYaTdAtXsoqXj6dH3PaApO2kcJfGuZoj1YMT1dGjN0iuYIOubZ1dXF1fG5dG6mX9zNBinnEcg3udIRuepbDbsumZN9LD3KwcXOIp9vd2i7urx+tbW5pax2bSG7vdUh+h645qCe/e9SlF265wNW/cSO6AJ9H73h/zhzVeAevBX42Grh42YdaLfkB1HfUmy26V7drLvWntOdalUN4OXq4aftt3wzaTCdsz9G1QXWi4T0hQaU1d+tYI5iZP1Fm/tod7evepqnKtTqptC1RWds00O1ai12A5n3b9XEP//qOpgEfO3sqU64FQX6xmxGdDcuCyejjkNtm/n2IMud58M94uAsSslBwfuJAzi6zt37/w83RT/tI5UN8MaozK/P25i5OYnE6N4nIr+EE6ybvgH0W1eD+nfZfOu0fOwS7Cp43A/uztq8nkvuwXYdOeU9EcwuQEsxEl9/bxP51KdVFfB7uXn6Y3h3THPn7hrDtbk/QDWM9ugL9k470J07/vk8BSpbgkIw53DLUbt8+3Eulg2VT8kOtfacWDX2L5GS2fHEj6j94xUt4i7+c2apCLr3RxwThP0sd84ffdNTK0QXnp0N6TjzE039t6R6hYpONeXVx1E0rs5arrjgR9VXZrW6+tHV/w1nhLO+YSYkOrmwcCLvaBXHVdH7c8XR7ubaa1W/xFqcXP7oHXbHro7TYDL6J0j1c1DUZg0KA7KBO+jfvv+9vry4vJHuLj8dPvYGzlXkLIOJgyp7vdBT5tlI7z+OEw1OzNMDfvFd45UJ8Ij1YnwSHUiPFKdCI9UJ8Ij1YnwTFWneYlFOHKoLo3iZE/PiIlgFGV3rRHFdfSwOoMpAlGUnQ9p1IgPei8TKAjxtuRlL46js5ProVQnQpGXw9OjaNAfFOphRSiKcT7oRSVv7VFbJ0KR51+KCO/u7h4hQsDfZ+a8xLP7cYR4a6C1PIvw/pPvcnXu/rBPJvAlmaadFQyf08U34ydl8Obl/CcH9ZOPo4x3bfuQnwKc2d3a/vWdvl9SvaSdrVqAW3gDmIfLywf8CExvrvybeA30rSWOJn5yFwtffAqP40V45vL37AUOLjMc1LjCcI2JUTgTBZy92cTl/F0rlhV5/YlqQGKUzx7J4SbQ05/w9l8Y1lMG1ZVY8kE/A5MLPrK8yKEjiuY73FNrpjhas/XljCMZ3nCMjZAss6fy3wKWF/nl/LklH/QD0Am3wgRMfwhyMWIBVHKZc1znvpo/C3ilbOAUmGt77OrbmG1pCbmGMvlnDtj+URBvN3G5L+cY374f17WVGrgFFBbFf6Py/pNjlR11egNXVz8Lq3x+uH6Sufz+3rTWxrUS7sVklC93JHo/lPA7tPtjQCN8HpF5+pA/jisxcE0eXlj2cWIe1Eq3H50eXwx+6rUJ1j30AvfcESao398B2Gl4sYW0D1MYly1ubO7eai/CLyVv6vZBfxykRWrbApaexX+r8v6TU4wHJ8dRo7b9c+85YROFmrdlfmB/OvX8Dks2XGVK+3zLPQjXLC7HkT+eiyul22z31TGXokIxfqr/JUrj3e7Pbev864X/72sfo7CJHfr8CXgozDae01ZQfwxyMWKeouzVa1Ha3Hv6yfXzT1jbxXD0586ceF48/ARn/yUpyk6tGa3xuQmpzsYBf77gLx6kutVwXuKEd7DrXmLKhbLzK+Lt4LzEnINdz004pLkQ6MlEER6pToRHqhPhkepEeKQ6ER6pToRHqhPhkepEeKQ6ER6pToRHqhPhkepEeKQ6EZ63Up09QiDESrzqeH+dD/kpUHKSnXiFaVu3+3Pvr5PqxDeA6jgvcbrzk+8lVg8rXodzsEN1yc9WnRCvU5Tdeho1kp/cw34HFLmE/l5px3HUiHfboc+cSHXvmLJTr0UnJ5d9vx4Me1DZPQzoQ8T7oX90wtl1Mr8aDGjNzWZjx7ucoMFHiP/6FFmnH+Xln5nf48dAhmzuuIC3LOcUXhLe+4A7fhJxx/+pCT5+hNyaOlvgDGKUnFT3PmAHV0Q4kgje1kFypjU3vjPNSXTvBez4LMKgzqZbC41Jzi9Lde8ITlAp1YmwcEZM/cqJCAzUFhU55/nzAUK8MRBbjqOJCbo3qU4Egj8iUkadTmck0YlQFEX+2I4Otk96uudEhKO7uxM1413d6STCUfKek1iqE0Fpx2lUTzRDrAgH7+rkHex6MlGEw93BrudhRUjc0zpSnQjJWz2FLcTrSHUiPFKdCI9UJ8Ij1YnwSHUiPFKdCI9UJ8Ij1YnwSHUiPFKdCI9UJ8Ij1YnwONXFP3s2bCG+wbStCz9Xp3i/5JPeWup6WD03IQJRlN1aih52vyvViXB06s0oaey3pToRjieqLtlXDyuCUZT9WhzF9Z0nza4jQlGMO2tr0eHReV9tnQhFMR4cHUTt3kAziYlgQGq9XjQux3nwqf/F+yXPx2XEaWLV1IlgFEUxjEYL8wML8cZAdXmUuZ9WEiIMpjpOE6vGTgQDYsujUqoTIYHYMvttnf8Xvzch3in8scKoLEsdTohgUGtl1O+Pxro2IUKBlq47iM7PPg11bUKEohgPzlrRZrzf9QFCvD1Fp5FECedg9wFCvDW8lzjhbNhtqU6Egs9N6MlEERY9DyvCI9WJ8Eh1IjxSnQiPVCfCI9WJ8Eh1IjxSnQiPVCfCI9WJ8Eh1IjxSnQiPVCfC41THO518gBBvzvROp13NXyeC4e7qbCQ7Up0ISKdej9Jkt+NXhXhzirJTi6Mk2ZPqRDDcHOx1zsHuQ4R4a3g00bTf1vEBQrw5aOuguiTe1e9NiGBQdXHU1K+ciIBAdUk9Ot5v9SU6EY7+wW7U7fQ0GbYIR5F321GhORNFSCi4qNSETiI0VJ0aOxGWiB2tZogV4UAjFxWchN2vC/H2UHV4t38hQmBqwzEs+ld1sSIQ1rdGeZbpcEKEo8izUZSP8lwDOxEMNHOj6PT4YqBxnQgFf5X4OFpv6nf/RTiKspc2eAe7ZsMW4Sg79Zj31+nJRBGOsm1P6+yphxXhKDtxLNWJsJRdqU6Exqku1owTIiDWw/J5WB3DinC04zpn15HqRDCKsltPNJOYCIp+vU6ER7MmivBIdSI8Up0Ij1QnwiPVifBIdSI8Up0Ij1QnwiPVifBIdSI8Up0Ij1QnwiPVifA41XHmf6lOhAKqW0uhOj0PK8KRT7pQXdrY62jGCRGKouzVm9H9zUMm1YlQFMXw7iYqRpnmrxPBKMblaBBNNB22CIhpLaLmJDoRjDwflxGnTJTqRCiskYswpOPMxEIEgXor7Bed1NaJUHA2bM7BLtWJcEB19nsTQoQEbVyUjTQFuwhIUWRZdHfzMJLqRDiyzzfR/vZpT1f/RTDK/s5OlNR2NVenCEZRdtbSqFbfk+pEMKC6qBnVeC+xVCcCAdV9WI9qjX3NSywC0l5rRPVU4zoREJsNO052pDoRkE5cj5JUv+gkwqHZsEV4NBu2CI+ewhbhkepEeKQ6ER6pToRHqhPhkepEeKQ6ER6pToRHqhPhkepEeKQ6ER6pToRHqhPhcaqLpToREH+nk2bDFgEpym4tjRp1qU6Eoyg7tXq0mR739NyECEe32Yg+XT9mmg1bhKIos+tP0SjLNamTCEYxzod5VOBDqhPByDPOmsjZsKU6EYocfWxU5lku1YlQFEVeoIfN8lJT/4tQsIXLo4yTYUt1IhBuNuzShndCBKLIMjuG1dT/Ihj2QxNRVk6srxUiBPl4Mn6O2r3+SJcmRCjQtT51o53mUVdHEyIURfG0tRE1kn1d/RfBKMpevBYlyV5bbZ0IRVF26mlUT6E6IQLBuzobUdzY7kz+UFu3ZPx62t/3Om/xXQfS32PzOtPUdvT+Df5cLgvQ1bfczYrkP18B0c7id+z+IKu8zYW9lpmF+8hv1aS3W7Dgr5w0ozjdWVSdM8X7gvGUIrcDXu43O8vMf4t4gYG+LPSy0hUtFiK44NxVcbbeeqWNTz31sMrCHJgTH1u18Qlp6sy95QsWZUl9jBkxdPm2HTOz9C6N+atgVrSzSP+xgDn3ae1zpc30nZ5WGDCO2XhL4oxdqnls1XlhbL7KHSP4x0/GrrBxHszUW77QWUNbx3HdjKkXamvV+RTEZD6+yPDBsGU7u4kFfuzfyr1cKsYwajzOpgG0oNNqps4X8+Ku5ceyO2bpYnIsrL6HxkUwrU++OicWBMWwjGwrF2BKXzM+xvJyuS7m6fKBFZ3QHd8q5EWJlEhrfir5AStGYZfKUWL4qNpYVn67zUs1I/PPG9qw4OAmIHc6XcAMEJNn5gdZrigT88hL84dYl6uP81jcuESBizKzwr8YdGsJVLc7dzRBa74RLi5D/3mWWRTKQ8dz/hwMMAdmbP58zBQzQPgsxgKYbXXfTAsMb6gJ+FvYAo+lZQQdrio3A121OwerROAi6Mh5ybPlgvukiOO7C6JPfAEZNw8saGj5TfFRL3CfWXou+oUlEIhSIIL33iIj1oGP8lgJEMsPrELGTLUA/butmiXGglteMrVdwsLCn10pZe4u5gVEIs4KRLuZrxkMsjC++JVBobyFe1qnVt/tP8/SQFGu+eHVMjq1lTnyERq7kobOK5Z8zAykc5WAAnMb3NsirrAz//QEBeO1/N2zHEYsuDkrXZ34uBlwN0KBXByzW7aAAdJmiEHJkI+zWoQ5oVgwxCKqdcLyLGB7jmlH00vXtqkl9vSyGLhtVgnFhALG/7IzkBcTaobbhiqlQcUG+bHU3CklNh5Nx7If5GK1Zsn546tVNyjFCNU7GbvmgrjNX7IjZsFYlt82uGLFMmcThiMzbDy/DD7Kwwpm64TGlH5QObZ3CJ9MTKNGc3/uV05YTfxzFcbESzBD+IEBc0OZqINFWFhUEZ24jasoiUHw8uKfistG8G3f1AVoM+F+RYaIQ4aVeocR+gPuEvMIL9NNnIF17hDsPhaO8qsWihGws3C765AhC5gFE84yoF9TxLKt1TV6GH470cVYjfqoF9jBWj7cebZZSxkiAFuNwIxfc1upqNfVmgW6isfbkgkCEMVLULPErjRLdsQ8MZZJ+I6qcjEzIP3xiKHsZK1KliuTuwF1Al/WUcN+9q2E6ngMG889mYjULjOTDnHhL2CzbGDHjKADZuBjpjCA0dPNX/H15CrDXFYWQKkwmcUtwLAXRaFcy80KQIjVDvJ0X/VlC6a08qBgNJzQ2QJYp0iYmgrBG619pAch2HhrunwM3FBw1aYMqxQUGmlb5CYw/SJUnfkwvaN8S06AZWWKxUe2yoTN/PQrwqK7pQX8xkxYjlmQX1iC24M3xJrwbLOq7uCQO4//VrKVJiwal6xWuWjkk+6HNKrX93vPjglfrEQmYO7Pv3yxwDkmz1/KZzQ+8FJCx5NJ+WXZAl/byTMaYKdlKgbrC9gq3hDOvMxszKCy9JFzsBVAp843OEWRJpNlExjx+87njlCsSTlBsZdtJs+/oEDsymBUPhfIaQF4ZRokZpnZGFbLzQKySuBlVmwrMqphyZTOuNPYq03G5bOVqOIO/rhbXEtJT1UT1iaLjL2SjZ8LOvJRDrpFiO03971h9S9i9TH58gWZTMvt/Cwb0p1JylQDW4ZU3SGzEk2naQ6lrjqCSr5YY4eczM/8fu0mjShNN8+uZlzftKddX97+5APnueT/px6LZXXxdHWNoAUuEXB9dfUw5BbyW9a/9TFzXNLq8voeboy8d8OQFVze9q0S8C3M7q+uPsF1hcuruyGMuGuK/t0qC8vwyb68+B8PKqV2XF/d9p0bDIUeVji6ZNgFjHyxR49wzE1x0XNcdlHn2H/Q+P319coyXV3d9C0rZDn4vMIHuLi8fsTOZZHy8XCVGwu77Tgb9GmPq4qN3XR5eWP9ooFKeqUGHvHlZV7jontrvit2cPbgjmDx9eys3CGwubRdYpU5urc6cly36glUV19bqxlrax/Wmi2rULz6rcbaXz74qBlr4EPzBvGmztHHWeopXIdR/bjNRpXfm4fNWsXmAzyvfahtWxWA7LbBoFo1ww9bt2jOMSYry84xcoPZsrfah/ruozWuqIzPO7GVYREk+0v9nMMj1Na4+Ly+oth0vrZxwy8TTIreERz5WAfWkBc2bvPGNeTjon3EEjH1kunah6uR6zeK4jCGYwa6uCmopI07Dtewn7MHK/aSBd2sJUc4FGBe4+KugfWKlw+okvWrIbpf1lLvLK2YfPjLWh1vm21zBCa7SFcpEFhLPvKLwGJnnzbpvWrFncuxGr9Tg4uUFhWTv3yo7T2Y5tA4dQ4+zJS0Vk8+1KM4aaRpw0gbzXTjfMhSQTD983WE+6gFahufTU0cKl2kiQ+dkcJfmqQnHW4ivqb57fayG+YIo7S5z8xIdofckHJalhfS3Vv7imILuqdpus4gFzOFq3tP9sVCl/awZ86XSJJm2jwfMSv2jvebScWEpMk2NI6qQq0OD6t+UGQUM9669aobd07itOmi7P2F+DbPkB9qsjxmZLWiELr9wP3Ckj/uNprNan78P+HwASYo9opdgr3WbCRbn0bMCuXutTYqXppNbG4z2X4yE1AeNbEhlewQsn7WQ4H4vcuvNrFpK/YIws4wRmQrVoyumnUG+JgZzUa8/whH+MvHvcO4wZIbsI2jZn1j9+DQs79/+tn2DKwHdx8P9mcxc+zvt55wyI/j4qIc3R7t+eApB4dHR7A5/jSg6jDWyHotBC6DkIPDkwtmRkZPZ7Q5qlgeHLbaBQ6USwzNBzeW2ypnVz3IG63dpGhfHB9Wy30ATj/jaNKGWeP22QoT5n7w8dGdpSjL0c3xclb0cnC4f9h6cqXGd/Mame0fHiF4gYODo7sRv+gc2V0jYP+YOSyC3HrUCpqoonN5tKq2EXRyO0KRJxiw5U8f91eVG68WBjSs7/F4eHtayciMDg4uus4EXGAnVa1gdHyPY12Oa8v8sbVaANiW2+zZWhQMelCTFSNU5f7hNXYJKgDViUram9kc7Ca1/wvFwEusTD5E0QAAAABJRU5ErkJggg==';
    }
}

function aeviIsUndefinedOrNull(val) {
	return _.isUndefined(val) || _.isNull(val);
}

function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    document.body.appendChild(outer);
    
    var widthNoScroll = outer.offsetWidth;
    outer.style.overflow = "scroll";
    
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);
    
    var widthWithScroll = inner.offsetWidth;
    
    outer.parentNode.removeChild(outer);
        
    return widthNoScroll - widthWithScroll;
}

interface String {
    makeClass(): string;
    makeClassFromSelector(): string;
    replaceAll(find, replace): string;
}

String.prototype.makeClassFromSelector = function() {
    return this.replace('.', '').replace('#', '').replace(/ /g, '');
};

String.prototype.makeClass = function() {
    return this.replace('.', '').replace('#', '').replace(/ /g, '');
};

String.prototype.replaceAll = function (find, replace) {
    var str = this;
    return str.replace(new RegExp(find, 'g'), replace);
};

function aeviGetNumberWithDecimalPlaceCount(num: number, decimalPlaceCount: number) {
    return (Math.round(num * 100) / 100).toFixed(decimalPlaceCount);
}

function isCellInViewport(cellEl, bodyEl) {
    var cell = getOffset(cellEl);
    var body = getOffset(bodyEl);

    var isInViewportVertically = (cell.top >= body.top && cell.bottom <= body.bottom) ? true : false;
    if (!isInViewportVertically)
        return false;

    var isInViewportHorizontally = (cell.left >= body.left && cell.right <= body.right) ? true : false;
    if (isInViewportHorizontally)
        return true;

    return false;
}

function getOffset(element) {
    return element.getBoundingClientRect();
}

function findWithAttr(array, attr, value) {
    for (var i = 0; i < array.length; i += 1) {
        if (array[i][attr] === value) {
            return i;
        }
    }
}

interface Array<T> {
    aeviClean(deleteValue): any[];
    getClosestIndex(target): number;
    getClosest(target): any;
}

Array.prototype.aeviClean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};

Array.prototype.getClosest = function(target) {
    var tuples: any = _.map(this, function(val: any) {
        return [val, Math.abs(val - target)];
    });

    return _.reduce(tuples, function(memo: any, val) {
        return (memo[1] < val[1]) ? memo : val;
    }, [-1, 999])[0];
};

Array.prototype.getClosestIndex = function(target) {
    var i = 0, j = this.length - 1, k;

    while (i <= j) {
        k = Math.floor((i + j) / 2);
        if (target === this[k] || Math.abs(i - j) <= 1) {
            return k;
        } else if (target < this[k]) {
            j = k - 1;
        } else {
            i = k + 1;
        }
    }
    return -1;
};

function removeArrayItem(arr, index) {
    var what, a = arguments, L = a.length, ax;

    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}


function aeviFormatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
}


interface HTMLElement {
    setAttributes(attrs: any): void;
}

HTMLElement.prototype.setAttributes = function(attrs) {
    for (var key in attrs) {
        if(!_.isNull(attrs[key]))
            this.setAttribute(key, attrs[key]);
    }
}

// jQuery plugin pro vypocet sirky textu
$.fn.textWidth = function(text, font) {
    if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
    $.fn.textWidth.fakeEl.text(text || this.val() || this.text()).css('font', font || this.css('font'));
    return $.fn.textWidth.fakeEl.width();
};

function setSelectionRange(input, selectionStart, selectionEnd) {
    if (input.setSelectionRange) {
        input.focus();
        input.setSelectionRange(selectionStart, selectionEnd);
    }
    else if (input.createTextRange) {
        var range = input.createTextRange();
        range.collapse(true);
        range.moveEnd('character', selectionEnd);
        range.moveStart('character', selectionStart);
        range.select();
    }
}
