/// <reference path='../references.ts' />

class AeviBubbless {
	AeviGrid: any;
	isBubbleRendered: boolean;
	bubblePadding: number;
	bubbleSpaceBetweenCell: number;
	bubbleElement: JQuery;
	bubbleSpan: HTMLSpanElement;
    random: number;
    destroyBubbleIfIsRendered: boolean;
    bubbleWrapper: JQuery;

	constructor(aeviGrid: any, destroyBubbleIfIsRendered?: boolean) {
		this.AeviGrid = aeviGrid;
		this.isBubbleRendered = false;
		this.bubblePadding = 15 * 2;
		this.bubbleSpaceBetweenCell = 7;
        this.random = Math.random() * 100;
        this.random = parseInt(this.random.toString());

        this.destroyBubbleIfIsRendered =  (aeviIsUndefinedOrNull(destroyBubbleIfIsRendered)) ? true : destroyBubbleIfIsRendered;
	}

	render(bubbleWrapper: JQuery): void {
        this.bubbleWrapper = bubbleWrapper;
		var HTML: string = '<span id="aeviBubble-' + this.random + '" class="aeviBubble"><span class="aeviBubble__inner"><span></span></span></span>';
		bubbleWrapper.append(HTML);
        this.setElements();
        this.isBubbleRendered = true;
	}

    setElements() {
        this.bubbleElement = $('#aeviBubble-' + this.random);
        this.bubbleSpan = this.bubbleElement.find('.aeviBubble__inner span')[0];
    }

    setText(text) {
        this.bubbleSpan.innerHTML = text;
    }

	/**
	 * Show description
	 */
	show(bubbleWrapper: JQuery, text: string) {
        if (_.isNull(text)) {
            this.destroy();
            return;
        }

        if (this.isBubbleRendered && this.destroyBubbleIfIsRendered) {
            this.destroy();
            this.render(bubbleWrapper);
        }

        this.setText(text);
        this.setStyle();

        this.bubbleElement.addClass('aeviBubble--show');
	}

    setStyle() {
        this.bubbleSpan.style.maxWidth = (this.bubbleWrapper.outerWidth() - this.bubblePadding).toString();

        var bubblePosition: number = -1 * this.bubbleElement.outerHeight() - this.bubbleSpaceBetweenCell;

        if (!this.isInViewport(this.bubbleElement)) {
            this.bubbleElement.addClass('bottom');
            this.bubbleElement.css('bottom', bubblePosition);
        } else {
            this.bubbleElement.css('top', bubblePosition);
        }

        if (this.isBubbleWiderThanCell(this.bubbleWrapper)) {
            this.bubbleElement.addClass('wider');
        }
    }

    hide() {
        this.bubbleElement.removeClass('aeviBubble--show');
    }

	/**
	 * Destroying AeviBubbleElement with detach() method
	 */
	destroy() {
		if (!aeviIsUndefinedOrNull(this.bubbleElement)) {
			this.bubbleElement.detach();
        }
	}

	/**
	 * Destroying AeviBubbleElement with remove() method
	 */
	delete() {
		if (!aeviIsUndefinedOrNull(this.bubbleElement)) {
			this.bubbleElement.remove();
		}
	}

	isBubbleWiderThanCell(bubbleWrapper: JQuery): boolean {
		var cellWidth = bubbleWrapper.outerWidth(true);
		var bubbleWidth = this.bubbleElement.find('span').outerWidth(true);

		return bubbleWidth > cellWidth;
	}

	isInViewport(bubble: JQuery): boolean {
		var appEl = $('#' + this.AeviGrid.tableId);

		var saveVerticalZone = bubble[0].offsetHeight + 10;

		var elOffset = bubble.offset();
		var elOffsetTop = elOffset.top - saveVerticalZone;
		var elOffsetBottom = elOffsetTop + bubble[0].offsetHeight;
		var elOffsetLeft = elOffset.left;
		var elOffsetRight = elOffsetLeft + bubble[0].offsetWidth;

		var aeviAppOffset = appEl.offset();
		var aeviAppOffsetTop = aeviAppOffset.top;
		var aeviAppOffsetBottom = aeviAppOffsetTop + appEl[0].offsetHeight;
		var aeviAppOffsetLeft = aeviAppOffset.left;
		var aeviAppOffsetRight = aeviAppOffsetLeft + appEl[0].offsetWidth;

		var isInViewportVertically = (elOffsetTop >= aeviAppOffsetTop && elOffsetBottom <= aeviAppOffsetBottom) ? true : false;

		return isInViewportVertically;
	}
}
