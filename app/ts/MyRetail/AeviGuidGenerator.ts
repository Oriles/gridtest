/// <reference path='../references.ts' />

class AeviGuidGenerator {
	constructor() {/**/}

	generate(): string {
		var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		});

		return guid;
	}

	generateOld(): string {
		return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
	}

	s4(): string {
		return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
	}
}
