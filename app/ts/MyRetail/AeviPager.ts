/// <reference path='../references.ts' />
/// <reference path='AeviHandlers/AeviPagerHandler.ts' />

class AeviPager {
	AeviGrid: any;
	AeviPagerHandler: any;
	aeviConsts: any;

	showScrollArrows: boolean;
	rowHeight: number;
	tollerance: number;
	numberOfNewVisibleRows: number;
	recordsLength: number;

	arrowSteps: any;

	aeviAppElement: JQuery;
	scrollContainer: JQuery;	

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.aeviConsts = this.AeviGrid.AeviConsts;

		this.showScrollArrows = true;

		this.aeviAppElement = $('#' + this.AeviGrid.tableId);

		this.rowHeight = this.aeviConsts.pager.rowHeight;
		this.tollerance = this.aeviConsts.pager.tollerance;
		this.numberOfNewVisibleRows = this.aeviConsts.pager.visibleRows;

		this.recordsLength = this.AeviGrid.AeviDataService.AeviDataRepository.getVisibleRecordsLength();

		this.scrollContainer = this.renderScrollContainer(this.recordsLength * this.rowHeight);

		this.AeviPagerHandler = new AeviPagerHandler(this, this.AeviGrid);
		this.setTableHeadScrollable();
	}

	render() {
		this.rowHeight = this.aeviConsts.pager.rowHeight;
		this.tollerance = this.aeviConsts.pager.tollerance;
		this.numberOfNewVisibleRows = this.aeviConsts.pager.visibleRows;

		this.AeviPagerHandler.setInitialValues();

		var from = 0;
		var to = this.numberOfNewVisibleRows;
		var data = this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableData.getRecords(from, to);

		this.AeviGrid.AeviGridEditor.renderTableBody(data);
		this.AeviPagerHandler.setRange({ from: from, to: to });

		this.AeviPagerHandler.setJumps();

		if (this.showScrollArrows && data.length > 0)
			this.renderArrows();
	}

	refresh() {
		this.AeviGrid.AeviGridEditor.deleteTableBody();
		this.render();
	}

	renderScrollContainer(height) {
		var width = 'auto';

		if (height === 0 || height === this.rowHeight)
			width = this.aeviAppElement.find('table').css('width');

		var heightInPx = height + 'px';

		var html = '<div id="aeviScrollDiv" class="aeviScrollDiv" style="width: ' + width + '; height: ' + heightInPx + '"></div>';

		this.aeviAppElement.find(this.aeviConsts.tableBodySelector).append(html);

		return this.aeviAppElement.find('.aeviScrollDiv');
	}

	refreshScrollContainer() {
		this.scrollContainer.css({
			'width': this.aeviAppElement.find('table').css('width'),
			'height': this.recordsLength * this.rowHeight
		});
	}

	setScrollContainerHeight(height) {
		this.scrollContainer[0].style.height = height + 'px';
	}

	setTableHeadScrollable() {
		$(this.aeviConsts.tableBodySelector).scroll((event) => {
			var scroll = <HTMLElement>event.currentTarget;
			var bounding = scroll.getBoundingClientRect();
			var scrollDiff = bounding.left;
			var position: number = -1 * scroll.scrollLeft + scrollDiff;			

			$(this.aeviConsts.tableHeadSelector).offset({
				left: position,
				top: null
			});
		});
	}

	renderArrows() {
		var aeviWrapper = $(this.aeviConsts.aeviWrapperSelector);
		var arrowClass = this.aeviConsts.arrowSelector.makeClassFromSelector();

        if(this.AeviGrid.AeviDOM.length(this.aeviConsts.arrowSelector))
			return;

		aeviWrapper.append(
			//'<i class="' + arrowClass + ' top"></i>' +
			'<i class="' + arrowClass + ' right"></i>' +
			//'<i class="' + arrowClass + ' bottom"></i>' +
			'<i class="' + arrowClass + ' left"></i>'
		);

		this.setArrowPositions();
	}

	setArrowPositions() {
		var arrowOffset = -(this.aeviConsts.arrowSize) + 'px';

		if (window.innerWidth < 800)
			arrowOffset = -50 + 'px';

		if (window.innerWidth < 600)
			arrowOffset = -40 + 'px';

		$(this.aeviConsts.arrowSelector + '.top').css({
			'bottom': arrowOffset,
			'right': '46%',
			'left': 'auto',
		});

		$(this.aeviConsts.arrowSelector + '.right').css({
			'top': '52%',
			'right': arrowOffset,
			'left': 'auto',
		});

		$(this.aeviConsts.arrowSelector + '.bottom').css({
			'bottom': arrowOffset,
			'right': 'auto',
			'left': '46%',
		});

		$(this.aeviConsts.arrowSelector + '.left').css({
			'top': '52%',
			'right': 'auto',
			'left': arrowOffset,
		});
	}

    moveTo(props: IAeviMoveToProps) {
        var rowIndex: number = (_.isUndefined(props.rowId)) ? null : props.rowId;
        rowIndex = (_.isUndefined(props.guid) ? rowIndex : this.AeviGrid.AeviDataService.AeviDataRepository.getRecord(props.guid)[0][0]);
        var target: HTMLElement|JQuery = (_.isUndefined(props.cellId)) ? document.getElementById('row-' + rowIndex) : document.getElementById('cell-' + rowIndex + '.' + props.cellId);

		var currentRange = this.AeviPagerHandler.getRange();

		if (rowIndex !== 0 && (rowIndex >= currentRange.from && rowIndex <= currentRange.to)) {
			this.AeviGrid.AeviGridEditor.refreshData({ From: currentRange.from, To: currentRange.to });
		} else {
			var closestIndex = this.AeviPagerHandler.jumps.getClosestIndex(rowIndex);
			var fromIndex = (closestIndex === 0) ? closestIndex : closestIndex - 2;
			var from = this.AeviPagerHandler.jumps[fromIndex];
			var to = this.AeviPagerHandler.jumps[fromIndex + 4];
			to = (_.isUndefined(to)) ? this.AeviPagerHandler.jumps[this.AeviPagerHandler.jumps.length - 1] : to;
			this.AeviPagerHandler.pageData(from, to);
		}

		if (_.isNull(target))
			target = (_.isUndefined(props.cellId)) ? document.getElementById('row-' + rowIndex) : document.getElementById('cell-' + rowIndex + '.' + props.cellId);

		target = $(target);

		if (props.click) {
			this.AeviGrid.AeviGridHandler.cellChange(target);
		} else {
			if (props.scrollLeft) {
                var offsetLeft = parseInt(target[0].offsetLeft);

				this.aeviAppElement.find(this.aeviConsts.tableBodySelector).animate({
					scrollLeft: offsetLeft
				}, 0);
			} else {
				var tableTop = this.AeviPagerHandler.tableTop;

				if (_.isUndefined(tableTop))
					tableTop = 0;

				var numTableTop: number = parseInt(tableTop);
				var numOffsetTop: number = parseInt(target[0].offsetTop);

				var offsetTop = numTableTop + numOffsetTop - 50;

                this.aeviAppElement.find(this.aeviConsts.tableBodySelector).animate({
					scrollTop: offsetTop
				}, 0);
			}
		}
	}

    getVisibleCellIndexes(): any {
        var table = document.getElementById(this.AeviGrid.tableId);
        var rows = table.querySelectorAll('.aeviRow.render, .aeviRow.newrow');
        var rowIndexesInViewport: number[] = [];

        for (var i = 0; i < rows.length; i++) {
            var rowIndex = this.AeviGrid.AeviDOM.getRowIndex(rows[i]);

            if (AeviGlobal.isElementInBodyViewportVertically(rows[i], this.aeviAppElement[0])) {
                rowIndexesInViewport.push(rowIndex);
            }
        }

        var firstRowIndexInViewport = rowIndexesInViewport[0];
        var row = this.AeviGrid.AeviDOM.getPureRow(firstRowIndexInViewport);

        if(_.isNull(row))
            return null;

        var cells = row.querySelectorAll('td');
        var cellIndexesInViewport: number[] = [];

        for (var i = 0; i < cells.length; i++) {
            var cellIndex = this.AeviGrid.AeviDOM.getCellIndexes(cells[i]).cellIndex;

            if (AeviGlobal.isElementInBodyViewportHorizontally(cells[i], this.aeviAppElement[0])) {
                cellIndexesInViewport.push(cellIndex);
            }
        }

        var firstVisibleCellIndex: IAeviCellIndex = { rowIndex: rowIndexesInViewport[0], cellIndex: cellIndexesInViewport[0]};
        var lastVisibleCellIndex: IAeviCellIndex = { rowIndex: rowIndexesInViewport[rowIndexesInViewport.length - 1], cellIndex: cellIndexesInViewport[cellIndexesInViewport.length -1]};

        return {
            firstVisibleCellIndex: firstVisibleCellIndex,
            lastVisibleCellIndex: lastVisibleCellIndex,
            visibleRowsLength: rowIndexesInViewport.length,
            visibleColumnsLength: cellIndexesInViewport.length
        };
    }

	setVisibleContentInfo() {
		var visibleRowsLength = this.AeviGrid.AeviDataRepository.getVisibleRecordsLength();

        if(visibleRowsLength <= 0 && this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport())
            return;

        var visibleCells = this.getVisibleCellIndexes();
        if(_.isNull(visibleCells))
            return;

        var firstCellIndexes = visibleCells.firstVisibleCellIndex;
        var lastCellIndexes = visibleCells.lastVisibleCellIndex;
		var visibleCols = visibleCells.visibleColumnsLength;

        this.enableArrows(firstCellIndexes, lastCellIndexes);

		var nextHorizontalStep: IAeviCellIndex = _.clone(lastCellIndexes);
        nextHorizontalStep.cellIndex = nextHorizontalStep.cellIndex + 1;

        var prevHorizontalStep: IAeviCellIndex = _.clone(firstCellIndexes);
        prevHorizontalStep.cellIndex = prevHorizontalStep.cellIndex - visibleCols + 1;
        if(prevHorizontalStep.cellIndex < 0)
            prevHorizontalStep.cellIndex = 0;

        this.arrowSteps = {
            prevLeftStep: prevHorizontalStep,
            nextRightStep: nextHorizontalStep
			//nextBottomStep: this.AeviGrid.AeviDOM.getCellIndexes(nextBottomStep),
			//prevTopStep: this.AeviGrid.AeviDOM.getCellIndexes(prevTopStep),
		};
	}

	enableArrows(firstCellIndexes, lastCellIndexes) {
		if (lastCellIndexes.cellIndex === this.AeviGrid.AeviDataRepository.AeviTableDescription.getVisibleCellsLength() - 1)
			this.hideArrow('right');
		else
			this.showArrow('right');

		if (firstCellIndexes.cellIndex <= 1)
			this.hideArrow('left');
		else
			this.showArrow('left');
	}

	hideArrow(className: string): void {
		this.changeArrow(className, 'none');
	}

	showArrow(className: string): void {
		this.changeArrow(className, 'block');
	}

	changeArrow(className: string, action: string): void {
		var arrow = <HTMLElement>document.querySelector('.' + className);

		if (aeviIsUndefinedOrNull(arrow)) {
			this.renderArrows();
			this.setVisibleContentInfo();
			return;
		}

		arrow.style.display = action;
	}
}
