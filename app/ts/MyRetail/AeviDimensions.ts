/// <reference path='../references.ts' />

class AeviDimensions {
	AeviGrid: any;
	cons: any;
	fixedHeight: number;
	appWrapperScrollTop: number;
	privateDimensions: any;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.cons = this.AeviGrid.AeviConsts;
		this.privateDimensions = [];
		this.fixedHeight = null;
		this.appWrapperScrollTop = $(this.cons.aeviWrapperSelector).offset().top;
		this.getInitialDimensions();
	}

	getInitialDimensions() {
		var elements = [
			window,
			this.cons.aeviWrapperSelector,
			this.cons.statusBarSelector,
			this.cons.toolbarSelector,
			this.cons.outerContainerSelector,
			this.cons.innerContainerSelector,
			this.cons.tableHeadSelector,
			this.cons.tableBodySelector
		];

		this.setPrivateDimensions(elements);
		this.setDimensions();
	}

	setPrivateDimensions(elements) {
		for (var i = 0; i < elements.length; i++) {
			this.updatePrivateDimension(elements[i]);

			switch (elements[i]) {
				case this.cons.outerContainerSelector:
				case this.cons.tableBodySelector:
                    this.setDimension(AeviDimensions.getDimension(elements[i]));
					this.updatePrivateDimension(elements[i]);
					break;
				default:
					break;
			}
		}
	}

	updatePrivateDimension(selector: string) {
		var info = AeviDimensions.getDimension(selector);
		var key = (_.isObject(info.selector)) ? 'window' : info.selector.makeClassFromSelector().replace('-', '_');
		this.privateDimensions[key] = info;
	}

	setOuterContainerHeight(): void {
		var visibleRecordsLength = this.AeviGrid.AeviDataRepository.getVisibleAndNewRecordsLength();

		if (this.AeviGrid.mode.clipboard === 1) {
			visibleRecordsLength = this.AeviGrid.AeviDataRepository.AeviTableData.getRecordsLength();
		}

		var tableBodyRowsHeight = visibleRecordsLength * this.cons.rowHeight;
		var outerContainerHeight;
		var minimumRowsHeight = this.cons.minimumRows * this.cons.rowHeight;

        if (this.getMode() === 'Standard') {
			outerContainerHeight = AeviDimensions.getViewport().height;
			outerContainerHeight -= this.appWrapperScrollTop;
			outerContainerHeight -= this.cons.statusBarHeight;
			outerContainerHeight -= this.privateDimensions.aeviToolbar.height;
			outerContainerHeight -= this.cons.outerContainerBottomEmptySpace;

			var tableBodyHeight = outerContainerHeight - this.privateDimensions.table_head.height - this.cons.scrollHeight;

			if (AeviDimensions.isTableBodyHeightLowerThanMinimumRowsHeight(tableBodyHeight, minimumRowsHeight))
				outerContainerHeight = this.getMinimumOuterContainerHeight();

			if (this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport())
				outerContainerHeight = this.subtractSumElementHeight(outerContainerHeight);

		}
		/*
		else if(this.getMode() === 'Extended'){
			visibleRecordsLength = (visibleRecordsLength < this.fixedHeight) ? visibleRecordsLength : this.fixedHeight;
			tableBodyRowsHeight = visibleRecordsLength * this.cons.rowHeight;
			outerContainerHeight = tableBodyRowsHeight + this.privateDimensions.table_head.height;	
		}
		*/

		tableBodyRowsHeight = this.getEmptySpace(tableBodyRowsHeight);

		if (AeviDimensions.isRecordsHeightIsLowerThanHeight(tableBodyRowsHeight, outerContainerHeight)) {
			outerContainerHeight = tableBodyRowsHeight + this.cons.scrollHeight + this.privateDimensions.table_head.height;
		}

		this.updateDimension({ selector: this.cons.outerContainerSelector, height: outerContainerHeight });
	}

	getEmptySpace(tableBodyRowsHeight: number): number {
		if (this.AeviGrid.mode.clipboard === 1)
			tableBodyRowsHeight = this.subtractRow(tableBodyRowsHeight);

		tableBodyRowsHeight = this.setEmptyRowSpace(tableBodyRowsHeight);

		return tableBodyRowsHeight;
	}

	addRow(tableBodyRowsHeight: number): number {
		return tableBodyRowsHeight + this.cons.rowHeight;
	}

	subtractRow(tableBodyRowsHeight: number): number {
		return tableBodyRowsHeight - this.cons.rowHeight;
	}

	setEmptyRowSpace(tableBodyRowsHeight: number): number {
		return this.addRow(tableBodyRowsHeight);
	}

	subtractSumElementHeight(height: number): number {
        if(this.AeviGrid.AeviDOM.length('.aeviSum'))
			return height - ($('.aeviSum').outerHeight() + this.cons.sumContainerBottomEmptySpace);
		return height;
	}

	getMinimumOuterContainerHeight(): number {
		return (this.cons.minimumRows * this.cons.rowHeight) + this.cons.scrollHeight + this.privateDimensions.table_head.height;
	}

	static isTableBodyHeightLowerThanMinimumRowsHeight(tableBodyHeight: number, minimumRowsHeight: number): boolean {
		return (tableBodyHeight < minimumRowsHeight);
	}

	static isRecordsHeightIsLowerThanHeight(tableBodyRowsHeight: number, height: number): boolean {
        return  (tableBodyRowsHeight < height);
	}

	static getViewport(): any {
		var a: string;
		var e: any;

		if (!('innerWidth' in window)) {
			a = 'client';
			e = document.documentElement || document.body;
		}else {
			e = window;
			a = 'inner';
		}

		return {
			width: e[a + 'Width'],
			height: e[a + 'Height']
		};
	}

	setTableBodyHeight(): void {
		var height = AeviDimensions.getDimension(this.cons.outerContainerSelector).height - this.privateDimensions.table_head.height;
		this.updateDimension({ selector: this.cons.tableBodySelector, height: height });
	}

	setAppWrapperHeight(): void {
		var height = 0;

		height += this.cons.statusBarHeight;
		height += this.privateDimensions.aeviToolbar.height;
		height += this.privateDimensions.table_head.height;
		height += this.privateDimensions.table_body.height;

		this.updateDimension({ selector: this.cons.aeviWrapperSelector, height: height });
	}
	
	setDimensions(): void {
		this.setOuterContainerHeight();
		this.setTableBodyHeight();
		this.setAppWrapperHeight();

		if (!_.isUndefined(this.AeviGrid.AeviPager))
			this.AeviGrid.AeviPager.setScrollContainerHeight((this.AeviGrid.AeviDataService.AeviDataRepository.getVisibleRecordsLength()) * this.cons.rowHeight);
	}

	updateDimension(object: any): void {
		var key = AeviDimensions.getDimension(object.selector);
		key.height = object.height;

		if (!_.isUndefined(object.width))
			key.width = object.width;

		this.setDimension(key);
		this.updatePrivateDimension(object.selector);
	}

	setDimension(object: any): void {
		switch (object.selector) {
			case this.cons.outerContainerSelector:
				$(object.selector).css({
					'height': object.height,
					'top': parseInt(this.privateDimensions.aeviToolbar.height + this.cons.statusBarHeight)
				});
				break;
			default:
				$(object.selector).height(object.height);
				break;
		}
	}

	static getDimension(selector: string): any {
		var el = (_.isObject(selector)) ? $(selector) : $(selector);

		return {
			selector: selector,
			width: el.outerWidth(true),
			height: el.outerHeight(true)
		};
	}

	/**
	 * getMode This method is not used
	 * @method getMode
	 * return {string} Return mode
	 */
	getMode() {
		if (_.isNull(this.fixedHeight) || this.fixedHeight < 1)
			return 'Standard';
		return 'Extended';
	}	
}
