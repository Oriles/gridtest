/// <reference path='../references.ts' />

class AeviConsts {
	AeviGrid: any;
	tableId: string;
	putType: any;
	dataTypes: any;
	imageFormat: string;
	controlCommandKey: string;
	clipboard: any;
	tableSelector: string;
	aeviWrapperSelector: string;
	aeviTableId: string;
	tableHeadSelector: string;
	tableBodySelector: string;
	tableHeaderSelector: string;
	rowSelector: string;
	rowNumberSelector: string;
	renderRowSelector: string;
	cellSelector: string;
	renderCellSelector: string;
	columnSelector: string;
    sortButtonSelector: string;
	innerContainerSelector: string;
	outerContainerSelector: string;
	statusBarSelector: string;
	statusBarItemSelector: string;
	statusBarHeight: number;
	toolbarSelector: string;
	toolbar: any;
	aeviSelectAllSelector: string;
	arrowsWrapperSelector: string;
	arrowSelector: string;
	arrowSize: number;
	searchSelector: string;
	search: any;
	aeviStatusBarHelperSelector: string;
	aeviStatusBarInfoSelector: string;
	aeviStatusBarQuestSelector: string;
	aeviSumSelector: string;
	aeviSumColSelector: string;
	helperSelector: string;
	fakeInputSelector: string;
	firstColumnWidth: number;
	rowHeight: number;
	scrollHeight: number;
	minimumRows: number;
	outerContainerBottomEmptySpace: number;
	sumContainerBottomEmptySpace: number;
	statuses: any;
	pager: any;
	keys: any;
    aeviModalSelector: string;
    copyModalId: string;
    copyModalTextareaSelector: string;

    statesSelector: any;

    noSelectionClass: string;

    modalWindowSelectorId: string;
    contextMenuSelector: string;

    selectedCellClasses: string[];
    selectedCellClassesToRemove: string[];

    public static keyDownEvent = 'keydown';
    public static clickEvent = 'click';
    public static mouseUpEvent = 'mouseup';
    public static doubleClickEvent = 'dblclick';
    public static pasteEvent = 'paste';
    public static resizeEvent = 'resize';
    public static touchEvent = 'touch';

    public static imageModalUpload = ' .aeviImageModal__upload';
    public static imageModalFile = ' .aeviImageModal__file';
    public static imageModalEdit = ' .aeviImageModal__edit';
    public static imageModalDelete = ' .aeviImageModal__delete';
    public static imageModalSaveAndClose = ' .aeviImageModal__saveAndClose';
    public static imageModalCloseWithoutClose = ' .aeviImageModal__closeWithoutSave';
    public static imageModalClose = ' .aeviImageModal__close';
    public static imageModalChangeEvent = 'aeviImageEditorChanged';

    public static form = ' .aeviForm ';
    public static formImage = ' .aeviForm__image';
    public static formImageUploadButton = ' *[data-imageaction="upload"]';
    public static formImageDeleteButton = ' *[data-imageaction="delete"]';
    public static formImageEditButton = ' *[data-imageaction="edit"]';

    public static allEvents = 'blur change click dblclick keydown keypress keyup mousedown mouseup select';
    public static fileImportInput = ' #aeviImportFile ';

	constructor(aeviGrid: any, tableId: string) {
		this.AeviGrid = aeviGrid;
		this.tableId = tableId;
		this.setContants();
	}

	setContants() {

		this.putType = {
			insert: 0,
			update: 1
		};

        this.modalWindowSelectorId = ' #simplemodal-container ';

        this.contextMenuSelector = ' .aeviContextMenu ';

		this.dataTypes = {
			0 : 'Text',
			1 : 'Number',
			2 : 'IntegerNumber',
			3 : 'Currency',
			4 : 'DateTime',
			5 : 'ShortDate',
			6 : 'ShortTime',
			7 : 'Boolean',
			8 : 'Enum',
			9 : 'Hyperlink',
			10 : 'RegularExpression',
			11 : 'Image',
			98 : 'SelectedCellsArray',
			99 : 'InvalidCellsArray',
			100 : 'Status',
		};

		this.imageFormat = 'jpeg';

		this.fakeInputSelector = '#aeviFakeEditorInput';

		this.controlCommandKey = '_var.controlCommandKey';

		this.clipboard = {
			maximumCountOfInsertedRows : 1000
		};

		this.tableSelector = '#' + this.tableId + ' ';

		this.aeviWrapperSelector = ' .aeviWrapper ';

		this.aeviTableId = ' #aeviTable ';
		
		this.tableSelector = ' .aeviTable ';
		this.tableHeadSelector = ' .table-head ';
		this.tableBodySelector = ' .table-body ';

		this.tableHeaderSelector = ' #table-header ';

        this.selectedCellClasses = [
            'selected',
            'selectedTop',
            'selectedBottom',
            'selectedLeft',
            'selectedRight'
        ];

        this.selectedCellClassesToRemove = [
            'tinge',
            'selected',
            'selectedTop',
            'selectedBottom',
            'selectedLeft',
            'selectedRight'
        ];

		this.rowSelector = ' .aeviRow ';
		this.rowNumberSelector = ' .aeviRowNumber ';
		this.renderRowSelector = ' .aeviRow.render ';
		this.cellSelector = ' .aeviCell ';
		this.renderCellSelector = ' .aeviCell.visible, .aeviCell.sortedRow ';
		this.columnSelector = ' .aeviColumn ';

        this.sortButtonSelector = ' .aeviSort ';

		this.innerContainerSelector = ' .inner-container ';
		this.outerContainerSelector = ' .outer-container ';

		this.statusBarSelector = ' .aeviStatusBar ';
		this.statusBarItemSelector = ' .aeviStatusBarItem ';
		this.statusBarHeight = 31;

		this.toolbarSelector = ' .aeviToolbar ';

		this.toolbar = {
			selectors : {
				deleteRow : ' .aeviToolbar__delete_row ',
				insertRow : ' .aeviToolbar__insert_row ',
				paste : ' .aeviToolbar__paste '
			}
		};

        this.noSelectionClass = 'noselection';

		this.aeviSelectAllSelector = ' .aeviSelectAll ';

		this.arrowsWrapperSelector = ' .aeviArrows ';
		this.arrowSelector = ' .aeviArrow';
		this.arrowSize = 44 + 20;

		this.searchSelector = ' .aeviSearch ';

		this.search = {
			selector : {
				find : ' .aeviSearch__find ',
				cancel : ' .aeviSearch__cancel ',
				field : ' .aeviSearch__field ',
				radio : ' .aeviSearch__radio ',
				direction : ' .aeviSearch__radio[name="direction"]'
			}
		};

		this.aeviStatusBarHelperSelector = ' .aeviStatusBarHelper';
		this.aeviStatusBarInfoSelector = ' .aeviStatusBarInfo';
		this.aeviStatusBarQuestSelector = ' .aeviStatusBarQuest';

		this.aeviSumSelector = ' .aeviSum';
		this.aeviSumColSelector = ' .aeviSumCol';

		this.helperSelector = ' .aeviHelp';

        this.aeviModalSelector = ' .aeviModal ';

        this.copyModalId = '#' + this.tableId + '-aeviClipboardDialog';
        this.copyModalTextareaSelector = this.copyModalId + ' textarea';

		this.firstColumnWidth = 47;

		// change in css TOO!!
		this.rowHeight = 25;

		this.scrollHeight = getScrollbarWidth();
		this.minimumRows = 8;
		this.outerContainerBottomEmptySpace = 30;
		this.sumContainerBottomEmptySpace = 30;

		if(this.AeviGrid.AeviClientSide.isIe())
			this.scrollHeight = this.scrollHeight + 4;

		/**
		 * selected - The selected row
		 * render - Classic row
		 * newrow - The row is new and did not push to dataservice
		 * hidden - The row is not rendered
		 * invalid - Info about validity
		 * softInvalid - The soft validity information (its possible to push)
		 * sortedRow - The row which is calculating in sorting. Although has not render status
		 */
		this.statuses = {
			selected: 'selected',
			render: 'render',
			newrow: 'newrow',
			hidden: 'hidden',
			invalid: 'invalid',
			softInvalid: 'softInvalid',
			sortedRow: 'sortedRow'
		};

        this.statesSelector = {
            selected: ' .selected ',
            tinge: ' .tinge '

        };

		this.pager = {
			rowHeight : this.rowHeight,
			tollerance : 25,
			visibleRows : 100,
			countOfNewRows : 25
		};

		this.keys = {
			8 : 'backspace',
			9 : 'tab',
			13 : 'enter',
			16 : 'shift',
			17 : 'ctrl',
			18 : 'alt',
			19 : 'pause/break',
			20 : 'caps lock',
			27 : 'escape',
			32 : '(space)',
			33 : 'page up',
			34 : 'page down',
			35 : 'end',
			36 : 'home',
			37 : 'left arrow',
			38 : 'up arrow',
			39 : 'right arrow',
			40 : 'down arrow',
			45 : 'insert',
			46 : 'delete',
			48 : 0,
			49 : 1,
			50 : 2,
			51 : 3,
			52 : 4,
			53 : 5,
			54 : 6,
			55 : 7,
			56 : 8,
			57 : 9,
			65 : 'a',
			66 : 'b',
			67 : 'c',
			68 : 'd',
			69 : 'e',
			70 : 'f',
			71 : 'g',
			72 : 'h',
			73 : 'i',
			74 : 'j',
			75 : 'k',
			76 : 'l',
			77 : 'm',
			78 : 'n',
			79 : 'o',
			80 : 'p',
			81 : 'q',
			82 : 'r',
			83 : 's',
			84 : 't',
			85 : 'u',
			86 : 'v',
			87 : 'w',
			88 : 'x',
			89 : 'y',
			90 : 'z',
			91 : 'left window key',
			92 : 'right window key',
			93 : 'select key',
			96 : 'numpad 0',
			97 : 'numpad 1',
			98 : 'numpad 2',
			99 : 'numpad 3',
			100 : 'numpad 4',
			101 : 'numpad 5',
			102 : 'numpad 6',
			103 : 'numpad 7',
			104 : 'numpad 8',
			105 : 'numpad 9',
			106 : 'multiply',
			107 : 'add',
			109 : 'subtract',
			110 : 'decimal point',
			111 : 'divide',
			112 : 'f1',
			113 : 'f2',
			114 : 'f3',
			115 : 'f4',
			116 : 'f5',
			117 : 'f6',
			118 : 'f7',
			119 : 'f8',
			120 : 'f9',
			121 : 'f10',
			122 : 'f11',
			123 : 'f12',
			144 : 'num lock',
			145 : 'scroll lock',
			186 : 'semi-colon',
			187 : 'equal sign',
			188 : 'comma',
			189 : 'dash',
			190 : 'period',
			191 : 'forward slash',
			192 : 'grave accent',
			219 : 'open bracket',
			220 : 'back slash',
			221 : 'close braket',
			222 : 'single quote'
		};
	}
}

