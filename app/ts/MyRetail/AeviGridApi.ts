/// <reference path='../references.ts' />

class AeviGridApi {
	AeviGrid: any;
	AeviDataRepository: IAeviDataRepository;

    constructor(aeviGrid: any, aeviDataRepository?: any) {
    	this.AeviGrid = aeviGrid;
    	this.AeviDataRepository = aeviDataRepository;
    }

    getTableDescription() {
		return this.AeviDataRepository.AeviTableDescription.getDescription();
    }

	getColumnHeader(index: number) {
		return this.AeviDataRepository.AeviTableDescription.getColumnHeader(index);
	}

	getColumnHeaderIndexByName(name: string) {
		return this.AeviDataRepository.AeviTableDescription.getColumnIndexByName(name);
	}

	getTableData() {
		return this.AeviDataRepository.AeviTableData.getData();
	}

	getColumnDisplayTypes() {
		return this.AeviGrid.AeviConsts.dataTypes;
	}

	log(msg: string) {
		return this.AeviGrid.print(msg);
	}
    
	getCulture() {
		return this.AeviGrid.AeviLocalization.getCulture();
	}

	updateCellValue(rowIndex: number, cellIndex: number, value: any) {
		this.AeviDataRepository.AeviTableData.updateRecordCellValue(rowIndex, cellIndex, value);
	}

	refreshRow(rowIndex: number) {
		this.AeviGrid.AeviGridEditor.refreshRow(<IAeviRecordIdentification>{rowIndex: rowIndex });
	}

	getMode(): any {
		return this.AeviGrid.mode;
	}
}
