﻿/// <reference path='../references.ts' />

class AeviDate {
    culture: string;
    type: string;
    null: boolean;
    year: number;
    month: number;
    day: number;
    hours: number;
    minutes: number;
    seconds: number;
    
    constructor(culture: string, type: any, value?: string) {
        this.culture = culture;
        this.type = this._parseType(type);
        this.null = true;
        this.year = 0;
        this.month = 0;
        this.day = 0;
        this.hours = 0;
        this.minutes = 0;
        this.seconds = 0;

        this.parse(value);
    }

    parse(value: string) {
        if (value && value.length > 0) {
            if (value.indexOf('T') != -1)
                this.parseIsoString(value);
            else
                this.parseString(value);
        }
    }

    parseString(value: string) {
        if (value) {
            var parts = this._splitDateTime(value);
            if (parts.length) {
                var date = new Date();

                if (this.culture == 'cs') {
                    this.day = (parts[0] ? parts[0] : date.getDate());
                    this.month = (parts.length > 1 && parts[1] ? parts[1] : date.getMonth() + 1);
                }
                else {
                    if (parts.length > 1) {
                        this.day = (parts[1] ? parts[1] : date.getDate());
                        this.month = (parts[0] ? parts[0] : date.getMonth() + 1);
                    }
                    else {
                        this.day = (parts[0] ? parts[0] : date.getDate());
                    }
                }

                this.year = (parts.length > 2 && parts[2] ? parts[2] : date.getFullYear());

                if (parts.length > 3) {
                    this.hours = parts[3];
                    if (parts.length > 4) {
                        this.minutes = parts[4];
                        if (parts.length > 5)
                            this.seconds = parts[5];
                    }
                }

                this.null = false;
            }
        }
    }

    parseIsoString(value: string) {
        if (value && value.length > 10 && value[4] == '-' && value[7] == '-' && value[10] == 'T') {
            this.year = parseInt(value.slice(0, 4));
            this.month = parseInt(value.slice(5, 7));
            this.day = parseInt(value.slice(8, 10));
            this.null = false;
        }
        else
            this.null = true;
    }

    _parseType(type: string) {
        if (type) {
            if (type == 'ShortDate' || type == '5')
                return 'ShortDate';

            if (type == 'ShortTime' || type == '6')
                return 'ShortTime';
        }
        return 'DateTime';
    }

    _splitDateTime(value: string) {
        var result = [];
        var state = 1;
        var index;
        var separator = this._getDateSeparator();

        while (state > 0) {
            if (state == 1) {
                index = value.indexOf(separator);
                if (index != -1) {
                    if (!this._parseDateTimePart(value.substr(0, index), result))
                        break;

                    value = value.substr(index + 1);
                }
                else {
                    value = value.trim();
                    if (value.indexOf(':') != -1 || value.indexOf(' ') != -1) {
                        index = value.indexOf(' ');
                        if (index != -1) {
                            if (!this._parseDateTimePart(value.substr(0, index), result))
                                break;

                            value = value.substr(index + 1);
                        }
                        else {
                            while (result.length < 3)
                                result.push(0);
                        }
                        state = 2;
                    }
                    else {
                        this._parseDateTimePart(value, result);
                        state = 0;
                    }
                }

                if (result.length == 3 && state == 1)
                    break;
            }
            else {
                index = value.indexOf(':');
                if (index != -1) {
                    if (!this._parseDateTimePart(value.substr(0, index), result))
                        break;

                    value = value.substr(index + 1);

                    if (result.length == 6)
                        break;
                }
                else {
                    this._parseDateTimePart(value, result);
                    state = 0;
                }
            }
        }

        return (state ? [] : result);
    }

    _getDateSeparator(): string {
        return this.culture == 'cs' ? '.' : '/';
    }

    isNull(): boolean {
        return this.null;
    }

    getDateFormat(): string {
        return this.culture == 'cs' ? 'd.m.yy' : 'mm/dd/yy';
    }

    getIsoString(): string {
        if (this.null)
            return '';

        return this.year + '-' + ('0' + this.month).slice(-2) + '-' + ('0' + this.day).slice(-2) + 'T00:00:00';
    }

    getString(): string {
        return this.getShortString();
    }

    getShortString(): string {
        if (this.null)
            return '';

        return (this.culture == 'cs' ?
            this.day + '.' + this.month + '.' + this.year : 
            ('0' + this.month).slice(-2) + '/' + ('0' + this.day).slice(-2) + '/' + this.year
        );
    }

    _parseDateTimePart(str: string, result: any): boolean {
        if (str) {
            str = str.trim();
            if (str.length > 0) {
                var value = parseInt(str, 10);
                if (!isNaN(value) && value > 0 && ('0000' + value.toString()).slice(-str.length) == str) {
                    result.push(value);
                    return true;
                }
            }
        }
        return false;
    }
}
