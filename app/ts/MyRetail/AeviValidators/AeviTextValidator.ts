/// <reference path='../../references.ts' />
/// <reference path='IAeviValidator.ts' />
/// <reference path='IAeviValidationInfo.ts' />
/// <reference path='AeviValidator.ts' />
/// <reference path='AeviEANValidator.ts' />
/// <reference path='AeviServerGeneratedValidator.ts' />
/// <reference path='AeviMaxLengthValidator.ts' />

class AeviTextValidator extends AeviValidator implements IAeviValidator {
	
	constructor(aeviLocalization: any, column: any, value: any) {
        super(aeviLocalization, column, value);
    }

	validate(): IAeviValidationInfo {
		var validationInfo: IAeviValidationInfo = {
			hard : true,
			valid : true,
			message : null
		};

		var cellValue: any = this.getValue();

		if(this.column.Required && (_.isEmpty(cellValue) || cellValue === ' ' || _.isNull(cellValue) || cellValue === 'null')) {
		    validationInfo.message = this.AeviLocalization.translate('value_required');
			validationInfo.valid = false;
	    }

		if(this.column.SemanticType === 1 && validationInfo.valid) {
			var EANValidator = new AeviEANValidator(this.AeviLocalization, null, null);
			validationInfo = EANValidator.validate(cellValue);
	    }

	    if(this.column.MaxLength > 0 && validationInfo.valid) {
	    	var maxLengthValidator = new AeviMaxLengthValidator(this.AeviLocalization, null, null, this.column.MaxLength);
	    	validationInfo = maxLengthValidator.validate(cellValue);
	    }

        if(this.column.ServerGenerated && validationInfo.valid) {
	    	var serverGeneratedValidator = new AeviServerGeneratedValidator(this.AeviLocalization, null, cellValue);
	    	validationInfo = serverGeneratedValidator.validate();
	    }

	    return validationInfo;
	}
}
