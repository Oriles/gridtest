/// <reference path='../../references.ts' />
/// <reference path='IAeviValidator.ts' />
/// <reference path='AeviValidator.ts' />
/// <reference path='AeviEANValidator.ts' />
/// <reference path='AeviServerGeneratedValidator.ts' />
/// <reference path='AeviMaxLengthValidator.ts' />

class AeviIntegerNumberValidator extends AeviValidator implements IAeviValidator {
	
	constructor(aeviLocalization: any, column: any, value: any) {
        super(aeviLocalization, column, value);
    }

	validate() {
		var validationInfo: any = {
			hard : true,
	    	valid : true,
	    	message : null
		};
		
		var cellValue: any = this.getValue();

        var numberRegex = /^\d+$/;

		if(_.isString(cellValue)) {
			if(cellValue.length > 0) {
				validationInfo.valid = numberRegex.test(cellValue);
			}
		}else {
			validationInfo.valid = numberRegex.test(cellValue);
		}

		if(validationInfo.valid === false) {
			validationInfo.message = this.AeviLocalization.translate('validate_integer_number_error');
		}

		if(_.isNaN(cellValue)) {
			return validationInfo;
		}

        if(this.column.Required && (_.isNull(cellValue) || cellValue === 'null' || cellValue === '')) {
            validationInfo.valid = false;
			validationInfo.message = this.AeviLocalization.translate('value_required');
		}

		if(this.column.Required === false && ((_.isEmpty(cellValue) || _.isNull(cellValue)) || cellValue === 'null')) {
			validationInfo.valid = true;
			validationInfo.message = '';
		}

		// EAN
		if(this.column.SemanticType === 1 && validationInfo.valid) {
			var EANValidator = new AeviEANValidator(this.AeviLocalization, null, null);
			validationInfo = EANValidator.validate(cellValue);
	    }

	    // MAX LENGTH
	    if(this.column.MaxLength > 0 && validationInfo.valid) {
	    	var maxLengthValidator = new AeviMaxLengthValidator(this.AeviLocalization, null, null, this.column.MaxLength);
	    	validationInfo = maxLengthValidator.validate(cellValue);
	    }

	    // SERVER GANERATED
	    if(this.column.ServerGenerated && validationInfo.valid) {
			var serverGeneratedValidator = new AeviServerGeneratedValidator(this.AeviLocalization, null, cellValue);
	    	validationInfo = serverGeneratedValidator.validate();	
	    }

        return validationInfo;
	};
}
