/// <reference path='../../references.ts' />
/// <reference path='IAeviValidator.ts' />

class AeviValidator implements IAeviValidator {
    AeviLocalization: any;
    column: IAeviColumnDescription;
    value: any;

    constructor(aeviLocalization: any, column: IAeviColumnDescription, value: any) {
        this.AeviLocalization = aeviLocalization;
        this.column = column;
        this.value = value;
    }

    getValue(): any {
        return this.value;
    }

    validate(...args: any[]): any {/**/}

    static fakeValidate(invalid?: boolean): IAeviValidationInfo {
        if (!aeviIsUndefinedOrNull(invalid)) {
            if (invalid) {
                return {
                    valid: false,
                    hard: true,
                    message: 'Error'
                };
            } else {
                return {
                    valid: true,
                    hard: true,
                    message: ''
                };
            }
        } else {
            return {
                valid: true,
                hard: true,
                message: ''
            };
        }
    }
}
