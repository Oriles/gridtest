/// <reference path='../../references.ts' />
/// <reference path='IAeviValidator.ts' />
/// <reference path='AeviValidator.ts' />

/**
 * AeviCurrencyValidator
 * @class  AeviCurrencyValidator
 */
class AeviMaxLengthValidator extends AeviValidator implements IAeviValidator {
	maxLength: number;

	constructor(aeviLocalization: any, column: any, value: any, maxLength: number) {
		super(aeviLocalization, column, value);		
		this.maxLength = maxLength;
	}

	validate(value: string): any {
		var valid,
			message;

		if(_.isNull(this.maxLength) || this.maxLength === 0) {
			valid = true;
			message = null;
		}else {
			if(_.isNull(value)) {
				valid = true;
				message = null;
			}else {
				value = value.toString();
				valid = (value.length <= this.maxLength) ? true : false;
				message = (valid) ? null : this.AeviLocalization.translate('error_validate_maxlength') + (this.maxLength + 1);
			}
		}

	    return {
	        hard : true,
	        valid : valid,
	        message : message
	    };
	}
}
