/// <reference path='../../references.ts' />
/// <reference path='IAeviValidator.ts' />
/// <reference path='AeviValidator.ts' />
/// <reference path='../AeviDate.ts' />

class AeviDateTimeValidator extends AeviValidator implements IAeviValidator {
    
    constructor(aeviLocalization: any, column: any, value: any) {
        super(aeviLocalization, column, value);
    }

    validate(): any {
        var validationInfo: any = {
            hard : true,
            valid : true,
            message : null
        };

        var cellValue: any = null;

        cellValue = this.getValue();
        
        if (cellValue && cellValue.length > 0 && cellValue !== 'null') {
            
            var date = new AeviDate(this.AeviLocalization.getCulture(), this.column.DisplayType, cellValue);

            if (date.null || date.month > 12 || date.day > 31) {
                validationInfo.message = this.AeviLocalization.translate('datetime_error');
                validationInfo.valid = false;
            }
        }
        else {
            if(this.column.Required) {
                validationInfo.message = this.AeviLocalization.translate('value_required');
                validationInfo.valid = false;
            }
        }

        return validationInfo;
    }
}
