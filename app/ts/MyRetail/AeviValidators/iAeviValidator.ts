/// <reference path='../../references.ts' />
/// <reference path='IAeviValidationInfo.ts' />

interface IAeviValidator {
    AeviLocalization: any;
    column: any;
	value: any;
    validate(...args: any[]): IAeviValidationInfo;
    getValue(): any;
}
