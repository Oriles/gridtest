/// <reference path='../../references.ts' />
/// <reference path='IAeviValidator.ts' />
/// <reference path='IAeviValidationInfo.ts' />
/// <reference path='AeviValidator.ts' />

class AeviEnumValidator extends AeviValidator implements IAeviValidator {
	
    constructor(aeviLocalization: any, column: any, value: any) {
        super(aeviLocalization, column, value);
    }

	validate(): IAeviValidationInfo {
		var validationInfo: IAeviValidationInfo = {
			hard : true,
	    	valid : true,
	    	message : null
		};

		var cellValue: any = this.getValue();

		if(this.column.Required) {
			if((cellValue === '' || _.isNull(cellValue) || cellValue === 'null')) {
				validationInfo.message = this.AeviLocalization.translate('value_required');
				validationInfo.valid = false;
			}
		}

		return validationInfo;
	}
}
