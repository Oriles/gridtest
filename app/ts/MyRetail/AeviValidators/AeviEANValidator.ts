/// <reference path='../../references.ts' />
/// <reference path='IAeviValidator.ts' />
/// <reference path='AeviValidator.ts' />

class AeviEANValidator extends AeviValidator implements IAeviValidator {
	
    constructor(aeviLocalization: any, column: any, value: any) {
        super(aeviLocalization, column, value);
    }

	validate(value: string): any {
		var result: any = this.getEANValidateResult(value);
		var msg: string = null;

	    if (!result)
			msg = this.AeviLocalization.translate('error_validate_ean');

		return {
	    	hard : false,
	    	valid : result,
	    	message : msg
	    };
	}

	getEANValidateResult(value: string) {
		if (value && value !== '') {
	        var numbersOnly: boolean = true;
	        var numberChars: string = "0123456789";

	        for (var i = 0; i < value.length; i++) {
	            if(numberChars.indexOf(value.charAt(i)) == -1)
	                numbersOnly = false;
	        }

	        if (numbersOnly && (value.length == 8 || value.length == 13)) {
	            // EAN
	            if(value.length == 8)
	                value = "00000" + value;

	            var originalCheck: string = value.substring(value.length - 1);
	            var eanCode: string = value.substring(0, value.length - 1);

	            // Add even numbers together
	            var even: number = Number(eanCode.charAt(1)) +
	                   Number(eanCode.charAt(3)) +
	                   Number(eanCode.charAt(5)) +
	                   Number(eanCode.charAt(7)) +
	                   Number(eanCode.charAt(9)) +
	                   Number(eanCode.charAt(11));
	            // Multiply this result by 3
	            even *= 3;

	            // Add odd numbers together
	            var odd: number = Number(eanCode.charAt(0)) +
	                  Number(eanCode.charAt(2)) +
	                  Number(eanCode.charAt(4)) +
	                  Number(eanCode.charAt(6)) +
	                  Number(eanCode.charAt(8)) +
	                  Number(eanCode.charAt(10));

	            // Add two totals together
	            var total: number = even + odd;

	            // Calculate the checksum
	            // Divide total by 10 and store the remainder
	            var checksum: number = total % 10;
	            
	            // If result is not 0 then take away 10
	            if(checksum !==  0 )
	                checksum = 10 - checksum;

	            var checksumString: string = checksum.toString();

	            return (checksumString === originalCheck);
	        } else {
	            return false;
	        }
	    }

	    return false;
	}
}
