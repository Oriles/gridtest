/// <reference path='../../references.ts' />
/// <reference path='IAeviValidator.ts' />
/// <reference path='AeviValidator.ts' />

class AeviRowNumberValidator extends AeviValidator implements IAeviValidator {
	
	constructor(aeviLocalization: any, column: any, value: any) {
        super(aeviLocalization, column, value);
    }

	validate(): any {
		return {
	        hard : true,
	        valid : true,
	        message : null
	    };
	}
}
