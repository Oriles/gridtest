/// <reference path='../../references.ts' />
/// <reference path='IAeviValidator.ts' />
/// <reference path='AeviValidator.ts' />
/// <reference path='AeviServerGeneratedValidator.ts' />
/// <reference path='AeviMaxLengthValidator.ts' />

class AeviCurrencyValidator extends AeviValidator implements IAeviValidator {
	
	constructor(aeviLocalization: any, column: any, value: any) {
        super(aeviLocalization, column, value);
    }

	validate(): any {
		var validationInfo: any = {
			hard : true,
			valid : true,
			message : null
		};

		var cellValue: any = this.value;

		var numberRegex = /^[+-]?[0-9]+([.][0-9]+)?$/;

		if(!_.isNull(cellValue) && cellValue.length > 0 && cellValue !== 'null') {
			if(cellValue.indexOf(',') !== -1)
				cellValue = cellValue.replace(',', '.');

			cellValue = parseFloat(cellValue);

			validationInfo.valid = numberRegex.test(cellValue);
		}

		if(validationInfo.valid === false) {
			validationInfo.message = this.AeviLocalization.translate('error_validate_number');
		}

		if(_.isNaN(cellValue)) {
			return validationInfo;
		}

		if(!_.isNull(cellValue))
			cellValue = cellValue.toString();

		if(cellValue === 'null')
			cellValue = null;

		if(this.column.Required && (_.isEmpty(cellValue) || _.isNull(cellValue))) {
			validationInfo.valid = false;
			validationInfo.message = this.AeviLocalization.translate('value_required');
		}

		// MAX LENGTH
	    if(this.column.MaxLength > 0 && validationInfo.valid) {
	    	var maxLengthValidator = new AeviMaxLengthValidator(this.AeviLocalization, null, null, this.column.MaxLength);
	    	validationInfo = maxLengthValidator.validate(cellValue);
	    }

	    // SERVER GANERATED
	    if(this.column.ServerGenerated && validationInfo.valid) {
			var serverGeneratedValidator = new AeviServerGeneratedValidator(this.AeviLocalization, null, cellValue);
	    	validationInfo = serverGeneratedValidator.validate();	
	    }

        return validationInfo;
	}
}
