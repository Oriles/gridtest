/// <reference path='../../references.ts' />
/// <reference path='IAeviValidator.ts' />
/// <reference path='AeviValidator.ts' />

class AeviServerGeneratedValidator extends AeviValidator implements IAeviValidator {
	
	constructor(aeviLocalization: any, column: any, value: any) {
        super(aeviLocalization, column, value);
    }

	validate() {
		var valid: boolean = true;
		var message: string = null;

        if(_.isNull(this.value) || this.value === 'null' || this.value.length < 1) {
	    	valid = false;
			message = this.AeviLocalization.translate('server_generated_message');
	    }

		return {
			hard : false,
			valid : valid,
			message : message
		};
	}
}
