/// <reference path='../../references.ts' />

interface IAeviValidationInfo {
	hard: boolean;
	valid: boolean;
	message: string;
}
