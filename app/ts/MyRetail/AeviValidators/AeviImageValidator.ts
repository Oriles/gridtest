/// <reference path='../../references.ts' />
/// <reference path='IAeviValidator.ts' />
/// <reference path='AeviValidator.ts' />

class AeviImageValidator extends AeviValidator implements IAeviValidator {
    
    constructor(aeviLocalization: any, column: any, value: any) {
        super(aeviLocalization, column, value);
    }    

    validate() {
        var validationInfo = {
            hard: true,
            valid: true,
            message: null
        };

        if (this.column.Required) {

            if (!this.value.HasValue) {
                validationInfo.message = this.AeviLocalization.translate('value_required');
                validationInfo.valid = false;
            }
        }

        return validationInfo;
    }
}
