/// <reference path='../../references.ts' />
/// <reference path='AeviValidator.ts' />
/// <reference path='AeviNumberValidator.ts' />
/// <reference path='AeviIntegerNumberValidator.ts' />
/// <reference path='AeviRowNumberValidator.ts' />
/// <reference path='AeviTextValidator.ts' />
/// <reference path='AeviCurrencyValidator.ts' />
/// <reference path='AeviServerGeneratedValidator.ts' />
/// <reference path='AeviDateTimeValidator.ts' />
/// <reference path='AeviMaxLengthValidator.ts' />
/// <reference path='AeviEANValidator.ts' />
/// <reference path='AeviEnumValidator.ts' />
/// <reference path='AeviImageValidator.ts' />

class AeviValidatorFactory {
	AeviConsts: any;
	AeviLocalization: any;
	AeviTableDescription: any;
	validator: any;
	
	constructor(aeviConsts: any, aeviLocalization: any, aeviTableDescription: any) {
		this.AeviConsts = aeviConsts;
		this.AeviLocalization = aeviLocalization;
		this.AeviTableDescription = aeviTableDescription;
	}

	createValidator(value: any, cellIndex: number): void {
		var column = this.AeviTableDescription.getColumnHeader(cellIndex);
        var columnType = this.AeviTableDescription.getColumnDisplayType(column);
        var displayType = this.AeviTableDescription.getColumnStringDisplayType(columnType);

        switch(displayType) {
			case 'Text':
			case 'SelectedCellsArray':
			case 'InvalidCellsArray':
			case 'Status':
				this.validator = new AeviTextValidator(this.AeviLocalization, column, value);
				break;
			
			case 'Number':
				this.validator = new AeviNumberValidator(this.AeviLocalization, column, value);
				break;
			
			case 'IntegerNumber':
				this.validator = new AeviIntegerNumberValidator(this.AeviLocalization, column, value);
				break;

			case 'Currency':
				this.validator = new AeviCurrencyValidator(this.AeviLocalization, column, value);
				break;

			case 'Enum':
                this.validator = new AeviEnumValidator(this.AeviLocalization, column, value);
                break;

			case 'DateTime':
			case 'ShortDate':
			case 'ShortTime':
				this.validator = new AeviDateTimeValidator(this.AeviLocalization, column, value);
				break;

			case 'Image':
				this.validator = new AeviImageValidator(this.AeviLocalization, column, value);
				break;

			default:
				this.validator = null;
				break;

			//case 'Boolean':
			//	return new AeviBooleanValidator(this.AeviLocalization, column, selectedCell);
			
			//case 'Enum':
			//	return new AeviEnumValidator(this.AeviLocalization, column, selectedCell);

			//case 'Hyperlink':
			//	return new AeviHyperlinkValidator(selectedCell);

			//case 'RegularExpression':
			//	return new AeviRegularExpressionValidator(selectedCell);

			//case 'Image':
			//	return new AeviImageValidator(this.AeviLocalization, column, selectedCell);
		}
	}

	getValidator(): any {
		return this.validator;
	}
}
