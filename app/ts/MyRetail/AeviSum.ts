/// <reference path='../references.ts' />

class AeviSum {
	AeviGrid: any;
	isSumRendered: boolean;
	sumsLen: number;
	blocks: any[];
	blocksLen: number;
	sums: any[];

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.isSumRendered = false;
	}

	init(sums) {
		this.sumsLen = sums.length;
		this.blocks = [];

		for (var i = 0; i < sums.length; i++)
			this.blocks.push(sums[i].BlockNumber);

		this.blocksLen = _.uniq(this.blocks).length;
		this.sums = sums;

		if (!this.isSumRendered)
			this.render();
		else
			this.refresh(sums);
	}

	render() {
		var cols = this.getCols();
		var sum;
		var sumItemTemplate = _.template('<p class=""><%= caption %>: <strong><%= value %> <%= iso %></strong></p>');
		this.sums = _.sortBy(this.sums, function(sum) { return sum.BlockNumber; });
		var sumHtml = [];

		for (var i = 0; i < cols.length; i++) {
			var colNumber = cols[i].colNumber;
			var block = ['<div class="aeviSumCol">'];

			for (var j = 0; j < this.sumsLen; j++) {
				sum = this.sums[j];

				var sumValue = sum.Value;

				if (!aeviIsUndefinedOrNull(sumValue) && !aeviIsUndefinedOrNull(sum.DecimalPlaceCount))
					sumValue = aeviGetNumberWithDecimalPlaceCount(sumValue, sum.DecimalPlaceCount);

				sumValue = this.AeviGrid.AeviClientSide.getNumberWithLocalizedDecimalPoint(sumValue);

				if (colNumber == sum.BlockNumber) {
					block.push(sumItemTemplate({
						caption: sum.Caption,
						value: sumValue,
						iso: sum.CurrencyISOCode
					}));
				}
			}

			block.push('</div>');

			var blockStringify: string = block.join('');

			sumHtml.push(blockStringify);
		}

		var sumTemplate: any = _.template('<div class="<%= aeviSumSelector %> cols-<%= colsLength %>"><%= col %></div>');
		
		sumTemplate = sumTemplate({
			aeviSumSelector: this.AeviGrid.AeviConsts.aeviSumSelector.makeClassFromSelector(),
			col: sumHtml.join(''),
			colsLength: this.blocksLen
		});

		$(sumTemplate).insertAfter('.aeviWrapper');

		this.isSumRendered = true;
	}

	refresh(sums) {
		$(this.AeviGrid.AeviConsts.aeviSumSelector).remove();
		this.isSumRendered = false;
		this.init(sums);
	}

	getCols() {
		var cols = [];
		var colTemplate = _.template('<div class="<%= aeviSumColSelector %>" data-blockNumber="<%= colNumber %>"></div>');
		var aeviSumColSelector = this.AeviGrid.AeviConsts.aeviSumColSelector.makeClassFromSelector();

		for (var i = 0; i < this.blocksLen; i++) {
			cols.push({
				colNumber: this.blocks[i],
				html: colTemplate({
					aeviSumColSelector: aeviSumColSelector,
					colNumber: this.blocks[i]
				})
			});
		}

		return cols;
	}
}
