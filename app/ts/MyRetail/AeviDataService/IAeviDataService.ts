interface IAeviDataService {
    AeviGrid: any;
    AeviApiService: IAeviApiService;
    AeviDataRepository: IAeviDataRepository;

    initDataEntity();
    initTableDescriptionAndTableData(blockMode: boolean);
    initFilters(responseFilters: any[]): any[];
    getDataByQuery(query: string, force: boolean);
    showData(type: string): void;
    getDataRow(guid: string);
    getImageData(guid: string, cellIndex: number): IAeviImageData;
    getData(): any[];
    commit(query: string, reload: boolean): JQueryDeferred<any>;

    deleteData(guids: string[], firstNotDeletedRecordIndex: number): void;
    putRow(rowIndex: number);
    putData(putData: any, async?: boolean): JQueryDeferred<any>;
    paste(data, cellIndex: IAeviCellIndex);
    pasteOneCell(data, cellIndex: IAeviCellIndex);
    executeAction(rowIndex: number, actionParameters: any): void;
    clearData(): void;
}