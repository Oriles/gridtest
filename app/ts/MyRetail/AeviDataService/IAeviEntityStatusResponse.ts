interface IAeviEntityStatusResponse {
    Status: MyRetail.IAeviResponseStatus;
    Message: string;
    ExtendedData: any;
}
