/// <reference path='IAeviSuccessResponse.ts' />

interface IAeviTableDescriptionSuccessResponse extends IAeviSuccessResponse {
    Columns: IAeviColumnDescription[];
    KeyColumnName: string;
    ErrorCodeColumnName: string;
    ErrorMessageColumnName: string;
    ReferenceSettingsColumnName: string;
    ReadOnly: boolean;
    FixedSize: number;
    MaxRowCount: number;
    AllowLoadDataImmediately: boolean;
    Filters: IAeviColumnDescription[];
    FixedColumnCount: number;
    AllowPasteFromExcel: boolean;
    SaveModeExcel: boolean;
    MaxRowCountExcel: number;
}
