
module MyRetail {
    export enum IAeviBlockStateCommand {
        /**
         * can continue in action
         */
        CONTINUE = 0,
        /**
         * need lock grid
         */
        LOCK = 1,
        /**
         * need reload data
         */
        RELOAD = 2
    }
}
