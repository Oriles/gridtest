/// <reference path='IAeviSuccessResponse.ts' />

interface IAeviEntitySuccessResponse extends IAeviSuccessResponse {
    ID: string;
    Caption: string;
    Description: string;
    Help: string;
    ImageClass: string;
    CategoryID: string;
    PrefferedSize: any;
    Form: boolean;
}
