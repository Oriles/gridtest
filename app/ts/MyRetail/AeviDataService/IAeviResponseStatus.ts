module MyRetail {
    export enum IAeviResponseStatus {
        OK = 0,
        Error = 1,
        Blocked = 2,
        Changed = 3
    }
}