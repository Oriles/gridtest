interface IImportResponse {
    Data: string;
    ErrorMessage: string;
}