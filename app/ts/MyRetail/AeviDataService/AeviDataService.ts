/// <reference path='../../references.ts' />

class AeviDataService implements IAeviDataService {
	AeviGrid: any;
    AeviUser: any;
	AeviApiService: any;
	AeviDataRepository: any;
	translates: any;

	isServerDataInvalid: boolean;
	invalidRecordsLength: number;

    stopInitDataGrid: boolean;

	constructor(aeviGrid: any, aeviApiService: IAeviApiService, aeviDataRepository: IAeviDataRepository) {
		this.AeviGrid = aeviGrid;
		this.AeviApiService = aeviApiService;
		this.AeviDataRepository = aeviDataRepository;
		this.translates = this.AeviGrid.AeviLocalization;

        if (aeviIsUndefinedOrNull(this.AeviApiService.token)) {
			this.AeviApiService.getAccessToken().done(() => {
                this.initDataEntity();
			}).fail((err) => {
                var message: string = null;

                if (!aeviIsUndefinedOrNull(err)) {
                    try {
                        var parsed = JSON.parse(err.responseText);
                        var responseText = parsed.error_description;
                    } catch(error) {
                        message = this.translates.translate('connection_fail');
                    }

                    if (!aeviIsUndefinedOrNull(responseText)) {
                        message = responseText;
                    }
                } else {
                    message = this.translates.translate('connection_fail');
                }

                toastr.error(message);
			});
		} else {
			this.initDataEntity();
        }
	}

    initDataEntity() {
        this.AeviUser = new AeviUser(this.AeviGrid.roles);

        if (!this.AeviUser.roles.Read) {
            var msg = this.translates.translate('roles_cannot_read');
            toastr.error(msg);
            this.AeviGrid.AeviStatusBar.error(msg);
            return;
        }

        this.AeviApiService.getDataEntity().done((response: IAeviEntitySuccessResponse) => {
            this.AeviDataRepository.initDataEntity(this, response);
            this.initTableDescriptionAndTableData();
        }).fail((err) => {
            var response = err.responseJSON;
            var message = this.translates.translate('connection_fail');

            if (!aeviIsUndefinedOrNull(response) && !aeviIsUndefinedOrNull(response.ExceptionMessage))
                message = response.ExceptionMessage;

            toastr.error(message);
            this.AeviGrid.AeviStatusBar.error(message);
            return;
        });
	}

    initTableDescriptionAndTableData() {
        if (!aeviIsUndefinedOrNull(this.AeviDataRepository.AeviDataEntity.data.Help)) {
            this.AeviGrid.AeviStatusBar.renderHelpIcon();
        }

        if (this.AeviDataRepository.AeviDataEntity.data.Form === true) {
            this.AeviApiService.getFormDescription().done((response: IAeviFormSuccessResponse) => {
                this.AeviDataRepository.initForm(response);
            });
        }

        this.AeviApiService.getTableDescription().done((response: IAeviTableDescription) => {
            this.AeviDataRepository.initTableDescription(response);
            this.AeviGrid.AeviGridEditor.renderTableHead();

            var filters = null;

            if (!_.isNull(response.Filters)) {
                this.initFilters(response.Filters);
                filters = this.AeviGrid.AeviFilter.getFilters();
            }

            this.AeviGrid.AeviToolbar = new AeviToolbar(this.AeviGrid, filters);
            this.AeviGrid.AeviToolbar.render();
            this.AeviGrid.setSizes();

            if (aeviIsUndefinedOrNull(this.AeviGrid.AeviPager)) {
                this.AeviGrid.AeviPager = new AeviPager(this.AeviGrid);
            }

            if (this.AeviDataRepository.AeviTableDescription.allowLoadDataImmediately) {
                if (this.AeviDataRepository.AeviTableDescription.isReport())
                    this.AeviGrid.AeviToolbar.AeviToolbarHandler.query();
                else {
                    this.getDataByQuery('', true);
                }
            } else {
                toastr.info(this.translates.translate('must_filter'));
            }
        }).fail((err: IAeviErrorResponse) => {
            var response = err.responseJSON;
            var message = this.translates.translate('connection_fail');

            if (!aeviIsUndefinedOrNull(response) && !aeviIsUndefinedOrNull(response.ExceptionMessage))
                message = response.ExceptionMessage;

            toastr.error(message);
            this.AeviGrid.AeviStatusBar.error(message);
        });
    }

	initFilters(responseFilters: any[]): any[] {
		var filters = null;

		if (!_.isNull(responseFilters)) {
			this.AeviGrid.AeviFilter = new AeviFilter(this.AeviGrid, responseFilters);
			filters = this.AeviGrid.AeviFilter.getFilters();
		}

		return filters;
	}

	getDataByQuery(query: string, force: boolean) {
		this.AeviGrid.AeviAjaxPreloader.show();
		this.AeviGrid.AeviStatusBar.clear();

		var promise = $.Deferred();

		return this.AeviApiService.getTableData(query, force).done((response) => {
			if (AeviDataService.hasConnectionProblem(response)) {
				toastr.error(response.Message || this.translates.translate('connection_fail'));
				this.AeviGrid.AeviStatusBar.error(response.Message || this.translates.translate('connection_fail'));
				this.AeviGrid.AeviAjaxPreloader.hide();
				return;
			}

			var renderType: string = 'render';

			if (this.AeviDataRepository.repositoryDataInit) {
				renderType = 'refresh';

				if (this.AeviDataRepository.AeviTableData.getRecordsLength() < response.Data.length)
					renderType = 're-render';
			}

			if (renderType !== 'render') {
				this.AeviGrid.AeviGridHandler.triggerClick();
			}

			this.AeviDataRepository.initTableData(response.Data.slice());

			this.AeviApiService.setDataChanged(false);
			this.AeviApiService.setCellChanged(false);

			if (!_.isNull(response.Sums))
				this.AeviGrid.AeviSum.init(response.Sums);

			this.AeviGrid.AeviPluginApi.callPluginsOnDataLoaded();
			this.showData(renderType);
            this.AeviGrid.AeviAjaxPreloader.hide();
			promise.resolve();

		}).fail(() => {
			toastr.error(this.translates.translate('connection_fail'));
		});
	}

	showData(type: string): void {
		this.AeviGrid.setSizes();

		switch (type) {
			case 'render':
				this.AeviGrid.AeviPager.render();
				this.AeviGrid.AeviPager.setVisibleContentInfo();
				break;

			case 're-render':
				this.AeviGrid.AeviPager.refresh();
				break;

			case 'refresh':
				this.AeviGrid.AeviPager.AeviPagerHandler.setJumps();
				this.AeviGrid.AeviGridEditor.refreshData();
				break;

			default:
				break;
		}

        if (this.AeviGrid.firstLoad === true) {
			this.AeviGrid.AeviGridLocker = new AeviGridLocker(this.AeviGrid);
			this.AeviGrid.AeviGridLocker.lock();
			this.AeviGrid.firstLoad = false;
		}

        if (this.AeviDataRepository.AeviTableData.getRecordsLength() > 0) {
            this.AeviGrid.selectFirstCell();
        }

		this.AeviGrid.AeviDOM.closeModals();
	}

	getDataRow(guid: string) {
        var result = null;
		this.AeviGrid.AeviAjaxPreloader.show();

		this.AeviApiService.getTableRow(guid).done((response) => {
			this.AeviGrid.AeviAjaxPreloader.hide();

            if (_.isNull(response)) {
                result = response;
            } else {
                result = response.Data;
            }
		});

        return result;
	}

    /**
     * return -startFakeColumnsLength because in raw data is not extra columns
     */
    getImageData(guid: string, cellIndex: number): IAeviImageData {
        var imageCellIndex: number = cellIndex - this.AeviDataRepository.AeviTableDescription.startFakeColumnsLength;
        var dataRow: any[] = this.getDataRow(guid);

        if(_.isNull(dataRow)) {
            this.AeviGrid.log('data with GUID: ' + guid + ' does not exists in TEMP TABLE. Data will be taken from repository.');
            return this.AeviDataRepository.AeviTableData.getRecord(guid)[cellIndex];
        } else {
            return dataRow[imageCellIndex];
        }
    }

	getData(): any[] {
		return this.AeviDataRepository.AeviTableData.getData();
	}

    commit(query: string, reload: boolean): JQueryDeferred<any> {
        var result = $.Deferred();

        if (this.AeviApiService.activeAjaxConnection > 0) {
			this.AeviGrid.AeviAjaxPreloader.show();

			setTimeout(() => {
				this.commit(query, reload);
			}, 500);

			return;
		}

		var message: string = '';
		var filter = (aeviIsUndefinedOrNull(query)) ? '' : query;

		if (this.isServerDataInvalid) {
			var modal = new AeviModal(this.AeviGrid);

			modal.setContent({
				title: this.AeviGrid.AeviLocalization.translate('warning'),
				text: '<p>' + this.AeviGrid.AeviLocalization.translate('server_invalid_records_message') + '</p>',
				buttons: [
					'aeviDataServiceCommitTempAndClose',
					'aeviDataServiceDeleteTempIgnoreAndClose',
					'aeviDataServiceClose'
				]
			});

			modal.show();
			modal.focus('first');
			return;
		}

		this.invalidRecordsLength = this.AeviDataRepository.AeviTableData.getInvalidRecordsIndexes().length;

		if (this.invalidRecordsLength > 0) {
			this.AeviGrid.AeviGridHandler.triggerClick();

            message = this.translates.translate('invalid_records');
            this.AeviGrid.AeviStatusBar.error(message, 'error');
            toastr.error(message);

			this.AeviGrid.AeviDataRepository.AeviTableData.sortInvalid();
			this.AeviGrid.AeviGridEditor.refreshData();

			this.AeviGrid.selectFirstCell();

			return;
		}

		this.AeviGrid.AeviAjaxPreloader.show();

		this.AeviApiService.commit().done((response: IAeviCommitResponse) => {
			if (AeviDataService.isServerDataValid(response)) {

				if (!aeviIsUndefinedOrNull(reload) && reload === true) {
					if (this.AeviGrid.mode.clipboard === 1) {
						this.AeviDataRepository.deleteRepositoryTableData();
						this.AeviGrid.mode.clipboard = 0;
						this.AeviGrid.AeviClipboard.disableOneCellPasting();
					}

                    this.getDataByQuery(filter, true);
                    this.AeviDataRepository.enableOrDisableImageColumns('enable');
					toastr.success(this.translates.translate('table_data_commit'));
				}

				this.isServerDataInvalid = false;

			} else if (!AeviDataService.isServerDataValid(response)) {
				var invalidRecordsLength = response.Data.length;

				if (invalidRecordsLength > 0) {
					this.AeviDataRepository.initTableData(response.Data);

					for (var i = 0; i < invalidRecordsLength; i++) {
						this.AeviDataRepository.AeviTableData.AeviStatus.updateByRowIndex(i, 'render serverInvalid');
					}

					this.showData('re-render');

					if (this.AeviGrid.AeviGridLocker.isLocked) {
						this.AeviGrid.AeviGridLocker.unLock();
						this.AeviGrid.AeviGridLocker.lock();
					} else {
						this.AeviGrid.AeviGridLocker.lock();
						this.AeviGrid.AeviGridLocker.unLock();
					}
				}

				toastr.error(this.translates.translate('commit_invalid_rows'));

				this.isServerDataInvalid = true;

				var firstCell = this.AeviGrid.AeviDOM.getCell(1, 0);
				this.AeviGrid.AeviGridHandler.cellChange(firstCell);

				if (response.Message)
					message = response.Message;
				else
					message = this.translates.translate('invalid_data_status');
			} else {
				if (response && response.Message) {
					message = response.Message;
					toastr.error(message);
				}
			}

            this.AeviGrid.AeviAjaxPreloader.hide();
            this.AeviGrid.AeviStatusBar.error(message);
            this.AeviGrid.AeviToolbar.refresh();
            this.AeviGrid.AeviGridLocker.removeProtectedView();

            this.AeviGrid.AeviGridLocker.lock();

            if (!AeviDataService.isServerDataValid(response)) {
                setTimeout(() => {
                    this.AeviGrid.AeviStatusBar.error(message);
                }, 300);
            }

            result.resolve();
		}).fail(() => {
			this.AeviGrid.AeviAjaxPreloader.show();
			this.AeviGrid.AeviDOM.closeModals();

            setTimeout(() => {
				this.commit(query, reload);
			}, 2000);
		});

        return result;
	}

    public static hasConnectionProblem(response: any): boolean {
        return (_.isNull(response.Data) || (!_.isNull(response.Message) && response.Message !== ''))
    }

    public static isServerDataValid(response: IAeviCommitResponse): boolean {
        if (response.Status === MyRetail.IAeviResponseStatus.Error) {
            return false;
        }

        if (_.isNull(response.Data)) {
            return true;
        }

        return false;

        //if (response.Status === MyRetail.IAeviResponseStatus.OK) {
        //    return true;
        //}
        //
        //return false;
	}

    public static isLocalDataValid(records: number): boolean {
		return (records < 1);
	}

	deleteData(guids: string[], firstNotDeletedRecordIndex: number): void {
		var deleteMessage;
		var repoGuids = guids.slice();

		this.AeviApiService.deleteData(guids).done((response: IAeviPutResponse) => {
            if (_.isNull(response.Data)) {
                deleteMessage = this.translates.translate('table_data_deleted');
			} else {
				deleteMessage = response.Message;
			}

			var deletingDataRows = [];

			for (var i = 0; i < repoGuids.length; i++) {
				var row = this.AeviDataRepository.AeviTableData.getRecord(repoGuids[i]);

				if (!aeviIsUndefinedOrNull(row) && row.length > 0)
					deletingDataRows.push(row);
			}

			this.AeviGrid.AeviPluginApi.callPluginsOnRowsDeleting(deletingDataRows);
			this.AeviDataRepository.AeviTableData.deleteData(repoGuids);

			this.AeviGrid.AeviAjaxPreloader.hide();
			this.AeviGrid.AeviPager.AeviPagerHandler.setJumps();
			this.AeviGrid.AeviGridEditor.refreshData();

			this.AeviGrid.AeviPager.moveTo({
				rowId: firstNotDeletedRecordIndex
			});

			this.AeviGrid.AeviAjaxPreloader.hide();

            this.checkResponseStatus(response);
		}).fail(() => {
			toastr.error(this.translates.translate('connection_fail'));
			this.AeviGrid.AeviAjaxPreloader.show();
			setTimeout(() => {
				this.AeviGrid.AeviGridEditor.deleteRow();
			}, 4000);
		});
	}

	putRow(rowIndex: number) {
		var apiData = this.AeviDataRepository.AeviTableData.getApiDataByRowIndex(rowIndex);
		apiData = this.setPutType(rowIndex, apiData);
		var async: boolean = true;
		this.putData(apiData, async);
	};

	setPutType(rowIndex: number, apiData: any): void {
        var putType = null;
        var status = this.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(rowIndex);

        if (status.indexOf('insert') !== -1)
            putType = MyRetail.putType.insert;
        else if (status.indexOf('update') !== -1)
            putType = MyRetail.putType.update;

        this.AeviGrid.log('PUT: ' + MyRetail.putType[putType]);

        apiData[0].splice(0, 0, putType);

		return apiData;
	}

    putData(putData: any, async?: boolean): JQueryDeferred<any> {
		var result = $.Deferred();
		this.AeviApiService.activeAjaxConnection++;

        if (!_.isEmpty(putData)) {
			var guidIndex: number = this.AeviDataRepository.AeviTableDescription.getGuidIndex();

            this.AeviApiService.putData(putData, async).done((response: IAeviPutResponse) => {
				this.AeviApiService.activeAjaxConnection--;

                for (var i = 0; i < putData.length; i++) {
                    // + 1 because rowIndex was transformed to putType, not deleted
                    var guid: string = putData[i][guidIndex - this.AeviDataRepository.AeviTableDescription.startFakeColumnsLength + 1];
                    var row = this.AeviDataRepository.AeviTableData.getRecord(guid);
                    var rowIndex = row[this.AeviDataRepository.AeviTableDescription.getRowNumberIndex()];
                    var isErrorRow: boolean = false;

                    /**
                     *  DataServices return error rows
                     */
                    if (response.Status === MyRetail.IAeviResponseStatus.Error) {
                        var errorRows = response.Data;

                        if (!_.isNull(errorRows) && errorRows.length > 0) {
                            for (var j = 0; j < errorRows.length; j++) {
                                if (putData[i][guidIndex] === errorRows[j].DataItem[guidIndex - this.AeviDataRepository.AeviTableDescription.startFakeColumnsLength]) {
                                    this.AeviDataRepository.AeviTableData.setErrorMessage(rowIndex, errorRows[j].Message);
                                    isErrorRow = true;
                                    errorRows.splice(j, 1);
                                    break;
                                }
                            }

                            if (isErrorRow) {
                                this.AeviDataRepository.AeviTableData.AeviStatus.update(guid, 'render insert invalid');
                            }
                        }
                    }

					var status: string = 'render';

					if (this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'softInvalid'))
						status += ' softInvalid';

					if (this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'serverInvalid'))
						status += ' serverInvalid';

                    for (var x = 1; x < putData[i].length; x++) {
						var column = this.AeviDataRepository.AeviTableDescription.getColumnHeader(x);
						var displayType = this.AeviDataRepository.AeviTableDescription.getDisplayType(column);
						var stringDisplayType = this.AeviDataRepository.AeviTableDescription.getColumnStringDisplayType(displayType);

						if (stringDisplayType === 'Image') {
                            row[x].Changed = false;
						}
					}

					if (!isErrorRow) {
                        this.AeviDataRepository.AeviTableData.AeviStatus.updateByRowIndex(rowIndex, status);
                    }

                    this.AeviGrid.AeviGridEditor.refreshRow({rowIndex: rowIndex});

                    this.checkResponseStatus(response);

                    $(document).trigger('mouseup');
				}

				if (this.AeviApiService.activeAjaxConnection < 1)
					this.AeviGrid.AeviAjaxPreloader.hide();

				this.AeviGrid.AeviPager.AeviPagerHandler.setJumps();

				result.resolve();

			}).fail(() => {
				this.AeviApiService.activeAjaxConnection--;
				this.AeviGrid.AeviAjaxPreloader.show();
				toastr.error(this.translates.translate('connection_fail'));
				setTimeout(() => {
					this.putData(putData);
				}, 8000);
			});
		}

		return result;
	}

    checkResponseStatus(response: IAeviEntityStatusResponse) {
        if (response.Status === MyRetail.IAeviResponseStatus.Blocked) {
            this.showBlockModal(response)
                .done(() => {
                    this.AeviApiService.setDataEntityBlockStatus();
                })
                .fail(() => {
                    this.AeviGrid.AeviGridLocker.lock();
                });
        }

        if (response.Status === MyRetail.IAeviResponseStatus.Changed) {
            this.showDataChangedModal(response)
                .done(() => {
                    this.AeviApiService.setDataEntityBlockStatus().done(() => {
                        this.getDataByQuery('', true);
                    });
                })
                .fail(() => {
                    this.AeviGrid.AeviGridLocker.lock();
                });
        }
    }

    paste(data, cellIndex: IAeviCellIndex): void {
        if(this.AeviGrid.mode.clipboard === 0 || (this.AeviGrid.mode.clipboard === 1 && this.AeviGrid.AeviClipboard.canPasteOneCell)) {
            this.pasteOneCell(data, cellIndex);
            return;
        }

        this.AeviGrid.AeviGridLocker.unLockWithoutAccessControl();

        this.AeviDataRepository.AeviTableData.pasteRecords(data, cellIndex);

        this.AeviGrid.AeviPluginApi.callPluginsOnRowsChanged(data);

        var firstPastedRowIndex: number = cellIndex.rowIndex;
		var lastPastedRowIndex: number = data.Rows.length + firstPastedRowIndex;
		var request: any[] = [];

		for (var rowIndex = firstPastedRowIndex; rowIndex < lastPastedRowIndex; rowIndex++) {
            if(this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'invalid'))
                continue;

            var putType = MyRetail.putType.update;

			if (this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'newrow'))
	            putType = MyRetail.putType.insert;

            var apiData = this.AeviDataRepository.AeviTableData.getApiDataByRowIndex(rowIndex);
            apiData[0].splice(0, 0, putType);
            request.push(apiData[0]);
		}

        this.AeviApiService.setDataChanged(true);

        this.AeviGrid.AeviPager.AeviPagerHandler.setJumps();
		this.AeviGrid.AeviPager.setVisibleContentInfo();
        this.showData('re-render');
		this.AeviGrid.setSizes();

        if(request.length > 0) {
            this.AeviGrid.AeviAjaxPreloader.show();

            this.putData(request, true).done(() => {
                if (!this.AeviGrid.AeviClipboard.canPasteOneCell) {
                    this.AeviDataRepository.enableOrDisableImageColumns('disable');
                    this.AeviGrid.AeviStatusBar.info(this.AeviGrid.AeviLocalization.translate('save_mode_excel_' + this.AeviDataRepository.AeviTableDescription.saveModeExcel));
                    this.AeviGrid.AeviGridLocker.renderProtectedView('<div class="aeviProtected"><p>' + this.AeviGrid.AeviLocalization.translate('save_mode_excel_' + this.AeviDataRepository.AeviTableDescription.saveModeExcel) + '</p><span class="aeviProtected__save">' + this.AeviGrid.AeviLocalization.translate('toolbar_save') + '</span></div>');
                    this.showData('re-render');
                    this.AeviGrid.setSizes();
                    this.AeviGrid.AeviClipboard.allowOneCellPasting();
                }
            });
        }
	}

    pasteOneCell(data, cellIndex: IAeviCellIndex): void {
        var header = this.AeviDataRepository.AeviTableDescription.getColumnHeader(cellIndex.cellIndex);

        if (this.AeviDataRepository.AeviTableDescription.isColumnReadOnly(header)) {
            return;
        }

        this.AeviDataRepository.AeviTableData.pasteOneCell(data, cellIndex);
        if(!this.AeviDataRepository.AeviTableData.AeviStatus.is(cellIndex.rowIndex, 'invalid')) {
            this.putRow(cellIndex.rowIndex);
            this.AeviApiService.setDataChanged(true);
        }
    }

    executeAction(rowIndex: number, actionParameters: IAeviEntityActionDefinition): void {
        switch (MyRetail.IAeviEntityActionResultType[actionParameters.ResultType]) {
            case 'Image':
                this.executeImage(rowIndex, actionParameters);
                break;

            case 'Form':
                this.executeForm(rowIndex);
                break;

            default:
                break;
        }
	}

    executeForm(rowIndex: number) {
        return new AeviForm(rowIndex, this, {isOpenedByToolbar: false});
    }

    executeImage(rowIndex: number, actionParameters: IAeviEntityActionDefinition): void {
        var data = this.AeviDataRepository.AeviTableData.getRecordByRowIndex(rowIndex);
        var apiData = this.AeviDataRepository.AeviTableData.getApiDataWithColumnNames(data);

        var executeActionDefinition: IAeviEntityActionDefinition = {
            'ActionId': actionParameters.ActionId,
            'ResultType': actionParameters.ResultType,
            'Data': apiData
        };

        this.AeviGrid.AeviAjaxPreloader.show();

        this.AeviApiService.executeAction(executeActionDefinition, true).done((response) => {
            if(aeviIsUndefinedOrNull(response)) {
                toastr.error(this.translates.translate('connection_fail'));
                return;
            }

            if (!aeviIsUndefinedOrNull(response.Message)) {
                this.AeviGrid.AeviStatusBar.error(response.Message);
                toastr.error(response.Message);
                return
            }

            if (!aeviIsUndefinedOrNull(response.Data)) {
                var image = document.createElement('img');
                image.src = 'data:image/' + this.AeviGrid.AeviConsts.imageFormat + ';base64,' + response.Data;

                image.addEventListener('load', () => {
                    var modal = new AeviModal(this.AeviGrid);
                    modal.setContent({
                        title: this.translates.translate('preview'),
                        text: '<div class="actionImage"><img width="' + image.width + '" height="' + image.height + '" src="' + image.src + '"></div>'
                    });
                    modal.show();
                });
            }

            this.AeviGrid.AeviAjaxPreloader.hide();
        }).fail(() => {
            toastr.error(this.translates.translate('connection_fail'));
        });
    }

	clearData(): void {
		var selectedCells: IAeviSelectedPositions = this.AeviDataRepository.getSelectedCellsPositions();
        var clearingRowsIndexes: number[] = [];

        for (var i = 0; i < selectedCells.rows.length; i++) {
			var rowIndex: number = selectedCells.rows[i];
			if (!this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'render'))
				continue;

			clearingRowsIndexes.push(rowIndex);
		}

        this.AeviDataRepository.clearCells(selectedCells);
        this.AeviDataRepository.AeviTableData.validate();

		var request: any[] = [];

        for (i = 0; i < clearingRowsIndexes.length; i++) {
			var rowIndex = clearingRowsIndexes[i];

            if (!this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'invalid')) {
                var putType = MyRetail.putType.update;

                if (this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'newrow'))
                    putType = MyRetail.putType.insert;

                var apiData = this.AeviDataRepository.AeviTableData.getApiDataByRowIndex(rowIndex);

                // setting put type
                apiData[0].splice(0, 0, putType);

                request.push(apiData[0]);
			}
		}

        if (request.length > 0) {
            this.putData(request);
        }
	}

    importFile(formData: IAeviBinaryData, fileType: string): void {
        this.AeviGrid.AeviAjaxPreloader.show();

        this.AeviGrid.AeviDataService.AeviApiService.importFile(formData, fileType).done((response: IImportResponse) => {
            this.AeviGrid.AeviAjaxPreloader.hide();


            if (!_.isNull(response.ErrorMessage)) {
                var modal = new AeviModal(this.AeviGrid);
                modal.setContent({
                    title: this.AeviGrid.AeviLocalization.translate('warning'),
                    text: response.ErrorMessage
                });
                modal.show();
                return;
            }

            if (_.isString(response.Data)) {
                var event = new Event('fakePaste');
                this.AeviGrid.AeviClipboard.setClipboardMode();
                this.AeviGrid.AeviClipboard.paste(event, response.Data);
            }
        }).error((err) => {
            this.AeviGrid.AeviAjaxPreloader.hide();
        });
    }

    showBlockModal(response: IAeviEntityStatusResponse) {
        var promise = $.Deferred();

        var modal = new AeviModal(this.AeviGrid, (confirmSetMeAsBlockerUser) => {
            if (confirmSetMeAsBlockerUser) {
                promise.resolve();
            } else {
                promise.reject();
            }
        });

        var blockingUserName = (aeviIsUndefinedOrNull(response.ExtendedData)) ? 'empty' : response.ExtendedData.UserName;

        modal.setContent({
            title: this.AeviGrid.AeviLocalization.translate('warning'),
            text: this.AeviGrid.AeviLocalization.translate('entity_is_blocked1') + '<strong>' + blockingUserName + '</strong>' + this.AeviGrid.AeviLocalization.translate('entity_is_blocked2'),
            buttons: ['aeviModalAccept', 'aeviModalDenied'],
            showCloseButton: false
        });

        modal.show();

        return promise;
    }

    showDataChangedModal(response: IAeviEntityStatusResponse) {
        var promise = $.Deferred();

        var modal = new AeviModal(this.AeviGrid, (confirmReloadData) => {
            if (confirmReloadData) {
                promise.resolve();
            } else {
                promise.reject();
            }
        });

        var blockingUserName = (aeviIsUndefinedOrNull(response.ExtendedData)) ? 'empty' : response.ExtendedData.UserName;

        modal.setContent({
            title: this.AeviGrid.AeviLocalization.translate('warning'),
            text: this.AeviGrid.AeviLocalization.translate('entity_is_changed1') + '<strong>' + blockingUserName + '</strong>' + this.AeviGrid.AeviLocalization.translate('entity_is_changed2'),
            buttons: ['aeviModalAccept', 'aeviModalDenied'],
            showCloseButton: false
        });

        modal.show();

        return promise;
    }

    getBlockStateCommand() {
        var promise = $.Deferred();

        this.AeviGrid.AeviDataService.AeviApiService.getDataEntityStatus()
            .done((response: IAeviEntityStatusResponse) => {
                switch(response.Status) {
                    case MyRetail.IAeviResponseStatus.Blocked:
                        this.AeviGrid.AeviDataService.showBlockModal(response)
                            .done(() => {
                                this.AeviGrid.AeviDataService.AeviApiService.setDataEntityBlockStatus().done(() => {
                                    promise.resolve(MyRetail.IAeviBlockStateCommand.CONTINUE);
                                });
                            })
                            .fail(() => {
                                promise.reject(MyRetail.IAeviBlockStateCommand.LOCK);
                            });
                        break;

                    case MyRetail.IAeviResponseStatus.Changed:
                        this.AeviGrid.AeviDataService.showDataChangedModal(response)
                            .done(() => {
                                this.AeviGrid.AeviDataService.AeviApiService.setDataEntityBlockStatus().done(() => {
                                    promise.reject(MyRetail.IAeviBlockStateCommand.RELOAD);
                                });
                            })
                            .fail(() => {
                                promise.reject(MyRetail.IAeviBlockStateCommand.LOCK);
                            });
                        break;

                    default:
                        this.AeviGrid.AeviDataService.AeviApiService.setDataEntityBlockStatus().done(() => {
                            promise.resolve(MyRetail.IAeviBlockStateCommand.CONTINUE);
                        });
                        break;
                }
            });

        return promise;
    }
}
