/// <reference path='../../references.ts' />

interface IAeviModalOptions {
    minWidth?: number;
    minHeight?: number;
}
