/// <reference path='../../references.ts' />
/// <reference path='IAeviModal.ts' />
/// <reference path='IAeviModalContent.ts' />
/// <reference path='IAeviModalOptions.ts' />
/// <reference path='../../libs/jquery.simplemodal.d.ts' />
/// <reference path='../AeviHandlers/AeviModalHandler.ts' />

class AeviSimpleModal implements IAeviModal {
	AeviGrid: any;
	AeviModalHandler: IAeviHandler;
	callback: any;
	modalWindow: HTMLElement;
	content: string;
    options: IAeviModalOptions;
    showCloseButton: boolean;

    minWidth: number;
    minHeight: number;

	constructor(aeviGrid: any, callback?) {
		this.AeviGrid = aeviGrid;
		this.callback = callback;
		this.listener();
	}

	listener() {
		$(document).off('click', '#aeviModalAccept').on('click', '#aeviModalAccept', (event) => {
			event.preventDefault();
			this.callback(true);
			this.close();
		});

		$(document).off('click', '#aeviModalDenied').on('click', '#aeviModalDenied', (event) => {		
			event.preventDefault();
			this.callback(false);
			this.close();
		});
	}

	show(onShowFunction?, onCloseFunction?) {
		if(this.AeviGrid.isModalOpened)
            this.close();

        $.modal(
            this.content, {
            minWidth: this.minWidth,
            minHeight: this.minHeight,
	    	onShow: (event) => {
                this.setSizes();
                this.modalWindow = $(event.currentTarget)[0];
		    	this.AeviModalHandler = new AeviModalHandler(this.AeviGrid, this, this.modalWindow);
		    	this.AeviGrid.isModalOpened = true;
                if(!aeviIsUndefinedOrNull(onShowFunction))
		    		onShowFunction();

                if (!this.showCloseButton) {
                    $('.modalCloseImg').remove();
                }
	        },
	        onClose: () => {
	        	this.AeviGrid.isModalOpened = false;
                this.close();
	        	if(!aeviIsUndefinedOrNull(onCloseFunction))
		    		onCloseFunction();
	        }
	    });
	}

	close() {
		$.modal.close();
	}

	focus(what: string) {
		$.modal.focus(what);
	}

	getButtons(keys): string {
		var modalButtons = {
			'aeviDataServiceCommitTemp' :  '<input type="button" id="aeviDataServiceCommitTemp" class="aeviButton aeviButton--secondary" value="' + this.AeviGrid.AeviLocalization.translate('save_data') + '">',
			'aeviDataServiceCommitTempAndClose' :  '<input type="button" id="aeviDataServiceCommitTempAndClose" class="aeviButton aeviButton--secondary" value="' + this.AeviGrid.AeviLocalization.translate('save_data') + '">',			
			'aeviDataServiceDeleteTemp' : '<input type="button" id="aeviDataServiceDeleteTemp" class="aeviButton" value="' + this.AeviGrid.AeviLocalization.translate('delete_data') + '">',
			'aeviDataServiceDeleteTempAndClose' : '<input type="button" id="aeviDataServiceDeleteTempAndClose" class="aeviButton" value="' + this.AeviGrid.AeviLocalization.translate('delete_data') + '">',
			'aeviDataServiceDeleteTempIgnoreAndClose' : '<input type="button" id="aeviDataServiceDeleteTempAndClose" class="aeviButton" value="' + this.AeviGrid.AeviLocalization.translate('ignore_errors') + '">',
			'aeviDataServiceClose' : '<input type="button" id="aeviDataServiceClose" class="aeviButton" value="' + this.AeviGrid.AeviLocalization.translate('cancel') + '">',
			'aeviProtectedUnlock' : '<input type="button" class="aeviButton aeviButton--secondary aeviProtected__unlock" value="' + this.AeviGrid.AeviLocalization.translate('button_enable_editing') + '">',
			'simpleModalClose' : '<input type="button" class="aeviButton simplemodal-close" value="' + this.AeviGrid.AeviLocalization.translate('cancel') + '">',
			'aeviModalAccept' : '<input type="button" id="aeviModalAccept" class="aeviButton aeviButton--secondary" value="' + this.AeviGrid.AeviLocalization.translate('yes') + '">',
			'aeviModalDenied' : '<input type="button" id="aeviModalDenied" class="aeviButton" value="' + this.AeviGrid.AeviLocalization.translate('no') + '">',
			'aeviDataServiceDiscard' : '<input type="button" id="aeviDataServiceDiscard" class="aeviButton aeviButton--secondary" value="' + this.AeviGrid.AeviLocalization.translate('ok') + '">',
		};

		var buttons = [];

		for(var i = 0; i < keys.length; i++)
			buttons.push(modalButtons[keys[i]]);

		return buttons.join('');
	}

    setOptions(options: IAeviModalOptions) {
        this.options = options;
        for(var key in this.options)
            this[key] = this.options[key];
    }

    /**
     * TODO!
     * hardstring
     */
	setContent(content: IAeviModalContent) {
		content.id = (aeviIsUndefinedOrNull(content.id)) ? 'aeviModalWindow' : content.id;
		content.title = (aeviIsUndefinedOrNull(content.title)) ? '' : content.title;
		content.text = (aeviIsUndefinedOrNull(content.text)) ? '' : content.text;
		content.buttons = (aeviIsUndefinedOrNull(content.buttons)) ? '' : this.getButtons(content.buttons); 

		this.content = 
			'<div id="' + content.id + '" class="aeviWsw aeviModal">' + 
				'<h2>' + content.title  + '</h2>' +
				content.text + 
				'<div class="aeviModal__buttons text-right">' +
					content.buttons + 
				'</div>' +
			'</div>';

        this.showCloseButton = (aeviIsUndefinedOrNull(content.showCloseButton) || content.showCloseButton === true) ? true : false;
	}

	static closeAll(): void {
        $.modal.close();
	}

    /**
     * TODO!
     * set height size...
     */
    setSizes(): void {
        if (aeviIsUndefinedOrNull(this.minWidth))
            return;

        $(this.AeviGrid.AeviConsts.aeviModalSelector).css({
            'max-width': this.minWidth
        });
    }
}
