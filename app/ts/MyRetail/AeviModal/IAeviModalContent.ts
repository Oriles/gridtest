/// <reference path='../../references.ts' />

interface IAeviModalContent {
    id?: string;
    title?: string;
    text: string;
    buttons?: string|string[];
    showCloseButton?: boolean;
}
