/// <reference path='../../references.ts' />
/// <reference path='IAeviModalContent.ts' />

interface IAeviModal {
    setOptions(options: IAeviModalOptions): void;
    setContent(content: IAeviModalContent): void;
	show(onShowFunction?, onCloseFunction?);
    close(...args: any[]): void;
}
