/// <reference path='../../references.ts' />

interface IAeviCellValue {
    DataValue: any;
	DisplayValue: any;
    OutputValue?: any;
}
