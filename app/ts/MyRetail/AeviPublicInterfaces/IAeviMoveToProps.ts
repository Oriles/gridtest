/// <reference path='../../references.ts' />

interface IAeviMoveToProps {
    cellId?: number;
    rowId?: number;
    click?: boolean;
    guid?: string;
    scrollLeft?: boolean;
}
