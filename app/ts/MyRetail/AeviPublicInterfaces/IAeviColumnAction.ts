/// <reference path='../../references.ts' />

interface IAeviColumnAction {
    columnIndex: number;
    columnActionType: MyRetail.IAeviColumnActionType;
}
