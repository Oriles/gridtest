/// <reference path='../../references.ts' />

interface IAeviImageData {
    HasValue: boolean;
    Changed: boolean;
    Value: string;
}
