/// <reference path='../../references.ts' />

interface IAeviCellIndex {
	rowIndex: number;
	cellIndex: number;
	rowId?: number;
	cellId?: number;
}
