/// <reference path='../references.ts' />

class AeviAjaxPreloader {
	AeviGrid: any;
	loader: Element;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.render();
	}

	render(): void {
		var aeviWrapperElement: Element = document.querySelector(this.AeviGrid.AeviConsts.aeviWrapperSelector);
		var preloaderHTML: string = '<i class="aeviAjaxPreloader"></i>';

		this.AeviGrid.AeviDOM.append(aeviWrapperElement, preloaderHTML);
		this.loader = document.querySelector('.aeviAjaxPreloader');
	}

	listen() {
		$(document)
			.ajaxStart(() => {this.show();})
			.ajaxStop(() => {this.hide();});
	}

	show(): void {
        this.loader.classList.add('visible');
	}

	hide(): void {
        this.loader.classList.remove('visible');
	}
}
