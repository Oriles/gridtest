/// <reference path='../references.ts' />
/// <reference path='AeviHandlers/AeviSearchHandler.ts' />

class AeviSearch {
	AeviGrid: any;
	AeviSearchHandler: any;
	isSearchRendered: boolean;
	isSearchVisible: boolean;
	aeviSearch: string;
	positionOfFoundRecords: any[];
	keyword: string;
	currentCell: any;
	direction: string;
	actualSelected: any;

	constructor(aeviGrid: any) {
		this.AeviGrid = aeviGrid;
		this.isSearchRendered = false;
		this.isSearchVisible = false;

		this.aeviSearch = this.AeviGrid.tableId + '-aeviSearch';

		if (!this.isSearchRendered) {
			this.render();
			this.AeviSearchHandler = new AeviSearchHandler(this.AeviGrid, this);
		}

		this.positionOfFoundRecords = [];
		this.keyword = null;

		this.show();
	}

	render(): void {
		var translator = this.AeviGrid.AeviLocalization;
		var template = _.template(
			'<div class="<%= className %>">' +
                '<h3 class="aeviSearch__title"><%= title %></h3>' +
                '<p class="aeviSearch__subtitle"><%= subTitle %></p>' +
                '<input type="text" class="aeviSearch__field">' +
                '<div>' +
                    '<p class="aeviSearch__direction"><%= direction %></p>' +
                    '<label><input type="radio" name="direction" value="down" class="aeviSearch__radio" checked><%= down %></label>' +
                    '<label><input type="radio" name="direction" value="up" class="aeviSearch__radio"><%= up %></label>' +
                '</div>' +
                '<div class="textright">' +
                    '<input type="button" class="aeviSearch__find aeviButton aeviButton--secondary" value="<%= find %>">' +
                    '<input type="button" class="aeviSearch__cancel aeviButton" value="<%= cancel %>">' +
                '</div>' +
                '<i class="aeviSearch__close"></i>' +
			'</div>'
		);

		$('body').append(
			template({
				className: this.aeviSearch + ' aeviSearch aeviWsw',
				title: translator.translate('search_title'),
				subTitle: translator.translate('search_insert_keyword'),
				direction: translator.translate('search_direction'),
				up: translator.translate('search_up'),
				down: translator.translate('search_down'),
				find: translator.translate('search_find'),
				cancel: translator.translate('cancel')
			})
		);

		this.isSearchRendered = true;
	}

	show(): void {
		$('.' + this.aeviSearch + ', .aeviSearchBlackBox').show('fade', 200);
		$('.' + this.aeviSearch).find('.aeviSearch__field').focus();

		if (this.AeviGrid.AeviClientSide.isIe())
			$('.aeviSearch__field').trigger('click');

		this.isSearchVisible = true;
	}

	hide(): void {
		$('.' + this.aeviSearch + ', .aeviSearchBlackBox').hide('fade', 200);
		this.isSearchVisible = false;
	}

	find(keyword: string, direction: string) {
		if (keyword === '' || aeviIsUndefinedOrNull(direction))
			return;

		if (keyword === this.keyword) {
			this.keyword = keyword;
			this.direction = direction;
			this.selectFoundCell();
			return;
		}

		var aeviDataRepository = this.AeviGrid.AeviDataService.AeviDataRepository;
		var data = aeviDataRepository.AeviTableData.getData();

		if (data.length < 1)
			return;

		this.cleanPositionOffFoundRecords();

		for (var rowIndex = 0; rowIndex < data.length; rowIndex++) {
			for (var columnIndex = 0; columnIndex < data[rowIndex].length; columnIndex++) {
				var dataValue = data[rowIndex][columnIndex];

				var displayValue = aeviDataRepository.AeviTableData.getDisplayValue(dataValue, columnIndex);

				var column = aeviDataRepository.AeviTableDescription.getColumnHeader(columnIndex);

				if (!aeviIsUndefinedOrNull(column))
					keyword = this.getSearchKeyword(keyword, column);

				if (AeviSearch.isSearchKeywordInDisplayValue(displayValue, keyword)) {
					this.addFoundPosition(rowIndex, columnIndex);
				}
			}
		}

		this.keyword = keyword;
		this.direction = direction;

		var foundRecordsPosition = this.getPositionOfFoundRecords();

		if (foundRecordsPosition.length < 1) {
			toastr.info(this.AeviGrid.AeviLocalization.translate('no_found'));
			this.AeviGrid.AeviStatusBar.info(this.AeviGrid.AeviLocalization.translate('no_found'));
			return;
		} else {
			toastr.info(this.AeviGrid.AeviLocalization.translate('found') + foundRecordsPosition.length + this.AeviGrid.AeviLocalization.translate('records'));
			this.AeviGrid.AeviStatusBar.info(this.AeviGrid.AeviLocalization.translate('found') + foundRecordsPosition.length + this.AeviGrid.AeviLocalization.translate('records'));
		}

		this.selectFoundCell();
	}

	cleanPositionOffFoundRecords(): void {
		this.positionOfFoundRecords = [];
	}

	static isSearchKeywordInDisplayValue(displayValue: string, keyword: string): boolean {
		return displayValue.toLowerCase().indexOf(keyword.toLowerCase()) !== -1;
	}

	addFoundPosition(rowIndex: number, columnIndex: number): void {
		this.positionOfFoundRecords.push({ rowId: rowIndex, cellId: columnIndex });
	}

	getPositionOfFoundRecords(): any {
		return this.positionOfFoundRecords;
	}

	getFoundCell() {
		this.currentCell = this.getCurrentCell();
		var selectedRecordsPosition = this.getSelectedFoundPositions();
		var closestPosition = this.getClosestPositions(selectedRecordsPosition);

		if (!aeviIsUndefinedOrNull(closestPosition))
			return closestPosition;

		return null;
	}

	getClosestPositions(rows: any[]) {
		var position = {
			prevPosition: null,
			nextPosition: null
		};

		for (var i = 0; i < rows.length; i++) {
			var item = rows[i];

			if (item.selected) {
				position.prevPosition = (i === 0) ? rows[rows.length - 1].cellIndexes : rows[i - 1].cellIndexes;
				position.nextPosition = (i === rows.length - 1) ? rows[0].cellIndexes : rows[i + 1].cellIndexes;
			}
		}

		if (this.direction === 'down')
			return position.nextPosition;
		else if (this.direction === 'up')
			return position.prevPosition;

		return null;
	}

	getCurrentCell() {
		var currentCell = null;
		var lastCell = this.AeviGrid.AeviGridHandler.selectedCell;

		if (aeviIsUndefinedOrNull(lastCell))
			lastCell = this.AeviGrid.AeviGridHandler.lastSelectedCell;

		if (aeviIsUndefinedOrNull(lastCell))
			currentCell = { rowId: -1, cellId: -1 };
		else
			currentCell = this.AeviGrid.AeviDOM.getCellIndexes(lastCell);

		return currentCell;
	}

	getSelectedFoundPositions() {
		var foundRecordsPosition = this.getPositionOfFoundRecords();

		var selectedRecordsPosition = [];
		for (var i = 0; i < foundRecordsPosition.length; i++) {
			var row = foundRecordsPosition[i];

			if (this.currentCell.rowId === row.rowId && this.currentCell.cellId === row.cellId)
				continue;

			selectedRecordsPosition.push({
				selected: false,
				cellIndexes: row,
				position: parseFloat(row.rowId + '.' + row.cellId)
			});
		}

		selectedRecordsPosition.push({
			selected: true,
			cellIndexes: this.currentCell,
			position: parseFloat(this.currentCell.rowId + '.' + this.currentCell.cellId)
		});

		for (i = 0; i < selectedRecordsPosition.length; i++) {
			if (selectedRecordsPosition[i].cellIndexes == this.actualSelected)
				selectedRecordsPosition[i].selected = this.actualSelected;
		}

		selectedRecordsPosition = _.sortBy(selectedRecordsPosition, function(props) { return props.position; });

		return selectedRecordsPosition;
	}

	selectFoundCell() {
		var cell: IAeviMoveToProps = _.clone(this.getFoundCell());

        if (aeviIsUndefinedOrNull(cell))
			return;

		cell.click = true;
		this.AeviGrid.unSelectAllRowsAndCells();
		this.AeviGrid.AeviGridHandler.triggerClick();
		this.AeviGrid.AeviPager.moveTo(cell);

		/**
		 * because we support enter key for navigate
		 */
		setTimeout(() => {
			this.AeviSearchHandler.focusField();
		}, 50);
	}

	getSearchKeyword(keyword, column) {
		var displayType = this.AeviGrid.AeviDataRepository.AeviTableDescription.getDisplayType(column);
        var columnType = this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnStringDisplayType(displayType);

		switch (columnType) {
			case 'Currency':
			case 'Number':
				var decimalPlaces = AeviGlobal.getDecimalPlaces(keyword);

                if (decimalPlaces < 1 && !_.isNaN(decimalPlaces))
					keyword = AeviGlobal.removeDecimalPlaces(keyword);

				return keyword;

			default:
				break;
		}

		return keyword;
	}
}
