/**
 * libraries
 */
/// <reference path='libs/jquery.d.ts' />
/// <reference path='libs/jqueryui.d.ts' />
/// <reference path='libs/aevi.d.ts' />
/// <reference path='libs/jquery.ui.datetimepicker.d.ts' />
/// <reference path='libs/jquery.textwidth.d.ts' />
/// <reference path='libs/jquery.cookie.d.ts' />
/// <reference path='libs/underscore.d.ts' />
/// <reference path='libs/toastr.d.ts' />
/// <reference path='libs/window.d.ts' />
/// <reference path='MyRetail/AeviGlobal.ts' />
