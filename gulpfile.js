﻿﻿var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var less = require('gulp-less');
var eol = require('gulp-eol');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify-css');
var rename = require('gulp-rename');
var rimraf = require('gulp-rimraf');
var typedoc = require("gulp-typedoc");
var ts = require('gulp-typescript');

gulp.task('compileTs', function() {
	MyRetail.compileTS();
});

gulp.task('compileThemes', function() {
	var pckg = require('./package.json');
	var themes = pckg.themes;

	for(var i = 0; i < themes.length; i++) {
		var themeName = themes[i];
		MyRetail.compileThemeCSS(themeName);
		MyRetail.copyJSAndMinify(themeName);
	}

	MyRetail.cleanTemporaryFiles();
});

gulp.task("generateDocs", function() {
	MyRetail.concatTS();

	setTimeout(function() {
		MyRetail.generateDocs();
	}, 4000);
});

var MyRetail = (function() {
	var deploy = require('./package.json').deploy;

	var moduleName = deploy.moduleName;
	var buildFolder = deploy.buildFolder;
	var docsFolder = deploy.docsFolder;

	var tempLESS = deploy.temporaryLESSFile;
	var tempCSS = deploy.temporaryCSSFile;
	var sourceLESSPath = deploy.LESSPath;
	var sourceLESSConfigName = sourceLESSPath + deploy.LESSConfigName;
	var compileLESSname = deploy.compileLESSname;
	var LESSThemesFolderName = deploy.LESSThemesFolderName;

	var sourceTSPath = deploy.TSPath;
	var jsBuildFolder = deploy.jsBuildFolder;
	var compileJSName = deploy.compileJSName;

	var compileTS = function() {
		var tsProject = ts.createProject('./tsconfig.json');
		var tsResult = tsProject.src().pipe(ts(tsProject));

		return tsResult.js.pipe(gulp.dest('./'));
	};

	var concatTS = function() {
		gulp.src([
				sourceTSPath + '*.ts',
				sourceTSPath + moduleName + '/**/*.ts'
			])
			.pipe(concat(moduleName + '.ts'))
			.pipe(gulp.dest(jsBuildFolder));
	};

	var generateDocs = function() {
		gulp.src([jsBuildFolder + '/' + moduleName + '.ts'])
			.pipe(typedoc({
				module: 'commonjs',
				target: 'es5',
				out: docsFolder,
				name: moduleName,
				ignoreCompilerErrors: true
			}));
	};

	var copyJSAndMinify = function(themeName) {
		gulp.src(jsBuildFolder + '/' + compileJSName + '.js')
			.pipe(rename(function (path) {
				path.basename = compileJSName;
			}))
			.pipe(gulp.dest(buildFolder + '/' + themeName))
			.pipe(uglify())
			.pipe(rename(function (path) {
				path.basename = compileJSName + '.min';
			}))
			.pipe(gulp.dest(buildFolder + '/' + themeName));
	};

	var compileThemeCSS = function(themeName) {
		console.log('Compiling theme ' + themeName);

		gulp.src([sourceLESSPath + LESSThemesFolderName + '/' + themeName + '.less', sourceLESSConfigName + '.less'])
			.pipe(concat(tempLESS))
			.pipe(gulp.dest(sourceLESSPath))
			.pipe(less())
			.pipe(eol('\r\n'))
			.pipe(gulp.dest(sourceLESSPath))
			.pipe(rename(function (path) {
				path.basename = compileLESSname;
			}))
			.pipe(gulp.dest(buildFolder + '/' + themeName))
			.pipe(minify())
			.pipe(rename(function (path) {
				path.basename = compileLESSname + '.min';
			}))
			.pipe(gulp.dest(buildFolder + '/' + themeName));
	};

	var cleanTemporaryFiles = function() {
		setTimeout(function(){
			return gulp.src(
				[sourceLESSPath + '/' + tempLESS, sourceLESSPath + '/' + tempCSS],
				{ read: true }
				)
				.pipe(rimraf({ force: true }));
		}, 3000);
	};

	return {
		compileTS: compileTS,
		compileThemeCSS: compileThemeCSS,
		cleanTemporaryFiles: cleanTemporaryFiles,
		copyJSAndMinify: copyJSAndMinify,
		generateDocs: generateDocs,
		concatTS: concatTS
	};
}());
