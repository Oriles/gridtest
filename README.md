# MyRetail - Grid

#### Grid API
- Generate Docs:

```
gulp generateDocs
```
- Root: `docs/modules/_myretail_.html`

#### Deployment with Gulp

- You must have installed **NODE.js** and **NPM package manager**

- Every task run from project root

- Install packages dependencies:

```
npm install
```

##### JS

- Typescript is transpiled to javascript with Gulp

```
gulp compileTs
```

- Configuration is specified in `tsconfig.json`.
- Transpiled file `AeviDataGrid.js` is located in `./js/`

##### CSS

- Generate CSS and copy JS for each theme:
    - Each theme must have theme config file located in `app/less/themes/themeName.less`
    - Each theme name must be in `package.json`!

```
gulp compileThemes
```
