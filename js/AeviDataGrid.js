var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AeviGlobal = (function () {
    function AeviGlobal() {
    }
    AeviGlobal.isNegative = function (value) {
        if (aeviIsUndefinedOrNull(value))
            return null;
        var stringValue = value.toString();
        return stringValue.charAt(0) === '-';
    };
    AeviGlobal.removeDecimalPlaces = function (num) {
        var strNum = num.toString();
        var parts = [];
        if (strNum.indexOf('.'))
            parts = strNum.split('.');
        if (strNum.indexOf(','))
            parts = strNum.split(',');
        return parts[0];
    };
    AeviGlobal.getDecimalPlaces = function (num) {
        var strNum = num.toString();
        var parts = [];
        var hasDecimalPoint = true;
        if (strNum.indexOf('.')) {
            parts = strNum.split('.');
            hasDecimalPoint = true;
        }
        else
            hasDecimalPoint = false;
        if (strNum.indexOf(',')) {
            parts = strNum.split(',');
            hasDecimalPoint = true;
        }
        else
            hasDecimalPoint = false;
        if (!hasDecimalPoint)
            return null;
        return parseFloat(parts[1]);
    };
    AeviGlobal.isElementInBodyViewport = function (element, bodyEl) {
        var cell = AeviGlobal.getBounding(element);
        var body = AeviGlobal.getBounding(bodyEl);
        var isInViewportVertically = (cell.top >= body.top && cell.bottom <= body.bottom);
        if (!isInViewportVertically)
            return false;
        var isInViewportHorizontally = (cell.left >= body.left && cell.right <= body.right);
        if (isInViewportHorizontally)
            return true;
        return false;
    };
    AeviGlobal.isElementInBodyViewportHorizontally = function (element, bodyEl) {
        var cell = AeviGlobal.getBounding(element);
        var body = AeviGlobal.getBounding(bodyEl);
        return (cell.left >= body.left && cell.right <= body.right);
    };
    AeviGlobal.isElementInBodyViewportVertically = function (element, bodyEl) {
        var cell = AeviGlobal.getBounding(element);
        var body = AeviGlobal.getBounding(bodyEl);
        return (cell.top >= body.top && cell.bottom <= body.bottom);
    };
    AeviGlobal.getBounding = function (element) {
        return element.getBoundingClientRect();
    };
    AeviGlobal.stringToHTML = function (str) {
        if (aeviIsUndefinedOrNull(str)) {
            return null;
        }
        var HTMLString = str.replaceAll('\r\n', '<br>');
        HTMLString = HTMLString.replaceAll('\r', '<br>').replaceAll('\n', '<br>');
        return HTMLString;
    };
    AeviGlobal.getEmptyImageBase64 = function () {
        return 'iVBORw0KGgoAAAANSUhEUgAAAnUAAAHQCAMAAADwEGymAAAABGdBTUEAALGPC/xhBQAAAwBQTFRFsLCwsbGxsrKys7OztLS0tbW1tra2t7e3uLi4ubm5urq6u7u7vLy8vb29vr6+v7+/wMDAwcHBwsLCw8PDxMTExcXFxsbGx8fHyMjIycnJysrKy8vLzMzMzc3Nzs7Oz8/P0NDQ0dHR0tLS09PT1NTU1dXV1tbW19fX2NjY2dnZ2tra29vb3Nzc3d3d3t7e39/f4ODg4eHh4uLi4+Pj5OTk5eXl5ubm5+fn6Ojo6enp6urq6+vr7Ozs7e3t7u7u7+/v8PDw8fHx8vLy8/Pz9PT09fX19vb29/f3+Pj4+fn5+vr6+/v7/Pz8/f39/v7+////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIl5M9gAAAAlwSFlzAAAOwgAADsIBFShKgAAAAAd0SU1FB98EFAsxKPr6IvsAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNWWFMmUAACsiSURBVHhe7Z0JY+M4lqTZaYmk5Ps+Ur7STl+6RVKUs3pm9urZnZ6uyv//bzbiAZIlUc7Kykpjt8fx2ZJI4OEBBEMAeEHReQucOz6ent6M/+Prr7/+9ttvX/+jvD0/+ziNmtI6g/ntv/z2G42+fv0tQ5CP8mAVfs7O+39D/Nff/vHb1/91X7H5SMet1uUDTIx//Ntt6+PZWeuiYtq6+yt8fEWGX//3oHX28bSaI0Kf/ictUKRf//XxolUpNor08fw6+z+/fWWpv/72r5/OKiYwOvt4/vlfbPP/8Y9f/31wuZzTeevk7BRG9391pf769W/91tnpSdUZ6vT57+YJGQ4uWmcn1fywwTf/+qtt2m//+d+fLl2tLILavhr83cqMbftvN2enlSKh2k7PbiZ///oP2yX/PrquZoX9doqa/JsrNHhsoZL47+OntK6K//j1t19ZT//5fAvXVQmgUi76LA+37u/PN/BSlcnHs/Pu/3C7Dfut34IgfAyrOdpsNJrrDnw2tq7yL8/gyy/ZRbNhYQs01zfSdPPuy5fnL3g9P181ly3W6bDZbHwcThD/y/Pz5H5vyQ3WNjY21uH/kHmR8nEzZsysMDOSnYfJczl+/iuKdI6kVQsENI97yOl5Uj5/aR+kqY94gSbp+sX4F/DlefKlvVXxgrxZ8J37Ccv95Ze/ZieWbg5sfGMDn429h1+w+WRwljSaoFqq9HP5ZTIpkd8vZzRYR20u0WzudJgV/JSdw0ZjHXXiozwoUNL4WP7yPGZtT562uE8WQdYowe7nAl4m4+dfRq1qTo0UJW/GW30rM/jleHnbPM2N1oDbj2KVt1sN7KVGWjFsNlt//cK6/vKluNlIEF8p9kYzOWhjy6wuB8dpc2NqwpqI0jjd3t3eIXjf3rsaFEWRZUUx+LS3vbW1ZTEvbG1tb+3s3+d5keXZOBtdVSx2tgGszrujPMtyeHs42lk0osX29g48neWe0f0+A+De20zZ3jp6QG45SlT0znc2EVAxQdBJu4AXmOWPJ1ULbBp8b1+OYIMiZUV7H/kvgWTg8C4vxgWL3W9hc32cA2vbu9hgFGlspc6LbmuTNrY982D9dggnNBq3sGpl8JGe7e3dgyeUB2TDNou97MXKvXs+oh9s2+gRu2S53AxBsW+HLE4+Hvcv9irbto1CopgHXVgY41NfpGW2di8G8MMdN7o92Mb+ru6Snc2tC/gZcpcMb5Eb6tbHeEwAp21s18hq8gz5ey+ISpMoSbeun+4fjHv89VDjY1Dkg6dHC1vk/v7+6WE4zscoWgmHD3c+Yp67+/u7XpZz742L8ai95OUeq3f26jArUgweHy18OcP7hzb2XlmyQrPu/Z23mgdFenwaWWYo1ahTtQAIu+viq4LqQpHyh8/VciPV/cPTgEVmXeW9ZUfYqvunu7vHu6eR1RGKnXU+w+gezpZNH/pwAovxuOw+woBmPtLDco9Qathh/7SxupQfnd4/PHax7SU2HzX+8LjC5PEJjvompnFZZF1UkY/z3DPk7rHdzlgcKzd2CYyWfTGkh9Jg600AD/esporV3X3P6shU8ogiVcrE/dTGtrEKUN9tCGIa9fApaURxstt9njhKMsaeYbmKcWlhFvMCLCaIhys0Gfm4amBeJiWFYoWH+Co++IdAVtK0GswInUg1Q2TGKsdrTM+TFe7oyuoKb9yEZQuAIGSFwlh9TcZF1cYVnK0YJQ5r5rYAPKMXRwnKWbHNalWOLAcqCRbFmAarTOiBIsc7lqsGVk8woi75PoGnSpnKCcoEM9vHtKv4YcBzWT5zj7DUcOV2wKoczQ8KREfc1GqOhLVNI1gxcx86BesYFNnGsT6tBnzU5EtvLaXqOjQwnCf7o5Qt60VYbrzwhp0D2WGpAotCC7dgX8ElmM7805MBabrad6uL0AdrlE4hdh86DyIylxxFXu3EkvpMqU0fOo+rbmdEOx+8CGv6JQpLVjK/OgcCKU7EomR06cPngQzgDJG0Xp0dC2UODKvSJWzXM7HtDb+8CL/6TDrbn1z1CZaBOxSWbzRHntM088AhDPAyO/paxhUUdeDKbmHGpLuGcV26+2Tb7KOoTmaIMFvyUTNoYl8/7Lmc7dmyBbeGjpyL6cISNOKHMwDMEnZcWsJFM57/Jha/C15wcTMzLiyCFLZvuIQFjhIrTqyYVllTLxU/2GiLe9kTPtWKMtGa20d9u13og5ewCHO0nBsxhaPCmQ3fIUAfM8UntcK4l4+YgyqyQpuZt4JdtUi2YcjI9gwTrnIHKy8Ns3G+FrBwq0vqnQYzi3Gn3sC4brc9DfHxZvOKOx+NNyzSbpWFGUxduIVlzIF9OizAErrwF1w04+3frS0xjfNmKyzILHh1tA9mau9lpR/GmZXhU620JDOr1wxe0q82YQSjnIGtLeKTWjbu5SPmYNCLmbdaYQd8vC0wkS1XYKQzelmex6W1KL7Nuym7NYzr0r2XHtYwM+ISVHBFcQurLX6I73P1mtVPLAj4md6+q45+x8ZF/9xt/AbfkdEfKMuiaT7poYdNKqoT4u0oJj20dVKdCIlUJ8Ij1YnwONVVjyaEeDvc0YRUJ0LiVJfGu22pToSiKDv1erS3ddyV6kQwyt72ZnT76W7k14V4c4rx6OY2Go3cPSZChABiGxURFCfRiWBAbGUZ2e2SQoSiKPKRU51aOxEKdLF5tHDblRBvjamON/ZJdSIUvPEri9DS6RhWBIOqQw/rFoQIAsVWRKPhSD2sCEY+LoaD6Ob6bjR9zk6It6YoRp+uo52tk64PECIA3Z2tqJHuzR7AF+Lt6SQxn4ftqIcVwSg79ZTPw3YnuigmgtGpNaI02euqrROhKMpunTNOaFwnwpFPelBdnOoOdhEOPZkowgPVacYJERipToRHPawIj1QnwiPVifBIdSI8Up0Ij1QnwiPVifBIdSI8Up0Ij1QnwiPVifBIdSI8TnWal1iEJIfqUqlOBMWpzn4zUYhAFGVnrRnFNT2tI8JRlL1aPVpPDrqa50SEohh3G3F0eXbd06ROIhRF0W+dRvbj3FKdCEQxfi5G0QSfEp0IRp5NikhzJoqgFEVhM8RKdyIY0FpWRKNxoXmJRTDQ1BVlZA2dZCdCgTauiEq8qbETwbBxHT5HmVQnQlGgkcO4blyqhxWh4IhuEmXjPFcPK0IB1Y2eo/29s76u/otQ5OVgdztaT/ee1MOKUOSTXlyPkmS3rbZOhILzEidRLdHM/yIcbjbserrX9gFCvDnoYTVXpwiMnkwU4ZHqRHikOhEeqU6ER6oT4ZHqRHikOhEeqU6ER6oT4ZHqRHikOhEeqU6ER6oT4ZHqRHikOhEezhAr1YmwONXVpToRkKLsmup2dQe7CEiHd7AnO3pGTISj7NSSqFHf6U5yHyLEW1N26vVoM93XzP8iIN31NGqdXvYlOhGKohicnUTd3iDzAUK8OUVRdHtRlmvSRBGOYlxkeQTt5TqYEMFAI5dHfNck7CIU1spFaPH0MyciGJwGe+xUJ0QgTG8RZ2IXIhTQ3CTTLzqJoGBcNxlF9ulDhHhz8qLMo/ZTN9PRhAgFxnXtx+jo4Lwv1YlQFOPB0UGUxnsdHyBEADpxGsX1nbbuJRahKMruWqrnJkRQ9LSOCI+eTBThkepEeKQ6ER6pToRHqhPhkepEeKQ6ER6pToRHqhPhkepEeKQ6ER6pToRHqhPhkepEeKQ6ER6nujjd0wyxIhj5pLfWiNJkt6NZE0UoirIT16OUs2FLdSIYnaTmelipTgSiKLv1OKolmpdYhMM9I5Y29p7U1olQYFy31ozS+o6OJkQwikm/Fkdb6X5HbZ0IRVE8NtKo0+7kUp0IRTEuHh+iSVlqCjsRjKIoS86aWBaag12EogBQHfpXqU6EgnOwj6MJxaceVoQiRxcblXnGIZ4QYWBjF9moTqoToWDPGuVSnQiI+5UT+7kJqU4EAv0r2rrhMJPmRDiKfDiMbi7vR5KdCMfo00W01Tjs+lUh3pyi7G4kUZzsag52EQw+N5FKdSIoTnV6MlGERM/DivBIdSI8Up0Ij1QnwiPVifBIdSI8Up0Ij1QnwiPVifBIdSI8Up0Ij1QnwiPVifA41cWcq1O8gu6y/tk41aUJ2jpV7irs4XS8uefU+ZTJ2GqqRN/A7oHx9vCJ+H7c/XWNWDPEvgKlxs+cwipyKC3rdR4f7h8eHx87/bws+UBxbpIU30tRdmtQXaIe9hVcEzcVVZF3765bp0cH+/uHh0enreuH7iA3ZUp5f4hOHUcTnPnfr4sF8jwvi8wauiIftm9Ot9fX00aa4tVYbzS2T64eh0VJM/Wz30/ZidOoFkt1r0Ap5WzO8nzUvthbb6RJHCcgTur1OI7T5t5F254mnr7E71O247rOnLwK2zjXd+bD691mGtfjNEFLB9mhwaMA6/HG4XUXfSylJ9V9H2UnidXDvopTEnvZp5PtBho4SC5JG1QclMe2Lq4ljc2zh4wjO6nuOym7SS1q1PVk4mr8ccI4vz9omuLY1rF7hd7Q1CEIK2nSOLgb6WDiD9BOa9HRzse+ztetBP0rWrrRzV4KnXE854Tmlqi8BBFx3Ni7GfkU0352rj6lx0WKcX9vN3q8fcrUO6zEus3sdjeN2dLZkG76AhYEDab1ePd2aMewTmH2zvEgPl0A34QjH+ef7yIbMPsQsQDqpcju91275vW2DIUXJ7ufhzTm8M4Exxc7Zx4FOxUKD2ojzyO/IqpwVPdw1ISqvMRWYK1eEh88cVJxO3NHwWV8Qw1jXV/qFUh1rwPF9E/TOg8ZvMaqMDJO643zIb7FeTlhIrZwuU33zJmf8eH9iSlS3eugzbrasAOHb6uuhuitK8guM5FZl0rVlZQf1/km5pDqXgViedpNGw10sBjYvQZi7SzK4RP6VQhu2Lm/74zGk9I6W2v6vD8xQ6p7laLIPjY5cnPHr6vhuK7BBm/jYljm3euzo/29/YPj1p3vYHEgLNlVkOpeJ3/cThs8Vfetps61dvg8eBjeHm25w4u0uddqZ26Mpx62ilT3Ollrnb2nna7zcMmtOXXZy3WyG6fn2zyL4tbT9PBuRNVJckuwQqIs08hjEVYHa6To7zXjetp4OZbggumM/5SdW8EHR3ZNjgCnRgja+zwyX2rr5sFQN8ujm9vHl+s5AvCcB38yN7vZ9BKbYlfBkgYavyRZ39xo2kJq7ZsT3jxJsnfPn5BRc7dAUWaf7qKt9EBzsC+QYyhG1Y1a6ytUx5tO4iReP+v2zhr1pB5j1alxWXVx8+hpYre5e8cCFGV3PYkaNf3u/wJ2voNHAf1D09kcJq06WrZ682L0XPTP0jrCXPCS5mBcj5sXPJyAiMULZTv+EDWTne5EFfMCvoGQXJ4X7e2KlNCXxjW0YuutYVmMyp5dueBhLF/eZgrviNp/0qh5GburM46lukXQ0LG1y+42KKZ5KMI4adSbrcGEl1rH3aO4TmlyULfUv1ro5mWmMyfLdOI4qvFeYtXLHOwSUSGjTxvLTR1W+d887U0w8itx1PF4CMHxcGJZoFhHTOOIh7Gq3TmKslOL+dyEnkxcoOBlhaIcXdsh7AI8Xxynp70yz+x3ddENO9lZ2zYP1iG6ZKdvIhYz8km3lkRpXU/reHybZJcT8rFd+Z9qyL0gpSRpHne/jIe8la5EiuJxHzrkiTpvMwOdbiPefEBP7NwKA23dWhw1Y6nOM6e6PHtNdfFRv8zzcpwV/Mch6sNejYexq1QXJ9uf1cEuUuAYdj1KOM+JD3nPQBw8tQaJoEtEK1YMr+x0HTA9oW9N4kZaPx5wpAa9WRL8Z/f7GNjFKZ8im7uOgaWkXt+5y3g8IWZwxgk3p5NURwnZvcB25TQf8Vjh0/QYloJLUjs5nJx22LdSnJaIz1aMbvcQQYXaRQpLQrCytvmQQ8vOWBD9et0ivAeTyoOmsnE5vD1AW2caYscKXfGg9KA7gQFtfBqKdHS762RnRlPZsfmLt7t052wF0ayJS1inyaYOfW3nfMfUZgLimREO3tL9x7zEaA7y9N0mh4BFOfy0DcGhZbRLspaEGsTSER+VlermkOqWsKtXTncPR00+cuh6WMonjhtJevAwzqZP5EyT8BaK8fByK4a1tXYz1aVxff08o0NnK4hUtwhFhxcPTm93TXBTBeFIAUvJ7r0/SVfylJ6jKMoyy8vBxaYds+KQwimVDWQS791lOl+3iFS3iDVh0EiZf9pE32rzSzgB2cnheO8eNjy6ta7YJ4KoeFtJOTjfqJvsfCL2zknjrG/dsXhBqlsEkqPovhRXO426Ozp4UV2S7HwuJyMebpQ28POJxtmomKAFLPutDTvksGaRqsNSenDX082Li0h1i1hTh97zYR+aYUfJaZucgpKktn3HWzT5/A1PwM0aMKZxSbutZo2m7nACC7wLqrl7cjdtGdXmEaluEaoHr95J058AoYQIT4Hs3Bb+WUMbqM0UxHX2y3jrnjU5mrMkwLrletLYbnWm55+lO6luGSojL4YY1Pkmzt4B1Ne4zp8LXlJdOIIFrtGzoOLp0InVJzPQaDaP73lMgU5ZqpPqKuRZXmSPB9Mm7oW4ttnBMQMVRnXNyw4qRO3xPoDxeNjiVbFF1WEtqe98hmB5Y5SQ6paBrIr+eaO+LDqM8jbv+sPRaDgaDgYjzryOTtYlKUb9Qb8/GPQHw+HTEY8lllOnaS3Zvx3ZvQI+1XtGqlsCUsof9+tLfaQb1+0ef2y1Phpnj3YfsVEM7xncOm99bH3c4xVb62TnwXpS376j5KQ6qa4CVDe43IByKqpLa3HaxOiu0eT5uEv/nCsouqe0QAyieLbFHYfMw/MptfiwU84u3r5rpLolinHZPnq5qDWD6whFz8vzdvGHixFP2Tl6pzWKDa86n6Cojut4bRbJ0rOBNEec6uJEdzrNKO527Sr+EjwdQuXwKlea1C55cdUn6J5QdRCjSY4C9GlmxPWUDzNu3rgU7x3e6YS2TjP/zyhG11vWYnnBeCwEqmM7hlf9asR20cg7xzW0ZmwL2aqZYBdTMzGCk/Rw4JK8c3hXZxJtrR92pTpPr7XBdm1ZdU47VBybvXnVoa2ruzPDlszO9C2mZkNoyTc/86SL/bu075XuRiO6OPs01DDXUXZO/GXUV4B4Uqju5Qkcqo6BL4kqyZkEukyap7yIwdMz0zHh+6QYXpxEw0y/0TEFBxP+usSrONV5e6oOPew3hUqs5012+jyKRWW/c9VxTqeR3VXmQ9455dNB9VBikR9SnR2gJM0HqQ6wjYPqNNKYUj7uVS5MLFFRHcZ1v6c6jveSRtL4xLPLNrJ7z/DCYsRbymZnn945UJ0dGXyDH1IdZZemjZafM/Zdqw7funwc+UkCBSjbB7/fbv1x1dnBBDhBunff2mHT+ds6/O6phzXKzrFdSv0GEFhtXnU8X/dt1SGW5+twRHGA0Qy+30X5rh+lwNfOftHpfTf5c5Td02+fOXFt3eVoVmG5na/7plJjNIa8ZpEk+5l9w/P3rTpUAds6drTCsIcfvFpWwvEZVDfrHIreKWcE+FZzhySUMj44rRj7l/fdw1JvbOukuinD661vH06wWavzOqxP8D2qQxRVV2+cSnWmuryI7j8/vnQY7xvUx/2ee4B/NdQPeswr9JE+ybh/inEdVPVNrSISThuXQ7uF733XdlGMbm+ig51T/Ra2A1/D7okJxKtlGTZZST0+H75cuO6e4EiB/e6rsmMi10je8Uoa/33ad0pvdytqciYxqc7Iy9H11rfaLeoxiY8fh4P+wPF53w4UTFmrYaTdAtXsoqXj6dH3PaApO2kcJfGuZoj1YMT1dGjN0iuYIOubZ1dXF1fG5dG6mX9zNBinnEcg3udIRuepbDbsumZN9LD3KwcXOIp9vd2i7urx+tbW5pax2bSG7vdUh+h645qCe/e9SlF265wNW/cSO6AJ9H73h/zhzVeAevBX42Grh42YdaLfkB1HfUmy26V7drLvWntOdalUN4OXq4aftt3wzaTCdsz9G1QXWi4T0hQaU1d+tYI5iZP1Fm/tod7evepqnKtTqptC1RWds00O1ai12A5n3b9XEP//qOpgEfO3sqU64FQX6xmxGdDcuCyejjkNtm/n2IMud58M94uAsSslBwfuJAzi6zt37/w83RT/tI5UN8MaozK/P25i5OYnE6N4nIr+EE6ybvgH0W1eD+nfZfOu0fOwS7Cp43A/uztq8nkvuwXYdOeU9EcwuQEsxEl9/bxP51KdVFfB7uXn6Y3h3THPn7hrDtbk/QDWM9ugL9k470J07/vk8BSpbgkIw53DLUbt8+3Eulg2VT8kOtfacWDX2L5GS2fHEj6j94xUt4i7+c2apCLr3RxwThP0sd84ffdNTK0QXnp0N6TjzE039t6R6hYpONeXVx1E0rs5arrjgR9VXZrW6+tHV/w1nhLO+YSYkOrmwcCLvaBXHVdH7c8XR7ubaa1W/xFqcXP7oHXbHro7TYDL6J0j1c1DUZg0KA7KBO+jfvv+9vry4vJHuLj8dPvYGzlXkLIOJgyp7vdBT5tlI7z+OEw1OzNMDfvFd45UJ8Ij1YnwSHUiPFKdCI9UJ8Ij1YnwTFWneYlFOHKoLo3iZE/PiIlgFGV3rRHFdfSwOoMpAlGUnQ9p1IgPei8TKAjxtuRlL46js5ProVQnQpGXw9OjaNAfFOphRSiKcT7oRSVv7VFbJ0KR51+KCO/u7h4hQsDfZ+a8xLP7cYR4a6C1PIvw/pPvcnXu/rBPJvAlmaadFQyf08U34ydl8Obl/CcH9ZOPo4x3bfuQnwKc2d3a/vWdvl9SvaSdrVqAW3gDmIfLywf8CExvrvybeA30rSWOJn5yFwtffAqP40V45vL37AUOLjMc1LjCcI2JUTgTBZy92cTl/F0rlhV5/YlqQGKUzx7J4SbQ05/w9l8Y1lMG1ZVY8kE/A5MLPrK8yKEjiuY73FNrpjhas/XljCMZ3nCMjZAss6fy3wKWF/nl/LklH/QD0Am3wgRMfwhyMWIBVHKZc1znvpo/C3ilbOAUmGt77OrbmG1pCbmGMvlnDtj+URBvN3G5L+cY374f17WVGrgFFBbFf6Py/pNjlR11egNXVz8Lq3x+uH6Sufz+3rTWxrUS7sVklC93JHo/lPA7tPtjQCN8HpF5+pA/jisxcE0eXlj2cWIe1Eq3H50eXwx+6rUJ1j30AvfcESao398B2Gl4sYW0D1MYly1ubO7eai/CLyVv6vZBfxykRWrbApaexX+r8v6TU4wHJ8dRo7b9c+85YROFmrdlfmB/OvX8Dks2XGVK+3zLPQjXLC7HkT+eiyul22z31TGXokIxfqr/JUrj3e7Pbev864X/72sfo7CJHfr8CXgozDae01ZQfwxyMWKeouzVa1Ha3Hv6yfXzT1jbxXD0586ceF48/ARn/yUpyk6tGa3xuQmpzsYBf77gLx6kutVwXuKEd7DrXmLKhbLzK+Lt4LzEnINdz004pLkQ6MlEER6pToRHqhPhkepEeKQ6ER6pToRHqhPhkepEeKQ6ER6pToRHqhPhkepEeKQ6EZ63Up09QiDESrzqeH+dD/kpUHKSnXiFaVu3+3Pvr5PqxDeA6jgvcbrzk+8lVg8rXodzsEN1yc9WnRCvU5Tdeho1kp/cw34HFLmE/l5px3HUiHfboc+cSHXvmLJTr0UnJ5d9vx4Me1DZPQzoQ8T7oX90wtl1Mr8aDGjNzWZjx7ucoMFHiP/6FFmnH+Xln5nf48dAhmzuuIC3LOcUXhLe+4A7fhJxx/+pCT5+hNyaOlvgDGKUnFT3PmAHV0Q4kgje1kFypjU3vjPNSXTvBez4LMKgzqZbC41Jzi9Lde8ITlAp1YmwcEZM/cqJCAzUFhU55/nzAUK8MRBbjqOJCbo3qU4Egj8iUkadTmck0YlQFEX+2I4Otk96uudEhKO7uxM1413d6STCUfKek1iqE0Fpx2lUTzRDrAgH7+rkHex6MlGEw93BrudhRUjc0zpSnQjJWz2FLcTrSHUiPFKdCI9UJ8Ij1YnwSHUiPFKdCI9UJ8Ij1YnwSHUiPFKdCI9UJ8Ij1YnwONXFP3s2bCG+wbStCz9Xp3i/5JPeWup6WD03IQJRlN1aih52vyvViXB06s0oaey3pToRjieqLtlXDyuCUZT9WhzF9Z0nza4jQlGMO2tr0eHReV9tnQhFMR4cHUTt3kAziYlgQGq9XjQux3nwqf/F+yXPx2XEaWLV1IlgFEUxjEYL8wML8cZAdXmUuZ9WEiIMpjpOE6vGTgQDYsujUqoTIYHYMvttnf8Xvzch3in8scKoLEsdTohgUGtl1O+Pxro2IUKBlq47iM7PPg11bUKEohgPzlrRZrzf9QFCvD1Fp5FECedg9wFCvDW8lzjhbNhtqU6Egs9N6MlEERY9DyvCI9WJ8Eh1IjxSnQiPVCfCI9WJ8Eh1IjxSnQiPVCfCI9WJ8Eh1IjxSnQiPVCfC41THO518gBBvzvROp13NXyeC4e7qbCQ7Up0ISKdej9Jkt+NXhXhzirJTi6Mk2ZPqRDDcHOx1zsHuQ4R4a3g00bTf1vEBQrw5aOuguiTe1e9NiGBQdXHU1K+ciIBAdUk9Ot5v9SU6EY7+wW7U7fQ0GbYIR5F321GhORNFSCi4qNSETiI0VJ0aOxGWiB2tZogV4UAjFxWchN2vC/H2UHV4t38hQmBqwzEs+ld1sSIQ1rdGeZbpcEKEo8izUZSP8lwDOxEMNHOj6PT4YqBxnQgFf5X4OFpv6nf/RTiKspc2eAe7ZsMW4Sg79Zj31+nJRBGOsm1P6+yphxXhKDtxLNWJsJRdqU6Exqku1owTIiDWw/J5WB3DinC04zpn15HqRDCKsltPNJOYCIp+vU6ER7MmivBIdSI8Up0Ij1QnwiPVifBIdSI8Up0Ij1QnwiPVifBIdSI8Up0Ij1QnwiPVifA41XHmf6lOhAKqW0uhOj0PK8KRT7pQXdrY62jGCRGKouzVm9H9zUMm1YlQFMXw7iYqRpnmrxPBKMblaBBNNB22CIhpLaLmJDoRjDwflxGnTJTqRCiskYswpOPMxEIEgXor7Bed1NaJUHA2bM7BLtWJcEB19nsTQoQEbVyUjTQFuwhIUWRZdHfzMJLqRDiyzzfR/vZpT1f/RTDK/s5OlNR2NVenCEZRdtbSqFbfk+pEMKC6qBnVeC+xVCcCAdV9WI9qjX3NSywC0l5rRPVU4zoREJsNO052pDoRkE5cj5JUv+gkwqHZsEV4NBu2CI+ewhbhkepEeKQ6ER6pToRHqhPhkepEeKQ6ER6pToRHqhPhkepEeKQ6ER6pToRHqhPhcaqLpToREH+nk2bDFgEpym4tjRp1qU6Eoyg7tXq0mR739NyECEe32Yg+XT9mmg1bhKIos+tP0SjLNamTCEYxzod5VOBDqhPByDPOmsjZsKU6EYocfWxU5lku1YlQFEVeoIfN8lJT/4tQsIXLo4yTYUt1IhBuNuzShndCBKLIMjuG1dT/Ihj2QxNRVk6srxUiBPl4Mn6O2r3+SJcmRCjQtT51o53mUVdHEyIURfG0tRE1kn1d/RfBKMpevBYlyV5bbZ0IRVF26mlUT6E6IQLBuzobUdzY7kz+UFu3ZPx62t/3Om/xXQfS32PzOtPUdvT+Df5cLgvQ1bfczYrkP18B0c7id+z+IKu8zYW9lpmF+8hv1aS3W7Dgr5w0ozjdWVSdM8X7gvGUIrcDXu43O8vMf4t4gYG+LPSy0hUtFiK44NxVcbbeeqWNTz31sMrCHJgTH1u18Qlp6sy95QsWZUl9jBkxdPm2HTOz9C6N+atgVrSzSP+xgDn3ae1zpc30nZ5WGDCO2XhL4oxdqnls1XlhbL7KHSP4x0/GrrBxHszUW77QWUNbx3HdjKkXamvV+RTEZD6+yPDBsGU7u4kFfuzfyr1cKsYwajzOpgG0oNNqps4X8+Ku5ceyO2bpYnIsrL6HxkUwrU++OicWBMWwjGwrF2BKXzM+xvJyuS7m6fKBFZ3QHd8q5EWJlEhrfir5AStGYZfKUWL4qNpYVn67zUs1I/PPG9qw4OAmIHc6XcAMEJNn5gdZrigT88hL84dYl6uP81jcuESBizKzwr8YdGsJVLc7dzRBa74RLi5D/3mWWRTKQ8dz/hwMMAdmbP58zBQzQPgsxgKYbXXfTAsMb6gJ+FvYAo+lZQQdrio3A121OwerROAi6Mh5ybPlgvukiOO7C6JPfAEZNw8saGj5TfFRL3CfWXou+oUlEIhSIIL33iIj1oGP8lgJEMsPrELGTLUA/butmiXGglteMrVdwsLCn10pZe4u5gVEIs4KRLuZrxkMsjC++JVBobyFe1qnVt/tP8/SQFGu+eHVMjq1lTnyERq7kobOK5Z8zAykc5WAAnMb3NsirrAz//QEBeO1/N2zHEYsuDkrXZ34uBlwN0KBXByzW7aAAdJmiEHJkI+zWoQ5oVgwxCKqdcLyLGB7jmlH00vXtqkl9vSyGLhtVgnFhALG/7IzkBcTaobbhiqlQcUG+bHU3CklNh5Nx7If5GK1Zsn546tVNyjFCNU7GbvmgrjNX7IjZsFYlt82uGLFMmcThiMzbDy/DD7Kwwpm64TGlH5QObZ3CJ9MTKNGc3/uV05YTfxzFcbESzBD+IEBc0OZqINFWFhUEZ24jasoiUHw8uKfistG8G3f1AVoM+F+RYaIQ4aVeocR+gPuEvMIL9NNnIF17hDsPhaO8qsWihGws3C765AhC5gFE84yoF9TxLKt1TV6GH470cVYjfqoF9jBWj7cebZZSxkiAFuNwIxfc1upqNfVmgW6isfbkgkCEMVLULPErjRLdsQ8MZZJ+I6qcjEzIP3xiKHsZK1KliuTuwF1Al/WUcN+9q2E6ngMG889mYjULjOTDnHhL2CzbGDHjKADZuBjpjCA0dPNX/H15CrDXFYWQKkwmcUtwLAXRaFcy80KQIjVDvJ0X/VlC6a08qBgNJzQ2QJYp0iYmgrBG619pAch2HhrunwM3FBw1aYMqxQUGmlb5CYw/SJUnfkwvaN8S06AZWWKxUe2yoTN/PQrwqK7pQX8xkxYjlmQX1iC24M3xJrwbLOq7uCQO4//VrKVJiwal6xWuWjkk+6HNKrX93vPjglfrEQmYO7Pv3yxwDkmz1/KZzQ+8FJCx5NJ+WXZAl/byTMaYKdlKgbrC9gq3hDOvMxszKCy9JFzsBVAp843OEWRJpNlExjx+87njlCsSTlBsZdtJs+/oEDsymBUPhfIaQF4ZRokZpnZGFbLzQKySuBlVmwrMqphyZTOuNPYq03G5bOVqOIO/rhbXEtJT1UT1iaLjL2SjZ8LOvJRDrpFiO03971h9S9i9TH58gWZTMvt/Cwb0p1JylQDW4ZU3SGzEk2naQ6lrjqCSr5YY4eczM/8fu0mjShNN8+uZlzftKddX97+5APnueT/px6LZXXxdHWNoAUuEXB9dfUw5BbyW9a/9TFzXNLq8voeboy8d8OQFVze9q0S8C3M7q+uPsF1hcuruyGMuGuK/t0qC8vwyb68+B8PKqV2XF/d9p0bDIUeVji6ZNgFjHyxR49wzE1x0XNcdlHn2H/Q+P319coyXV3d9C0rZDn4vMIHuLi8fsTOZZHy8XCVGwu77Tgb9GmPq4qN3XR5eWP9ooFKeqUGHvHlZV7jontrvit2cPbgjmDx9eys3CGwubRdYpU5urc6cly36glUV19bqxlrax/Wmi2rULz6rcbaXz74qBlr4EPzBvGmztHHWeopXIdR/bjNRpXfm4fNWsXmAzyvfahtWxWA7LbBoFo1ww9bt2jOMSYry84xcoPZsrfah/ruozWuqIzPO7GVYREk+0v9nMMj1Na4+Ly+oth0vrZxwy8TTIreERz5WAfWkBc2bvPGNeTjon3EEjH1kunah6uR6zeK4jCGYwa6uCmopI07Dtewn7MHK/aSBd2sJUc4FGBe4+KugfWKlw+okvWrIbpf1lLvLK2YfPjLWh1vm21zBCa7SFcpEFhLPvKLwGJnnzbpvWrFncuxGr9Tg4uUFhWTv3yo7T2Y5tA4dQ4+zJS0Vk8+1KM4aaRpw0gbzXTjfMhSQTD983WE+6gFahufTU0cKl2kiQ+dkcJfmqQnHW4ivqb57fayG+YIo7S5z8xIdofckHJalhfS3Vv7imILuqdpus4gFzOFq3tP9sVCl/awZ86XSJJm2jwfMSv2jvebScWEpMk2NI6qQq0OD6t+UGQUM9669aobd07itOmi7P2F+DbPkB9qsjxmZLWiELr9wP3Ckj/uNprNan78P+HwASYo9opdgr3WbCRbn0bMCuXutTYqXppNbG4z2X4yE1AeNbEhlewQsn7WQ4H4vcuvNrFpK/YIws4wRmQrVoyumnUG+JgZzUa8/whH+MvHvcO4wZIbsI2jZn1j9+DQs79/+tn2DKwHdx8P9mcxc+zvt55wyI/j4qIc3R7t+eApB4dHR7A5/jSg6jDWyHotBC6DkIPDkwtmRkZPZ7Q5qlgeHLbaBQ6USwzNBzeW2ypnVz3IG63dpGhfHB9Wy30ATj/jaNKGWeP22QoT5n7w8dGdpSjL0c3xclb0cnC4f9h6cqXGd/Mame0fHiF4gYODo7sRv+gc2V0jYP+YOSyC3HrUCpqoonN5tKq2EXRyO0KRJxiw5U8f91eVG68WBjSs7/F4eHtayciMDg4uus4EXGAnVa1gdHyPY12Oa8v8sbVaANiW2+zZWhQMelCTFSNU5f7hNXYJKgDViUram9kc7Ca1/wvFwEusTD5E0QAAAABJRU5ErkJggg==';
    };
    return AeviGlobal;
}());
function aeviIsUndefinedOrNull(val) {
    return _.isUndefined(val) || _.isNull(val);
}
function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    document.body.appendChild(outer);
    var widthNoScroll = outer.offsetWidth;
    outer.style.overflow = "scroll";
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);
    var widthWithScroll = inner.offsetWidth;
    outer.parentNode.removeChild(outer);
    return widthNoScroll - widthWithScroll;
}
String.prototype.makeClassFromSelector = function () {
    return this.replace('.', '').replace('#', '').replace(/ /g, '');
};
String.prototype.makeClass = function () {
    return this.replace('.', '').replace('#', '').replace(/ /g, '');
};
String.prototype.replaceAll = function (find, replace) {
    var str = this;
    return str.replace(new RegExp(find, 'g'), replace);
};
function aeviGetNumberWithDecimalPlaceCount(num, decimalPlaceCount) {
    return (Math.round(num * 100) / 100).toFixed(decimalPlaceCount);
}
function isCellInViewport(cellEl, bodyEl) {
    var cell = getOffset(cellEl);
    var body = getOffset(bodyEl);
    var isInViewportVertically = (cell.top >= body.top && cell.bottom <= body.bottom) ? true : false;
    if (!isInViewportVertically)
        return false;
    var isInViewportHorizontally = (cell.left >= body.left && cell.right <= body.right) ? true : false;
    if (isInViewportHorizontally)
        return true;
    return false;
}
function getOffset(element) {
    return element.getBoundingClientRect();
}
function findWithAttr(array, attr, value) {
    for (var i = 0; i < array.length; i += 1) {
        if (array[i][attr] === value) {
            return i;
        }
    }
}
Array.prototype.aeviClean = function (deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};
Array.prototype.getClosest = function (target) {
    var tuples = _.map(this, function (val) {
        return [val, Math.abs(val - target)];
    });
    return _.reduce(tuples, function (memo, val) {
        return (memo[1] < val[1]) ? memo : val;
    }, [-1, 999])[0];
};
Array.prototype.getClosestIndex = function (target) {
    var i = 0, j = this.length - 1, k;
    while (i <= j) {
        k = Math.floor((i + j) / 2);
        if (target === this[k] || Math.abs(i - j) <= 1) {
            return k;
        }
        else if (target < this[k]) {
            j = k - 1;
        }
        else {
            i = k + 1;
        }
    }
    return -1;
};
function removeArrayItem(arr, index) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}
function aeviFormatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
}
HTMLElement.prototype.setAttributes = function (attrs) {
    for (var key in attrs) {
        if (!_.isNull(attrs[key]))
            this.setAttribute(key, attrs[key]);
    }
};
$.fn.textWidth = function (text, font) {
    if (!$.fn.textWidth.fakeEl)
        $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
    $.fn.textWidth.fakeEl.text(text || this.val() || this.text()).css('font', font || this.css('font'));
    return $.fn.textWidth.fakeEl.width();
};
function setSelectionRange(input, selectionStart, selectionEnd) {
    if (input.setSelectionRange) {
        input.focus();
        input.setSelectionRange(selectionStart, selectionEnd);
    }
    else if (input.createTextRange) {
        var range = input.createTextRange();
        range.collapse(true);
        range.moveEnd('character', selectionEnd);
        range.moveStart('character', selectionStart);
        range.select();
    }
}
var AeviAjaxPreloader = (function () {
    function AeviAjaxPreloader(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.render();
    }
    AeviAjaxPreloader.prototype.render = function () {
        var aeviWrapperElement = document.querySelector(this.AeviGrid.AeviConsts.aeviWrapperSelector);
        var preloaderHTML = '<i class="aeviAjaxPreloader"></i>';
        this.AeviGrid.AeviDOM.append(aeviWrapperElement, preloaderHTML);
        this.loader = document.querySelector('.aeviAjaxPreloader');
    };
    AeviAjaxPreloader.prototype.listen = function () {
        var _this = this;
        $(document)
            .ajaxStart(function () { _this.show(); })
            .ajaxStop(function () { _this.hide(); });
    };
    AeviAjaxPreloader.prototype.show = function () {
        this.loader.classList.add('visible');
    };
    AeviAjaxPreloader.prototype.hide = function () {
        this.loader.classList.remove('visible');
    };
    return AeviAjaxPreloader;
}());
var AeviHelpHandler = (function () {
    function AeviHelpHandler(grid, help) {
        this.AeviGrid = grid;
        this.AeviHelp = help;
        this.listener();
    }
    AeviHelpHandler.prototype.listener = function () {
        var _this = this;
        $(document).on('click', '.aeviHelp__close', function (event) {
            event.preventDefault();
            _this.AeviHelp.hide();
        });
    };
    return AeviHelpHandler;
}());
var AeviHelp = (function () {
    function AeviHelp(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.selector = this.AeviGrid.AeviConsts.helperSelector;
    }
    AeviHelp.prototype.render = function () {
        var className = this.selector.makeClassFromSelector();
        var aeviWrapperElement = $(this.AeviGrid.AeviConsts.aeviWrapperSelector);
        var aeviWrapperWidth = aeviWrapperElement.outerWidth();
        var html = '<div class="' + className + '" style="width: ' + aeviWrapperWidth + 'px;">' +
            '<p class="title">' + this.AeviGrid.AeviLocalization.translate('tooltip_help') + '</p>' +
            '<p>' + this.AeviGrid.AeviDataRepository.AeviDataEntity.data.Help + '</p>' +
            '<span class="aeviHelp__close" title="' + this.AeviGrid.AeviLocalization.translate('image_close') + '"></span>' +
            '</div>';
        aeviWrapperElement.parent().prepend(html);
        this.element = $(this.selector);
        var handler = new AeviHelpHandler(this.AeviGrid, this);
    };
    AeviHelp.prototype.show = function () {
        if (aeviIsUndefinedOrNull(this.element))
            this.render();
        this.element.slideDown(300);
    };
    AeviHelp.prototype.hide = function () {
        this.element.slideUp(300);
    };
    return AeviHelp;
}());
var AeviSearchHandler = (function () {
    function AeviSearchHandler(aeviGrid, aeviSearch) {
        this.AeviGrid = aeviGrid;
        this.AeviSearch = aeviSearch;
        this.AeviConsts = this.AeviGrid.AeviConsts;
        this.setSearchAsDraggable();
        this.listener();
    }
    AeviSearchHandler.prototype.setSearchAsDraggable = function () {
        $('.' + this.AeviSearch.aeviSearch).draggable({
            start: function (event) { }
        });
    };
    AeviSearchHandler.prototype.focusField = function () {
        $(this.AeviConsts.search.selector.field).focus();
    };
    AeviSearchHandler.prototype.listener = function () {
        var _this = this;
        $(document).off(AeviConsts.clickEvent, this.AeviConsts.search.selector.find).on('click', this.AeviConsts.search.selector.find, function (event) {
            event.preventDefault();
            _this.AeviSearch.find(AeviSearchHandler.getKeyWord(_this.AeviConsts), AeviSearchHandler.getDirection(_this.AeviConsts));
        });
        $(document).on(AeviConsts.clickEvent, this.AeviConsts.search.selector.cancel, function (event) {
            event.preventDefault();
            _this.AeviSearch.hide();
        });
        $(document).on(AeviConsts.keyDownEvent, this.AeviConsts.search.selector.field, function (event) {
            var code = event.keyCode || event.which;
            var codeKey = _this.AeviConsts.keys[code];
            if (codeKey === 'enter') {
                event.preventDefault();
                _this.AeviSearch.find(AeviSearchHandler.getKeyWord(_this.AeviConsts), AeviSearchHandler.getDirection(_this.AeviConsts));
            }
            if (codeKey === 'escape') {
                event.preventDefault();
                _this.AeviSearch.hide();
            }
        });
    };
    AeviSearchHandler.getKeyWord = function (consts) {
        var el = document.querySelector(consts.search.selector.field);
        return el.value;
    };
    AeviSearchHandler.getDirection = function (consts) {
        var el = document.querySelector(consts.search.selector.direction + ':checked');
        return el.value;
    };
    return AeviSearchHandler;
}());
var AeviSearch = (function () {
    function AeviSearch(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.isSearchRendered = false;
        this.isSearchVisible = false;
        this.aeviSearch = this.AeviGrid.tableId + '-aeviSearch';
        if (!this.isSearchRendered) {
            this.render();
            this.AeviSearchHandler = new AeviSearchHandler(this.AeviGrid, this);
        }
        this.positionOfFoundRecords = [];
        this.keyword = null;
        this.show();
    }
    AeviSearch.prototype.render = function () {
        var translator = this.AeviGrid.AeviLocalization;
        var template = _.template('<div class="<%= className %>">' +
            '<h3 class="aeviSearch__title"><%= title %></h3>' +
            '<p class="aeviSearch__subtitle"><%= subTitle %></p>' +
            '<input type="text" class="aeviSearch__field">' +
            '<div>' +
            '<p class="aeviSearch__direction"><%= direction %></p>' +
            '<label><input type="radio" name="direction" value="down" class="aeviSearch__radio" checked><%= down %></label>' +
            '<label><input type="radio" name="direction" value="up" class="aeviSearch__radio"><%= up %></label>' +
            '</div>' +
            '<div class="textright">' +
            '<input type="button" class="aeviSearch__find aeviButton aeviButton--secondary" value="<%= find %>">' +
            '<input type="button" class="aeviSearch__cancel aeviButton" value="<%= cancel %>">' +
            '</div>' +
            '<i class="aeviSearch__close"></i>' +
            '</div>');
        $('body').append(template({
            className: this.aeviSearch + ' aeviSearch aeviWsw',
            title: translator.translate('search_title'),
            subTitle: translator.translate('search_insert_keyword'),
            direction: translator.translate('search_direction'),
            up: translator.translate('search_up'),
            down: translator.translate('search_down'),
            find: translator.translate('search_find'),
            cancel: translator.translate('cancel')
        }));
        this.isSearchRendered = true;
    };
    AeviSearch.prototype.show = function () {
        $('.' + this.aeviSearch + ', .aeviSearchBlackBox').show('fade', 200);
        $('.' + this.aeviSearch).find('.aeviSearch__field').focus();
        if (this.AeviGrid.AeviClientSide.isIe())
            $('.aeviSearch__field').trigger('click');
        this.isSearchVisible = true;
    };
    AeviSearch.prototype.hide = function () {
        $('.' + this.aeviSearch + ', .aeviSearchBlackBox').hide('fade', 200);
        this.isSearchVisible = false;
    };
    AeviSearch.prototype.find = function (keyword, direction) {
        if (keyword === '' || aeviIsUndefinedOrNull(direction))
            return;
        if (keyword === this.keyword) {
            this.keyword = keyword;
            this.direction = direction;
            this.selectFoundCell();
            return;
        }
        var aeviDataRepository = this.AeviGrid.AeviDataService.AeviDataRepository;
        var data = aeviDataRepository.AeviTableData.getData();
        if (data.length < 1)
            return;
        this.cleanPositionOffFoundRecords();
        for (var rowIndex = 0; rowIndex < data.length; rowIndex++) {
            for (var columnIndex = 0; columnIndex < data[rowIndex].length; columnIndex++) {
                var dataValue = data[rowIndex][columnIndex];
                var displayValue = aeviDataRepository.AeviTableData.getDisplayValue(dataValue, columnIndex);
                var column = aeviDataRepository.AeviTableDescription.getColumnHeader(columnIndex);
                if (!aeviIsUndefinedOrNull(column))
                    keyword = this.getSearchKeyword(keyword, column);
                if (AeviSearch.isSearchKeywordInDisplayValue(displayValue, keyword)) {
                    this.addFoundPosition(rowIndex, columnIndex);
                }
            }
        }
        this.keyword = keyword;
        this.direction = direction;
        var foundRecordsPosition = this.getPositionOfFoundRecords();
        if (foundRecordsPosition.length < 1) {
            toastr.info(this.AeviGrid.AeviLocalization.translate('no_found'));
            this.AeviGrid.AeviStatusBar.info(this.AeviGrid.AeviLocalization.translate('no_found'));
            return;
        }
        else {
            toastr.info(this.AeviGrid.AeviLocalization.translate('found') + foundRecordsPosition.length + this.AeviGrid.AeviLocalization.translate('records'));
            this.AeviGrid.AeviStatusBar.info(this.AeviGrid.AeviLocalization.translate('found') + foundRecordsPosition.length + this.AeviGrid.AeviLocalization.translate('records'));
        }
        this.selectFoundCell();
    };
    AeviSearch.prototype.cleanPositionOffFoundRecords = function () {
        this.positionOfFoundRecords = [];
    };
    AeviSearch.isSearchKeywordInDisplayValue = function (displayValue, keyword) {
        return displayValue.toLowerCase().indexOf(keyword.toLowerCase()) !== -1;
    };
    AeviSearch.prototype.addFoundPosition = function (rowIndex, columnIndex) {
        this.positionOfFoundRecords.push({ rowId: rowIndex, cellId: columnIndex });
    };
    AeviSearch.prototype.getPositionOfFoundRecords = function () {
        return this.positionOfFoundRecords;
    };
    AeviSearch.prototype.getFoundCell = function () {
        this.currentCell = this.getCurrentCell();
        var selectedRecordsPosition = this.getSelectedFoundPositions();
        var closestPosition = this.getClosestPositions(selectedRecordsPosition);
        if (!aeviIsUndefinedOrNull(closestPosition))
            return closestPosition;
        return null;
    };
    AeviSearch.prototype.getClosestPositions = function (rows) {
        var position = {
            prevPosition: null,
            nextPosition: null
        };
        for (var i = 0; i < rows.length; i++) {
            var item = rows[i];
            if (item.selected) {
                position.prevPosition = (i === 0) ? rows[rows.length - 1].cellIndexes : rows[i - 1].cellIndexes;
                position.nextPosition = (i === rows.length - 1) ? rows[0].cellIndexes : rows[i + 1].cellIndexes;
            }
        }
        if (this.direction === 'down')
            return position.nextPosition;
        else if (this.direction === 'up')
            return position.prevPosition;
        return null;
    };
    AeviSearch.prototype.getCurrentCell = function () {
        var currentCell = null;
        var lastCell = this.AeviGrid.AeviGridHandler.selectedCell;
        if (aeviIsUndefinedOrNull(lastCell))
            lastCell = this.AeviGrid.AeviGridHandler.lastSelectedCell;
        if (aeviIsUndefinedOrNull(lastCell))
            currentCell = { rowId: -1, cellId: -1 };
        else
            currentCell = this.AeviGrid.AeviDOM.getCellIndexes(lastCell);
        return currentCell;
    };
    AeviSearch.prototype.getSelectedFoundPositions = function () {
        var foundRecordsPosition = this.getPositionOfFoundRecords();
        var selectedRecordsPosition = [];
        for (var i = 0; i < foundRecordsPosition.length; i++) {
            var row = foundRecordsPosition[i];
            if (this.currentCell.rowId === row.rowId && this.currentCell.cellId === row.cellId)
                continue;
            selectedRecordsPosition.push({
                selected: false,
                cellIndexes: row,
                position: parseFloat(row.rowId + '.' + row.cellId)
            });
        }
        selectedRecordsPosition.push({
            selected: true,
            cellIndexes: this.currentCell,
            position: parseFloat(this.currentCell.rowId + '.' + this.currentCell.cellId)
        });
        for (i = 0; i < selectedRecordsPosition.length; i++) {
            if (selectedRecordsPosition[i].cellIndexes == this.actualSelected)
                selectedRecordsPosition[i].selected = this.actualSelected;
        }
        selectedRecordsPosition = _.sortBy(selectedRecordsPosition, function (props) { return props.position; });
        return selectedRecordsPosition;
    };
    AeviSearch.prototype.selectFoundCell = function () {
        var _this = this;
        var cell = _.clone(this.getFoundCell());
        if (aeviIsUndefinedOrNull(cell))
            return;
        cell.click = true;
        this.AeviGrid.unSelectAllRowsAndCells();
        this.AeviGrid.AeviGridHandler.triggerClick();
        this.AeviGrid.AeviPager.moveTo(cell);
        setTimeout(function () {
            _this.AeviSearchHandler.focusField();
        }, 50);
    };
    AeviSearch.prototype.getSearchKeyword = function (keyword, column) {
        var displayType = this.AeviGrid.AeviDataRepository.AeviTableDescription.getDisplayType(column);
        var columnType = this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnStringDisplayType(displayType);
        switch (columnType) {
            case 'Currency':
            case 'Number':
                var decimalPlaces = AeviGlobal.getDecimalPlaces(keyword);
                if (decimalPlaces < 1 && !_.isNaN(decimalPlaces))
                    keyword = AeviGlobal.removeDecimalPlaces(keyword);
                return keyword;
            default:
                break;
        }
        return keyword;
    };
    return AeviSearch;
}());
var AeviStorage = (function () {
    function AeviStorage() {
        var storage = AeviStorage.getStorage();
        if (_.isNull(storage)) {
            storage = AeviStorage.getEmptyStorage();
            AeviStorage.setStorage(storage);
        }
        AeviStorage.listener();
    }
    AeviStorage.listener = function () {
        $(document).on('click', '.aevistorage', function (event) {
            var target = event.currentTarget;
            var nodeType = target.getAttribute('type');
            var storageKey = target.getAttribute('data-storageKey');
            if (nodeType === 'checkbox') {
                var isChecked = target.checked;
                AeviStorage.setStorageParam(storageKey, isChecked);
            }
        });
    };
    AeviStorage.getEmptyStorage = function () {
        return {};
    };
    AeviStorage.setStorage = function (storage) {
        localStorage.setItem(AeviStorage.storageKey, JSON.stringify(_.clone(storage)));
    };
    AeviStorage.getStorage = function () {
        var storage = localStorage.getItem(AeviStorage.storageKey);
        if (_.isNull(storage) || _.isUndefined(storage)) {
            return null;
        }
        return JSON.parse(storage);
    };
    AeviStorage.setStorageParam = function (key, value) {
        var storage = AeviStorage.getStorage();
        storage[key] = value;
        AeviStorage.setStorage(storage);
    };
    AeviStorage.getStorageParam = function (key) {
        var storage = AeviStorage.getStorage();
        if (_.isNull(storage)) {
            return null;
        }
        var storageKey = storage[key];
        if (_.isNull(storageKey) || _.isUndefined(storageKey)) {
            return null;
        }
        return storageKey;
    };
    AeviStorage.storageKey = 'AeviStorage';
    return AeviStorage;
}());
var AeviApiDataService = (function () {
    function AeviApiDataService(aeviGrid, apiAddress, username, password, slot, entity, lang, token, entityType) {
        this.AeviGrid = aeviGrid;
        this.apiAddress = (apiAddress[apiAddress.length - 1] === '/') ? apiAddress.substring(0, apiAddress.length - 1) : apiAddress;
        this.username = username;
        this.password = password;
        this.slot = slot;
        this.entity = entity;
        this.lang = lang;
        this.entityType = entityType;
        this.async = true;
        this.caption = null;
        this.activeAjaxConnection = 0;
        this.setDataChanged(false);
        this.setCellChanged(false);
        this.token = null;
        if (!aeviIsUndefinedOrNull(token)) {
            this.token = token;
        }
    }
    AeviApiDataService.prototype.getAccessToken = function () {
        var _this = this;
        return $.ajax({
            type: 'POST',
            async: this.async,
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                AeviApiDataService.TOKEN
            ]),
            data: 'username=' + this.username + '&password=' + this.password + '&grant_type=password&SlotID=' + this.slot + '&languageIsoCode=cs'
        }).done(function (response) {
            _this.token = AeviApiDataService.getTokenWithBearer(response.access_token);
        });
    };
    AeviApiDataService.getTokenWithBearer = function (token) {
        return 'Bearer ' + token;
    };
    AeviApiDataService.prototype.getTokenWithoutBearer = function () {
        if (_.isUndefined(this.token)) {
            console.log('Token is undefined');
            return null;
        }
        return this.token.replace('Bearer', '');
    };
    AeviApiDataService.prototype.getTableDescription = function () {
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'GET',
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableDescription' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang },
                    { name: AeviApiDataService.ENTITYTYPE, value: this.entityType }
                ]
            ]),
            contentType: 'application/json; charset=utf-8'
        });
    };
    AeviApiDataService.prototype.getFormDescription = function () {
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'GET',
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableDescription' },
                    { name: AeviApiDataService.ACTION, value: 'GetFormDescription' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang },
                    { name: AeviApiDataService.ENTITYTYPE, value: this.entityType }
                ]
            ]),
            contentType: 'application/json; charset=utf-8'
        });
    };
    AeviApiDataService.prototype.getTableData = function (query, force) {
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'GET',
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableData' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang },
                    { name: AeviApiDataService.JSONFILTER, value: query },
                    { name: AeviApiDataService.FORCE, value: force },
                    { name: AeviApiDataService.ENTITYTYPE, value: this.entityType }
                ]
            ]),
            contentType: 'application/json; charset=utf-8'
        });
    };
    AeviApiDataService.prototype.getTableRow = function (guid) {
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'GET',
            async: false,
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableData' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang },
                    { name: AeviApiDataService.ITEMKEY, value: guid }
                ]
            ]),
            contentType: 'application/json; charset=utf-8'
        });
    };
    AeviApiDataService.prototype.putData = function (data, async) {
        this.setDataChanged(true);
        var asyncType = (aeviIsUndefinedOrNull(async)) ? this.async : async;
        return $.ajax({
            headers: { 'Authorization': this.token },
            async: asyncType,
            type: 'PUT',
            contentType: 'application/json',
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableData' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang },
                    { name: AeviApiDataService.DATAMODE, value: this.AeviGrid.mode.clipboard }
                ]
            ]),
            data: JSON.stringify(data)
        });
    };
    AeviApiDataService.prototype.deleteData = function (guids) {
        this.setDataChanged(true);
        var deleteUrl = AeviApiDataService.getComposedUrl([
            this.apiAddress,
            [
                { name: AeviApiDataService.API },
                { name: AeviApiDataService.CONTROLLER, value: 'TableData' },
                { name: AeviApiDataService.ID, value: this.entity },
                { name: AeviApiDataService.LANGUAGE, value: this.lang + '&' },
            ]
        ]);
        var batch = [];
        var query = deleteUrl;
        for (var i = 0; i < guids.length; i++) {
            guids[i] = 'ids=' + guids[i] + '&';
            if (query.length < AeviApiDataService.SAVE_URL_LENGTH) {
                query += guids[i];
            }
            else {
                batch.push(query);
                query = deleteUrl + guids[i];
            }
        }
        batch.push(query);
        if (batch[batch.length - 1] === deleteUrl) {
            batch.pop();
        }
        var ajaxRequests = [];
        for (var j = 0; j < batch.length; j++) {
            batch[j] = batch[j].substring(0, batch[j].length - 1);
            var request = $.ajax({
                headers: { 'Authorization': this.token },
                type: 'DELETE',
                async: this.async,
                url: batch[j],
                contentType: 'application/json'
            });
            ajaxRequests.push(request);
        }
        return ajaxRequests[0];
    };
    AeviApiDataService.prototype.commit = function () {
        return $.ajax({
            headers: { 'Authorization': this.token },
            async: this.async,
            type: 'POST',
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableData' },
                    { name: AeviApiDataService.ACTION, value: 'Commit' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang }
                ]
            ]),
            data: { '__RequestVerificationToken': this.AeviGrid.antiForgeryToken }
        });
    };
    AeviApiDataService.prototype.getDataEntity = function () {
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'GET',
            async: this.async,
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'DataEntity' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang },
                    { name: AeviApiDataService.ENTITYTYPE, value: this.entityType }
                ]
            ]),
            contentType: 'application/json; charset=utf-8'
        });
    };
    AeviApiDataService.getComposedUrl = function (url) {
        for (var i = 0; i < url.length; i++) {
            if (_.isArray(url[i])) {
                var params = [];
                var parameters = url[i];
                url.splice(i, 1);
                for (var j = 0; j < parameters.length; j++) {
                    var parameter = parameters[j];
                    parameter = AeviApiDataService.getComposedParameter(parameter);
                    params.push(parameter);
                }
                var stringParameters = params.join('&');
                stringParameters = stringParameters.replace('&', '');
                url.push(stringParameters);
            }
        }
        return url.join('/');
    };
    AeviApiDataService.getComposedParameter = function (parameter) {
        if (_.isUndefined(parameter.value))
            return parameter.name;
        return parameter.name + '=' + parameter.value;
    };
    AeviApiDataService.prototype.getDownloadFileUrl = function (JSONFilter, JSONSortInfos, fileType) {
        return AeviApiDataService.getComposedUrl([
            this.apiAddress,
            [
                { name: 'GetFile?' },
                { name: AeviApiDataService.ID, value: this.entity },
                { name: AeviApiDataService.CAPTION, value: this.caption },
                { name: AeviApiDataService.LANGUAGE, value: this.lang },
                { name: AeviApiDataService.JSONFILTER, value: JSONFilter },
                { name: AeviApiDataService.FORCE, value: false },
                { name: AeviApiDataService.ENTITYTYPE, value: this.entityType },
                { name: AeviApiDataService.JSONSORTINFOS, value: JSONSortInfos },
                { name: AeviApiDataService.OUTPUTTYPE, value: fileType },
                { name: AeviApiDataService.ACCESSTOKEN, value: this.getTokenWithoutBearer() }
            ]
        ]);
    };
    AeviApiDataService.prototype.getExportFileUrl = function (fileType) {
        var JSONFilter = '';
        if (!aeviIsUndefinedOrNull(this.AeviGrid.AeviFilter)) {
            JSONFilter = encodeURIComponent(this.AeviGrid.AeviFilter.getJSONFilterValues());
        }
        return AeviApiDataService.getComposedUrl([
            this.apiAddress,
            [
                { name: 'ExportFile' + fileType + '?' },
                { name: AeviApiDataService.ID, value: this.entity },
                { name: AeviApiDataService.CAPTION, value: this.caption },
                { name: AeviApiDataService.LANGUAGE, value: this.lang },
                { name: AeviApiDataService.JSONFILTER, value: JSONFilter },
                { name: AeviApiDataService.ENTITYTYPE, value: this.entityType },
                { name: AeviApiDataService.ACCESSTOKEN, value: this.getTokenWithoutBearer() }
            ]
        ]);
    };
    AeviApiDataService.prototype.executeAction = function (actionData, async) {
        var JSONData = JSON.stringify(actionData);
        var asyncType = (aeviIsUndefinedOrNull(async)) ? this.async : async;
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'POST',
            async: asyncType,
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableData' },
                    { name: AeviApiDataService.ACTION, value: 'ExecuteAction' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang },
                    { name: AeviApiDataService.ENTITYTYPE, value: this.entityType }
                ]
            ]),
            data: { __RequestVerificationToken: this.AeviGrid.antiForgeryToken, jsonRequest: JSONData },
            contentType: "application/x-www-form-urlencoded"
        });
    };
    AeviApiDataService.prototype.getRoles = function () {
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'GET',
            async: this.async,
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                'api',
                'Roles',
                this.lang,
                this.entity,
                this.entityType
            ]),
            contentType: 'application/json; charset=utf-8'
        });
    };
    AeviApiDataService.prototype.setTableSettings = function (tableSettings) {
        return this.getOrSetTableSettings('PUT', JSON.stringify(tableSettings));
    };
    AeviApiDataService.prototype.getTableSettings = function () {
        return this.getOrSetTableSettings('GET');
    };
    AeviApiDataService.prototype.getOrSetTableSettings = function (requestType, tableSettings) {
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: requestType,
            async: this.async,
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: AeviApiDataService.API },
                    { name: AeviApiDataService.CONTROLLER, value: 'TableSettings' },
                    { name: AeviApiDataService.ID, value: this.entity },
                    { name: AeviApiDataService.LANGUAGE, value: this.lang }
                ]
            ]),
            data: tableSettings,
            contentType: "application/json; charset=utf-8"
        });
    };
    AeviApiDataService.prototype.setDataChanged = function (val) {
        this.isDataChanged = val;
    };
    AeviApiDataService.prototype.setCellChanged = function (val) {
        this.isCellChanged = val;
    };
    AeviApiDataService.prototype.importFile = function (data, fileType) {
        var importData = {};
        importData['__RequestVerificationToken'] = this.AeviGrid.antiForgeryToken;
        importData['BinaryData'] = data;
        importData[AeviApiDataService.ID] = this.entity;
        importData[AeviApiDataService.LANGUAGE] = this.lang;
        importData[AeviApiDataService.ENTITYTYPE] = this.entityType;
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'POST',
            async: this.async,
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: 'ImportFile' + fileType }
                ]
            ]),
            data: importData
        });
    };
    AeviApiDataService.prototype.setDataEntityBlockStatus = function () {
        var importData = {};
        importData['__RequestVerificationToken'] = this.AeviGrid.antiForgeryToken;
        importData[AeviApiDataService.ID] = this.entity;
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'POST',
            async: this.async,
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: 'SetDataEntityBlockStatus' }
                ]
            ]),
            data: importData
        });
    };
    AeviApiDataService.prototype.getDataEntityStatus = function () {
        return $.ajax({
            headers: { 'Authorization': this.token },
            type: 'GET',
            async: this.async,
            url: AeviApiDataService.getComposedUrl([
                this.apiAddress,
                [
                    { name: 'GetDataEntityStatus?' },
                    { name: AeviApiDataService.ID, value: this.entity }
                ]
            ])
        });
    };
    AeviApiDataService.SAVE_URL_LENGTH = 1800;
    AeviApiDataService.API = 'Call?';
    AeviApiDataService.TOKEN = 'token';
    AeviApiDataService.CONTROLLER = 'Controller';
    AeviApiDataService.ACTION = 'Action';
    AeviApiDataService.ID = 'Id';
    AeviApiDataService.CAPTION = 'Caption';
    AeviApiDataService.LANGUAGE = 'Language';
    AeviApiDataService.JSONFILTER = 'JsonFilter';
    AeviApiDataService.FORCE = 'Force';
    AeviApiDataService.ENTITYTYPE = 'EntityType';
    AeviApiDataService.JSONSORTINFOS = 'JsonSortInfos';
    AeviApiDataService.OUTPUTTYPE = 'OutputType';
    AeviApiDataService.ACCESSTOKEN = 'access_token';
    AeviApiDataService.ITEMKEY = 'ItemKey';
    AeviApiDataService.DATAMODE = 'DataMode';
    return AeviApiDataService;
}());
var AeviApiService = (function (_super) {
    __extends(AeviApiService, _super);
    function AeviApiService() {
        _super.apply(this, arguments);
    }
    return AeviApiService;
}(AeviApiDataService));
var AeviApiAuthorization = (function () {
    function AeviApiAuthorization() {
        this.getCookies();
        this.nullCookies();
        this.listener();
    }
    AeviApiAuthorization.prototype.getCookies = function () {
        this.requestVerificationToken = $.cookie(AeviApiAuthorization.requestVerificationTokenName);
        this.aspNetSessionId = $.cookie(AeviApiAuthorization.aspNetSessionIdName);
        this.aspxAuth = $.cookie(AeviApiAuthorization.aspxAuthName);
    };
    AeviApiAuthorization.prototype.setCookies = function () {
        $.cookie(AeviApiAuthorization.requestVerificationTokenName, this.requestVerificationToken);
        $.cookie(AeviApiAuthorization.aspNetSessionIdName, this.aspNetSessionId);
        $.cookie(AeviApiAuthorization.aspxAuthName, this.aspxAuth);
        this.isCookiesNull = false;
    };
    AeviApiAuthorization.prototype.nullCookies = function () {
        $.cookie(AeviApiAuthorization.requestVerificationTokenName, null);
        $.cookie(AeviApiAuthorization.aspNetSessionIdName, null);
        $.cookie(AeviApiAuthorization.aspxAuthName, null);
        this.isCookiesNull = true;
    };
    AeviApiAuthorization.prototype.listener = function () {
        var _this = this;
        $(document).ajaxSend(function () {
            _this.nullCookies();
        }).ajaxStop(function () {
            _this.setCookies();
        });
    };
    AeviApiAuthorization.requestVerificationTokenName = '__RequestVerificationToken';
    AeviApiAuthorization.aspNetSessionIdName = 'ASP.NET_SessionId';
    AeviApiAuthorization.aspxAuthName = '.ASPXAUTH';
    return AeviApiAuthorization;
}());
var MyRetail;
(function (MyRetail) {
    (function (IAeviResponseStatus) {
        IAeviResponseStatus[IAeviResponseStatus["OK"] = 0] = "OK";
        IAeviResponseStatus[IAeviResponseStatus["Error"] = 1] = "Error";
        IAeviResponseStatus[IAeviResponseStatus["Blocked"] = 2] = "Blocked";
        IAeviResponseStatus[IAeviResponseStatus["Changed"] = 3] = "Changed";
    })(MyRetail.IAeviResponseStatus || (MyRetail.IAeviResponseStatus = {}));
    var IAeviResponseStatus = MyRetail.IAeviResponseStatus;
})(MyRetail || (MyRetail = {}));
var MyRetail;
(function (MyRetail) {
    (function (IAeviBlockStateCommand) {
        IAeviBlockStateCommand[IAeviBlockStateCommand["CONTINUE"] = 0] = "CONTINUE";
        IAeviBlockStateCommand[IAeviBlockStateCommand["LOCK"] = 1] = "LOCK";
        IAeviBlockStateCommand[IAeviBlockStateCommand["RELOAD"] = 2] = "RELOAD";
    })(MyRetail.IAeviBlockStateCommand || (MyRetail.IAeviBlockStateCommand = {}));
    var IAeviBlockStateCommand = MyRetail.IAeviBlockStateCommand;
})(MyRetail || (MyRetail = {}));
var AeviDataService = (function () {
    function AeviDataService(aeviGrid, aeviApiService, aeviDataRepository) {
        var _this = this;
        this.AeviGrid = aeviGrid;
        this.AeviApiService = aeviApiService;
        this.AeviDataRepository = aeviDataRepository;
        this.translates = this.AeviGrid.AeviLocalization;
        if (aeviIsUndefinedOrNull(this.AeviApiService.token)) {
            this.AeviApiService.getAccessToken().done(function () {
                _this.initDataEntity();
            }).fail(function (err) {
                var message = null;
                if (!aeviIsUndefinedOrNull(err)) {
                    try {
                        var parsed = JSON.parse(err.responseText);
                        var responseText = parsed.error_description;
                    }
                    catch (error) {
                        message = _this.translates.translate('connection_fail');
                    }
                    if (!aeviIsUndefinedOrNull(responseText)) {
                        message = responseText;
                    }
                }
                else {
                    message = _this.translates.translate('connection_fail');
                }
                toastr.error(message);
            });
        }
        else {
            this.initDataEntity();
        }
    }
    AeviDataService.prototype.initDataEntity = function () {
        var _this = this;
        this.AeviUser = new AeviUser(this.AeviGrid.roles);
        if (!this.AeviUser.roles.Read) {
            var msg = this.translates.translate('roles_cannot_read');
            toastr.error(msg);
            this.AeviGrid.AeviStatusBar.error(msg);
            return;
        }
        this.AeviApiService.getDataEntity().done(function (response) {
            _this.AeviDataRepository.initDataEntity(_this, response);
            _this.initTableDescriptionAndTableData();
        }).fail(function (err) {
            var response = err.responseJSON;
            var message = _this.translates.translate('connection_fail');
            if (!aeviIsUndefinedOrNull(response) && !aeviIsUndefinedOrNull(response.ExceptionMessage))
                message = response.ExceptionMessage;
            toastr.error(message);
            _this.AeviGrid.AeviStatusBar.error(message);
            return;
        });
    };
    AeviDataService.prototype.initTableDescriptionAndTableData = function () {
        var _this = this;
        if (!aeviIsUndefinedOrNull(this.AeviDataRepository.AeviDataEntity.data.Help)) {
            this.AeviGrid.AeviStatusBar.renderHelpIcon();
        }
        if (this.AeviDataRepository.AeviDataEntity.data.Form === true) {
            this.AeviApiService.getFormDescription().done(function (response) {
                _this.AeviDataRepository.initForm(response);
            });
        }
        this.AeviApiService.getTableDescription().done(function (response) {
            _this.AeviDataRepository.initTableDescription(response);
            _this.AeviGrid.AeviGridEditor.renderTableHead();
            var filters = null;
            if (!_.isNull(response.Filters)) {
                _this.initFilters(response.Filters);
                filters = _this.AeviGrid.AeviFilter.getFilters();
            }
            _this.AeviGrid.AeviToolbar = new AeviToolbar(_this.AeviGrid, filters);
            _this.AeviGrid.AeviToolbar.render();
            _this.AeviGrid.setSizes();
            if (aeviIsUndefinedOrNull(_this.AeviGrid.AeviPager)) {
                _this.AeviGrid.AeviPager = new AeviPager(_this.AeviGrid);
            }
            if (_this.AeviDataRepository.AeviTableDescription.allowLoadDataImmediately) {
                if (_this.AeviDataRepository.AeviTableDescription.isReport())
                    _this.AeviGrid.AeviToolbar.AeviToolbarHandler.query();
                else {
                    _this.getDataByQuery('', true);
                }
            }
            else {
                toastr.info(_this.translates.translate('must_filter'));
            }
        }).fail(function (err) {
            var response = err.responseJSON;
            var message = _this.translates.translate('connection_fail');
            if (!aeviIsUndefinedOrNull(response) && !aeviIsUndefinedOrNull(response.ExceptionMessage))
                message = response.ExceptionMessage;
            toastr.error(message);
            _this.AeviGrid.AeviStatusBar.error(message);
        });
    };
    AeviDataService.prototype.initFilters = function (responseFilters) {
        var filters = null;
        if (!_.isNull(responseFilters)) {
            this.AeviGrid.AeviFilter = new AeviFilter(this.AeviGrid, responseFilters);
            filters = this.AeviGrid.AeviFilter.getFilters();
        }
        return filters;
    };
    AeviDataService.prototype.getDataByQuery = function (query, force) {
        var _this = this;
        this.AeviGrid.AeviAjaxPreloader.show();
        this.AeviGrid.AeviStatusBar.clear();
        var promise = $.Deferred();
        return this.AeviApiService.getTableData(query, force).done(function (response) {
            if (AeviDataService.hasConnectionProblem(response)) {
                toastr.error(response.Message || _this.translates.translate('connection_fail'));
                _this.AeviGrid.AeviStatusBar.error(response.Message || _this.translates.translate('connection_fail'));
                _this.AeviGrid.AeviAjaxPreloader.hide();
                return;
            }
            var renderType = 'render';
            if (_this.AeviDataRepository.repositoryDataInit) {
                renderType = 'refresh';
                if (_this.AeviDataRepository.AeviTableData.getRecordsLength() < response.Data.length)
                    renderType = 're-render';
            }
            if (renderType !== 'render') {
                _this.AeviGrid.AeviGridHandler.triggerClick();
            }
            _this.AeviDataRepository.initTableData(response.Data.slice());
            _this.AeviApiService.setDataChanged(false);
            _this.AeviApiService.setCellChanged(false);
            if (!_.isNull(response.Sums))
                _this.AeviGrid.AeviSum.init(response.Sums);
            _this.AeviGrid.AeviPluginApi.callPluginsOnDataLoaded();
            _this.showData(renderType);
            _this.AeviGrid.AeviAjaxPreloader.hide();
            promise.resolve();
        }).fail(function () {
            toastr.error(_this.translates.translate('connection_fail'));
        });
    };
    AeviDataService.prototype.showData = function (type) {
        this.AeviGrid.setSizes();
        switch (type) {
            case 'render':
                this.AeviGrid.AeviPager.render();
                this.AeviGrid.AeviPager.setVisibleContentInfo();
                break;
            case 're-render':
                this.AeviGrid.AeviPager.refresh();
                break;
            case 'refresh':
                this.AeviGrid.AeviPager.AeviPagerHandler.setJumps();
                this.AeviGrid.AeviGridEditor.refreshData();
                break;
            default:
                break;
        }
        if (this.AeviGrid.firstLoad === true) {
            this.AeviGrid.AeviGridLocker = new AeviGridLocker(this.AeviGrid);
            this.AeviGrid.AeviGridLocker.lock();
            this.AeviGrid.firstLoad = false;
        }
        if (this.AeviDataRepository.AeviTableData.getRecordsLength() > 0) {
            this.AeviGrid.selectFirstCell();
        }
        this.AeviGrid.AeviDOM.closeModals();
    };
    AeviDataService.prototype.getDataRow = function (guid) {
        var _this = this;
        var result = null;
        this.AeviGrid.AeviAjaxPreloader.show();
        this.AeviApiService.getTableRow(guid).done(function (response) {
            _this.AeviGrid.AeviAjaxPreloader.hide();
            if (_.isNull(response)) {
                result = response;
            }
            else {
                result = response.Data;
            }
        });
        return result;
    };
    AeviDataService.prototype.getImageData = function (guid, cellIndex) {
        var imageCellIndex = cellIndex - this.AeviDataRepository.AeviTableDescription.startFakeColumnsLength;
        var dataRow = this.getDataRow(guid);
        if (_.isNull(dataRow)) {
            this.AeviGrid.log('data with GUID: ' + guid + ' does not exists in TEMP TABLE. Data will be taken from repository.');
            return this.AeviDataRepository.AeviTableData.getRecord(guid)[cellIndex];
        }
        else {
            return dataRow[imageCellIndex];
        }
    };
    AeviDataService.prototype.getData = function () {
        return this.AeviDataRepository.AeviTableData.getData();
    };
    AeviDataService.prototype.commit = function (query, reload) {
        var _this = this;
        var result = $.Deferred();
        if (this.AeviApiService.activeAjaxConnection > 0) {
            this.AeviGrid.AeviAjaxPreloader.show();
            setTimeout(function () {
                _this.commit(query, reload);
            }, 500);
            return;
        }
        var message = '';
        var filter = (aeviIsUndefinedOrNull(query)) ? '' : query;
        if (this.isServerDataInvalid) {
            var modal = new AeviModal(this.AeviGrid);
            modal.setContent({
                title: this.AeviGrid.AeviLocalization.translate('warning'),
                text: '<p>' + this.AeviGrid.AeviLocalization.translate('server_invalid_records_message') + '</p>',
                buttons: [
                    'aeviDataServiceCommitTempAndClose',
                    'aeviDataServiceDeleteTempIgnoreAndClose',
                    'aeviDataServiceClose'
                ]
            });
            modal.show();
            modal.focus('first');
            return;
        }
        this.invalidRecordsLength = this.AeviDataRepository.AeviTableData.getInvalidRecordsIndexes().length;
        if (this.invalidRecordsLength > 0) {
            this.AeviGrid.AeviGridHandler.triggerClick();
            message = this.translates.translate('invalid_records');
            this.AeviGrid.AeviStatusBar.error(message, 'error');
            toastr.error(message);
            this.AeviGrid.AeviDataRepository.AeviTableData.sortInvalid();
            this.AeviGrid.AeviGridEditor.refreshData();
            this.AeviGrid.selectFirstCell();
            return;
        }
        this.AeviGrid.AeviAjaxPreloader.show();
        this.AeviApiService.commit().done(function (response) {
            if (AeviDataService.isServerDataValid(response)) {
                if (!aeviIsUndefinedOrNull(reload) && reload === true) {
                    if (_this.AeviGrid.mode.clipboard === 1) {
                        _this.AeviDataRepository.deleteRepositoryTableData();
                        _this.AeviGrid.mode.clipboard = 0;
                        _this.AeviGrid.AeviClipboard.disableOneCellPasting();
                    }
                    _this.getDataByQuery(filter, true);
                    _this.AeviDataRepository.enableOrDisableImageColumns('enable');
                    toastr.success(_this.translates.translate('table_data_commit'));
                }
                _this.isServerDataInvalid = false;
            }
            else if (!AeviDataService.isServerDataValid(response)) {
                var invalidRecordsLength = response.Data.length;
                if (invalidRecordsLength > 0) {
                    _this.AeviDataRepository.initTableData(response.Data);
                    for (var i = 0; i < invalidRecordsLength; i++) {
                        _this.AeviDataRepository.AeviTableData.AeviStatus.updateByRowIndex(i, 'render serverInvalid');
                    }
                    _this.showData('re-render');
                    if (_this.AeviGrid.AeviGridLocker.isLocked) {
                        _this.AeviGrid.AeviGridLocker.unLock();
                        _this.AeviGrid.AeviGridLocker.lock();
                    }
                    else {
                        _this.AeviGrid.AeviGridLocker.lock();
                        _this.AeviGrid.AeviGridLocker.unLock();
                    }
                }
                toastr.error(_this.translates.translate('commit_invalid_rows'));
                _this.isServerDataInvalid = true;
                var firstCell = _this.AeviGrid.AeviDOM.getCell(1, 0);
                _this.AeviGrid.AeviGridHandler.cellChange(firstCell);
                if (response.Message)
                    message = response.Message;
                else
                    message = _this.translates.translate('invalid_data_status');
            }
            else {
                if (response && response.Message) {
                    message = response.Message;
                    toastr.error(message);
                }
            }
            _this.AeviGrid.AeviAjaxPreloader.hide();
            _this.AeviGrid.AeviStatusBar.error(message);
            _this.AeviGrid.AeviToolbar.refresh();
            _this.AeviGrid.AeviGridLocker.removeProtectedView();
            _this.AeviGrid.AeviGridLocker.lock();
            if (!AeviDataService.isServerDataValid(response)) {
                setTimeout(function () {
                    _this.AeviGrid.AeviStatusBar.error(message);
                }, 300);
            }
            result.resolve();
        }).fail(function () {
            _this.AeviGrid.AeviAjaxPreloader.show();
            _this.AeviGrid.AeviDOM.closeModals();
            setTimeout(function () {
                _this.commit(query, reload);
            }, 2000);
        });
        return result;
    };
    AeviDataService.hasConnectionProblem = function (response) {
        return (_.isNull(response.Data) || (!_.isNull(response.Message) && response.Message !== ''));
    };
    AeviDataService.isServerDataValid = function (response) {
        if (response.Status === MyRetail.IAeviResponseStatus.Error) {
            return false;
        }
        if (_.isNull(response.Data)) {
            return true;
        }
        return false;
    };
    AeviDataService.isLocalDataValid = function (records) {
        return (records < 1);
    };
    AeviDataService.prototype.deleteData = function (guids, firstNotDeletedRecordIndex) {
        var _this = this;
        var deleteMessage;
        var repoGuids = guids.slice();
        this.AeviApiService.deleteData(guids).done(function (response) {
            if (_.isNull(response.Data)) {
                deleteMessage = _this.translates.translate('table_data_deleted');
            }
            else {
                deleteMessage = response.Message;
            }
            var deletingDataRows = [];
            for (var i = 0; i < repoGuids.length; i++) {
                var row = _this.AeviDataRepository.AeviTableData.getRecord(repoGuids[i]);
                if (!aeviIsUndefinedOrNull(row) && row.length > 0)
                    deletingDataRows.push(row);
            }
            _this.AeviGrid.AeviPluginApi.callPluginsOnRowsDeleting(deletingDataRows);
            _this.AeviDataRepository.AeviTableData.deleteData(repoGuids);
            _this.AeviGrid.AeviAjaxPreloader.hide();
            _this.AeviGrid.AeviPager.AeviPagerHandler.setJumps();
            _this.AeviGrid.AeviGridEditor.refreshData();
            _this.AeviGrid.AeviPager.moveTo({
                rowId: firstNotDeletedRecordIndex
            });
            _this.AeviGrid.AeviAjaxPreloader.hide();
            _this.checkResponseStatus(response);
        }).fail(function () {
            toastr.error(_this.translates.translate('connection_fail'));
            _this.AeviGrid.AeviAjaxPreloader.show();
            setTimeout(function () {
                _this.AeviGrid.AeviGridEditor.deleteRow();
            }, 4000);
        });
    };
    AeviDataService.prototype.putRow = function (rowIndex) {
        var apiData = this.AeviDataRepository.AeviTableData.getApiDataByRowIndex(rowIndex);
        apiData = this.setPutType(rowIndex, apiData);
        var async = true;
        this.putData(apiData, async);
    };
    ;
    AeviDataService.prototype.setPutType = function (rowIndex, apiData) {
        var putType = null;
        var status = this.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(rowIndex);
        if (status.indexOf('insert') !== -1)
            putType = MyRetail.putType.insert;
        else if (status.indexOf('update') !== -1)
            putType = MyRetail.putType.update;
        this.AeviGrid.log('PUT: ' + MyRetail.putType[putType]);
        apiData[0].splice(0, 0, putType);
        return apiData;
    };
    AeviDataService.prototype.putData = function (putData, async) {
        var _this = this;
        var result = $.Deferred();
        this.AeviApiService.activeAjaxConnection++;
        if (!_.isEmpty(putData)) {
            var guidIndex = this.AeviDataRepository.AeviTableDescription.getGuidIndex();
            this.AeviApiService.putData(putData, async).done(function (response) {
                _this.AeviApiService.activeAjaxConnection--;
                for (var i = 0; i < putData.length; i++) {
                    var guid = putData[i][guidIndex - _this.AeviDataRepository.AeviTableDescription.startFakeColumnsLength + 1];
                    var row = _this.AeviDataRepository.AeviTableData.getRecord(guid);
                    var rowIndex = row[_this.AeviDataRepository.AeviTableDescription.getRowNumberIndex()];
                    var isErrorRow = false;
                    if (response.Status === MyRetail.IAeviResponseStatus.Error) {
                        var errorRows = response.Data;
                        if (!_.isNull(errorRows) && errorRows.length > 0) {
                            for (var j = 0; j < errorRows.length; j++) {
                                if (putData[i][guidIndex] === errorRows[j].DataItem[guidIndex - _this.AeviDataRepository.AeviTableDescription.startFakeColumnsLength]) {
                                    _this.AeviDataRepository.AeviTableData.setErrorMessage(rowIndex, errorRows[j].Message);
                                    isErrorRow = true;
                                    errorRows.splice(j, 1);
                                    break;
                                }
                            }
                            if (isErrorRow) {
                                _this.AeviDataRepository.AeviTableData.AeviStatus.update(guid, 'render insert invalid');
                            }
                        }
                    }
                    var status = 'render';
                    if (_this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'softInvalid'))
                        status += ' softInvalid';
                    if (_this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'serverInvalid'))
                        status += ' serverInvalid';
                    for (var x = 1; x < putData[i].length; x++) {
                        var column = _this.AeviDataRepository.AeviTableDescription.getColumnHeader(x);
                        var displayType = _this.AeviDataRepository.AeviTableDescription.getDisplayType(column);
                        var stringDisplayType = _this.AeviDataRepository.AeviTableDescription.getColumnStringDisplayType(displayType);
                        if (stringDisplayType === 'Image') {
                            row[x].Changed = false;
                        }
                    }
                    if (!isErrorRow) {
                        _this.AeviDataRepository.AeviTableData.AeviStatus.updateByRowIndex(rowIndex, status);
                    }
                    _this.AeviGrid.AeviGridEditor.refreshRow({ rowIndex: rowIndex });
                    _this.checkResponseStatus(response);
                    $(document).trigger('mouseup');
                }
                if (_this.AeviApiService.activeAjaxConnection < 1)
                    _this.AeviGrid.AeviAjaxPreloader.hide();
                _this.AeviGrid.AeviPager.AeviPagerHandler.setJumps();
                result.resolve();
            }).fail(function () {
                _this.AeviApiService.activeAjaxConnection--;
                _this.AeviGrid.AeviAjaxPreloader.show();
                toastr.error(_this.translates.translate('connection_fail'));
                setTimeout(function () {
                    _this.putData(putData);
                }, 8000);
            });
        }
        return result;
    };
    AeviDataService.prototype.checkResponseStatus = function (response) {
        var _this = this;
        if (response.Status === MyRetail.IAeviResponseStatus.Blocked) {
            this.showBlockModal(response)
                .done(function () {
                _this.AeviApiService.setDataEntityBlockStatus();
            })
                .fail(function () {
                _this.AeviGrid.AeviGridLocker.lock();
            });
        }
        if (response.Status === MyRetail.IAeviResponseStatus.Changed) {
            this.showDataChangedModal(response)
                .done(function () {
                _this.AeviApiService.setDataEntityBlockStatus().done(function () {
                    _this.getDataByQuery('', true);
                });
            })
                .fail(function () {
                _this.AeviGrid.AeviGridLocker.lock();
            });
        }
    };
    AeviDataService.prototype.paste = function (data, cellIndex) {
        var _this = this;
        if (this.AeviGrid.mode.clipboard === 0 || (this.AeviGrid.mode.clipboard === 1 && this.AeviGrid.AeviClipboard.canPasteOneCell)) {
            this.pasteOneCell(data, cellIndex);
            return;
        }
        this.AeviGrid.AeviGridLocker.unLockWithoutAccessControl();
        this.AeviDataRepository.AeviTableData.pasteRecords(data, cellIndex);
        this.AeviGrid.AeviPluginApi.callPluginsOnRowsChanged(data);
        var firstPastedRowIndex = cellIndex.rowIndex;
        var lastPastedRowIndex = data.Rows.length + firstPastedRowIndex;
        var request = [];
        for (var rowIndex = firstPastedRowIndex; rowIndex < lastPastedRowIndex; rowIndex++) {
            if (this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'invalid'))
                continue;
            var putType = MyRetail.putType.update;
            if (this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'newrow'))
                putType = MyRetail.putType.insert;
            var apiData = this.AeviDataRepository.AeviTableData.getApiDataByRowIndex(rowIndex);
            apiData[0].splice(0, 0, putType);
            request.push(apiData[0]);
        }
        this.AeviApiService.setDataChanged(true);
        this.AeviGrid.AeviPager.AeviPagerHandler.setJumps();
        this.AeviGrid.AeviPager.setVisibleContentInfo();
        this.showData('re-render');
        this.AeviGrid.setSizes();
        if (request.length > 0) {
            this.AeviGrid.AeviAjaxPreloader.show();
            this.putData(request, true).done(function () {
                if (!_this.AeviGrid.AeviClipboard.canPasteOneCell) {
                    _this.AeviDataRepository.enableOrDisableImageColumns('disable');
                    _this.AeviGrid.AeviStatusBar.info(_this.AeviGrid.AeviLocalization.translate('save_mode_excel_' + _this.AeviDataRepository.AeviTableDescription.saveModeExcel));
                    _this.AeviGrid.AeviGridLocker.renderProtectedView('<div class="aeviProtected"><p>' + _this.AeviGrid.AeviLocalization.translate('save_mode_excel_' + _this.AeviDataRepository.AeviTableDescription.saveModeExcel) + '</p><span class="aeviProtected__save">' + _this.AeviGrid.AeviLocalization.translate('toolbar_save') + '</span></div>');
                    _this.showData('re-render');
                    _this.AeviGrid.setSizes();
                    _this.AeviGrid.AeviClipboard.allowOneCellPasting();
                }
            });
        }
    };
    AeviDataService.prototype.pasteOneCell = function (data, cellIndex) {
        var header = this.AeviDataRepository.AeviTableDescription.getColumnHeader(cellIndex.cellIndex);
        if (this.AeviDataRepository.AeviTableDescription.isColumnReadOnly(header)) {
            return;
        }
        this.AeviDataRepository.AeviTableData.pasteOneCell(data, cellIndex);
        if (!this.AeviDataRepository.AeviTableData.AeviStatus.is(cellIndex.rowIndex, 'invalid')) {
            this.putRow(cellIndex.rowIndex);
            this.AeviApiService.setDataChanged(true);
        }
    };
    AeviDataService.prototype.executeAction = function (rowIndex, actionParameters) {
        switch (MyRetail.IAeviEntityActionResultType[actionParameters.ResultType]) {
            case 'Image':
                this.executeImage(rowIndex, actionParameters);
                break;
            case 'Form':
                this.executeForm(rowIndex);
                break;
            default:
                break;
        }
    };
    AeviDataService.prototype.executeForm = function (rowIndex) {
        return new AeviForm(rowIndex, this, { isOpenedByToolbar: false });
    };
    AeviDataService.prototype.executeImage = function (rowIndex, actionParameters) {
        var _this = this;
        var data = this.AeviDataRepository.AeviTableData.getRecordByRowIndex(rowIndex);
        var apiData = this.AeviDataRepository.AeviTableData.getApiDataWithColumnNames(data);
        var executeActionDefinition = {
            'ActionId': actionParameters.ActionId,
            'ResultType': actionParameters.ResultType,
            'Data': apiData
        };
        this.AeviGrid.AeviAjaxPreloader.show();
        this.AeviApiService.executeAction(executeActionDefinition, true).done(function (response) {
            if (aeviIsUndefinedOrNull(response)) {
                toastr.error(_this.translates.translate('connection_fail'));
                return;
            }
            if (!aeviIsUndefinedOrNull(response.Message)) {
                _this.AeviGrid.AeviStatusBar.error(response.Message);
                toastr.error(response.Message);
                return;
            }
            if (!aeviIsUndefinedOrNull(response.Data)) {
                var image = document.createElement('img');
                image.src = 'data:image/' + _this.AeviGrid.AeviConsts.imageFormat + ';base64,' + response.Data;
                image.addEventListener('load', function () {
                    var modal = new AeviModal(_this.AeviGrid);
                    modal.setContent({
                        title: _this.translates.translate('preview'),
                        text: '<div class="actionImage"><img width="' + image.width + '" height="' + image.height + '" src="' + image.src + '"></div>'
                    });
                    modal.show();
                });
            }
            _this.AeviGrid.AeviAjaxPreloader.hide();
        }).fail(function () {
            toastr.error(_this.translates.translate('connection_fail'));
        });
    };
    AeviDataService.prototype.clearData = function () {
        var selectedCells = this.AeviDataRepository.getSelectedCellsPositions();
        var clearingRowsIndexes = [];
        for (var i = 0; i < selectedCells.rows.length; i++) {
            var rowIndex = selectedCells.rows[i];
            if (!this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'render'))
                continue;
            clearingRowsIndexes.push(rowIndex);
        }
        this.AeviDataRepository.clearCells(selectedCells);
        this.AeviDataRepository.AeviTableData.validate();
        var request = [];
        for (i = 0; i < clearingRowsIndexes.length; i++) {
            var rowIndex = clearingRowsIndexes[i];
            if (!this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'invalid')) {
                var putType = MyRetail.putType.update;
                if (this.AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'newrow'))
                    putType = MyRetail.putType.insert;
                var apiData = this.AeviDataRepository.AeviTableData.getApiDataByRowIndex(rowIndex);
                apiData[0].splice(0, 0, putType);
                request.push(apiData[0]);
            }
        }
        if (request.length > 0) {
            this.putData(request);
        }
    };
    AeviDataService.prototype.importFile = function (formData, fileType) {
        var _this = this;
        this.AeviGrid.AeviAjaxPreloader.show();
        this.AeviGrid.AeviDataService.AeviApiService.importFile(formData, fileType).done(function (response) {
            _this.AeviGrid.AeviAjaxPreloader.hide();
            if (!_.isNull(response.ErrorMessage)) {
                var modal = new AeviModal(_this.AeviGrid);
                modal.setContent({
                    title: _this.AeviGrid.AeviLocalization.translate('warning'),
                    text: response.ErrorMessage
                });
                modal.show();
                return;
            }
            if (_.isString(response.Data)) {
                var event = new Event('fakePaste');
                _this.AeviGrid.AeviClipboard.setClipboardMode();
                _this.AeviGrid.AeviClipboard.paste(event, response.Data);
            }
        }).error(function (err) {
            _this.AeviGrid.AeviAjaxPreloader.hide();
        });
    };
    AeviDataService.prototype.showBlockModal = function (response) {
        var promise = $.Deferred();
        var modal = new AeviModal(this.AeviGrid, function (confirmSetMeAsBlockerUser) {
            if (confirmSetMeAsBlockerUser) {
                promise.resolve();
            }
            else {
                promise.reject();
            }
        });
        var blockingUserName = (aeviIsUndefinedOrNull(response.ExtendedData)) ? 'empty' : response.ExtendedData.UserName;
        modal.setContent({
            title: this.AeviGrid.AeviLocalization.translate('warning'),
            text: this.AeviGrid.AeviLocalization.translate('entity_is_blocked1') + '<strong>' + blockingUserName + '</strong>' + this.AeviGrid.AeviLocalization.translate('entity_is_blocked2'),
            buttons: ['aeviModalAccept', 'aeviModalDenied'],
            showCloseButton: false
        });
        modal.show();
        return promise;
    };
    AeviDataService.prototype.showDataChangedModal = function (response) {
        var promise = $.Deferred();
        var modal = new AeviModal(this.AeviGrid, function (confirmReloadData) {
            if (confirmReloadData) {
                promise.resolve();
            }
            else {
                promise.reject();
            }
        });
        var blockingUserName = (aeviIsUndefinedOrNull(response.ExtendedData)) ? 'empty' : response.ExtendedData.UserName;
        modal.setContent({
            title: this.AeviGrid.AeviLocalization.translate('warning'),
            text: this.AeviGrid.AeviLocalization.translate('entity_is_changed1') + '<strong>' + blockingUserName + '</strong>' + this.AeviGrid.AeviLocalization.translate('entity_is_changed2'),
            buttons: ['aeviModalAccept', 'aeviModalDenied'],
            showCloseButton: false
        });
        modal.show();
        return promise;
    };
    AeviDataService.prototype.getBlockStateCommand = function () {
        var _this = this;
        var promise = $.Deferred();
        this.AeviGrid.AeviDataService.AeviApiService.getDataEntityStatus()
            .done(function (response) {
            switch (response.Status) {
                case MyRetail.IAeviResponseStatus.Blocked:
                    _this.AeviGrid.AeviDataService.showBlockModal(response)
                        .done(function () {
                        _this.AeviGrid.AeviDataService.AeviApiService.setDataEntityBlockStatus().done(function () {
                            promise.resolve(MyRetail.IAeviBlockStateCommand.CONTINUE);
                        });
                    })
                        .fail(function () {
                        promise.reject(MyRetail.IAeviBlockStateCommand.LOCK);
                    });
                    break;
                case MyRetail.IAeviResponseStatus.Changed:
                    _this.AeviGrid.AeviDataService.showDataChangedModal(response)
                        .done(function () {
                        _this.AeviGrid.AeviDataService.AeviApiService.setDataEntityBlockStatus().done(function () {
                            promise.reject(MyRetail.IAeviBlockStateCommand.RELOAD);
                        });
                    })
                        .fail(function () {
                        promise.reject(MyRetail.IAeviBlockStateCommand.LOCK);
                    });
                    break;
                default:
                    _this.AeviGrid.AeviDataService.AeviApiService.setDataEntityBlockStatus().done(function () {
                        promise.resolve(MyRetail.IAeviBlockStateCommand.CONTINUE);
                    });
                    break;
            }
        });
        return promise;
    };
    return AeviDataService;
}());
var AeviUser = (function () {
    function AeviUser(roles) {
        this.roles = {
            Read: roles.CanRead,
            Write: roles.CanWrite,
            NativeWrite: roles.CanWrite,
            Create: roles.CanCreate,
            Delete: roles.CanDelete
        };
    }
    return AeviUser;
}());
var AeviTableDescription = (function () {
    function AeviTableDescription(description, aeviDataRepository) {
        this.AeviDataRepository = aeviDataRepository;
        this.description = description;
        this.fixedSize = description.FixedSize;
        this.maxRowCount = description.MaxRowCount;
        this.readOnly = description.ReadOnly;
        this.realReadOnly = description.ReadOnly;
        this.allowPasteFromExcel = description.AllowPasteFromExcel;
        this.saveModeExcel = description.SaveModeExcel;
        this.columnsLength = description.Columns.length;
        this.sortInfos = null;
        this.columns = description.Columns;
        this.entityType = this.AeviDataRepository.AeviGrid.entityType;
        this.fakeColumnsLength = 0;
        this.startFakeColumnsLength = 0;
        this.columnsAction = [];
        this.allowLoadDataImmediately = description.AllowLoadDataImmediately;
        this.allowFilter = (!_.isNull(description.Filters) && description.Filters.length) ? true : false;
    }
    AeviTableDescription.prototype.getDescription = function () {
        return this.description;
    };
    AeviTableDescription.prototype.getColumnHeader = function (index) {
        var columnHeader = this.columns[index];
        if (_.isUndefined(columnHeader))
            return null;
        return columnHeader;
    };
    AeviTableDescription.prototype.getColumnHeaderByName = function (name) {
        for (var i = 0; i < this.getColumnsLength(); i++) {
            if (this.getColumnNameByIndex(i) === name) {
                return this.getColumnHeader(i);
            }
        }
        return null;
    };
    AeviTableDescription.prototype.getColumnName = function (columnHeader) {
        if (aeviIsUndefinedOrNull(columnHeader)) {
            this.AeviDataRepository.AeviGridApi.log('AeviTableDescription.getColumnName(), parameter "columnHeader" is undefined or null.');
            return null;
        }
        return columnHeader.ColumnName;
    };
    AeviTableDescription.prototype.getColumnNameByIndex = function (columnIndex) {
        var header = this.getColumnHeader(columnIndex);
        return this.getColumnName(header);
    };
    AeviTableDescription.prototype.getColumnDefaultValue = function (columnHeader) {
        return columnHeader.DefaultValue;
    };
    AeviTableDescription.prototype.getColumnVisibility = function (columnHeader) {
        return columnHeader.Visible;
    };
    AeviTableDescription.prototype.getColumnAction = function (columnHeader) {
        return columnHeader.EntityActionDefinition;
    };
    AeviTableDescription.getColumnActionType = function (columnHeader) {
        return columnHeader.ActionType;
    };
    AeviTableDescription.prototype.getColumnActionByColumnIndex = function (columnIndex) {
        var column = this.getColumnHeader(columnIndex);
        return this.getColumnAction(column);
    };
    AeviTableDescription.prototype.getColumnIndexByName = function (name) {
        for (var i = 0; i < this.columns.length; i++) {
            var columnName = this.getColumnName(this.columns[i]);
            if (columnName === name)
                return i;
        }
        return null;
    };
    AeviTableDescription.prototype.getDisplayType = function (columnHeader) {
        return this.getColumnDisplayType(columnHeader);
    };
    AeviTableDescription.prototype.getColumnDisplayType = function (columnHeader) {
        return columnHeader.DisplayType;
    };
    AeviTableDescription.prototype.getColumnStringDisplayType = function (columnDisplayType) {
        return this.AeviDataRepository.AeviGrid.AeviConsts.dataTypes[columnDisplayType];
    };
    AeviTableDescription.prototype.getColumnStringDisplayTypeByIndex = function (columnIndex) {
        var header = this.getColumnHeader(columnIndex);
        if (_.isNull(header)) {
            return null;
        }
        var displayType = this.getColumnDisplayType(header);
        return this.getColumnStringDisplayType(displayType);
    };
    AeviTableDescription.prototype.getColumns = function () {
        return this.columns;
    };
    AeviTableDescription.prototype.getColumnsLength = function () {
        return this.getColumns().length;
    };
    AeviTableDescription.prototype.isVisible = function (columnHeader) {
        return !!columnHeader.Visible;
    };
    AeviTableDescription.prototype.isRequired = function (column) {
        return column.Required;
    };
    AeviTableDescription.prototype.isRequiredByColumnIndex = function (columnIndex) {
        var column = this.getColumnHeader(columnIndex);
        return this.isRequired(column);
    };
    AeviTableDescription.prototype.isReadOnly = function () {
        return !!this.realReadOnly;
    };
    AeviTableDescription.prototype.isColumnReadOnly = function (column) {
        return column.ReadOnly;
    };
    AeviTableDescription.prototype.isFixed = function () {
        return (!_.isNull(this.fixedSize));
    };
    AeviTableDescription.prototype.isReport = function () {
        return !!this.entityType;
    };
    AeviTableDescription.prototype.isAllowedPastingFromExcel = function () {
        return this.allowPasteFromExcel;
    };
    AeviTableDescription.prototype.hasColumnDefaultValue = function (columnHeader) {
        if (aeviIsUndefinedOrNull(columnHeader)) {
            this.AeviDataRepository.AeviGridApi.log('AeviTableDescription.hasColumnDefaultValue(), "columnHeader" is undefined or null.');
            return false;
        }
        return !aeviIsUndefinedOrNull(columnHeader.DefaultValue);
    };
    AeviTableDescription.prototype.getFirstVisibleColumn = function () {
        if (!aeviIsUndefinedOrNull(this.firstVisibleColumn))
            return this.firstVisibleColumn;
        for (var i = 0; i < this.columns.length; i++) {
            var columnHeader = this.getColumnHeader(i);
            if (_.isNull(columnHeader))
                continue;
            if (this.getColumnVisibility(columnHeader) === true) {
                this.firstVisibleColumn = i;
                return this.firstVisibleColumn;
            }
        }
        return null;
    };
    AeviTableDescription.prototype.getLastVisibleColumn = function () {
        if (!aeviIsUndefinedOrNull(this.lastVisibleColumn))
            return this.lastVisibleColumn;
        for (var i = this.columns.length; i > -1; i--) {
            var columnHeader = this.getColumnHeader(i);
            if (_.isNull(columnHeader))
                continue;
            if (this.getColumnVisibility(columnHeader) === true) {
                this.lastVisibleColumn = i;
                return this.lastVisibleColumn;
            }
        }
        return null;
    };
    AeviTableDescription.prototype.setExtraColumn = function (column) {
        return this.fakeSetExtraColumn(column);
        if (column.ColumnName === 'RowNumber') {
            this.columns.splice(0, 0, column);
        }
        else {
            this.columns.push(column);
        }
        this.fakeColumnsLength++;
    };
    AeviTableDescription.prototype.fakeSetExtraColumn = function (column) {
        if (column.ColumnName === 'RowNumber') {
            this.columns.splice(0, 0, column);
            this.startFakeColumnsLength++;
        }
        else if (column.ColumnName === 'FormAction') {
            this.columns.splice(1, 0, column);
            this.startFakeColumnsLength++;
        }
        else {
            this.columns.push(column);
        }
        this.fakeColumnsLength++;
    };
    AeviTableDescription.prototype.setColumnsPositions = function () {
        var keyColumnName = this.description.KeyColumnName;
        var errorCodeColumnName = this.description.ErrorCodeColumnName;
        var errorMessageColumnName = this.description.ErrorMessageColumnName;
        var referenceSettingsColumnName = this.description.ReferenceSettingsColumnName;
        var length = this.getColumnsLength();
        this.columnPositions = {};
        for (var i = 0; i < length; i++) {
            var columnHeader = this.getColumnHeader(i);
            if (_.isNull(columnHeader))
                continue;
            var columnName = this.getColumnName(columnHeader);
            switch (columnName) {
                case 'RowNumber':
                    this.columnPositions.rowNumber = i;
                    break;
                case keyColumnName:
                    this.columnPositions.keyColumnName = i;
                    break;
                case errorCodeColumnName:
                    this.columnPositions.errorCodeColumnName = i;
                    break;
                case errorMessageColumnName:
                    this.columnPositions.errorMessageColumnName = i;
                    break;
                case referenceSettingsColumnName:
                    this.columnPositions.referenceSettingsColumnName = i;
                    break;
                case 'SelectedCellsArray':
                    this.columnPositions.selectedCellsArray = i;
                    break;
                case 'InvalidCellsArray':
                    this.columnPositions.invalidCellsArray = i;
                    break;
                case 'Status':
                    this.columnPositions.statusColumn = i;
                    break;
                case 'DefaultOrder':
                    this.columnPositions.defaultOrder = i;
                    break;
            }
        }
    };
    AeviTableDescription.prototype.getGuidId = function () {
        if (_.isUndefined(this.columnPositions) || _.isUndefined(this.columnPositions.keyColumnName))
            return null;
        return this.columnPositions.keyColumnName + 1;
    };
    AeviTableDescription.prototype.getStatusId = function () {
        if (aeviIsUndefinedOrNull(this.columnPositions) || aeviIsUndefinedOrNull(this.columnPositions.statusColumn)) {
            return null;
        }
        return this.columnPositions.statusColumn + 1;
    };
    AeviTableDescription.prototype.getSelectedCellsId = function () {
        if (_.isUndefined(this.columnPositions) || _.isUndefined(this.columnPositions.selectedCellsArray))
            return null;
        return this.columnPositions.selectedCellsArray + 1;
    };
    AeviTableDescription.prototype.getInvalidCellsId = function () {
        if (_.isUndefined(this.columnPositions) || _.isUndefined(this.columnPositions.invalidCellsArray))
            return null;
        return this.columnPositions.invalidCellsArray + 1;
    };
    AeviTableDescription.prototype.getGuidIndex = function () {
        return this.columnPositions.keyColumnName;
    };
    AeviTableDescription.prototype.getInvalidCellsIndex = function () {
        return this.columnPositions.invalidCellsArray;
    };
    AeviTableDescription.prototype.getSelectedCellsIndex = function () {
        return this.columnPositions.selectedCellsArray;
    };
    AeviTableDescription.prototype.getStatusIndex = function () {
        return this.columnPositions.statusColumn;
    };
    AeviTableDescription.prototype.getDefaultOrderIndex = function () {
        return this.columnPositions.defaultOrder;
    };
    AeviTableDescription.prototype.getRowIndex = function () {
        console.log("caller is " + arguments.callee.caller);
        this.AeviDataRepository.AeviGridApi.log('AeviTableDescription.getRowIndex() is deprecated, use getRowNumberIndex()!!!');
        return this.getRowNumberIndex();
    };
    AeviTableDescription.prototype.getRowNumberIndex = function () {
        return 0;
    };
    AeviTableDescription.prototype.getFirstDataColumnIndex = function () {
        return this.startFakeColumnsLength;
    };
    AeviTableDescription.prototype.getErrorMessageIndex = function () {
        return this.columnPositions.errorMessageColumnName;
    };
    AeviTableDescription.prototype.getReferenceSettingsColumnNameIndex = function () {
        if (_.isUndefined(this.columnPositions) || _.isUndefined(this.columnPositions.referenceSettingsColumnName))
            return null;
        return this.columnPositions.referenceSettingsColumnName;
    };
    AeviTableDescription.prototype.getVisibleCellsLength = function () {
        if (aeviIsUndefinedOrNull(this.visibleColumnsLength)) {
            this.visibleColumnsLength = 0;
            var columnsLength = this.getColumnsLength();
            for (var i = 0; i < columnsLength; i++) {
                var header = this.getColumnHeader(i);
                if (this.isVisible(header))
                    this.visibleColumnsLength++;
            }
        }
        return this.visibleColumnsLength;
    };
    AeviTableDescription.prototype.getCountOfPossibleColumnsToCopy = function () {
        var count = 0;
        var columnsLenght = this.getColumnsLength();
        if (!columnsLenght) {
            return 0;
        }
        for (var i = 0; i < columnsLenght; i++) {
            var column = this.getColumnHeader(i);
            if (aeviIsUndefinedOrNull(column)) {
                continue;
            }
            var canIncrease = false;
            var displayType = this.getDisplayType(column);
            if (column.Visible === true) {
                canIncrease = true;
                if (displayType === MyRetail.IAeviDisplayType.Image) {
                    canIncrease = false;
                }
            }
            if (!canIncrease) {
                continue;
            }
            var columnName = this.getColumnName(column);
            switch (columnName) {
                case 'RowNumber':
                case 'FormAction':
                case 'SelectedCellsArray':
                case 'InvalidCellsArray':
                case 'Status':
                case 'DefaultOrder':
                case this.description.KeyColumnName:
                case this.description.ErrorCodeColumnName:
                case this.description.ErrorMessageColumnName:
                case this.description.ReferenceSettingsColumnName:
                    canIncrease = false;
                    break;
                default:
                    canIncrease = true;
                    break;
            }
            if (canIncrease) {
                count++;
            }
        }
        return count;
    };
    AeviTableDescription.prototype.setSortInfos = function (sortInfos) {
        this.sortInfos = sortInfos;
    };
    AeviTableDescription.prototype.getSortInfos = function () {
        return this.sortInfos;
    };
    AeviTableDescription.prototype.getJSONSortInfos = function () {
        return JSON.stringify(this.getSortInfos());
    };
    AeviTableDescription.prototype.setColumnActionTypes = function () {
        for (var i = 0; i < this.getColumnsLength(); i++) {
            var column = this.getColumnHeader(i);
            var actionType = AeviTableDescription.getColumnActionType(column);
            if (aeviIsUndefinedOrNull(actionType)) {
                continue;
            }
            var columnActionType = {
                columnIndex: i,
                columnActionType: actionType
            };
            this.columnsAction.push(columnActionType);
        }
    };
    AeviTableDescription.prototype.hasColumnAction = function (columnIndex) {
        for (var i = 0; i < this.columnsAction.length; i++) {
            if (this.columnsAction[i].columnIndex === columnIndex)
                return true;
        }
        return false;
    };
    AeviTableDescription.prototype.getColumnActionType = function (columnIndex) {
        for (var i = 0; i < this.columnsAction.length; i++) {
            if (this.columnsAction[i].columnIndex === columnIndex) {
                return this.columnsAction[i].columnActionType;
            }
        }
        return null;
    };
    AeviTableDescription.prototype.fakeSetColumnActionTypes = function () {
        for (var i = 0; i < this.getColumnsLength(); i++) {
            if (i === 6) {
                var actionType = MyRetail.IAeviColumnActionType.NegativeRowFormating;
                if (aeviIsUndefinedOrNull(actionType)) {
                    continue;
                }
                var columnActionType = {
                    columnIndex: i,
                    columnActionType: actionType
                };
                this.columnsAction.push(columnActionType);
            }
        }
    };
    return AeviTableDescription;
}());
{ }
var AeviTableData = (function () {
    function AeviTableData(aeviDataRepository) {
        this.AeviDataRepository = aeviDataRepository;
        this.AeviTableDescription = this.AeviDataRepository.AeviTableDescription;
        this.AeviStatus = new AeviStatus(this.AeviDataRepository, this.AeviDataRepository.AeviGridApi);
    }
    AeviTableData.prototype.init = function (data) {
        this.data = data;
        this.cachedVisibleRecordsLength = this.data.length;
    };
    AeviTableData.prototype.setExtraColumn = function (value) {
        for (var i = 0; i < this.getRecordsLength(); i++) {
            if (value === 'RowNumber') {
                this.data[i].unshift(i);
            }
            else if (value === 'FormAction') {
                this.data[i].unshift(i);
            }
            else {
                this.data[i].push(value);
            }
        }
    };
    AeviTableData.prototype.deleteAllData = function () {
        this.data = [];
    };
    AeviTableData.prototype.getData = function () {
        return this.data;
    };
    AeviTableData.prototype.cloneData = function () {
        return this.data.slice();
    };
    AeviTableData.prototype.setDefaultOrder = function () {
        var dataLength = this.getRecordsLength();
        var orderColumnIndex = this.AeviTableDescription.getColumnIndexByName('DefaultOrder');
        for (var i = 0; i < dataLength; i++) {
            this.updateRecordCellValue(i, orderColumnIndex, i);
        }
    };
    AeviTableData.prototype.addHiddenRecords = function () {
        if (this.AeviTableDescription.isReport() || this.AeviDataRepository.AeviGridApi.getMode().clipboard === 1)
            return;
        var i;
        var poisedRows = 201;
        var realDataLen = this.getVisibleRecordsLength() + 1;
        for (i = this.getVisibleRecordsLength() + 1; i < realDataLen + poisedRows; i++) {
            var status = 'hidden';
            if (i === realDataLen)
                status = 'newrow';
            this.insertRecord({
                Index: i - 1,
                Status: status,
                isAddedByGrid: true
            });
        }
        this.updateVisibleRecordsLength();
    };
    AeviTableData.prototype.insertRecord = function (properties) {
        if (!_.isNull(this.AeviTableDescription.fixedSize))
            return false;
        if (aeviIsUndefinedOrNull(this.AeviGuidGenerator)) {
            this.AeviGuidGenerator = new AeviGuidGenerator();
        }
        var row = [];
        for (var i = 0; i < this.AeviTableDescription.getColumnsLength(); i++) {
            if (i === this.AeviTableDescription.getGuidIndex()) {
                row.push(this.AeviGuidGenerator.generate());
            }
            else {
                var returnEmptyValue = false;
                row.push(this.getDefaultValue(i, properties, returnEmptyValue));
            }
        }
        this.cachedVisibleRecordsLength++;
        var index = properties.Index + 1;
        this.data.splice(index, 0, row);
        this.updateRowNumber();
        this.updateVisibleRecordsLength();
    };
    AeviTableData.prototype.getDefaultValue = function (index, properties, returnEmptyValue) {
        var header = this.AeviTableDescription.getColumnHeader(index);
        return this.getDefaultCellValue(header, properties, returnEmptyValue);
    };
    AeviTableData.prototype.getDefaultCellValue = function (header, properties, returnEmptyValue) {
        if (returnEmptyValue === false && this.AeviTableDescription.hasColumnDefaultValue(header)) {
            return this.AeviTableDescription.getColumnDefaultValue(header);
        }
        var displayType = this.AeviTableDescription.getDisplayType(header);
        var displayStringType = this.AeviTableDescription.getColumnStringDisplayType(displayType);
        switch (displayStringType) {
            case 'Text':
            case 'Hyperlink':
            case 'RegularExpression':
                return '';
            case 'Number':
            case 'IntegerNumber':
            case 'Currency':
            case 'DateTime':
            case 'ShortDate':
            case 'ShortTime':
            case 'Boolean':
            case 'Enum':
                return null;
            case 'Image':
                if (!_.isNull(properties) && properties.ImageBlocked) {
                    return AeviImage.getDefaultBlockedValue();
                }
                return AeviImage.getDefaultValue();
            case 'SelectedCellsArray':
                return [];
            case 'InvalidCellsArray':
                var value = [];
                for (var i = 0; i < this.AeviTableDescription.getColumnsLength(); i++) {
                    value.push({
                        hard: false,
                        valid: true,
                        message: null
                    });
                }
                return value;
            case 'Status':
                return (!_.isUndefined(properties.Status)) ? properties.Status : null;
        }
        return null;
    };
    AeviTableData.prototype.getVisibleRecordsLength = function () {
        return this.updateVisibleRecordsLength();
    };
    AeviTableData.prototype.updateVisibleRecordsLength = function () {
        this.visibleRecordsLength = 0;
        for (var i = 0; i < this.data.length; i++) {
            if (this.AeviStatus.is(i, 'render'))
                this.visibleRecordsLength += 1;
        }
        return this.visibleRecordsLength;
    };
    AeviTableData.prototype.getRecordByRowIndex = function (rowIndex) {
        return this.data[rowIndex];
    };
    AeviTableData.prototype.getRecordCellValue = function (rowIndex, cellIndex) {
        if (aeviIsUndefinedOrNull(this.data[rowIndex])) {
            return null;
        }
        return this.data[rowIndex][cellIndex];
    };
    AeviTableData.prototype.updateRecordCellValue = function (rowIndex, cellIndex, val) {
        this.data[rowIndex][cellIndex] = val;
    };
    AeviTableData.prototype.getRecords = function (from, to) {
        var data = [];
        if (this.data.length > 0) {
            for (var i = from; i < to; i++) {
                if (!aeviIsUndefinedOrNull(this.data[i])) {
                    data.push(this.data[i].slice());
                }
            }
        }
        return data;
    };
    AeviTableData.prototype.getCloneRecordByRowId = function (rowIndex) {
        if (aeviIsUndefinedOrNull(rowIndex)) {
            this.AeviDataRepository.AeviGridApi.log('rowIndex is undefined or null');
            return null;
        }
        if (aeviIsUndefinedOrNull(this.data[rowIndex])) {
            this.AeviDataRepository.AeviGridApi.log('AeviDataRepository.getCloneRecordByRowId(). Clone record is undefined or null');
            return null;
        }
        return this.data[rowIndex].slice();
    };
    AeviTableData.prototype.updateRowNumber = function () {
        var length = this.getRecordsLength();
        for (var i = 0; i < length; i++)
            this.data[i].splice(0, 1, i);
    };
    AeviTableData.prototype.getRecordsLength = function () {
        return this.data.length;
    };
    AeviTableData.prototype.getVisibleAndNewRecordsLength = function () {
        var length = 0;
        for (var i = 0; i < this.data.length; i++) {
            if (this.AeviStatus.is(i, 'render') || this.AeviStatus.is(i, 'newrow'))
                length++;
        }
        return length;
    };
    AeviTableData.prototype.setSelectedPositions = function (from, to) {
        this.removeSelectedPositions();
        var firstRow = from.rowIndex;
        var firstCell = from.cellIndex;
        var lastRow = to.rowIndex;
        var lastCell = to.cellIndex;
        var helper = null;
        if (lastRow < firstRow) {
            helper = firstRow;
            firstRow = lastRow;
            lastRow = helper;
        }
        if (lastCell < firstCell) {
            helper = firstCell;
            firstCell = lastCell;
            lastCell = helper;
        }
        var listOfSelectedCells = [];
        for (var x = firstCell; x <= lastCell; x++) {
            listOfSelectedCells.push(x);
        }
        var selectedCellsIndex = this.AeviTableDescription.getSelectedCellsIndex();
        for (var i = firstRow; i < lastRow + 1; i++) {
            if (!_.isUndefined(this.data[i]))
                this.data[i][selectedCellsIndex] = listOfSelectedCells;
        }
    };
    AeviTableData.prototype.removeSelectedPositions = function () {
        var length = this.getRecordsLength();
        var selectedCellsIndex = this.AeviTableDescription.getSelectedCellsIndex();
        for (var i = 0; i < length; i++) {
            this.updateRecordCellValue(i, selectedCellsIndex, []);
            this.AeviStatus.removeByRowIndex(i, 'selected');
        }
    };
    AeviTableData.prototype.setRowValidityInfo = function (rowIndex, validityInfos) {
        for (var i = 0; i < validityInfos.length; i++)
            this.setCellValidityInfo(validityInfos[i], rowIndex, i);
        this.checkValidityStatus(rowIndex);
    };
    AeviTableData.prototype.setCellValidityInfo = function (validityInfo, rowIndex, cellIndex) {
        var invalidRecordsIndex = this.AeviDataRepository.AeviTableDescription.getInvalidCellsIndex();
        var cellValidityInfo = this.getRecordCellValue(rowIndex, invalidRecordsIndex).slice();
        cellValidityInfo[cellIndex] = validityInfo;
        this.updateRecordCellValue(rowIndex, invalidRecordsIndex, cellValidityInfo);
    };
    AeviTableData.prototype.checkValidityStatus = function (rowIndex) {
        this.AeviStatus.removeInvalidStatuses(rowIndex);
        var invalidRecordsIndex = this.AeviDataRepository.AeviTableDescription.getInvalidCellsIndex();
        var rowValidityInfo = this.getRecordCellValue(rowIndex, invalidRecordsIndex).slice();
        var isInvalid = false;
        var isSoftInvalid = false;
        for (var i = 0; i < rowValidityInfo.length; i++) {
            var cellValidityInfo = rowValidityInfo[i];
            if (cellValidityInfo.valid === false) {
                if (cellValidityInfo.hard === true)
                    isInvalid = true;
                else
                    isSoftInvalid = true;
            }
        }
        if (isInvalid)
            this.AeviStatus.addByRowIndex(rowIndex, 'invalid');
        if (isSoftInvalid)
            this.AeviStatus.addByRowIndex(rowIndex, 'softInvalid');
    };
    AeviTableData.prototype.getApiDataByRowIndex = function (rowIndex) {
        var row = this.getRecordByRowIndex(rowIndex);
        var apiData = [];
        var apiRowData = [];
        for (var i = this.AeviTableDescription.startFakeColumnsLength; i < row.length; i++) {
            var apiCellData = row[i];
            var column = this.AeviTableDescription.getColumnHeader(i);
            if (!_.isNull(column)) {
                var columnName = this.AeviTableDescription.getColumnName(column);
                switch (columnName) {
                    case 'SelectedCellsArray':
                    case 'InvalidCellsArray':
                    case 'Status':
                    case 'DefaultOrder':
                        continue;
                    default:
                        break;
                }
                var columnDisplayType = this.AeviTableDescription.getDisplayType(column);
                var columnStringDisplayType = this.AeviTableDescription.getColumnStringDisplayType(columnDisplayType);
                switch (columnStringDisplayType) {
                    case 'Text':
                    case 'Number':
                    case 'IntegerNumber':
                    case 'Currency':
                    case 'DateTime':
                    case 'ShortDate':
                    case 'ShortTime':
                    case 'Boolean':
                    case 'Enum':
                        if (apiCellData === '')
                            apiCellData = null;
                        break;
                    case 'Image':
                        if (_.isEmpty(apiCellData) || apiCellData === 'null')
                            apiCellData = null;
                        break;
                }
                apiCellData = (apiCellData === 'null') ? null : apiCellData;
                apiRowData.push(apiCellData);
            }
        }
        apiData.push(apiRowData);
        return apiData;
    };
    AeviTableData.prototype.getInvalidRecordsIndexes = function () {
        var invalidRowsIndexes = [];
        var dataLength = this.data.length;
        for (var i = 0; i < dataLength; i++) {
            if (this.AeviStatus.is(i, 'invalid'))
                invalidRowsIndexes.push(i);
        }
        return invalidRowsIndexes;
    };
    AeviTableData.prototype.sortInvalid = function () {
        var clipboardMode = this.AeviDataRepository.AeviGrid.mode.clipboard;
        if (clipboardMode === 0) {
            this.removeHiddenRecords();
        }
        var invalidDataGuids = [];
        var invalidData = [];
        var dataLength = this.getRecordsLength();
        for (var i = 0; i < dataLength; i++) {
            if (this.AeviStatus.is(i, 'invalid')) {
                invalidData.push(this.data[i]);
                invalidDataGuids.push(this.getGuidByRowIndex(i));
            }
        }
        this.deleteData(invalidDataGuids);
        for (var i = 0; i < invalidData.length; i++) {
            this.data.splice(i, 0, invalidData[i]);
        }
        if (clipboardMode === 0) {
            this.addHiddenRecords();
        }
        this.updateRowNumber();
    };
    AeviTableData.prototype.removeHiddenRecords = function () {
        if (this.AeviTableDescription.isReport())
            return;
        var guids = [];
        var dataLength = this.getRecordsLength();
        var guidIndex = this.AeviTableDescription.getGuidIndex();
        for (var rowIndex = 0; rowIndex < dataLength; rowIndex++) {
            var status = this.AeviStatus.getByRowIndex(rowIndex);
            if (status === 'newrow' || (this.AeviStatus.is(rowIndex, 'hidden') && !this.AeviStatus.is(rowIndex, 'sortedRow'))) {
                if (!this.AeviStatus.is(rowIndex, 'render')) {
                    guids.push(this.getRecordCellValue(rowIndex, guidIndex));
                }
            }
        }
        this.deleteData(guids);
    };
    AeviTableData.prototype.deleteData = function (guids) {
        var guidsLength = guids.length;
        for (var i = 0; i < guidsLength; i++) {
            this.updateRowNumber();
            var guid = guids[i];
            var rowIndex = this.getRecordIndexByGuid(guid);
            this.data.splice(rowIndex, 1);
            this.cachedVisibleRecordsLength--;
        }
        this.updateRowNumber();
        this.updateVisibleRecordsLength();
    };
    AeviTableData.prototype.getRecordIndexByGuid = function (guid) {
        var row = this.getRecord(guid);
        return row[this.AeviTableDescription.getRowNumberIndex()];
    };
    AeviTableData.prototype.getRecord = function (guid) {
        var guidIndex = this.AeviTableDescription.getGuidIndex();
        var dataLength = this.getRecordsLength();
        for (var rowIndex = 0; rowIndex < dataLength; rowIndex++) {
            if (this.getRecordCellValue(rowIndex, guidIndex) === guid)
                return this.data[rowIndex];
        }
        return null;
    };
    AeviTableData.prototype.getGuidByRowIndex = function (rowIndex) {
        var guid = this.getRecordCellValue(rowIndex, this.AeviTableDescription.getGuidIndex());
        if (aeviIsUndefinedOrNull(guid)) {
            this.AeviDataRepository.AeviGridApi.log('AeviTableData.getGuidByRowIndex(), guid is undefined or null.');
            return null;
        }
        return guid;
    };
    AeviTableData.prototype.getErrorMessage = function (identification) {
        var errorMessageIndex = this.AeviTableDescription.getErrorMessageIndex();
        var rowIndex = null;
        if (!_.isUndefined(identification.guid))
            rowIndex = this.getRecordIndexByGuid(identification.guid);
        if (!_.isUndefined(identification.rowIndex))
            rowIndex = identification.rowIndex;
        return this.getRecordCellValue(rowIndex, errorMessageIndex);
    };
    AeviTableData.prototype.setErrorMessage = function (rowIndex, message) {
        this.updateRecordCellValue(rowIndex, this.AeviTableDescription.getErrorMessageIndex(), message);
    };
    AeviTableData.getRecordKey = function (identification) {
        var recordKey = null;
        if (!_.isUndefined(identification.rowIndex))
            recordKey = identification.rowIndex;
        if (!_.isUndefined(identification.guid))
            recordKey = identification.guid;
        return recordKey;
    };
    AeviTableData.prototype.getSelectedRows = function () {
        var selectedRows = [];
        for (var i = 0; i < this.data.length; i++) {
            if (this.AeviStatus.is(i, 'selected')) {
                if (!this.AeviStatus.is(i, 'newrow') && !this.AeviStatus.is(i + 1, 'hidden')) {
                    selectedRows.push(i);
                }
            }
        }
        return selectedRows;
    };
    AeviTableData.prototype.setSelectedRows = function (rows, selectAllCells) {
        var selectedCellsIndex = this.AeviTableDescription.getSelectedCellsIndex();
        var statusIndex = this.AeviTableDescription.getStatusIndex();
        var recordsLength = this.getRecordsLength();
        for (var i = 0; i < recordsLength; i++) {
            if (selectAllCells === true) {
                this.data[i][selectedCellsIndex] = _.range(0, this.AeviTableDescription.getColumnsLength());
            }
            else {
                if (_.contains(rows, i)) {
                    this.data[i][selectedCellsIndex] = _.range(0, this.AeviTableDescription.getColumnsLength());
                    this.data[i][statusIndex] += (this.data[i][statusIndex].indexOf('selected') === -1) ? ' selected ' : '';
                }
                else {
                    this.data[i][selectedCellsIndex] = [];
                    this.data[i][statusIndex] = this.data[i][statusIndex].replace('selected', '');
                }
            }
        }
    };
    AeviTableData.prototype.setSelectedColumns = function (columnsIndexes) {
        var recordsLength = this.getRecordsLength();
        var selectedCellsIndex = this.AeviTableDescription.getSelectedCellsIndex();
        for (var i = 0; i < recordsLength; i++) {
            this.data[i][selectedCellsIndex] = columnsIndexes;
        }
    };
    AeviTableData.prototype.getApiDataWithColumnNames = function (data) {
        var apiData = {};
        for (var i = 1; i < this.AeviTableDescription.getColumnsLength() - this.AeviTableDescription.fakeColumnsLength + 1; i++) {
            var header = this.AeviTableDescription.getColumnHeader(i);
            var displayType = this.AeviTableDescription.getColumnDisplayType(header);
            var stringDisplayType = this.AeviTableDescription.getColumnStringDisplayType(displayType);
            var cellData = data[i];
            switch (stringDisplayType) {
                case 'Text':
                case 'Number':
                case 'IntegerNumber':
                case 'Currency':
                case 'DateTime':
                case 'ShortDate':
                case 'ShortTime':
                case 'Boolean':
                    if (cellData === '')
                        cellData = null;
                    break;
                case 'Enum':
                case 'Image':
                    if (_.isEmpty(cellData) || cellData === 'null')
                        cellData = null;
                    break;
            }
            if (stringDisplayType === 'Image')
                continue;
            cellData = (cellData === 'null') ? null : cellData;
            var object = {};
            var myVar = header.ColumnName;
            object[myVar] = cellData;
            apiData[header.ColumnName] = cellData;
        }
        return apiData;
    };
    AeviTableData.prototype.pasteOneCell = function (data, indexes) {
        this.updateRecordCellValue(indexes.rowIndex, indexes.cellIndex, data.Rows[0][0]);
        var status = this.AeviStatus.getByRowIndex(indexes.rowIndex);
        this.AeviStatus.addByRowIndex(indexes.rowIndex, 'update');
        if (status.indexOf('insert') !== -1 || status.indexOf('newrow') !== -1) {
            this.AeviStatus.removeByRowIndex(indexes.rowIndex, 'update');
            this.AeviStatus.addByRowIndex(indexes.rowIndex, 'insert');
        }
        this.validateCell(indexes.rowIndex, indexes.cellIndex);
    };
    AeviTableData.prototype.pasteRecords = function (data, indexes) {
        var startRowIndex = indexes.rowIndex;
        var startCellIndex = indexes.cellIndex;
        var rowsLength = data.Rows.length + startRowIndex;
        for (var i = startRowIndex; i < rowsLength; i++) {
            var status = 'render';
            if (aeviIsUndefinedOrNull(this.data[i])) {
                this.insertRecord({ Index: i - 1, Status: 'newrow', ImageBlocked: true });
                status += ' newrow';
                if (this.AeviTableDescription.isFixed())
                    continue;
            }
            var cellLength = data.Rows[i - startRowIndex].length + startCellIndex;
            var headerIndex = startCellIndex;
            for (var j = startCellIndex; j < cellLength; j++) {
                var cellValue = data.Rows[i - startRowIndex][j - startCellIndex];
                var header = this.AeviTableDescription.getColumnHeader(headerIndex);
                var displayType = this.AeviTableDescription.getColumnDisplayType(header);
                var displayTypeString = this.AeviTableDescription.getColumnStringDisplayType(displayType);
                switch (displayTypeString) {
                    case 'Enum':
                        var cell = new AeviBodyCell(this, this.AeviDataRepository.AeviGrid);
                        var values = cell.getValues(displayTypeString, header, cellValue);
                        cellValue = values.DataValue;
                        break;
                    case 'Currency':
                    case 'Number':
                        cellValue = cellValue.toString().replace(',', '.');
                        break;
                    case 'DateTime':
                    case 'ShortDate':
                    case 'ShortTime':
                        if (cellValue && cellValue.length > 0) {
                            var aeviDate = new AeviDate(this.AeviDataRepository.AeviGrid.AeviLocalization.getCulture(), header.DisplayType, cellValue);
                            cellValue = aeviDate.getIsoString();
                        }
                        break;
                    default:
                        break;
                }
                this.updateRecordCellValue(i, headerIndex, cellValue);
                if (this.isColumnHidden(headerIndex + 1))
                    headerIndex++;
                headerIndex++;
            }
            this.AeviStatus.updateByRowIndex(i, status);
        }
        this.validate();
    };
    AeviTableData.prototype.isColumnHidden = function (columnIndex) {
        var header = this.AeviTableDescription.getColumnHeader(columnIndex);
        return !this.AeviTableDescription.isVisible(header);
    };
    AeviTableData.prototype.validate = function () {
        var rowsLength = this.getRecordsLength();
        for (var rowIndex = 0; rowIndex < rowsLength; rowIndex++)
            this.validateRow(rowIndex);
    };
    AeviTableData.prototype.validateRow = function (rowIndex) {
        var cellsValidity = [];
        var cellsLength = this.AeviTableDescription.getColumnsLength() - this.AeviTableDescription.fakeColumnsLength;
        for (var cellIndex = 0; cellIndex < cellsLength; cellIndex++) {
            cellsValidity.push(this.validateCell(rowIndex, cellIndex));
        }
        this.setRowValidityInfo(rowIndex, cellsValidity);
    };
    AeviTableData.prototype.validateCell = function (rowIndex, cellIndex) {
        var validateValue = this.getRecordCellValue(rowIndex, cellIndex);
        var validatorFactory = new AeviValidatorFactory(this.AeviDataRepository.AeviGrid, this.AeviDataRepository.AeviLocalization, this.AeviTableDescription);
        validatorFactory.createValidator(validateValue, cellIndex);
        var validator = validatorFactory.getValidator();
        return validator.validate();
    };
    AeviTableData.prototype.getValidityInfo = function (rowIndex, cellIndex) {
        return this.data[rowIndex][this.AeviTableDescription.getInvalidCellsIndex()][cellIndex];
    };
    AeviTableData.prototype.exportDataToArray = function () {
        var columnNamesDoNotCopy = [];
        for (var i = 0; i < this.AeviTableDescription.getColumnsLength(); i++) {
            var column = this.AeviTableDescription.getColumnHeader(i);
            if (this.AeviTableDescription.getColumnName(column) === 'FormAction' || this.AeviTableDescription.getColumnName(column) === 'RowNumber') {
                columnNamesDoNotCopy.push(this.AeviTableDescription.getColumnName(column));
            }
            if (!this.AeviTableDescription.isVisible(column)) {
                columnNamesDoNotCopy.push(this.AeviTableDescription.getColumnName(column));
            }
        }
        var data = this.cloneData();
        var cellsLength = this.AeviTableDescription.getColumnsLength();
        var rows = [];
        for (var i = 0; i < data.length; i++) {
            var row = [];
            if (this.AeviStatus.is(i, 'hidden') || this.AeviStatus.is(i, 'newrow')) {
                continue;
            }
            for (var j = 0; j < cellsLength; j++) {
                var exportValue = null;
                var header = this.AeviTableDescription.getColumnHeader(j);
                var headerName = this.AeviTableDescription.getColumnName(header);
                var displayType = this.AeviTableDescription.getDisplayType(header);
                var displayTypeString = this.AeviTableDescription.getColumnStringDisplayType(displayType);
                if (!_.contains(columnNamesDoNotCopy, headerName)) {
                    var value = {
                        DataValue: data[i][j],
                        DisplayValue: data[i][j],
                        OutputValue: null
                    };
                    switch (displayTypeString) {
                        case 'Image':
                            continue;
                        case 'Enum':
                            var cell = new AeviBodyCell(this.AeviDataRepository, this.AeviDataRepository.AeviGrid);
                            var values = cell.getEnumValues(value.DisplayValue, header);
                            value.OutputValue = values.DisplayValue;
                            break;
                        case 'Currency':
                            var cell = new AeviBodyCell(this.AeviDataRepository, this.AeviDataRepository.AeviGrid);
                            var values = cell.getCurrencyValues(value, header);
                            if (!_.isNull(values.DisplayValue))
                                value.OutputValue = values.DisplayValue.toString().replace('.', ',');
                            break;
                        case 'DateTime':
                        case 'ShortDate':
                        case 'ShortTime':
                            if (value.DisplayValue && value.DisplayValue.length > 0) {
                                var aeviDate = new AeviDate(this.AeviDataRepository.AeviLocalization.getCulture(), displayType, value.DisplayValue);
                                value.DisplayValue = aeviDate.getString();
                                value.OutputValue = value.DisplayValue;
                            }
                            break;
                        case 'Number':
                            if (!_.isNull(value.DisplayValue))
                                value.OutputValue = value.DisplayValue.toString().replace('.', ',');
                            break;
                        default:
                            value.OutputValue = value.DisplayValue;
                            break;
                    }
                    if (_.isNull(value.OutputValue) || value.OutputValue === 'null') {
                        row.push('');
                        continue;
                    }
                    if (displayTypeString !== 'Currency') {
                        exportValue = this.wrapWithEquatingAndQuotes(value.OutputValue);
                    }
                    else {
                        exportValue = value.OutputValue;
                    }
                    row.push(exportValue);
                }
            }
            rows.push(row);
        }
        return rows;
    };
    AeviTableData.prototype.getDataAsCSV = function () {
        var csv = '';
        var data = this.exportDataToArray();
        var rowsLength = data.length;
        for (var i = 0; i < rowsLength; i++) {
            var isEndOfRow = false;
            var cellsLength = data[i].length;
            var dataString = data[i].join(";");
            for (var j = 0; j < cellsLength; j++) {
                if (j === cellsLength - 1) {
                    isEndOfRow = true;
                }
            }
            if (isEndOfRow) {
                csv += dataString + '\n';
            }
            else {
                csv += dataString;
            }
        }
        return csv;
    };
    AeviTableData.prototype.wrapWithEquatingAndQuotes = function (str) {
        return '="' + str + '"';
    };
    AeviTableData.prototype.getRecordsToClipboard = function () {
        var columnNamesDoNotCopy = [];
        for (var i = 0; i < this.AeviTableDescription.getColumnsLength(); i++) {
            var column = this.AeviTableDescription.getColumnHeader(i);
            if (this.AeviTableDescription.getColumnName(column) === 'FormAction' || this.AeviTableDescription.getColumnName(column) === 'RowNumber') {
                columnNamesDoNotCopy.push(this.AeviTableDescription.getColumnName(column));
            }
            if (!this.AeviTableDescription.isVisible(column)) {
                columnNamesDoNotCopy.push(this.AeviTableDescription.getColumnName(column));
            }
        }
        var selectedCellsIndex = this.AeviTableDescription.getSelectedCellsIndex();
        var rows = [];
        var cells = [];
        var data = this.cloneData();
        for (var i = 0; i < data.length; i++) {
            if (data[i][selectedCellsIndex].length && !this.AeviStatus.is(i, 'hidden')) {
                rows.push(i);
                cells = data[i][selectedCellsIndex];
            }
        }
        data = [];
        for (i = 0; i < rows.length; i++) {
            data.push(this.getRecordByRowIndex(rows[i]));
        }
        var startCellIndex = Math.min.apply(Math, cells);
        var finishCellIndex = Math.max.apply(Math, cells);
        var stringData = '';
        var isUserCopyOneCell = false;
        if (cells.length === 1 && rows.length === 1) {
            isUserCopyOneCell = true;
        }
        for (var i = 0; i < data.length; i++) {
            var isFirstCellComposed = false;
            for (var j = startCellIndex; j <= finishCellIndex; j++) {
                var header = this.AeviTableDescription.getColumnHeader(j);
                var headerName = this.AeviTableDescription.getColumnName(header);
                var displayType = this.AeviTableDescription.getDisplayType(header);
                var displayTypeString = this.AeviTableDescription.getColumnStringDisplayType(displayType);
                var firstChar = (j === startCellIndex) ? '' : '\t';
                var lastChar = (j === finishCellIndex) ? '\r\n' : '';
                if (_.contains(columnNamesDoNotCopy, headerName)) {
                }
                else {
                    if (isFirstCellComposed) {
                        stringData += firstChar;
                    }
                    isFirstCellComposed = true;
                    var value = {
                        DataValue: data[i][j],
                        DisplayValue: data[i][j],
                        OutputValue: null
                    };
                    switch (displayTypeString) {
                        case 'Image':
                            break;
                        case 'Enum':
                            var cell = new AeviBodyCell(this.AeviDataRepository, this.AeviDataRepository.AeviGrid);
                            var values = cell.getEnumValues(value.DisplayValue, header);
                            value.OutputValue = values.DisplayValue;
                            break;
                        case 'Currency':
                            var cell = new AeviBodyCell(this.AeviDataRepository, this.AeviDataRepository.AeviGrid);
                            var values = cell.getCurrencyValues(value, header);
                            if (!_.isNull(values.DisplayValue))
                                value.OutputValue = values.DisplayValue.toString().replace('.', ',');
                            break;
                        case 'DateTime':
                        case 'ShortDate':
                        case 'ShortTime':
                            if (value.DisplayValue && value.DisplayValue.length > 0) {
                                var aeviDate = new AeviDate(this.AeviDataRepository.AeviLocalization.getCulture(), displayType, value.DisplayValue);
                                value.DisplayValue = aeviDate.getString();
                                value.OutputValue = value.DisplayValue;
                            }
                            break;
                        case 'Number':
                            if (!_.isNull(value.DisplayValue))
                                value.OutputValue = value.DisplayValue.toString().replace('.', ',');
                            break;
                        default:
                            value.OutputValue = value.DisplayValue;
                            break;
                    }
                    if (_.isNull(value.OutputValue) || value.OutputValue === 'null')
                        stringData += '';
                    else {
                        if (!isUserCopyOneCell) {
                            if (displayTypeString !== 'Currency')
                                stringData += '="' + value.OutputValue + '"';
                            else {
                                stringData += value.OutputValue;
                            }
                        }
                        else {
                            stringData += value.OutputValue;
                        }
                    }
                }
                stringData += lastChar;
            }
        }
        return stringData;
    };
    AeviTableData.prototype.sort = function (direction, columnIndex) {
        if (aeviIsUndefinedOrNull(this.AeviDataSorter)) {
            this.AeviDataSorter = new AeviDataSorter(this.AeviTableDescription, this);
        }
        this.AeviDataSorter.sort(direction, columnIndex);
    };
    AeviTableData.prototype.getEnumDisplayValue = function (header, dataValue) {
        var enumValues = header.EnumValues;
        for (var i = 0; i < enumValues.length; i++) {
            if (enumValues[i].DataValue == dataValue)
                return enumValues[i].DisplayValue;
        }
        return null;
    };
    AeviTableData.prototype.getDisplayValue = function (dataValue, columnIndex) {
        var column = this.AeviTableDescription.getColumnHeader(columnIndex);
        if (aeviIsUndefinedOrNull(column) || column.Visible === false)
            return '';
        var displayValue = dataValue;
        var columnType = this.AeviTableDescription.getColumnStringDisplayTypeByIndex(columnIndex);
        switch (columnType) {
            case 'DateTime':
            case 'ShortDate':
            case 'ShortTime':
                var aeviDate = new AeviDate(this.AeviDataRepository.AeviGrid.AeviLocalization.getCulture(), columnType, dataValue);
                displayValue = aeviDate.getString();
                break;
            case 'Enum':
                var enumValues = column.EnumValues;
                for (var x = 0; x < enumValues.length; x++) {
                    if (enumValues[x].DataValue === dataValue)
                        displayValue = enumValues[x].DisplayValue;
                }
                break;
            case 'Image':
                displayValue = null;
                break;
            default:
                break;
        }
        if (aeviIsUndefinedOrNull(displayValue))
            displayValue = '';
        return displayValue.toString();
    };
    AeviTableData.prototype.getFirstHiddenRecordIndex = function () {
        for (var rowIndex = 0; rowIndex < this.getRecordsLength(); rowIndex++) {
            if (this.AeviStatus.is(rowIndex, 'hidden'))
                return rowIndex;
        }
        return null;
    };
    AeviTableData.prototype.getFilledVisibleRecordsLength = function () {
        var statusIndex = this.AeviTableDescription.getStatusIndex();
        var rowsWithStatusNewRowLength = 0;
        for (var rowIndex = 0; rowIndex < this.data.length; rowIndex++) {
            if (this.AeviStatus.is(rowIndex, 'newrow'))
                rowsWithStatusNewRowLength += 1;
        }
        return this.getVisibleRecordsLength() - rowsWithStatusNewRowLength;
    };
    return AeviTableData;
}());
var AeviStatus = (function () {
    function AeviStatus(aeviDataRepository, aeviGridApi) {
        this.AeviGridApi = aeviGridApi;
        this.AeviDataRepository = aeviDataRepository;
    }
    AeviStatus.prototype.get = function (guid) {
        var statusIndex = this.AeviDataRepository.getStatusId();
        if (aeviIsUndefinedOrNull(statusIndex)) {
            this.AeviGridApi.log('AeviStatus.get(), statusIndex is undefined or null.');
            return null;
        }
        return this.AeviDataRepository.getRecord(guid)[0][statusIndex];
    };
    AeviStatus.prototype.getByRowIndex = function (rowIndex) {
        var statusColumnIndex = this.AeviDataRepository.AeviTableDescription.getStatusIndex();
        return this.AeviDataRepository.AeviTableData.getRecordCellValue(rowIndex, statusColumnIndex);
    };
    AeviStatus.prototype.add = function (guid, status) {
        var record = this.AeviDataRepository.getRecord(guid).slice();
        if (aeviIsUndefinedOrNull(record)) {
            this.AeviGridApi.log('AeviStatus.add(), record is undefined or null.');
            return null;
        }
        var statusIndex = this.AeviDataRepository.getStatusId();
        if (aeviIsUndefinedOrNull(statusIndex)) {
            this.AeviGridApi.log('AeviStatus.add(), statusIndex is undefined or null.');
            return null;
        }
        record[0][statusIndex] += ' ' + status;
        this.AeviDataRepository.updateRecord(record);
    };
    AeviStatus.prototype.update = function (guid, status) {
        var record = this.AeviDataRepository.AeviTableData.getRecord(guid).slice();
        if (aeviIsUndefinedOrNull(record)) {
            this.AeviGridApi.log('AeviStatus.update(), record is undefined or null.');
            return null;
        }
        var statusIndex = this.AeviDataRepository.AeviTableDescription.getStatusIndex();
        var rowIndex = this.AeviDataRepository.AeviTableDescription.getRowNumberIndex();
        this.AeviDataRepository.AeviTableData.updateRecordCellValue(rowIndex, statusIndex, status);
    };
    AeviStatus.prototype.updateByRowIndex = function (rowIndex, status) {
        var statusColumnIndex = this.AeviDataRepository.AeviTableDescription.getStatusIndex();
        this.AeviDataRepository.AeviTableData.updateRecordCellValue(rowIndex, statusColumnIndex, status);
    };
    AeviStatus.prototype.addByRowIndex = function (rowIndex, status) {
        var statusColumnIndex = this.AeviDataRepository.AeviTableDescription.getStatusIndex();
        var originalStatus = this.getByRowIndex(rowIndex);
        if (originalStatus.indexOf(status) !== -1)
            return;
        var newStatus = originalStatus + ' ' + status;
        this.AeviDataRepository.AeviTableData.updateRecordCellValue(rowIndex, statusColumnIndex, newStatus);
    };
    AeviStatus.prototype.removeByRowIndex = function (rowIndex, status) {
        var statusColumnIndex = this.AeviDataRepository.AeviTableDescription.getStatusIndex();
        var originalStatus = this.getByRowIndex(rowIndex);
        originalStatus = originalStatus.replace(status, '');
        this.AeviDataRepository.AeviTableData.updateRecordCellValue(rowIndex, statusColumnIndex, originalStatus);
    };
    AeviStatus.prototype.is = function (rowIndex, status) {
        var statusIndex = this.AeviDataRepository.AeviTableDescription.getStatusIndex();
        var record = this.AeviDataRepository.AeviTableData.data[rowIndex];
        if (aeviIsUndefinedOrNull(record)) {
            return null;
        }
        return record[statusIndex].indexOf(status) !== -1;
    };
    AeviStatus.prototype.isNew = function (rowIndex) {
        return this.is(rowIndex, 'newrow');
    };
    AeviStatus.prototype.isInvalid = function (rowIndex) {
        return this.is(rowIndex, 'invalid');
    };
    AeviStatus.prototype.removeInvalidStatuses = function (rowIndex) {
        this.removeByRowIndex(rowIndex, 'invalid');
        this.removeByRowIndex(rowIndex, 'softInvalid');
    };
    return AeviStatus;
}());
var AeviGridApi = (function () {
    function AeviGridApi(aeviGrid, aeviDataRepository) {
        this.AeviGrid = aeviGrid;
        this.AeviDataRepository = aeviDataRepository;
    }
    AeviGridApi.prototype.getTableDescription = function () {
        return this.AeviDataRepository.AeviTableDescription.getDescription();
    };
    AeviGridApi.prototype.getColumnHeader = function (index) {
        return this.AeviDataRepository.AeviTableDescription.getColumnHeader(index);
    };
    AeviGridApi.prototype.getColumnHeaderIndexByName = function (name) {
        return this.AeviDataRepository.AeviTableDescription.getColumnIndexByName(name);
    };
    AeviGridApi.prototype.getTableData = function () {
        return this.AeviDataRepository.AeviTableData.getData();
    };
    AeviGridApi.prototype.getColumnDisplayTypes = function () {
        return this.AeviGrid.AeviConsts.dataTypes;
    };
    AeviGridApi.prototype.log = function (msg) {
        return this.AeviGrid.print(msg);
    };
    AeviGridApi.prototype.getCulture = function () {
        return this.AeviGrid.AeviLocalization.getCulture();
    };
    AeviGridApi.prototype.updateCellValue = function (rowIndex, cellIndex, value) {
        this.AeviDataRepository.AeviTableData.updateRecordCellValue(rowIndex, cellIndex, value);
    };
    AeviGridApi.prototype.refreshRow = function (rowIndex) {
        this.AeviGrid.AeviGridEditor.refreshRow({ rowIndex: rowIndex });
    };
    AeviGridApi.prototype.getMode = function () {
        return this.AeviGrid.mode;
    };
    return AeviGridApi;
}());
var AeviImage = (function () {
    function AeviImage(AeviGrid, imageEditor, cellIndexes, allowImageChange) {
        this.AeviGrid = AeviGrid;
        this.imageEditor = imageEditor;
        this.cellIndexes = cellIndexes;
        this.allowImageChange = allowImageChange;
        this.originalImageData = _.clone(this.getImageData());
        this.cell = this.AeviGrid.AeviDOM.getPureCell(cellIndexes.cellIndex, cellIndexes.rowIndex);
    }
    AeviImage.init = function (aeviGrid) {
        AeviImage.AeviGrid = aeviGrid;
    };
    AeviImage.prototype.render = function () {
        var _this = this;
        var imageData = this.getImageData();
        this.AeviGrid.croppingMode = false;
        var modal = new AeviModal(this.AeviGrid);
        var modalTextContent = this.getModalContent(imageData);
        modal.setContent({ text: modalTextContent });
        modal.setOptions({ minWidth: 630, minHeight: 500 });
        modal.show(function () {
            AeviImage.imageDataChanged(imageData);
            $(document).off('click', AeviConsts.imageModalUpload).on('click', AeviConsts.imageModalUpload, function () {
                $(AeviConsts.imageModalFile).click();
            });
            $(document).off('dragenter dragover', AeviConsts.imageModalUpload).on('dragenter dragover', AeviConsts.imageModalUpload, function (event) {
                event.preventDefault();
                event.stopPropagation();
            });
            $(document).off('drop', AeviConsts.imageModalUpload).on('drop', AeviConsts.imageModalUpload, function (event) {
                event.preventDefault();
                event.stopPropagation();
                var dropEvent = event.originalEvent || window.event;
                var files = dropEvent.dataTransfer.files;
                if (files) {
                    _this.uploadFile(files[0]);
                }
            });
            $(document).off('change', AeviConsts.imageModalFile).on('change', AeviConsts.imageModalFile, function (event) {
                _this.uploadFile(event.target.files[0]);
            });
            $(document).off('click', AeviConsts.imageModalSaveAndClose).on('click', AeviConsts.imageModalSaveAndClose, function () {
                _this.save(imageData);
                $(document).trigger(AeviConsts.imageModalChangeEvent, { type: 'saveAndClose', data: imageData });
            });
            $(document).off('click', AeviConsts.imageModalDelete).on('click', AeviConsts.imageModalDelete, function () {
                _this.AeviGrid.AeviDOM.closeModals();
                var modal = new AeviModal(AeviImage.AeviGrid, function (confirm) {
                    if (confirm) {
                        _this.deleteImage(imageData);
                        $(document).trigger(AeviConsts.imageModalChangeEvent, { type: 'delete' });
                    }
                    else {
                        _this.reopen();
                    }
                });
                modal.setContent({
                    title: AeviImage.getLocalizedText('warning'),
                    text: '<p>' + AeviImage.getLocalizedText('image_delete_message') + '</p>',
                    buttons: [
                        'aeviModalAccept',
                        'aeviModalDenied'
                    ]
                });
                modal.show();
                modal.focus('last');
            });
            $(document).off('click', AeviConsts.imageModalCloseWithoutClose).on('click', AeviConsts.imageModalCloseWithoutClose, function () {
                $('#aeviImage').cropper('destroy');
                _this.setInitialValue();
                _this.AeviGrid.AeviDOM.closeModals();
                $(document).trigger(AeviConsts.imageModalChangeEvent, { type: 'close' });
            });
            $(document).off('click', AeviConsts.imageModalClose).on('click', AeviConsts.imageModalClose, function () {
                if (AeviImage.AeviGrid.AeviDOM.length('.cropper-container')) {
                    var close = confirm(AeviImage.getLocalizedText('image_close_unsaved'));
                    if (close) {
                        $('#aeviImage').cropper('destroy');
                        $(document).trigger('aeviImageClose');
                    }
                }
                else {
                    $('#aeviImage').cropper('destroy');
                    AeviImage.AeviGrid.AeviDOM.closeModals();
                }
            });
        }, function () {
            if (_this.AeviGrid.AeviDOM.length('.cropper-container')) {
            }
            else {
                $('#aeviImage').cropper('destroy');
                _this.AeviGrid.AeviDOM.closeModals();
            }
        });
        $(document).off('click', AeviConsts.imageModalEdit).on('click', AeviConsts.imageModalEdit, function (event) {
            event.preventDefault();
            _this.callImageCropper();
            _this.imageEditor.isDataValueChanged = true;
            $(event.currentTarget).detach();
        });
    };
    AeviImage.prototype.getImageData = function () {
        var imageData = this.AeviGrid.AeviDataRepository.AeviTableData.getRecordCellValue(this.cellIndexes.rowIndex, this.cellIndexes.cellIndex);
        if (imageData.HasValue && (imageData.Value === null || imageData.Value.length === 0)) {
            var guid = this.AeviGrid.AeviDataRepository.AeviTableData.getGuidByRowIndex(this.cellIndexes.rowIndex);
            var newImageData = this.AeviGrid.AeviDataService.getImageData(guid, this.cellIndexes.cellIndex);
            if (!aeviIsUndefinedOrNull(newImageData)) {
                this.AeviGrid.AeviDataRepository.AeviTableData.updateRecordCellValue(this.cellIndexes.rowIndex, this.cellIndexes.cellIndex, newImageData);
                imageData = newImageData;
            }
        }
        return imageData;
    };
    AeviImage.prototype.getModalContent = function (imageData) {
        var HTML = '<div class="aeviImageModal">' +
            '<div class="image">' +
            '<div class="upload ' + AeviConsts.imageModalUpload.makeClass() + '">' + AeviImage.getLocalizedText('image_drag') + '</div>' +
            '<img id="aeviImage" src="data:image/' + AeviImage.getImageFormat() + ';base64,' + imageData.Value + '">' +
            '</div>' +
            '<div class="buttons">' +
            '<input type="file" class="file ' + AeviConsts.imageModalFile.makeClass() + '" accept="image/png, image/' + AeviImage.getImageFormat() + '" style="display:none;">' +
            '</div>' +
            '</div>';
        if (imageData.HasValue && this.allowImageChange)
            HTML += '<input type="button" class="aeviButton aeviImageEdit ' + AeviConsts.imageModalEdit.makeClass() + '" id="aeviImageEdit" value="' + AeviImage.getLocalizedText('image_edit') + '">';
        if (this.allowImageChange)
            HTML += '<input type="button" class="aeviButton pull-left delete ' + AeviConsts.imageModalDelete.makeClass() + '" value="' + AeviImage.getLocalizedText('image_delete') + '">';
        HTML += '<input type="button" class="aeviButton aeviButton--secondary pull-right saveAndClose ' + AeviConsts.imageModalSaveAndClose.makeClass() + '" value="' + AeviImage.getLocalizedText('save_and_close') + '">';
        if (this.allowImageChange)
            HTML += '<input type="button" class="aeviButton aeviButton--black pull-right ' + AeviConsts.imageModalCloseWithoutClose.makeClass() + '" value="' + AeviImage.getLocalizedText('close_without_save') + '">';
        HTML += '</div>' +
            '<a class="aeviImageModal__close"></a>' +
            '</div>';
        return HTML;
    };
    AeviImage.prototype.listenCropButtons = function (cropElement) {
        $(document).off('click', '.cropZoom').on('click', '.cropZoom', function (event) {
            var zoomValue = 0.1;
            var element = event.currentTarget;
            var action = element.getAttribute('data-action');
            if (action === 'unzoom')
                zoomValue = zoomValue * -1;
            cropElement.cropper('zoom', zoomValue);
        });
    };
    AeviImage.imageDataChanged = function (imageData) {
        if (imageData.HasValue) {
            $(AeviConsts.imageModalUpload).hide();
            $('.aeviImageModal img').attr('src', 'data:image/' + AeviImage.getImageFormat() + ';base64,' + imageData.Value).show();
            $(AeviConsts.imageModalDelete).show();
            $(AeviConsts.imageModalEdit).show();
        }
        else {
            $('.aeviImageModal img').hide().attr('src', '');
            $(AeviConsts.imageModalDelete).hide();
            $(AeviConsts.imageModalEdit).hide();
            $(AeviConsts.imageModalUpload).show();
        }
    };
    AeviImage.prototype.deleteImage = function (imageData) {
        this.clearImage();
        if (imageData.Changed) {
            this.imageEditor.isDataValueChanged = true;
        }
        $('#aeviImage').cropper('destroy');
        this.refreshCell();
        this.reopen();
    };
    AeviImage.prototype.save = function (imageData) {
        if (imageData.Changed) {
            this.imageEditor.isDataValueChanged = true;
            this.refreshCell();
        }
        if (this.AeviGrid.croppingMode) {
            this.cropImage(imageData);
        }
        $('#aeviImage').cropper('destroy');
        this.AeviGrid.AeviDOM.closeModals();
        if (imageData.Changed) {
            this.AeviGrid.AeviGridHandler.cellChange(this.AeviGrid.AeviDOM.getCell(this.cellIndexes.cellIndex, this.cellIndexes.rowIndex));
            this.AeviGrid.AeviGridHandler.selectedCellEditor.isDataValueChanged = true;
            this.AeviGrid.AeviGridHandler.triggerClick();
        }
    };
    AeviImage.prototype.reopen = function () {
        var _this = this;
        setTimeout(function () { return _this.imageEditor.renderImageWindow(); });
    };
    AeviImage.prototype.cropImage = function (imageData) {
        var cropElement = $('#aeviImage');
        var croppedCanvas = cropElement.cropper('getCroppedCanvas', { height: 200 });
        var croppedImage = AeviImage.convertToImage(croppedCanvas);
        cropElement.cropper('replace', croppedImage);
        imageData.Value = AeviImage.getImageDataWithoutPrefix(croppedImage);
        imageData.Changed = true;
        this.AeviGrid.croppingMode = false;
        AeviImage.imageDataChanged(imageData);
    };
    AeviImage.prototype.uploadFile = function (file) {
        var _this = this;
        this.readFile(file, function () {
            setTimeout(function () {
                _this.callImageCropper();
            }, 200);
        });
    };
    AeviImage.prototype.refreshCell = function () {
        if (this.AeviGrid.AeviDOM.isFormVisible()) {
            return;
        }
        this.AeviGrid.AeviGridEditor.refreshRow({ rowIndex: this.cellIndexes.rowIndex });
        this.AeviGrid.select(this.cell);
    };
    AeviImage.prototype.readFile = function (file, done) {
        var _this = this;
        var reader = new FileReader();
        reader.onload = function () {
            var imageData = _this.getImageData();
            imageData.HasValue = true;
            imageData.Value = reader.result.substring(reader.result.indexOf(',') + 1);
            imageData.Changed = true;
            AeviImage.imageDataChanged(imageData);
            $(document).trigger(AeviConsts.imageModalChangeEvent, { type: 'upload', data: imageData });
        };
        if (aeviIsUndefinedOrNull(file))
            return;
        reader.readAsDataURL(file);
        done(true);
    };
    AeviImage.prototype.setInitialValue = function () {
        var imageData = this.getImageData();
        imageData.HasValue = this.originalImageData.HasValue;
        imageData.Value = this.originalImageData.Value;
        imageData.Changed = this.originalImageData.Changed;
        AeviImage.imageDataChanged(imageData);
    };
    AeviImage.prototype.clearImage = function () {
        var imageData = this.getImageData();
        if (imageData) {
            imageData.HasValue = false;
            imageData.Value = null;
            imageData.Changed = true;
            AeviImage.imageDataChanged(imageData);
        }
    };
    AeviImage.prototype.callImageCropper = function () {
        var cropElement = $('#aeviImage');
        cropElement.cropper('destroy');
        this.AeviGrid.croppingMode = true;
        cropElement.cropper({
            aspectRatio: 2 / 3,
            autoCropArea: 1,
            strict: false,
            guides: false,
            background: true,
            dragCrop: false,
            cropBoxMovable: false,
            cropBoxResizable: false,
            doubleClickToggle: false
        });
        if (!$('.cropButtons').length) {
            var zoomingButtons = '<span class="cropButtons pull-left" style="margin-left: 7px;">' +
                '<span class="aeviButton cropZoom zoomin" data-action="zoom"></span>' +
                '<span class="aeviButton cropZoom zoomout" data-action="unzoom"></span>' +
                '</span>';
            $(zoomingButtons).insertAfter('.simplemodal-data .aeviButton.delete');
            this.listenCropButtons(cropElement);
        }
    };
    AeviImage.getDefaultValue = function () {
        return {
            HasValue: false,
            Changed: false,
            Value: null
        };
    };
    AeviImage.getDefaultBlockedValue = function () {
        return {
            HasValue: false,
            Changed: false,
            Value: 'blocked'
        };
    };
    AeviImage.getDisplayValue = function (imageData) {
        var result = null;
        var className = 'imagePresent';
        var text = AeviImage.getLocalizedText('image_present');
        if (imageData) {
            if (!imageData.HasValue) {
                className = 'imageMissing';
                text = AeviImage.getLocalizedText('image_missing');
            }
            if (imageData.Value === 'blocked') {
                className = 'imageBlocked';
                text = AeviImage.getLocalizedText('image_blocked');
            }
            result = '<a class="aeviCellImage ' + className + '">' + text + '</a>';
        }
        return result;
    };
    AeviImage.getDataValue = function () {
        return null;
    };
    AeviImage.getLocalizedText = function (code) {
        return AeviImage.AeviGrid.AeviLocalization.translate(code);
    };
    AeviImage.getImageFormat = function () {
        return AeviImage.AeviGrid.AeviConsts.imageFormat;
    };
    AeviImage.convertToImage = function (canvas) {
        canvas = AeviImage.convertTransparentPixelsToWhitePixels(canvas);
        return canvas.toDataURL('image/' + AeviImage.getImageFormat());
    };
    AeviImage.convertTransparentPixelsToWhitePixels = function (canvas) {
        var ctx = canvas.getContext('2d');
        var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        var data = imgData.data;
        for (var i = 0; i < data.length; i += 4) {
            if (data[i + 3] < 255) {
                data[i] = 255;
                data[i + 1] = 255;
                data[i + 2] = 255;
                data[i + 3] = 255;
            }
        }
        ctx.putImageData(imgData, 0, 0);
        return canvas;
    };
    AeviImage.getImageDataWithoutPrefix = function (url) {
        return url.replace('data:image/' + AeviImage.getImageFormat() + ';base64,', '').replace('data:image/png;base64,', '');
    };
    return AeviImage;
}());
var AeviDataRepository = (function () {
    function AeviDataRepository(aeviConstants, aeviLocalization, aeviGrid) {
        this.tableDesc = [];
        this.tableDescRealLength = 0;
        this.data = [];
        this.columnDifference = 0;
        this.dataLength = 0;
        this.AeviConsts = aeviConstants;
        this.AeviLocalization = aeviLocalization;
        this.dataTypes = this.AeviConsts.dataTypes;
        this.AeviGrid = aeviGrid;
        this.AeviGridApi = new AeviGridApi(this.AeviGrid);
        this.extraColumns = [];
        this.repositoryDataInit = false;
    }
    AeviDataRepository.prototype.initDataEntity = function (aeviDataService, response) {
        this.AeviDataEntity = new AeviDataEntity(aeviDataService, response);
    };
    AeviDataRepository.prototype.initTableDescription = function (response) {
        this.AeviTableDescription = new AeviTableDescription(response, this);
        this.initExtraColumns();
        this.setExtraColumnsToDescription();
        this.AeviTableDescription.setColumnActionTypes();
    };
    AeviDataRepository.prototype.initForm = function (response) {
        this.AeviFormDescription = new AeviFormDescription(response);
    };
    AeviDataRepository.prototype.initTableData = function (data) {
        this.AeviTableData = new AeviTableData(this);
        this.deleteRepositoryData();
        this.AeviTableData.init(data);
        this.setExtraColumnsToData();
        if (_.isNull(this.AeviTableDescription.fixedSize)) {
            if (!this.AeviTableDescription.realReadOnly) {
                this.AeviTableData.addHiddenRecords();
            }
        }
        this.repositoryDataInit = true;
    };
    AeviDataRepository.prototype.initExtraColumns = function () {
        var rowNumberValue = 'RowNumber';
        var rowNumberColumn = {
            ColumnName: 'RowNumber',
            Caption: '',
            Visible: true,
            DisplayType: 2,
            WidthType: 0,
            Width: this.AeviConsts.firstColumnWidth
        };
        this.extraColumns.push({ column: rowNumberColumn, value: rowNumberValue });
        if (this.AeviDataEntity.data.Form === true) {
            var formColumnValue = 'FormAction';
            var formColumn = {
                ColumnName: 'FormAction',
                Caption: '',
                Visible: true,
                DisplayType: 0,
                WidthType: 0,
                Width: 40,
                EntityActionDefinition: { ActionId: '0', ResultType: MyRetail.IAeviEntityActionResultType.Form }
            };
            this.extraColumns.push({ column: formColumn, value: formColumnValue });
        }
        var selectedCellsArrayValue = [];
        var selectedCellsArrayColumn = {
            ColumnName: 'SelectedCellsArray',
            Caption: 'SelectedCellsArray',
            Visible: false,
            DisplayType: 98,
            WidthType: 0,
            Width: 0
        };
        this.extraColumns.push({ column: selectedCellsArrayColumn, value: selectedCellsArrayValue });
        var invalidCellsArrayValue = [];
        for (var i = 0; i < this.AeviTableDescription.getColumnsLength(); i++) {
            invalidCellsArrayValue.push({
                hard: false,
                valid: true,
                message: null
            });
        }
        var invalidCellsArrayColumn = {
            ColumnName: 'InvalidCellsArray',
            Caption: 'InvalidCellsArray',
            Visible: false,
            DisplayType: 99,
            WidthType: 0,
            Width: 0
        };
        this.extraColumns.push({ column: invalidCellsArrayColumn, value: invalidCellsArrayValue });
        var statusValue = 'render';
        var statusColumn = {
            ColumnName: 'Status',
            Caption: 'Status',
            Visible: false,
            DisplayType: 100,
            WidthType: 0,
            Width: 0
        };
        this.extraColumns.push({ column: statusColumn, value: statusValue });
        var defaultOrderValue = null;
        var defaultOrderColumn = {
            ColumnName: 'DefaultOrder',
            Caption: 'DefaultOrder',
            Visible: false,
            DisplayType: 1,
            WidthType: 0,
            Width: 0
        };
        this.extraColumns.push({ column: defaultOrderColumn, value: defaultOrderValue });
    };
    AeviDataRepository.prototype.setExtraColumnsToDescription = function () {
        for (var i = 0; i < this.extraColumns.length; i++) {
            this.AeviTableDescription.setExtraColumn(this.extraColumns[i].column);
            this.columnDifference++;
        }
        this.AeviTableDescription.setColumnsPositions();
    };
    AeviDataRepository.prototype.setExtraColumnsToData = function () {
        for (var i = 0; i < this.extraColumns.length; i++) {
            this.AeviTableData.setExtraColumn(this.extraColumns[i].value);
        }
        this.AeviTableData.setDefaultOrder();
    };
    AeviDataRepository.prototype.deleteRepositoryData = function () {
        this.AeviTableData.deleteAllData();
    };
    AeviDataRepository.prototype.getVisibleRecordsLength = function () {
        if (aeviIsUndefinedOrNull(this.AeviTableData))
            return null;
        return this.AeviTableData.getVisibleRecordsLength();
    };
    AeviDataRepository.prototype.getVisibleAndNewRecordsLength = function () {
        if (aeviIsUndefinedOrNull(this.AeviTableData))
            return null;
        return this.AeviTableData.getVisibleAndNewRecordsLength();
    };
    AeviDataRepository.prototype.deleteRepositoryTableData = function () {
        this.AeviTableData.deleteAllData();
    };
    AeviDataRepository.prototype.getGuidId = function () { return this.AeviTableDescription.getGuidId(); };
    AeviDataRepository.prototype.getStatusId = function () { return this.AeviTableDescription.getStatusId(); };
    AeviDataRepository.prototype.getSelectedCellsId = function () { return this.AeviTableDescription.getSelectedCellsId(); };
    AeviDataRepository.prototype.getInvalidCellsId = function () { return this.AeviTableDescription.getInvalidCellsIndex(); };
    AeviDataRepository.prototype.getData = function () {
        return this.AeviTableData.getData();
    };
    AeviDataRepository.prototype.getDataType = function (dataTypeNumber) {
        return this.dataTypes[dataTypeNumber];
    };
    AeviDataRepository.prototype.getRecords = function (from, to) {
        this.AeviGridApi.log('AeviDataRepository.getRecords() is deprecated, use AeviTableData');
        return this.AeviTableData.getRecords(from, to);
    };
    AeviDataRepository.prototype.getSelectedCellsPositions = function () {
        var selectedCellsIndex = this.AeviTableDescription.getSelectedCellsIndex();
        var data = this.AeviTableData.cloneData();
        var length = data.length;
        var rows = [];
        var cells = [];
        for (var i = 0; i < length; i++) {
            var selectedCells = data[i][selectedCellsIndex];
            if (_.isNull(selectedCells))
                return null;
            if (selectedCells.length > 0) {
                rows.push(i);
                cells = selectedCells;
            }
        }
        return {
            rows: rows,
            cells: cells
        };
    };
    AeviDataRepository.prototype.getSelectedCellPosition = function () {
        var selectedPositions = this.getSelectedCellsPositions();
        var rowIndex = selectedPositions.rows[0];
        var cellIndex = selectedPositions.cells[0];
        if (aeviIsUndefinedOrNull(rowIndex) || aeviIsUndefinedOrNull(cellIndex))
            return null;
        return {
            rowIndex: rowIndex,
            cellIndex: cellIndex
        };
    };
    AeviDataRepository.prototype.getInvalidPosition = function (rowIndex) {
        return this.AeviTableData.getRecordCellValue(rowIndex, this.AeviTableDescription.getInvalidCellsIndex());
    };
    AeviDataRepository.prototype.getSelectedPosition = function (rowIndex) {
        return this.AeviTableData.getRecordCellValue(rowIndex, this.AeviTableDescription.getSelectedCellsIndex());
    };
    AeviDataRepository.prototype.getReferenceSettings = function (rowIndex, cellIndex) {
        var row = this.AeviTableData.getRecordByRowIndex(rowIndex);
        var referenceSettingsColumnNameIndex = this.AeviTableDescription.getReferenceSettingsColumnNameIndex();
        if (aeviIsUndefinedOrNull(referenceSettingsColumnNameIndex))
            return null;
        var referenceSettingsColumnString = row[referenceSettingsColumnNameIndex];
        if (_.isNull(referenceSettingsColumnString))
            return null;
        var referenceSettingsColumn = JSON.parse(referenceSettingsColumnString);
        var header = this.AeviTableDescription.getColumnHeader(cellIndex);
        if (aeviIsUndefinedOrNull(header))
            return null;
        var columnName = this.AeviTableDescription.getColumnName(header);
        if (referenceSettingsColumn.ColumnName === columnName)
            return referenceSettingsColumn;
        return null;
    };
    AeviDataRepository.prototype.isMoreCellsSelected = function () {
        var selectedPositions = this.getSelectedCellsPositions();
        return !!(selectedPositions.rows.length > 1 || selectedPositions.cells.length > 1);
    };
    AeviDataRepository.prototype.getApiDataByRowIndex = function (rowIndex) {
        return this.AeviTableData.getApiDataByRowIndex(rowIndex);
    };
    AeviDataRepository.prototype.isMaxRowCountExceeded = function (addRow) {
        if (aeviIsUndefinedOrNull(addRow))
            addRow = 0;
        var recordsLen = this.getVisibleRecordsLength() + addRow;
        if (aeviIsUndefinedOrNull(this.AeviTableDescription.maxRowCount))
            return false;
        return (this.AeviTableDescription.maxRowCount < recordsLen);
    };
    AeviDataRepository.prototype.enableOrDisableImageColumns = function (action) {
        var columnsLength = this.AeviTableDescription.getColumnsLength();
        var state = (action === 'disable');
        for (var i = 0; i < columnsLength; i++) {
            var column = this.AeviTableDescription.getColumnHeader(i);
            var displayType = this.AeviTableDescription.getColumnStringDisplayTypeByIndex(i);
            if (!_.isNull(column)) {
                if (displayType === 'Image') {
                    column.ReadOnly = state;
                }
            }
        }
    };
    AeviDataRepository.prototype.clearSelectedRows = function () {
        for (var i = 0; i < this.AeviTableData.getRecordsLength(); i++) {
            if (this.AeviTableData.AeviStatus.is(i, 'selected'))
                this.AeviTableData.AeviStatus.removeByRowIndex(i, 'selected');
        }
    };
    AeviDataRepository.prototype.clearCells = function (selectedCellsPositions) {
        var firstRow = selectedCellsPositions.rows[0];
        var lastRow = selectedCellsPositions.rows[selectedCellsPositions.rows.length - 1];
        var firstCell = selectedCellsPositions.cells[0];
        var lastCell = selectedCellsPositions.cells[selectedCellsPositions.cells.length - 1];
        for (var rowIndex = firstRow; rowIndex < lastRow + 1; rowIndex++) {
            for (var cellIndex = firstCell; cellIndex < lastCell + 1; cellIndex++) {
                this.clearCell({ rowIndex: rowIndex, cellIndex: cellIndex });
            }
        }
    };
    AeviDataRepository.prototype.clearCell = function (indexes) {
        var returnEmptyValue = true;
        var value = this.AeviTableData.getDefaultValue(indexes.cellIndex, null, returnEmptyValue);
        this.AeviTableData.updateRecordCellValue(indexes.rowIndex, indexes.cellIndex, value);
    };
    return AeviDataRepository;
}());
var AeviDataEntity = (function () {
    function AeviDataEntity(aeviDataService, response) {
        this.AeviDataService = aeviDataService;
        this.data = {};
        for (var key in response) {
            if (response.hasOwnProperty(key)) {
                this.data[key] = response[key];
            }
        }
        this.AeviDataService.AeviApiService.caption = this.data.Caption;
        this.data.Help = AeviGlobal.stringToHTML(this.data.Help);
    }
    return AeviDataEntity;
}());
var MyRetail;
(function (MyRetail) {
    (function (IAeviSaveModeExcel) {
        IAeviSaveModeExcel[IAeviSaveModeExcel["Standard"] = 0] = "Standard";
        IAeviSaveModeExcel[IAeviSaveModeExcel["Repleace"] = 1] = "Repleace";
    })(MyRetail.IAeviSaveModeExcel || (MyRetail.IAeviSaveModeExcel = {}));
    var IAeviSaveModeExcel = MyRetail.IAeviSaveModeExcel;
    ;
})(MyRetail || (MyRetail = {}));
var MyRetail;
(function (MyRetail) {
    (function (IAeviWidthType) {
        IAeviWidthType[IAeviWidthType["Pixels"] = 0] = "Pixels";
        IAeviWidthType[IAeviWidthType["Percents"] = 1] = "Percents";
    })(MyRetail.IAeviWidthType || (MyRetail.IAeviWidthType = {}));
    var IAeviWidthType = MyRetail.IAeviWidthType;
    ;
})(MyRetail || (MyRetail = {}));
var MyRetail;
(function (MyRetail) {
    (function (IAeviSemanticType) {
        IAeviSemanticType[IAeviSemanticType["Unspecified"] = 0] = "Unspecified";
        IAeviSemanticType[IAeviSemanticType["EAN"] = 1] = "EAN";
    })(MyRetail.IAeviSemanticType || (MyRetail.IAeviSemanticType = {}));
    var IAeviSemanticType = MyRetail.IAeviSemanticType;
    ;
})(MyRetail || (MyRetail = {}));
var MyRetail;
(function (MyRetail) {
    (function (IAeviDisplayType) {
        IAeviDisplayType[IAeviDisplayType["Text"] = 0] = "Text";
        IAeviDisplayType[IAeviDisplayType["Number"] = 1] = "Number";
        IAeviDisplayType[IAeviDisplayType["IntegerNumber"] = 2] = "IntegerNumber";
        IAeviDisplayType[IAeviDisplayType["Currency"] = 3] = "Currency";
        IAeviDisplayType[IAeviDisplayType["DateTime"] = 4] = "DateTime";
        IAeviDisplayType[IAeviDisplayType["ShortDate"] = 5] = "ShortDate";
        IAeviDisplayType[IAeviDisplayType["ShortTime"] = 6] = "ShortTime";
        IAeviDisplayType[IAeviDisplayType["Boolean"] = 7] = "Boolean";
        IAeviDisplayType[IAeviDisplayType["Enum"] = 8] = "Enum";
        IAeviDisplayType[IAeviDisplayType["Hyperlink"] = 9] = "Hyperlink";
        IAeviDisplayType[IAeviDisplayType["RegularExpression"] = 10] = "RegularExpression";
        IAeviDisplayType[IAeviDisplayType["Image"] = 11] = "Image";
        IAeviDisplayType[IAeviDisplayType["Reference"] = 12] = "Reference";
    })(MyRetail.IAeviDisplayType || (MyRetail.IAeviDisplayType = {}));
    var IAeviDisplayType = MyRetail.IAeviDisplayType;
    ;
})(MyRetail || (MyRetail = {}));
var MyRetail;
(function (MyRetail) {
    (function (IAeviColumnActionType) {
        IAeviColumnActionType[IAeviColumnActionType["None"] = 0] = "None";
        IAeviColumnActionType[IAeviColumnActionType["NegativeRowFormating"] = 1] = "NegativeRowFormating";
    })(MyRetail.IAeviColumnActionType || (MyRetail.IAeviColumnActionType = {}));
    var IAeviColumnActionType = MyRetail.IAeviColumnActionType;
    ;
})(MyRetail || (MyRetail = {}));
var MyRetail;
(function (MyRetail) {
    (function (IAeviEntityActionResultType) {
        IAeviEntityActionResultType[IAeviEntityActionResultType["Image"] = 0] = "Image";
        IAeviEntityActionResultType[IAeviEntityActionResultType["Form"] = 1] = "Form";
    })(MyRetail.IAeviEntityActionResultType || (MyRetail.IAeviEntityActionResultType = {}));
    var IAeviEntityActionResultType = MyRetail.IAeviEntityActionResultType;
    ;
})(MyRetail || (MyRetail = {}));
var AeviFormDescription = (function () {
    function AeviFormDescription(description) {
        this.data = description;
    }
    AeviFormDescription.prototype.getRows = function () {
        return this.data.Rows;
    };
    AeviFormDescription.getFakeRows = function () {
        var row0 = [
            {
                ColumnName: 'ArticleImage',
                Caption: 'Obrázek',
                MaxHeight: 200,
                Visible: true,
                Width: 12,
                Style: 'text-center image'
            }
        ];
        var row1 = [
            {
                ColumnName: 'VART',
                Caption: 'Číslo zboží',
                MaxHeight: null,
                Visible: true,
                Width: 4,
                Style: ''
            },
            {
                ColumnName: '',
                Caption: '',
                MaxHeight: null,
                Visible: true,
                Width: 2,
                Style: ''
            },
            {
                ColumnName: 'Caption',
                Caption: 'Název zboží',
                MaxHeight: null,
                Visible: true,
                Width: 6,
                Style: ''
            }
        ];
        var row2 = [
            {
                ColumnName: 'ProductGroupNumber',
                Caption: 'Skupina zboží',
                MaxHeight: null,
                Visible: true,
                Width: 4,
                Style: ''
            },
            {
                ColumnName: '',
                Caption: '',
                MaxHeight: null,
                Visible: true,
                Width: 2,
                Style: ''
            },
            {
                ColumnName: 'ActionTill',
                Caption: 'Akční cena platná do',
                MaxHeight: null,
                Visible: true,
                Width: 4,
                Style: ''
            }
        ];
        var row3 = [
            {
                ColumnName: 'VATID',
                Caption: 'DPH%',
                MaxHeight: null,
                Visible: true,
                Width: 3,
                Style: ''
            },
            {
                ColumnName: '',
                Caption: '',
                MaxHeight: null,
                Visible: true,
                Width: 3,
                Style: ''
            },
            {
                ColumnName: 'RetailPrice1',
                Caption: 'Prodejní cena',
                MaxHeight: null,
                Visible: true,
                Width: 4,
                Style: ''
            }
        ];
        var rows = [];
        rows.push(row0);
        rows.push(row1);
        rows.push(row2);
        rows.push(row3);
        return rows;
    };
    return AeviFormDescription;
}());
var AeviDataSorter = (function () {
    function AeviDataSorter(aeviTableDescription, aeviTableData) {
        this.AeviTableDescription = aeviTableDescription;
        this.AeviTableData = aeviTableData;
    }
    AeviDataSorter.prototype.prepareSort = function () {
        this.AeviTableData.removeSelectedPositions();
        this.AeviTableData.removeHiddenRecords();
    };
    AeviDataSorter.prototype.getSortOrder = function (direction) {
        switch (direction) {
            case 'down':
                return -1;
            case 'up':
                return 1;
            default:
                return 0;
        }
    };
    AeviDataSorter.prototype.getFixedColumnIndex = function (columnIndex) {
        return (_.isNull(columnIndex)) ? this.AeviTableDescription.getDefaultOrderIndex() : columnIndex;
    };
    AeviDataSorter.prototype.setSortInfo = function (sortOrder, columnIndex) {
        if (sortOrder === 0) {
            this.AeviTableDescription.setSortInfos(null);
        }
        else {
            this.AeviTableDescription.setSortInfos([{
                    ColumnName: this.AeviTableDescription.getColumnNameByIndex(columnIndex),
                    SortDirection: (sortOrder === -1) ? 'Ascending' : 'Descending'
                }]);
        }
    };
    AeviDataSorter.prototype.sort = function (direction, columnIndex) {
        var _this = this;
        if (this.AeviTableData.getVisibleRecordsLength() < 1) {
            return;
        }
        this.prepareSort();
        var sortOrder = this.getSortOrder(direction);
        columnIndex = this.getFixedColumnIndex(columnIndex);
        var sortedData = _.sortBy(this.AeviTableData.data, function (rowData) {
            var header = _this.AeviTableDescription.getColumnHeader(columnIndex);
            var displayType = _this.AeviTableDescription.getColumnStringDisplayTypeByIndex(columnIndex);
            var value = null;
            var compareValue = rowData[columnIndex];
            if (aeviIsUndefinedOrNull(compareValue) || compareValue === 'null') {
                compareValue = '';
            }
            switch (displayType) {
                case 'IntegerNumber':
                case 'Number':
                case 'Currency':
                    if (compareValue === '') {
                        compareValue = Number.NEGATIVE_INFINITY;
                        value = compareValue;
                    }
                    else {
                        value = parseFloat(compareValue);
                    }
                    break;
                case 'Enum':
                    value = _this.AeviTableData.getEnumDisplayValue(header, compareValue);
                    break;
                case 'Image':
                    value = compareValue.HasValue;
                    break;
                default:
                    value = compareValue;
                    break;
            }
            compareValue = value;
            if (_.isString(compareValue)) {
                value = compareValue.toLowerCase();
            }
            return value;
        });
        this.AeviTableData.data = (direction === 'up') ? sortedData.reverse() : sortedData;
        this.setSortInfo(sortOrder, columnIndex);
        this.AeviTableData.addHiddenRecords();
        this.AeviTableData.updateRowNumber();
    };
    return AeviDataSorter;
}());
var AeviForm = (function () {
    function AeviForm(rowIndex, aeviDataService, options) {
        this.rowIndex = rowIndex;
        this.AeviGrid = aeviDataService.AeviGrid;
        this.AeviDataService = aeviDataService;
        this.isOpenedByToolbar = options.isOpenedByToolbar;
        this.rows = this.AeviDataService.AeviDataRepository.AeviFormDescription.getRows();
        this.AeviFormRepository = new AeviFormRepository(this, this.AeviDataService.AeviDataRepository, this.rowIndex);
        this.showInReadOnlyMode = (this.AeviGrid.AeviGridLocker.isLocked || this.AeviDataService.AeviDataRepository.AeviTableDescription.isReadOnly());
        this.editors = [];
        this.render();
        this.handler = new AeviFormHandler(this);
    }
    AeviForm.prototype.render = function () {
        var fakeCell = '<div id="aeviFormFakeCell"><div></div></div>';
        var body = $('body');
        body.append(fakeCell);
        var template = AeviFormView.getMainTemplate();
        var previousTemplate = null;
        var nextTemplate = null;
        if (this.rowIndex <= 0) {
            this.isFirstRecordVisible = true;
            previousTemplate = AeviFormView.getEmptyTemplate();
        }
        else {
            this.isFirstRecordVisible = false;
            previousTemplate = AeviFormView.getPreviousArrowTemplate();
        }
        var saveTemplate = (this.showInReadOnlyMode) ? AeviFormView.getEmptyTemplate() : AeviFormView.getSaveButtonTemplate();
        var nextRowStatus = this.AeviDataService.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(this.rowIndex + 1);
        if ((nextRowStatus.indexOf('newrow') !== -1 || nextRowStatus.indexOf('hidden') !== -1) && nextRowStatus.indexOf('sortedRow') < 0) {
            this.isLastRecordVisible = true;
            nextTemplate = AeviFormView.getEmptyTemplate();
        }
        else {
            this.isLastRecordVisible = false;
            nextTemplate = AeviFormView.getNextArrowTemplate();
        }
        this.AeviEditorFactory = new AeviFormEditorFactory(this.AeviDataService.AeviGrid);
        var rows = [];
        for (var i = 0; i < this.rows.length; i++) {
            rows.push(this.renderRow(i));
        }
        var HTML = template({
            ReadOnly: this.showInReadOnlyMode,
            EntityCaption: this.AeviDataService.AeviDataRepository.AeviDataEntity.data.Caption,
            RecordIndex: this.rowIndex,
            RowNumber: this.rowIndex + 1,
            Rows: rows.join(''),
            SaveWithoutClose: this.AeviGrid.AeviLocalization.translate('close_without_save'),
            PreviousTemplate: previousTemplate({ RecordIndex: this.rowIndex, Previous: this.AeviGrid.AeviLocalization.translate('previous') }),
            NextTemplate: nextTemplate({ RecordIndex: this.rowIndex, Next: this.AeviGrid.AeviLocalization.translate('next') }),
            SaveTemplate: saveTemplate({ RecordIndex: this.rowIndex, Save: this.AeviGrid.AeviLocalization.translate('save_and_close') })
        });
        body.append(HTML);
        this.form = document.getElementById('aeviForm-' + this.rowIndex);
        this.$form = $(this.form);
    };
    AeviForm.prototype.refresh = function () {
        this.destroy();
        return new AeviForm(this.rowIndex, this.AeviDataService, { isOpenedByToolbar: this.isOpenedByToolbar });
    };
    AeviForm.prototype.renderRow = function (rowIndex) {
        var template = AeviFormView.getRowTemplate();
        var columnsLength = this.rows[rowIndex].Items.length;
        var columns = [];
        for (var columnIndex = 0; columnIndex < columnsLength; columnIndex++) {
            var rowColumn = this.rows[rowIndex].Items[columnIndex];
            if (!rowColumn.Visible) {
                continue;
            }
            if (_.isEmpty(rowColumn.ColumnName) || _.isNull(rowColumn.ColumnName)) {
                columns.push(AeviForm.renderEmptyColumn(rowColumn));
                continue;
            }
            var column = this.AeviFormRepository.items[rowColumn.ColumnName];
            if (!aeviIsUndefinedOrNull(column)) {
                column.IsReadOnly = this.showInReadOnlyMode;
            }
            columns.push(this.renderColumn(column));
        }
        return template({
            Columns: columns.join('')
        });
    };
    AeviForm.prototype.renderColumn = function (column) {
        if (aeviIsUndefinedOrNull(column)) {
            return '';
        }
        var emptyColumnTemplate = AeviFormView.getEmptyColumnTemplate();
        var fakeIndexes = {
            rowIndex: this.rowIndex,
            rowId: this.rowIndex,
            cellIndex: column.ColumnIndex,
            cellId: column.ColumnIndex
        };
        this.AeviEditorFactory.createFormEditor($('#aeviFormFakeCell'), fakeIndexes);
        var editor = this.AeviEditorFactory.getEditor();
        this.editors.push(editor);
        var columnTemplate = AeviFormView.getColumnTemplate();
        var editorHTML = editor.renderFormEditor(column, fakeIndexes);
        var columnHTML = columnTemplate({
            Caption: column.Caption,
            ColumnName: column.ColumnName,
            Class: column.Style,
            MaxHeight: (!column.MaxHeight || _.isNull(column.MaxHeight)) ? 'none' : column.MaxHeight + 'px',
            EditorHTML: editorHTML,
            Required: (column.IsRequired) ? '*' : ''
        });
        return emptyColumnTemplate({
            ColumnWidth: column.Width,
            Form: columnHTML
        });
    };
    AeviForm.renderEmptyColumn = function (column) {
        var emptyColumnTemplate = AeviFormView.getEmptyColumnTemplate();
        return emptyColumnTemplate({
            ColumnWidth: column.Width,
            Form: ''
        });
    };
    AeviForm.prototype.updateRecord = function () {
        for (var key in this.AeviFormRepository.items) {
            var item = this.AeviFormRepository.items[key];
            var columnIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnIndexByName(item.ColumnName);
            var displayType = this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnStringDisplayTypeByIndex(columnIndex);
            switch (displayType) {
                case 'Image':
                    break;
                default:
                    if (!AeviFormRepository.isEmpty(item)) {
                        var columnIndex = this.AeviDataService.AeviDataRepository.AeviTableDescription.getColumnIndexByName(item.ColumnName);
                        this.AeviDataService.AeviDataRepository.AeviTableData.updateRecordCellValue(this.rowIndex, columnIndex, item.Value);
                    }
                    break;
            }
        }
    };
    AeviForm.prototype.destroy = function () {
        this.$form.remove();
        $('.aeviForm__overlay, #aeviFormFakeCell').remove();
    };
    AeviForm.prototype.goToByRowInput = function () {
        var input = document.getElementById('aeviForm-' + this.rowIndex + '-rowInput');
        var rowIndex = parseInt(input.value) - 1;
        if (_.isNaN(rowIndex)) {
            return;
        }
        if (rowIndex < 0) {
            rowIndex = 0;
        }
        var visibleRecordsLength = this.AeviDataService.AeviDataRepository.AeviTableData.getVisibleRecordsLength();
        if (rowIndex > visibleRecordsLength - 1) {
            rowIndex = visibleRecordsLength - 1;
        }
        this.destroy();
        return new AeviForm(rowIndex, this.AeviDataService, { isOpenedByToolbar: this.isOpenedByToolbar });
    };
    AeviForm.prototype.setImageDefaultValue = function () {
        for (var key in this.AeviFormRepository.items) {
            var item = this.AeviFormRepository.items[key];
            var columnIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnIndexByName(item.ColumnName);
            var displayType = this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnStringDisplayTypeByIndex(columnIndex);
            var defaultValue = null;
            switch (displayType) {
                case 'Image':
                    var editor = this.getEditorByColumnName(item.ColumnName);
                    defaultValue = editor.defaultValue;
                    break;
                default:
                    break;
            }
            if (!_.isNull(defaultValue)) {
                this.AeviGrid.AeviDataRepository.AeviTableData.updateRecordCellValue(this.rowIndex, columnIndex, defaultValue);
            }
        }
    };
    AeviForm.prototype.getEditorByColumnName = function (columnName) {
        for (var i = 0; i < this.editors.length; i++) {
            if (aeviIsUndefinedOrNull(this.editors[i].columnName)) {
                continue;
            }
            if (this.editors[i].columnName === columnName) {
                return this.editors[i];
            }
        }
        return null;
    };
    return AeviForm;
}());
var AeviFormDataBinder = (function () {
    function AeviFormDataBinder(aeviFormRepository) {
        this.AeviFormRepository = aeviFormRepository;
    }
    AeviFormDataBinder.prototype.subscribe = function (columnName) {
        this.columnName = columnName;
    };
    AeviFormDataBinder.prototype.publish = function (value) {
        this.AeviFormRepository.items[this.columnName].Value = value;
    };
    return AeviFormDataBinder;
}());
var AeviFormRepository = (function () {
    function AeviFormRepository(aeviForm, aeviDataRepository, rowIndex) {
        this.AeviForm = aeviForm;
        this.AeviDataRepository = aeviDataRepository;
        this.rowIndex = rowIndex;
        var rows = this.AeviDataRepository.AeviFormDescription.getRows();
        this.items = {};
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            for (var j = 0; j < row.Items.length; j++) {
                var item = _.clone(row.Items[j]);
                if (AeviFormRepository.isEmpty(item)) {
                    this.items[item.ColumnName] = _.clone(item);
                    continue;
                }
                var columnIndex = this.AeviDataRepository.AeviTableDescription.getColumnIndexByName(item.ColumnName);
                var displayType = this.AeviDataRepository.AeviTableDescription.getColumnStringDisplayTypeByIndex(columnIndex);
                if (_.isNull(columnIndex)) {
                    this.AeviForm.AeviGrid.print('Form item with ColumnName: "' + item.ColumnName + '" is not in TableDescription.');
                    continue;
                }
                var value = null;
                switch (displayType) {
                    case 'Image':
                        var guid = this.AeviDataRepository.AeviTableData.getGuidByRowIndex(this.rowIndex);
                        value = this.AeviDataRepository.AeviGrid.AeviDataService.getImageData(guid, columnIndex);
                        break;
                    default:
                        value = this.AeviDataRepository.AeviTableData.getRecordCellValue(this.rowIndex, columnIndex);
                        break;
                }
                item.Value = value;
                item.RowIndex = i;
                item.ColumnIndex = columnIndex;
                item.IsRequired = this.AeviDataRepository.AeviTableDescription.isRequiredByColumnIndex(columnIndex);
                this.items[item.ColumnName] = item;
            }
        }
    }
    AeviFormRepository.isEmpty = function (item) {
        return (_.isNull(item.ColumnName) || _.isEmpty(item.ColumnName));
    };
    return AeviFormRepository;
}());
var AeviFormView = (function () {
    function AeviFormView() {
    }
    AeviFormView.getMainTemplate = function () {
        return _.template('<form class="aeviForm" id="aeviForm-<%= RecordIndex %>">' +
            '<fieldset class="well aeviForm__pager">' +
            '<div>' +
            '<%= PreviousTemplate %>' +
            '<%= NextTemplate %>' +
            '</div>' +
            '</fieldset>' +
            '<legend><%= EntityCaption %>, #<%= RowNumber %></legend>' +
            '<fieldset class="well aeviForm__content">' +
            '<%= Rows %>' +
            '</fieldset>' +
            '<div class="cfx text-right">' +
            '<button type="submit" class="aeviButton aeviButton--black aeviForm-<%= RecordIndex %>-close"><%= SaveWithoutClose %></button>' +
            '<%= SaveTemplate %>' +
            '</div>' +
            '<a class="aeviForm__close aeviForm-<%= RecordIndex %>-close"></a>' +
            '</form>' +
            '<div class="aeviForm__overlay"></div>');
    };
    AeviFormView.getPreviousArrowTemplate = function () {
        return _.template('<span id="aeviForm-<%= RecordIndex %>-left" class="aeviForm__left pull-left"><%= Previous %></span>');
    };
    AeviFormView.getNextArrowTemplate = function () {
        return _.template('<span id="aeviForm-<%= RecordIndex %>-right" class="aeviForm__right pull-right"><%= Next %></span>');
    };
    AeviFormView.getRowTemplate = function () {
        return _.template('<div class="row">' +
            '   <%= Columns %>' +
            '</div>');
    };
    AeviFormView.getSaveButtonTemplate = function () {
        return _.template('<button id="aeviForm-<%= RecordIndex %>-save" type="submit" class="aeviButton aeviButton--secondary"><%= Save %></button>');
    };
    AeviFormView.getColumnTemplate = function () {
        return _.template('<div class="form-group <%= Class %>" style="max-height: <%= MaxHeight %>;">' +
            '<label for="aeviForm-<%= ColumnName %>"><%= Caption %> <%= Required %></label>' +
            '<div class="aeviForm__editorWrapper"><%= EditorHTML %></div>' +
            '</div>');
    };
    AeviFormView.getEmptyColumnTemplate = function () {
        return _.template('<div class="aeviForm__col col-md-<%= ColumnWidth %>">' +
            '<%= Form %>' +
            '</div>');
    };
    AeviFormView.getEmptyTemplate = function () {
        return _.template('');
    };
    return AeviFormView;
}());
var AeviFormEditorFactory = (function () {
    function AeviFormEditorFactory(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.AeviDataRepository = aeviGrid.AeviDataRepository;
    }
    AeviFormEditorFactory.prototype.createFormEditor = function ($cell, indexes) {
        var column = this.AeviDataRepository.AeviTableDescription.getColumnHeader(indexes.cellIndex);
        if (aeviIsUndefinedOrNull(column)) {
            return;
        }
        var referenceSettingsColumn = this.AeviDataRepository.getReferenceSettings(indexes.rowIndex, indexes.cellIndex);
        if (!_.isNull(referenceSettingsColumn)) {
            column = referenceSettingsColumn;
        }
        if (!aeviIsUndefinedOrNull(column.EntityActionDefinition)) {
            return;
        }
        var displayType = this.AeviGrid.AeviConsts.dataTypes[column.DisplayType];
        switch (displayType) {
            case 'Image':
                this.editor = new AeviImageEditor(this.AeviGrid, $cell);
                break;
            case 'Text':
                this.editor = new AeviTextEditor(this.AeviGrid, $cell);
                break;
            case 'Number':
            case 'IntegerNumber':
                this.editor = new AeviNumberEditor(this.AeviGrid, $cell, displayType);
                break;
            case 'Currency':
                this.editor = new AeviCurrencyEditor(this.AeviGrid, $cell);
                break;
            case 'DateTime':
            case 'ShortDate':
            case 'ShortTime':
                this.editor = new AeviDateEditor(this.AeviGrid, $cell, displayType);
                break;
            case 'Enum':
                this.editor = new AeviEnumEditor(this.AeviGrid, $cell, column);
                break;
            case 'Boolean':
            case 'Hyperlink':
            case 'RegularExpression':
                this.editor = null;
                this.AeviGrid.print('AeviEditorFactory.createEditor(), "' + displayType + '" editor is not supported.');
                break;
            default:
                this.editor = null;
                break;
        }
    };
    AeviFormEditorFactory.prototype.getEditor = function () {
        return this.editor;
    };
    return AeviFormEditorFactory;
}());
var AeviHeaderColumn = (function () {
    function AeviHeaderColumn(aeviConsts) {
        this.AeviConsts = aeviConsts;
    }
    AeviHeaderColumn.prototype.render = function (column, columnIndex) {
        var displayType = this.AeviConsts.dataTypes[column.DisplayType];
        if (column.ColumnName === 'RowNumber')
            return AeviHeaderColumn.renderRowNumberColumn(displayType);
        return AeviHeaderColumn.renderOtherColumn(column, columnIndex, displayType);
    };
    AeviHeaderColumn.renderRowNumberColumn = function (displayType) {
        return '<th class="aeviSelectAll" id="col-0" data-type="' + displayType + '"></th>';
    };
    AeviHeaderColumn.renderOtherColumn = function (column, columnIndex, displayType) {
        var style = '';
        var renderSorting = true;
        if (column.Required) {
            if (column.Caption.charAt(0) !== '*')
                column.Caption = '* ' + column.Caption;
        }
        if (column.Visible !== true)
            style += ' display: none; ';
        if (!aeviIsUndefinedOrNull(column.EntityActionDefinition)) {
            if (column.EntityActionDefinition.ResultType === MyRetail.IAeviEntityActionResultType.Form) {
                renderSorting = false;
            }
        }
        var HTML = '<th id="col-' + columnIndex + '" class="aeviColumn col-' + columnIndex + '"' + 'style="' + style + '" data-columnName="' + column.ColumnName + '" data-type="' + displayType + '">' + column.Caption;
        if (renderSorting) {
            HTML += '<i class="aeviSort default"></i>';
        }
        HTML += '</th>';
        return HTML;
    };
    AeviHeaderColumn.prototype.refresh = function (column, columnIndex) {
        this.setColWidth(column, columnIndex);
    };
    AeviHeaderColumn.prototype.setColWidth = function (column, columnIndex) {
        if (column.Visible === false)
            return;
        var columnWidth = column.Width;
        var maxWidthPixels = column.MaxWidthPixels;
        var minWidthPixels = column.MinWidthPixels;
        var widthType = AeviHeaderColumn.getWidthType(column.WidthType);
        var columnWidthPixels;
        if (widthType === 'Pixels')
            columnWidthPixels = columnWidth;
        else
            columnWidthPixels = this.getColumnWidthByPercents(columnWidth);
        columnWidthPixels = AeviHeaderColumn.getColumnWidthInPixels(columnWidthPixels, maxWidthPixels, minWidthPixels);
        var columnElement = document.getElementById('col-' + columnIndex);
        AeviHeaderColumn.setWidth(columnElement, columnWidthPixels);
    };
    AeviHeaderColumn.setWidth = function (column, value) {
        column.setAttribute('data-width', value);
        column.style.width = value + 'px';
    };
    AeviHeaderColumn.prototype.getColumnWidthByPercents = function (columnWidth) {
        var wrapperWidth = Math.floor($(this.AeviConsts.aeviWrapperSelector).outerWidth()) - 1;
        var gridWidthInPixels = wrapperWidth - this.AeviConsts.firstColumnWidth - this.AeviConsts.scrollHeight;
        var onePercentPixels = gridWidthInPixels / 100;
        return columnWidth * onePercentPixels;
    };
    AeviHeaderColumn.getColumnWidthInPixels = function (columnWidthPixels, maxWidthPixels, minWidthPixels) {
        if (maxWidthPixels > 0) {
            if (columnWidthPixels > maxWidthPixels)
                columnWidthPixels = maxWidthPixels;
        }
        if (minWidthPixels > 0) {
            if (columnWidthPixels < minWidthPixels)
                columnWidthPixels = minWidthPixels;
        }
        return Math.floor(columnWidthPixels);
    };
    AeviHeaderColumn.getWidthType = function (value) {
        if (value === 0)
            return 'Pixels';
        return 'Percents';
    };
    return AeviHeaderColumn;
}());
var AeviHeaderRow = (function () {
    function AeviHeaderRow(aeviConsts) {
        this.AeviConsts = aeviConsts;
        this.headerData = [];
        this.columnsLength = 0;
    }
    AeviHeaderRow.prototype.render = function (columns) {
        var cols = '';
        var column = new AeviHeaderColumn(this.AeviConsts);
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].Visible === false)
                continue;
            cols += column.render(columns[i], i);
            this.columnsLength++;
        }
        var template = '<thead><tr>' +
            cols +
            '</tr></thead>';
        $(this.AeviConsts.tableSelector).append(template);
        this.setWidth(columns);
        this.cloneHeaderRow();
        this.setHeaderRowWidth();
        this.setTableWidth();
        if (this.isColumnsWidthThinnerThanTableWidth())
            this.recalculateColumnsWidth();
    };
    AeviHeaderRow.prototype.refresh = function (columns) {
        this.removeClonnedHeaderRow();
        this.render(columns);
    };
    AeviHeaderRow.prototype.setWidth = function (columns) {
        var column = new AeviHeaderColumn(this.AeviConsts);
        for (var i = 0; i < columns.length; i++)
            column.setColWidth(columns[i], i);
    };
    ;
    AeviHeaderRow.prototype.updateCols = function () {
        var columns = document.getElementById(this.AeviConsts.aeviTableId.makeClassFromSelector()).getElementsByTagName('th');
        var cols = [];
        for (var i = 0; i < columns.length; i++) {
            var columnWidth = parseInt(columns[i].getAttribute('data-width'));
            cols.push({
                index: i,
                width: columnWidth
            });
        }
        this.headerData.cols = cols;
    };
    AeviHeaderRow.prototype.removeClonnedHeaderRow = function () {
        $(this.AeviConsts.aeviWrapperSelector + ' table thead').remove();
        var heads = document.querySelectorAll('.clonnedHead');
        for (var i = 0; i < heads.length; i++) {
            if (i === 0)
                continue;
            var $head = $(heads[i]);
            $head.remove();
        }
    };
    AeviHeaderRow.prototype.cloneHeaderRow = function () {
        var cons = this.AeviConsts;
        this.updateCols();
        var tableHeadEl = $(cons.tableHeadSelector);
        var aeviTable = $(cons.aeviWrapperSelector + ' table');
        aeviTable.find('thead').clone().prependTo(tableHeadEl).wrap('<table id="' + cons.tableHeaderSelector.makeClassFromSelector() + '" class="clonnedHead"></div>');
    };
    AeviHeaderRow.prototype.getTableColumnsWidth = function () {
        var width = 0;
        for (var i = 0; i < this.headerData.cols.length; i++)
            width += this.headerData.cols[i].width;
        return width;
    };
    AeviHeaderRow.prototype.setTableWidth = function () {
        var tableWidth = $(this.AeviConsts.tableHeadSelector).outerWidth() - this.AeviConsts.firstColumnWidth;
        this.headerData.tableWidth = tableWidth;
    };
    AeviHeaderRow.prototype.setHeaderRowWidth = function (fixedWidth) {
        var columnsWidth = (aeviIsUndefinedOrNull(fixedWidth)) ? this.getTableColumnsWidth() : fixedWidth;
        var aeviTable = $(this.AeviConsts.aeviWrapperSelector + ' #aeviTable');
        var tableHeadEl = $(this.AeviConsts.tableHeadSelector);
        aeviTable.css('width', columnsWidth);
        $(this.AeviConsts.tableHeadSelector + ' table').css('width', columnsWidth);
        var tableHeaderHeight = tableHeadEl.outerHeight() * -1;
        aeviTable.css('margin-top', +tableHeaderHeight);
        this.headerData.headerWidth = columnsWidth;
    };
    AeviHeaderRow.prototype.getDifferenceSpace = function () {
        return this.headerData.tableWidth - this.headerData.headerWidth;
    };
    AeviHeaderRow.prototype.recalculateColumnsWidth = function () {
        var differenceSpace = this.getDifferenceSpace();
        var valueToAttribution = differenceSpace / this.columnsLength;
        var newColumnsWidth = 0;
        for (var i = 1; i < this.headerData.cols.length; i++) {
            var col = this.headerData.cols[i];
            var newColumnWidth = parseInt(col.width + valueToAttribution);
            newColumnsWidth += newColumnWidth;
            this.headerData.cols[i].width = newColumnWidth;
        }
        var tableHeadColumnsSelector = this.AeviConsts.aeviWrapperSelector + this.AeviConsts.tableHeadSelector + this.AeviConsts.columnSelector;
        this.setColumnsWidth(document.querySelectorAll(tableHeadColumnsSelector));
        var tableBodyColumnsSelector = this.AeviConsts.aeviWrapperSelector + this.AeviConsts.tableBodySelector + this.AeviConsts.columnSelector;
        this.setColumnsWidth(document.querySelectorAll(tableBodyColumnsSelector));
        this.setHeaderRowWidth(newColumnsWidth);
        this.checkEmptySpace();
    };
    AeviHeaderRow.prototype.setColumnsWidth = function (columns) {
        for (var i = 0; i < columns.length; i++)
            AeviHeaderColumn.setWidth(columns[i], this.headerData.cols[i + 1].width);
    };
    AeviHeaderRow.prototype.setLastColumnWidth = function (emptySpace) {
        var lastColumnWidth = this.headerData.cols[this.headerData.cols.length - 1].width;
        var lastColumn = [];
        var lastHeaderColumnSelector = this.AeviConsts.aeviWrapperSelector + this.AeviConsts.tableHeadSelector + this.AeviConsts.columnSelector;
        var lastHeadColumn = document.querySelectorAll(lastHeaderColumnSelector);
        lastColumn.push(lastHeadColumn[lastHeadColumn.length - 1]);
        var lastBodyColumnSelector = this.AeviConsts.aeviWrapperSelector + this.AeviConsts.tableBodySelector + this.AeviConsts.columnSelector;
        var lastBodyColumn = document.querySelectorAll(lastBodyColumnSelector);
        lastColumn.push(lastBodyColumn[lastBodyColumn.length - 1]);
        for (var i = 0; i < lastColumn.length; i++)
            AeviHeaderColumn.setWidth(lastColumn[i], Math.floor(lastColumnWidth + emptySpace));
    };
    AeviHeaderRow.prototype.checkEmptySpace = function () {
        var newColumnsWidth = this.headerData.headerWidth + this.AeviConsts.scrollHeight;
        var emptySpace = this.headerData.tableWidth - newColumnsWidth;
        if (emptySpace > 0) {
            this.setLastColumnWidth(emptySpace);
            this.setHeaderRowWidth(newColumnsWidth);
        }
    };
    AeviHeaderRow.prototype.isColumnsWidthThinnerThanTableWidth = function () {
        return (this.headerData.headerWidth < this.headerData.tableWidth);
    };
    return AeviHeaderRow;
}());
var AeviBodyCell = (function () {
    function AeviBodyCell(aeviDataRepository, aeviGrid) {
        this.AeviDataRepository = aeviDataRepository;
        this.AeviTableDescription = this.AeviDataRepository.AeviTableDescription;
        this.AeviGrid = aeviGrid;
        this.AeviLocalization = this.AeviGrid.AeviLocalization;
        this.dataTypes = this.AeviGrid.AeviConsts.dataTypes;
    }
    AeviBodyCell.prototype.render = function (rowIndex, cellIndex) {
        var htmlClass = (cellIndex === 0) ? 'aeviRowNumber' : 'aeviCell';
        var currentIndex = rowIndex + '.' + cellIndex;
        return '<td tabIndex="' + currentIndex + '" id="cell-' + currentIndex + '" class="' + htmlClass + '"><div class="aeviCellBorder"></div></td>';
    };
    AeviBodyCell.prototype.refresh = function (cell, cellValue, rowInfo, header) {
        var displayType = (aeviIsUndefinedOrNull(header)) ? '2' : this.AeviTableDescription.getDisplayType(header);
        var cellValues = this.getCellValues(displayType, cellValue, header, rowInfo.cellIndex, rowInfo);
        var aeviCellBorder = cell.childNodes[0];
        cell.className = cellValues.Class;
        var message = '';
        if (!aeviIsUndefinedOrNull(rowInfo.invalidCells[rowInfo.cellIndex])) {
            message = rowInfo.invalidCells[rowInfo.cellIndex].message;
        }
        cell.setAttributes({
            'data-action': cellValues.Action,
            'id': 'cell-' + rowInfo.rowIndex + '.' + rowInfo.cellIndex,
            'data-message': message
        });
        aeviCellBorder.innerHTML = cellValues.DisplayValue;
        aeviCellBorder.className = cellValues.ChildClass;
        return cellValues.SpecialStatus;
    };
    AeviBodyCell.prototype.getCellValues = function (displayType, displayValue, header, cellIndex, rowInfo) {
        var i;
        var cell = {
            DataValue: displayValue,
            DisplayValue: displayValue,
            Class: 'aeviCell',
            Style: '',
            ChildClass: 'aeviCellBorder',
            Action: null,
            SpecialStatus: ''
        };
        if (header.ColumnName === 'RowNumber') {
            cell.DataValue += 1;
            cell.DisplayValue += 1;
            cell.Class = 'aeviRowNumber';
        }
        if (!aeviIsUndefinedOrNull(rowInfo.invalidCells[cellIndex])) {
            if (!rowInfo.invalidCells[cellIndex].valid) {
                if (rowInfo.invalidCells[cellIndex].hard) {
                    cell.Class += ' validateError ';
                    cell.ChildClass += ' validateError ';
                }
                else {
                    cell.Class += ' softValidateError ';
                    cell.ChildClass += ' softValidateError ';
                }
            }
        }
        if (_.contains(rowInfo.selectedCells, cellIndex)) {
            cell.Class += ' selected ';
            cell.ChildClass += ' selected ';
        }
        if (!aeviIsUndefinedOrNull(header)) {
            if (header.Visible === false)
                cell.Class += ' hidden ';
            if (!this.AeviDataRepository.AeviTableDescription.isReport()) {
                if (header.ReadOnly)
                    cell.Class += ' readOnly ';
            }
            if (!aeviIsUndefinedOrNull(header.EntityActionDefinition)) {
                cell.Action = header.EntityActionDefinition.ActionId;
                cell.Class += ' aeviLink ';
                if (header.EntityActionDefinition.ResultType === MyRetail.IAeviEntityActionResultType.Form) {
                    cell.Class += ' aeviFormLink text-center';
                }
            }
        }
        var referenceSettingsColumn = this.AeviDataRepository.getReferenceSettings(rowInfo.rowIndex, cellIndex);
        if (!_.isNull(referenceSettingsColumn)) {
            header = referenceSettingsColumn;
            displayType = referenceSettingsColumn.DisplayType;
        }
        var values;
        var displayTypeString = this.dataTypes[displayType];
        switch (displayTypeString) {
            case 'DateTime':
            case 'ShortDate':
            case 'ShortTime':
                if (displayValue && displayValue.length > 0) {
                    var aeviDate = new AeviDate(this.AeviLocalization.getCulture(), displayType, displayValue);
                    cell.DisplayValue = aeviDate.getString();
                }
                break;
            case 'Number':
                values = this.getNumberValues(displayValue);
                if (header.ColumnName === 'RowNumber') {
                    values.DisplayValue++;
                }
                cell.DisplayValue = values.DisplayValue;
                cell.DataValue = values.DataValue;
                cell.Class += ' text-right ';
                break;
            case 'IntegerNumber':
                cell.Class += ' text-right ';
                break;
            case 'Enum':
                values = this.getEnumValues(displayValue, header);
                cell.DisplayValue = values.DisplayValue;
                cell.DataValue = values.DataValue;
                break;
            case 'Boolean':
                values = this.getBooleanValues(cell);
                cell.DisplayValue = values.DisplayValue;
                cell.DataValue = values.DataValue;
                cell.Class = values.Class;
                break;
            case 'Image':
                cell.DisplayValue = AeviImage.getDisplayValue(displayValue);
                cell.DataValue = AeviImage.getDataValue();
                break;
            case 'Currency':
                values = this.getCurrencyValues(cell, header);
                cell.DisplayValue = values.DisplayValue;
                cell.DataValue = values.DataValue;
                cell.Class += ' text-right ';
                break;
            default:
                break;
        }
        if (cell.DisplayValue === null || cell.DisplayValue === 'null' || cell.DisplayValue === '') {
            cell.DisplayValue = '';
            cell.DataValue = 'null';
        }
        if (this.AeviTableDescription.hasColumnAction(cellIndex)) {
            var columnAction = this.AeviTableDescription.getColumnActionType(cellIndex);
            switch (columnAction) {
                case MyRetail.IAeviColumnActionType.NegativeRowFormating:
                    var isValueNegative = AeviGlobal.isNegative(cell.DataValue);
                    if (isValueNegative) {
                        cell.SpecialStatus = ' negative ';
                    }
                    break;
                default:
                    break;
            }
        }
        return cell;
    };
    AeviBodyCell.prototype.getCurrencyValues = function (cell, header) {
        var val = {
            DataValue: null,
            DisplayValue: null
        };
        if (!_.isNull(cell.DisplayValue)) {
            val.DataValue = cell.DisplayValue;
            val.DisplayValue = val.DataValue;
            if (!aeviIsUndefinedOrNull(header.DecimalPlaceCount)) {
                val.DataValue = this.getNumberWithDecimalPlaceCount(val.DataValue, header.DecimalPlaceCount);
                val.DisplayValue = val.DataValue;
            }
            val.DisplayValue = aeviFormatNumber(val.DisplayValue).replace('.', ',');
            if (!_.isNull(header.CurrencyISOCode))
                val.DisplayValue += ' ' + header.CurrencyISOCode;
        }
        else {
            val.DataValue = cell.DataValue;
            val.DisplayValue = cell.DisplayValue;
        }
        return val;
    };
    AeviBodyCell.prototype.getNumberWithDecimalPlaceCount = function (number, decimalPlaceCount) {
        var roundedNumber = Math.round(number * 100) / 100;
        return roundedNumber.toFixed(decimalPlaceCount);
    };
    ;
    AeviBodyCell.prototype.getBooleanValues = function (cell) {
        var val = {};
        var checked = '';
        if (_.isNull(cell.DisplayValue)) {
            val.DataValue = null;
            val.DisplayValue = 'null';
            checked = 'null';
        }
        if (checked !== 'null')
            checked = (cell.DisplayValue) ? 'true' : 'false';
        val.DisplayValue = '<i class="aeviCellCheckboxRead ' + checked + '">' + checked + '</i>';
        val.DataValue = checked;
        val.Class = ' aeviCell textcenter ';
        if (this.AeviDataRepository.AeviTableDescription.readOnly) {
            val.DisplayValue = (checked === 'true') ? this.AeviGrid.AeviLocalization.translate('yes') : this.AeviGrid.AeviLocalization.translate('no');
            val.Class = val.Class.replace('textcenter', '');
        }
        return val;
    };
    AeviBodyCell.prototype.getNumberValues = function (cell) {
        var val = {};
        val.DataValue = cell;
        if (_.isNumber(cell))
            cell = cell.toString();
        if (_.isNull(cell))
            val.DisplayValue = cell;
        else
            val.DisplayValue = cell.replace('.', ',');
        return val;
    };
    AeviBodyCell.prototype.getEnumValues = function (displayValue, header, dataValue) {
        var val = {
            DataValue: null,
            DisplayValue: null
        };
        var enumValues = header.EnumValues;
        if (displayValue === 'true')
            displayValue = true;
        if (displayValue === 'false')
            displayValue = false;
        if (!_.isUndefined(enumValues)) {
            for (var i = 0; i < enumValues.length; i++) {
                if (!_.isNull(displayValue) && !_.isBoolean(displayValue)) {
                    displayValue = displayValue.toString().replace(' -', '-').replace('- ', '-').replace(' - ', '-');
                }
                if (_.isUndefined(dataValue)) {
                    if (enumValues[i].DataValue == displayValue) {
                        val.DataValue = enumValues[i].DataValue;
                        val.DisplayValue = enumValues[i].DisplayValue;
                    }
                }
                else {
                    if (enumValues[i].DisplayValue == displayValue) {
                        val.DataValue = enumValues[i].DataValue;
                        val.DisplayValue = enumValues[i].DisplayValue;
                    }
                }
            }
            if (_.isEmpty(val.DisplayValue) || val.DisplayValue == 'null') {
                val.DataValue = null;
                val.DisplayValue = '';
            }
            if (_.isNull(val.DataValue) && !_.isNull(displayValue)) {
                val = this.getEnumCombinationValues(val, enumValues, displayValue);
            }
        }
        return val;
    };
    AeviBodyCell.prototype.getEnumCombinationValues = function (val, enumValues, displayValue) {
        if (_.isNull(val.DataValue)) {
            for (var i = 0; i < enumValues.length; i++) {
                if (displayValue === enumValues[i].DataValue) {
                    val.DataValue = enumValues[i].DataValue;
                    val.DisplayValue = enumValues[i].DisplayValue;
                    return val;
                }
                var dataValueWithoutDisplayValue = this.getEnumDisplayValueWithoutDataValue(enumValues[i].DataValue, enumValues[i].DisplayValue);
                if (!_.isNull(dataValueWithoutDisplayValue) && !_.isNull(displayValue)) {
                    if (dataValueWithoutDisplayValue === displayValue) {
                        val.DataValue = enumValues[i].DataValue;
                        val.DisplayValue = enumValues[i].DisplayValue;
                        return val;
                    }
                }
            }
        }
        return {
            DataValue: null,
            DisplayValue: null
        };
    };
    AeviBodyCell.prototype.getEnumDisplayValueWithoutDataValue = function (dataValue, displayValue) {
        if (!_.isNull(dataValue) && !_.isNull(displayValue))
            return displayValue.toString().replace(dataValue.toString() + '-', '');
        return null;
    };
    AeviBodyCell.prototype.getValues = function (displayTypeString, header, displayValue, dataValue) {
        var values = {};
        switch (displayTypeString) {
            case 'Enum':
                values = this.getEnumValues(displayValue, header, displayValue);
                break;
        }
        return values;
    };
    AeviBodyCell.getOffset = function (element) {
        return element.getBoundingClientRect();
    };
    AeviBodyCell.isInViewport = function (cellEl, bodyEl) {
        var cell = AeviBodyCell.getOffset(cellEl);
        var body = AeviBodyCell.getOffset(bodyEl);
        var isInViewportVertically = (cell.top >= body.top && cell.bottom <= body.bottom) ? true : false;
        if (!isInViewportVertically)
            return false;
        var isInViewportHorizontally = (cell.left >= body.left && cell.right <= body.right) ? true : false;
        if (isInViewportHorizontally)
            return true;
        return false;
    };
    return AeviBodyCell;
}());
var AeviBodyRow = (function () {
    function AeviBodyRow(aeviDataRepository, aeviGrid) {
        this.AeviDataRepository = aeviDataRepository;
        this.AeviTableDescription = this.AeviDataRepository.AeviTableDescription;
        this.AeviGrid = aeviGrid;
        this.columnsLength = this.AeviTableDescription.getColumnsLength() - this.AeviTableDescription.fakeColumnsLength + this.AeviTableDescription.startFakeColumnsLength;
        this.AeviBodyCell = new AeviBodyCell(this.AeviDataRepository, this.AeviGrid);
    }
    AeviBodyRow.prototype.render = function (rowData, rowIndex) {
        var rowInfo = this.getRowInfo(rowData);
        var HTML = '<tr id="row-' + rowInfo.rowIndex + '" class="aeviRow ' + rowInfo.status + '">';
        for (var cellIndex = 0; cellIndex < this.columnsLength; cellIndex++) {
            var header = this.AeviTableDescription.getColumnHeader(cellIndex);
            if (header.Visible === false) {
                continue;
            }
            HTML += this.AeviBodyCell.render(rowIndex, cellIndex);
        }
        HTML += '</tr>';
        return HTML;
    };
    AeviBodyRow.prototype.refresh = function (row, rowData) {
        if (aeviIsUndefinedOrNull(row))
            return;
        if (aeviIsUndefinedOrNull(rowData)) {
            row.className = 'aeviRow hidden';
            return;
        }
        var rowInfo = this.getRowInfo(rowData);
        if (!this.AeviGrid.AeviDataService.AeviUser.roles.Create) {
            if (rowInfo.status.indexOf('newrow') !== -1) {
                rowInfo.status = AeviBodyRow.setRowAsHiddenAndGetHiddenStatus(rowInfo.status);
            }
        }
        row.className = 'aeviRow ' + rowInfo.status;
        row.id = 'row-' + rowInfo.rowIndex;
        var cellWithVisibleFalseLength = 0;
        for (var i = 0; i < this.columnsLength; i++) {
            var header = this.AeviTableDescription.getColumnHeader(i);
            if (header.Visible === false) {
                cellWithVisibleFalseLength++;
                continue;
            }
            rowInfo.cellIndex = i;
            var cell = row.childNodes[i - cellWithVisibleFalseLength];
            var specialStatus = this.AeviBodyCell.refresh(cell, rowData[i], rowInfo, header);
            AeviBodyRow.setSpecialStatus(row, specialStatus);
        }
    };
    AeviBodyRow.setSpecialStatus = function (row, status) {
        status = status.toString().replace(/ /g, '');
        if (status.length > 0)
            row.classList.add(status);
    };
    AeviBodyRow.setRowAsHiddenAndGetHiddenStatus = function (status) {
        status = status.replace('newrow', '').replace('render', '');
        return status += ' hidden';
    };
    AeviBodyRow.prototype.getRowInfo = function (listOfRows) {
        var rowIndex = listOfRows[this.AeviDataRepository.AeviTableDescription.getRowNumberIndex()];
        return {
            rowIndex: rowIndex,
            invalidCells: this.AeviDataRepository.getInvalidPosition(rowIndex),
            selectedCells: this.AeviDataRepository.getSelectedPosition(rowIndex),
            status: ' ' + this.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(rowIndex) + ' '
        };
    };
    return AeviBodyRow;
}());
var AeviClipboard = (function () {
    function AeviClipboard(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.browser = this.AeviGrid.AeviClientSide.browser.toLowerCase().replace(/[^a-zA-Z]/g, "");
        this.AeviDataService = this.AeviGrid.AeviDataService;
        this.canPasteOneCell = true;
    }
    AeviClipboard.prototype.getClipboardMode = function () {
        return this.AeviGrid.mode.clipboard;
    };
    AeviClipboard.prototype.allowOneCellPasting = function () {
        this.canPasteOneCell = true;
    };
    AeviClipboard.prototype.disableOneCellPasting = function () {
        this.canPasteOneCell = false;
    };
    AeviClipboard.prototype.copy = function () {
        var _this = this;
        if (aeviIsUndefinedOrNull(this.isDialogRendered) || !this.isDialogRendered) {
            var textToPutOnClipboard = this.AeviDataService.AeviDataRepository.AeviTableData.getRecordsToClipboard();
            this.renderDialog(textToPutOnClipboard);
        }
        else {
            setTimeout(function () {
                _this.hideDialog();
            }, 200);
        }
    };
    AeviClipboard.prototype.checkPastedData = function (parsedData, selectedCellIndexes) {
        if (aeviIsUndefinedOrNull(parsedData))
            return false;
        var pastedRowsLength = this.getPastedRowsLength(parsedData);
        if (this.isPastedRowsLengthExceeded(pastedRowsLength)) {
            this.showExceededModal();
            return false;
        }
        var pastedColumnsLength = this.getPastedColumnsLength(parsedData);
        var possibleColumnsLength = this.AeviGrid.AeviDataRepository.AeviTableDescription.getCountOfPossibleColumnsToCopy();
        if (this.isPastedRowsLengthExceededFixedSize(pastedRowsLength, selectedCellIndexes)) {
            this.AeviGrid.showMaxRowCountMessage();
            return false;
        }
        if (pastedColumnsLength !== possibleColumnsLength) {
            if (pastedColumnsLength !== 1) {
                if (this.getClipboardMode() === 0) {
                    this.showOneCellCellModal();
                    return false;
                }
            }
            if (this.getClipboardMode() === 1 && !this.canPasteOneCell) {
                this.showWrongColumnsModal();
                return false;
            }
        }
        var columnsLength = [];
        for (var i = 0; i < parsedData.Rows.length; i++) {
            var rowColumnsLength = 0;
            for (var j = 0; j < parsedData.Rows[i].length; j++)
                rowColumnsLength++;
            columnsLength.push(rowColumnsLength);
        }
        var firstRowColumnLen = columnsLength[0];
        for (i = 0; i < columnsLength.length; i++) {
            if (columnsLength[i] !== firstRowColumnLen) {
                this.showWrongTableModal();
                return false;
            }
        }
        if (columnsLength[0] === 1) {
            if (pastedRowsLength > 1 && this.getClipboardMode() === 0) {
                this.showOneCellCellModal();
                return false;
            }
            return true;
        }
        if (this.getClipboardMode() === 0) {
            this.showOneCellCellModal();
            return false;
        }
        if (this.getClipboardMode() === 0) {
            var rowStatus = this.AeviGrid.AeviDataService.AeviDataRepository.getStatusByRowIndex(selectedCellIndexes.rowId);
            if (rowStatus !== this.AeviGrid.AeviConsts.statuses.newrow) {
                this.showEndTableModal();
                return false;
            }
        }
        if (this.getClipboardMode() === 0) {
            if (selectedCellIndexes.cellId !== 1) {
                this.showDifferentThanFirstCellModal();
                return false;
            }
        }
        return true;
    };
    AeviClipboard.prototype.getDataToBePasted = function (event, dataToBePasted) {
        if (aeviIsUndefinedOrNull(dataToBePasted)) {
            return this.getClipboardData(event);
        }
        else {
            return dataToBePasted;
        }
    };
    AeviClipboard.prototype.setClipboardMode = function () {
        this.AeviGrid.mode.clipboard = 1;
        this.disableOneCellPasting();
    };
    AeviClipboard.prototype.paste = function (event, dataToBePasted) {
        var _this = this;
        event.preventDefault();
        if (aeviIsUndefinedOrNull(dataToBePasted)) {
            dataToBePasted = null;
        }
        var clipboardData = this.getDataToBePasted(event, dataToBePasted);
        if (!aeviIsUndefinedOrNull(this.AeviGrid.isClipboardModalOpened) && this.AeviGrid.isClipboardModalOpened) {
            this.setClipboardMode();
        }
        if (_.isNull(clipboardData)) {
            this.AeviGrid.print('AeviClipboard.paste(), variable "clipboardData" is null.');
            return;
        }
        var selectedCell = this.AeviGrid.AeviDOM.getSelectedCell();
        if (_.isNull(selectedCell) && this.getClipboardMode() === 0) {
            this.AeviGrid.print('AeviClipboard.paste(), variable "selectedCell" is null (no cell selected).');
            return;
        }
        var selectedCellIndex = (_.isNull(selectedCell) && this.getClipboardMode() === 1) ? null : this.AeviGrid.AeviDOM.getCellIndexes(selectedCell);
        var parsedData = this.parseData(clipboardData);
        if (!this.checkPastedData(parsedData, selectedCellIndex)) {
            this.AeviGrid.print('AeviClipboard.paste(), input clipboardData is not valid.');
            return;
        }
        if (this.canPasteOneCell && this.getClipboardMode() === 1) {
            this.AeviGrid.AeviDataService.pasteOneCell(parsedData, selectedCellIndex);
            this.AeviGrid.AeviGridEditor.refreshData();
            return;
        }
        if (this.getClipboardMode() === 1) {
            var indexWhereDataWillBePasted = {
                rowIndex: 0,
                cellIndex: this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.getFirstDataColumnIndex()
            };
            selectedCellIndex = indexWhereDataWillBePasted;
            this.AeviGrid.AeviDataService.AeviDataRepository.deleteRepositoryTableData();
            this.AeviGrid.AeviGridEditor.refreshData();
        }
        var reloadData = false;
        if (this.getClipboardMode() === 1) {
            this.AeviGrid.AeviDataService.commit(null, reloadData).done(function () {
                _this.AeviGrid.AeviDataService.paste(parsedData, selectedCellIndex);
                AeviModal.closeAll();
            });
        }
        else {
            this.AeviGrid.AeviDataService.paste(parsedData, selectedCellIndex);
            this.AeviGrid.AeviGridEditor.refreshData();
            this.AeviGrid.AeviPager.AeviPagerHandler.setJumps();
        }
    };
    AeviClipboard.prototype.getClipboardData = function (event) {
        var clipboardData = null;
        switch (this.browser) {
            case 'chrome':
            case 'safari':
            case 'firefox':
            case 'opera':
            case 'edge':
                clipboardData = event.clipboardData.getData('Text');
                break;
            case 'ie':
                clipboardData = window.clipboardData.getData("Text");
                break;
            default:
                toastr.error(this.AeviGrid.AeviLocalization.translate('clipboard_support_message'));
                return null;
        }
        return clipboardData;
    };
    AeviClipboard.prototype.parseData = function (clipboardData) {
        var _this = this;
        var outputData = {};
        var lastChar = clipboardData.slice(-1);
        var splitClipboardData = clipboardData.split(/\r\n|\r|\n/g);
        var dataRows = splitClipboardData;
        outputData.Rows = splitClipboardData;
        var countOfRows = outputData.Rows.length;
        var cells = [];
        for (var i = 0; i < dataRows.length; i++) {
            cells[i] = dataRows[i].split("\t");
            outputData.Rows[i] = cells[i];
        }
        var countOfCells = outputData.Rows[0].length;
        if (AeviClipboard.isLastCharEmpty(lastChar)) {
            outputData.Rows.length = (Array.isArray(outputData.Rows[countOfRows]) ? outputData.Rows.length : outputData.Rows.length - 1);
            countOfRows = outputData.Rows.length;
        }
        if (this.AeviGrid.AeviClientSide.isSafari()) {
            if (AeviClipboard.isLastCharEmpty(lastChar)) {
                outputData.Rows.length = (Array.isArray(outputData.Rows[countOfRows]) ? outputData.Rows.length : outputData.Rows.length - 1);
                countOfRows = outputData.Rows.length;
            }
        }
        this.countOfCells = countOfCells;
        this.countOfRows = countOfRows;
        return outputData;
    };
    AeviClipboard.isLastCharEmpty = function (lastChar) {
        if (lastChar === '\r\n' || lastChar === '\n')
            return true;
        return false;
    };
    AeviClipboard.prototype.renderDialog = function (data) {
        var _this = this;
        var copyModalId = this.AeviGrid.AeviConsts.copyModalId;
        this.copyModal = new AeviModal(this.AeviGrid);
        this.copyModal.setContent({
            id: copyModalId.replace('#', ''),
            title: this.AeviGrid.AeviLocalization.translate('warning'),
            text: '<p>' + this.AeviGrid.AeviLocalization.translate('manual_copy_message') + '</p>' + '<textarea>' + data + '</textarea>'
        });
        this.copyModal.show(null, function () {
            _this.hideDialog();
        });
        this.AeviGrid.AeviGridHandler.removeCellEditor();
        $(this.AeviGrid.AeviConsts.copyModalTextareaSelector).select();
        this.isDialogRendered = true;
    };
    AeviClipboard.prototype.hideDialog = function () {
        this.copyModal.close();
        this.isDialogRendered = false;
    };
    AeviClipboard.prototype.isPastedRowsLengthExceeded = function (rowsLength) {
        var maximumPastedRows = (aeviIsUndefinedOrNull(this.AeviGrid.AeviDataRepository.AeviTableDescription.description.MaxRowCountExcel)) ? this.AeviGrid.AeviConsts.clipboard.maximumCountOfInsertedRows : this.AeviGrid.AeviDataRepository.AeviTableDescription.description.MaxRowCountExcel;
        return (rowsLength > maximumPastedRows);
    };
    AeviClipboard.prototype.isPastedRowsLengthExceededFixedSize = function (rowsLength, selectedCellIndexes) {
        if (aeviIsUndefinedOrNull(this.AeviGrid.AeviDataRepository.maxRowCount))
            return false;
        if (aeviIsUndefinedOrNull(selectedCellIndexes)) {
            selectedCellIndexes = {
                rowId: 0,
                cellId: 1
            };
        }
        var firstPastedRowIndex = selectedCellIndexes.rowId;
        var newRowsLength = rowsLength + firstPastedRowIndex;
        return this.AeviGrid.AeviDataRepository.maxRowCount < newRowsLength;
    };
    AeviClipboard.prototype.getPastedRowsLength = function (clipboardData) {
        if (aeviIsUndefinedOrNull(clipboardData)) {
            this.AeviGrid.print('AeviClipboard.getPastedRowsLength(), parameter "clipboardData" is undefined or null.');
            return null;
        }
        if (clipboardData.Rows.length < 1) {
            this.AeviGrid.print('AeviClipboard.getPastedRowsLength(), parameter "clipboardData.Rows.length" is lower than 1.');
            return 0;
        }
        return clipboardData.Rows.length;
    };
    AeviClipboard.prototype.getPastedColumnsLength = function (clipboardData) {
        if (aeviIsUndefinedOrNull(clipboardData)) {
            this.AeviGrid.print('AeviClipboard.getPastedRowsLength(), parameter "clipboardData" is undefined or null.');
            return null;
        }
        if (clipboardData.Rows.length < 1) {
            this.AeviGrid.print('AeviClipboard.getPastedRowsLength(), parameter "clipboardData.Rows.length" is lower than 1.');
            return null;
        }
        if (clipboardData.Rows[0].length < 1) {
            this.AeviGrid.print('AeviClipboard.getPastedRowsLength(), parameter "clipboardData.Rows[0].length" is lower than 1.');
            return 0;
        }
        return clipboardData.Rows[0].length;
    };
    AeviClipboard.prototype.showExceededModal = function () {
        var maximumPastedRows = (aeviIsUndefinedOrNull(this.AeviGrid.AeviDataRepository.tableDesc.MaxRowCountExcel)) ? this.AeviGrid.AeviConsts.clipboard.maximumCountOfInsertedRows : this.AeviGrid.AeviDataRepository.tableDesc.MaxRowCountExcel;
        this.showModal({
            title: this.AeviGrid.AeviLocalization.translate('warning'),
            text: '<p>' + this.AeviGrid.AeviLocalization.translate('paste_exceeded') + ' ' + maximumPastedRows + ' ' + this.AeviGrid.AeviLocalization.translate('rows') + '</p>'
        });
    };
    AeviClipboard.prototype.showWrongColumnsModal = function () {
        this.showModal({
            title: this.AeviGrid.AeviLocalization.translate('warning'),
            text: '<p>' + this.AeviGrid.AeviLocalization.translate('paste_wrong_columns_count') + '</p>'
        });
    };
    AeviClipboard.prototype.showWrongTableModal = function () {
        this.showModal({
            title: this.AeviGrid.AeviLocalization.translate('warning'),
            text: '<p>' + this.AeviGrid.AeviLocalization.translate('paste_wrong_format') + '</p>'
        });
    };
    AeviClipboard.prototype.showWrongTableColumnsAndRowsModal = function () {
        this.showModal({
            title: this.AeviGrid.AeviLocalization.translate('warning'),
            text: '<p>' + this.AeviGrid.AeviLocalization.translate('paste_wrong_rows_and_cells') + '</p>'
        });
    };
    AeviClipboard.prototype.showEndTableModal = function () {
        this.showModal({
            title: this.AeviGrid.AeviLocalization.translate('warning'),
            text: '<p>' + this.AeviGrid.AeviLocalization.translate('paste_wrong_target_with_row') + '</p>'
        });
    };
    AeviClipboard.prototype.showDifferentThanFirstCellModal = function () {
        this.showModal({
            title: this.AeviGrid.AeviLocalization.translate('warning'),
            text: '<p>' + this.AeviGrid.AeviLocalization.translate('paste_wrong_target') + '</p>'
        });
    };
    AeviClipboard.prototype.showOneCellCellModal = function () {
        this.showModal({
            title: this.AeviGrid.AeviLocalization.translate('warning'),
            text: '<p>' + this.AeviGrid.AeviLocalization.translate('paste_allow_one_cell') + '</p>'
        });
    };
    AeviClipboard.prototype.showModal = function (content) {
        var modal = new AeviModal(this.AeviGrid);
        modal.setContent(content);
        modal.show();
    };
    return AeviClipboard;
}());
var AeviDate = (function () {
    function AeviDate(culture, type, value) {
        this.culture = culture;
        this.type = this._parseType(type);
        this.null = true;
        this.year = 0;
        this.month = 0;
        this.day = 0;
        this.hours = 0;
        this.minutes = 0;
        this.seconds = 0;
        this.parse(value);
    }
    AeviDate.prototype.parse = function (value) {
        if (value && value.length > 0) {
            if (value.indexOf('T') != -1)
                this.parseIsoString(value);
            else
                this.parseString(value);
        }
    };
    AeviDate.prototype.parseString = function (value) {
        if (value) {
            var parts = this._splitDateTime(value);
            if (parts.length) {
                var date = new Date();
                if (this.culture == 'cs') {
                    this.day = (parts[0] ? parts[0] : date.getDate());
                    this.month = (parts.length > 1 && parts[1] ? parts[1] : date.getMonth() + 1);
                }
                else {
                    if (parts.length > 1) {
                        this.day = (parts[1] ? parts[1] : date.getDate());
                        this.month = (parts[0] ? parts[0] : date.getMonth() + 1);
                    }
                    else {
                        this.day = (parts[0] ? parts[0] : date.getDate());
                    }
                }
                this.year = (parts.length > 2 && parts[2] ? parts[2] : date.getFullYear());
                if (parts.length > 3) {
                    this.hours = parts[3];
                    if (parts.length > 4) {
                        this.minutes = parts[4];
                        if (parts.length > 5)
                            this.seconds = parts[5];
                    }
                }
                this.null = false;
            }
        }
    };
    AeviDate.prototype.parseIsoString = function (value) {
        if (value && value.length > 10 && value[4] == '-' && value[7] == '-' && value[10] == 'T') {
            this.year = parseInt(value.slice(0, 4));
            this.month = parseInt(value.slice(5, 7));
            this.day = parseInt(value.slice(8, 10));
            this.null = false;
        }
        else
            this.null = true;
    };
    AeviDate.prototype._parseType = function (type) {
        if (type) {
            if (type == 'ShortDate' || type == '5')
                return 'ShortDate';
            if (type == 'ShortTime' || type == '6')
                return 'ShortTime';
        }
        return 'DateTime';
    };
    AeviDate.prototype._splitDateTime = function (value) {
        var result = [];
        var state = 1;
        var index;
        var separator = this._getDateSeparator();
        while (state > 0) {
            if (state == 1) {
                index = value.indexOf(separator);
                if (index != -1) {
                    if (!this._parseDateTimePart(value.substr(0, index), result))
                        break;
                    value = value.substr(index + 1);
                }
                else {
                    value = value.trim();
                    if (value.indexOf(':') != -1 || value.indexOf(' ') != -1) {
                        index = value.indexOf(' ');
                        if (index != -1) {
                            if (!this._parseDateTimePart(value.substr(0, index), result))
                                break;
                            value = value.substr(index + 1);
                        }
                        else {
                            while (result.length < 3)
                                result.push(0);
                        }
                        state = 2;
                    }
                    else {
                        this._parseDateTimePart(value, result);
                        state = 0;
                    }
                }
                if (result.length == 3 && state == 1)
                    break;
            }
            else {
                index = value.indexOf(':');
                if (index != -1) {
                    if (!this._parseDateTimePart(value.substr(0, index), result))
                        break;
                    value = value.substr(index + 1);
                    if (result.length == 6)
                        break;
                }
                else {
                    this._parseDateTimePart(value, result);
                    state = 0;
                }
            }
        }
        return (state ? [] : result);
    };
    AeviDate.prototype._getDateSeparator = function () {
        return this.culture == 'cs' ? '.' : '/';
    };
    AeviDate.prototype.isNull = function () {
        return this.null;
    };
    AeviDate.prototype.getDateFormat = function () {
        return this.culture == 'cs' ? 'd.m.yy' : 'mm/dd/yy';
    };
    AeviDate.prototype.getIsoString = function () {
        if (this.null)
            return '';
        return this.year + '-' + ('0' + this.month).slice(-2) + '-' + ('0' + this.day).slice(-2) + 'T00:00:00';
    };
    AeviDate.prototype.getString = function () {
        return this.getShortString();
    };
    AeviDate.prototype.getShortString = function () {
        if (this.null)
            return '';
        return (this.culture == 'cs' ?
            this.day + '.' + this.month + '.' + this.year :
            ('0' + this.month).slice(-2) + '/' + ('0' + this.day).slice(-2) + '/' + this.year);
    };
    AeviDate.prototype._parseDateTimePart = function (str, result) {
        if (str) {
            str = str.trim();
            if (str.length > 0) {
                var value = parseInt(str, 10);
                if (!isNaN(value) && value > 0 && ('0000' + value.toString()).slice(-str.length) == str) {
                    result.push(value);
                    return true;
                }
            }
        }
        return false;
    };
    return AeviDate;
}());
var AeviModalHandler = (function () {
    function AeviModalHandler(aeviGrid, modal, modalWindow) {
        this.AeviGrid = aeviGrid;
        this.modal = modal;
        this.modalWindow = modalWindow;
        this.listener();
    }
    AeviModalHandler.prototype.listener = function () {
        var _this = this;
        $(document).off('click', '#aeviDataServiceCommitTemp').on('click', '#aeviDataServiceCommitTemp', function (event) {
            _this.AeviGrid.AeviDataService.isServerDataInvalid = false;
            _this.commit(event, false);
        });
        $(document).off('click', '#aeviDataServiceCommitTempAndClose').on('click', '#aeviDataServiceCommitTempAndClose', function (event) {
            _this.AeviGrid.AeviDataService.isServerDataInvalid = false;
            _this.commit(event, true);
        });
        $(document).off('click', '#aeviDataServiceDeleteTemp').on('click', '#aeviDataServiceDeleteTemp', function (event) {
            _this.AeviGrid.AeviDataService.isServerDataInvalid = false;
            _this.deleteTemp(event, false);
        });
        $(document).off('click', '#aeviDataServiceDeleteTempAndClose').on('click', '#aeviDataServiceDeleteTempAndClose', function (event) {
            _this.AeviGrid.AeviDataService.isServerDataInvalid = false;
            _this.deleteTemp(event, true);
        });
        $(document).off('click', '#aeviDataServiceDeleteTempIgnoreAndClose').on('click', '#aeviDataServiceDeleteTempIgnoreAndClose', function (event) {
            _this.AeviGrid.AeviDataService.isServerDataInvalid = false;
            _this.deleteTemp(event, false);
        });
        $(document).off('click', '#aeviDataServiceClose').on('click', '#aeviDataServiceClose', function (event) {
            event.preventDefault();
            _this.AeviGrid.AeviDataService.showData();
            _this.modal.close();
        });
        $(document).off('click', '#aeviDataServiceDiscard').on('click', '#aeviDataServiceDiscard', function (event) {
            _this.discardTemp(event);
        });
    };
    AeviModalHandler.prototype.discardTemp = function (event) {
        var _this = this;
        event.preventDefault();
        this.AeviGrid.mode.clipboard = 0;
        this.AeviGrid.AeviDataRepository.enableOrDisableImageColumns('enable');
        this.AeviGrid.AeviDataService.getDataByQuery('', true).done(function () {
            _this.AeviGrid.AeviDataService.isServerDataInvalid = false;
            _this.AeviGrid.AeviGridLocker.unLock();
            _this.AeviGrid.AeviStatusBar.info(_this.AeviGrid.AeviLocalization.translate('discard_done_message'));
        });
        this.modal.close();
    };
    AeviModalHandler.prototype.deleteTemp = function (event, close) {
        event.preventDefault();
        var JSONFilter = '';
        this.AeviGrid.mode.clipboard = 0;
        if (!aeviIsUndefinedOrNull(this.AeviGrid.AeviFilter))
            JSONFilter = encodeURIComponent(this.AeviGrid.AeviFilter.getJSONFilterValues());
        this.AeviGrid.AeviDataService.getDataByQuery(JSONFilter, true);
        if (close)
            this.modal.close();
    };
    AeviModalHandler.prototype.commit = function (event, close) {
        var _this = this;
        event.preventDefault();
        var JSONFilter = '';
        if (!aeviIsUndefinedOrNull(this.AeviGrid.AeviFilter)) {
            JSONFilter = encodeURIComponent(this.AeviGrid.AeviFilter.getJSONFilterValues());
        }
        this.AeviGrid.AeviDataService.commit(JSONFilter, true).done(function () {
            if (close) {
                _this.modalWindow.close();
            }
        });
    };
    return AeviModalHandler;
}());
var AeviSimpleModal = (function () {
    function AeviSimpleModal(aeviGrid, callback) {
        this.AeviGrid = aeviGrid;
        this.callback = callback;
        this.listener();
    }
    AeviSimpleModal.prototype.listener = function () {
        var _this = this;
        $(document).off('click', '#aeviModalAccept').on('click', '#aeviModalAccept', function (event) {
            event.preventDefault();
            _this.callback(true);
            _this.close();
        });
        $(document).off('click', '#aeviModalDenied').on('click', '#aeviModalDenied', function (event) {
            event.preventDefault();
            _this.callback(false);
            _this.close();
        });
    };
    AeviSimpleModal.prototype.show = function (onShowFunction, onCloseFunction) {
        var _this = this;
        if (this.AeviGrid.isModalOpened)
            this.close();
        $.modal(this.content, {
            minWidth: this.minWidth,
            minHeight: this.minHeight,
            onShow: function (event) {
                _this.setSizes();
                _this.modalWindow = $(event.currentTarget)[0];
                _this.AeviModalHandler = new AeviModalHandler(_this.AeviGrid, _this, _this.modalWindow);
                _this.AeviGrid.isModalOpened = true;
                if (!aeviIsUndefinedOrNull(onShowFunction))
                    onShowFunction();
                if (!_this.showCloseButton) {
                    $('.modalCloseImg').remove();
                }
            },
            onClose: function () {
                _this.AeviGrid.isModalOpened = false;
                _this.close();
                if (!aeviIsUndefinedOrNull(onCloseFunction))
                    onCloseFunction();
            }
        });
    };
    AeviSimpleModal.prototype.close = function () {
        $.modal.close();
    };
    AeviSimpleModal.prototype.focus = function (what) {
        $.modal.focus(what);
    };
    AeviSimpleModal.prototype.getButtons = function (keys) {
        var modalButtons = {
            'aeviDataServiceCommitTemp': '<input type="button" id="aeviDataServiceCommitTemp" class="aeviButton aeviButton--secondary" value="' + this.AeviGrid.AeviLocalization.translate('save_data') + '">',
            'aeviDataServiceCommitTempAndClose': '<input type="button" id="aeviDataServiceCommitTempAndClose" class="aeviButton aeviButton--secondary" value="' + this.AeviGrid.AeviLocalization.translate('save_data') + '">',
            'aeviDataServiceDeleteTemp': '<input type="button" id="aeviDataServiceDeleteTemp" class="aeviButton" value="' + this.AeviGrid.AeviLocalization.translate('delete_data') + '">',
            'aeviDataServiceDeleteTempAndClose': '<input type="button" id="aeviDataServiceDeleteTempAndClose" class="aeviButton" value="' + this.AeviGrid.AeviLocalization.translate('delete_data') + '">',
            'aeviDataServiceDeleteTempIgnoreAndClose': '<input type="button" id="aeviDataServiceDeleteTempAndClose" class="aeviButton" value="' + this.AeviGrid.AeviLocalization.translate('ignore_errors') + '">',
            'aeviDataServiceClose': '<input type="button" id="aeviDataServiceClose" class="aeviButton" value="' + this.AeviGrid.AeviLocalization.translate('cancel') + '">',
            'aeviProtectedUnlock': '<input type="button" class="aeviButton aeviButton--secondary aeviProtected__unlock" value="' + this.AeviGrid.AeviLocalization.translate('button_enable_editing') + '">',
            'simpleModalClose': '<input type="button" class="aeviButton simplemodal-close" value="' + this.AeviGrid.AeviLocalization.translate('cancel') + '">',
            'aeviModalAccept': '<input type="button" id="aeviModalAccept" class="aeviButton aeviButton--secondary" value="' + this.AeviGrid.AeviLocalization.translate('yes') + '">',
            'aeviModalDenied': '<input type="button" id="aeviModalDenied" class="aeviButton" value="' + this.AeviGrid.AeviLocalization.translate('no') + '">',
            'aeviDataServiceDiscard': '<input type="button" id="aeviDataServiceDiscard" class="aeviButton aeviButton--secondary" value="' + this.AeviGrid.AeviLocalization.translate('ok') + '">'
        };
        var buttons = [];
        for (var i = 0; i < keys.length; i++)
            buttons.push(modalButtons[keys[i]]);
        return buttons.join('');
    };
    AeviSimpleModal.prototype.setOptions = function (options) {
        this.options = options;
        for (var key in this.options)
            this[key] = this.options[key];
    };
    AeviSimpleModal.prototype.setContent = function (content) {
        content.id = (aeviIsUndefinedOrNull(content.id)) ? 'aeviModalWindow' : content.id;
        content.title = (aeviIsUndefinedOrNull(content.title)) ? '' : content.title;
        content.text = (aeviIsUndefinedOrNull(content.text)) ? '' : content.text;
        content.buttons = (aeviIsUndefinedOrNull(content.buttons)) ? '' : this.getButtons(content.buttons);
        this.content =
            '<div id="' + content.id + '" class="aeviWsw aeviModal">' +
                '<h2>' + content.title + '</h2>' +
                content.text +
                '<div class="aeviModal__buttons text-right">' +
                content.buttons +
                '</div>' +
                '</div>';
        this.showCloseButton = (aeviIsUndefinedOrNull(content.showCloseButton) || content.showCloseButton === true) ? true : false;
    };
    AeviSimpleModal.closeAll = function () {
        $.modal.close();
    };
    AeviSimpleModal.prototype.setSizes = function () {
        if (aeviIsUndefinedOrNull(this.minWidth))
            return;
        $(this.AeviGrid.AeviConsts.aeviModalSelector).css({
            'max-width': this.minWidth
        });
    };
    return AeviSimpleModal;
}());
var AeviModal = (function (_super) {
    __extends(AeviModal, _super);
    function AeviModal() {
        _super.apply(this, arguments);
    }
    return AeviModal;
}(AeviSimpleModal));
var AeviGridEditor = (function () {
    function AeviGridEditor(aeviGrid, aeviDataService, sourceElementId) {
        this.AeviGrid = aeviGrid;
        this.AeviDataService = aeviDataService;
        this.AeviDataRepository = this.AeviGrid.AeviDataRepository;
        this.AeviApiService = this.AeviDataService.AeviApiService;
        this.sourceElementId = sourceElementId;
    }
    AeviGridEditor.prototype.renderTableHead = function () {
        this.table = $(this.AeviGrid.AeviConsts.tableSelector);
        if (this.AeviDataRepository.AeviTableDescription.isReport()) {
            this.table[0].classList.add('isReport');
        }
        this.headerRow = new AeviHeaderRow(this.AeviGrid.AeviConsts);
        this.headerRow.render(this.AeviDataRepository.AeviTableDescription.getColumns());
    };
    AeviGridEditor.prototype.renderTableBody = function (data) {
        var html = [];
        var from = 0;
        var to = data.length;
        var tbodySelector = this.table.selector + ' tbody';
        if (!this.AeviGrid.AeviDOM.length(tbodySelector)) {
            this.table.append('<tbody></tbody>');
        }
        var AeviDataRepository = this.AeviGrid.AeviDataService.AeviDataRepository;
        var bodyRow = new AeviBodyRow(AeviDataRepository, this.AeviGrid);
        for (var i = 0; i < to; i++)
            html.push(bodyRow.render(data[i], i));
        this.table.find('tbody').append(html.join(''));
        this.renderedRowsLength = to;
        this.refreshData({
            From: from,
            To: to
        });
    };
    AeviGridEditor.prototype.deleteTableBody = function () {
        this.table.find('tbody tr').remove();
    };
    AeviGridEditor.prototype.refreshData = function (range) {
        if (aeviIsUndefinedOrNull(range))
            range = this.actualRefreshRange;
        this.actualRefreshRange = range;
        var refreshData = this.AeviDataRepository.AeviTableData.getRecords(range.From, range.To);
        var table = this.AeviGrid.AeviDOM.getPureTable();
        var body = table.getElementsByTagName('tbody')[0];
        var rows = body.getElementsByTagName('tr');
        var bodyRow = new AeviBodyRow(this.AeviDataRepository, this.AeviGrid);
        var len = refreshData.length;
        len = this.AeviGrid.AeviConsts.pager.visibleRows;
        for (var i = 0; i < len; i++) {
            bodyRow.refresh(rows[i], refreshData[i]);
        }
        if (!_.isUndefined(this.AeviGrid.AeviPager))
            this.AeviGrid.AeviPager.setVisibleContentInfo();
        var selectedCells = this.AeviDataRepository.getSelectedCellsPositions();
        if (selectedCells) {
            if (selectedCells.cells.length < 2 && selectedCells.rows.length < 2) {
                var cell = this.AeviGrid.AeviDOM.getCell(selectedCells.cells[0], selectedCells.rows[0]);
                if (cell.length) {
                }
            }
        }
        setTimeout(function () {
            for (i = len + 1; i < rows.length; i++)
                rows[i].classList.add('hidden');
        });
        this.AeviGrid.checkRowTinge();
        this.AeviGrid.setSizes();
    };
    AeviGridEditor.prototype.refreshRow = function (properties) {
        var rowIndex = null;
        if (!_.isUndefined(properties.rowIndex))
            rowIndex = properties.rowIndex;
        if (!_.isUndefined(properties.guid))
            rowIndex = this.AeviDataRepository.AeviTableData.getRecordIndexByGuid(properties.guid);
        var locAeviBodyRow = new AeviBodyRow(this.AeviDataRepository, this.AeviGrid);
        var row = this.AeviGrid.AeviDOM.getPureRow(rowIndex);
        var refreshData = this.AeviDataRepository.AeviTableData.getRecords(rowIndex, rowIndex + 1);
        locAeviBodyRow.refresh(row, refreshData[0]);
        this.AeviGrid.setSizes();
    };
    AeviGridEditor.prototype.renderNewRow = function () {
        var _this = this;
        var addRow = 1;
        if (this.AeviDataRepository.isMaxRowCountExceeded(addRow)) {
            this.AeviGrid.showMaxRowCountMessage();
            return;
        }
        var selectedRowIndex = this.AeviGrid.AeviDOM.getSelectedRowIndex();
        if (_.isNull(selectedRowIndex)) {
            var lastSelectedCell = this.AeviGrid.AeviGridHandler.lastSelectedCell[0];
            var cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes(lastSelectedCell);
            selectedRowIndex = cellIndexes.rowIndex;
        }
        if (aeviIsUndefinedOrNull(selectedRowIndex)) {
            this.AeviGrid.print('AeviGridEditor.renderNewRow(), variable "rowIndex" is undefined or null.');
            return;
        }
        var properties = {
            Index: selectedRowIndex,
            Status: this.AeviGrid.AeviConsts.statuses.newrow + ' ' + this.AeviGrid.AeviConsts.statuses.sortedRow
        };
        this.AeviDataRepository.AeviTableData.insertRecord(properties);
        this.refreshData();
        this.AeviGrid.setSizes();
        var cellToBeFocused = this.AeviGrid.AeviDOM.getCell(1, selectedRowIndex + 1);
        setTimeout(function () {
            _this.AeviGrid.AeviGridHandler.cellChange(cellToBeFocused);
        });
    };
    AeviGridEditor.prototype.deleteRow = function () {
        var guids = this.getDeletedRowGuids();
        var rowNumberIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getRowNumberIndex();
        if (guids.length > 0) {
            var firstNotDeletedRecordIndex = this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableData.getRecord(guids[0])[rowNumberIndex] - 1;
            if (firstNotDeletedRecordIndex < 0)
                firstNotDeletedRecordIndex = 0;
            this.AeviGrid.AeviDataService.deleteData(guids.slice(), firstNotDeletedRecordIndex);
        }
        this.AeviGrid.setSizes();
    };
    AeviGridEditor.prototype.getDeletedRowGuids = function () {
        var guids = [];
        var selectedRowsIndexes = this.AeviDataRepository.AeviTableData.getSelectedRows();
        var selectedRowsLength = selectedRowsIndexes.length;
        for (var i = 0; i < selectedRowsLength; i++) {
            guids.push(this.AeviDataRepository.AeviTableData.getGuidByRowIndex(selectedRowsIndexes[i]));
        }
        return guids;
    };
    AeviGridEditor.prototype.showBorder = function (fromCellIndex, toCellIndex) {
        var selectedIndexes = [];
        var selectedCells = document.querySelectorAll('.aeviCell.selected');
        var selectedCellsLength = selectedCells.length;
        for (var i = 0; i < selectedCellsLength; i++) {
            var cell = selectedCells[i];
            var cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes(cell);
            selectedIndexes.push(cellIndexes.cellIndex);
            if (fromCellIndex.rowIndex == cellIndexes.rowId)
                cell.classList.add('selectedTop');
            if (toCellIndex.rowIndex == cellIndexes.rowId)
                cell.classList.add('selectedBottom');
        }
        var minCellIndex = _.min(selectedIndexes);
        var maxCellIndex = _.max(selectedIndexes);
        for (var i = 0; i < selectedCellsLength; i++) {
            var cell = selectedCells[i];
            var cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes(cell);
            if (cellIndexes.cellId === minCellIndex)
                cell.classList.add('selectedLeft');
            if (cellIndexes.cellId === maxCellIndex)
                cell.classList.add('selectedRight');
        }
    };
    return AeviGridEditor;
}());
var AeviDimensions = (function () {
    function AeviDimensions(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.cons = this.AeviGrid.AeviConsts;
        this.privateDimensions = [];
        this.fixedHeight = null;
        this.appWrapperScrollTop = $(this.cons.aeviWrapperSelector).offset().top;
        this.getInitialDimensions();
    }
    AeviDimensions.prototype.getInitialDimensions = function () {
        var elements = [
            window,
            this.cons.aeviWrapperSelector,
            this.cons.statusBarSelector,
            this.cons.toolbarSelector,
            this.cons.outerContainerSelector,
            this.cons.innerContainerSelector,
            this.cons.tableHeadSelector,
            this.cons.tableBodySelector
        ];
        this.setPrivateDimensions(elements);
        this.setDimensions();
    };
    AeviDimensions.prototype.setPrivateDimensions = function (elements) {
        for (var i = 0; i < elements.length; i++) {
            this.updatePrivateDimension(elements[i]);
            switch (elements[i]) {
                case this.cons.outerContainerSelector:
                case this.cons.tableBodySelector:
                    this.setDimension(AeviDimensions.getDimension(elements[i]));
                    this.updatePrivateDimension(elements[i]);
                    break;
                default:
                    break;
            }
        }
    };
    AeviDimensions.prototype.updatePrivateDimension = function (selector) {
        var info = AeviDimensions.getDimension(selector);
        var key = (_.isObject(info.selector)) ? 'window' : info.selector.makeClassFromSelector().replace('-', '_');
        this.privateDimensions[key] = info;
    };
    AeviDimensions.prototype.setOuterContainerHeight = function () {
        var visibleRecordsLength = this.AeviGrid.AeviDataRepository.getVisibleAndNewRecordsLength();
        if (this.AeviGrid.mode.clipboard === 1) {
            visibleRecordsLength = this.AeviGrid.AeviDataRepository.AeviTableData.getRecordsLength();
        }
        var tableBodyRowsHeight = visibleRecordsLength * this.cons.rowHeight;
        var outerContainerHeight;
        var minimumRowsHeight = this.cons.minimumRows * this.cons.rowHeight;
        if (this.getMode() === 'Standard') {
            outerContainerHeight = AeviDimensions.getViewport().height;
            outerContainerHeight -= this.appWrapperScrollTop;
            outerContainerHeight -= this.cons.statusBarHeight;
            outerContainerHeight -= this.privateDimensions.aeviToolbar.height;
            outerContainerHeight -= this.cons.outerContainerBottomEmptySpace;
            var tableBodyHeight = outerContainerHeight - this.privateDimensions.table_head.height - this.cons.scrollHeight;
            if (AeviDimensions.isTableBodyHeightLowerThanMinimumRowsHeight(tableBodyHeight, minimumRowsHeight))
                outerContainerHeight = this.getMinimumOuterContainerHeight();
            if (this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport())
                outerContainerHeight = this.subtractSumElementHeight(outerContainerHeight);
        }
        tableBodyRowsHeight = this.getEmptySpace(tableBodyRowsHeight);
        if (AeviDimensions.isRecordsHeightIsLowerThanHeight(tableBodyRowsHeight, outerContainerHeight)) {
            outerContainerHeight = tableBodyRowsHeight + this.cons.scrollHeight + this.privateDimensions.table_head.height;
        }
        this.updateDimension({ selector: this.cons.outerContainerSelector, height: outerContainerHeight });
    };
    AeviDimensions.prototype.getEmptySpace = function (tableBodyRowsHeight) {
        if (this.AeviGrid.mode.clipboard === 1)
            tableBodyRowsHeight = this.subtractRow(tableBodyRowsHeight);
        tableBodyRowsHeight = this.setEmptyRowSpace(tableBodyRowsHeight);
        return tableBodyRowsHeight;
    };
    AeviDimensions.prototype.addRow = function (tableBodyRowsHeight) {
        return tableBodyRowsHeight + this.cons.rowHeight;
    };
    AeviDimensions.prototype.subtractRow = function (tableBodyRowsHeight) {
        return tableBodyRowsHeight - this.cons.rowHeight;
    };
    AeviDimensions.prototype.setEmptyRowSpace = function (tableBodyRowsHeight) {
        return this.addRow(tableBodyRowsHeight);
    };
    AeviDimensions.prototype.subtractSumElementHeight = function (height) {
        if (this.AeviGrid.AeviDOM.length('.aeviSum'))
            return height - ($('.aeviSum').outerHeight() + this.cons.sumContainerBottomEmptySpace);
        return height;
    };
    AeviDimensions.prototype.getMinimumOuterContainerHeight = function () {
        return (this.cons.minimumRows * this.cons.rowHeight) + this.cons.scrollHeight + this.privateDimensions.table_head.height;
    };
    AeviDimensions.isTableBodyHeightLowerThanMinimumRowsHeight = function (tableBodyHeight, minimumRowsHeight) {
        return (tableBodyHeight < minimumRowsHeight);
    };
    AeviDimensions.isRecordsHeightIsLowerThanHeight = function (tableBodyRowsHeight, height) {
        return (tableBodyRowsHeight < height);
    };
    AeviDimensions.getViewport = function () {
        var a;
        var e;
        if (!('innerWidth' in window)) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        else {
            e = window;
            a = 'inner';
        }
        return {
            width: e[a + 'Width'],
            height: e[a + 'Height']
        };
    };
    AeviDimensions.prototype.setTableBodyHeight = function () {
        var height = AeviDimensions.getDimension(this.cons.outerContainerSelector).height - this.privateDimensions.table_head.height;
        this.updateDimension({ selector: this.cons.tableBodySelector, height: height });
    };
    AeviDimensions.prototype.setAppWrapperHeight = function () {
        var height = 0;
        height += this.cons.statusBarHeight;
        height += this.privateDimensions.aeviToolbar.height;
        height += this.privateDimensions.table_head.height;
        height += this.privateDimensions.table_body.height;
        this.updateDimension({ selector: this.cons.aeviWrapperSelector, height: height });
    };
    AeviDimensions.prototype.setDimensions = function () {
        this.setOuterContainerHeight();
        this.setTableBodyHeight();
        this.setAppWrapperHeight();
        if (!_.isUndefined(this.AeviGrid.AeviPager))
            this.AeviGrid.AeviPager.setScrollContainerHeight((this.AeviGrid.AeviDataService.AeviDataRepository.getVisibleRecordsLength()) * this.cons.rowHeight);
    };
    AeviDimensions.prototype.updateDimension = function (object) {
        var key = AeviDimensions.getDimension(object.selector);
        key.height = object.height;
        if (!_.isUndefined(object.width))
            key.width = object.width;
        this.setDimension(key);
        this.updatePrivateDimension(object.selector);
    };
    AeviDimensions.prototype.setDimension = function (object) {
        switch (object.selector) {
            case this.cons.outerContainerSelector:
                $(object.selector).css({
                    'height': object.height,
                    'top': parseInt(this.privateDimensions.aeviToolbar.height + this.cons.statusBarHeight)
                });
                break;
            default:
                $(object.selector).height(object.height);
                break;
        }
    };
    AeviDimensions.getDimension = function (selector) {
        var el = (_.isObject(selector)) ? $(selector) : $(selector);
        return {
            selector: selector,
            width: el.outerWidth(true),
            height: el.outerHeight(true)
        };
    };
    AeviDimensions.prototype.getMode = function () {
        if (_.isNull(this.fixedHeight) || this.fixedHeight < 1)
            return 'Standard';
        return 'Extended';
    };
    return AeviDimensions;
}());
var MyRetail;
(function (MyRetail) {
    (function (putType) {
        putType[putType["insert"] = 0] = "insert";
        putType[putType["update"] = 1] = "update";
    })(MyRetail.putType || (MyRetail.putType = {}));
    var putType = MyRetail.putType;
})(MyRetail || (MyRetail = {}));
var AeviDataBinder = (function () {
    function AeviDataBinder(aeviTableData) {
        this.AeviTableData = aeviTableData;
    }
    AeviDataBinder.prototype.subscribe = function (indexes) {
        this.indexes = indexes;
    };
    AeviDataBinder.prototype.publish = function (editorValue) {
        var dataValue = null;
        var header = this.AeviTableData.AeviTableDescription.getColumnHeader(this.indexes.cellIndex);
        var displayType = this.AeviTableData.AeviTableDescription.getDisplayType(header);
        var displayStringType = this.AeviTableData.AeviTableDescription.getColumnStringDisplayType(displayType);
        switch (displayStringType) {
            case 'Number':
            case 'Currency':
            case 'IntegerNumber':
                dataValue = editorValue.replace(',', '.');
                dataValue = parseFloat(dataValue);
                if (_.isNaN(dataValue)) {
                    dataValue = '';
                }
                break;
            default:
                dataValue = editorValue;
                break;
        }
        this.AeviTableData.updateRecordCellValue(this.indexes.rowIndex, this.indexes.cellIndex, dataValue);
    };
    return AeviDataBinder;
}());
var AeviEditor = (function () {
    function AeviEditor(aeviGrid, cell, settings) {
        this.AeviGrid = aeviGrid;
        this.$cell = cell;
        this.cell = this.$cell[0];
        this.border = this.cell.childNodes[0];
        this.borderWidth = $(this.border).outerWidth();
        this.cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes(this.cell);
        this.editorId = 'aeviTextEditor';
        this.isDataValueChanged = false;
        this.fakeInputId = this.AeviGrid.AeviConsts.fakeInputSelector;
        this.AeviBubble = new AeviBubbless(this.AeviGrid);
        this.AeviBubble.render(this.$cell);
        this.showBubble();
        this.setState('initial');
    }
    AeviEditor.prototype.showBubble = function () {
        var AeviDataRepository = this.AeviGrid.AeviDataRepository;
        if (AeviDataRepository.AeviTableDescription.isReport()) {
            return;
        }
        var rowIndex = this.cellIndexes.rowIndex;
        var message = null;
        if (AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'invalid') || AeviDataRepository.AeviTableData.AeviStatus.is(rowIndex, 'softInvalid')) {
            message = AeviDataRepository.AeviTableData.getRecordCellValue(rowIndex, AeviDataRepository.AeviTableDescription.getInvalidCellsIndex())[this.cellIndexes.cellIndex].message;
        }
        if (!aeviIsUndefinedOrNull(this.AeviBubble)) {
            this.AeviBubble.show(this.$cell, message);
        }
    };
    AeviEditor.prototype.getState = function () {
        return this.editorState;
    };
    AeviEditor.prototype.setState = function (state) {
        this.editorState = state;
    };
    AeviEditor.prototype.setReady = function () {
        var _this = this;
        $(this.border).off('mousedown').on('mousedown', function (event) {
            if (_this.getState() === 'ready') {
                if (_this.AeviGrid.AeviDOM.getTagName(event.currentTarget) === 'TEXTAREA') {
                    return false;
                }
                _this.render();
                var cursorPosition = _this.calculateCursorPosition(event);
                setTimeout(function () {
                    setSelectionRange($(_this.editorEl).focus().get(0), cursorPosition, cursorPosition);
                });
            }
        });
        var input = $(this.fakeInputId);
        setTimeout(function () {
            _this.AeviGrid.AeviDOM.focusWithoutScroll(input);
        }, 10);
        setTimeout(function () {
            _this.setState('ready');
        }, 80);
        $(document).on('keydown', this.fakeInputId, function (event) {
            if (!_this.isInputValueValid(_this.getDataVal(), event)) {
                _this.AeviBubble.show(_this.$cell, _this.AeviGrid.AeviLocalization.translate('cannot_insert_char'));
                event.preventDefault();
                return false;
            }
            if (_this.getState() === 'ready') {
                if (!_this.AeviGrid.AeviClientSide.isNavigationKeyPressed(event)) {
                    _this.render();
                }
            }
        });
    };
    AeviEditor.prototype.render = function () {
        this.removeReadyListeners();
        var value = this.getDataVal();
        this.defaultValue = value;
        var inputValue = (_.isNull(value) || value === 'null') ? '' : this.defaultValue;
        this.cell.innerHTML = '<div class="aeviCellBorder"><textarea id="' + this.editorId + '" class="aeviCellInput" value="' + inputValue + '">' + inputValue + '</textarea></div>';
        this.cell.classList.add('hasEditor');
        this.cellClass = this.cell.getAttribute('class');
        this.editorEl = this.getEditorEl();
        $(this.editorEl).select().focus();
        this.setState('render');
        this.listener();
    };
    AeviEditor.prototype.renderFormEditor = function (column, fakeIndexes) {
        var dataValue = column.Value;
        if (_.isNull(dataValue) || dataValue === 'null') {
            dataValue = '';
        }
        this.cellIndexes = fakeIndexes;
        this.columnName = column.ColumnName;
        var validity = this.AeviGrid.AeviDataRepository.AeviTableData.getValidityInfo(this.cellIndexes.rowIndex, this.cellIndexes.cellIndex);
        var validityClass = '';
        if (!validity.valid) {
            if (validity.hard) {
                validityClass = 'validateError';
            }
            else {
                validityClass = 'softValidateError';
            }
        }
        var readOnly = '';
        if (column.IsReadOnly) {
            readOnly = 'readonly';
        }
        var template = _.template('<input id="aeviForm-<%= ColumnName %>" <%= ReadOnly %> class="form-control <%= ValidityClass %>" name="<%= ColumnName %>" data-columnName="<%= ColumnName %>" type="text" value="<%= DataValue %>">');
        return template({
            ColumnName: column.ColumnName,
            ValidityClass: validityClass,
            DataValue: dataValue,
            ReadOnly: readOnly
        });
    };
    AeviEditor.prototype.remove = function () {
        this.removeReadyListeners();
        this.AeviBubble.destroy();
        if (this.getState() !== 'render') {
            this.setState('initial');
            return;
        }
        this.setCellVal(this.getEditorVal());
        if (this.isValueChanged(this.getDataVal())) {
            this.isDataValueChanged = true;
        }
        this.setState('initial');
        $(document).off('keydown keyup', '#' + this.editorId);
    };
    AeviEditor.prototype.isValueChanged = function (val) {
        return (val !== this.defaultValue);
    };
    AeviEditor.prototype.getEditorEl = function () {
        return document.getElementById(this.editorId);
    };
    AeviEditor.prototype.getEditorVal = function () {
        if (aeviIsUndefinedOrNull(this.editorEl)) {
            this.AeviGrid.print('AeviEditor.getEditorVal(), "this.editorEl" is undefined or null.');
            return null;
        }
        return this.editorEl.value;
    };
    AeviEditor.prototype.setCellVal = function (val) {
        this.cell.innerHTML = '<div class="aeviCellBorder">' + val + '</div>';
    };
    AeviEditor.prototype.getCellVal = function () {
        return this.cell.innerText;
    };
    AeviEditor.prototype.getDataVal = function () {
        return this.AeviGrid.AeviDataRepository.AeviTableData.getRecordCellValue(this.cellIndexes.rowId, this.cellIndexes.cellId);
    };
    AeviEditor.prototype.removeCellClasses = function () {
        this.cellClass = this.cellClass.replace('hasEditor', '').replace('typing', '');
        this.cell.className = this.cellClass;
    };
    AeviEditor.prototype.initBinder = function () {
        this.binder = new AeviDataBinder(this.AeviGrid.AeviDataRepository.AeviTableData);
        this.binder.subscribe(this.cellIndexes);
    };
    AeviEditor.prototype.initFormBinder = function (formRepository) {
        this.binder = new AeviFormDataBinder(formRepository);
        this.binder.subscribe(this.columnName);
    };
    AeviEditor.prototype.formListener = function () {
        var _this = this;
        var editorWrapper = $('#aeviForm-' + this.columnName).closest('.aeviForm__editorWrapper');
        var destroyBubbleIfIsRendered = false;
        this.AeviBubble = new AeviBubbless(this.AeviGrid, destroyBubbleIfIsRendered);
        this.AeviBubble.render(editorWrapper);
        $(document).on('mousedown', '#aeviForm-' + this.columnName, function (event) {
            _this.showBubble();
        });
        $(document).on('keyup', '#aeviForm-' + this.columnName, function (event) {
            var editor = event.currentTarget;
            _this.binder.publish(editor.value);
            var validatorFactory = new AeviValidatorFactory(_this.AeviGrid, _this.AeviGrid.AeviDataRepository.AeviLocalization, _this.AeviGrid.AeviDataRepository.AeviTableDescription);
            validatorFactory.createValidator(editor.value, _this.cellIndexes.cellIndex);
            var validator = validatorFactory.getValidator();
            var result = validator.validate();
            if (!result.valid) {
                _this.AeviBubble.show(editorWrapper, result.message);
            }
        });
        $(document).on('keydown', '#aeviForm-' + this.columnName, function (event) {
            var editor = event.currentTarget;
            var value = editor.value;
            _this.AeviBubble.hide();
            if (!_this.isInputValueValid(value, event)) {
                _this.AeviBubble.show(editorWrapper, _this.AeviGrid.AeviLocalization.translate('cannot_insert_char'));
            }
        });
        $(document).on('blur', '#aeviForm-' + this.columnName, function (event) {
            _this.AeviBubble.hide();
        });
    };
    AeviEditor.prototype.listener = function () {
        var _this = this;
        this.initBinder();
        $(document).on('keydown', '#' + this.editorId, function (event) {
            var codeKey = _this.AeviGrid.AeviClientSide.getKeyName(event);
            if (codeKey === 'escape') {
                _this.editorEl.value = _this.defaultValue;
            }
            if (codeKey === 'left arrow') {
                _this.goPrevCellIfRequired(_this.$cell, $(_this.editorEl), event);
            }
            if (codeKey === 'right arrow') {
                _this.goNextCellIfRequired(_this.$cell, $(_this.editorEl), event);
            }
            if (!_this.isInputValueValid(_this.getEditorVal(), event)) {
                _this.AeviBubble.show(_this.$cell, _this.AeviGrid.AeviLocalization.translate('cannot_insert_char'));
            }
        });
        $(document).on('keyup', '#' + this.editorId, function (event) {
            _this.binder.publish(_this.getEditorVal());
        });
    };
    AeviEditor.prototype.removeReadyListeners = function () {
        $(document).off('keydown', this.fakeInputId);
        $(this.border).off('mousedown');
    };
    AeviEditor.prototype.calculateCursorPosition = function (event) {
        var offsetHelper = 0;
        if (this.AeviGrid.AeviClientSide.isChrome() || this.AeviGrid.AeviClientSide.isSafari())
            offsetHelper = 5;
        var cellX = this.$cell.offset().left + offsetHelper;
        var clickedX = event.pageX - cellX;
        var textWidth = $(this.editorEl).textWidth();
        var editorValue = this.getEditorVal();
        var charCount = (aeviIsUndefinedOrNull(editorValue)) ? 0 : editorValue.length;
        var oneCharWidth = Math.round(textWidth / charCount);
        return Math.floor(clickedX / oneCharWidth);
    };
    AeviEditor.prototype.isInputValueValid = function (value, event) {
        return true;
    };
    AeviEditor.prototype.goPrevCellIfRequired = function (cell, editor, event) {
        var caretPosition = this.getCaretPosition(editor);
        if (0 === caretPosition) {
            event.preventDefault();
            event.stopPropagation();
            this.AeviGrid.AeviGridHandler.AeviBodyCellHandler.shiftTab(cell);
            return;
        }
    };
    AeviEditor.prototype.goNextCellIfRequired = function (cell, editor, event) {
        var caretPosition = this.getCaretPosition(editor);
        if (editor.val().length === caretPosition) {
            event.preventDefault();
            event.stopPropagation();
            this.AeviGrid.AeviGridHandler.AeviBodyCellHandler.tab(cell);
            return;
        }
    };
    AeviEditor.prototype.getCaretPosition = function ($input) {
        var input = $input.get(0);
        if (!input)
            return;
        if (document.selection)
            input.focus();
        return 'selectionStart' in input ? input.selectionStart : '' || Math.abs(document.selection.createRange().moveStart('character', -input.value.length));
    };
    return AeviEditor;
}());
var AeviTextEditor = (function (_super) {
    __extends(AeviTextEditor, _super);
    function AeviTextEditor() {
        _super.apply(this, arguments);
    }
    return AeviTextEditor;
}(AeviEditor));
var AeviNumberEditor = (function (_super) {
    __extends(AeviNumberEditor, _super);
    function AeviNumberEditor(aeviGrid, cell, dataType) {
        _super.call(this, aeviGrid, cell);
        this.editorId = 'aeviNumberEditor';
        this.dataType = dataType;
    }
    AeviNumberEditor.prototype.render = function () {
        _super.prototype.render.call(this);
        this.editorEl.classList.add('text-right');
    };
    AeviNumberEditor.prototype.isInputValueValid = function (value, event) {
        var keyCode = event.keyCode;
        if (!_.isNull(value)) {
            value = value.toString();
            if ((~value.indexOf(',') && (keyCode == 188 || keyCode == 190 || keyCode == 110)) ||
                (~value.indexOf('.') && (keyCode == 190 || keyCode == 188 || keyCode == 110))) {
                event.preventDefault();
                return false;
            }
        }
        if ($.inArray(keyCode, [46, 8, 9, 27, 13, 110, 190, 188]) !== -1 ||
            (keyCode == 65 && (event.ctrlKey === true || event.metaKey === true)) ||
            (keyCode >= 35 && keyCode <= 40)) {
            return true;
        }
        if (((keyCode < 48 || keyCode > 57)) && (keyCode < 96 || keyCode > 105)) {
            event.preventDefault();
            return false;
        }
        if (keyCode > 47 && keyCode < 58) {
            if (!event.shiftKey) {
                event.preventDefault();
                return false;
            }
        }
        return true;
    };
    return AeviNumberEditor;
}(AeviEditor));
var AeviCurrencyEditor = (function (_super) {
    __extends(AeviCurrencyEditor, _super);
    function AeviCurrencyEditor(aeviGrid, cell) {
        _super.call(this, aeviGrid, cell);
        this.editorId = 'aeviCurrencyEditor';
    }
    AeviCurrencyEditor.prototype.render = function () {
        _super.prototype.render.call(this);
        this.editorEl.classList.add('text-right');
    };
    AeviCurrencyEditor.prototype.getEditorVal = function () {
        return AeviCurrencyEditor.setAndGetDot(this.editorEl.value);
    };
    AeviCurrencyEditor.prototype.isInputValueValid = function (value, event) {
        var keyCode = event.keyCode;
        if (!_.isNull(value)) {
            value = value.toString();
            if ((~value.indexOf(',') && (keyCode == 188 || keyCode == 190 || keyCode == 110)) ||
                (~value.indexOf('.') && (keyCode == 190 || keyCode == 188 || keyCode == 110))) {
                event.preventDefault();
                return false;
            }
        }
        if ($.inArray(keyCode, [46, 8, 9, 27, 13, 110, 190, 188]) !== -1 ||
            (keyCode == 65 && (event.ctrlKey === true || event.metaKey === true)) ||
            (keyCode >= 35 && keyCode <= 40)) {
            return true;
        }
        if (((keyCode < 48 || keyCode > 57)) && (keyCode < 96 || keyCode > 105)) {
            event.preventDefault();
            return false;
        }
        if (keyCode > 47 && keyCode < 58) {
            if (!event.shiftKey) {
                event.preventDefault();
                return false;
            }
        }
        return true;
    };
    AeviCurrencyEditor.setAndGetDot = function (value) {
        return value.replaceAll(',', '.').replace(/ /g, '');
    };
    return AeviCurrencyEditor;
}(AeviEditor));
var AeviActionEditor = (function (_super) {
    __extends(AeviActionEditor, _super);
    function AeviActionEditor(aeviGrid, cell, entityActionDefinition) {
        _super.call(this, aeviGrid, cell);
        this.editorId = 'aeviActionEditor';
        this.actionParameters = entityActionDefinition;
    }
    AeviActionEditor.prototype.setReady = function () {
        this.render();
    };
    AeviActionEditor.prototype.render = function () {
        this.defaultValue = this.getDataVal();
        var HTML = null;
        if (this.actionParameters.ResultType === MyRetail.IAeviEntityActionResultType.Image) {
            HTML = '<div class="aeviCellBorder"><a class="aeviActionLink" id="aeviActionEditor">' + this.defaultValue + '</a></div>';
        }
        if (this.actionParameters.ResultType === MyRetail.IAeviEntityActionResultType.Form) {
            HTML = '<div class="aeviCellBorder noImage"><a class="aeviActionLink" id="aeviActionEditor"></a></div>';
        }
        this.cell.innerHTML = HTML;
        this.setState('render');
        this.listener();
    };
    AeviActionEditor.prototype.remove = function () {
        this.cell.innerHTML = '<div class="aeviCellBorder">' + this.defaultValue + '</div>';
        this.setState('initial');
    };
    AeviActionEditor.prototype.listener = function () {
        var _this = this;
        $(document).off('click', '#aeviActionEditor').on('click', '#aeviActionEditor', function (event) {
            event.preventDefault();
            _this.AeviGrid.AeviDataService.executeAction(_this.cellIndexes.rowIndex, _this.actionParameters);
        });
    };
    AeviActionEditor.prototype.trigger = function () {
        $('#aeviActionEditor').trigger('click');
    };
    return AeviActionEditor;
}(AeviEditor));
var AeviImageEditor = (function (_super) {
    __extends(AeviImageEditor, _super);
    function AeviImageEditor(aeviGrid, cell) {
        _super.call(this, aeviGrid, cell);
        this.editorId = '.aeviCellImage';
        this.isDataValueChanged = false;
        this.setState('initial');
    }
    AeviImageEditor.prototype.setReady = function () {
        this.render();
    };
    AeviImageEditor.prototype.render = function () {
        this.renderHiddenInput();
        this.editorEl = this.$cell.find('div > input');
        this.cell.classList.add('hasEditor');
        this.listener();
        this.setState('render');
    };
    AeviImageEditor.prototype.renderFormEditor = function (column, fakeIndexes) {
        var image = column.Value;
        var deleteBtnClass = '';
        var editBtnClass = '';
        var uploadBtnClass = ' hidden ';
        var imgClass = '';
        if (_.isNull(image.Value)) {
            deleteBtnClass = ' hidden ';
            editBtnClass = ' hidden ';
            uploadBtnClass = '';
            imgClass = ' hidden ';
        }
        this.cellIndexes = fakeIndexes;
        this.columnName = column.ColumnName;
        this.defaultValue = _.clone(image);
        if (this.AeviGrid.mode.clipboard === 1) {
            return '<input class="form-control" disabled type="text" readonly value="' + AeviImage.getLocalizedText('image_blocked') + '">';
        }
        var HTML = '';
        var buttonTemplate = AeviImageEditor.getButtonTemplate();
        var imgTemplate = AeviImageEditor.getImgTemplate();
        HTML += imgTemplate({ imgClass: imgClass, maxHeight: column.MaxHeight - 40, imgFormat: AeviImage.getImageFormat(), imgValue: image.Value });
        if (!column.IsReadOnly) {
            HTML += buttonTemplate({ buttonClass: 'aeviButton--secondary' + uploadBtnClass, action: 'upload', translate: this.AeviGrid.AeviLocalization.translate('image_missing') });
            HTML += buttonTemplate({ buttonClass: 'aeviButton--black' + deleteBtnClass, action: 'delete', translate: this.AeviGrid.AeviLocalization.translate('image_delete') });
            HTML += buttonTemplate({ buttonClass: editBtnClass, action: 'edit', translate: this.AeviGrid.AeviLocalization.translate('image_edit') });
        }
        return HTML;
    };
    AeviImageEditor.getButtonTemplate = function () {
        return _.template('<button class="aeviButton <%= buttonClass %> text-bottom" data-imageAction="<%= action %>"><%= translate %></button>');
    };
    AeviImageEditor.getImgTemplate = function () {
        return _.template('<img class="aeviForm__image <%= imgClass %>" style="max-height: <%= maxHeight %>px;" src="data:image/<%= imgFormat %>;base64,<%= imgValue %>">');
    };
    AeviImageEditor.prototype.renderHiddenInput = function () {
        this.$cell.find('div').append('<input type="text" class="inputHidden">');
    };
    AeviImageEditor.prototype.remove = function () {
        this.cell.classList.remove('hasEditor');
        if (!aeviIsUndefinedOrNull(this.editorEl) && this.editorEl.length)
            this.editorEl.remove();
        this.setState('initial');
    };
    AeviImageEditor.prototype.isValueChanged = function () {
        return this.isDataValueChanged;
    };
    AeviImageEditor.prototype.formListener = function () {
        var _this = this;
        $(document).off('click', '*[data-imageAction]').on('click', '*[data-imageAction]', function (event) {
            event.preventDefault();
            var button = event.currentTarget;
            var action = button.getAttribute('data-imageAction');
            var allowImageChange = true;
            var imageWindow = new AeviImage(_this.AeviGrid, _this, _this.cellIndexes, allowImageChange);
            imageWindow.render();
            switch (action) {
                case 'upload':
                    $(AeviConsts.imageModalFile).trigger('click');
                    break;
                case 'edit':
                    $(AeviConsts.imageModalEdit).trigger('click');
                    break;
                case 'delete':
                    $(AeviConsts.imageModalDelete).trigger('click');
                    break;
                default:
                    break;
            }
        });
        $(document).off(AeviConsts.imageModalChangeEvent).on(AeviConsts.imageModalChangeEvent, function (event, settings) {
            if (!_this.AeviGrid.AeviDOM.isFormVisible()) {
                return;
            }
            switch (settings.type) {
                case 'upload':
                    _this.binder.publish(settings.data);
                    $(AeviConsts.formImage).attr('src', 'data:image/' + AeviImage.getImageFormat() + ';base64,' + settings.data.Value);
                    break;
                case 'delete':
                    $(AeviConsts.formImage + ',' + AeviConsts.formImageDeleteButton + ',' + AeviConsts.formImageEditButton).addClass('hidden');
                    $(AeviConsts.formImageUploadButton).removeClass('hidden');
                    break;
                case 'close':
                    $(AeviConsts.formImage).attr('src', 'data:image/' + AeviImage.getImageFormat() + ';base64,' + _this.defaultValue.Value);
                    break;
                case 'saveAndClose':
                    _this.binder.publish(settings.data);
                    $(AeviConsts.formImage).attr('src', 'data:image/' + AeviImage.getImageFormat() + ';base64,' + settings.data.Value);
                    $(AeviConsts.formImage + ',' + AeviConsts.formImageDeleteButton + ',' + AeviConsts.formImageEditButton).removeClass('hidden');
                    $(AeviConsts.formImageUploadButton).addClass('hidden');
                    break;
                default:
                    break;
            }
        });
    };
    AeviImageEditor.prototype.listener = function () {
        var _this = this;
        $(document).off('click', this.editorId).on('click', this.editorId, function (event) {
            _this.imageEditorLink = event.currentTarget;
            _this.renderImageWindow();
        });
    };
    AeviImageEditor.prototype.renderImageWindow = function () {
        if (aeviIsUndefinedOrNull(this.imageEditorLink)) {
            return;
        }
        var className = this.imageEditorLink.getAttribute('class');
        if (className.indexOf('imageBlocked') !== -1) {
            return;
        }
        if (this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(this.cellIndexes.rowIndex, 'newrow') && this.AeviGrid.AeviDataRepository.isMaxRowCountExceeded()) {
            event.preventDefault();
            this.AeviGrid.showMaxRowCountMessage();
            return false;
        }
        var isReport = this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport();
        var isLocked = this.AeviGrid.AeviGridLocker.isLocked;
        var isReadOnly = (isReport || isLocked);
        if (isReadOnly) {
            if (className.indexOf('imageMissing') !== -1) {
                return;
            }
        }
        var allowImageChange = !isReadOnly;
        var imageWindow = new AeviImage(this.AeviGrid, this, this.cellIndexes, allowImageChange);
        imageWindow.render();
    };
    return AeviImageEditor;
}(AeviEditor));
var AeviDateEditor = (function (_super) {
    __extends(AeviDateEditor, _super);
    function AeviDateEditor(aeviGrid, cell, displayType) {
        _super.call(this, aeviGrid, cell);
        this.editorId = 'aeviDateInput';
        this.displayType = displayType;
        this.AeviDataRepository = this.AeviGrid.AeviDataRepository;
        $.datepicker.setDefaults($.datepicker.regional[this.AeviGrid.AeviLocalization.getCulture()]);
    }
    AeviDateEditor.prototype.render = function () {
        this.removeReadyListeners();
        this.initDateFormat();
        this.defaultValue = this.getDataVal();
        this.cell.innerHTML = '<div class="aeviCellBorder">' + '<input type="text" id="' + this.editorId + '" class="aeviCellDate" value="' + this.AeviDate.getString() + '">' + '<i class="datepicker"></i>' + '</div>';
        this.cell.classList.add('hasEditor');
        this.cellClass = this.cell.getAttribute('class');
        this.editorEl = this.getEditorEl();
        $(this.editorEl).select().focus();
        this.setState('render');
        this.listener();
    };
    AeviDateEditor.prototype.renderFormEditor = function (column) {
        var _this = this;
        var dataVal = column.Value;
        this.AeviDate = new AeviDate(this.AeviGrid.AeviLocalization.getCulture(), this.displayType, dataVal);
        $(document).off('click touchstart', 'input[data-fakeColumnName="' + column.ColumnName + '"] + .datepicker').on('click touchstart', 'input[data-fakeColumnName="' + column.ColumnName + '"] + .datepicker', function (event) {
            _this.editorEl = document.querySelector('input[data-fakeColumnName="' + column.ColumnName + '"]');
            _this.hiddenEditorEl = document.querySelector('input[data-columnName="' + column.ColumnName + '"]');
            _this.showFormCalendar();
        });
        this.columnName = column.ColumnName;
        var readOnly = (column.IsReadOnly) ? 'readonly' : '';
        var HTML = '<input type="hidden" data-columnName="' + column.ColumnName + '" name="' + column.ColumnName + '"><input class="form-control aeviCellDate" ' + readOnly + ' id="aeviForm-' + this.columnName + '" data-fakeColumnName="' + column.ColumnName + '" type="text" value="' + this.AeviDate.getString() + '">';
        if (column.IsReadOnly) {
            return HTML;
        }
        return HTML + '<i class="datepicker"></i>';
    };
    AeviDateEditor.prototype.formListener = function () { };
    AeviDateEditor.prototype.showFormCalendar = function () {
        var _this = this;
        var __this = this;
        var old_goToToday = $.datepicker._gotoToday;
        $.datepicker._gotoToday = function (id) {
            var currentDatepicker = this;
            old_goToToday.call(currentDatepicker, id);
            currentDatepicker._selectDate(id);
            setTimeout(function () {
                $(__this.editorEl).datetimepicker('hide');
            }, 0);
            __this.AeviDate.parseString(this.value);
            __this.binder.publish(__this.AeviDate.getIsoString());
        };
        $(this.editorEl).datetimepicker({
            showTimepicker: false,
            onSelect: function (val) {
                _this.AeviDate.parseString(val);
                _this.binder.publish(_this.AeviDate.getIsoString());
            },
            currentText: $.datepicker.regional[this.AeviGrid.AeviLocalization.getCulture()].currentText,
            closeText: $.datepicker.regional[this.AeviGrid.AeviLocalization.getCulture()].closeText
        });
        $(this.editorEl).datetimepicker('show');
    };
    AeviDateEditor.prototype.publishChanges = function () {
        this.AeviDate.parseString(this.getEditorVal());
        this.binder.publish(this.AeviDate.getIsoString());
    };
    AeviDateEditor.prototype.listener = function () {
        var _this = this;
        this.initBinder();
        $(document).on('keyup', '#' + this.editorId, function (event) {
            _this.publishChanges();
        });
        $(document).on('click touchstart', this.$cell.selector + ' .datepicker', function (event) {
            _this.showCalendar();
        });
        if (this.AeviGrid.AeviDOM.length('#' + this.editorId) && window.navigator.msPointerEnabled) {
            this.editorEl.addEventListener("MSPointerDown", function () {
                _this.showCalendar();
            }, false);
        }
    };
    AeviDateEditor.prototype.initDateFormat = function () {
        this.AeviDate = new AeviDate(this.AeviGrid.AeviLocalization.getCulture(), this.displayType, this.getDataVal());
    };
    AeviDateEditor.prototype.remove = function () {
        _super.prototype.remove.call(this);
        $(document).off('click touchstart', this.$cell.selector + ' .datepicker');
        $(this.editorEl).datepicker('destroy');
    };
    AeviDateEditor.prototype.showCalendar = function () {
        var _this = this;
        var __this = this;
        var old_goToToday = $.datepicker._gotoToday;
        $.datepicker._gotoToday = function (id) {
            var currentDatepicker = this;
            old_goToToday.call(currentDatepicker, id);
            currentDatepicker._selectDate(id);
            setTimeout(function () {
                $(__this.editorEl).datetimepicker('hide');
            }, 0);
            __this.publishChanges();
        };
        $(this.editorEl).datetimepicker({
            showTimepicker: false,
            onSelect: function () {
                _this.publishChanges();
            },
            currentText: $.datepicker.regional[this.AeviGrid.AeviLocalization.getCulture()].currentText,
            closeText: $.datepicker.regional[this.AeviGrid.AeviLocalization.getCulture()].closeText
        });
        $(this.editorEl).datetimepicker('show');
    };
    return AeviDateEditor;
}(AeviEditor));
var AeviEnumEditor = (function (_super) {
    __extends(AeviEnumEditor, _super);
    function AeviEnumEditor(aeviGrid, cell, column) {
        _super.call(this, aeviGrid, cell);
        this.editorId = 'aeviEnumEditor';
        this.column = column;
    }
    AeviEnumEditor.prototype.setReady = function () {
        AeviDOM.disableScroll();
        this.render();
        this.showBubble();
        setTimeout(function () { AeviDOM.enableScroll(); }, 50);
    };
    AeviEnumEditor.prototype.render = function () {
        this.defaultValue = this.getDataVal();
        this.cell.innerHTML = this.getRenderedElement();
        this.editorEl = document.getElementById(this.editorId);
        this.cell.classList.add('hasEditor');
        this.cell.classList.add('aeviCellSelect');
        this.cellClass = this.cell.getAttribute('class');
        this.setState('render');
        this.checkEditorRendered();
        this.listener();
    };
    AeviEnumEditor.prototype.renderFormEditor = function (column, fakeIndexes) {
        this.columnName = column.ColumnName;
        this.cellIndexes = fakeIndexes;
        return this.getRenderedElement({ renderForm: true, className: 'form-control', columnName: column.ColumnName, id: ('aeviForm-' + column.ColumnName), IsReadOnly: column.IsReadOnly });
    };
    AeviEnumEditor.prototype.getRenderedElement = function (settings) {
        if (this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(this.cellIndexes.rowId, 'newrow') && this.AeviGrid.AeviDataService.AeviDataRepository.isMaxRowCountExceeded())
            return '<div class="aeviCellBorder"></div>';
        if (aeviIsUndefinedOrNull(settings)) {
            var settings = {
                className: '',
                columnName: '',
                renderForm: false,
                id: 'aeviEnumEditor'
            };
        }
        var disabled = '';
        if (!aeviIsUndefinedOrNull(settings.IsReadOnly)) {
            if (settings.IsReadOnly) {
                disabled = 'disabled';
            }
        }
        var column = this.column;
        var enumValues = column.EnumValues;
        var className = 'aeviEnumEditor';
        var HTML = '';
        if (settings.renderForm === false) {
            HTML += '<div class="aeviCellBorder">';
        }
        this.isAnyOptionSelected = false;
        HTML += '<select id="' + settings.id + '" data-columnName="' + settings.columnName + '" class="' + settings.className + ' ' + className + '">';
        for (var i = 0; i < enumValues.length; i++) {
            var enumValue = enumValues[i];
            HTML += '<option ' + disabled + ' value="' + enumValue.DataValue + '"';
            var cellDataValue = this.getDataVal();
            if (cellDataValue === 'null')
                cellDataValue = null;
            if (cellDataValue === 'true')
                cellDataValue = true;
            if (cellDataValue === 'false')
                cellDataValue = false;
            if (enumValue.DataValue == cellDataValue) {
                HTML += ' selected';
                this.isAnyOptionSelected = true;
            }
            HTML += '>' + enumValue.DisplayValue + '</option>';
        }
        HTML += '</select>';
        if (settings.renderForm === false) {
            HTML += '</div>';
        }
        return HTML;
    };
    ;
    AeviEnumEditor.prototype.remove = function () {
        this.removeCellClasses();
        this.setCellVal(this.getEditorVal());
        if (this.isValueChanged(this.getDataVal())) {
            this.isDataValueChanged = true;
        }
        this.setState('initial');
    };
    AeviEnumEditor.prototype.formListener = function () {
        var _this = this;
        $(document).off('change', '#aeviForm-' + this.columnName).on('change', '#aeviForm-' + this.columnName, function () {
            var editor = document.getElementById('aeviForm-' + _this.columnName);
            _this.binder.publish(editor.value);
        });
        $('#aeviForm-' + this.columnName).change();
    };
    AeviEnumEditor.prototype.listener = function () {
        var _this = this;
        this.initBinder();
        if (this.isAnyOptionSelected === false) {
            this.isDataValueChanged = true;
            this.AeviGrid.AeviDataService.AeviApiService.setCellChanged(true);
            this.binder.publish(this.getEditorDataVal());
        }
        $(document).off('mousedown', '#' + this.editorId).on('mousedown', '#' + this.editorId, function (event) {
            if (_this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(_this.cellIndexes.rowId, 'newrow') && _this.AeviGrid.AeviDataService.AeviDataRepository.isMaxRowCountExceeded()) {
                event.preventDefault();
                _this.AeviGrid.showMaxRowCountMessage();
                return false;
            }
        });
        $(document).off('change', '#' + this.editorId).on('change', '#' + this.editorId, function () {
            _this.AeviGrid.AeviDataService.AeviApiService.setCellChanged(true);
            _this.binder.publish(_this.getEditorDataVal());
        });
        $(document).off('keydown', '#' + this.editorId).on('keydown', '#' + this.editorId, function (event) {
            var codeKey = _this.AeviGrid.AeviClientSide.getKeyName(event);
            if (codeKey === 'left arrow') {
                event.preventDefault();
                _this.AeviGrid.AeviGridHandler.AeviBodyCellHandler.shiftTab(_this.$cell);
            }
            if (codeKey === 'right arrow') {
                event.preventDefault();
                _this.AeviGrid.AeviGridHandler.AeviBodyCellHandler.tab(_this.$cell);
            }
        });
        document.addEventListener("keydown", function (event) {
            if (_this.$cell && _this.$cell.hasClass('hasEditor')) {
                var key = _this.AeviGrid.AeviClientSide.getKeyName(event);
                if (key === 'backspace') {
                    event.preventDefault();
                }
                else if (key === '(space)' && _this.AeviGrid.AeviClientSide.isIe()) {
                    _this.$cell.toggleClass('typing');
                }
                _this.editorEl.focus();
            }
        });
        if (this.AeviGrid.AeviClientSide.isSafari()) {
            $(document).off('click', '#' + this.editorId).on('click', '#' + this.editorId, function () {
                setTimeout(function () { return _this.AeviGrid.select(_this.cell); }, 0);
            });
        }
    };
    AeviEnumEditor.prototype.getEditorDataVal = function () {
        var val = (this.isEditorRendered) ? this.editorEl.options[this.editorEl.selectedIndex].value : this.cell.innerText;
        return (val === '') ? null : val;
    };
    AeviEnumEditor.prototype.getEditorVal = function () {
        var val = (this.isEditorRendered) ? this.editorEl.options[this.editorEl.selectedIndex].text : this.cell.innerText;
        return (val === '') ? '' : val;
    };
    AeviEnumEditor.prototype.checkEditorRendered = function () {
        var optionsLength = 0;
        this.isEditorRendered = (!aeviIsUndefinedOrNull(this.editorEl));
        if (this.isEditorRendered)
            optionsLength = this.editorEl.options.length;
        if (this.isEditorRendered) {
            if (optionsLength < 1)
                this.isEditorRendered = false;
        }
    };
    return AeviEnumEditor;
}(AeviEditor));
var AeviEditorFactory = (function () {
    function AeviEditorFactory(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.AeviDataRepository = this.AeviGrid.AeviDataRepository;
    }
    AeviEditorFactory.prototype.createEditor = function ($cell) {
        var indexes = this.AeviGrid.AeviDOM.getCellIndexes($cell);
        var column = this.AeviDataRepository.AeviTableDescription.getColumnHeader(indexes.cellIndex);
        if (aeviIsUndefinedOrNull(column)) {
            this.AeviGrid.print('AeviEditor.create(), variable "column" is undefined or null.');
            this.focus($cell);
            return;
        }
        var referenceSettingsColumn = this.AeviDataRepository.getReferenceSettings(indexes.rowIndex, indexes.cellIndex);
        if (!_.isNull(referenceSettingsColumn)) {
            column = referenceSettingsColumn;
        }
        if (!aeviIsUndefinedOrNull(column.EntityActionDefinition)) {
            this.editor = new AeviActionEditor(this.AeviGrid, $cell, column.EntityActionDefinition);
            return;
        }
        var displayType = this.AeviGrid.AeviConsts.dataTypes[column.DisplayType];
        if (displayType === 'Image') {
            this.editor = new AeviImageEditor(this.AeviGrid, $cell);
            return;
        }
        if (column.ReadOnly || this.AeviDataRepository.AeviTableDescription.isReport() || this.AeviGrid.AeviGridLocker.isLocked) {
            this.editor = null;
            this.focus($cell);
            return;
        }
        switch (displayType) {
            case 'Image':
                this.editor = new AeviImageEditor(this.AeviGrid, $cell);
                break;
            case 'Text':
                this.editor = new AeviTextEditor(this.AeviGrid, $cell);
                break;
            case 'Number':
            case 'IntegerNumber':
                this.editor = new AeviNumberEditor(this.AeviGrid, $cell, displayType);
                break;
            case 'Currency':
                this.editor = new AeviCurrencyEditor(this.AeviGrid, $cell);
                break;
            case 'DateTime':
            case 'ShortDate':
            case 'ShortTime':
                this.editor = new AeviDateEditor(this.AeviGrid, $cell, displayType);
                break;
            case 'Enum':
                this.editor = new AeviEnumEditor(this.AeviGrid, $cell, column);
                break;
            case 'Boolean':
            case 'Hyperlink':
            case 'RegularExpression':
                this.editor = null;
                this.AeviGrid.print('AeviEditorFactory.createEditor(), "' + displayType + '" editor is not supported.');
                break;
            default:
                this.editor = null;
                break;
        }
    };
    AeviEditorFactory.prototype.getEditor = function () {
        return this.editor;
    };
    AeviEditorFactory.prototype.isEditorExists = function () {
        return !aeviIsUndefinedOrNull(this.editor);
    };
    AeviEditorFactory.prototype.focus = function ($cell) {
        $cell.focus();
    };
    return AeviEditorFactory;
}());
var AeviValidator = (function () {
    function AeviValidator(aeviLocalization, column, value) {
        this.AeviLocalization = aeviLocalization;
        this.column = column;
        this.value = value;
    }
    AeviValidator.prototype.getValue = function () {
        return this.value;
    };
    AeviValidator.prototype.validate = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i - 0] = arguments[_i];
        }
    };
    AeviValidator.fakeValidate = function (invalid) {
        if (!aeviIsUndefinedOrNull(invalid)) {
            if (invalid) {
                return {
                    valid: false,
                    hard: true,
                    message: 'Error'
                };
            }
            else {
                return {
                    valid: true,
                    hard: true,
                    message: ''
                };
            }
        }
        else {
            return {
                valid: true,
                hard: true,
                message: ''
            };
        }
    };
    return AeviValidator;
}());
var AeviEANValidator = (function (_super) {
    __extends(AeviEANValidator, _super);
    function AeviEANValidator(aeviLocalization, column, value) {
        _super.call(this, aeviLocalization, column, value);
    }
    AeviEANValidator.prototype.validate = function (value) {
        var result = this.getEANValidateResult(value);
        var msg = null;
        if (!result)
            msg = this.AeviLocalization.translate('error_validate_ean');
        return {
            hard: false,
            valid: result,
            message: msg
        };
    };
    AeviEANValidator.prototype.getEANValidateResult = function (value) {
        if (value && value !== '') {
            var numbersOnly = true;
            var numberChars = "0123456789";
            for (var i = 0; i < value.length; i++) {
                if (numberChars.indexOf(value.charAt(i)) == -1)
                    numbersOnly = false;
            }
            if (numbersOnly && (value.length == 8 || value.length == 13)) {
                if (value.length == 8)
                    value = "00000" + value;
                var originalCheck = value.substring(value.length - 1);
                var eanCode = value.substring(0, value.length - 1);
                var even = Number(eanCode.charAt(1)) +
                    Number(eanCode.charAt(3)) +
                    Number(eanCode.charAt(5)) +
                    Number(eanCode.charAt(7)) +
                    Number(eanCode.charAt(9)) +
                    Number(eanCode.charAt(11));
                even *= 3;
                var odd = Number(eanCode.charAt(0)) +
                    Number(eanCode.charAt(2)) +
                    Number(eanCode.charAt(4)) +
                    Number(eanCode.charAt(6)) +
                    Number(eanCode.charAt(8)) +
                    Number(eanCode.charAt(10));
                var total = even + odd;
                var checksum = total % 10;
                if (checksum !== 0)
                    checksum = 10 - checksum;
                var checksumString = checksum.toString();
                return (checksumString === originalCheck);
            }
            else {
                return false;
            }
        }
        return false;
    };
    return AeviEANValidator;
}(AeviValidator));
var AeviServerGeneratedValidator = (function (_super) {
    __extends(AeviServerGeneratedValidator, _super);
    function AeviServerGeneratedValidator(aeviLocalization, column, value) {
        _super.call(this, aeviLocalization, column, value);
    }
    AeviServerGeneratedValidator.prototype.validate = function () {
        var valid = true;
        var message = null;
        if (_.isNull(this.value) || this.value === 'null' || this.value.length < 1) {
            valid = false;
            message = this.AeviLocalization.translate('server_generated_message');
        }
        return {
            hard: false,
            valid: valid,
            message: message
        };
    };
    return AeviServerGeneratedValidator;
}(AeviValidator));
var AeviMaxLengthValidator = (function (_super) {
    __extends(AeviMaxLengthValidator, _super);
    function AeviMaxLengthValidator(aeviLocalization, column, value, maxLength) {
        _super.call(this, aeviLocalization, column, value);
        this.maxLength = maxLength;
    }
    AeviMaxLengthValidator.prototype.validate = function (value) {
        var valid, message;
        if (_.isNull(this.maxLength) || this.maxLength === 0) {
            valid = true;
            message = null;
        }
        else {
            if (_.isNull(value)) {
                valid = true;
                message = null;
            }
            else {
                value = value.toString();
                valid = (value.length <= this.maxLength) ? true : false;
                message = (valid) ? null : this.AeviLocalization.translate('error_validate_maxlength') + (this.maxLength + 1);
            }
        }
        return {
            hard: true,
            valid: valid,
            message: message
        };
    };
    return AeviMaxLengthValidator;
}(AeviValidator));
var AeviNumberValidator = (function (_super) {
    __extends(AeviNumberValidator, _super);
    function AeviNumberValidator(aeviLocalization, column, value) {
        _super.call(this, aeviLocalization, column, value);
    }
    AeviNumberValidator.prototype.validate = function () {
        var validationInfo = {
            hard: true,
            valid: true,
            message: null
        };
        var cellValue = this.getValue();
        if (_.isEmpty(cellValue))
            validationInfo.valid = false;
        if (cellValue === 'null')
            cellValue = null;
        var numberRegex = /^[+-]?[0-9]+([\.,\,][0-9]+)?$/;
        if (_.isString(cellValue)) {
            if (cellValue.length > 0) {
                cellValue = parseFloat(cellValue);
                validationInfo.valid = numberRegex.test(cellValue);
            }
        }
        else {
            if (!_.isNull(cellValue)) {
                cellValue = parseFloat(cellValue);
                validationInfo.valid = numberRegex.test(cellValue);
            }
        }
        if (validationInfo.valid === false)
            validationInfo.message = this.AeviLocalization.translate('error_validate_number');
        if (_.isNaN(cellValue))
            return validationInfo;
        if (this.column.Required && (_.isEmpty(cellValue) || _.isNull(cellValue) || cellValue === 'null' || cellValue === '')) {
            validationInfo.valid = false;
            validationInfo.message = this.AeviLocalization.translate('value_required');
        }
        if (this.column.Required === false && ((_.isEmpty(cellValue) || _.isNull(cellValue)) || cellValue === 'null')) {
            validationInfo.valid = true;
            validationInfo.message = '';
        }
        if (this.column.SemanticType === 1 && validationInfo.valid) {
            var EANValidator = new AeviEANValidator(this.AeviLocalization, null, null);
            validationInfo = EANValidator.validate(cellValue);
        }
        if (this.column.MaxLength > 0 && validationInfo.valid) {
            var maxLengthValidator = new AeviMaxLengthValidator(this.AeviLocalization, null, null, this.column.MaxLength);
            validationInfo = maxLengthValidator.validate(cellValue);
        }
        if (this.column.ServerGenerated && validationInfo.valid) {
            var serverGeneratedValidator = new AeviServerGeneratedValidator(this.AeviLocalization, null, cellValue);
            validationInfo = serverGeneratedValidator.validate();
        }
        return validationInfo;
    };
    ;
    return AeviNumberValidator;
}(AeviValidator));
var AeviIntegerNumberValidator = (function (_super) {
    __extends(AeviIntegerNumberValidator, _super);
    function AeviIntegerNumberValidator(aeviLocalization, column, value) {
        _super.call(this, aeviLocalization, column, value);
    }
    AeviIntegerNumberValidator.prototype.validate = function () {
        var validationInfo = {
            hard: true,
            valid: true,
            message: null
        };
        var cellValue = this.getValue();
        var numberRegex = /^\d+$/;
        if (_.isString(cellValue)) {
            if (cellValue.length > 0) {
                validationInfo.valid = numberRegex.test(cellValue);
            }
        }
        else {
            validationInfo.valid = numberRegex.test(cellValue);
        }
        if (validationInfo.valid === false) {
            validationInfo.message = this.AeviLocalization.translate('validate_integer_number_error');
        }
        if (_.isNaN(cellValue)) {
            return validationInfo;
        }
        if (this.column.Required && (_.isNull(cellValue) || cellValue === 'null' || cellValue === '')) {
            validationInfo.valid = false;
            validationInfo.message = this.AeviLocalization.translate('value_required');
        }
        if (this.column.Required === false && ((_.isEmpty(cellValue) || _.isNull(cellValue)) || cellValue === 'null')) {
            validationInfo.valid = true;
            validationInfo.message = '';
        }
        if (this.column.SemanticType === 1 && validationInfo.valid) {
            var EANValidator = new AeviEANValidator(this.AeviLocalization, null, null);
            validationInfo = EANValidator.validate(cellValue);
        }
        if (this.column.MaxLength > 0 && validationInfo.valid) {
            var maxLengthValidator = new AeviMaxLengthValidator(this.AeviLocalization, null, null, this.column.MaxLength);
            validationInfo = maxLengthValidator.validate(cellValue);
        }
        if (this.column.ServerGenerated && validationInfo.valid) {
            var serverGeneratedValidator = new AeviServerGeneratedValidator(this.AeviLocalization, null, cellValue);
            validationInfo = serverGeneratedValidator.validate();
        }
        return validationInfo;
    };
    ;
    return AeviIntegerNumberValidator;
}(AeviValidator));
var AeviRowNumberValidator = (function (_super) {
    __extends(AeviRowNumberValidator, _super);
    function AeviRowNumberValidator(aeviLocalization, column, value) {
        _super.call(this, aeviLocalization, column, value);
    }
    AeviRowNumberValidator.prototype.validate = function () {
        return {
            hard: true,
            valid: true,
            message: null
        };
    };
    return AeviRowNumberValidator;
}(AeviValidator));
var AeviTextValidator = (function (_super) {
    __extends(AeviTextValidator, _super);
    function AeviTextValidator(aeviLocalization, column, value) {
        _super.call(this, aeviLocalization, column, value);
    }
    AeviTextValidator.prototype.validate = function () {
        var validationInfo = {
            hard: true,
            valid: true,
            message: null
        };
        var cellValue = this.getValue();
        if (this.column.Required && (_.isEmpty(cellValue) || cellValue === ' ' || _.isNull(cellValue) || cellValue === 'null')) {
            validationInfo.message = this.AeviLocalization.translate('value_required');
            validationInfo.valid = false;
        }
        if (this.column.SemanticType === 1 && validationInfo.valid) {
            var EANValidator = new AeviEANValidator(this.AeviLocalization, null, null);
            validationInfo = EANValidator.validate(cellValue);
        }
        if (this.column.MaxLength > 0 && validationInfo.valid) {
            var maxLengthValidator = new AeviMaxLengthValidator(this.AeviLocalization, null, null, this.column.MaxLength);
            validationInfo = maxLengthValidator.validate(cellValue);
        }
        if (this.column.ServerGenerated && validationInfo.valid) {
            var serverGeneratedValidator = new AeviServerGeneratedValidator(this.AeviLocalization, null, cellValue);
            validationInfo = serverGeneratedValidator.validate();
        }
        return validationInfo;
    };
    return AeviTextValidator;
}(AeviValidator));
var AeviCurrencyValidator = (function (_super) {
    __extends(AeviCurrencyValidator, _super);
    function AeviCurrencyValidator(aeviLocalization, column, value) {
        _super.call(this, aeviLocalization, column, value);
    }
    AeviCurrencyValidator.prototype.validate = function () {
        var validationInfo = {
            hard: true,
            valid: true,
            message: null
        };
        var cellValue = this.value;
        var numberRegex = /^[+-]?[0-9]+([.][0-9]+)?$/;
        if (!_.isNull(cellValue) && cellValue.length > 0 && cellValue !== 'null') {
            if (cellValue.indexOf(',') !== -1)
                cellValue = cellValue.replace(',', '.');
            cellValue = parseFloat(cellValue);
            validationInfo.valid = numberRegex.test(cellValue);
        }
        if (validationInfo.valid === false) {
            validationInfo.message = this.AeviLocalization.translate('error_validate_number');
        }
        if (_.isNaN(cellValue)) {
            return validationInfo;
        }
        if (!_.isNull(cellValue))
            cellValue = cellValue.toString();
        if (cellValue === 'null')
            cellValue = null;
        if (this.column.Required && (_.isEmpty(cellValue) || _.isNull(cellValue))) {
            validationInfo.valid = false;
            validationInfo.message = this.AeviLocalization.translate('value_required');
        }
        if (this.column.MaxLength > 0 && validationInfo.valid) {
            var maxLengthValidator = new AeviMaxLengthValidator(this.AeviLocalization, null, null, this.column.MaxLength);
            validationInfo = maxLengthValidator.validate(cellValue);
        }
        if (this.column.ServerGenerated && validationInfo.valid) {
            var serverGeneratedValidator = new AeviServerGeneratedValidator(this.AeviLocalization, null, cellValue);
            validationInfo = serverGeneratedValidator.validate();
        }
        return validationInfo;
    };
    return AeviCurrencyValidator;
}(AeviValidator));
var AeviDateTimeValidator = (function (_super) {
    __extends(AeviDateTimeValidator, _super);
    function AeviDateTimeValidator(aeviLocalization, column, value) {
        _super.call(this, aeviLocalization, column, value);
    }
    AeviDateTimeValidator.prototype.validate = function () {
        var validationInfo = {
            hard: true,
            valid: true,
            message: null
        };
        var cellValue = null;
        cellValue = this.getValue();
        if (cellValue && cellValue.length > 0 && cellValue !== 'null') {
            var date = new AeviDate(this.AeviLocalization.getCulture(), this.column.DisplayType, cellValue);
            if (date.null || date.month > 12 || date.day > 31) {
                validationInfo.message = this.AeviLocalization.translate('datetime_error');
                validationInfo.valid = false;
            }
        }
        else {
            if (this.column.Required) {
                validationInfo.message = this.AeviLocalization.translate('value_required');
                validationInfo.valid = false;
            }
        }
        return validationInfo;
    };
    return AeviDateTimeValidator;
}(AeviValidator));
var AeviEnumValidator = (function (_super) {
    __extends(AeviEnumValidator, _super);
    function AeviEnumValidator(aeviLocalization, column, value) {
        _super.call(this, aeviLocalization, column, value);
    }
    AeviEnumValidator.prototype.validate = function () {
        var validationInfo = {
            hard: true,
            valid: true,
            message: null
        };
        var cellValue = this.getValue();
        if (this.column.Required) {
            if ((cellValue === '' || _.isNull(cellValue) || cellValue === 'null')) {
                validationInfo.message = this.AeviLocalization.translate('value_required');
                validationInfo.valid = false;
            }
        }
        return validationInfo;
    };
    return AeviEnumValidator;
}(AeviValidator));
var AeviImageValidator = (function (_super) {
    __extends(AeviImageValidator, _super);
    function AeviImageValidator(aeviLocalization, column, value) {
        _super.call(this, aeviLocalization, column, value);
    }
    AeviImageValidator.prototype.validate = function () {
        var validationInfo = {
            hard: true,
            valid: true,
            message: null
        };
        if (this.column.Required) {
            if (!this.value.HasValue) {
                validationInfo.message = this.AeviLocalization.translate('value_required');
                validationInfo.valid = false;
            }
        }
        return validationInfo;
    };
    return AeviImageValidator;
}(AeviValidator));
var AeviValidatorFactory = (function () {
    function AeviValidatorFactory(aeviConsts, aeviLocalization, aeviTableDescription) {
        this.AeviConsts = aeviConsts;
        this.AeviLocalization = aeviLocalization;
        this.AeviTableDescription = aeviTableDescription;
    }
    AeviValidatorFactory.prototype.createValidator = function (value, cellIndex) {
        var column = this.AeviTableDescription.getColumnHeader(cellIndex);
        var columnType = this.AeviTableDescription.getColumnDisplayType(column);
        var displayType = this.AeviTableDescription.getColumnStringDisplayType(columnType);
        switch (displayType) {
            case 'Text':
            case 'SelectedCellsArray':
            case 'InvalidCellsArray':
            case 'Status':
                this.validator = new AeviTextValidator(this.AeviLocalization, column, value);
                break;
            case 'Number':
                this.validator = new AeviNumberValidator(this.AeviLocalization, column, value);
                break;
            case 'IntegerNumber':
                this.validator = new AeviIntegerNumberValidator(this.AeviLocalization, column, value);
                break;
            case 'Currency':
                this.validator = new AeviCurrencyValidator(this.AeviLocalization, column, value);
                break;
            case 'Enum':
                this.validator = new AeviEnumValidator(this.AeviLocalization, column, value);
                break;
            case 'DateTime':
            case 'ShortDate':
            case 'ShortTime':
                this.validator = new AeviDateTimeValidator(this.AeviLocalization, column, value);
                break;
            case 'Image':
                this.validator = new AeviImageValidator(this.AeviLocalization, column, value);
                break;
            default:
                this.validator = null;
                break;
        }
    };
    AeviValidatorFactory.prototype.getValidator = function () {
        return this.validator;
    };
    return AeviValidatorFactory;
}());
var AeviBodyCellHandler = (function () {
    function AeviBodyCellHandler(aeviGrid, aeviGridHandler) {
        this.AeviGrid = aeviGrid;
        this.AeviGridHandler = aeviGridHandler;
        this.listener();
    }
    AeviBodyCellHandler.prototype.listener = function () {
        var _this = this;
        $(document).off('touchstart', this.AeviGrid.AeviConsts.cellSelector).on('touchstart', this.AeviGrid.AeviConsts.cellSelector, function (event) {
            _this.touchElement = event.currentTarget;
        });
        $(document).off('touchmove', this.AeviGrid.AeviConsts.cellSelector).on('touchmove', this.AeviGrid.AeviConsts.cellSelector, function (event) {
            _this.touchElement = null;
        });
        $(document).off('touchend', this.AeviGrid.AeviConsts.cellSelector).on('touchend', this.AeviGrid.AeviConsts.cellSelector, function (event) {
            if (_this.touchElement == event.currentTarget)
                _this.AeviGrid.AeviGridHandler.cellChange(event.currentTarget);
            _this.touchElement = null;
        });
        $(document).off('mousedown', this.AeviGrid.AeviConsts.cellSelector).on('mousedown', this.AeviGrid.AeviConsts.cellSelector, function (event) {
            var cell = event.currentTarget;
            var $cell = $(cell);
            if (event.button === 0 || !event.originalEvent) {
                _this.mouseDown = true;
                _this.AeviGrid.AeviGridHandler.cellChange($cell, MyRetail.IAeviChangeType.byClick);
                _this.firstCellIndex = _this.AeviGrid.AeviDOM.getCellIndex(cell);
                _this.firstRowIndex = _this.AeviGrid.AeviDOM.getCellParentIndex(cell);
                _this.mouseDownCell = $cell;
            }
            if ($('.aeviContextMenu').length) {
                $('.aeviContextMenu').hide();
            }
        });
        $(document).on('mouseup', function (event) {
            _this.mouseDown = false;
            _this.removeNoSelection();
        });
        $(document).off('mouseover', this.AeviGrid.AeviConsts.cellSelector).on('mouseover', this.AeviGrid.AeviConsts.cellSelector, function (event) {
            if (_this.mouseDown === true) {
                var cell = event.currentTarget;
                _this.addNoSelection();
                _this.lastCellIndex = _this.AeviGrid.AeviDOM.getCellIndex(cell);
                _this.lastRowIndex = _this.AeviGrid.AeviDOM.getCellParentIndex(cell);
                var fromCellIndex = {
                    rowIndex: _this.firstRowIndex,
                    cellIndex: _this.firstCellIndex
                };
                var toCellIndex = {
                    rowIndex: _this.lastRowIndex,
                    cellIndex: _this.lastCellIndex
                };
                _this.AeviGrid.multiSelect(fromCellIndex, toCellIndex);
            }
        });
        $(document).on(AeviConsts.keyDownEvent, function (event) {
            if (_this.AeviGrid.AeviDOM.isSearchVisible()) {
                return;
            }
            var code = event.keyCode || event.which;
            var selectedCell = _this.AeviGrid.AeviDOM.getSelectedCell();
            var $selectedCell = $(selectedCell);
            var isCellSelected = (!_.isNull(selectedCell));
            if (!_this.AeviGrid.AeviDOM.isModalVisible()) {
                var key = _this.AeviGrid.AeviConsts.keys[code];
                if (key === 'tab') {
                    if (!aeviIsUndefinedOrNull(_this.AeviGrid.AeviSearch) && _this.AeviGrid.AeviSearch.isSearchVisible) {
                        return;
                    }
                    if (_this.AeviGrid.AeviDOM.isFormVisible()) {
                        return;
                    }
                    event.preventDefault();
                    if (event.shiftKey) {
                        if (isCellSelected) {
                            _this.shiftTab($selectedCell);
                        }
                    }
                    else {
                        if (isCellSelected) {
                            _this.tab($selectedCell);
                        }
                    }
                }
                if (key === 'enter') {
                    event.preventDefault();
                    if (isCellSelected) {
                        _this.enter($selectedCell);
                    }
                }
                var isEditorInRenderMode = false;
                var isEditorExists = (!aeviIsUndefinedOrNull(_this.AeviGrid.AeviGridHandler.selectedCellEditor));
                if (isEditorExists)
                    isEditorInRenderMode = (_this.AeviGrid.AeviGridHandler.selectedCellEditor.getState() === 'render');
                if (isCellSelected && !isEditorInRenderMode) {
                    if (!AeviClientSide.isPasteEvent(event, key)) {
                        event.preventDefault();
                    }
                    switch (key) {
                        case 'left arrow':
                            _this.leftArrow($selectedCell);
                            break;
                        case 'right arrow':
                            _this.rightArrow($selectedCell);
                            break;
                        case 'up arrow':
                            _this.upArrow($selectedCell);
                            break;
                        case 'down arrow':
                            _this.downArrow($selectedCell);
                            break;
                        case 'home':
                            if (event.ctrlKey || event.metaKey)
                                _this.firstRowCell();
                            else
                                _this.firstCell($selectedCell);
                            break;
                        case 'end':
                            if (event.ctrlKey || event.metaKey)
                                _this.lastRowCell();
                            else
                                _this.lastCell($selectedCell);
                            break;
                        default:
                            break;
                    }
                }
            }
        });
    };
    AeviBodyCellHandler.prototype.addNoSelection = function () {
        $(this.AeviGrid.AeviConsts.toolbarSelector + ', ' + this.AeviGrid.AeviConsts.statusBarSelector).addClass(this.AeviGrid.AeviConsts.noSelectionClass);
    };
    AeviBodyCellHandler.prototype.removeNoSelection = function () {
        $(this.AeviGrid.AeviConsts.toolbarSelector + ', ' + this.AeviGrid.AeviConsts.statusBarSelector).removeClass(this.AeviGrid.AeviConsts.noSelectionClass);
    };
    AeviBodyCellHandler.prototype.tab = function (cellObject) {
        var _this = this;
        var newCell = this.AeviGrid.AeviDOM.getCellByTab(cellObject, 'tab');
        var cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes(newCell);
        if (this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(cellIndexes.rowIndex, 'hidden'))
            return;
        this.AeviGridHandler.cellChange(newCell, MyRetail.IAeviChangeType.byKey);
        if (cellIndexes.cellIndex === this.AeviGrid.AeviDataRepository.AeviTableDescription.getRowNumberIndex()) {
            setTimeout(function () {
                _this.tab(newCell);
            });
        }
    };
    AeviBodyCellHandler.prototype.shiftTab = function (cellObject) {
        var _this = this;
        var cell = this.AeviGrid.AeviDOM.getCellByTab(cellObject, 'shiftTab');
        var cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes(cell);
        if (cellIndexes.cellIndex === this.AeviGrid.AeviDataRepository.AeviTableDescription.getRowNumberIndex())
            return;
        this.AeviGridHandler.cellChange(cell, MyRetail.IAeviChangeType.byKey);
        if (cellIndexes.cellIndex === this.AeviGrid.AeviDataRepository.AeviTableDescription.getRowNumberIndex()) {
            setTimeout(function () {
                _this.tab(cell);
            });
        }
    };
    AeviBodyCellHandler.prototype.downArrow = function (cellObject) {
        var cell = this.AeviGrid.AeviDOM.getCellByArrow(cellObject, 'down');
        var cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes(cell);
        if (this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(cellIndexes.rowIndex, 'hidden'))
            return;
        this.AeviGridHandler.cellChange(cell, MyRetail.IAeviChangeType.byKey);
    };
    AeviBodyCellHandler.prototype.enter = function (cellObject) {
        this.downArrow(cellObject);
    };
    AeviBodyCellHandler.prototype.leftArrow = function (cellObject) {
        this.shiftTab(cellObject);
    };
    AeviBodyCellHandler.prototype.rightArrow = function (cellObject) {
        this.tab(cellObject);
    };
    AeviBodyCellHandler.prototype.upArrow = function (cell) {
        var newCell = this.AeviGrid.AeviDOM.getCellByArrow(cell, 'up');
        this.AeviGridHandler.cellChange(newCell, MyRetail.IAeviChangeType.byKey);
    };
    AeviBodyCellHandler.prototype.firstCell = function (cell) {
        var rowIndex = this.AeviGrid.AeviDOM.getCellIndexes(cell).rowIndex;
        var cellIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getFirstVisibleColumn();
        this.AeviGrid.AeviPager.moveTo({ cellId: cellIndex, rowId: rowIndex, click: true });
        this.AeviGrid.AeviPager.moveTo({ cellId: cellIndex + 1, rowId: rowIndex, click: true });
    };
    AeviBodyCellHandler.prototype.firstRowCell = function () {
        var cellIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getFirstVisibleColumn();
        this.AeviGrid.AeviPager.moveTo({ cellId: cellIndex, rowId: 0, click: true });
        this.AeviGrid.AeviPager.moveTo({ cellId: cellIndex + 1, rowId: 0, click: true });
    };
    AeviBodyCellHandler.prototype.lastCell = function (cell) {
        var rowIndex = this.AeviGrid.AeviDOM.getCellIndexes(cell).rowIndex;
        var cellIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getLastVisibleColumn();
        var newCell = this.AeviGrid.AeviDOM.getCell(cellIndex, rowIndex);
        this.AeviGridHandler.cellChange(newCell);
    };
    AeviBodyCellHandler.prototype.lastRowCell = function () {
        var cellIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getLastVisibleColumn();
        var repository = this.AeviGrid.AeviDataService.AeviDataRepository;
        var firstHiddenRecordIndex = repository.AeviTableData.getFirstHiddenRecordIndex();
        var rowIndex = firstHiddenRecordIndex - 2;
        if (_.isNull(firstHiddenRecordIndex)) {
            rowIndex = repository.getVisibleAndNewRecordsLength();
            rowIndex = rowIndex - 1;
        }
        if (rowIndex < 0)
            rowIndex = 0;
        this.AeviGrid.AeviPager.moveTo({ cellId: cellIndex, rowId: rowIndex, click: true });
    };
    return AeviBodyCellHandler;
}());
var AeviHeaderColumnHandler = (function () {
    function AeviHeaderColumnHandler(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.ctrlHold = false;
        this.shiftHold = false;
        this.columnIndexes = [];
        this.aeviColumnNumber = '.aeviColumn';
        this.listener();
    }
    AeviHeaderColumnHandler.prototype.listener = function () {
        var _this = this;
        $(document).off('mousedown', this.AeviGrid.AeviConsts.columnSelector).on('mousedown', this.AeviGrid.AeviConsts.columnSelector, function (event) {
            _this.mouseDown = true;
            _this.mouseDownCell = $(event.currentTarget);
            _this.AeviGrid.AeviGridHandler.triggerClick();
            if (_this.mouseDownCell.hasClass('selected')) {
                _this.mouseDownCell.removeClass('selected');
                _this.unSelect();
            }
            else {
                _this.click(_this.mouseDownCell);
                _this.mouseDownCell.addClass('selected');
            }
        });
        $(document).off('mouseup', this.AeviGrid.AeviConsts.columnSelector).on('mouseup', this.AeviGrid.AeviConsts.columnSelector, function () {
            _this.mouseDown = false;
        });
        $(document).off('mouseover', this.AeviGrid.AeviConsts.columnSelector).on('mouseover', this.AeviGrid.AeviConsts.columnSelector, function () {
            if (_this.mouseDown && _this.mouseDownCell.length) {
                var from = _this.mouseDownCell.index();
                var to = $(_this).index();
                var range = [];
                if (!_this.ctrlHold) {
                    if (from >= to) {
                        var helper = from;
                        from = to;
                        to = helper;
                        range = _.range(from, to + 1);
                    }
                    else {
                        range = _.range(from, to + 1);
                    }
                }
                else {
                    range = _this.range;
                }
                _this.AeviGrid.selectColumn(range);
                _this.range = range;
            }
        });
        $(document).on('keydown', function (event) {
            var code = event.keyCode || event.which;
            if (code === 17 || _this.AeviGrid.AeviClientSide.isMac())
                _this.ctrlHold = true;
            _this.shiftHold = (code === 16);
        });
        $(document).on('keyup', function () {
            _this.ctrlHold = false;
            _this.shiftHold = false;
        });
        $(document).on('mousedown touchstart', this.AeviGrid.AeviConsts.columnSelector + ' .aeviSort', function (event) {
            var sortButton = $(event.currentTarget);
            var sortButtons = $(_this.AeviGrid.AeviConsts.sortButtonSelector);
            _this.AeviGrid.AeviGridHandler.triggerClick();
            sortButtons.removeClass('active').addClass('default');
            sortButton.addClass('active').removeClass('default');
            var classList = null;
            var defaultOrder = true;
            if (sortButton.hasClass('sortUp'))
                classList = 'sortUp';
            if (sortButton.hasClass('sortDown'))
                classList = 'sortDown';
            if (sortButton.hasClass('default'))
                classList = 'default';
            sortButtons.removeClass('sortUp sortDown');
            var sortDirection = null;
            switch (classList) {
                case null:
                    sortDirection = 'up';
                    sortButton.addClass('sortUp');
                    defaultOrder = false;
                    break;
                case 'sortUp':
                    sortDirection = 'down';
                    sortButton.removeClass('sortUp').addClass('sortDown');
                    defaultOrder = false;
                    break;
                case 'sortDown':
                    sortDirection = 'default';
                    sortButton.removeClass('sortDown').addClass('default');
                    defaultOrder = true;
                    break;
            }
            var sortColumnIndex = parseInt(sortButton.parent()[0].getAttribute('id').split('-')[1]);
            if (defaultOrder) {
                sortColumnIndex = _this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnIndexByName('DefaultOrder');
            }
            _this.sort(sortDirection, sortColumnIndex);
            return false;
        });
        $(document).on('click', this.AeviGrid.AeviConsts.aeviSelectAllSelector, function (event) {
            var button = $(event.currentTarget);
            if (button.hasClass('isSelected')) {
                button.removeClass('isSelected');
                _this.AeviGrid.unSelectAllRowsAndCells();
                _this.AeviGrid.AeviDataRepository.AeviTableData.removeSelectedPositions();
            }
            else {
                _this.AeviGrid.AeviGridHandler.triggerClick();
                _this.AeviGrid.selectRow(_.range(0, _this.AeviGrid.AeviDataRepository.getVisibleRecordsLength()));
                button.addClass('isSelected');
            }
        });
    };
    AeviHeaderColumnHandler.prototype.sort = function (sortDirection, sortBy) {
        this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableData.sort(sortDirection, sortBy);
        this.AeviGrid.AeviGridEditor.refreshData();
    };
    AeviHeaderColumnHandler.prototype.unSort = function () {
        $(this.AeviGrid.AeviConsts.columnSelector + this.AeviGrid.AeviConsts.aeviSelectAllSelector).removeClass('active sortDown sortUp').addClass('default');
    };
    AeviHeaderColumnHandler.prototype.unSelect = function () {
        this.AeviGrid.selectColumn([], false);
    };
    AeviHeaderColumnHandler.prototype.click = function (columnElement) {
        var _this = this;
        var columnIndex = AeviDOM.getColumnIndex(columnElement);
        if (this.range) {
            $.each(this.range, function (i, el) {
                if ($.inArray(el, _this.columnIndexes) === -1)
                    _this.columnIndexes.push(_this.range[i]);
            });
        }
        if (!this.ctrlHold && this.columnIndexes !== undefined) {
            this.columnIndexes.length = 0;
        }
        if (this.shiftHold) {
            if (this.firstClick !== undefined && this.firstClick != columnIndex) {
                var direction = (this.firstClick > columnIndex) ? 1 : 0;
                var from = 0;
                var to = 0;
                var helper = null;
                if (!direction) {
                    from = this.firstClick;
                    to = columnIndex;
                }
                else {
                    from = columnIndex;
                    to = helper;
                }
                this.columnIndexes = _.range(from, to + 1);
            }
            else {
                this.columnIndexes.push(columnIndex);
            }
        }
        else {
            this.columnIndexes.push(columnIndex);
            if (columnElement.hasClass('selected')) {
                this.columnIndexes = _.without(this.columnIndexes, columnIndex);
            }
            this.firstClick = columnIndex;
        }
        this.range = this.columnIndexes;
        this.AeviGrid.selectColumn(this.columnIndexes);
    };
    return AeviHeaderColumnHandler;
}());
var AeviBodyRowHandler = (function () {
    function AeviBodyRowHandler(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.AeviDataRepository = this.AeviGrid.AeviDataRepository;
        this.ctrlHold = false;
        this.shiftHold = false;
        this.rowIndexes = [];
        this.firstRowIndex = null;
        this.aeviRowNumber = $(this.AeviGrid.AeviConsts.rowNumberSelector);
        this.listener();
    }
    AeviBodyRowHandler.prototype.listener = function () {
        var _this = this;
        $(document).off('mousedown', this.aeviRowNumber.selector).on('mousedown', this.aeviRowNumber.selector, function (event) {
            _this.AeviGrid.AeviGridHandler.triggerClick();
            if (event.button === 0) {
                _this.mouseDown = true;
                _this.mouseDownCell = $(event.currentTarget);
                _this.click(_this.mouseDownCell);
                if (_this.AeviGrid.AeviDOM.isContextMenuVisible())
                    _this.AeviGrid.AeviDOM.hideAllContextMenus();
            }
        });
        $(document).on('mouseup', this.aeviRowNumber.selector + ', ' + this.AeviGrid.AeviConsts.cellSelector, function () {
            _this.mouseDown = false;
            _this.firstRowIndex = null;
        });
        $(document).on('mouseover', this.aeviRowNumber.selector + ', ' + this.AeviGrid.AeviConsts.cellSelector, function (event) {
            if (_this.mouseDown) {
                var range = [];
                var firstRowIndex = _this.firstRowIndex;
                if (_this.ctrlHold)
                    range = _this.range;
                else
                    range = [];
                if (_.isNull(firstRowIndex)) {
                    firstRowIndex = _this.AeviGrid.AeviDOM.getCellIndexes(_this.mouseDownCell).rowId;
                    _this.firstRowIndex = firstRowIndex;
                }
                var lastRowIndex = _this.AeviGrid.AeviDOM.getCellIndexes($(event.currentTarget)).rowId;
                var direction = (firstRowIndex < lastRowIndex) ? 'down' : 'up';
                var i = 0;
                if (direction == 'down') {
                    for (i = firstRowIndex; i < lastRowIndex + 1; i++) {
                        range.push(i);
                    }
                }
                else {
                    for (i = firstRowIndex; i > lastRowIndex - 1; i--) {
                        range.push(i);
                    }
                }
                _this.AeviGrid.selectRow(range);
                _this.range = range;
            }
        });
        $(document).on('keydown', function (event) {
            var key = _this.AeviGrid.AeviClientSide.getKeyName(event);
            if (key === 'ctrl' || _this.AeviGrid.AeviClientSide.isMac())
                _this.ctrlHold = true;
            if (key === 'shift')
                _this.shiftHold = true;
            if (key === 'delete' && $('.aeviRowNumber').hasClass('selected')) {
                if (_this.AeviGrid.mode.clipboard === 1)
                    return;
                _this.AeviGrid.AeviToolbar.AeviToolbarHandler.deleteRows();
            }
        });
        $(document).on('keyup', function () {
            _this.ctrlHold = false;
            _this.shiftHold = false;
        });
        $(document).on('contextmenu', this.aeviRowNumber.selector, function (event) {
            event.preventDefault();
            var target = $(event.currentTarget);
            if (_this.AeviGrid.AeviDataRepository.AeviTableDescription.readOnly || _this.AeviGrid.mode.clipboard === 1 || _this.AeviGrid.AeviGridLocker.isLocked)
                return false;
            if (!target.hasClass('selected'))
                _this.click(target);
            _this.AeviGrid.lastSelectedCell = $(event.currentTarget);
            if (!_this.AeviContextMenu)
                _this.AeviContextMenu = new AeviContextMenu(_this.AeviGrid, 'rowNumber');
            _this.AeviContextMenu.showMenu(event);
        });
    };
    AeviBodyRowHandler.prototype.click = function (cell) {
        var _this = this;
        this.AeviGrid.lastSelectedCell = cell;
        var rowIndex = this.AeviGrid.AeviDOM.getCellParentIndex(cell[0]);
        var message = this.AeviDataRepository.AeviTableData.getErrorMessage({ rowIndex: rowIndex });
        message = (message === '' || aeviIsUndefinedOrNull(message)) ? null : message;
        this.AeviGrid.AeviStatusBar.error(message);
        if (this.range) {
            $.each(this.range, function (i, el) {
                if ($.inArray(el, _this.rowIndexes) === -1)
                    _this.rowIndexes.push(_this.range[i]);
            });
        }
        if (!this.ctrlHold) {
            this.rowIndexes.length = 0;
            this.AeviDataRepository.clearSelectedRows();
        }
        if (this.shiftHold) {
            if (this.firstClick !== undefined && this.firstClick != rowIndex) {
                var direction = (this.firstClick > rowIndex) ? 1 : 0;
                var from = 0;
                var to = 0;
                var helper = null;
                if (!direction) {
                    from = this.firstClick;
                    to = rowIndex;
                }
                else {
                    helper = this.firstClick;
                    from = rowIndex;
                    to = helper;
                }
                for (var x = from; x <= to; x++) {
                    this.rowIndexes.push(x);
                }
            }
            else {
                this.rowIndexes.push(rowIndex);
            }
        }
        else {
            if (!this.ctrlHold)
                this.rowIndexes = [];
            this.rowIndexes.push(rowIndex);
            if (cell.hasClass('selected')) {
                this.rowIndexes = _.without(this.rowIndexes, rowIndex);
            }
            this.firstClick = rowIndex;
        }
        this.range = this.rowIndexes;
        this.AeviGrid.selectRow(this.rowIndexes);
    };
    return AeviBodyRowHandler;
}());
var AeviGridHandler = (function () {
    function AeviGridHandler(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.AeviDataRepository = this.AeviGrid.AeviDataRepository;
        this.AeviBodyCellHandler = new AeviBodyCellHandler(this.AeviGrid, this);
        this.AeviHeaderColumnHandler = new AeviHeaderColumnHandler(this.AeviGrid);
        this.AeviBodyRowHandler = new AeviBodyRowHandler(this.AeviGrid);
        this.AeviGrid.AeviClipboard = new AeviClipboard(this.AeviGrid);
        this.listener();
    }
    AeviGridHandler.prototype.cellChange = function (cell, changeBy) {
        if (this.selectedCell) {
            if (_.isNull(cell)) {
                if (!_.isNull(this.selectedCell)) {
                    this.cellLeave(true);
                    this.selectedCell = null;
                    this.AeviGrid.AeviGridEditor.refreshData();
                }
                this.AeviGrid.AeviBubbless.destroy();
                return false;
            }
            if (cell.attr('id') == this.selectedCell.attr('id'))
                return false;
            this.cellLeave(this.isRowPositionChanged(cell));
        }
        this.cellEnter(cell, changeBy);
    };
    AeviGridHandler.prototype.cellLeave = function (isRowPositionChanged) {
        var cellIndex = this.AeviGrid.AeviDOM.getCellIndexes(this.selectedCell);
        this.lastSelectedCell = this.selectedCell;
        if (!aeviIsUndefinedOrNull(this.selectedCellEditor)) {
            this.removeCellEditor();
            var rowStatus = this.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(cellIndex.rowIndex);
            if (this.selectedCellEditor.isDataValueChanged) {
                var validityResult = this.AeviDataRepository.AeviTableData.validateCell(cellIndex.rowIndex, cellIndex.cellIndex);
                this.AeviDataRepository.AeviTableData.setCellValidityInfo(validityResult, cellIndex.rowIndex, cellIndex.cellIndex);
                this.AeviDataRepository.AeviTableData.checkValidityStatus(cellIndex.rowIndex);
                rowStatus = this.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(cellIndex.rowIndex);
                if (rowStatus.indexOf('newrow') === -1) {
                    this.AeviDataRepository.AeviTableData.AeviStatus.addByRowIndex(cellIndex.rowIndex, 'update');
                }
                if (rowStatus.indexOf('newrow') !== -1) {
                    this.AeviDataRepository.AeviTableData.AeviStatus.addByRowIndex(cellIndex.rowIndex, 'insert');
                    this.AeviDataRepository.AeviTableData.validateRow(cellIndex.rowIndex);
                }
            }
            else {
            }
            var isRowWillBeRefreshedOnPutDone = false;
            if (isRowPositionChanged) {
                rowStatus = this.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(cellIndex.rowIndex);
                if (rowStatus.indexOf('invalid') === -1) {
                    var callPutRequest = false;
                    if (rowStatus.indexOf('insert') !== -1 || rowStatus.indexOf('update') !== -1) {
                        callPutRequest = true;
                    }
                    if (callPutRequest) {
                        this.AeviGrid.AeviDataService.putRow(cellIndex.rowIndex);
                        isRowWillBeRefreshedOnPutDone = true;
                    }
                }
            }
            if (this.selectedCellEditor.isDataValueChanged && !isRowWillBeRefreshedOnPutDone) {
                this.AeviGrid.AeviGridEditor.refreshRow({ rowIndex: cellIndex.rowIndex });
                var nextRowIndex = cellIndex.rowIndex + 1;
                var nextRowStatus = this.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(nextRowIndex);
                if (!_.isNull(nextRowStatus)) {
                    if (nextRowStatus.indexOf('hidden') !== -1) {
                        this.AeviDataRepository.AeviTableData.AeviStatus.removeByRowIndex(nextRowIndex, 'hidden');
                        this.AeviDataRepository.AeviTableData.AeviStatus.addByRowIndex(nextRowIndex, 'newrow');
                        this.AeviGrid.AeviGridEditor.refreshRow({ rowIndex: nextRowIndex });
                    }
                }
            }
        }
        if (!_.isNull(cellIndex)) {
            var nextRowIndex = cellIndex.rowIndex + 1;
            this.scrollToNextRowIfIsNew(nextRowIndex);
        }
    };
    AeviGridHandler.prototype.cellEnter = function ($cell, callBy) {
        if (aeviIsUndefinedOrNull(callBy)) {
            callBy = MyRetail.IAeviChangeType.byGrid;
        }
        this.selectedCell = $cell;
        if (aeviIsUndefinedOrNull(this.AeviGrid.AeviFakeEditor))
            this.AeviGrid.AeviFakeEditor = new AeviFakeEditor(this.AeviGrid);
        var cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes($cell[0]);
        if (aeviIsUndefinedOrNull(cellIndexes)) {
            this.AeviGrid.print('AeviGridHandler.cellEnter(), variable "cellIndexes" is undefined or null because parameter "cell" is empty or null');
            return null;
        }
        this.selectedRowIndex = cellIndexes.rowIndex;
        var selectedIndexes = { rowIndex: cellIndexes.rowIndex, cellIndex: cellIndexes.cellIndex };
        this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableData.setSelectedPositions(selectedIndexes, selectedIndexes);
        var action = this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumnActionByColumnIndex(cellIndexes.cellIndex);
        var editorFactory = new AeviEditorFactory(this.AeviGrid);
        if (this.AeviGrid.AeviGridLocker.isLocked) {
            editorFactory.focus($cell);
            editorFactory.createEditor($cell);
            this.selectedCellEditor = editorFactory.getEditor();
            if (this.selectedCellEditor instanceof AeviImageEditor) {
                this.selectedCellEditor.listener();
            }
            if (!aeviIsUndefinedOrNull(action)) {
                if (!_.isNull(action.ActionId) || action.ActionId !== 'null' || action.ActionId !== '') {
                    editorFactory.createEditor($cell);
                    this.selectedCellEditor = editorFactory.getEditor();
                    this.selectedCellEditor.setReady();
                    if (callBy === MyRetail.IAeviChangeType.byClick) {
                        this.selectedCellEditor.trigger();
                    }
                }
            }
        }
        else {
            editorFactory.createEditor($cell);
            this.selectedCellEditor = editorFactory.getEditor();
            if (editorFactory.isEditorExists()) {
                editorFactory.focus($cell);
                this.selectedCellEditor.setReady();
                if (this.selectedCellEditor instanceof AeviActionEditor && callBy === MyRetail.IAeviChangeType.byClick) {
                    this.selectedCellEditor.trigger();
                }
            }
        }
        this.AeviGrid.select(this.selectedCell[0]);
        this.AeviGrid.AeviFakeEditor.setContent($cell);
    };
    AeviGridHandler.prototype.removeCellEditor = function () {
        if (!aeviIsUndefinedOrNull(this.selectedCellEditor))
            this.selectedCellEditor.remove();
    };
    AeviGridHandler.prototype.triggerClick = function () {
        if (this.selectedCell && this.selectedCell.length) {
            this.AeviGrid.AeviDataRepository.AeviTableData.removeSelectedPositions();
            this.cellChange(null);
            $(document).trigger(AeviConsts.mouseUpEvent);
        }
    };
    AeviGridHandler.prototype.isRowPositionChanged = function (cell) {
        if (cell.length < 1 || _.isNull(cell)) {
            return;
        }
        return (this.selectedRowIndex !== this.AeviGrid.AeviDOM.getCellIndexes(cell).rowId);
    };
    AeviGridHandler.prototype.scrollToNextRowIfIsNew = function (nextRowIndex) {
        if (!this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(nextRowIndex, 'newrow')) {
            return;
        }
        if (!this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.is(nextRowIndex + 1, 'hidden')) {
            return;
        }
        var nextCell = this.AeviGrid.AeviDOM.getCell(0, nextRowIndex);
        if (aeviIsUndefinedOrNull(nextCell) || nextCell.length < 1) {
            return;
        }
        if (!AeviBodyCell.isInViewport(nextCell[0], document.getElementById(this.AeviGrid.tableId))) {
            $(document).trigger('mouseup');
            this.AeviGrid.AeviPager.moveTo({ rowId: nextRowIndex });
        }
    };
    AeviGridHandler.isUserClickToJqueryUI = function (clickedElement) {
        return AeviGridHandler.isUserClickTo(clickedElement, '.ui-widget');
    };
    AeviGridHandler.isUserClickToModal = function (clickedElement) {
        if (!$('.simplemodal-container').length)
            return false;
        return AeviGridHandler.isUserClickTo(clickedElement, '.simplemodal-container');
    };
    AeviGridHandler.isUserClickTo = function (clickedElement, selector) {
        return $.contains($(selector)[0], clickedElement);
    };
    AeviGridHandler.prototype.isUserClickToAeviTable = function (element) {
        var outerContainer = document.querySelector(this.AeviGrid.AeviConsts.outerContainerSelector);
        var search = document.querySelector(this.AeviGrid.AeviConsts.searchSelector);
        var result = $.contains(outerContainer, element);
        if (!result) {
            if (!aeviIsUndefinedOrNull(search))
                result = $.contains(search, element);
        }
        if (!result) {
            if ($(element).hasClass('aeviArrow'))
                result = true;
        }
        return result;
    };
    AeviGridHandler.prototype.listener = function () {
        var _this = this;
        $(document).on(AeviConsts.keyDownEvent, function (event) {
            var code = event.keyCode || event.which;
            var codeKey = _this.AeviGrid.AeviConsts.keys[code];
            if (codeKey === 'a' && (event.ctrlKey || event.metaKey)) {
                event.preventDefault();
                $(_this.AeviGrid.AeviConsts.aeviSelectAllSelector).trigger('click');
            }
            if (codeKey === 'c' && (event.ctrlKey || event.metaKey)) {
                if (!aeviIsUndefinedOrNull(_this.selectedCell) || _this.AeviGrid.AeviDOM.isRowSelected() || _this.AeviGrid.AeviClipboard.isDialogRendered)
                    _this.AeviGrid.AeviClipboard.copy();
            }
            if (codeKey === 'backspace') {
                if ($('.aeviToolbar__textfilter').is(':focus')) {
                    return;
                }
                if (_this.AeviGrid.AeviDOM.isFormVisible()) {
                    return;
                }
                if (_this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport()) {
                    return;
                }
                if (!$(_this.AeviGrid.AeviConsts.cellSelector).hasClass('hasEditor')) {
                    if (aeviIsUndefinedOrNull(_this.AeviGrid.AeviSearch)) {
                        event.preventDefault();
                        return;
                    }
                    else {
                        if (!_this.AeviGrid.AeviSearch.isSearchVisible) {
                            event.preventDefault();
                            return;
                        }
                    }
                }
            }
            if (codeKey === 'delete') {
                if (!aeviIsUndefinedOrNull(_this.AeviGrid.AeviGridLocker)) {
                    if (!_this.AeviGrid.AeviGridLocker.isLocked) {
                        if (_this.AeviGrid.AeviDataRepository.isMoreCellsSelected()) {
                            event.preventDefault();
                            _this.AeviGrid.AeviDataService.clearData();
                            _this.triggerClick();
                            _this.AeviGrid.AeviGridEditor.refreshData();
                        }
                    }
                }
            }
        });
        $(document).on(AeviConsts.mouseUpEvent, 'body', function (event) {
            var clickedElement = event.target;
            if (!_this.isUserClickToAeviTable(clickedElement)) {
                if ($('.ui-widget').length && AeviGridHandler.isUserClickToJqueryUI(clickedElement)) {
                    return;
                }
                if (_this.AeviGrid.isModalOpened) {
                    return;
                }
                _this.triggerClick();
            }
        });
        $(document).on(AeviConsts.clickEvent, 'body', function (event) {
            if (!_this.isUserClickToAeviTable(event.target)) {
                if (!_this.AeviGrid.isModalOpened)
                    _this.triggerClick();
            }
            if (_this.AeviGrid.AeviDOM.length(_this.AeviGrid.AeviConsts.contextMenuSelector))
                $(_this.AeviGrid.AeviConsts.contextMenuSelector).hide();
        });
        $(document).on(AeviConsts.keyDownEvent, function (event) {
            var key = _this.AeviGrid.AeviClientSide.getKeyName(event);
            if (key === 'f3' || ((event.ctrlKey || event.metaKey) && key === 'f')) {
                event.preventDefault();
                if (_this.AeviGrid.AeviDOM.isCellSelected()) {
                    _this.triggerClick();
                }
                if (aeviIsUndefinedOrNull(_this.AeviGrid.AeviSearch)) {
                    _this.AeviGrid.AeviSearch = new AeviSearch(_this.AeviGrid);
                }
                else {
                    _this.AeviGrid.AeviSearch.show();
                }
            }
        });
        $(document).off(AeviConsts.doubleClickEvent, '.aeviCell').on(AeviConsts.doubleClickEvent, '.aeviCell', function (event) {
            event.preventDefault();
            if (!_this.AeviGrid.AeviDataService.AeviUser.roles.Write) {
                return;
            }
            if (_this.AeviGrid.AeviDOM.isCellSelected() && !_this.AeviGrid.isLockMessageShowed() && _this.AeviGrid.AeviGridLocker.isLocked && !_this.AeviGrid.AeviDataRepository.AeviTableDescription.realReadOnly) {
                _this.AeviGrid.showLockMessage();
                return false;
            }
            if (_this.AeviGrid.AeviDataRepository.isMaxRowCountExceeded() && !_this.AeviGrid.AeviGridLocker.isLocked) {
                var indexes = _this.AeviGrid.AeviDOM.getCellIndexes(event.currentTarget);
                if (_this.AeviGrid.AeviDataRepository.AeviTableData.AeviStatus.isNew(indexes.rowIndex)) {
                    event.preventDefault();
                    _this.AeviGrid.showMaxRowCountMessage();
                    return false;
                }
            }
            return false;
        });
        window.onbeforeunload = function () {
            if (!aeviIsUndefinedOrNull(_this.AeviGrid.AeviDataService.AeviApiService.AeviApiAuthorization)) {
                if (_this.AeviGrid.AeviDataService.AeviApiService.AeviApiAuthorization.isCookiesNull) {
                    _this.AeviGrid.AeviDataService.AeviApiService.AeviApiAuthorization.setCookies();
                }
            }
            if (_this.AeviGrid.AeviDataService.AeviApiService.isDataChanged || _this.AeviGrid.AeviDataService.AeviApiService.isCellChanged) {
                return _this.AeviGrid.AeviLocalization.translate('close_window');
            }
        };
        document.addEventListener(AeviConsts.pasteEvent, function (event) {
            if (!_this.AeviGrid.isClipboardModalOpened) {
                if (!_this.AeviGrid.AeviDOM.isCellSelected())
                    return;
            }
            if (_this.AeviGrid.AeviDataRepository.AeviTableDescription.readOnly || _this.AeviGrid.AeviGridLocker.isLocked) {
                event.preventDefault();
                return false;
            }
            _this.AeviGrid.AeviClipboard.paste(event);
        });
        $(window).on(AeviConsts.resizeEvent, function (event) {
            _this.AeviGrid.setSizes();
            _this.AeviGrid.AeviGridEditor.headerRow.refresh(_this.AeviGrid.AeviDataRepository.AeviTableDescription.getColumns());
            _this.AeviGrid.AeviPager.refreshScrollContainer();
            _this.AeviGrid.AeviPager.setArrowPositions();
        });
    };
    return AeviGridHandler;
}());
var AeviPluginFactory = (function () {
    function AeviPluginFactory(aeviGridApi) {
        this.AeviGridApi = aeviGridApi;
    }
    AeviPluginFactory.prototype.get = function (pluginName) {
        var instanceName = 'Aevi' + pluginName + 'Plugin';
        var plugin = null;
        try {
            plugin = new window[instanceName](this.AeviGridApi);
        }
        catch (err) {
            this.AeviGridApi.log('AeviPluginFactory.get(), plugin with name: ' + pluginName + ' doesnt exists.');
        }
        return plugin;
    };
    return AeviPluginFactory;
}());
var AeviDOM = (function () {
    function AeviDOM(aeviGrid) {
        this.AeviGrid = aeviGrid;
    }
    AeviDOM.prototype.length = function (selector) {
        if (aeviIsUndefinedOrNull(selector) || !_.isString(selector)) {
            this.AeviGrid.print('AeviDOM.length(), parameter "selector" is undefined or null or is not string.');
            return false;
        }
        return document.querySelectorAll(selector).length >= 1;
    };
    AeviDOM.prototype.getById = function (id) {
        return document.getElementById(id);
    };
    AeviDOM.prototype.getEl = function (selector) {
        if (aeviIsUndefinedOrNull(selector) || !_.isString(selector)) {
            this.AeviGrid.print('AeviDOM.length(), parameter "selector" is undefined or null or is not string.');
            return null;
        }
        return document.querySelector(selector);
    };
    AeviDOM.prototype.getEls = function (selector) {
        if (aeviIsUndefinedOrNull(selector) || !_.isString(selector)) {
            this.AeviGrid.print('AeviDOM.length(), parameter "selector" is undefined or null or is not string.');
            return null;
        }
        return document.querySelectorAll(selector);
    };
    AeviDOM.prototype.isPure = function (element) {
        return (element instanceof HTMLElement);
    };
    AeviDOM.prototype.getTagName = function (element) {
        if (this.isPure(element))
            return element.tagName;
        return element[0].tagName;
    };
    AeviDOM.prototype.getClass = function (element) {
        if (this.isPure(element))
            return element.getAttribute('class');
        return element[0].getAttribute('class');
    };
    AeviDOM.prototype.append = function (element, appendHTML) {
        var div = document.createElement('div');
        div.innerHTML = appendHTML;
        while (div.children.length > 0) {
            element.appendChild(div.children[0]);
        }
    };
    AeviDOM.prototype.show = function (el) {
        el.style.display = 'block';
    };
    AeviDOM.prototype.hide = function (el) {
        el.style.display = 'none';
    };
    AeviDOM.prototype.closeModals = function () {
        AeviModal.closeAll();
    };
    AeviDOM.prototype.getVal = function (el) {
        return el.value;
    };
    AeviDOM.prototype.getPureTable = function () {
        return document.getElementById(this.AeviGrid.aeviTableId);
    };
    AeviDOM.prototype.getCell = function (cellIndex, rowIndex) {
        return $(this.getPureCell(cellIndex, rowIndex));
    };
    AeviDOM.prototype.getPureCell = function (cellIndex, rowIndex) {
        return document.getElementById('cell-' + rowIndex + '.' + cellIndex);
    };
    AeviDOM.prototype.getRow = function (rowIndex) {
        return $(this.getPureRow(rowIndex));
    };
    AeviDOM.prototype.getPureRow = function (rowIndex) {
        return document.getElementById('row-' + rowIndex);
    };
    AeviDOM.prototype.getSelectedRow = function () {
        return document.querySelector('tr' + this.AeviGrid.AeviConsts.statesSelector.selected);
    };
    AeviDOM.prototype.getSelectedRowsAndCells = function () {
        return document.querySelectorAll(this.AeviGrid.AeviConsts.statesSelector.selected + ',' + this.AeviGrid.AeviConsts.statesSelector.tinge);
    };
    AeviDOM.prototype.getRowIndex = function (row) {
        var stringIndex = null;
        if (this.isPure(row)) {
            if (_.isNull(row))
                return stringIndex;
            else
                stringIndex = row.getAttribute('id').split('-')[1];
        }
        else {
            if (!row.length)
                return stringIndex;
            else
                stringIndex = row.attr('id').split('-')[1];
        }
        return parseInt(stringIndex);
    };
    AeviDOM.prototype.getCellIndex = function (cell) {
        var stringIndex = null;
        if (this.isPure(cell))
            stringIndex = cell.getAttribute('id').split('-')[1].split('.')[1];
        else
            stringIndex = cell.attr('id').split('-')[1].split('.')[1];
        return parseInt(stringIndex);
    };
    AeviDOM.prototype.getCellParentIndex = function (cell) {
        var indexes = this.getCellIndexes(cell);
        var row = this.getPureRow(indexes.rowId);
        return this.getRowIndex(row);
    };
    AeviDOM.prototype.getCellIndexes = function (object) {
        if (aeviIsUndefinedOrNull(object) || object.length < 1) {
            this.AeviGrid.print('AeviDOM.getCellIndexes(), variable "object" is undefined or null or empty');
            return null;
        }
        var attr;
        if (this.isPure(object))
            attr = object.id;
        else if (_.isString(object))
            attr = object;
        else
            attr = object.attr('id');
        return this.getSplittedCellIndexes(attr);
    };
    AeviDOM.prototype.getSplittedCellIndexes = function (attr) {
        var pieces = attr;
        if (attr.indexOf('-') !== -1)
            pieces = attr.split('-')[1];
        pieces = pieces.split('.');
        return {
            rowId: parseInt(pieces[0]),
            cellId: parseInt(pieces[1]),
            rowIndex: parseInt(pieces[0]),
            cellIndex: parseInt(pieces[1])
        };
    };
    AeviDOM.prototype.getCellByTab = function (cellObject, direction) {
        var aeviDataRepository = this.AeviGrid.AeviDataRepository;
        var indexes = this.getCellIndexes(cellObject);
        var rowsLength = aeviDataRepository.AeviTableData.getRecordsLength();
        var visibleCellsLength = aeviDataRepository.AeviTableDescription.getVisibleCellsLength() - 1;
        var isFirstCell = (indexes.cellIndex === 1);
        var isLastCell = (indexes.cellIndex === visibleCellsLength);
        var isLastRow = (indexes.rowIndex + 1 === rowsLength);
        if (direction == 'tab') {
            if (isLastCell) {
                if (!isLastRow) {
                    indexes.cellIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getRowNumberIndex();
                    indexes.rowIndex += 1;
                }
            }
            else {
                indexes.cellIndex += 1;
            }
        }
        if (direction == 'shiftTab') {
            if (isFirstCell) {
                indexes.rowIndex -= 1;
                indexes.cellIndex = visibleCellsLength;
            }
            else {
                indexes.cellIndex -= 1;
            }
        }
        if (indexes.rowIndex === -1) {
            indexes.rowIndex = 0;
            indexes.cellIndex = this.AeviGrid.AeviDataRepository.AeviTableDescription.getRowNumberIndex();
        }
        return this.getCell(indexes.cellIndex, indexes.rowIndex);
    };
    AeviDOM.prototype.getCellByArrow = function (cellObject, direction) {
        var cell = this.getCellIndexes(cellObject);
        if (direction == 'down')
            cell.rowId += 1;
        if (direction == 'up')
            cell.rowId -= 1;
        if (cell.rowId === -1)
            cell.rowId = 0;
        return this.getCell(cell.cellId, cell.rowId);
    };
    AeviDOM.prototype.isModalVisible = function () {
        return $(this.AeviGrid.AeviConsts.modalWindowSelectorId).length;
    };
    AeviDOM.prototype.isFormVisible = function () {
        return this.length(AeviConsts.form);
    };
    AeviDOM.prototype.isSearchVisible = function () {
        return this.length(this.AeviGrid.AeviConsts.searchSelector);
    };
    AeviDOM.prototype.getSelectedCell = function () {
        return document.querySelector('#aeviTable tbody tr .aeviCell.selected');
    };
    AeviDOM.prototype.isRowSelected = function () {
        return ($(this.AeviGrid.AeviConsts.rowNumberSelector).hasClass(this.AeviGrid.AeviConsts.statuses.selected));
    };
    AeviDOM.prototype.isCellSelected = function () {
        return ($(this.AeviGrid.AeviConsts.cellSelector).hasClass(this.AeviGrid.AeviConsts.statuses.selected));
    };
    AeviDOM.prototype.isContextMenuVisible = function () {
        return $(this.AeviGrid.AeviConsts.contextMenuSelector).length;
    };
    AeviDOM.prototype.hideAllContextMenus = function () {
        $(this.AeviGrid.AeviConsts.contextMenuSelector).hide();
    };
    AeviDOM.prototype.getSelectedRowIndex = function () {
        var selectedRow = this.getSelectedRow();
        return (_.isNull(selectedRow)) ? null : this.getRowIndex(selectedRow);
    };
    AeviDOM.getColumnIndex = function (column) {
        return parseInt(column[0].getAttribute('id').split('-')[1]);
    };
    AeviDOM.addClass = function (el, className) {
        el.classList.add(className);
    };
    AeviDOM.removeClass = function (el, className) {
        el.classList.remove(className);
    };
    AeviDOM.focus = function (el) {
        el.focus();
    };
    AeviDOM.prototype.focusWithoutScroll = function (el) {
        var x = window.scrollX, y = window.scrollY;
        if (this.AeviGrid.AeviClientSide.isFirefox()) {
            AeviDOM.disableScroll();
            setTimeout(function () {
                el.focus();
                setTimeout(function () {
                    window.scrollTo(x, y);
                    AeviDOM.enableScroll();
                }, 5);
            });
        }
        else if (this.AeviGrid.AeviClientSide.isIe() || this.AeviGrid.AeviClientSide.isEdge()) {
            el.focus();
        }
        else {
            AeviDOM.disableScroll();
            el.focus();
            window.scrollTo(x, y);
            AeviDOM.enableScroll();
        }
    };
    AeviDOM.enableScroll = function () {
        window.onscroll = function () { };
    };
    AeviDOM.disableScroll = function () {
        var x = window.scrollX;
        var y = window.scrollY;
        window.onscroll = function () { window.scrollTo(x, y); };
    };
    return AeviDOM;
}());
var AeviClientSide = (function () {
    function AeviClientSide(aeviGrid) {
        this.mac = null;
        this.windows = null;
        this.chrome = null;
        this.safari = null;
        this.ie = null;
        this.firefox = null;
        this.opera = null;
        this.AeviGrid = aeviGrid;
        this.browser = this.getBrowser();
        this.system = this.getSystem();
        this.touchDevice = 'ontouchstart' in window || navigator.msMaxTouchPoints ? true : false;
        document.querySelector('body').classList.add(this.getTrimmedBrowser());
    }
    AeviClientSide.prototype.getBrowser = function () {
        var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE ' + (tem[1] || '');
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\bOPR\/(\d+)/);
            if (tem !== null)
                return 'Opera ' + tem[1];
        }
        if (/Edge/i.test(navigator.userAgent)) {
            return 'edge';
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) !== null)
            M.splice(1, 1, tem[1]);
        return M.join(' ');
    };
    AeviClientSide.prototype.getTrimmedBrowser = function () {
        return this.browser.toLowerCase().split(' ')[0];
    };
    AeviClientSide.prototype.getSystem = function () {
        var os = null;
        var appVersion = navigator.appVersion;
        if (appVersion.indexOf('Win') != -1) {
            os = AeviClientSide.SYSTEM.WINDOWS;
        }
        if (appVersion.indexOf('Mac') != -1) {
            os = AeviClientSide.SYSTEM.MACOS;
        }
        if (appVersion.indexOf('X11') != -1) {
            os = AeviClientSide.SYSTEM.UNIX;
        }
        if (appVersion.indexOf('Linux') != -1) {
            os = AeviClientSide.SYSTEM.LINUX;
        }
        return os;
    };
    AeviClientSide.prototype.isMac = function () {
        if (!aeviIsUndefinedOrNull(this.mac))
            return this.mac;
        this.mac = navigator.platform.toUpperCase().indexOf('MAC') >= 0;
        return this.mac;
    };
    AeviClientSide.prototype.isBrowser = function (browser) {
        if (this.browser.toLowerCase().indexOf(browser) !== -1)
            return true;
        return false;
    };
    AeviClientSide.prototype.isChrome = function () {
        return this.isBrowser(AeviClientSide.BROWSER.CHROME);
    };
    AeviClientSide.prototype.isIe = function () {
        return this.isBrowser(AeviClientSide.BROWSER.IE);
    };
    AeviClientSide.prototype.isEdge = function () {
        return this.isBrowser(AeviClientSide.BROWSER.EDGE);
    };
    AeviClientSide.prototype.isOpera = function () {
        return this.isBrowser(AeviClientSide.BROWSER.OPERA);
    };
    AeviClientSide.prototype.isFirefox = function () {
        return this.isBrowser(AeviClientSide.BROWSER.FIREFOX);
    };
    AeviClientSide.prototype.isSafari = function () {
        return this.isBrowser(AeviClientSide.BROWSER.SAFARI);
    };
    AeviClientSide.prototype.isLowerThanIe11 = function () {
        if (!this.isIe())
            return false;
        if (this.browser.toLowerCase().indexOf('11') === -1)
            return true;
        return false;
    };
    AeviClientSide.prototype.isTouch = function () {
        return this.touchDevice;
    };
    AeviClientSide.prototype.getKeyName = function (event) {
        return this.AeviGrid.AeviConsts.keys[event.keyCode || event.which];
    };
    AeviClientSide.prototype.isKey = function (event, key) {
        var keyName = this.getKeyName(event);
        return (keyName == key);
    };
    AeviClientSide.prototype.getNumberWithLocalizedDecimalPoint = function (val) {
        if (aeviIsUndefinedOrNull(val))
            return null;
        val = val.replace('.', ',');
        return val;
    };
    AeviClientSide.isPasteEvent = function (event, key) {
        return ((event.ctrlKey || event.metaKey) && key === 'v');
    };
    AeviClientSide.prototype.isNavigationKeyPressed = function (event) {
        var key = this.getKeyName(event);
        if (aeviIsUndefinedOrNull(key))
            return true;
        if (event.ctrlKey || event.metaKey)
            return true;
        switch (key) {
            case 'backspace':
            case 'enter':
            case 'shift':
            case 'ctrl':
            case 'tab':
            case 'alt':
            case 'pause/break':
            case 'caps lock':
            case 'insert':
            case 'num lock':
            case 'scroll lock':
            case 'f1':
            case 'f2':
            case 'f3':
            case 'f4':
            case 'f5':
            case 'f6':
            case 'f7':
            case 'f8':
            case 'f9':
            case 'f10':
            case 'f11':
            case 'f12':
            case '33':
            case '34':
            case '35':
            case '46':
            case '37':
            case '38':
            case '39':
            case '40':
            case 'up arrow':
            case 'right arrow':
            case 'down arrow':
            case 'left arrow':
            case 'home':
            case 'end':
            case 'page down':
            case 'page up':
            case 'escape':
            case 'left window key':
            case 'right window key':
                return true;
            default:
                return false;
        }
    };
    AeviClientSide.BROWSER = {
        CHROME: 'chrome',
        IE: 'ie',
        OPERA: 'opera',
        FIREFOX: 'firefox',
        SAFARI: 'safari',
        EDGE: 'edge'
    };
    AeviClientSide.SYSTEM = {
        WINDOWS: 'Windows',
        MACOS: 'MacOS',
        UNIX: 'UNIX',
        LINUX: 'Linux'
    };
    return AeviClientSide;
}());
var AeviConsts = (function () {
    function AeviConsts(aeviGrid, tableId) {
        this.AeviGrid = aeviGrid;
        this.tableId = tableId;
        this.setContants();
    }
    AeviConsts.prototype.setContants = function () {
        this.putType = {
            insert: 0,
            update: 1
        };
        this.modalWindowSelectorId = ' #simplemodal-container ';
        this.contextMenuSelector = ' .aeviContextMenu ';
        this.dataTypes = {
            0: 'Text',
            1: 'Number',
            2: 'IntegerNumber',
            3: 'Currency',
            4: 'DateTime',
            5: 'ShortDate',
            6: 'ShortTime',
            7: 'Boolean',
            8: 'Enum',
            9: 'Hyperlink',
            10: 'RegularExpression',
            11: 'Image',
            98: 'SelectedCellsArray',
            99: 'InvalidCellsArray',
            100: 'Status'
        };
        this.imageFormat = 'jpeg';
        this.fakeInputSelector = '#aeviFakeEditorInput';
        this.controlCommandKey = '_var.controlCommandKey';
        this.clipboard = {
            maximumCountOfInsertedRows: 1000
        };
        this.tableSelector = '#' + this.tableId + ' ';
        this.aeviWrapperSelector = ' .aeviWrapper ';
        this.aeviTableId = ' #aeviTable ';
        this.tableSelector = ' .aeviTable ';
        this.tableHeadSelector = ' .table-head ';
        this.tableBodySelector = ' .table-body ';
        this.tableHeaderSelector = ' #table-header ';
        this.selectedCellClasses = [
            'selected',
            'selectedTop',
            'selectedBottom',
            'selectedLeft',
            'selectedRight'
        ];
        this.selectedCellClassesToRemove = [
            'tinge',
            'selected',
            'selectedTop',
            'selectedBottom',
            'selectedLeft',
            'selectedRight'
        ];
        this.rowSelector = ' .aeviRow ';
        this.rowNumberSelector = ' .aeviRowNumber ';
        this.renderRowSelector = ' .aeviRow.render ';
        this.cellSelector = ' .aeviCell ';
        this.renderCellSelector = ' .aeviCell.visible, .aeviCell.sortedRow ';
        this.columnSelector = ' .aeviColumn ';
        this.sortButtonSelector = ' .aeviSort ';
        this.innerContainerSelector = ' .inner-container ';
        this.outerContainerSelector = ' .outer-container ';
        this.statusBarSelector = ' .aeviStatusBar ';
        this.statusBarItemSelector = ' .aeviStatusBarItem ';
        this.statusBarHeight = 31;
        this.toolbarSelector = ' .aeviToolbar ';
        this.toolbar = {
            selectors: {
                deleteRow: ' .aeviToolbar__delete_row ',
                insertRow: ' .aeviToolbar__insert_row ',
                paste: ' .aeviToolbar__paste '
            }
        };
        this.noSelectionClass = 'noselection';
        this.aeviSelectAllSelector = ' .aeviSelectAll ';
        this.arrowsWrapperSelector = ' .aeviArrows ';
        this.arrowSelector = ' .aeviArrow';
        this.arrowSize = 44 + 20;
        this.searchSelector = ' .aeviSearch ';
        this.search = {
            selector: {
                find: ' .aeviSearch__find ',
                cancel: ' .aeviSearch__cancel ',
                field: ' .aeviSearch__field ',
                radio: ' .aeviSearch__radio ',
                direction: ' .aeviSearch__radio[name="direction"]'
            }
        };
        this.aeviStatusBarHelperSelector = ' .aeviStatusBarHelper';
        this.aeviStatusBarInfoSelector = ' .aeviStatusBarInfo';
        this.aeviStatusBarQuestSelector = ' .aeviStatusBarQuest';
        this.aeviSumSelector = ' .aeviSum';
        this.aeviSumColSelector = ' .aeviSumCol';
        this.helperSelector = ' .aeviHelp';
        this.aeviModalSelector = ' .aeviModal ';
        this.copyModalId = '#' + this.tableId + '-aeviClipboardDialog';
        this.copyModalTextareaSelector = this.copyModalId + ' textarea';
        this.firstColumnWidth = 47;
        this.rowHeight = 25;
        this.scrollHeight = getScrollbarWidth();
        this.minimumRows = 8;
        this.outerContainerBottomEmptySpace = 30;
        this.sumContainerBottomEmptySpace = 30;
        if (this.AeviGrid.AeviClientSide.isIe())
            this.scrollHeight = this.scrollHeight + 4;
        this.statuses = {
            selected: 'selected',
            render: 'render',
            newrow: 'newrow',
            hidden: 'hidden',
            invalid: 'invalid',
            softInvalid: 'softInvalid',
            sortedRow: 'sortedRow'
        };
        this.statesSelector = {
            selected: ' .selected ',
            tinge: ' .tinge '
        };
        this.pager = {
            rowHeight: this.rowHeight,
            tollerance: 25,
            visibleRows: 100,
            countOfNewRows: 25
        };
        this.keys = {
            8: 'backspace',
            9: 'tab',
            13: 'enter',
            16: 'shift',
            17: 'ctrl',
            18: 'alt',
            19: 'pause/break',
            20: 'caps lock',
            27: 'escape',
            32: '(space)',
            33: 'page up',
            34: 'page down',
            35: 'end',
            36: 'home',
            37: 'left arrow',
            38: 'up arrow',
            39: 'right arrow',
            40: 'down arrow',
            45: 'insert',
            46: 'delete',
            48: 0,
            49: 1,
            50: 2,
            51: 3,
            52: 4,
            53: 5,
            54: 6,
            55: 7,
            56: 8,
            57: 9,
            65: 'a',
            66: 'b',
            67: 'c',
            68: 'd',
            69: 'e',
            70: 'f',
            71: 'g',
            72: 'h',
            73: 'i',
            74: 'j',
            75: 'k',
            76: 'l',
            77: 'm',
            78: 'n',
            79: 'o',
            80: 'p',
            81: 'q',
            82: 'r',
            83: 's',
            84: 't',
            85: 'u',
            86: 'v',
            87: 'w',
            88: 'x',
            89: 'y',
            90: 'z',
            91: 'left window key',
            92: 'right window key',
            93: 'select key',
            96: 'numpad 0',
            97: 'numpad 1',
            98: 'numpad 2',
            99: 'numpad 3',
            100: 'numpad 4',
            101: 'numpad 5',
            102: 'numpad 6',
            103: 'numpad 7',
            104: 'numpad 8',
            105: 'numpad 9',
            106: 'multiply',
            107: 'add',
            109: 'subtract',
            110: 'decimal point',
            111: 'divide',
            112: 'f1',
            113: 'f2',
            114: 'f3',
            115: 'f4',
            116: 'f5',
            117: 'f6',
            118: 'f7',
            119: 'f8',
            120: 'f9',
            121: 'f10',
            122: 'f11',
            123: 'f12',
            144: 'num lock',
            145: 'scroll lock',
            186: 'semi-colon',
            187: 'equal sign',
            188: 'comma',
            189: 'dash',
            190: 'period',
            191: 'forward slash',
            192: 'grave accent',
            219: 'open bracket',
            220: 'back slash',
            221: 'close braket',
            222: 'single quote'
        };
    };
    AeviConsts.keyDownEvent = 'keydown';
    AeviConsts.clickEvent = 'click';
    AeviConsts.mouseUpEvent = 'mouseup';
    AeviConsts.doubleClickEvent = 'dblclick';
    AeviConsts.pasteEvent = 'paste';
    AeviConsts.resizeEvent = 'resize';
    AeviConsts.touchEvent = 'touch';
    AeviConsts.imageModalUpload = ' .aeviImageModal__upload';
    AeviConsts.imageModalFile = ' .aeviImageModal__file';
    AeviConsts.imageModalEdit = ' .aeviImageModal__edit';
    AeviConsts.imageModalDelete = ' .aeviImageModal__delete';
    AeviConsts.imageModalSaveAndClose = ' .aeviImageModal__saveAndClose';
    AeviConsts.imageModalCloseWithoutClose = ' .aeviImageModal__closeWithoutSave';
    AeviConsts.imageModalClose = ' .aeviImageModal__close';
    AeviConsts.imageModalChangeEvent = 'aeviImageEditorChanged';
    AeviConsts.form = ' .aeviForm ';
    AeviConsts.formImage = ' .aeviForm__image';
    AeviConsts.formImageUploadButton = ' *[data-imageaction="upload"]';
    AeviConsts.formImageDeleteButton = ' *[data-imageaction="delete"]';
    AeviConsts.formImageEditButton = ' *[data-imageaction="edit"]';
    AeviConsts.allEvents = 'blur change click dblclick keydown keypress keyup mousedown mouseup select';
    AeviConsts.fileImportInput = ' #aeviImportFile ';
    return AeviConsts;
}());
var AeviLocalization = (function () {
    function AeviLocalization(apiLang, settings, aeviConsts, aeviClientSide) {
        this.apiLang = apiLang;
        this.setDateFormats();
        this.AeviConsts = aeviConsts;
        this.AeviClientSide = aeviClientSide;
        this.setTranslations(this.getCulture(), settings);
    }
    AeviLocalization.prototype.getAvailableLanguages = function () {
        return 'en|cs';
    };
    AeviLocalization.prototype.getCulture = function () {
        if (!_.isUndefined(this.apiLang)) {
            return this.apiLang.substring(0, 2);
        }
        else {
            return (navigator.language && this.getAvailableLanguages().indexOf(navigator.language) !== -1) ? navigator.language : 'en';
        }
    };
    AeviLocalization.prototype.translate = function (key) {
        if (!_.isUndefined(this.translates[key])) {
            var translate = this.translates[key];
            if (translate.indexOf(this.AeviConsts.controlCommandKey) !== -1) {
                var deviceControlCommandKey = (this.AeviClientSide.isMac()) ? this.translate('safari_command') : this.translate('windows_control');
                translate = translate.replace(this.AeviConsts.controlCommandKey, deviceControlCommandKey);
            }
            return translate;
        }
        else {
            return 'missing translate for: ' + key;
        }
    };
    AeviLocalization.prototype.setTranslations = function (culture, settings) {
        var translates = {
            'en': {
                'connection_fail': 'Connection failed'
            },
            'cs': {
                'connection_fail': 'Služba není nyní dostupná, zkuste to prosím později nebo kontaktujte naše servisní oddělení.',
                'hello_world': 'Ahoj Světe!',
                'query_placeholder': 'Vyhledat ...',
                'save': 'Uloženo ...',
                'toolbar_lock': 'Zamknout',
                'toolbar_unlock': 'Odemknout',
                'applocked': 'Tabulka byla uzamčena',
                'appunlocked': 'Tabulka byla odemčena',
                'toolbar_save': 'Uložit',
                'toolbar_copy': 'Kopírovat',
                'toolbar_search': 'Vyhledat',
                'toolbar_pdf': 'Stáhnout jako PDF',
                'toolbar_import': 'Import',
                'toolbar_export': 'Export',
                'toolbar_importexport': 'Import | Export',
                'new_row_added': 'Nový řádek byl úspěšně uložen do temp tabulky',
                'table_description_loaded': 'Hlavička byla úspěšně načtena',
                'table_data_loaded': 'Data byla úspěšně načtena',
                'table_data_saved': 'Data byla úspěšně uložena do temp tabulky',
                'table_data_commit': 'Data byla úspěšně uložena do cloudu',
                'table_data_deleted': 'Řádek / Řádky byly úspěšně odstraněny',
                'deleting_temp_table': 'Promazávám obsah temp tabulky',
                'cannot_delete': 'Tento řádek nemůže být odstraněn',
                'no_found': 'Pro hledaný řetězec nebylo nic nalezeno.',
                'found': 'Pro hledaný řetězec bylo nalezeno: ',
                'records': ' záznamů',
                'validate_integer_number_error': 'Hodnota záznamu není celé číslo.',
                'image_present': 'detail obrázku',
                'image_missing': 'nahrát obrázek',
                'image_drag': 'Přetáhněte zde Váš obrázek nebo klikněte',
                'image_delete': 'Smazat',
                'image_close': 'Zavřít',
                'image_edit': 'Upravit',
                'yes': 'Ano',
                'no': 'Ne',
                'ok': 'OK',
                'cancel': 'Storno',
                'temporary_message': 'Poslední změny nebyly uloženy do databáze. Pro uložení změn zvolte <strong>\"Uložit\"</strong>, pro smazání změn <strong>\"Smazat\"</strong>, pro další práci s daty zvolte <strong>\"Storno\"</strong>',
                'temporary_message_filter': 'Provedli jste změny, které prozatím nebyly uloženy. Pro uložení změn zvolte <strong>\"Uložit\"</strong>, pro smazání změn <strong>\"Smazat\"</strong>, pro další práci s daty zvolte <strong>\"Storno\"</strong>.',
                'server_invalid_records_message': 'Pokoušíte se uložit nevalidní data. Pro pokus o uložení změn zvolte <strong>\"Uložit\"</strong>,<br> pro smazání změn <strong>\"Ignorovat chyby\"</strong> (chybné řádky nebudou uloženy a jejich obsah bude ztracen), <br> pro editaci chybných dat zvolte <strong>\"Storno\"</strong>.',
                'value_required': 'Hodnota je povinná',
                'datetime_error': 'Hodnota musí být platné datum',
                'no_rows_selected': 'Nejsou označeny žádné řádky',
                'delete_rows_question': 'Opravdu chcete odstranit řádek / řádky agendy ',
                'close_window': 'V temp table se nachází neuložené záznamy.',
                'commit_invalid_rows': 'Některé řádky nemohly být uloženy do cloudu. Opravte je prosím.',
                'manual_copy_message': 'Pro zkopírování obsahu stiskněte znovu CTRL + C.',
                'readonly_message': 'Tabulka je nyní zamčena. Chcete-li nad tabulkou provést změny, klikněte na tlačítko "Odemknout"',
                'first_load_readonly_message': 'Tato tabulka je jen pro čtení.',
                'allowfilter_message': 'V této datové entitě není možné filtrovat data.',
                'filtering_disable': 'Filtr nelze použít',
                'error_validate_ean': 'Zadaný EAN není ve správném formátu.',
                'error_validate_number': 'Hodnota musí být číslo.',
                'error_validate_maxlength': 'Počet znaků musí být menší než ',
                'readonly_column': 'Tato buňka je jen pro čtení - vyplní systém',
                'save_data': 'Uložit',
                'delete_data': 'Smazat',
                'toolbar_disable_toastr': 'Zobrazovat informační hlášení',
                'delete_row': 'Odstranit řádek',
                'toolbar_delete_row': 'Odstranit řádek',
                'insert_row': 'Vložit řádek',
                'toolbar_insert_row': 'Vložit řádek',
                'toolbar_insert_by_form': 'Vložit pomocí formuláře',
                'grid_version': 'Verze gridu',
                'api_version': 'Verze Dataservice',
                'count_of_rows': 'Velikost tabulky',
                'dynamic': 'Automatická',
                'clipboard_support_message': 'Funkce kopírovat / vložit není v tomto prohlížeči podporována.',
                'result_showed': 'Celkově zobrazeno: ',
                'toolbar_filter': 'Použít ',
                'warning': 'Upozornění',
                'must_filter': 'Pro zobrazení dat musíte použít filtr.',
                'some_filters_not_filled': 'Pro načtení dat musíte vyplnit povinné filtry, které jsou označeny hvězdičkou.',
                'tooltip_help': 'Nápověda',
                'invalid_records': 'Řádky označené červeně nelze uložit, protože jsou nekorektně vyplněny.',
                'max_row_count_1': 'Váš účet umožňuje založit max. ',
                'max_row_count_2': ' řádků. Pro bližší informace k možnostem rozšíření kontaktujte naše obchodní oddělení.',
                'protected_mode': 'Tabulka je nyní z bezpečnostních důvodů uzamčena. Chcete-li tabulku editovat, klikněte na tlačítko povolit úpravy nebo odemknout.',
                'protected_mode_title': 'CHRÁNĚNÉ ZOBRAZENÍ',
                'enable_editing': 'Povolit úpravy',
                'ignore_errors': 'Ignorovat chyby',
                'toolbar_xls': 'Vzorový excel pro stažení',
                'invalid_data_status': 'Zobrazená data jsou chybná. Ostatní byla v pořádku uložena. Kliknutím na červené číslo řádku se zobrazí popis chyby.',
                'server_generated_message': 'Nevyplníte-li hodnotu buňky, pak bude hodnota generována systémem při uložení',
                'image_delete_message': 'Opravdu chcete obrázek smazat?',
                'image_close_unsaved': 'Opravdu chcete editor uzavřít? Změny, které jste provedli nebudou uloženy.',
                'close_without_save': 'Zavřít bez uložení',
                'paste_exceeded': 'Pokoušíte se vložit více než',
                'rows': 'řádků',
                'toolbar_paste': 'Vyčistit a vložit',
                'paste_wrong_columns_count': 'Počet sloupců neodpovídá vzorovému excelu.',
                'paste_wrong_format': 'Pokoušíte se vložit data, která nemají formát tabulky.',
                'paste_wrong_rows_and_cells': 'Pokoušíte se vložit více než jeden řádek a současně vkládáte počet sloupců, který neodpovídá počtu sloupců ve vzorovému excelu. <br> Samostatně lze vložit jen jednu buňku',
                'paste_wrong_target_with_row': 'Nemůžete vložit více než jednu buňku jinam, než na konec tabulky, pro vložení více řádků stisktněte tlačítko "název tlačítka" v panelu nástrojů.',
                'paste_allow_one_cell': 'Můžete vložit pouze jednu buňku',
                'paste_wrong_target': 'Pokoušíte se vkládat jinam než do první buňky',
                'paste_first_save': 'Nemůžete vložit, protože nemáte uložená data. Nejprve je uložte a poté spusťte tuto funkci znovu.',
                'toolbar_paste_message': 'Po vložení budou zobrazeny jen vložené řádky. Stiskněte prosím klávesovou zkratku <strong>' + this.AeviConsts.controlCommandKey + ' + V</strong>',
                'safari_command': 'Control',
                'windows_control': 'Ctrl',
                'image_preview': 'Náhled obrázku',
                'toolbar_paste_done': 'Data z clipboardu byla úspěšně vložena',
                'toolbar_discard_changes': 'Zrušit změny',
                'discard_modal_message': 'Kliknutím na tlačítko Smazat budou veškeré vaše změny ztraceny.',
                'no_changes_message': 'Neprovedli jste žádné změny.',
                'discard_done_message': 'Data byla úspěšně obnovena.',
                'extended': 'Rozšířená',
                'cannot_insert_char': 'Pokoušíte se zadat znak, který není možné do této buňky zapsat.',
                'save_mode_excel_0': 'Data z clipboardu byla úspěšně vložena. Po uložení budou aktualizována.',
                'save_mode_excel_1': 'Data z clipboardu byla úspěšně vložena. Po uložení budou přepsána.',
                'preview': 'Náhled',
                'search_title': 'Vyhledávání',
                'search_find': 'Vyhledat',
                'search_up': 'Nahoru',
                'search_down': 'Dolů',
                'search_insert_keyword': 'Zadejte hledaný výraz: ',
                'image_blocked': 'blokováno do uložení',
                'button_enable_editing': 'Povolit úpravy',
                'save_and_close': 'Uložit a zavřít',
                'modal_inserting': 'Vložení',
                'search_direction': 'Směr: ',
                'previous': 'Předchozí',
                'next': 'Další',
                'bad_file_format': 'Pokoušíte se vložit soubor v nepodporovaném formátu.',
                'import_modal_message': 'Importní funkce vyhodnocuje první řádek jako hlavičku a proto nebude tento řádek do tabulky vložen. Chcete data importovat?',
                'roles_cannot_read': 'Nemáte dostatečné oprávnění na zobrazení této agendy',
                'entity_is_blocked1': 'Tabulku již edituje uživatel ',
                'entity_is_blocked2': ', chcete data editovat místo tohoto uživatele ?',
                'entity_is_changed1': 'Data již byla uložena uživatelem ',
                'entity_is_changed2': ' , aby jste je mohli dále editovat je nutné data obnovit.',
                'you_are_grid_blocked_user': 'Převzali jste kontrolu nad gridem.'
            }
        };
        this.translates = translates[culture];
        if (!_.isUndefined(settings.translations)) {
            $.extend(this.translates, settings.translations);
        }
    };
    AeviLocalization.prototype.setDateFormats = function () {
        this.isoCodes = {
            "ar-SA": "dd/MM/yy",
            "bg-BG": "dd.M.yyyy",
            "ca-ES": "dd/MM/yyyy",
            "zh-TW": "yyyy/M/d",
            "cs-CZ": "d.m.yy",
            "cs": "d.m.yy",
            "da-DK": "dd-MM-yyyy",
            "de-DE": "dd.MM.yyyy",
            "el-GR": "d/M/yyyy",
            "en-US": "mm.dd.yy",
            "fi-FI": "d.M.yyyy",
            "fr-FR": "dd/MM/yyyy",
            "he-IL": "dd/MM/yyyy",
            "hu-HU": "yyyy. MM. dd.",
            "is-IS": "d.M.yyyy",
            "it-IT": "dd/MM/yyyy",
            "ja-JP": "yyyy/MM/dd",
            "ko-KR": "yyyy-MM-dd",
            "nl-NL": "d-M-yyyy",
            "nb-NO": "dd.MM.yyyy",
            "pl-PL": "yyyy-MM-dd",
            "pt-BR": "d/M/yyyy",
            "ro-RO": "dd.MM.yyyy",
            "ru-RU": "dd.MM.yyyy",
            "hr-HR": "d.M.yyyy",
            "sk-SK": "d. M. yyyy",
            "sq-AL": "yyyy-MM-dd",
            "sv-SE": "yyyy-MM-dd",
            "th-TH": "d/M/yyyy",
            "tr-TR": "dd.MM.yyyy",
            "ur-PK": "dd/MM/yyyy",
            "id-ID": "dd/MM/yyyy",
            "uk-UA": "dd.MM.yyyy",
            "be-BY": "dd.MM.yyyy",
            "sl-SI": "d.M.yyyy",
            "et-EE": "d.MM.yyyy",
            "lv-LV": "yyyy.MM.dd.",
            "lt-LT": "yyyy.MM.dd",
            "fa-IR": "MM/dd/yyyy",
            "vi-VN": "dd/MM/yyyy",
            "hy-AM": "dd.MM.yyyy",
            "az-Latn-AZ": "dd.MM.yyyy",
            "eu-ES": "yyyy/MM/dd",
            "mk-MK": "dd.MM.yyyy",
            "af-ZA": "yyyy/MM/dd",
            "ka-GE": "dd.MM.yyyy",
            "fo-FO": "dd-MM-yyyy",
            "hi-IN": "dd-MM-yyyy",
            "ms-MY": "dd/MM/yyyy",
            "kk-KZ": "dd.MM.yyyy",
            "ky-KG": "dd.MM.yy",
            "sw-KE": "M/d/yyyy",
            "uz-Latn-UZ": "dd/MM yyyy",
            "tt-RU": "dd.MM.yyyy",
            "pa-IN": "dd-MM-yy",
            "gu-IN": "dd-MM-yy",
            "ta-IN": "dd-MM-yyyy",
            "te-IN": "dd-MM-yy",
            "kn-IN": "dd-MM-yy",
            "mr-IN": "dd-MM-yyyy",
            "sa-IN": "dd-MM-yyyy",
            "mn-MN": "yy.MM.dd",
            "gl-ES": "dd/MM/yy",
            "kok-IN": "dd-MM-yyyy",
            "syr-SY": "dd/MM/yyyy",
            "dv-MV": "dd/MM/yy",
            "ar-IQ": "dd/MM/yyyy",
            "zh-CN": "yyyy/M/d",
            "de-CH": "dd.MM.yyyy",
            "en-GB": "dd/MM/yyyy",
            "es-MX": "dd/MM/yyyy",
            "fr-BE": "d/MM/yyyy",
            "it-CH": "dd.MM.yyyy",
            "nl-BE": "d/MM/yyyy",
            "nn-NO": "dd.MM.yyyy",
            "pt-PT": "dd-MM-yyyy",
            "sr-Latn-CS": "d.M.yyyy",
            "sv-FI": "d.M.yyyy",
            "az-Cyrl-AZ": "dd.MM.yyyy",
            "ms-BN": "dd/MM/yyyy",
            "uz-Cyrl-UZ": "dd.MM.yyyy",
            "ar-EG": "dd/MM/yyyy",
            "zh-HK": "d/M/yyyy",
            "de-AT": "dd.MM.yyyy",
            "en-AU": "d/MM/yyyy",
            "es-ES": "dd/MM/yyyy",
            "fr-CA": "yyyy-MM-dd",
            "sr-Cyrl-CS": "d.M.yyyy",
            "ar-LY": "dd/MM/yyyy",
            "zh-SG": "d/M/yyyy",
            "de-LU": "dd.MM.yyyy",
            "en-CA": "dd/MM/yyyy",
            "es-GT": "dd/MM/yyyy",
            "fr-CH": "dd.MM.yyyy",
            "ar-DZ": "dd-MM-yyyy",
            "zh-MO": "d/M/yyyy",
            "de-LI": "dd.MM.yyyy",
            "en-NZ": "d/MM/yyyy",
            "es-CR": "dd/MM/yyyy",
            "fr-LU": "dd/MM/yyyy",
            "ar-MA": "dd-MM-yyyy",
            "en-IE": "dd/MM/yyyy",
            "es-PA": "MM/dd/yyyy",
            "fr-MC": "dd/MM/yyyy",
            "ar-TN": "dd-MM-yyyy",
            "en-ZA": "yyyy/MM/dd",
            "es-DO": "dd/MM/yyyy",
            "ar-OM": "dd/MM/yyyy",
            "en-JM": "dd/MM/yyyy",
            "es-VE": "dd/MM/yyyy",
            "ar-YE": "dd/MM/yyyy",
            "en-029": "MM/dd/yyyy",
            "es-CO": "dd/MM/yyyy",
            "ar-SY": "dd/MM/yyyy",
            "en-BZ": "dd/MM/yyyy",
            "es-PE": "dd/MM/yyyy",
            "ar-JO": "dd/MM/yyyy",
            "en-TT": "dd/MM/yyyy",
            "es-AR": "dd/MM/yyyy",
            "ar-LB": "dd/MM/yyyy",
            "en-ZW": "M/d/yyyy",
            "es-EC": "dd/MM/yyyy",
            "ar-KW": "dd/MM/yyyy",
            "en-PH": "M/d/yyyy",
            "es-CL": "dd-MM-yyyy",
            "ar-AE": "dd/MM/yyyy",
            "es-UY": "dd/MM/yyyy",
            "ar-BH": "dd/MM/yyyy",
            "es-PY": "dd/MM/yyyy",
            "ar-QA": "dd/MM/yyyy",
            "es-BO": "dd/MM/yyyy",
            "es-SV": "dd/MM/yyyy",
            "es-HN": "dd/MM/yyyy",
            "es-NI": "dd/MM/yyyy",
            "es-PR": "dd/MM/yyyy",
            "am-ET": "d/M/yyyy",
            "tzm-Latn-DZ": "dd-MM-yyyy",
            "iu-Latn-CA": "d/MM/yyyy",
            "sma-NO": "dd.MM.yyyy",
            "mn-Mong-CN": "yyyy/M/d",
            "gd-GB": "dd/MM/yyyy",
            "en-MY": "d/M/yyyy",
            "prs-AF": "dd/MM/yy",
            "bn-BD": "dd-MM-yy",
            "wo-SN": "dd/MM/yyyy",
            "rw-RW": "M/d/yyyy",
            "qut-GT": "dd/MM/yyyy",
            "sah-RU": "MM.dd.yyyy",
            "gsw-FR": "dd/MM/yyyy",
            "co-FR": "dd/MM/yyyy",
            "oc-FR": "dd/MM/yyyy",
            "mi-NZ": "dd/MM/yyyy",
            "ga-IE": "dd/MM/yyyy",
            "se-SE": "yyyy-MM-dd",
            "br-FR": "dd/MM/yyyy",
            "smn-FI": "d.M.yyyy",
            "moh-CA": "M/d/yyyy",
            "arn-CL": "dd-MM-yyyy",
            "ii-CN": "yyyy/M/d",
            "dsb-DE": "d. M. yyyy",
            "ig-NG": "d/M/yyyy",
            "kl-GL": "dd-MM-yyyy",
            "lb-LU": "dd/MM/yyyy",
            "ba-RU": "dd.MM.yy",
            "nso-ZA": "yyyy/MM/dd",
            "quz-BO": "dd/MM/yyyy",
            "yo-NG": "d/M/yyyy",
            "ha-Latn-NG": "d/M/yyyy",
            "fil-PH": "M/d/yyyy",
            "ps-AF": "dd/MM/yy",
            "fy-NL": "d-M-yyyy",
            "ne-NP": "M/d/yyyy",
            "se-NO": "dd.MM.yyyy",
            "iu-Cans-CA": "d/M/yyyy",
            "sr-Latn-RS": "d.M.yyyy",
            "si-LK": "yyyy-MM-dd",
            "sr-Cyrl-RS": "d.M.yyyy",
            "lo-LA": "dd/MM/yyyy",
            "km-KH": "yyyy-MM-dd",
            "cy-GB": "dd/MM/yyyy",
            "bo-CN": "yyyy/M/d",
            "sms-FI": "d.M.yyyy",
            "as-IN": "dd-MM-yyyy",
            "ml-IN": "dd-MM-yy",
            "en-IN": "dd-MM-yyyy",
            "or-IN": "dd-MM-yy",
            "bn-IN": "dd-MM-yy",
            "tk-TM": "dd.MM.yy",
            "bs-Latn-BA": "d.M.yyyy",
            "mt-MT": "dd/MM/yyyy",
            "sr-Cyrl-ME": "d.M.yyyy",
            "se-FI": "d.M.yyyy",
            "zu-ZA": "yyyy/MM/dd",
            "xh-ZA": "yyyy/MM/dd",
            "tn-ZA": "yyyy/MM/dd",
            "hsb-DE": "d. M. yyyy",
            "bs-Cyrl-BA": "d.M.yyyy",
            "tg-Cyrl-TJ": "dd.MM.yy",
            "sr-Latn-BA": "d.M.yyyy",
            "smj-NO": "dd.MM.yyyy",
            "rm-CH": "dd/MM/yyyy",
            "smj-SE": "yyyy-MM-dd",
            "quz-EC": "dd/MM/yyyy",
            "quz-PE": "dd/MM/yyyy",
            "hr-BA": "d.M.yyyy.",
            "sr-Latn-ME": "d.M.yyyy",
            "sma-SE": "yyyy-MM-dd",
            "en-SG": "d/M/yyyy",
            "ug-CN": "yyyy-M-d",
            "sr-Cyrl-BA": "d.M.yyyy",
            "es-US": "M/d/yyyy"
        };
    };
    return AeviLocalization;
}());
var AeviStatusBarHandler = (function () {
    function AeviStatusBarHandler(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.listener();
    }
    AeviStatusBarHandler.prototype.listener = function () {
        var _this = this;
        $(document).on('click', '.aeviStatusBarInfo', function (event) {
            event.preventDefault();
            var modal = new AeviModal(_this.AeviGrid, null);
            modal.setContent({
                text: '<p>' + _this.AeviGrid.AeviLocalization.translate('grid_version') + ': <strong>' + _this.AeviGrid.gridVersion + '</strong></p>' +
                    '<p>' + _this.AeviGrid.AeviLocalization.translate('api_version') + ': <strong>' + _this.AeviGrid.apiVersion + '</strong></p>'
            });
            modal.show(null, null);
        });
        $(document).on('click', '.aeviStatusBarQuest', function (event) {
            event.preventDefault();
            _this.AeviGrid.AeviHelp.show();
        });
        $(document).on('click', '.aeviProtected p, .aeviStatusBarItem', function (event) {
            event.preventDefault();
            $(_this).parent().toggleClass('isOpened');
        });
    };
    return AeviStatusBarHandler;
}());
var AeviStatusBar = (function () {
    function AeviStatusBar(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.render();
    }
    AeviStatusBar.prototype.render = function () {
        var statusBarClass = this.AeviGrid.AeviConsts.statusBarSelector.replace('.', '');
        var statusBarItemClass = this.AeviGrid.AeviConsts.statusBarItemSelector.replace('.', '');
        var statusBarItemHelperClass = this.AeviGrid.AeviConsts.aeviStatusBarHelperSelector.replace('.', '');
        if (!$(this.AeviGrid.AeviConsts.statusBarSelector).length) {
            this.AeviGrid.aeviWrapper.append('<div class="' + statusBarClass + '">' +
                '<p class="' + statusBarItemClass + '"></p>' +
                '<p class="' + statusBarItemHelperClass + '" title="' + this.AeviGrid.AeviLocalization.translate('tooltip_help') + '"></p>' +
                '</div>');
            this.statusBarItem = this.AeviGrid.AeviDOM.getEl(this.AeviGrid.AeviConsts.statusBarItemSelector);
        }
        return new AeviStatusBarHandler(this.AeviGrid);
    };
    AeviStatusBar.prototype.show = function (message, iconType) {
        this.clear();
        if (!_.isNull(message) && message !== 'null') {
            if (_.isUndefined(iconType))
                iconType = 'error';
            this.statusBarItem.classList.add(iconType);
            this.statusBarItem.innerHTML = message;
        }
    };
    AeviStatusBar.prototype.error = function (message) {
        if (aeviIsUndefinedOrNull(message) || message === '')
            message = null;
        this.show(message, 'error');
    };
    AeviStatusBar.prototype.info = function (message) {
        if (aeviIsUndefinedOrNull(message) || message === '')
            message = null;
        this.show(message, 'info');
    };
    AeviStatusBar.prototype.success = function (message) {
        if (aeviIsUndefinedOrNull(message) || message === '')
            message = null;
        this.show(message, 'success');
    };
    AeviStatusBar.prototype.clear = function () {
        this.statusBarItem.classList.remove('error');
        this.statusBarItem.classList.remove('info');
        this.statusBarItem.classList.remove('success');
        this.statusBarItem.innerHTML = '';
    };
    AeviStatusBar.prototype.renderHelpIcon = function () {
        var aeviStatusBarQuestSelectorClass = this.AeviGrid.AeviConsts.aeviStatusBarQuestSelector.makeClassFromSelector();
        $(this.AeviGrid.AeviConsts.aeviStatusBarHelperSelector).append('<i class="' + aeviStatusBarQuestSelectorClass + '"></i>');
    };
    AeviStatusBar.prototype.renderInfoIcon = function () {
        var aeviStatusBarInfoSelectorClass = this.AeviGrid.AeviConsts.aeviStatusBarInfoSelector.makeClassFromSelector();
        $(this.AeviGrid.AeviConsts.aeviStatusBarHelperSelector).append('<i class="' + aeviStatusBarInfoSelectorClass + '"></i>');
    };
    return AeviStatusBar;
}());
var AeviBubbless = (function () {
    function AeviBubbless(aeviGrid, destroyBubbleIfIsRendered) {
        this.AeviGrid = aeviGrid;
        this.isBubbleRendered = false;
        this.bubblePadding = 15 * 2;
        this.bubbleSpaceBetweenCell = 7;
        this.random = Math.random() * 100;
        this.random = parseInt(this.random.toString());
        this.destroyBubbleIfIsRendered = (aeviIsUndefinedOrNull(destroyBubbleIfIsRendered)) ? true : destroyBubbleIfIsRendered;
    }
    AeviBubbless.prototype.render = function (bubbleWrapper) {
        this.bubbleWrapper = bubbleWrapper;
        var HTML = '<span id="aeviBubble-' + this.random + '" class="aeviBubble"><span class="aeviBubble__inner"><span></span></span></span>';
        bubbleWrapper.append(HTML);
        this.setElements();
        this.isBubbleRendered = true;
    };
    AeviBubbless.prototype.setElements = function () {
        this.bubbleElement = $('#aeviBubble-' + this.random);
        this.bubbleSpan = this.bubbleElement.find('.aeviBubble__inner span')[0];
    };
    AeviBubbless.prototype.setText = function (text) {
        this.bubbleSpan.innerHTML = text;
    };
    AeviBubbless.prototype.show = function (bubbleWrapper, text) {
        if (_.isNull(text)) {
            this.destroy();
            return;
        }
        if (this.isBubbleRendered && this.destroyBubbleIfIsRendered) {
            this.destroy();
            this.render(bubbleWrapper);
        }
        this.setText(text);
        this.setStyle();
        this.bubbleElement.addClass('aeviBubble--show');
    };
    AeviBubbless.prototype.setStyle = function () {
        this.bubbleSpan.style.maxWidth = (this.bubbleWrapper.outerWidth() - this.bubblePadding).toString();
        var bubblePosition = -1 * this.bubbleElement.outerHeight() - this.bubbleSpaceBetweenCell;
        if (!this.isInViewport(this.bubbleElement)) {
            this.bubbleElement.addClass('bottom');
            this.bubbleElement.css('bottom', bubblePosition);
        }
        else {
            this.bubbleElement.css('top', bubblePosition);
        }
        if (this.isBubbleWiderThanCell(this.bubbleWrapper)) {
            this.bubbleElement.addClass('wider');
        }
    };
    AeviBubbless.prototype.hide = function () {
        this.bubbleElement.removeClass('aeviBubble--show');
    };
    AeviBubbless.prototype.destroy = function () {
        if (!aeviIsUndefinedOrNull(this.bubbleElement)) {
            this.bubbleElement.detach();
        }
    };
    AeviBubbless.prototype.delete = function () {
        if (!aeviIsUndefinedOrNull(this.bubbleElement)) {
            this.bubbleElement.remove();
        }
    };
    AeviBubbless.prototype.isBubbleWiderThanCell = function (bubbleWrapper) {
        var cellWidth = bubbleWrapper.outerWidth(true);
        var bubbleWidth = this.bubbleElement.find('span').outerWidth(true);
        return bubbleWidth > cellWidth;
    };
    AeviBubbless.prototype.isInViewport = function (bubble) {
        var appEl = $('#' + this.AeviGrid.tableId);
        var saveVerticalZone = bubble[0].offsetHeight + 10;
        var elOffset = bubble.offset();
        var elOffsetTop = elOffset.top - saveVerticalZone;
        var elOffsetBottom = elOffsetTop + bubble[0].offsetHeight;
        var elOffsetLeft = elOffset.left;
        var elOffsetRight = elOffsetLeft + bubble[0].offsetWidth;
        var aeviAppOffset = appEl.offset();
        var aeviAppOffsetTop = aeviAppOffset.top;
        var aeviAppOffsetBottom = aeviAppOffsetTop + appEl[0].offsetHeight;
        var aeviAppOffsetLeft = aeviAppOffset.left;
        var aeviAppOffsetRight = aeviAppOffsetLeft + appEl[0].offsetWidth;
        var isInViewportVertically = (elOffsetTop >= aeviAppOffsetTop && elOffsetBottom <= aeviAppOffsetBottom) ? true : false;
        return isInViewportVertically;
    };
    return AeviBubbless;
}());
var AeviSum = (function () {
    function AeviSum(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.isSumRendered = false;
    }
    AeviSum.prototype.init = function (sums) {
        this.sumsLen = sums.length;
        this.blocks = [];
        for (var i = 0; i < sums.length; i++)
            this.blocks.push(sums[i].BlockNumber);
        this.blocksLen = _.uniq(this.blocks).length;
        this.sums = sums;
        if (!this.isSumRendered)
            this.render();
        else
            this.refresh(sums);
    };
    AeviSum.prototype.render = function () {
        var cols = this.getCols();
        var sum;
        var sumItemTemplate = _.template('<p class=""><%= caption %>: <strong><%= value %> <%= iso %></strong></p>');
        this.sums = _.sortBy(this.sums, function (sum) { return sum.BlockNumber; });
        var sumHtml = [];
        for (var i = 0; i < cols.length; i++) {
            var colNumber = cols[i].colNumber;
            var block = ['<div class="aeviSumCol">'];
            for (var j = 0; j < this.sumsLen; j++) {
                sum = this.sums[j];
                var sumValue = sum.Value;
                if (!aeviIsUndefinedOrNull(sumValue) && !aeviIsUndefinedOrNull(sum.DecimalPlaceCount))
                    sumValue = aeviGetNumberWithDecimalPlaceCount(sumValue, sum.DecimalPlaceCount);
                sumValue = this.AeviGrid.AeviClientSide.getNumberWithLocalizedDecimalPoint(sumValue);
                if (colNumber == sum.BlockNumber) {
                    block.push(sumItemTemplate({
                        caption: sum.Caption,
                        value: sumValue,
                        iso: sum.CurrencyISOCode
                    }));
                }
            }
            block.push('</div>');
            var blockStringify = block.join('');
            sumHtml.push(blockStringify);
        }
        var sumTemplate = _.template('<div class="<%= aeviSumSelector %> cols-<%= colsLength %>"><%= col %></div>');
        sumTemplate = sumTemplate({
            aeviSumSelector: this.AeviGrid.AeviConsts.aeviSumSelector.makeClassFromSelector(),
            col: sumHtml.join(''),
            colsLength: this.blocksLen
        });
        $(sumTemplate).insertAfter('.aeviWrapper');
        this.isSumRendered = true;
    };
    AeviSum.prototype.refresh = function (sums) {
        $(this.AeviGrid.AeviConsts.aeviSumSelector).remove();
        this.isSumRendered = false;
        this.init(sums);
    };
    AeviSum.prototype.getCols = function () {
        var cols = [];
        var colTemplate = _.template('<div class="<%= aeviSumColSelector %>" data-blockNumber="<%= colNumber %>"></div>');
        var aeviSumColSelector = this.AeviGrid.AeviConsts.aeviSumColSelector.makeClassFromSelector();
        for (var i = 0; i < this.blocksLen; i++) {
            cols.push({
                colNumber: this.blocks[i],
                html: colTemplate({
                    aeviSumColSelector: aeviSumColSelector,
                    colNumber: this.blocks[i]
                })
            });
        }
        return cols;
    };
    return AeviSum;
}());
var AeviPluginApi = (function () {
    function AeviPluginApi(aeviGrid) {
        this.AeviGrid = aeviGrid;
    }
    AeviPluginApi.prototype.callPluginsOnRowsChanged = function (rows) {
        try {
            if (!aeviIsUndefinedOrNull(this.AeviGrid.plugins)) {
                for (var x = 0; x < this.AeviGrid.plugins.length; x++) {
                    this.AeviGrid.plugins[x].onRowsChanged(rows);
                }
            }
        }
        catch (ex) {
            console.log(ex.message);
        }
    };
    AeviPluginApi.prototype.callPluginsOnRowsDeleting = function (rows) {
        try {
            if (!aeviIsUndefinedOrNull(this.AeviGrid.plugins)) {
                for (var x = 0; x < this.AeviGrid.plugins.length; x++) {
                    this.AeviGrid.plugins[x].onRowsDeleting(rows);
                }
            }
        }
        catch (ex) {
            console.log(ex.message);
        }
    };
    AeviPluginApi.prototype.callPluginsOnDataLoaded = function () {
        try {
            if (!aeviIsUndefinedOrNull(this.AeviGrid.plugins)) {
                for (var x = 0; x < this.AeviGrid.plugins.length; x++)
                    this.AeviGrid.plugins[x].onDataLoaded();
            }
        }
        catch (ex) {
            this.AeviGrid.print('AeviGridHandler.callPluginsOnDataLoaded()' + ', ' + ex.message);
        }
    };
    AeviPluginApi.prototype.callPluginsOnCellLeave = function ($cell, enterValue, leaveValue) {
        var cellIndexes = this.AeviGrid.AeviDOM.getCellIndexes($cell);
        try {
            if (!aeviIsUndefinedOrNull(this.AeviGrid.plugins)) {
                for (var x = 0; x < this.AeviGrid.plugins.length; x++) {
                    this.AeviGrid.plugins[x].onCellLeave(cellIndexes.rowId, cellIndexes.cellId, enterValue, leaveValue);
                }
            }
        }
        catch (ex) {
            console.log(ex.message);
        }
    };
    return AeviPluginApi;
}());
var AeviLockMessage = (function () {
    function AeviLockMessage(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.lockMessageShowed = false;
        this.isModalOpened = false;
        this.maximumViews = 3;
        this.views = 0;
    }
    AeviLockMessage.prototype.show = function () {
        if (this.views >= this.maximumViews)
            return;
        var modal = new AeviModal(this.AeviGrid);
        modal.setContent({
            title: this.AeviGrid.AeviLocalization.translate('protected_mode_title'),
            text: '<p>' + this.AeviGrid.AeviLocalization.translate('protected_mode') + '</p>',
            buttons: [
                'aeviProtectedUnlock',
                'simpleModalClose'
            ]
        });
        modal.show();
        modal.focus('last');
        this.increaseViews();
    };
    AeviLockMessage.prototype.isLockMessageShowed = function () {
        if (aeviIsUndefinedOrNull(this.lockMessageShowed) || !this.lockMessageShowed)
            return false;
        else
            return true;
    };
    AeviLockMessage.prototype.increaseViews = function () {
        this.views++;
    };
    return AeviLockMessage;
}());
var AeviGrid = (function () {
    function AeviGrid(settings) {
        this.blockMode = false;
        this.firstLoad = true;
        this.initializeParameters(settings);
        this.initializeEnvironment({
            debugger: true,
            gridVersion: '1.0'
        });
        this.setGridTheme();
        this.setGridModes();
        new AeviStorage();
        this.AeviDOM = new AeviDOM(this);
        this.AeviClientSide = new AeviClientSide(this);
        this.AeviConsts = new AeviConsts(this, this.tableId);
        this.AeviLocalization = new AeviLocalization(this.apiLang, settings, this.AeviConsts, this.AeviClientSide);
        this.AeviHelp = new AeviHelp(this);
        AeviGrid.initializeToastr();
        this.render();
        this.AeviAjaxPreloader = new AeviAjaxPreloader(this);
        this.scrollEl = $('#' + this.tableId).find('.table-body');
        this.AeviStatusBar = new AeviStatusBar(this);
        this.AeviBubbless = new AeviBubbless(this);
        this.AeviSum = new AeviSum(this);
        this.AeviApiService = new AeviApiService(this, this.apiAddress, this.apiUsername, this.apiPassword, this.apiSlot, this.apiEntity, this.apiLang, this.apiToken, this.entityType);
        this.AeviDataRepository = new AeviDataRepository(this.AeviConsts, this.AeviLocalization, this);
        this.AeviDownloader = new AeviDownloader(this);
        this.AeviGridApi = new AeviGridApi(this, this.AeviDataRepository);
        this.initializePlugins();
        this.AeviDataService = new AeviDataService(this, this.AeviApiService, this.AeviDataRepository);
        this.AeviGridEditor = new AeviGridEditor(this, this.AeviDataService, this.tableId);
        this.AeviGridHandler = new AeviGridHandler(this);
        AeviImage.init(this);
    }
    AeviGrid.prototype.render = function () {
        var app = document.getElementById(this.tableId);
        this.aeviTableId = 'aeviTable';
        app.classList.add('inner-container');
        var tableHeadHTML = '<div class="table-head"></div><div class="table-body"><table id="' + this.aeviTableId + '" class="' + this.aeviTableId + '"></table></div>';
        this.AeviDOM.append(app, tableHeadHTML);
        $(app).wrap('<div id="' + this.tableId + '-wrapper' + '" class="aeviWrapper"></div>');
        this.aeviWrapper = $(document.getElementById(this.tableId + '-wrapper'));
        $(app).wrap('<div class="outer-container"></div>');
    };
    AeviGrid.prototype.setGridModes = function () {
        this.mode = { clipboard: 0 };
    };
    AeviGrid.prototype.setGridTheme = function () {
        document.querySelector('body').classList.add('aeviTheme--' + this.theme);
    };
    AeviGrid.initializeToastr = function () {
        toastr.options = {
            closeButton: true,
            positionClass: 'toast-top-right',
            timeOut: 8000,
            showEasing: 'swing',
            hideEasing: 'linear',
            hideDuration: 300
        };
    };
    AeviGrid.prototype.initializeEnvironment = function (settings) {
        this.debugger = settings.debugger;
        if (!this.debugger) {
            console.log = function () { };
        }
        this.gridVersion = settings.gridVersion;
        this.apiVersion = settings.apiVersion;
    };
    AeviGrid.prototype.initializeParameters = function (settings) {
        var requiredParameters = {
            tableId: { required: true, value: '' },
            apiAddress: { required: true, value: '' },
            apiUsername: { required: false, value: '' },
            apiPassword: { required: false, value: '' },
            apiSlot: { required: false, value: '' },
            apiEntity: { required: true, value: '' },
            apiLang: { required: false, value: '' },
            apiToken: { required: false, value: '' },
            entityType: { required: true, value: 0 },
            theme: { required: false, value: 0 },
            pluginsNames: { required: false, value: [] },
            excelFilePath: { required: false, value: [] },
            antiForgeryToken: { required: true, value: '' },
            roles: { required: true, value: {} }
        };
        var stateIndex;
        for (var index in settings) {
            for (stateIndex in requiredParameters) {
                if (stateIndex === index) {
                    this[stateIndex] = settings[index];
                }
            }
        }
    };
    AeviGrid.prototype.initializePlugins = function () {
        this.AeviPluginApi = new AeviPluginApi(this);
        if (aeviIsUndefinedOrNull(this.pluginsNames) || _.isEmpty(this.pluginsNames))
            this.pluginsNames = [];
        if (!_.contains(this.pluginsNames, 'DynamicEnum'))
            this.pluginsNames.push('DynamicEnum');
        this.plugins = [];
        var pluginFactory = new AeviPluginFactory(this.AeviGridApi);
        for (var i = 0; i < this.pluginsNames.length; i++) {
            var pluginName = this.pluginsNames[i];
            var plugin = pluginFactory.get(pluginName);
            if (!aeviIsUndefinedOrNull(plugin)) {
                this.plugins.push(plugin);
            }
        }
    };
    AeviGrid.prototype.print = function (message) {
        if (this.debugger) {
            console.log('%cAeviErrorLog: ' + message, 'color: #ed1c24; font-size: 12px');
        }
    };
    AeviGrid.prototype.log = function (message) {
        if (this.debugger) {
            console.log('%cAeviLog: ' + message, 'color: #008C25; font-size: 12px');
        }
    };
    AeviGrid.prototype.setSizes = function () {
        if (_.isUndefined(this.AeviDimensions))
            this.AeviDimensions = new AeviDimensions(this);
        else
            this.AeviDimensions.getInitialDimensions();
    };
    AeviGrid.prototype.selectFirstCell = function () {
        var props = {
            rowId: 0,
            cellId: 0,
            scrollLeft: true,
            click: false
        };
        this.AeviPager.moveTo(props);
        var columnIndex = 0;
        var isVisible = false;
        do {
            columnIndex++;
            var header = this.AeviDataRepository.AeviTableDescription.getColumnHeader(columnIndex);
            isVisible = this.AeviDataRepository.AeviTableDescription.isVisible(header);
        } while (!isVisible);
        props.cellId = columnIndex;
        props.click = true;
        this.AeviPager.moveTo(props);
    };
    AeviGrid.prototype.select = function (cell) {
        this.unSelectAllRowsAndCells();
        this.addSelectedCellClasses(cell);
        this.checkRowTinge();
    };
    AeviGrid.prototype.unSelectAllRowsAndCells = function () {
        var elems = this.AeviDOM.getSelectedRowsAndCells();
        for (var i = 0; i < elems.length; i++) {
            for (var j = 0; j < this.AeviConsts.selectedCellClassesToRemove.length; j++) {
                AeviDOM.removeClass(elems[i], this.AeviConsts.selectedCellClassesToRemove[j]);
            }
        }
    };
    AeviGrid.prototype.addSelectedCellClasses = function (cell) {
        for (var i = 0; i < this.AeviConsts.selectedCellClasses.length; i++) {
            AeviDOM.addClass(cell, this.AeviConsts.selectedCellClasses[i]);
        }
    };
    AeviGrid.prototype.checkRowTinge = function () {
        if (this.AeviDataRepository.isMoreCellsSelected())
            return;
        var selectedCellPosition = this.AeviDataRepository.getSelectedCellPosition();
        if (_.isNull(selectedCellPosition))
            return;
        var row = this.AeviDOM.getPureRow(selectedCellPosition.rowIndex);
        if (!_.isNull(row))
            AeviDOM.addClass(row, 'tinge');
    };
    AeviGrid.prototype.multiSelect = function (fromCellIndex, toCellIndex) {
        var upToDown = (fromCellIndex.rowIndex < toCellIndex.rowIndex);
        var helper = null;
        if (!upToDown) {
            helper = fromCellIndex.rowIndex;
            fromCellIndex.rowIndex = toCellIndex.rowIndex;
            toCellIndex.rowIndex = helper;
        }
        var leftToRight = (fromCellIndex.cellIndex < toCellIndex.cellIndex);
        if (!leftToRight) {
            helper = fromCellIndex.cellIndex;
            fromCellIndex.cellIndex = toCellIndex.cellIndex;
            toCellIndex.cellIndex = helper;
        }
        this.AeviDataService.AeviDataRepository.AeviTableData.setSelectedPositions(fromCellIndex, toCellIndex);
        this.AeviFakeEditor.setContent();
        this.AeviFakeEditor.focus();
        if (JSON.stringify(fromCellIndex) === JSON.stringify(toCellIndex))
            return;
        var table = document.getElementById(this.tableId);
        var tableBody = table.querySelector('table tbody');
        this.unSelectAllRowsAndCells();
        for (var j = fromCellIndex.rowIndex; j <= toCellIndex.rowIndex; j++) {
            for (var i = fromCellIndex.cellIndex; i <= toCellIndex.cellIndex; i++) {
                if (j < 200) {
                    var row = tableBody.rows[j];
                    row.cells[i].classList.add('selected');
                }
            }
        }
        this.AeviGridEditor.showBorder(fromCellIndex, toCellIndex);
    };
    AeviGrid.prototype.showLockMessage = function () {
        if (!aeviIsUndefinedOrNull(this.AeviSearch) && this.AeviSearch.isSearchVisible)
            return;
        if (!aeviIsUndefinedOrNull(this.lockMessageShowed) && this.lockMessageShowed)
            return;
        if (aeviIsUndefinedOrNull(this.AeviLockMessage))
            this.AeviLockMessage = new AeviLockMessage(this);
        if (!this.AeviDOM.isModalVisible())
            this.AeviLockMessage.show();
    };
    AeviGrid.prototype.isLockMessageShowed = function () {
        if (aeviIsUndefinedOrNull(this.AeviLockMessage))
            return false;
        else
            return this.AeviLockMessage.isLockMessageShowed();
    };
    AeviGrid.prototype.showMaxRowCountMessage = function () {
        var message = this.AeviLocalization.translate('max_row_count_1');
        message += this.AeviDataService.AeviDataRepository.maxRowCount;
        message += this.AeviLocalization.translate('max_row_count_2');
        var modal = new AeviModal(this);
        modal.setContent({
            title: this.AeviLocalization.translate('warning'),
            text: '<p>' + message + '</p>'
        });
        modal.show();
    };
    AeviGrid.prototype.selectRow = function (rowIndexes) {
        this.unSelectAllRowsAndCells();
        this.AeviDataRepository.AeviTableData.setSelectedRows(rowIndexes, false);
        this.isAllRowsSelected = this.AeviDataRepository.AeviTableData.getVisibleRecordsLength() === rowIndexes.length;
        for (var i = 0; i < rowIndexes.length; i++) {
            var row = this.AeviDOM.getPureRow(rowIndexes[i]);
            if (!aeviIsUndefinedOrNull(row)) {
                row.classList.add('selected');
                var rowNumber = row.querySelector('.aeviRowNumber');
                var rowCells = row.querySelectorAll('.aeviCell');
                rowNumber.classList.add('selected');
                for (var j = 0; j < rowCells.length; j++)
                    rowCells[j].classList.add('selected');
            }
        }
    };
    AeviGrid.prototype.selectColumn = function (columnIndexes, selectAllCells) {
        this.unSelectAllRowsAndCells();
        if (selectAllCells === true)
            this.AeviDataRepository.AeviTableData.setSelectedRows([0], selectAllCells);
        else
            this.AeviDataRepository.AeviTableData.setSelectedColumns(columnIndexes);
        this.AeviGridEditor.refreshData();
    };
    return AeviGrid;
}());
var AeviGridLocker = (function () {
    function AeviGridLocker(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.AeviLocalization = this.AeviGrid.AeviLocalization;
        this.lockButton = $('#' + this.AeviGrid.tableId + '-lock');
        this.protectedMessageElement = null;
    }
    AeviGridLocker.prototype.lock = function () {
        if (!this.AeviGrid.AeviDataService.AeviUser.roles.Write) {
            this.AeviGrid.setSizes();
            this.isLocked = true;
            this.AeviGrid.AeviToolbar.refresh();
            return;
        }
        if (this.AeviGrid.firstLoad)
            this.realReadOnly = this.AeviGrid.AeviDataRepository.AeviTableDescription.readOnly;
        if (!this.realReadOnly && !this.AeviGrid.firstLoad)
            toastr.info(this.AeviLocalization.translate('applocked'));
        var message = this.AeviLocalization.translate('readonly_message');
        if (this.AeviGrid.firstLoad) {
            if (this.realReadOnly) {
                message = this.AeviLocalization.translate('first_load_readonly_message');
                toastr.info(this.AeviLocalization.translate('first_load_readonly_message'));
            }
            else {
                if (aeviIsUndefinedOrNull(this.protectedMessageElement)) {
                    this.renderProtectedView('<div class="aeviProtected"><p>' + this.AeviLocalization.translate('protected_mode') + '</p><span class="aeviProtected__unlock">' + this.AeviLocalization.translate('enable_editing') + '</span></div>');
                }
                toastr.info(this.AeviLocalization.translate('readonly_message'));
            }
        }
        this.AeviGrid.AeviStatusBar.show(message, 'info');
        this.AeviGrid.setSizes();
        this.isLocked = true;
        this.AeviGrid.AeviToolbar.refresh();
    };
    AeviGridLocker.prototype.unLock = function () {
        var _this = this;
        this.AeviGrid.AeviDataService.getBlockStateCommand()
            .done(function (response) {
            if (response === MyRetail.IAeviBlockStateCommand.CONTINUE) {
                _this.unLockWithoutAccessControl();
            }
        })
            .fail(function (response) {
            if (response === MyRetail.IAeviBlockStateCommand.RELOAD) {
                _this.AeviGrid.AeviDataService.getDataByQuery('', true);
                _this.unLockWithoutAccessControl();
            }
        });
    };
    AeviGridLocker.prototype.unLockWithoutAccessControl = function () {
        toastr.info(this.AeviLocalization.translate('appunlocked'));
        this.AeviGrid.AeviStatusBar.show(this.AeviLocalization.translate('appunlocked'), 'info');
        this.AeviGrid.AeviGridHandler.triggerClick();
        this.removeProtectedView();
        this.AeviGrid.setSizes();
        this.isLocked = false;
        this.AeviGrid.AeviToolbar.refresh();
    };
    AeviGridLocker.prototype.renderProtectedView = function (html) {
        if (!this.AeviGrid.AeviDataService.AeviUser.roles.Write) {
            return;
        }
        this.AeviGrid.aeviWrapper.append(html);
        this.protectedMessageElement = $('.aeviProtected');
        this.protectedViewListener();
    };
    AeviGridLocker.prototype.removeProtectedView = function () {
        if (this.AeviGrid.AeviDOM.length('.aeviProtected'))
            this.protectedMessageElement.remove();
        this.AeviGrid.lockMessageShowed = true;
    };
    AeviGridLocker.prototype.protectedViewListener = function () {
        var _this = this;
        $(document).off('click', '.aeviProtected__unlock').on('click', '.aeviProtected__unlock', function (event) {
            event.preventDefault();
            _this.unLock();
            _this.AeviGrid.AeviDOM.closeModals();
        });
        $(document).off('click', '.aeviProtected__save').on('click', '.aeviProtected__save', function (event) {
            event.preventDefault();
            _this.AeviGrid.AeviToolbar.AeviToolbarHandler.save();
        });
    };
    return AeviGridLocker;
}());
var AeviPagerHandler = (function () {
    function AeviPagerHandler(aeviPager, aeviGrid) {
        var _this = this;
        this.AeviGrid = aeviGrid;
        this.AeviPager = aeviPager;
        this.aeviConsts = this.AeviGrid.AeviConsts;
        this.canScrollUp = false;
        this.aeviScrollElement = this.AeviPager.aeviAppElement.find(this.aeviConsts.tableBodySelector);
        this.table = this.aeviScrollElement.find('table');
        this.lastScrollTop = 0;
        this.lastScrollLeft = 0;
        this.setInitialValues();
        this.listener();
    }
    AeviPagerHandler.prototype.setInitialValues = function () {
        this.heightOfRow = this.AeviPager.rowHeight;
        this.tollerance = this.AeviPager.tollerance;
        this.numberOfNewVisibleRows = this.AeviPager.numberOfNewVisibleRows;
        this.countOfNewRows = parseInt(this.aeviConsts.pager.countOfNewRows);
        this.setJumps();
    };
    AeviPagerHandler.prototype.getScrollContainerOffset = function (element) {
        var xPosition = 0;
        var yPosition = 0;
        while (element) {
            xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
            yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
            element = element.offsetParent;
        }
        return yPosition;
    };
    AeviPagerHandler.prototype.scroll = function (aeviScrollEl, event) {
        if (this.AeviGrid.AeviDataService.AeviDataRepository.getVisibleRecordsLength() < this.numberOfNewVisibleRows)
            return false;
        var scrollLeft = aeviScrollEl.scrollLeft();
        if (this.lastScrollLeft !== scrollLeft) {
            this.lastScrollLeft = scrollLeft;
            return;
        }
        var scrollTop = aeviScrollEl.scrollTop();
        var direction = (scrollTop > this.lastScrollTop) ? 'down' : 'up';
        this.lastScrollTop = scrollTop;
        var scrollContainerOffset = this.getScrollContainerOffset(document.getElementById('aeviScrollDiv'));
        var closestIndex = null;
        if (direction == 'down') {
            if (scrollContainerOffset < this.toTolleranceOffset) {
                closestIndex = this.getClosestIndex(scrollContainerOffset);
                if (closestIndex > this.offsetJumps.length)
                    closestIndex = this.offsetJumps.length;
                this.pageData(this.offsetJumps[closestIndex].jump, this.offsetJumps[closestIndex].jump + this.numberOfNewVisibleRows);
            }
        }
        else {
            if (scrollContainerOffset > this.fromTolleranceOffset) {
                closestIndex = this.getClosestIndex(scrollContainerOffset);
                if (closestIndex < 0)
                    closestIndex = 0;
                this.pageData(this.offsetJumps[closestIndex].jump, this.offsetJumps[closestIndex].jump + this.numberOfNewVisibleRows);
            }
        }
    };
    AeviPagerHandler.prototype.getClosestIndex = function (scrollContainerOffset) {
        return findWithAttr(this.offsetJumps, 'offset', this.offsets.getClosest(scrollContainerOffset)) - 2;
    };
    AeviPagerHandler.prototype.pageData = function (from, to) {
        if (this.from === from || this.to === to)
            return false;
        this.from = from;
        this.to = to;
        this.setTableTop(this.from * this.heightOfRow);
        this.setRange({ from: this.from, to: this.to });
        this.AeviGrid.AeviGridEditor.refreshData({ From: this.from, To: this.to });
    };
    AeviPagerHandler.prototype.setTableTop = function (value) {
        this.table[0].style.top = value + 'px';
        this.tableTop = value;
    };
    AeviPagerHandler.prototype.setRange = function (range) {
        this.from = range.from;
        this.to = range.to;
        this.canScrollUp = (this.from === 0);
        var fromJumpValue = this.from + this.tollerance;
        var fromJumpId = findWithAttr(this.offsetJumps, 'jump', fromJumpValue);
        this.fromTolleranceOffset = this.offsetJumps[fromJumpId].offset;
        var toJumpValue = this.to - this.tollerance;
        var toJumpId = findWithAttr(this.offsetJumps, 'jump', toJumpValue);
        if (_.isUndefined(toJumpId))
            toJumpId = fromJumpId;
        this.toTolleranceOffset = this.offsetJumps[toJumpId].offset;
    };
    AeviPagerHandler.prototype.getRange = function () {
        return {
            from: this.from,
            to: this.to
        };
    };
    AeviPagerHandler.prototype.setJumps = function () {
        var visibleRecordsLength = parseInt(this.AeviGrid.AeviDataService.AeviDataRepository.getVisibleRecordsLength());
        var countOfJumps = (visibleRecordsLength / this.countOfNewRows) + 3;
        this.jumps = [];
        var jump = 0;
        for (var i = 0; i < countOfJumps; i++) {
            this.jumps.push(jump);
            jump += this.countOfNewRows;
        }
        this.offsetJumps = [];
        var tableBodyHeight = this.AeviGrid.AeviDimensions.privateDimensions.table_body.height;
        var tableBodyOffsetTop = this.aeviScrollElement.offset().top;
        for (i = 0; i < this.jumps.length; i++) {
            this.offsetJumps.push({
                jump: this.jumps[i],
                offset: ((this.heightOfRow * this.jumps[i]) - tableBodyHeight - tableBodyOffsetTop) * -1
            });
        }
        this.offsets = [];
        for (i = 0; i < this.offsetJumps.length; i++) {
            this.offsets.push(this.offsetJumps[i].offset);
        }
        this.AeviPager.setScrollContainerHeight(visibleRecordsLength * this.heightOfRow);
    };
    AeviPagerHandler.prototype.getLastTolleranceRow = function () {
        var rowIndex = this.to - this.tollerance;
        var rowNode = this.AeviGrid.AeviDOM.getRow(rowIndex);
        var rowOffsetTop = (rowNode.length) ? rowNode.offset().top : null;
        return {
            rowIndex: rowIndex,
            rowNode: rowNode,
            rowOffsetTop: rowOffsetTop
        };
    };
    AeviPagerHandler.prototype.getFirstTolleranceRow = function () {
        var rowIndex = this.from + this.tollerance;
        var rowNode = this.AeviGrid.AeviDOM.getRow(rowIndex);
        var rowOffsetTop = (rowNode.length) ? rowNode.offset().top : null;
        return {
            rowIndex: rowIndex,
            rowNode: rowNode,
            rowOffsetTop: rowOffsetTop
        };
    };
    AeviPagerHandler.prototype.listener = function () {
        var _this = this;
        this.aeviScrollElement.on('scroll', function (event) {
            _this.scroll($(event.currentTarget), event);
            _this.AeviPager.setVisibleContentInfo();
        });
        $(document).on('click', this.aeviConsts.arrowSelector, function (event) {
            event.preventDefault();
            var arrow = event.currentTarget;
            var arrowClass = _this.aeviConsts.arrowSelector.makeClassFromSelector();
            var arrowDirection = arrow.getAttribute('class').replace(arrowClass, '').replace(/ /g, '');
            var props = {};
            switch (arrowDirection) {
                case 'right':
                    props.rowId = _this.AeviPager.arrowSteps.nextRightStep.rowIndex;
                    props.cellId = _this.AeviPager.arrowSteps.nextRightStep.cellIndex;
                    props.scrollLeft = true;
                    break;
                case 'left':
                    props.rowId = _this.AeviPager.arrowSteps.prevLeftStep.rowIndex;
                    props.cellId = _this.AeviPager.arrowSteps.prevLeftStep.cellIndex;
                    props.scrollLeft = true;
                    break;
            }
            _this.AeviPager.moveTo(props);
        });
    };
    return AeviPagerHandler;
}());
var AeviPager = (function () {
    function AeviPager(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.aeviConsts = this.AeviGrid.AeviConsts;
        this.showScrollArrows = true;
        this.aeviAppElement = $('#' + this.AeviGrid.tableId);
        this.rowHeight = this.aeviConsts.pager.rowHeight;
        this.tollerance = this.aeviConsts.pager.tollerance;
        this.numberOfNewVisibleRows = this.aeviConsts.pager.visibleRows;
        this.recordsLength = this.AeviGrid.AeviDataService.AeviDataRepository.getVisibleRecordsLength();
        this.scrollContainer = this.renderScrollContainer(this.recordsLength * this.rowHeight);
        this.AeviPagerHandler = new AeviPagerHandler(this, this.AeviGrid);
        this.setTableHeadScrollable();
    }
    AeviPager.prototype.render = function () {
        this.rowHeight = this.aeviConsts.pager.rowHeight;
        this.tollerance = this.aeviConsts.pager.tollerance;
        this.numberOfNewVisibleRows = this.aeviConsts.pager.visibleRows;
        this.AeviPagerHandler.setInitialValues();
        var from = 0;
        var to = this.numberOfNewVisibleRows;
        var data = this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableData.getRecords(from, to);
        this.AeviGrid.AeviGridEditor.renderTableBody(data);
        this.AeviPagerHandler.setRange({ from: from, to: to });
        this.AeviPagerHandler.setJumps();
        if (this.showScrollArrows && data.length > 0)
            this.renderArrows();
    };
    AeviPager.prototype.refresh = function () {
        this.AeviGrid.AeviGridEditor.deleteTableBody();
        this.render();
    };
    AeviPager.prototype.renderScrollContainer = function (height) {
        var width = 'auto';
        if (height === 0 || height === this.rowHeight)
            width = this.aeviAppElement.find('table').css('width');
        var heightInPx = height + 'px';
        var html = '<div id="aeviScrollDiv" class="aeviScrollDiv" style="width: ' + width + '; height: ' + heightInPx + '"></div>';
        this.aeviAppElement.find(this.aeviConsts.tableBodySelector).append(html);
        return this.aeviAppElement.find('.aeviScrollDiv');
    };
    AeviPager.prototype.refreshScrollContainer = function () {
        this.scrollContainer.css({
            'width': this.aeviAppElement.find('table').css('width'),
            'height': this.recordsLength * this.rowHeight
        });
    };
    AeviPager.prototype.setScrollContainerHeight = function (height) {
        this.scrollContainer[0].style.height = height + 'px';
    };
    AeviPager.prototype.setTableHeadScrollable = function () {
        var _this = this;
        $(this.aeviConsts.tableBodySelector).scroll(function (event) {
            var scroll = event.currentTarget;
            var bounding = scroll.getBoundingClientRect();
            var scrollDiff = bounding.left;
            var position = -1 * scroll.scrollLeft + scrollDiff;
            $(_this.aeviConsts.tableHeadSelector).offset({
                left: position,
                top: null
            });
        });
    };
    AeviPager.prototype.renderArrows = function () {
        var aeviWrapper = $(this.aeviConsts.aeviWrapperSelector);
        var arrowClass = this.aeviConsts.arrowSelector.makeClassFromSelector();
        if (this.AeviGrid.AeviDOM.length(this.aeviConsts.arrowSelector))
            return;
        aeviWrapper.append('<i class="' + arrowClass + ' right"></i>' +
            '<i class="' + arrowClass + ' left"></i>');
        this.setArrowPositions();
    };
    AeviPager.prototype.setArrowPositions = function () {
        var arrowOffset = -(this.aeviConsts.arrowSize) + 'px';
        if (window.innerWidth < 800)
            arrowOffset = -50 + 'px';
        if (window.innerWidth < 600)
            arrowOffset = -40 + 'px';
        $(this.aeviConsts.arrowSelector + '.top').css({
            'bottom': arrowOffset,
            'right': '46%',
            'left': 'auto'
        });
        $(this.aeviConsts.arrowSelector + '.right').css({
            'top': '52%',
            'right': arrowOffset,
            'left': 'auto'
        });
        $(this.aeviConsts.arrowSelector + '.bottom').css({
            'bottom': arrowOffset,
            'right': 'auto',
            'left': '46%'
        });
        $(this.aeviConsts.arrowSelector + '.left').css({
            'top': '52%',
            'right': 'auto',
            'left': arrowOffset
        });
    };
    AeviPager.prototype.moveTo = function (props) {
        var rowIndex = (_.isUndefined(props.rowId)) ? null : props.rowId;
        rowIndex = (_.isUndefined(props.guid) ? rowIndex : this.AeviGrid.AeviDataService.AeviDataRepository.getRecord(props.guid)[0][0]);
        var target = (_.isUndefined(props.cellId)) ? document.getElementById('row-' + rowIndex) : document.getElementById('cell-' + rowIndex + '.' + props.cellId);
        var currentRange = this.AeviPagerHandler.getRange();
        if (rowIndex !== 0 && (rowIndex >= currentRange.from && rowIndex <= currentRange.to)) {
            this.AeviGrid.AeviGridEditor.refreshData({ From: currentRange.from, To: currentRange.to });
        }
        else {
            var closestIndex = this.AeviPagerHandler.jumps.getClosestIndex(rowIndex);
            var fromIndex = (closestIndex === 0) ? closestIndex : closestIndex - 2;
            var from = this.AeviPagerHandler.jumps[fromIndex];
            var to = this.AeviPagerHandler.jumps[fromIndex + 4];
            to = (_.isUndefined(to)) ? this.AeviPagerHandler.jumps[this.AeviPagerHandler.jumps.length - 1] : to;
            this.AeviPagerHandler.pageData(from, to);
        }
        if (_.isNull(target))
            target = (_.isUndefined(props.cellId)) ? document.getElementById('row-' + rowIndex) : document.getElementById('cell-' + rowIndex + '.' + props.cellId);
        target = $(target);
        if (props.click) {
            this.AeviGrid.AeviGridHandler.cellChange(target);
        }
        else {
            if (props.scrollLeft) {
                var offsetLeft = parseInt(target[0].offsetLeft);
                this.aeviAppElement.find(this.aeviConsts.tableBodySelector).animate({
                    scrollLeft: offsetLeft
                }, 0);
            }
            else {
                var tableTop = this.AeviPagerHandler.tableTop;
                if (_.isUndefined(tableTop))
                    tableTop = 0;
                var numTableTop = parseInt(tableTop);
                var numOffsetTop = parseInt(target[0].offsetTop);
                var offsetTop = numTableTop + numOffsetTop - 50;
                this.aeviAppElement.find(this.aeviConsts.tableBodySelector).animate({
                    scrollTop: offsetTop
                }, 0);
            }
        }
    };
    AeviPager.prototype.getVisibleCellIndexes = function () {
        var table = document.getElementById(this.AeviGrid.tableId);
        var rows = table.querySelectorAll('.aeviRow.render, .aeviRow.newrow');
        var rowIndexesInViewport = [];
        for (var i = 0; i < rows.length; i++) {
            var rowIndex = this.AeviGrid.AeviDOM.getRowIndex(rows[i]);
            if (AeviGlobal.isElementInBodyViewportVertically(rows[i], this.aeviAppElement[0])) {
                rowIndexesInViewport.push(rowIndex);
            }
        }
        var firstRowIndexInViewport = rowIndexesInViewport[0];
        var row = this.AeviGrid.AeviDOM.getPureRow(firstRowIndexInViewport);
        if (_.isNull(row))
            return null;
        var cells = row.querySelectorAll('td');
        var cellIndexesInViewport = [];
        for (var i = 0; i < cells.length; i++) {
            var cellIndex = this.AeviGrid.AeviDOM.getCellIndexes(cells[i]).cellIndex;
            if (AeviGlobal.isElementInBodyViewportHorizontally(cells[i], this.aeviAppElement[0])) {
                cellIndexesInViewport.push(cellIndex);
            }
        }
        var firstVisibleCellIndex = { rowIndex: rowIndexesInViewport[0], cellIndex: cellIndexesInViewport[0] };
        var lastVisibleCellIndex = { rowIndex: rowIndexesInViewport[rowIndexesInViewport.length - 1], cellIndex: cellIndexesInViewport[cellIndexesInViewport.length - 1] };
        return {
            firstVisibleCellIndex: firstVisibleCellIndex,
            lastVisibleCellIndex: lastVisibleCellIndex,
            visibleRowsLength: rowIndexesInViewport.length,
            visibleColumnsLength: cellIndexesInViewport.length
        };
    };
    AeviPager.prototype.setVisibleContentInfo = function () {
        var visibleRowsLength = this.AeviGrid.AeviDataRepository.getVisibleRecordsLength();
        if (visibleRowsLength <= 0 && this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport())
            return;
        var visibleCells = this.getVisibleCellIndexes();
        if (_.isNull(visibleCells))
            return;
        var firstCellIndexes = visibleCells.firstVisibleCellIndex;
        var lastCellIndexes = visibleCells.lastVisibleCellIndex;
        var visibleCols = visibleCells.visibleColumnsLength;
        this.enableArrows(firstCellIndexes, lastCellIndexes);
        var nextHorizontalStep = _.clone(lastCellIndexes);
        nextHorizontalStep.cellIndex = nextHorizontalStep.cellIndex + 1;
        var prevHorizontalStep = _.clone(firstCellIndexes);
        prevHorizontalStep.cellIndex = prevHorizontalStep.cellIndex - visibleCols + 1;
        if (prevHorizontalStep.cellIndex < 0)
            prevHorizontalStep.cellIndex = 0;
        this.arrowSteps = {
            prevLeftStep: prevHorizontalStep,
            nextRightStep: nextHorizontalStep
        };
    };
    AeviPager.prototype.enableArrows = function (firstCellIndexes, lastCellIndexes) {
        if (lastCellIndexes.cellIndex === this.AeviGrid.AeviDataRepository.AeviTableDescription.getVisibleCellsLength() - 1)
            this.hideArrow('right');
        else
            this.showArrow('right');
        if (firstCellIndexes.cellIndex <= 1)
            this.hideArrow('left');
        else
            this.showArrow('left');
    };
    AeviPager.prototype.hideArrow = function (className) {
        this.changeArrow(className, 'none');
    };
    AeviPager.prototype.showArrow = function (className) {
        this.changeArrow(className, 'block');
    };
    AeviPager.prototype.changeArrow = function (className, action) {
        var arrow = document.querySelector('.' + className);
        if (aeviIsUndefinedOrNull(arrow)) {
            this.renderArrows();
            this.setVisibleContentInfo();
            return;
        }
        arrow.style.display = action;
    };
    return AeviPager;
}());
var IAeviBlockStateCommand = MyRetail.IAeviBlockStateCommand;
var AeviToolbarHandler = (function () {
    function AeviToolbarHandler(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.AeviLocalization = this.AeviGrid.AeviLocalization;
    }
    AeviToolbarHandler.prototype.save = function () {
        var _this = this;
        this.AeviGrid.AeviDataService.AeviApiService.getDataEntityStatus()
            .done(function (response) {
            if (response.Status === MyRetail.IAeviResponseStatus.Blocked) {
                _this.AeviGrid.AeviDataService.showBlockModal(response)
                    .done(function () {
                    _this.AeviGrid.AeviDataService.AeviApiService.setDataEntityBlockStatus().done(function () {
                        _this.callCommit();
                    });
                })
                    .fail(function () {
                    _this.AeviGrid.AeviGridLocker.lock();
                });
            }
            else if (response.Status === MyRetail.IAeviResponseStatus.Changed) {
                _this.AeviGrid.AeviDataService.showDataChangedModal(response)
                    .done(function () {
                    _this.AeviGrid.AeviDataService.AeviApiService.setDataEntityBlockStatus().done(function () {
                        _this.AeviGrid.AeviDataService.getDataByQuery('', true);
                    });
                })
                    .fail(function () {
                    _this.AeviGrid.AeviGridLocker.lock();
                });
            }
            else {
                _this.callCommit();
            }
        });
    };
    AeviToolbarHandler.prototype.callCommit = function () {
        this.AeviGrid.AeviGridHandler.triggerClick();
        var JSONFilter = '';
        if (!aeviIsUndefinedOrNull(this.AeviGrid.AeviFilter))
            JSONFilter = encodeURIComponent(this.AeviGrid.AeviFilter.getJSONFilterValues());
        this.AeviGrid.AeviDataService.commit(JSONFilter, true);
        this.AeviGrid.AeviGridHandler.AeviHeaderColumnHandler.unSort();
    };
    AeviToolbarHandler.prototype.discard = function () {
        this.AeviGrid.AeviGridHandler.triggerClick();
        var modal = new AeviModal(this.AeviGrid);
        var content = {
            title: null,
            text: null
        };
        content.title = this.AeviLocalization.translate('warning');
        if (this.AeviGrid.AeviDataService.AeviApiService.isDataChanged || this.AeviGrid.AeviDataService.AeviApiService.isCellChanged) {
            content.text = '<p>' + this.AeviLocalization.translate('discard_modal_message') + '</p>';
            content.buttons = ['aeviDataServiceDiscard', 'aeviDataServiceClose'];
        }
        else {
            content.text = '<p>' + this.AeviLocalization.translate('no_changes_message') + '</p>';
        }
        modal.setContent(content);
        modal.show();
        modal.focus('last');
    };
    AeviToolbarHandler.prototype.query = function () {
        var _this = this;
        $('.aeviToolbar__textfilter').trigger('blur');
        this.AeviGrid.AeviGridHandler.triggerClick();
        if (!aeviIsUndefinedOrNull(this.AeviGrid.AeviFilter)) {
            if (!this.AeviGrid.AeviFilter.isFiltersFilled()) {
                toastr.error(this.AeviLocalization.translate('some_filters_not_filled'));
                this.AeviGrid.AeviStatusBar.error(this.AeviLocalization.translate('some_filters_not_filled'));
                return;
            }
        }
        var JSONFilter = encodeURIComponent(this.AeviGrid.AeviFilter.getJSONFilterValues());
        if (this.AeviGrid.AeviDataService.AeviApiService.isDataChanged || this.AeviGrid.AeviDataService.AeviApiService.isCellChanged) {
            var modal = new AeviModal(this.AeviGrid);
            modal.setContent({
                title: this.AeviLocalization.translate('warning'),
                text: '<p>' + this.AeviLocalization.translate('temporary_message_filter') + '</p>',
                buttons: [
                    'aeviDataServiceCommitTempAndClose',
                    'aeviDataServiceDeleteTempAndClose',
                    'aeviDataServiceClose'
                ]
            });
            modal.show();
            modal.focus('last');
        }
        else {
            if (aeviIsUndefinedOrNull(JSONFilter)) {
                this.AeviGrid.print('AeviToolbarHandler.query(), variable "JSONFilter" is undefined or null.');
                return;
            }
            this.AeviGrid.AeviDataService.getDataByQuery(JSONFilter, true).done(function (response) {
                if (!_.isNull(response.Data))
                    toastr.info(_this.AeviLocalization.translate('result_showed') + '' + response.Data.length + ' ' + _this.AeviLocalization.translate('records'));
                _this.AeviGrid.AeviAjaxPreloader.hide();
            });
        }
    };
    AeviToolbarHandler.prototype.locker = function () {
        if (aeviIsUndefinedOrNull(this.AeviGrid.AeviGridLocker))
            this.AeviGrid.AeviGridLocker = new AeviGridLocker(this.AeviGrid);
        if (this.AeviGrid.AeviGridLocker.isLocked)
            this.AeviGrid.AeviGridLocker.unLock();
        else
            this.AeviGrid.AeviGridLocker.lock();
    };
    AeviToolbarHandler.prototype.search = function () {
        this.AeviGrid.AeviGridHandler.triggerClick();
        if (aeviIsUndefinedOrNull(this.AeviGrid.AeviSearch))
            this.AeviGrid.AeviSearch = new AeviSearch(this.AeviGrid);
        else
            this.AeviGrid.AeviSearch.show();
    };
    AeviToolbarHandler.prototype.refresh = function () {
        this.AeviGrid.AeviGridEditor.refreshData();
    };
    AeviToolbarHandler.prototype.insertRow = function () {
        this.AeviGrid.AeviGridEditor.renderNewRow();
    };
    AeviToolbarHandler.prototype.insertByForm = function () {
        var _this = this;
        this.AeviGrid.AeviDataService.getBlockStateCommand()
            .done(function (response) {
            if (response === MyRetail.IAeviBlockStateCommand.CONTINUE) {
                _this.insertRow();
                var selectedRowIndex = _this.AeviGrid.AeviDOM.getSelectedRowIndex();
                if (_.isNull(selectedRowIndex)) {
                    var lastSelectedCell = _this.AeviGrid.AeviGridHandler.lastSelectedCell[0];
                    var cellIndexes = _this.AeviGrid.AeviDOM.getCellIndexes(lastSelectedCell);
                    selectedRowIndex = cellIndexes.rowIndex;
                }
                selectedRowIndex++;
                return new AeviForm(selectedRowIndex, _this.AeviGrid.AeviDataService, { isOpenedByToolbar: true });
            }
        })
            .fail(function (response) {
            if (response === MyRetail.IAeviBlockStateCommand.LOCK) {
                _this.AeviGrid.AeviGridLocker.lock();
            }
            if (response === MyRetail.IAeviBlockStateCommand.RELOAD) {
                _this.AeviGrid.AeviDataService.getDataByQuery('', true);
            }
        });
    };
    AeviToolbarHandler.prototype.deleteRows = function () {
        var _this = this;
        if (!_.isNull(this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.fixedSize) || this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.readOnly)
            return;
        var rowGuidsLength = this.AeviGrid.AeviGridEditor.getDeletedRowGuids().length;
        if (rowGuidsLength === 0) {
            var modalWarning = new AeviModal(this.AeviGrid);
            modalWarning.setContent({
                title: this.AeviLocalization.translate('warning'),
                text: '<p>' + this.AeviLocalization.translate('no_rows_selected') + '</p>'
            });
            modalWarning.show();
            return;
        }
        var modal = new AeviModal(this.AeviGrid, function (confirm) {
            if (confirm)
                _this.AeviGrid.AeviGridEditor.deleteRow();
        });
        modal.setContent({
            title: this.AeviLocalization.translate('warning'),
            text: '<p>' + this.AeviLocalization.translate('delete_rows_question') + ' ' + this.AeviGrid.AeviDataRepository.AeviDataEntity.data.Caption + ' ?' + '</p>',
            buttons: [
                'aeviModalAccept',
                'aeviModalDenied'
            ]
        });
        modal.show();
        modal.focus('last');
    };
    AeviToolbarHandler.prototype.popupSwitch = function () {
        var toastrContainer = $('#toast-container');
        var button = $('.aeviToolbar__showPopup');
        if (toastr.options.positionClass.indexOf('hidden') === -1) {
            toastrContainer.addClass('hidden');
            toastr.options.positionClass += ' hidden';
            button.removeClass('checked');
            localStorage.setItem('aeviPopupSwitch', 'false');
        }
        else {
            toastrContainer.removeClass('hidden');
            toastr.options.positionClass = toastr.options.positionClass.replace('hidden', '');
            button.addClass('checked');
            localStorage.setItem('aeviPopupSwitch', 'true');
        }
    };
    AeviToolbarHandler.prototype.setFixRows = function (value) {
        if (_.isNumber(value))
            value = parseInt(value);
        if (value === 'null')
            value = null;
        value = parseInt(value);
        this.AeviGrid.AeviDimensions.fixedHeight = value;
        this.AeviGrid.AeviConsts.pager = {
            rowHeight: this.rowHeight,
            tollerance: 50,
            visibleRows: 200,
            countOfNewRows: 50
        };
        this.AeviGrid.AeviDataService.showData('re-render');
    };
    AeviToolbarHandler.prototype.paste = function () {
        var _this = this;
        this.AeviGrid.AeviDataService.getBlockStateCommand()
            .done(function (response) {
            if (response === MyRetail.IAeviBlockStateCommand.CONTINUE) {
                _this.showPasteModal();
            }
        })
            .fail(function (response) {
            if (response === MyRetail.IAeviBlockStateCommand.LOCK) {
                _this.AeviGrid.AeviGridLocker.lock();
            }
            if (response === MyRetail.IAeviBlockStateCommand.RELOAD) {
                _this.AeviGrid.AeviDataService.getDataByQuery('', true);
            }
        });
    };
    AeviToolbarHandler.prototype.showPasteModal = function () {
        var _this = this;
        var modal = new AeviModal(this.AeviGrid);
        var onShowFunction = null;
        var onCloseFunction = null;
        if (this.AeviGrid.AeviDataService.AeviApiService.isDataChanged) {
            modal.setContent({
                title: this.AeviLocalization.translate('warning'),
                text: '<p>' + this.AeviLocalization.translate('paste_first_save') + '</p>'
            });
        }
        else {
            modal.setContent({
                title: this.AeviGrid.AeviLocalization.translate('modal_inserting'),
                text: '<p>' + this.AeviLocalization.translate('toolbar_paste_message') + '</p><input type="text" class="fakeInput" id="fakeInput">'
            });
            onShowFunction = function () { _this.AeviGrid.isClipboardModalOpened = 1; };
            onCloseFunction = function () { _this.AeviGrid.isClipboardModalOpened = 0; };
        }
        modal.show(onShowFunction, onCloseFunction);
    };
    AeviToolbarHandler.prototype.exportCSV = function () {
        var csv = this.AeviGrid.AeviDataRepository.AeviTableData.getDataAsCSV();
        var fileName = this.AeviGrid.AeviDataRepository.AeviDataEntity.data.Caption + '.csv';
        var dataType = 'data:text/csv;charset=utf-8,%ef%bb%bf';
        this.saveFile(csv, dataType, fileName);
    };
    AeviToolbarHandler.prototype.saveFile = function (data, dataType, fileName) {
        var browser = this.AeviGrid.AeviClientSide.getTrimmedBrowser();
        switch (browser) {
            case 'chrome':
            case 'firefox':
                var encodedURI = dataType + encodeURI(data);
                var link = document.createElement('a');
                link.setAttribute('href', encodedURI);
                link.setAttribute('target', '_blank');
                link.setAttribute('download', fileName);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
                break;
            case 'ie':
            case 'edge':
                var encodedURI = '\ufeff' + data;
                var link = document.createElement('a');
                if (window.navigator.msSaveOrOpenBlob) {
                    var blobObject = new Blob([encodedURI]);
                    link.onclick = function () {
                        window.navigator.msSaveOrOpenBlob(blobObject, fileName);
                    };
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
                break;
            case 'safari':
                this.AeviGrid.log('saveFile is not supported for this browser.');
                break;
            default:
                break;
        }
    };
    AeviToolbarHandler.prototype.importFile = function () {
        var _this = this;
        if (!this.AeviGrid.AeviDOM.length(AeviConsts.fileImportInput)) {
            var importHTML = '<form id="importForm" style="display: none;" enctype="multipart/form-data" action="/" method="POST"><input name="file" id="' + AeviConsts.fileImportInput.makeClassFromSelector() + '" type="file" /></form>';
            $('body').append(importHTML);
        }
        $(document).off('change', AeviConsts.fileImportInput).on('change', AeviConsts.fileImportInput, function (event) {
            _this.AeviGrid.AeviDataService.getBlockStateCommand()
                .done(function (response) {
                if (response === MyRetail.IAeviBlockStateCommand.CONTINUE) {
                    var fileInput = document.getElementById(AeviConsts.fileImportInput.makeClassFromSelector());
                    var file = fileInput.files[0];
                    var type = file.type;
                    var fileType = null;
                    if (type === 'application/vnd.ms-excel' || type === 'text/csv' || type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        var csvRegex = new RegExp(".+(\.csv)$");
                        var xlsRegex = new RegExp(".+(\.xlsx?)$");
                        if (csvRegex.test(file.name)) {
                            fileType = 'Csv';
                        }
                        if (xlsRegex.test(file.name)) {
                            fileType = 'Xlsx';
                        }
                        var reader = new FileReader();
                        reader.onload = function () {
                            var base64Str = 'base64,';
                            var importDataWithMime = reader.result;
                            var startIndexOfBase64 = importDataWithMime.indexOf(base64Str);
                            var importDataStartIndex = startIndexOfBase64 + base64Str.length;
                            var importData = importDataWithMime.substring(importDataStartIndex, importDataWithMime.length);
                            _this.AeviGrid.AeviDataService.importFile(importData, fileType);
                            fileInput.value = '';
                        };
                        var hideImportMessage = AeviStorage.getStorageParam('hideImportMessage');
                        if (hideImportMessage === false || _.isNull(hideImportMessage)) {
                            var modal = new AeviModal(_this.AeviGrid, function (confirm) {
                                if (confirm) {
                                    reader.readAsDataURL(file);
                                }
                                else {
                                    fileInput.value = '';
                                }
                            });
                            modal.setContent({
                                title: _this.AeviLocalization.translate('warning'),
                                text: _this.AeviLocalization.translate('import_modal_message') + '<br><br><label><input class="aevistorage" data-storageKey="hideImportMessage" type="checkbox"> ' + _this.AeviLocalization.translate('do_not_show_message_again') + '</label>',
                                buttons: ['aeviModalAccept', 'aeviModalDenied']
                            });
                            modal.show();
                            modal.focus('last');
                        }
                        else {
                            reader.readAsDataURL(file);
                        }
                    }
                    else {
                        var modal = new AeviModal(_this.AeviGrid);
                        modal.setContent({
                            title: _this.AeviLocalization.translate('warning'),
                            text: _this.AeviLocalization.translate('bad_file_format')
                        });
                        modal.show();
                    }
                }
            })
                .fail(function (response) {
                if (response === MyRetail.IAeviBlockStateCommand.LOCK) {
                    _this.AeviGrid.AeviGridLocker.lock();
                }
                if (response === MyRetail.IAeviBlockStateCommand.RELOAD) {
                    _this.AeviGrid.AeviDataService.getDataByQuery('', true);
                }
            });
        });
        $('#aeviImportFile').trigger('click');
    };
    AeviToolbarHandler.prototype.importOrExportFile = function (event) {
        event.stopPropagation();
        if (aeviIsUndefinedOrNull(this.importExportSubMenu)) {
            this.importExportSubMenu = new AeviToolbarSubMenu(this.AeviGrid, 'importexport');
        }
        if (this.importExportSubMenu.isOpened()) {
            this.importExportSubMenu.close();
        }
        else {
            this.importExportSubMenu.open(event.target);
        }
    };
    return AeviToolbarHandler;
}());
var AeviToolbar = (function () {
    function AeviToolbar(aeviGrid, filters) {
        this.AeviGrid = aeviGrid;
        this.AeviAppSelector = this.AeviGrid.tableId;
        this.localization = this.AeviGrid.AeviLocalization;
        this.filters = filters;
        this.initializeTools(filters);
    }
    AeviToolbar.prototype.initializeTools = function (filters) {
        var availableTools = [
            'border',
            'lock',
            'unlock',
            'save',
            'search',
            'copy',
            'refresh',
            'delete_row',
            'insert_row',
            'toolbar_disable_toastr',
            'sort_invalid',
            'page_size',
            'break',
            'query',
            'filter',
            'pdf',
            'xls',
            'paste',
            'discard_changes',
            'insert_by_form',
            'export',
            'importexport'
        ];
        this.tools = [];
        for (var i = 0; i < availableTools.length; i++) {
            var name = availableTools[i];
            var translate = this.localization.translate('toolbar_' + name);
            var html = '';
            var className = '';
            switch (name) {
                case 'page_size':
                    html = '<p class="aeviToolbar__' + name + '">' +
                        '<span>' + this.localization.translate('count_of_rows') + ': </span>' +
                        '<select>' +
                        '<option value="null">' + this.localization.translate('dynamic') + '</option>' +
                        '<option value="100">' + this.localization.translate('extended') + '</option>' +
                        '</select>' +
                        '</p>';
                    break;
                case 'query':
                    html = '<p class="aeviToolbar__searchwrapper"><input type="text" tabindex="-1" placeholder="' + this.localization.translate('query_placeholder') + '" id="' + this.AeviAppSelector + '-query' + '" class="aeviToolbar__query"></p>';
                    break;
                case 'break':
                    html = '<br>';
                    break;
                default:
                    if (name === 'pdf' || name === 'unlock' || name === 'save' || name === 'unset_block_mode') {
                        className = AeviToolbar.getSecondaryButtonClass();
                    }
                    if (name === 'xls' || name === 'insert_by_form' || name === 'importexport' || name === 'export') {
                        className = 'pull-right';
                    }
                    html = '<i id="' + this.AeviAppSelector + '-' + name + '" class="aeviToolbar__' + name + ' ' + className + '">' + translate + '</i>';
                    break;
            }
            this.tools[name] = html;
        }
        if (!_.isNull(filters) && filters.length) {
            for (i = 0; i < filters.length; i++) {
                this.tools['filter_' + filters[i].id] = filters[i].html;
            }
        }
    };
    AeviToolbar.getSecondaryButtonClass = function () {
        return 'aeviButton--secondary';
    };
    AeviToolbar.prototype.getTools = function (keys) {
        var html = '';
        var arrayOfKeys = keys.split('|').aeviClean('');
        for (var i = 0; i < arrayOfKeys.length; i++) {
            var tool = this.tools[arrayOfKeys[i]];
            if (!_.isUndefined(tool) && tool)
                html += tool;
        }
        this.renderedTools = arrayOfKeys;
        return html;
    };
    AeviToolbar.prototype.render = function () {
        var _this = this;
        this.clearTools();
        this.refreshTools();
        var template = '<div id="' + this.AeviAppSelector + '-toolbar" class="aeviToolbar">' + this.getTools(this.renderTools) + '</div>';
        $(this.AeviGrid.aeviWrapper).prepend(template);
        this.listener();
        setTimeout(function () {
            _this.AeviGrid.setSizes();
        }, 700);
    };
    AeviToolbar.prototype.refresh = function () {
        if (this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport())
            return;
        this.clearTools();
        this.refreshTools();
        $('#' + this.AeviAppSelector + '-toolbar').html(this.getTools(this.renderTools));
        this.AeviGrid.setSizes();
    };
    AeviToolbar.prototype.clearTools = function () {
        this.renderTools = '';
    };
    AeviToolbar.prototype.refreshTools = function () {
        this.clearTools();
        if (!_.isNull(this.filters)) {
            for (var i = 0; i < this.filters.length; i++)
                this.addTool('filter_' + this.filters[i].id + '|');
        }
        if (!_.isNull(this.filters) && this.filters.length)
            this.addTool('filter|');
        if (!this.AeviGrid.AeviDataRepository.AeviTableDescription.isFixed()) {
            if (!this.AeviGrid.AeviDataRepository.AeviTableDescription.isReadOnly()) {
                if (this.AeviGrid.AeviDataRepository.AeviDataEntity.data.Form === true) {
                    if (this.AeviGrid.AeviDataService.AeviUser.roles.Create) {
                        this.addTool('insert_by_form|');
                    }
                }
            }
        }
        if (_.isNull(this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.getReferenceSettingsColumnNameIndex())) {
            var showOnlyExport = false;
            if (this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.isReport()) {
                showOnlyExport = true;
            }
            else {
                if (!this.AeviGrid.AeviDataRepository.AeviTableDescription.isFixed()) {
                    if (!this.AeviGrid.AeviDataRepository.AeviTableDescription.isReadOnly()) {
                        showOnlyExport = false;
                    }
                }
                else {
                    showOnlyExport = true;
                }
            }
            if (showOnlyExport) {
                this.addTool('export|');
            }
            else {
                this.addTool('importexport|');
            }
        }
        this.addTool('break|');
        if (this.AeviGrid.AeviDataService.AeviUser.roles.Write) {
            this.addTool('save|');
            this.addTool('discard_changes|');
        }
        if (this.AeviGrid.AeviDataService.AeviUser.roles.Write) {
            if (!aeviIsUndefinedOrNull(this.AeviGrid.AeviGridLocker)) {
                if (!this.AeviGrid.AeviGridLocker.isLocked)
                    this.addTool('lock|');
                else
                    this.addTool('unlock|');
            }
        }
        this.addTool('search|');
        if (this.AeviGrid.blockMode) {
            this.addTool('unset_block_mode|');
        }
        if (!this.AeviGrid.AeviDataRepository.AeviTableDescription.isFixed()) {
            if (!this.AeviGrid.AeviDataRepository.AeviTableDescription.isReadOnly()) {
                if (this.AeviGrid.AeviDataRepository.AeviTableDescription.isAllowedPastingFromExcel()) {
                    if (this.AeviGrid.AeviDataService.AeviUser.roles.Create) {
                        this.addTool('paste|');
                    }
                }
                if (this.AeviGrid.AeviDataService.AeviUser.roles.Create) {
                    this.addTool('insert_row|');
                }
                if (this.AeviGrid.AeviDataService.AeviUser.roles.Delete) {
                    this.addTool('delete_row|');
                }
                if (!aeviIsUndefinedOrNull(this.AeviGrid.AeviGridLocker)) {
                    if (this.AeviGrid.AeviGridLocker.isLocked) {
                        this.removeTool('paste|');
                        this.removeTool('insert_row|');
                        this.removeTool('insert_by_form');
                        this.removeTool('delete_row|');
                    }
                }
            }
        }
        if (this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport()) {
            this.addTool('pdf|');
        }
        if (this.AeviGrid.mode.clipboard === 1) {
            this.removeTool('paste|');
            this.removeTool('insert_row|');
            this.removeTool('delete_row|');
            this.removeTool('unlock|');
            this.removeTool('lock|');
            this.removeTool('filter|');
            this.removeTool('filter_DataEntityFullTextFilter|');
            this.removeTool('importexport|');
            this.removeTool('insert_by_form');
        }
        if (this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport() || this.AeviGrid.AeviDataRepository.AeviTableDescription.isReadOnly()) {
            this.removeTool('unlock|');
            this.removeTool('save|');
            this.removeTool('discard_changes|');
        }
        if (this.AeviGrid.AeviDataRepository.AeviTableDescription.isReadOnly() && !this.AeviGrid.AeviDataRepository.AeviTableDescription.isReport()) {
            this.removeTool('importexport|');
        }
    };
    AeviToolbar.prototype.getSelectors = function () {
        var selectors = [];
        for (var tool in this.tools) {
            selectors.push('#' + this.AeviAppSelector + '-' + tool);
        }
        return selectors;
    };
    AeviToolbar.prototype.listener = function () {
        var _this = this;
        if (aeviIsUndefinedOrNull(this.AeviToolbarHandler)) {
            this.AeviToolbarHandler = new AeviToolbarHandler(this.AeviGrid);
        }
        var selectors = this.getSelectors();
        selectors.push('.aeviToolbar__textfilter');
        var stringifySelectors = selectors.join(", ");
        $(document).off('change', '.aeviToolbar__page_size select').on('change', '.aeviToolbar__page_size select', function (event) {
            _this.AeviToolbarHandler.setFixRows($(event.currentTarget).val());
        });
        $(document).off('click', stringifySelectors).on('click', stringifySelectors, function (event) {
            var className = _this.AeviGrid.AeviDOM.getClass(event.currentTarget).split(' ')[0];
            switch (className) {
                case 'aeviToolbar__save':
                    _this.AeviToolbarHandler.save();
                    break;
                case 'aeviToolbar__discard_changes':
                    _this.AeviToolbarHandler.discard();
                    break;
                case 'aeviToolbar__search':
                    _this.AeviToolbarHandler.search();
                    break;
                case 'aeviToolbar__lock':
                case 'aeviToolbar__unlock':
                    _this.AeviToolbarHandler.locker();
                    break;
                case 'aeviToolbar__filter':
                    _this.AeviToolbarHandler.query();
                    break;
                case 'aeviToolbar__refresh':
                    _this.AeviToolbarHandler.refresh();
                    break;
                case 'aeviToolbar__insert_row':
                    _this.AeviToolbarHandler.insertRow();
                    break;
                case 'aeviToolbar__delete_row':
                    _this.AeviToolbarHandler.deleteRows();
                    break;
                case 'aeviToolbar__toolbar_disable_toastr':
                    _this.AeviToolbarHandler.popupSwitch();
                    break;
                case 'aeviToolbar__pdf':
                    _this.AeviGrid.AeviDownloader.pdf();
                    break;
                case 'aeviToolbar__export':
                case 'aeviToolbar__importexport':
                    _this.AeviToolbarHandler.importOrExportFile(event);
                    break;
                case 'aeviToolbar__paste':
                    _this.AeviToolbarHandler.paste();
                    break;
                case 'aeviToolbar__sort_invalid':
                    _this.AeviGrid.AeviDataRepository.sortInvalid();
                    _this.AeviGrid.AeviGridEditor.refreshData();
                    break;
                case 'aeviToolbar__insert_by_form':
                    _this.AeviToolbarHandler.insertByForm();
                    break;
                default:
                    event.preventDefault();
                    break;
            }
        });
        $(document).off('keydown', stringifySelectors).on('keydown', stringifySelectors, function (event) {
            var className = _this.AeviGrid.AeviDOM.getClass(event.currentTarget).split(' ')[0];
            var keyString = _this.AeviGrid.AeviClientSide.getKeyName(event);
            switch (className) {
                case 'aeviToolbar__textfilter':
                    if (keyString === 'enter')
                        _this.AeviToolbarHandler.query();
                    break;
                default:
                    break;
            }
        });
    };
    AeviToolbar.prototype.addTool = function (tool) {
        this.renderTools += tool;
    };
    AeviToolbar.prototype.removeTool = function (tool) {
        this.renderTools = this.renderTools.replace(tool, '').replace('||', '|');
    };
    return AeviToolbar;
}());
var AeviToolbarSubMenu = (function () {
    function AeviToolbarSubMenu(aeviGrid, action) {
        this.AeviGrid = aeviGrid;
        this.action = action;
        this.isSubMenuOpened = false;
    }
    AeviToolbarSubMenu.prototype.render = function (el) {
        var HTML = '';
        HTML += '<div id="' + AeviToolbarSubMenu.SUBMENU_ID + '" class="aeviToolbar__submenu">';
        switch (this.action) {
            case 'importexport':
                HTML += this.getImportHTML();
                HTML += this.getExportHTML();
                break;
            case 'export':
                HTML = this.getExportHTML();
                break;
            default:
                break;
        }
        HTML += '</div>';
        $(el).append(HTML);
    };
    AeviToolbarSubMenu.prototype.open = function (el) {
        this.render(el);
        this.isSubMenuOpened = true;
        this.listener();
    };
    AeviToolbarSubMenu.prototype.close = function () {
        $('#' + AeviToolbarSubMenu.SUBMENU_ID).detach();
        this.isSubMenuOpened = false;
    };
    AeviToolbarSubMenu.prototype.listener = function () {
        var _this = this;
        $(document).off('click', '#' + AeviToolbarSubMenu.SUBMENU_ID + ' > *').on('click', '#' + AeviToolbarSubMenu.SUBMENU_ID + ' > *', function (event) {
            var btn = event.currentTarget;
            var action = btn.getAttribute('data-action');
            switch (action) {
                case 'export':
                    var exportType = btn.getAttribute('data-type');
                    switch (exportType) {
                        case 'csv':
                            return _this.AeviGrid.AeviDownloader.exportedCsv();
                        case 'patternexcel':
                            return _this.AeviGrid.AeviDownloader.exportedPatternXls();
                        case 'xlsx':
                            return _this.AeviGrid.AeviDownloader.exportedXlsx();
                        default:
                            break;
                    }
                    break;
                case 'import':
                    setTimeout(function () { return _this.close(); });
                    _this.AeviGrid.AeviToolbar.AeviToolbarHandler.importFile();
                    break;
                default:
                    break;
            }
        });
        $(document).on('click', 'body', function () {
            _this.close();
        });
    };
    AeviToolbarSubMenu.prototype.isOpened = function () {
        return this.isSubMenuOpened;
    };
    AeviToolbarSubMenu.prototype.getImportHTML = function () {
        var HTML = '';
        if (this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.isReport() || this.AeviGrid.AeviDataRepository.AeviTableDescription.isFixed()) {
            return HTML;
        }
        if (!this.AeviGrid.AeviGridLocker.isLocked) {
            HTML = '<span data-action="import">Import</span>';
        }
        return HTML;
    };
    AeviToolbarSubMenu.prototype.getExportHTML = function () {
        var HTML = '';
        var exportFormats = [
            'csv',
            'xlsx'
        ];
        if (this.AeviGrid.excelFilePath !== '' && this.AeviGrid.AeviDataRepository.AeviTableDescription.isAllowedPastingFromExcel()) {
            if (this.AeviGrid.mode.clipboard === 0) {
                exportFormats.push('patternexcel');
            }
        }
        for (var i = 0; i < exportFormats.length; i++) {
            var itemName = 'Export .' + exportFormats[i];
            if (exportFormats[i] === 'patternexcel') {
                itemName = this.AeviGrid.AeviLocalization.translate('toolbar_xls');
            }
            HTML += '<span data-action="export" data-type="' + exportFormats[i] + '">' + itemName + '</span>';
        }
        return HTML;
    };
    AeviToolbarSubMenu.SUBMENU_ID = 'aeviSubMenu';
    return AeviToolbarSubMenu;
}());
var AeviGuidGenerator = (function () {
    function AeviGuidGenerator() {
    }
    AeviGuidGenerator.prototype.generate = function () {
        var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
        return guid;
    };
    AeviGuidGenerator.prototype.generateOld = function () {
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
    };
    AeviGuidGenerator.prototype.s4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return AeviGuidGenerator;
}());
var AeviContextMenu = (function () {
    function AeviContextMenu(aeviGrid, contextMenuType) {
        this.contextMenuType = contextMenuType;
        this.AeviGrid = aeviGrid;
        this.AeviDataRepository = this.AeviGrid.AeviDataRepository;
        this.isContextMenuEmpty = false;
        this.initializeItems();
        if (this.AeviGrid.AeviDOM.isContextMenuVisible())
            $('.aeviContextMenu').hide('fade', 300);
        this.render();
        if (!this.handler) {
            this.handler = new AeviContextMenuHandler(this.AeviGrid, this);
        }
    }
    AeviContextMenu.prototype.initializeItems = function () {
        this.contextMenuItems = {
            'insertRowHtml': '<span class="aeviContextMenu__addRow"><%= insertRow %></span>',
            'deleteRowHtml': '<span class="aeviContextMenu__deleteRow"><%= deleteRow %></span>',
            'cut': '<span class="aeviContextMenu__cutCell"><%= cut %></span>',
            'copy': '<span class="aeviContextMenu__copyCell"><%= copy %></span>',
            'insert': '<span class="aeviContextMenu__insertCell"><%= paste %></span>'
        };
    };
    AeviContextMenu.prototype.getItems = function (keys) {
        var HTML = '';
        var arrayOfKeys = keys.split('|');
        this.renderedItems = [];
        for (var i = 0; i < arrayOfKeys.length; i++) {
            var value = this.contextMenuItems[arrayOfKeys[i]];
            if (!_.isUndefined(value) && value)
                HTML += value;
        }
        this.renderedItems = arrayOfKeys;
        return HTML;
    };
    AeviContextMenu.prototype.checkAndGetRowNumberTools = function () {
        if (!aeviIsUndefinedOrNull(this.AeviGrid.mode) && !aeviIsUndefinedOrNull(this.AeviGrid.mode.clipboard) && this.AeviGrid.mode.clipboard === true)
            return '';
        if (!_.isNull(this.AeviDataRepository.AeviTableDescription.fixedSize) || this.AeviDataRepository.AeviTableDescription.readOnly)
            return '';
        var tools = '';
        if (this.AeviGrid.AeviDataService.AeviUser.roles.Create) {
            tools += this.getItems('insertRowHtml|');
        }
        if (this.AeviGrid.AeviDataService.AeviUser.roles.Delete) {
            tools += this.getItems('deleteRowHtml|');
        }
        return tools;
    };
    AeviContextMenu.prototype.render = function () {
        var items = '';
        switch (this.contextMenuType) {
            case 'rowNumber':
                items = this.checkAndGetRowNumberTools();
                break;
            default:
                break;
        }
        if (items === '')
            this.isContextMenuEmpty = true;
        var itemsTemplate = _.template(items);
        var template = _.template('<div class="aeviContextMenu aeviContextMenu-<%= type %>">' +
            '<div class="aeviContextMenu__inner">' +
            '<%= items %>' +
            '</div>' +
            '</div>');
        $('html').append(template({
            type: this.contextMenuType,
            items: itemsTemplate({
                insertRow: this.AeviGrid.AeviLocalization.translate('insert_row'),
                deleteRow: this.AeviGrid.AeviLocalization.translate('delete_row'),
                copy: this.AeviGrid.AeviLocalization.translate('toolbar_copy')
            })
        }));
        this.contextMenu = $('.aeviContextMenu-' + this.contextMenuType);
    };
    AeviContextMenu.prototype.refreshItems = function () {
        var items = null;
        switch (this.contextMenuType) {
            case 'rowNumber':
                items = this.checkAndGetRowNumberTools();
                break;
            default:
                break;
        }
    };
    AeviContextMenu.prototype.showMenu = function (event) {
        if (!this.isContextMenuEmpty)
            this.handler.showMenu(event);
    };
    AeviContextMenu.prototype.hideMenu = function () {
        this.handler.hideMenu();
    };
    return AeviContextMenu;
}());
var AeviFakeEditor = (function () {
    function AeviFakeEditor(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.render();
    }
    AeviFakeEditor.prototype.render = function () {
        var selector = this.AeviGrid.AeviConsts.fakeInputSelector;
        if (this.AeviGrid.AeviDOM.length(selector))
            return;
        selector = selector.makeClassFromSelector();
        var styles = 'position: fixed; top: 0; left: 0; width: 0; height: 0; z-index: -9999999999;';
        $('body').append('<textarea id="' + selector + '" type= "text" style= "' + styles + '"></textarea>');
        this.editorEl = document.getElementById(selector);
    };
    AeviFakeEditor.prototype.setCSSPosition = function (y, x) {
        this.editorEl.style.top = y + 'px';
        this.editorEl.style.left = x + 'px';
    };
    AeviFakeEditor.prototype.setContent = function (cell) {
        this.currentValue = this.AeviGrid.AeviDataRepository.AeviTableData.getRecordsToClipboard();
        this.editorEl.value = this.currentValue;
        if (this.AeviGrid.AeviClientSide.isIe() || this.AeviGrid.AeviClientSide.isEdge()) {
            var offset = AeviBodyCell.getOffset(cell[0]);
            this.setCSSPosition(offset.top, offset.left);
        }
    };
    AeviFakeEditor.prototype.getContent = function () {
        return this.currentValue;
    };
    AeviFakeEditor.prototype.focus = function () {
        this.editorEl.select();
    };
    return AeviFakeEditor;
}());
var AeviFilterHandler = (function () {
    function AeviFilterHandler(aeviFilter) {
        this.AeviFilter = aeviFilter;
        this.AeviGrid = this.AeviFilter.AeviGrid;
        this.listener();
    }
    AeviFilterHandler.prototype.listener = function () {
        var _this = this;
        var selectors = ['.aeviToolbar__numberfilter', '.aeviToolbar__textfilter'].join(', ');
        $(document).on('blur', selectors, function (event) {
            var editor = $(event.currentTarget);
            var val = editor.val();
            var columnName = editor[0].getAttribute('data-column');
            _this.AeviFilter.setFilterValue(columnName, val);
        });
    };
    return AeviFilterHandler;
}());
var AeviFilterEditor = (function () {
    function AeviFilterEditor(filter) {
        this.filter = filter;
    }
    AeviFilterEditor.prototype.render = function () {
        this.filter.Caption = (_.isNull(this.filter.Caption)) ? '' : '<span class="aeviToolbar__filterCaption">' + this.filter.Caption + ': </span>';
        var required = (this.filter.Required) ? '* ' : '';
        return '<div class="dib">' + this.filter.Caption + required + '<span class="aeviToolbar__filterWrapper" style="width: ' + this.filter.MinWidthPixels + 'px"><input type="text" tabindex="-1" id="filter_' + this.filter.ColumnName + '" data-column="' + this.filter.ColumnName + '" class="aeviToolbar__textfilter"></span></div>';
    };
    return AeviFilterEditor;
}());
var AeviNumberFilterEditor = (function (_super) {
    __extends(AeviNumberFilterEditor, _super);
    function AeviNumberFilterEditor(filter) {
        _super.call(this, filter);
    }
    AeviNumberFilterEditor.prototype.render = function () {
        return '<span class="aeviToolbar__filterWrapper" style="width: ' + this.filter.MinWidthPixels + 'px"><input type="text" tabindex="-1" placeholder="' + this.filter.Caption + '" id="filter_' + '" data-column="' + this.filter.ColumnName + '" class="aeviToolbar__numberfilter"></span>';
    };
    return AeviNumberFilterEditor;
}(AeviFilterEditor));
var AeviEnumFilterEditor = (function (_super) {
    __extends(AeviEnumFilterEditor, _super);
    function AeviEnumFilterEditor(filter) {
        _super.call(this, filter);
    }
    AeviEnumFilterEditor.prototype.render = function () {
        this.filter.Caption = (_.isNull(this.filter.Caption)) ? '' : '<span class="aeviToolbar__filterCaption">' + this.filter.Caption + ': </span>';
        var required = (this.filter.Required) ? '* ' : '';
        var html = [
            '<div class="dib">',
            this.filter.Caption,
            required,
            '<span class="aeviToolbarFilter aeviToolbarFilter_',
            this.filter.ColumnName,
            ' relative aeviToolbar__filterWrapper" style="width: ',
            this.filter.MinWidthPixels,
            'px">',
            this.getRenderedElement(),
            '</span></div>'
        ];
        return html.join('');
    };
    AeviEnumFilterEditor.prototype.getRenderedElement = function () {
        var enumValues = this.filter.EnumValues;
        var className = 'aeviToolbar__enumfilter';
        var defaultValue = this.filter.DefaultValue;
        var code = '<select id="aeviEnumEditor" class="' + className + '">';
        for (var i = 0; i < enumValues.length; i++) {
            var enumValue = enumValues[i];
            code += '<option value="' + enumValue.DataValue + '"' + this.getDefaultValue(defaultValue, enumValue.DataValue) + '>' + enumValue.DisplayValue + '</option>';
        }
        code += '</select>';
        return code;
    };
    AeviEnumFilterEditor.prototype.getDefaultValue = function (defaultValue, enumDataValue) {
        if (defaultValue == enumDataValue)
            return ' selected';
        return '';
    };
    return AeviEnumFilterEditor;
}(AeviFilterEditor));
var AeviDateFilterEditor = (function (_super) {
    __extends(AeviDateFilterEditor, _super);
    function AeviDateFilterEditor(filter, culture, displayType) {
        _super.call(this, filter);
        this.culture = culture;
        this.displayType = displayType;
    }
    AeviDateFilterEditor.prototype.initDateFormat = function () {
        this.AeviDate = new AeviDate(this.culture, this.displayType);
    };
    AeviDateFilterEditor.prototype.render = function () {
        var _this = this;
        this.initDateFormat();
        setTimeout(function () {
            _this.filterEl = $('.aeviToolbarFilter_' + _this.filter.ColumnName);
            _this.listener();
        });
        this.filter.Caption = (_.isNull(this.filter.Caption)) ? '' : '<span class="aeviToolbar__filterCaption">' + this.filter.Caption + ': </span>';
        var required = (this.filter.Required) ? '* ' : '';
        var aeviDate = new AeviDate(this.culture, this.filter.DisplayType, this.filter.DefaultValue);
        this.filter.DefaultValue = aeviDate.getString();
        return '<div class="dib">' + this.filter.Caption + required + '<span class="aeviToolbar__filterWrapper aeviToolbarFilter aeviToolbarFilter_' + this.filter.ColumnName + ' relative" style="width: ' + this.filter.MinWidthPixels + 'px"><input type="text" value="' + this.filter.DefaultValue + '" id="filter_' + this.filter.ColumnName + '" class="aeviToolbar__datefilter" value=""><span class="datepicker"></span></span></div>';
    };
    AeviDateFilterEditor.prototype.listener = function () {
        var _this = this;
        $(document).on(AeviConsts.clickEvent + ' ' + AeviConsts.touchEvent, this.filterEl.selector + ' .datepicker', function () {
            _this.showCalendar();
        });
        var editor = this.filterEl.find('input, textarea, select');
        if (editor.length && window.navigator.msPointerEnabled) {
            editor[0].addEventListener("MSPointerDown", function () { this.showCalendar(); }, false);
        }
    };
    AeviDateFilterEditor.prototype.showCalendar = function () {
        var editor = this.filterEl.find('.aeviToolbar__datefilter');
        var culture = this.AeviDate.culture;
        $.datepicker.setDefaults($.datepicker.regional[culture]);
        var old_goToToday = $.datepicker._gotoToday;
        $.datepicker._gotoToday = function (id) {
            var currentDatepicker = this;
            old_goToToday.call(currentDatepicker, id);
            currentDatepicker._selectDate(id);
            setTimeout(function () {
                editor.datetimepicker('hide');
            }, 0);
        };
        editor.datetimepicker({ showTimepicker: false });
        editor.datetimepicker('show');
    };
    return AeviDateFilterEditor;
}(AeviFilterEditor));
var AeviFilterFactory = (function () {
    function AeviFilterFactory(aeviGrid) {
        this.AeviGrid = aeviGrid;
        this.AeviDataRepository = this.AeviGrid.AeviDataRepository;
    }
    AeviFilterFactory.prototype.createFilter = function (filter) {
        var displayType = this.AeviGrid.AeviConsts.dataTypes[filter.DisplayType];
        switch (displayType) {
            case 'Text':
                this.filter = new AeviFilterEditor(filter);
                break;
            case 'Number':
            case 'IntegerNumber':
                this.filter = new AeviNumberFilterEditor(filter);
                break;
            case 'DateTime':
            case 'ShortDate':
            case 'ShortTime':
                this.filter = new AeviDateFilterEditor(filter, this.AeviGrid.AeviLocalization.getCulture(), displayType);
                break;
            case 'Enum':
                this.filter = new AeviEnumFilterEditor(filter);
                break;
            default:
                this.filter = null;
                break;
        }
    };
    AeviFilterFactory.prototype.getFilter = function () {
        return this.filter;
    };
    return AeviFilterFactory;
}());
var AeviFilter = (function () {
    function AeviFilter(aeviGrid, filters) {
        this.AeviGrid = aeviGrid;
        this.initFilters(filters);
    }
    AeviFilter.prototype.initFilters = function (filters) {
        this.filters = [];
        this.AeviGrid.AeviFilterFactory = new AeviFilterFactory(this.AeviGrid);
        for (var i = 0; i < filters.length; i++) {
            var filter = filters[i];
            this.AeviGrid.AeviFilterFactory.createFilter(filter);
            var filterEditor = this.AeviGrid.AeviFilterFactory.getFilter();
            var editor = filterEditor.render();
            this.filters.push({
                id: filter.ColumnName,
                html: editor,
                value: null,
                displayType: this.AeviGrid.AeviConsts.dataTypes[filter.DisplayType],
                required: filter.Required
            });
        }
        this.handler = new AeviFilterHandler(this);
    };
    AeviFilter.prototype.setFilterValue = function (columnName, val) {
        for (var i = 0; i < this.filters.length; i++) {
            if (this.filters[i].id === columnName)
                this.filters[i].value = val;
        }
    };
    AeviFilter.prototype.getFilters = function () {
        return this.filters;
    };
    AeviFilter.prototype.getFilterValues = function () {
        var values = [];
        for (var i = 0; i < this.filters.length; i++) {
            var filter = this.filters[i];
            var value;
            switch (filter.displayType) {
                case 'DateTime':
                case 'ShortDate':
                case 'ShortTime':
                    var dateVal = $('.aeviToolbarFilter_' + filter.id + ' input').val();
                    this.AeviDate = new AeviDate(this.AeviGrid.AeviLocalization.getCulture(), filter.displayType, dateVal);
                    value = this.AeviDate.getIsoString();
                    break;
                case 'Enum':
                    value = $('.aeviToolbarFilter_' + filter.id + ' select').val();
                    break;
                default:
                    value = filter.value;
                    break;
            }
            if (value === 'null')
                value = null;
            values.push({
                ColumnName: filter.id,
                Value: value
            });
        }
        return values;
    };
    AeviFilter.prototype.getRequiredFiltersId = function () {
        var filters = [];
        for (var i = 0; i < this.filters.length; i++) {
            var filter = this.filters[i];
            if (filter.required)
                filters.push(filter.id);
        }
        return filters;
    };
    AeviFilter.prototype.isFiltersFilled = function () {
        var requiredFiltersId = this.getRequiredFiltersId();
        var filterValues = this.getFilterValues();
        for (var i = 0; i < filterValues.length; i++) {
            for (var j = 0; j < requiredFiltersId.length; j++) {
                if (filterValues[i].ColumnName === requiredFiltersId[j]) {
                    var filterVal = filterValues[i].Value;
                    if (aeviIsUndefinedOrNull(filterVal) || filterVal === 'null' || filterVal === '' || filterVal === ' ' || _.isEmpty(filterVal))
                        return false;
                }
            }
        }
        return true;
    };
    AeviFilter.prototype.getJSONFilterValues = function () {
        return JSON.stringify(this.getFilterValues());
    };
    return AeviFilter;
}());
var MyRetail;
(function (MyRetail) {
    (function (IAeviChangeType) {
        IAeviChangeType[IAeviChangeType["byClick"] = 0] = "byClick";
        IAeviChangeType[IAeviChangeType["byKey"] = 1] = "byKey";
        IAeviChangeType[IAeviChangeType["byGrid"] = 2] = "byGrid";
    })(MyRetail.IAeviChangeType || (MyRetail.IAeviChangeType = {}));
    var IAeviChangeType = MyRetail.IAeviChangeType;
})(MyRetail || (MyRetail = {}));
var AeviContextMenuHandler = (function () {
    function AeviContextMenuHandler(aeviGrid, aeviContextMenu) {
        this.AeviGrid = aeviGrid;
        this.AeviContextMenu = aeviContextMenu;
        this.contextMenu = this.AeviContextMenu.contextMenu;
        this.listener();
    }
    AeviContextMenuHandler.prototype.listener = function () {
        var _this = this;
        $(document).on('mousedown', '.aeviContextMenu span', function (event) {
            event.preventDefault();
            event.stopPropagation();
            var menuItem = event.currentTarget;
            var className = menuItem.getAttribute('class');
            switch (className) {
                case 'aeviContextMenu__addRow':
                    _this.AeviGrid.AeviGridEditor.renderNewRow();
                    break;
                case 'aeviContextMenu__deleteRow':
                    _this.AeviGrid.AeviToolbar.AeviToolbarHandler.deleteRows();
                    break;
                default:
                    break;
            }
            _this.hideMenu();
        });
    };
    AeviContextMenuHandler.prototype.showMenu = function (event) {
        this.contextMenu
            .css({
            'top': event.pageY + 'px',
            'left': event.pageX + 'px'
        })
            .show('fade', 100)
            .addClass('isVisible');
    };
    AeviContextMenuHandler.prototype.hideMenu = function () {
        this.contextMenu.removeClass('isVisible').hide('fade', 75);
    };
    return AeviContextMenuHandler;
}());
var AeviFormHandler = (function () {
    function AeviFormHandler(aeviForm) {
        this.AeviForm = aeviForm;
        this.AeviGrid = this.AeviForm.AeviGrid;
        this.buttons = {
            'close': $('.' + AeviFormHandler.formPrefix + this.AeviForm.rowIndex + '-close'),
            'save': $('#' + AeviFormHandler.formPrefix + this.AeviForm.rowIndex + '-save'),
            'leftArrow': $('#' + AeviFormHandler.formPrefix + this.AeviForm.rowIndex + '-left'),
            'rightArrow': $('#' + AeviFormHandler.formPrefix + this.AeviForm.rowIndex + '-right'),
            'rowInput': $('#' + AeviFormHandler.formPrefix + this.AeviForm.rowIndex + '-rowInput')
        };
        this.listener();
        this.formListener();
    }
    AeviFormHandler.prototype.listener = function () {
        var _this = this;
        for (var i = 0; i < this.AeviForm.editors.length; i++) {
            var editor = this.AeviForm.editors[i];
            editor.initFormBinder(this.AeviForm.AeviFormRepository);
            editor.formListener();
        }
        $(document).on('keydown', function (event) {
            if (!_this.AeviGrid.AeviDOM.isFormVisible()) {
                return;
            }
            var key = _this.AeviGrid.AeviClientSide.getKeyName(event);
            if (key === 'escape') {
                _this.AeviForm.destroy();
            }
        });
    };
    AeviFormHandler.prototype.formListener = function () {
        var _this = this;
        this.buttons.close.on('click', function (event) {
            event.preventDefault();
            _this.AeviForm.setImageDefaultValue();
            _this.AeviForm.destroy();
            _this.AeviGrid.AeviGridEditor.refreshRow({ rowIndex: _this.AeviForm.rowIndex });
        });
        this.buttons.save.on('click', function (event) {
            event.preventDefault();
            _this.AeviForm.updateRecord();
            _this.setRowPutType();
            _this.AeviGrid.AeviDataRepository.AeviTableData.validateRow(_this.AeviForm.rowIndex);
            _this.AeviGrid.AeviGridEditor.refreshRow({ rowIndex: _this.AeviForm.rowIndex });
            if (!_this.AeviForm.AeviDataService.AeviDataRepository.AeviTableData.AeviStatus.is(_this.AeviForm.rowIndex, 'invalid')) {
                _this.AeviForm.AeviDataService.putRow(_this.AeviForm.rowIndex);
            }
            _this.AeviForm.destroy();
        });
        this.buttons.leftArrow.on('click', function (event) {
            event.preventDefault();
            _this.AeviForm.setImageDefaultValue();
            _this.AeviForm.destroy();
            return new AeviForm(_this.AeviForm.rowIndex - 1, _this.AeviForm.AeviDataService, { isOpenedByToolbar: _this.AeviForm.isOpenedByToolbar });
        });
        this.buttons.rightArrow.on('click', function (event) {
            event.preventDefault();
            _this.AeviForm.setImageDefaultValue();
            _this.AeviForm.destroy();
            return new AeviForm(_this.AeviForm.rowIndex + 1, _this.AeviForm.AeviDataService, { isOpenedByToolbar: _this.AeviForm.isOpenedByToolbar });
        });
    };
    AeviFormHandler.prototype.setRowPutType = function () {
        var rowStatus = this.AeviForm.AeviDataService.AeviDataRepository.AeviTableData.AeviStatus.getByRowIndex(this.AeviForm.rowIndex);
        if (rowStatus.indexOf('newrow') === -1) {
            this.AeviForm.AeviDataService.AeviDataRepository.AeviTableData.AeviStatus.addByRowIndex(this.AeviForm.rowIndex, 'update');
        }
        if (rowStatus.indexOf('newrow') !== -1) {
            this.AeviForm.AeviDataService.AeviDataRepository.AeviTableData.AeviStatus.addByRowIndex(this.AeviForm.rowIndex, 'insert');
        }
    };
    AeviFormHandler.formPrefix = 'aeviForm-';
    return AeviFormHandler;
}());
var AeviDownloader = (function () {
    function AeviDownloader(aeviGrid) {
        this.AeviGrid = aeviGrid;
    }
    AeviDownloader.prototype.download = function (url) {
        return window.open(url, '_blank');
    };
    AeviDownloader.prototype.pdf = function () {
        var JSONFilter = encodeURIComponent(this.AeviGrid.AeviFilter.getJSONFilterValues());
        var JSONSortInfos = this.AeviGrid.AeviDataService.AeviDataRepository.AeviTableDescription.getJSONSortInfos();
        var url = this.AeviGrid.AeviDataService.AeviApiService.getDownloadFileUrl(JSONFilter, JSONSortInfos, 'Pdf');
        this.download(url);
    };
    AeviDownloader.prototype.exportedPatternXls = function () {
        this.download(this.AeviGrid.excelFilePath);
    };
    AeviDownloader.prototype.exportedCsv = function () {
        this.download(this.AeviGrid.AeviDataService.AeviApiService.getExportFileUrl('Csv'));
    };
    AeviDownloader.prototype.exportedXls = function () {
        this.download(this.AeviGrid.AeviDataService.AeviApiService.getExportFileUrl('Xls'));
    };
    AeviDownloader.prototype.exportedXlsx = function () {
        this.download(this.AeviGrid.AeviDataService.AeviApiService.getExportFileUrl('Xlsx'));
    };
    return AeviDownloader;
}());
var AeviDynamicEnumWrapper = (function () {
    function AeviDynamicEnumWrapper(dynamicEnum, tableDescription) {
        this.dataValueColumnIndex = null;
        this.displayValueColumnIndex = null;
        this.originalEnumValues = [];
        this.dynamicEnum = dynamicEnum;
        this.tableDescription = tableDescription;
        this.dataValueColumnName = dynamicEnum.DynamicEnumSettings.DataValueColumnName;
        this.displayValueColumnName = dynamicEnum.DynamicEnumSettings.DisplayValueColumnName;
        this.dataValueColumnIndex = null;
        this.displayValueColumnIndex = null;
        this.init();
    }
    AeviDynamicEnumWrapper.prototype.init = function () {
        this._isValid = this.validate();
        if (!this._isValid)
            return;
        this.originalEnumValues = this.dynamicEnum.EnumValues.slice();
    };
    AeviDynamicEnumWrapper.prototype.refreshOriginalEnumValues = function () {
        if (this.isValid()) {
            this.dynamicEnum.EnumValues = this.originalEnumValues.slice();
        }
    };
    AeviDynamicEnumWrapper.prototype.fillColumnsIndexes = function () {
        for (var i = 0; i < this.tableDescription.Columns.length; i++) {
            var column = this.tableDescription.Columns[i];
            var indexDeviation = 1;
            if (column.ColumnName == this.dataValueColumnName) {
                this.dataValueColumnIndex = i + indexDeviation;
            }
            else if (column.ColumnName == this.displayValueColumnName) {
                this.displayValueColumnIndex = i + indexDeviation;
            }
        }
        return (!aeviIsUndefinedOrNull(this.dataValueColumnIndex) && !aeviIsUndefinedOrNull(this.displayValueColumnIndex));
    };
    AeviDynamicEnumWrapper.prototype.getDynamicEnumValue = function (row) {
        if (aeviIsUndefinedOrNull(row) || row.length - 1 < this.dataValueColumnIndex || row.length - 1 < this.displayValueColumnIndex) {
            console.log("Row is invalid");
            return null;
        }
        return {
            DataValue: row[this.dataValueColumnIndex],
            DisplayValue: row[this.displayValueColumnIndex]
        };
    };
    AeviDynamicEnumWrapper.prototype.addOrUpdateEnumsValues = function (row) {
        this.addUpdateOrDeleteEnumsValues(row, 'insertUpdate');
    };
    AeviDynamicEnumWrapper.prototype.deleteEnumsValues = function (row) {
        this.addUpdateOrDeleteEnumsValues(row, 'delete');
    };
    ;
    AeviDynamicEnumWrapper.prototype.addUpdateOrDeleteEnumsValues = function (row, action) {
        if (!this.isValid()) {
            return;
        }
        if (aeviIsUndefinedOrNull(row)) {
            return;
        }
        var foundEnumValue = false;
        var _this = this;
        var dynamicEnumValue = _this.getDynamicEnumValue(row);
        if (!dynamicEnumValue) {
            return;
        }
        for (var i = 0; i < _this.dynamicEnum.EnumValues.length; i++) {
            var enumValue = _this.dynamicEnum.EnumValues[i];
            if (enumValue.DataValue === dynamicEnumValue.DataValue) {
                if (action === 'delete') {
                    _this.dynamicEnum.EnumValues.splice(i, 1);
                    return;
                }
                else {
                    foundEnumValue = true;
                    enumValue.DisplayValue = dynamicEnumValue.DisplayValue;
                    break;
                }
            }
        }
        if (!foundEnumValue) {
            _this.dynamicEnum.EnumValues.push({
                DisplayValue: dynamicEnumValue.DisplayValue,
                DataValue: dynamicEnumValue.DataValue
            });
        }
    };
    ;
    AeviDynamicEnumWrapper.prototype.isValid = function () {
        if (!this._isValid) {
            console.log("AeviDynamicEnumWrapper is not valid");
            return false;
        }
        return this._isValid;
    };
    AeviDynamicEnumWrapper.prototype.validate = function () {
        if (aeviIsUndefinedOrNull(this.dynamicEnum)) {
            console.log('this.dynamicEnum is null');
            return false;
        }
        if (aeviIsUndefinedOrNull(this.dynamicEnum.EnumValues)) {
            console.log('this.dynamicEnum.EnumValues is null');
            return false;
        }
        if (aeviIsUndefinedOrNull(this.tableDescription)) {
            console.log('this.tableDescription is null');
            return false;
        }
        if (aeviIsUndefinedOrNull(this.tableDescription.Columns) || this.tableDescription.Columns.length === 0) {
            console.log('this.tableDescription.Columns is null');
            return false;
        }
        if (!this.fillColumnsIndexes()) {
            console.log('Cannot find indexes for following columns: ' + this.dataValueColumnName + ' and ' + this.displayValueColumnName);
            return false;
        }
        return true;
    };
    return AeviDynamicEnumWrapper;
}());
var AeviDynamicEnumPlugin = (function () {
    function AeviDynamicEnumPlugin(aeviGridApi) {
        this.AeviGridApi = aeviGridApi;
    }
    AeviDynamicEnumPlugin.prototype.init = function () {
        if (!this.isInitialized) {
            this.tableDescription = this.AeviGridApi.getTableDescription();
            this.fillDynamicEnums();
            this.isInitialized = true;
        }
        if (aeviIsUndefinedOrNull(this.dynamicEnumWrappers) || this.dynamicEnumWrappers.length === 0) {
            return;
        }
        var data = this.AeviGridApi.getTableData();
        if (aeviIsUndefinedOrNull(data) || data.length === 0) {
            return;
        }
        if (this.isInitialized === true) {
            this.refreshOriginalEnumsValues();
        }
        this.addUpdateOrDeleteEnumsValues(data, 'insertUpdate');
    };
    AeviDynamicEnumPlugin.prototype.onDataLoaded = function () {
        this.init();
    };
    AeviDynamicEnumPlugin.prototype.onRowsChanged = function (rows) {
        this.addUpdateOrDeleteEnumsValues(rows, 'insertUpdate');
    };
    AeviDynamicEnumPlugin.prototype.onRowsDeleting = function (rows) {
        this.addUpdateOrDeleteEnumsValues(rows, 'delete');
    };
    AeviDynamicEnumPlugin.prototype.onCellLeave = function () { };
    AeviDynamicEnumPlugin.prototype.fillDynamicEnums = function () {
        if (!aeviIsUndefinedOrNull(this.tableDescription) || !aeviIsUndefinedOrNull(this.tableDescription.Columns)) {
            var dataTypes = this.AeviGridApi.getColumnDisplayTypes();
            if (aeviIsUndefinedOrNull(dataTypes)) {
                this.AeviGridApi.log('dataTypes is null');
                return;
            }
            for (var i = 0; i < this.tableDescription.Columns.length; i++) {
                var column = this.tableDescription.Columns[i];
                if (this.isDynamicEnum(column, dataTypes)) {
                    this.dynamicEnumWrappers.push(new AeviDynamicEnumWrapper(column, this.tableDescription));
                }
            }
        }
    };
    AeviDynamicEnumPlugin.prototype.refreshOriginalEnumsValues = function () {
        for (var i = 0; i < this.dynamicEnumWrappers.length; i++) {
            this.dynamicEnumWrappers[i].refreshOriginalEnumValues();
        }
    };
    AeviDynamicEnumPlugin.prototype.addUpdateOrDeleteEnumsValues = function (rows, action) {
        if ((aeviIsUndefinedOrNull(rows) ||
            rows.length === 0) ||
            aeviIsUndefinedOrNull(this.dynamicEnumWrappers) ||
            this.dynamicEnumWrappers.length === 0) {
            return;
        }
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            for (var j = 0; j < this.dynamicEnumWrappers.length; j++) {
                if (action === 'delete') {
                    this.dynamicEnumWrappers[j].deleteEnumsValues(row);
                }
                else {
                    this.dynamicEnumWrappers[j].addOrUpdateEnumsValues(row);
                }
            }
        }
    };
    AeviDynamicEnumPlugin.prototype.isDynamicEnum = function (column, dataTypes) {
        var displayType = dataTypes[column.DisplayType];
        return (displayType === 'Enum' && !aeviIsUndefinedOrNull(column.DynamicEnumSettings) && !aeviIsUndefinedOrNull(column.DynamicEnumSettings.DataValueColumnName) && !aeviIsUndefinedOrNull(column.DynamicEnumSettings.DisplayValueColumnName));
    };
    return AeviDynamicEnumPlugin;
}());
var AeviActionPricePlugin = (function () {
    function AeviActionPricePlugin(aeviGridApi) {
        this.AeviGridApi = aeviGridApi;
        this.columnNames = {
            action: 'ActionPrice',
            actionSince: 'ActionSince',
            actionTill: 'ActionTill'
        };
    }
    AeviActionPricePlugin.prototype.onCellLeave = function (rowIndex, columnIndex, enterValue, leaveValue) {
        enterValue = (_.isNull(enterValue) || enterValue === 'null') ? null : enterValue;
        leaveValue = (_.isNull(leaveValue) || leaveValue === 'null') ? null : leaveValue;
        var column = this.AeviGridApi.getColumnHeader(columnIndex);
        if (this.isColumnNameActionPrice(column.ColumnName)) {
            if (!_.isNull(enterValue) || _.isNull(leaveValue))
                return;
            var todaysISODate = this.getTodaysISODate();
            var actionSinceIndex = this.AeviGridApi.getColumnHeaderIndexByName(this.columnNames.actionSince) + 1;
            this.AeviGridApi.updateCellValue(rowIndex, actionSinceIndex, todaysISODate);
            var actionTillIndex = this.AeviGridApi.getColumnHeaderIndexByName(this.columnNames.actionTill) + 1;
            this.AeviGridApi.updateCellValue(rowIndex, actionTillIndex, todaysISODate);
            this.AeviGridApi.refreshRow(rowIndex);
        }
    };
    AeviActionPricePlugin.prototype.isColumnNameActionPrice = function (columnName) {
        if (columnName.substring(0, 11) === this.columnNames.action)
            return true;
        return false;
    };
    AeviActionPricePlugin.prototype.getTodaysISODate = function () {
        return new Date().toISOString();
    };
    AeviActionPricePlugin.prototype.onDataLoaded = function () { };
    AeviActionPricePlugin.prototype.onRowsChanged = function () { };
    AeviActionPricePlugin.prototype.onRowsDeleting = function () { };
    return AeviActionPricePlugin;
}());
